static char rcsid[]="$Id: cicsLib.c,v 1.1.1.1 2005/12/21 11:25:52 gemvx Exp $";

/*
 *   FILENAME
 *   -------- 
 *   cicsLib.c
 *
 *   PURPOSE
 *   -------
 *   This file contains the source for all the library functions used by the
 *   Core Instrument Control System
 *
 *
 *   FUNCTION NAME(S)
 *   ----------------
 *   cicsDbGet         - Get a value from an EPICS field using database access
 *   cicsDbPut         - Put a value to an EPICS field using database access
 *   cicsSetDebug      - Set new debug level
 *   cicsGetDebug      - Get current debug level
 *   cicsSetTop        - Set top level prefixes for EPICS database.
 *
 *   DEPENDENCIES
 *   ------------
 *
 *   LIMITATIONS
 *   ------------
 *
 *   AUTHOR
 *   ------
 *   Steven Beard  (smb@roe.ac.uk)
 *   Janet Tvedt   (tvedt@noao.edu)
 *   Hubert Yamada (yamada@newton.ifa.hawaii.edu)
 *
 *   HISTORY
 *   -------
 *INDENT-OFF*
 *
 * $Log: cicsLib.c,v $
 * Revision 1.1.1.1  2005/12/21 11:25:52  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.2  2004/08/20 02:45:57  pjy
 * Gem 8.6
 * Added cicsGetDebug routine
 *
 * Revision 1.1.1.1  2000/09/21 01:33:37  jarnyk
 * Niri sources
 *
 * Revision 1.5  1999/11/14 02:05:39  yamada
 * Reformatted logs.
 *
 *
 *INDENT-ON*
 *
 *   04-Dec-1996: Original version with just cicsLogMessage,
 *                cicsLogLong and cicsLogString, based on Michelle
 *                logging library (which did not conform to the
 *                Gemini SPS).                                        (smb)
 *   16-Dec-1996: cicsLogFloat added.                                 (smb)
 *   10-Jan-1997: cicsLogDouble added.                                (smb)
 *   15-Jan-1997: A means of setting and testing the debug level
 *                added. Fixed some syntax and formatting problems
 *                noticed by Janet Tvedt.                             (smb)
 *   16-Jan-1997: Functions renamed to conform to new Gemini SPS.     (smb)
 *   23-Jan-1997: Removed the "static" storage class specifier on
 *                the various message string buffers. This was
 *                causing messages generated simultaneously by
 *                different parallel processes to be overwritten.
 *                Use MAX_STRING_SIZE instead of hard-wired size for
 *                log message.                                        (smb)
 *   27-Jan-1997: Modified cicsLogMessage to use a ring buffer of
 *                messages to get around a deficiency of logMsg().    (smb)
 *   04-Jun-1997: Size of message buffer increased from 40 to 64.     (smb)
 *   17-Jun-1997: Modified to use a semaphore to ensure that two
 *                tasks do not attempt to use the same element of
 *                the message buffer at the same time.                (smb)
 *   17-Jun-1997: Set top level prefixes. Janet Tvedt's database
 *                access functions added.                             (smb,tvedt)
 *   26-Jun-1997: Channel Access get and put functions added, but
 *                then transferred to "cicsLib2.c" because it was
 *                not possible to include "dbAccess.h" and
 *                "cadefs.h" in the same file..                       (smb)
 *   13-Oct-1999: Added cicsLogFmt and increased the size of the
 *                message buffer.                                     (yamada)
 */

/* Global Constants */

#include  <math.h>
#include  <time.h>
#include  <stdlib.h>
#include  <string.h>
#include  <stdarg.h>

#include  <dbDefs.h>
#include  <dbCommon.h>
#include  <recSup.h>
#include  <dbAccess.h>
#include  <errlog.h>

#include  <cicsConst.h>
#include  <cicsLib.h>


/* Global Variables */

static char cicsTop[MAX_STRING_SIZE] = "cics:";      /* Top level prefix */
static char cicsSadtop[MAX_STRING_SIZE] = "cics::";  /* Top level SAD prefix */

static long debugLevel = 1;     /* Current debugging level */
 /*                                0 = NOLOG
 *                                     No debugging or logging.
 *                                     Errors and warnings only.
 *                                 1 = NONE (default)
 *                                     Logging but no debugging.
 *                                     Errors and warnings, plus important
 *                                     log messages.
 *                                 2 = MIN
 *                                     Minimum debugging.
 *                                     Errors, warnings, log messages, plus
 *                                     supplemental information.
 *                                 3 = FULL
 *                                     Full debugging. All messages.
 */

/* ===================================================================== */


/*
 *+
 * FUNCTION NAME:
 * cicsDbGet
 *
 * INVOCATION:
 * long status;
 * char *fieldName;  
 * char *errMess;
 * unsigned short type;
 * double *outVal;
 *
 * status = cicsDbGet(fieldName, errMess, type, &outVal);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > fieldName (char *)               pointer to reocord_name.field_name
 * ! errMess   (char *)               pointer to string
 * > type      (unsigned short)       data type to convert output to 
 *                                    (DBF_DOUBLE, DBF_LONG, etc.)
 * < outval    (void *)               pointer to output value
 *
 * FUNCTION VALUE:
 * long  - status value returned to calling routine, a non-zero value
 *         indicates an error
 *
 * PURPOSE:
 * Function to get a value from an EPICS field by database access.
 *
 * DESCRIPTION:
 * This routine is called to obtain the value of the specified field 
 * within an EPICS database record.  It handles potential errors by
 * sending messages to the CICS logging functions and returning an
 * error emssage and status value to the calling routine.
 *
 * EXTERNAL VARIABLES:
 * dbNameToAddr    - EPICS database access routine for finding record address
 * dbGetField      - EPICS database access routine for retrieving the data
 * cicsLogMessage  - CICS logging function for a message
 * cicsLogString   - CICS logging function for a message + a string
 *
 * PRIOR REQUIREMENTS:
 * None.
 * 
 * DEFICIENCIES:
 * I am informed that the routine "recGblGetLinkValue" is preferred
 * to "dbGetField" by the EPICS community (because it can choose whether
 * to use Channel Access or Database Access according to circumstances),
 * but "recGblGetLinkValue" is much more complicated, and I don't understand
 * its description in the "EPICS IOC Application Developers Guide".
 *
 * HISTORY (optional):
 * 13-Mar-1997  Original version as getDbInfo.			Janet Tvedt
 * 17-Jun-1997  Imported into cicsLib.				Steven Beard
 *-
 */

long cicsDbGet(char *fieldName, char *errMess, unsigned short type, void *outVal)
{
    struct dbAddr addr;
    long ret, options=0L, nRq = 1L;
    long status;
    
    status = PASS;

    /* Get the address of the data structure  and handle any errors */
    if( (ret = dbNameToAddr (fieldName,&addr)) != 0L)
    {
	status = FAIL;
	sprintf(errMess, "dbNameToAddr error = %ld", ret);
	errlogMessage(errMess);
	errlogPrintf("dbName = %s", fieldName);
    }

    /* If address found, get the data.  Handle any errors. */
    if( status == PASS )
    {
       if( (ret = dbGetField(&addr, type, outVal, &options, &nRq, NULL)) != 0L)
       {
	   status = FAIL;
           sprintf(errMess, "dbGet error = %ld", ret);
	   errlogMessage(errMess);
       }
    }

    /* Return error status */
    return status;
}

/* ===================================================================== */


/*
 *+
 * FUNCTION NAME:
 * cicsDbPut
 *
 * INVOCATION:
 * char *fieldName;
 * char *errMess;
 * unsigned short type;
 * double outVal;
 * long status;
 *
 * status = cicsDbPut(fieldName, errMess, DBF_DOUBLE, &outVal)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > fieldName (char *)           pointer to record+field name
 * ! errMess   (char *)           pointer to string
 * > type      (unsigned short)   data type of value to put
 * > outVal    (void *)           pointer of data to put
 *
 * FUNCTION VALUE:
 * long  Status value returned to calling routine, a non-zero value indicates
 *       an error
 *
 * PURPOSE:
 * Function to put a value into an EPICS field by database access
 *
 * DESCRIPTION:
 * This routine may be called by a user subroutine to put a value
 * into an EPICS databaser record field.  The complete field name must
 * be specified (for example: sytem:subsystem:recordx.FIELDY).  This function
 * will also handle errors by logging them through the CICS logging functions
 * and by returning an error message and status value to the calling routine.
 *
 * EXTERNAL VARIABLES:
 * dbNameToAddr    - EPICS database access routine for finding record address
 * dbPutField      - EPICS database access routine for putting the data
 *
 * PRIOR REQUIREMENTS:
 * None
 *
 * DEFICIENCIES:
 * I am informed that the routine "recGblGetLinkValue" is preferred
 * to "dbGetField" by the EPICS community (because it can choose whether
 * to use Channel Access or Database Access according to circumstances),
 * but "recGblGetLinkValue" is much more complicated, and I don't understand
 * its description in the "EPICS IOC Application Developers Guide".
 *
 * HISTORY (optional):
 * 19-Mar-1997  Original version as putDbInfo.			Janet Tvedt
 * 17-Jun-1997  Imported into cicsLib				Steven Beard
 *-
 */


long cicsDbPut(char *fieldName, char *errMess, unsigned short type, void *outVal)
{
    struct dbAddr addr;
    long ret, nRq = 1L;
    long status;
    
    
    status = PASS;

    /* Get the address of the data structure  and handle any errors */
    if( (ret = dbNameToAddr (fieldName,&addr)) != 0L)
    {
	status = FAIL;
	sprintf(errMess, "dbNameToAddr error = %ld", ret);
	errlogMessage(errMess);
	errlogPrintf("dbName = %s ", fieldName);
    }

    /* If address found, write the data.  Handle any errors. */
    if( status == PASS )
    {
       if( (ret = dbPutField(&addr, type, outVal, nRq)) != 0L)
       {
	   status = FAIL;
           sprintf(errMess, "dbPutField error = %ld", ret);
	   errlogMessage(errMess);
       }
    }

    /* Return error status */
    return status;
}


/* ===================================================================== */


/*+
 *   Function name:
 *   cicsSetDebug
 *
 *   Purpose:
 *   Change the current debugging level.
 *
 *   Purpose:
 *   This routine is called whenever the CICS software needs to change the
 *   current debugging level.
 *
 *   Invocation:
 *   long debug;
 *   cicsSetDebug( debug  );
 *
 *   Parameters in:
 *      < debug       long       New debugging level.
 *                               For details see "cicsLogMessage" above.
 * 
 *   Parameters out:
 *
 *   Return value:
 *      This is a void function. It has no status value.
 *
 *   Globals:
 *      External functions:
 *
 *      External variables:
 *      < debugLevel  long        Current debugging level.
 *                                For details see "cicsLogMessage" above.
 *
 *   Requirements:
 *
 *   Limitations:
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   14-Jan-1997: Original version.                  (smb)
 *-
 */

void cicsSetDebug ( const long debug )
{

    debugLevel = debug;
}

/*+
 *   Function name:
 *   cicsGetDebug
 *
 *   Purpose:
 *   Gets the current debugging level.
 *
 *   Purpose:
 *   This routine is called whenever the CICS software needs to check the
 *   current debugging level.
 *
 *   Invocation:
 *   debug = cicsGetDebug();
 *
 *   Parameters in:
 * 
 *   Parameters out:
 *
 *   Return value:
 *      Returns current debugLevel
 *
 *   Globals:
 *      External functions:
 *
 *      External variables:
 *      < debugLevel  long        Current debugging level.
 *                                For details see "cicsLogMessage" above.
 *
 *   Requirements:
 *
 *   Limitations:
 *
 *   Author:
 *   Peter Young - RSAA
 *
 *   History:
 *   2 Aug 2004: Original version.                  (pjy)
 *-
 */

long cicsGetDebug ( void )
{

    return debugLevel;
}



/* ===================================================================== */


/*+
 *   Function name:
 *   cicsSetTop
 *
 *   Purpose:
 *   Set the current top level record name prefixes.
 *
 *   Purpose:
 *   This routine is called to define the top level record name prefixes.
 *
 *   Invocation:
 *   long debug;
 *   cicsSetTop( top, sadtop  );
 *   It may be invoked from the VxWorks startup script.
 *
 *   Parameters in:
 *      < top       *string      Top level record name prefix.
 *      < sadtop    *string      Top level SAD record name prefix.
 * 
 *   Parameters out:
 *
 *   Return value:
 *      This is a void function. It has no status value.
 *
 *   Globals:
 *      External functions:
 *
 *      External variables:
 *      < debugLevel  long        Current debugging level.
 *                                For details see "cicsLogMessage" above.
 *
 *   Requirements:
 *
 *   Limitations:
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   17-Jun-1997: Original version.                  (smb)
 *-
 */

void cicsSetTop ( const char* top, const char* sadtop )
{

    strncpy( cicsTop, top, MAX_STRING_SIZE );
    strncpy( cicsSadtop, sadtop, MAX_STRING_SIZE );
}
