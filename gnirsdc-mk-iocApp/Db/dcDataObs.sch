[schematic2]
uniq 479
[tools]
[detail]
w 1810 1067 100 0 n#478 ecad20.dataObs.OUTI 1824 1056 1856 1056 hwout.hwout#477.outp
w 1242 907 100 0 n#476 estringouts.obs_cosmMin.OUT 1152 896 1392 896 1392 1088 1504 1088 ecad20.dataObs.I
w 1938 203 100 0 n#474 ecad20.dataObs.OCID 1824 1696 1888 1696 1888 192 2048 192 cadCar.dataObsCC.ICID
w 1946 235 100 0 n#348 junction 1904 1792 1904 224 2048 224 cadCar.dataObsCC.STAT
w 826 1995 100 0 n#348 ecad20.dataObs.VAL 1824 1792 2288 1792 2288 1984 -576 1984 -576 1232 -432 1232 eapply.dataObsApply.INPA
w 1906 171 100 0 n#473 ecad20.dataObs.STLK 1824 160 2048 160 cadCar.dataObsCC.SLNK
w 738 1659 100 0 n#471 embbis.obs_dataDest.FLNK 544 1712 672 1712 672 1648 864 1648 estringouts.obs_dataDestStr.SLNK
w 674 1691 100 0 n#470 embbis.obs_dataDest.VAL 544 1680 864 1680 estringouts.obs_dataDestStr.DOL
w 1242 1643 100 0 n#469 estringouts.obs_dataDestStr.OUT 1120 1632 1424 1632 1424 1600 1504 1600 ecad20.dataObs.A
w 1218 1099 100 0 n#462 estringouts.obs_cosmThrsh.OUT 1152 1088 1344 1088 1344 1152 1504 1152 ecad20.dataObs.H
w 1122 1227 100 0 n#460 ebos.obs_cosmRej.OUT 528 528 800 528 800 1216 1504 1216 ecad20.dataObs.G
w 1106 1291 100 0 n#455 ebos.obs_saveQual.OUT 528 720 768 720 768 1280 1504 1280 ecad20.dataObs.F
w 1074 1355 100 0 n#454 ebos.obs_saveVar.OUT 528 912 704 912 704 1344 1504 1344 ecad20.dataObs.E
w 1042 1419 100 0 n#448 ebos.obs_saveCoadds.OUT 528 1088 640 1088 640 1408 1504 1408 ecad20.dataObs.D
w 1026 1483 100 0 n#445 ebos.obs_saveNdrs.OUT 528 1264 608 1264 608 1472 1504 1472 ecad20.dataObs.C
w 1810 1131 100 0 n#444 ecad20.dataObs.OUTH 1824 1120 1856 1120 hwout.hwout#442.outp
w 1810 1195 100 0 n#440 ecad20.dataObs.OUTG 1824 1184 1856 1184 hwout.hwout#439.outp
w 1810 1259 100 0 n#421 ecad20.dataObs.OUTF 1824 1248 1856 1248 hwout.hwout#410.outp
w 1810 1323 100 0 n#420 ecad20.dataObs.OUTE 1824 1312 1856 1312 hwout.hwout#409.outp
w 1810 1387 100 0 n#415 ecad20.dataObs.OUTD 1824 1376 1856 1376 hwout.hwout#404.outp
w 1810 1451 100 0 n#414 ecad20.dataObs.OUTC 1824 1440 1856 1440 hwout.hwout#403.outp
w 1810 1515 100 0 n#413 ecad20.dataObs.OUTB 1824 1504 1856 1504 hwout.hwout#402.outp
w 1810 1579 100 0 n#401 ecad20.dataObs.OUTA 1824 1568 1856 1568 hwout.hwout#400.outp
w 1010 1547 100 0 n#384 ecad20.dataObs.B 1504 1536 576 1536 576 1440 528 1440 ebos.obs_save.OUT
w 826 2027 100 0 n#349 ecad20.dataObs.MESS 1824 1760 2320 1760 2320 2016 -608 2016 -608 1200 -432 1200 eapply.dataObsApply.INMA
w -494 1307 -100 0 CLID inhier.CLID.P -528 1376 -496 1376 -496 1296 -432 1296 eapply.dataObsApply.CLID
w 682 1867 100 0 n#468 eapply.dataObsApply.OCLA -48 1200 192 1200 192 1856 1232 1856 1232 1760 1504 1760 ecad20.dataObs.ICID
w 682 1899 100 0 n#325 eapply.dataObsApply.OUTA -48 1232 160 1232 160 1888 1264 1888 1264 1792 1504 1792 ecad20.dataObs.DIR
w 68 1371 100 0 MESS eapply.dataObsApply.MESS -48 1296 64 1296 64 1456 112 1456 outhier.MESS.p
w -12 1427 100 0 VAL eapply.dataObsApply.VAL -48 1328 -16 1328 -16 1536 112 1536 outhier.VAL.p
w -460 1395 100 0 n#114 inhier.DIR.P -496 1472 -464 1472 -464 1328 -432 1328 eapply.dataObsApply.DIR
s -688 -48 200 0 Up to 20 observation parameters can be set here
s -688 32 200 0 Obs mode data parameters Apply/CAD record
[cell use]
use hwout 1856 1015 100 0 hwout#477
xform 0 1952 1056
p 2000 1056 100 0 -1 val(outp):$(sadtop)d_obs_cosmMin PP NMS
use hwout 1856 1079 100 0 hwout#442
xform 0 1952 1120
p 2000 1120 100 0 -1 val(outp):$(sadtop)d_obs_cosmThrsh PP NMS
use hwout 1856 1527 100 0 hwout#400
xform 0 1952 1568
p 2000 1568 100 0 -1 val(outp):$(sadtop)d_obs_dataDest PP NMS
use hwout 1856 1463 100 0 hwout#402
xform 0 1952 1504
p 2000 1504 100 0 -1 val(outp):$(sadtop)d_obs_save PP NMS
use hwout 1856 1399 100 0 hwout#403
xform 0 1952 1440
p 2000 1440 100 0 -1 val(outp):$(sadtop)d_obs_saveNdrs PP NMS
use hwout 1856 1335 100 0 hwout#404
xform 0 1952 1376
p 2000 1376 100 0 -1 val(outp):$(sadtop)d_obs_saveCoadds PP NMS
use hwout 1856 1271 100 0 hwout#409
xform 0 1952 1312
p 2000 1312 100 0 -1 val(outp):$(sadtop)d_obs_saveVar PP NMS
use hwout 1856 1207 100 0 hwout#410
xform 0 1952 1248
p 2000 1248 100 0 -1 val(outp):$(sadtop)d_obs_saveQual PP NMS
use hwout 1856 1143 100 0 hwout#439
xform 0 1952 1184
p 2000 1184 100 0 -1 val(outp):$(sadtop)d_obs_cosmRej PP NMS
use estringouts 896 839 100 0 obs_cosmMin
xform 0 1024 912
use estringouts 896 1031 100 0 obs_cosmThrsh
xform 0 1024 1104
use estringouts 864 1575 100 0 obs_dataDestStr
xform 0 992 1648
p 800 1454 100 0 0 OMSL:closed_loop
use cadCar 2048 71 100 0 dataObsCC
xform 0 2128 176
p 2068 44 100 0 1 set:cad dataObs
use embbis 368 1760 100 0 obs_dataDest
xform 0 416 1696
p 352 1616 100 0 1 ONST:FITS
p 480 1616 100 0 1 TWST:NONE
p 240 1616 100 0 1 ZRST:DHS
use ebos 336 1552 100 0 obs_save
xform 0 400 1472
p -48 1326 100 0 0 ONAM:No
p -48 1358 100 0 0 ZNAM:Yes
use ebos 336 1376 100 0 obs_saveNdrs
xform 0 400 1296
p -48 1150 100 0 0 ONAM:No
p -48 1182 100 0 0 ZNAM:Yes
use ebos 336 1200 100 0 obs_saveCoadds
xform 0 400 1120
p -48 974 100 0 0 ONAM:No
p -48 1006 100 0 0 ZNAM:Yes
use ebos 336 1024 100 0 obs_saveVar
xform 0 400 944
p -48 798 100 0 0 ONAM:No
p -48 830 100 0 0 ZNAM:Yes
use ebos 336 832 100 0 obs_saveQual
xform 0 400 752
p -48 606 100 0 0 ONAM:No
p -48 638 100 0 0 ZNAM:Yes
use ebos 336 640 100 0 obs_cosmRej
xform 0 400 560
p -48 414 100 0 0 ONAM:No
p -48 446 100 0 0 ZNAM:Yes
use ecars -352 560 150 0 dataObsC
xform 0 -272 400
p -448 208 100 0 1 def(FLNK):$(top)combCars12.PROC
use inhier -512 1431 100 0 DIR
xform 0 -496 1472
use inhier -544 1335 100 0 CLID
xform 0 -528 1376
use ecad20 1584 1840 150 0 dataObs
xform 0 1664 960
p 1600 1728 100 0 0 CTYP:9
p 1600 1536 100 0 1 FTVA:STRING
p 1600 1504 100 0 1 FTVB:LONG
p 1600 1472 100 0 1 FTVC:LONG
p 1600 1440 100 0 1 FTVD:LONG
p 1600 1408 100 0 1 FTVE:LONG
p 1600 1376 100 0 1 FTVF:LONG
p 1600 1344 100 0 1 FTVG:LONG
p 1600 1312 100 0 1 FTVH:DOUBLE
p 1600 1280 100 0 1 FTVI:DOUBLE
p 1600 1248 100 0 0 FTVJ:DOUBLE
p 1600 1216 100 0 0 FTVK:LONG
p 1600 1184 100 0 0 FTVL:DOUBLE
p 1600 1152 100 0 0 FTVM:DOUBLE
p 1600 1120 100 0 0 FTVN:DOUBLE
p 1632 848 100 0 0 INAM:dcCADdataInit
p 1600 60 100 0 1 SNAM:dcCADdata
use eapply -368 1408 150 0 dataObsApply
xform 0 -240 1056
use outhier 80 1495 100 0 VAL
xform 0 96 1536
use outhier 80 1415 100 0 MESS
xform 0 96 1456
use eborderC -848 -313 100 0 eborderC#87
xform 0 832 992
p 1728 -160 100 768 -1 author:Peter Young
p 1728 -20 40 0 -1 comment1:2001-02-02 Modified for GNIRS. Peter Young
p 1728 -192 100 768 -1 date:2000-11-02
p 1952 -112 200 768 -1 file:dcDataObs.sch
p 2224 -160 100 0 -1 page:1
p 2336 -160 100 0 -1 pages:1
p 2000 -160 100 0 -1 revision:0
p 1952 -48 150 768 -1 system:Gemini GNIRS DC
[comments]
