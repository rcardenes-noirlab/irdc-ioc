[schematic2]
uniq 7
[tools]
[detail]
w -440 1019 100 0 n#1 esirs.viewEnabled.FLNK -496 1072 -464 1072 -464 1008 -368 1008 embbis.viewEnabledOut.SLNK
w -456 1051 100 0 n#2 esirs.viewEnabled.VAL -496 1040 -368 1040 embbis.viewEnabledOut.INP
w -96 -261 100 0 n#3 ecalcs.obsMode.SLNK -16 -272 -128 -272 -128 192 -208 192 efanouts.efanouts#88.LNK2
w -680 379 100 0 n#4 efanouts.efanouts#88.SLNK -448 144 -528 144 -528 368 -784 368 esirs.irMode.FLNK
w -96 283 100 0 n#5 ecalcs.viewMode.SLNK -16 272 -128 272 -128 224 -208 224 efanouts.efanouts#88.LNK1
w -456 347 100 0 n#6 esirs.irMode.VAL -784 336 -80 336 -80 112 -16 112 ecalcs.obsMode.INPA
w -72 667 100 0 n#6 ecalcs.viewMode.INPA -16 656 -80 656 -80 336 junction
s 1008 -1056 500 512 GNIRS DC Sequence Status/Alarm Database
s -1056 288 100 0 0 = Null mode
s -1056 240 100 0 1 = Viewing
s 368 352 100 0 0 = Disabled
s 368 320 100 0 1 = Enabled
s -1056 192 100 0 2 = Observing
s -528 480 100 0 These calc records exist so that
s -528 448 100 0 the dm display can do a test
s -528 416 100 0 against zero.
[cell use]
use bc200tr -1504 -1208 -100 0 frame
xform 0 176 96
p 1072 -1040 100 0 1 author:P Young
p 1296 -1056 100 0 -1 border:C
p 1072 -1072 100 0 1 checked:
p 1328 -1056 100 0 -1 date:22 Mar 2001
p 1312 -928 100 0 -1 project:Gemini GNIRS DC
p 1312 -992 150 0 -1 title:dcSeqSad.sch
use esirs -592 -809 100 0 version
xform 0 -384 -656
p -496 -832 100 0 1 DESC:Software version number Va.b.c
p -656 -1008 100 0 0 EGU:
p -656 -1072 100 0 0 FDSC:Software version number Va.b.c
p -496 -864 100 0 1 FTVL:STRING
p -496 -928 100 0 1 PV:$(sadtop)
p -496 -896 100 0 1 SNAM:
use esirs 80 823 100 0 irTarget
xform 0 288 976
p 176 800 100 0 1 DESC:Number of rows to readout for exposure
p 16 624 100 0 0 EGU:
p 176 800 100 0 0 FDSC:Number of rows to readout for exposure
p 176 768 100 0 1 FTVL:LONG
p 176 704 100 0 1 PV:$(sadtop)
p 176 736 100 0 1 SNAM:
use esirs 672 359 100 0 irProgress
xform 0 880 512
p 768 336 100 0 1 DESC:Readout rows progress
p 608 160 100 0 0 EGU:
p 608 96 100 0 0 FDSC:Readout rows progress
p 768 304 100 0 1 FTVL:LONG
p 768 240 100 0 1 PV:$(sadtop)
p 768 272 100 0 1 SNAM:
use esirs 1232 -89 100 0 nreads
xform 0 1440 64
p 1328 -112 100 0 1 DESC:Number of reads
p 1168 -288 100 0 0 EGU:
p 1168 -352 100 0 0 FDSC:Number of reads
p 1328 -144 100 0 1 FTVL:LONG
p 1328 -208 100 0 1 PV:$(sadtop)
p 1328 -176 100 0 1 SNAM:
use esirs 1232 -569 100 0 perTimeLeft
xform 0 1440 -416
p 1328 -592 100 0 1 DESC:Time left in period
p 1168 -768 100 0 0 EGU:
p 1168 -832 100 0 0 FDSC:Time left in period
p 1328 -624 100 0 1 FTVL:DOUBLE
p 1328 -688 100 0 1 PV:$(sadtop)
p 1328 -656 100 0 1 SNAM:
use esirs 688 -569 100 0 nfsDone
xform 0 896 -416
p 784 -592 100 0 1 DESC:Number of fowler samples finished
p 624 -768 100 0 0 EGU:
p 624 -832 100 0 0 FDSC:Number of fowler samples finished
p 784 -624 100 0 1 FTVL:LONG
p 784 -688 100 0 1 PV:$(sadtop)
p 784 -656 100 0 1 SNAM:
use esirs 688 839 100 0 elapsed
xform 0 896 992
p 784 816 100 0 1 DESC:Total elapsed time
p 624 576 100 0 0 FDSC:Total elapsed time
p 784 784 100 0 1 FTVL:DOUBLE
p 784 720 100 0 1 PV:$(sadtop)
p 784 752 100 0 1 SNAM:
use esirs 688 -89 100 0 timeLeft
xform 0 896 64
p 784 -112 100 0 1 DESC:Time left in exposure
p 624 -352 100 0 0 FDSC:Time left in exposure
p 784 -144 100 0 1 FTVL:DOUBLE
p 784 -208 100 0 1 PV:$(sadtop)
p 784 -176 100 0 1 SNAM:
use esirs -864 832 100 0 viewEnabled
xform 0 -704 976
p -816 800 100 0 1 DESC:VIEW operations enabled?
p -976 560 100 0 0 FDSC:View operations enabled?
p -816 768 100 0 1 FTVL:LONG
p -816 704 100 0 1 PV:$(sadtop)
p -816 736 100 0 1 SNAM:
use esirs -1200 119 100 0 irMode
xform 0 -992 272
p -1104 96 100 0 1 DESC:Current mode of operation
p -1264 -144 100 0 0 FDSC:Current mode of operation
p -1104 64 100 0 1 FTVL:LONG
p -1104 0 100 0 1 PV:$(sadtop)
p -1104 32 100 0 1 SNAM:
use esirs 1232 839 100 0 ncoaddsDone
xform 0 1440 992
p 1328 816 100 0 1 DESC:Number of coadds finished
p 1168 640 100 0 0 EGU:
p 1168 576 100 0 0 FDSC:Number of coadds finished
p 1328 784 100 0 1 FTVL:LONG
p 1328 720 100 0 1 PV:$(sadtop)
p 1328 752 100 0 1 SNAM:
use esirs 1232 391 100 0 nreadsDone
xform 0 1440 544
p 1328 368 100 0 1 DESC:Number of reads finished
p 1168 192 100 0 0 EGU:
p 1168 128 100 0 0 FDSC:Number of reads finished
p 1328 336 100 0 1 FTVL:LONG
p 1328 272 100 0 1 PV:$(sadtop)
p 1328 304 100 0 1 SNAM:
use esirs -1184 -793 100 0 cadStartMsg
xform 0 -976 -640
p -1088 -816 100 0 1 DESC:General status message
p -1248 -992 100 0 0 EGU:
p -1248 -1056 100 0 0 FDSC:CAD start status message
p -1088 -848 100 0 1 FTVL:STRING
p -1088 -912 100 0 1 PV:$(sadtop)
p -1088 -880 100 0 1 SNAM:
use esirs -1200 -329 100 0 expElapsed
xform 0 -992 -176
p -1104 -352 100 0 1 DESC:Exposure elapsed time
p -1264 -592 100 0 0 FDSC:Exposure elapsed time
p -1104 -384 100 0 1 FTVL:DOUBLE
p -1104 -448 100 0 1 PV:$(sadtop)
p -1104 -416 100 0 1 SNAM:
use embbis -368 935 100 0 viewEnabledOut
xform 0 -240 1008
p -240 798 100 0 0 EIST:
p -304 864 100 0 1 ONST:Disabled
p -304 832 100 0 1 PV:$(sadtop)
p -304 896 100 0 1 ZRST:Enabled
use efanouts -448 7 100 0 efanouts#88
xform 0 -328 160
p -336 0 100 1024 1 name:$(dctop)$(I)
use ecalcs -16 183 100 0 viewMode
xform 0 128 448
p 160 272 100 0 1 CALC:A-1
p 160 240 100 0 1 PV:$(sadtop)
use ecalcs -16 -361 100 0 obsMode
xform 0 128 -96
p 176 -272 100 0 1 CALC:A-2
p 176 -304 100 0 1 PV:$(sadtop)
use dcSeqReadoutSad 352 -825 100 0 dcSeqReadoutSad#75
xform 0 464 -720
p 336 -880 100 0 1 set0:sadtop $(sadtop)obs_
use dcSeqReadoutSad -32 -825 100 0 dcSeqReadoutSad#74
xform 0 80 -720
p -48 -880 100 0 1 set0:sadtop $(sadtop)view_
[comments]
