/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  regions_test.cc
 * 
 * GENERAL DESCRIPTION
 *  Test routine for Regions class
 *   
 * ORIGINAL AUTHOR: 
 *  Kim Sebo - RSAA 1997
 *
 * HISTORY
 *   Mar  1997 Kim    Created for RSAA CICADA package
 *   Sept 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */

#include <iostream>
#include <string>
#include "regions.h"
#include "utility.h"
#include "exception.h"

#ifdef vxWorks
int regionsTest()
#else
int main()
#endif
{
  try {

    Regions regs;
    string reg_file;
    
    // Test algorithm
    // 1. Read region and amp definition files
    // 2. Output amplifier readout specification
 
    cout << "Enter name of regions file:";
    cin >> reg_file;
    
    regs.read_regs(reg_file.c_str(), 2048, 2048);
    cout << "num regions: " << regs.nregions << endl;
    
  }
  catch (Error &e ) {
    e.print_error(__FILE__,__LINE__);
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return(0);
}
