/*
 * Copyright (c) 1994-2002 by MSSSO Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME sdsu_setup.cc
 *
 * GENERAL DESCRIPTION
 * Implements an SDSU preferences class using base Preferences class
 *
 */

/* Include files */
#include <stdio.h>
#include <string>
#include <map>
#include <sys/types.h>
#include <iostream>
#include "utility.h"
#include "exception.h"

#include "sdsu_utils.h"

/* defines */

/* typedefs */
/* class declarations */

/* global variables */

// Support for parameter.asm construction - use hash tables to store key/val pairs
static map<string, string, less<string> > clocks;
static map<string, string, less<string> > biases;
static map<string, string, less<string> > pars;
static map<string, string, less<string> > board_pars;
static map<string, string, less<string> > defs;
static map<string, string, less<string> > board_defs;

/* local function declarations */

/* local function definitions */

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: sdsu_path
 *
 * INVOCATION: sdsu_path(const char* name, const char* chip_name)
*
* PARAMETERS: (">" input, "!" modified, "<" output)
 * > name - basename of filename to create
 * > chip_name - name of CHIP associated
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Constructs a full pathname for SDSU configuration files
 *
 * DESCRIPTION:
 * Rules:
 * 1. If environment variable SDSU_DIR available then it is used
 * 2. else constant SDSU_DIR is relative to CICADA_HOME
 *
 * EXTERNAL VARIABLES: None.
 *
 * PRIOR REQUIREMENTS: None.
 *
 * DEFICIENCIES:
 *
 *-------------------------------------------------------------------------*/
std::string sdsu_path(const char* name, const char* chip_name)
{
  Filenametype path_buf;
  char *cp;
  // Use global buffer for storing generated path.

  if ((cp = getenv("SDSU_DIR")) == NULL) {
    strlcpy(path_buf, SDSU_DIR, FILENAMELEN);
  } else {
    strlcpy(path_buf, cp, FILENAMELEN);
  }
  if (path_buf[strlen(path_buf)-1] != '/')
    strlcat(path_buf, "/", FILENAMELEN);

  // name might be specific to controller setup
  if (strlen(chip_name) > 0) {
    strlcat(path_buf, chip_name, FILENAMELEN);
    strlcat(path_buf, "/", FILENAMELEN);
  }
  strlcat(path_buf, name, FILENAMELEN);
  return std::string(path_buf);
}

/*
 *+
 * FUNCTION NAME: clear_sdsu_hash_tables
 *
 * INVOCATION: clear_sdsu_hash_tables()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Clear DSP hash tables
 *
 * DESCRIPTION: Call relevant clear method for each of the DSP hash tables
 * in turn.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

void clear_sdsu_hash_tables()
{
  clocks.clear();
  biases.clear();
  pars.clear();
  board_pars.clear();
  defs.clear();
  board_defs.clear();
}

/*
 *+
 * FUNCTION NAME: calc_adu_val
 *
 * INVOCATION: calc_adu_val(Volts_Desc, Sdsu_Voltage_Index )
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > Volts_Desc  - a volts description struct from which the adu value
 * will be calculated
 * > Sdsu_Voltage_Index - whether to do the calc for the low or high voltage
 * > v - voltage to be converted to ADUs
 *
 * FUNCTION VALUE: int (adu value)
 *
 * PURPOSE: to calc an adu value from a voltage
 *
 * DESCRIPTION: Using a formula supplied by Mark Downing, calculate
 * an ADU value from a voltage value and calibration data.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

int calc_adu_val(Volts_Desc vd, Sdsu_Voltage_Index i, double v)
{

  // This formula worked out my Mark Jarnyk
  return (int) ((v - vd.cal2_volt[i]) * (vd.cal1_adu[i] - vd.cal2_adu[i]) / (vd.cal1_volt[i] - vd.cal2_volt[i]) + vd.cal2_adu[i]);

  // This formula from Mark Downing, but does not cope correctly
  // with ADCs where the voltage increases with decreasing ADUs
  //return (int) ((v - vd.cal1_volt[i])/(vd.cal2_volt[i] - vd.cal1_volt[i]) * (vd.cal2_adu[i] - vd.cal1_adu[i]));

}
/*
 *+
 * FUNCTION NAME: calc_volt_val
 *
 * INVOCATION: calc_volt_val(Volts_Desc, Sdsu_Voltage_Index, adu_val)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > Volts_Desc  - a volts description struct from which the adu value
 * will be calculated
 * > Sdsu_Voltage_Index - whether to do the calc for the low or high voltage
 * > adu_val - ADU value to be converted to a voltage
 *
 * FUNCTION VALUE: double (voltage value)
 *
 * PURPOSE: to calc an adu value from a voltage
 *
 * DESCRIPTION: Using a formula supplied by Mark Downing, calculate
 * a voltage value from an ADU value and calibration data.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

double calc_volt_val(Volts_Desc vd, Sdsu_Voltage_Index i, int adu_val)
{

  // This formula worked out my Mark Jarnyk
  return (double) (vd.cal2_volt[i] + (adu_val - vd.cal2_adu[i]) * (vd.cal1_volt[i] - vd.cal2_volt[i])/(vd.cal1_adu[i] - vd.cal2_adu[i]));

  // This formula from Mark Downing, but does not cope correctly
  // with ADCs where the voltage increases with decreasing ADUs
  //  return (double) (adu_val * ((vd.cal2_volt[i] - vd.cal1_volt[i]) / (vd.cal2_adu[i] - vd.cal1_adu[i])) + vd.cal1_volt[i]);

}
