/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section
 * CICADA PROJECT
 *
 * FILENAME
 *  ipc.cc
 *
 * GENERAL DESCRIPTION
 *  Library of IPC and process handling routines.
 *
 * ORIGINAL AUTHOR:
 *  Peter Young - RSAA 1993
 *
 * HISTORY
 *   Sept 1993 PJY    Created for RSAA CICADA package
 *   Sept 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */

/* Include files */
/*
#include <pthread.h>
#include <sys/mman.h>
*/
#include <string>
#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <errno.h>
#include "ipc.h"
#include <unordered_map>
#include <epicsThread.h>

using std::shared_ptr;

// 10ms
constexpr auto SEM_SLEEP_QUANTUM = 0.01;

/* defines */
/* typedefs */

/* class declarations */

class mutex_guard {
public:
	mutex_guard(epicsMutex& mutex, std::string name) : _name(name), _mutex(mutex)
	{
		_mutex.lock();
	}
	~mutex_guard() {
		_mutex.unlock();
	}
private:
	std::string _name;
	epicsMutex& _mutex;
};

/* global variables */


/* local function declarations */


//////////////////////////////////////////////////////////
//
// Semaphore class methods
//
//////////////////////////////////////////////////////////

namespace {
	std::map<std::string, shared_ptr<Semaphore>> sem_store;
	std::map<std::string, shared_ptr<Mailbox>> mbox_store;
}
/*
 *+
 * FUNCTION NAME: Semaphore::create
 *
 * Static method that creates new semaphores.
 */

shared_ptr<Semaphore>
Semaphore::create(const std::string& name, bool must_wait)
{
	// TODO: Raise an exception if the semaphore exists...
	sem_store.emplace(name, make_shared<Semaphore>(name, must_wait));

	return sem_store.at(name);
}

/*
 *+
 * FUNCTION NAME: Semaphore::get
 *
 * Static method that returns an existing semaphore
 */
shared_ptr<Semaphore>
Semaphore::get(const std::string& name)
{
	return sem_store.at(name);
}

/*
 *+
 * FUNCTION NAME: Semaphore::Semaphore
 *
 * INVOCATION: new Semaphore()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Contructor
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Semaphore::Semaphore() : Semaphore("")
{
	waiting = false;
}

Semaphore::Semaphore(const std::string& name, bool initially_taken)
	: waiting(false),
	  _event(initially_taken ? epicsEventEmpty : epicsEventFull ),
	  _signaled(!initially_taken),
	  _name(name)
{
}
/*
 *+
 * FUNCTION NAME: Semaphore::wait
 *
 * INVOCATION: wait()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Wait for semaphore semnum to reach val where val is either 0 or 1
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Semaphore::wait()
{
	{
		mutex_guard(_mutex, "wait");

		if (_signaled) {
			_signaled = false;
			return;
		}

		waiting = true;
	}

	// NOTE: There's risk here that some other thread will
	//       signal the semaphore while we're still not waiting,
	//	 but that's something we'll have to figure out at some point.
	_event.wait();
	waiting = false;
}
/*
 *+
 * FUNCTION NAME: Semaphore::timed_wait
 *  and sem_timer_thread
 *
 * INVOCATION: timed_wait(int timeout_msec)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > timout_msec - number of msecs to wait before timing out
 *
 * FUNCTION VALUE: int (returns 1 on success, 0 on failure)
 *
 * PURPOSE: Wait for semaphore semnum to reach val where val is either 0 or 1
   timeout after msecs
 *
 * DESCRIPTION: Uses IPC_Alarm and associated handler
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *   Has an overhead in the order of 10ms.
 *-
 */

int Semaphore::timed_wait(int timeout_msec)
{
	double timeout = double(timeout_msec) / 1000;

	{
		mutex_guard(_mutex, "timed_wait");

		if (_signaled) {
			_signaled = false;
			return 1;
		}

		waiting = 1;
	}

	int result;
	// NOTE: There's risk here that some other thread will
	//       signal the semaphore while we're still not waiting,
	//	 but that's something we'll have to figure out at some point.
	result = int(_event.wait(timeout));

	waiting = false;
	return result;
}


void Semaphore::signal()
{
	mutex_guard(_mutex, "signal");

	_signaled = true;
	_event.signal();
}

/*
 *+
 * FUNCTION NAME: Semaphore::try_wait
 *
 * INVOCATION: try_wait(int timeout_msec)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: int
 *
 * PURPOSE: trywait - try taking a semaphore
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
bool Semaphore::try_wait(int timeout_msec)
{
	if (timeout_msec)
		return timed_wait(timeout_msec);

	mutex_guard(_mutex, "wait");

	if (_signaled) {
		_signaled = false;
		return true;
	}
	return false;
}

// TODO: Doc
const std::string&
Semaphore::name() const
{
	return _name;
}

/* class function definitions */
/*
 *+
 * FUNCTION NAME: Mailbox::create
 *
 * Static method that creates new mailboxes.
 */

shared_ptr<Mailbox>
Mailbox::create(const std::string& name, int maxmsg, int maxmsg_size)
{
	// TODO: Raise an exception if the mailbox exists...
	mbox_store.emplace(name, shared_ptr<Mailbox>(new Mailbox(name, maxmsg, maxmsg_size)));

	return mbox_store.at(name);
}

/*
 *+
 * FUNCTION NAME: Mailbox::get
 *
 * Static method that returns an existing mailbox
 */
shared_ptr<Mailbox>
Mailbox::get(const std::string& name)
{
	return mbox_store.at(name);
}
//////////////////////////////////////////////////////////
//
// Mailbox (message queue) class methods
//
//////////////////////////////////////////////////////////
/*
 *+
 * FUNCTION NAME: Mailbox::Mailbox
 *
 * INVOCATION: new Mailbox()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Simple mailbox constructor
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Mailbox::Mailbox()
	: emq(new epicsMessageQueue(IPC_MAXMSG, IPC_MAXMSGSIZE)),
	  msgs_received(0),
	  msgs_sent(0),
	  max_msg_size(IPC_MAXMSGSIZE)
{
}

/*
 *+
 * FUNCTION NAME: Mailbox::Mailbox
 *
 * INVOCATION: new Mailbox(const std::string mqn,  int own,  int mode)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > mqn - message queue name
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Mailbox::Mailbox(const std::string mqn, int maxmsg, int maxmsg_size)
	: emq(new epicsMessageQueue(maxmsg, maxmsg_size)),
	  msgs_received(0),
	  msgs_sent(0),
	  max_msg_size(maxmsg_size),
	  mqname(mqn)
{
}

/*
 *+
 * FUNCTION NAME: void Mailbox::send_message
 *
 * INVOCATION: send_message(const char* msg,  int msgsz)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > msg - pointer to buffer containing message
 * > msgsz - length of message in bytes
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Post a message to a message queue
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static const auto MAILBOX_ETOOLARGE = "The message was larger than the maximum size";
static const auto MAILBOX_EFULLQUEUE = "Could not queue the message";
void Mailbox::send_message(const char* msg, int msgsz, int msgflg)
     /* msgsz: size of message */
{
	if (msgflg == IPC_WAIT) {
		if (emq->send((void *)msg, msgsz) == -1)
			throw IPC_Error(MAILBOX_ETOOLARGE, E_ERROR,errno,__FILE__,__LINE__);
	} else {
		if (emq->trySend((void *)msg, msgsz) == -1) {
			throw IPC_Error(msgsz > max_msg_size ? MAILBOX_ETOOLARGE : MAILBOX_EFULLQUEUE,
					E_ERROR,errno,__FILE__,__LINE__);
		}
	}
}

/*
 *+
 * FUNCTION NAME: int Mailbox::get_message
 *
 * INVOCATION: get_message(char* msg,  int msgsz,  int msgflg)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * < msg - pointer to buffer containing message
 * > msgsz - length of message in bytes
 * > msgflg -  message flag, action modifier
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Wait for a message on a message queue
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Mailbox::get_message(char* msg,  int msgsz,  int msgflg)
{
	int rtrn;
	char *mbuf = (char *)malloc(max_msg_size);

try {
	if (msgflg == IPC_WAIT) {
		// Receive without a timeout
	  	try {
			rtrn = emq->receive((void*)mbuf, max_msg_size);
		} catch(...) {
			throw;
		}
		if (rtrn < 0) {
			throw IPC_Error("Received a message larger than the buffer",
					E_ERROR,errno,__FILE__,__LINE__);
		}
	} else {
		rtrn = emq->receive((void*)mbuf, max_msg_size, double(msgflg));
		if (rtrn < 0) {
			throw IPC_Error("No message or message larger than the buffer",
					E_ERROR,errno,__FILE__,__LINE__);
		}
	}

	/*
	 * Two cases to consider:
	 *  - rtrn > 0 : got a message
	 *  - rtrn == 0: possibly interrupted by a signal, just return
	 *               0 bytes
	 */

	if (rtrn > 0) {
		memcpy(msg, mbuf, MIN(rtrn, msgsz));
		msgs_received++;
	}
}
catch (...) {
	free(mbuf);

	throw;
}
	free(mbuf);

	return rtrn;
}
/*
 *+
 * FUNCTION NAME: unsigned long Mailbox::messages_waiting
 *
 * INVOCATION: messages_waiting()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Return number of messages waiting on queue
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
unsigned long Mailbox::messages_waiting()
{
	return emq->pending();
}


//////////////////////////////////////////////////////////
//
// Thread class methods
//
//////////////////////////////////////////////////////////
/*
 *+
 * FUNCTION NAME: Thread::Thread
 *
 * INVOCATION: new Thread()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Constructor
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

Thread::Thread()
	: locked(false),
	  locking(false),
	  waiting(false),
	  tid(epicsInvalidThread),
	  predicate(false),
	  except(nullptr),
	  priority(THREAD_DEFAULT_PRIO),
	  stackSize(THREAD_DEFAULT_STACK)
{
}

//////////////////////////////////////////////////////////
//
// Thread class methods
//
//////////////////////////////////////////////////////////
/*
 *+
 * FUNCTION NAME: Thread::Thread
 *
 * INVOCATION: thr = new Thread(const char* tname, void *(*start_routine, void*), bool dolock,
 *                              int priority, int stackSize)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > tname - new thread name
 * > start_routine - name of function to run in thread
 * > dolock - set if required to lock the mutex after creation
 * > priority - priority to run task
 * > stackSize - size of task stack
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Contructor
 *
 * DESCRIPTION: Initializes structures for a thread
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

Thread::Thread(const char* tname, unsigned int priority, int stackSize, bool dolock)
	: Thread()
{
	this->priority = priority;
	this->stackSize = stackSize;
	name = tname;

	if (dolock)
		lock();
}

Thread::~Thread() {
	//+ TODO: Not yet implemented...
}

void thread_callback(void *t) {
	static_cast<Thread *>(t)->run_and_wait();
}

void
Thread::start() {
	auto callback = [](void *this_ptr) { static_cast<Thread *>(this_ptr)->run_and_wait(); };
	auto thunk = [](void *fn) {
		(*static_cast<decltype(callback)*>(fn))(nullptr);
	};

	tid = epicsThreadCreate(name.c_str(), priority, stackSize, thread_callback, this);
	if (!tid) {
		throw std::runtime_error(string("Could not create the thread: ") + name);
	}
}

void Thread::run_and_wait() {
	// Ensures that we'll signal completion
	auto termination_event_guard{event_guard<epicsEvent>(&finished, name)};

	run();
}

void Thread::join() {
	finished.wait();
}

bool Thread::done() {
	return finished.tryWait();
}

/*
 *+
 * FUNCTION NAME: Thread::wait
 *
 * INVOCATION: thr->wait()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Wait for thread condition variable to be signalled
 *
 * DESCRIPTION: Calls POSIX pthread_cond_wait unlocking mutex
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

void
Thread::wait() {
	waiting = true;
	// Use a boolean predicate to handle spurious wakeups from wait()
	// signal() will set predicate to true so we can detect a real wakeup
	predicate = false;

	mu.unlock();
	while (!predicate) {
		cv.wait();
	}
	mu.lock();

	waiting = false;
}

/*
 *+
 * FUNCTION NAME: Thread::timed_wait
 *
 * INVOCATION: thr->timed_wait(int timeout_msec)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > timeout_msec - timeout period in msec
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Wait for condition variable to be signalled within timeout, returns 0 if timeout
 *
 * DESCRIPTION: Calls POSIX pthread_cond_timedwait unlocking mutex
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

int
Thread::timed_wait(int timeout_seconds) {
	int ret = 0;
	waiting = true;

	mu.unlock();
	if (!cv.wait(double(timeout_seconds) / 1000))
		ret = 1;
	mu.lock();

	waiting = false;
	return ret;
}

/*
 *+
 * FUNCTION NAME: Thread::signal
 *
 * INVOCATION: thr->signal()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: signal thread condition variable
 *
 * DESCRIPTION: Signals the internal event and sets predicate = true
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

void Thread::signal()
{
	predicate = true;
	cv.signal();
}


/*
 *+
 * FUNCTION NAME: Thread::lock
 *
 * INVOCATION: thr->lock()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Locks threads mutex
 *
 * DESCRIPTION: Calls POSIX pthread_mutex_lock to lock mutex
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

void Thread::lock()
{
	locking = true;
	mu.lock();
	locked = true;
	locking = false;
}

/*
 *+
 * FUNCTION NAME: Thread::unlock
 *
 * INVOCATION: thr->unlock()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Unlocks threads mutex
 *
 * DESCRIPTION: Calls POSIX pthread_mutex_unlock to lock mutex
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

void
Thread::unlock() {
	mu.unlock();
	locked = false;
}
