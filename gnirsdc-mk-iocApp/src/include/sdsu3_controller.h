/*
 * Copyright (c) 1994 - 2002 by MSSSO Computing Section 
 *
 * CICADA PROJECT
 *
 * FILENAME sdsu3_controller.h
 * 
 * PURPOSE
 * Header file defining the sdsu generation 3 controller class.
 * This is nearly equivalent to the generic SDSU class so is 
 * derived from that
 *
 */
#ifndef _SDSU3_CONTROLLER_H
#define _SDSU3_CONTROLLER_H

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#include "sdsu2_controller.h"

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/
//+ DSP parameter numbers
//+ Y memory options word
const int SDSU3_TEST_PATTERN_BIT = 0x2;
const int SDSU3_TEST_PATTERN_CONST_BIT = 0x4;
//const int SDSU_TEST_PATTERN_SOURCE_BIT = 0x8;
const int SDSU3_AUTOSHUTTER_BIT = 0x20;
const int SDSU3_AUTOREAD_BIT = 0x40;
const int SDSU3_AUTOCLEAR_BIT = 0x10;
const int SDSU3_PON_BIT = 0x400000;

/*--------------------------------------------------------------------------
 Class declarations 
 -------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
CLASS
    Sdsu3_Controller

DESCRIPTION  
    SDSU Generation 2 hardware class definition.
 
KEYWORDS
    Sdsu3_Controller, Sdsu_Controller

-------------------------------------------------------------------------*/
class Sdsu3_Controller: public Sdsu2_Controller {

public:
  //+ Basic constructor - used to set up configuration
  // > cam - Camera_Desc - description of the camera
  // > camid - controller id
  // > c - sleep time msecs
  Sdsu3_Controller(Camera_Desc& cam, int camid, int c, Platform_Specific* ps);

  //+ Constructor for parameters?
  // > cam        - Camera_Desc - description of the camera
  // > camid      - camera id
  // > c          - controller id
  // > param_host - host on which the parameter database resides
  // > st	  - status memory for this instrument
  // > pm         - parameter map
  Sdsu3_Controller(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st, Platform_Specific* ps);

  //+ This constructor is used on instrument machine
  // > st   - status memory for this instrument
  // > ip   - instrument status parameter object
  // > deb  - debug level
  Sdsu3_Controller(Instrument_Status *st, Parameter* ip, Parameter* cp, Parameter* op,
		  int deb, Platform_Specific* ps);

  //+ Destructor
  virtual ~Sdsu3_Controller();

  // GROUP: Private member functions

  //+ Send a command to the SDSU DSP to get the DSP code version currently loaded
  // > board_id - board to send command to
  virtual char* get_dsp_version(int board_id);

  //+ set board ids for various control functions
  virtual void do_set_boards();

  //+ Turns on controller power - for SDSU3 uses timing board
  virtual void do_power_on();

  //+ Do a controller shutdown
  virtual void controller_shutdown();

  //+ Returns address of DSP regions setup memory.
  virtual int get_regions_memory_address();

  //+ Read back current voltage settings
  virtual void read_current_voltage_settings();

  //+ Get controller information
  virtual void get_controller_info();

  //+ Sets the exposure time 
  // > msec - exposure time in msec to set 
  virtual void set_exposure_time(int msec);

  //+ Gets the exposure time counter value - msec
  virtual int get_exposure_time();

  //+ Sets controller's test pattern mode.  Generation 3.
  virtual void do_test_pattern();

protected:

  // GROUP: Protected member functions

};

#endif //_SDSU3_CONTROLLER_H
