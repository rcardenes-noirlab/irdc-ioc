/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_cad.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI NIFS PROJECT
 * 
 * FILENAME nifs_cad.cc
 * 
 * GENERAL DESCRIPTION
 *
 * Loads and verifies CAD input parameters using the Parameter Database
 *
 *
 * $Log: dc_cad.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.15  2005/09/23 01:33:24  pjy
 * Fix for boolean parameters - need to switch for reverse polarity EPICS
 *
 * Revision 1.14  2005/08/18 03:22:44  pjy
 * Shortened msg
 *
 * Revision 1.13  2005/08/02 05:28:48  pjy
 * Clear cad start message take 2
 *
 * Revision 1.12  2005/08/02 05:07:07  pjy
 * Clear cad start message
 *
 * Revision 1.11  2005/08/02 04:47:06  pjy
 * New cad start message
 *
 * Revision 1.10  2005/07/30 01:44:28  pjy
 * Fixed message len
 *
 * Revision 1.9  2005/07/30 01:14:26  pjy
 * Fixed typo
 *
 * Revision 1.8  2005/07/29 23:46:37  pjy
 * Use set_and_verify to check to see if observc is busy
 *
 * Revision 1.7  2005/07/18 00:36:01  pjy
 * Check dc_status db too
 * Correct type casting
 *
 * Revision 1.6  2005/07/12 06:46:15  pjy
 * Added status put call for boolean,long,float & double params
 *
 * Revision 1.5  2004/12/22 06:09:52  pjy
 * DC cad src files ready for first Gemini release
 *
 * Revision 1.4  2004/08/20 02:27:04  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.3  2004/04/27 07:25:52  pjy
 * T2.2
 *
 * Revision 1.1  2004/03/23 01:59:46  pjy
 * Gem 8.6 port
 *
 * 
 */
/* VxWorks include files */
#include <cstdio>
#include <cstring>
#include <cmath>

#ifdef __cplusplus
extern "C" {
#endif
#include "genSubRecord.h"
#include "cadRecord.h"
#include "cad.h"
#include "dbAccess.h"
#include "cicsConst.h"
#include "dbCommon.h"
#ifdef __cplusplus
}
#endif

#include "parameter.h"
#include "configuration.h"
#include "ir_camera.h"
#include "ir_setup.h"
#include "table.h"
#include "utility.h"
#include "dc.h"
#include "logging.h"
#include "dc_epics_db.h"

#define GENERATE_CADNAME(top, name) (STRINGIFY(top) name)

// Globals
//+ The camera setup
extern IR_Setup* ir_setup;                        //+ Used for retrieving and storing setup information from disk

//+ The camera
extern IR_Camera *ir_camera;                      //+ The infra red camera object - created by gnirsSlave task

// The general status DB
extern Parameter *dc_status;                      //+ Internal DC status parameters
extern Parameter *dc_par;                         //+ Internal DC status parameters

/* Function declarations */
#ifdef __cplusplus
extern "C" void set_and_verify_output_cad(struct cadRecord *pCad);
extern "C" void load_input_cad(struct cadRecord *pCad);
#endif

/*
 *+
 * FUNCTION NAME: copy_outval
 *
 * INVOCATION: copy_outval(const char*|long|double input_val, char*|long|double output_val)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > input_val - a CAD input of appropriate Gemini data type (char*,long,double)
 * < output_val - a CAD output value
 *
 * FUNCTION VALUE:
 * None
 *
 * PURPOSE:
 * Simply copies the input to the output value - no checking
 *
 * DESCRIPTION:
 * Assumes input value has been validated before this call. Simple convenicens routine for 
 * doing a copy of the appropriate datatype
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */
static void copy_outval(const char* input_val, char* output_val)
{
  strcpy(output_val, input_val);
}
static void copy_outval(const long input_val, long* output_val)
{
  *output_val = input_val;
}
static void copy_outval(const double input_val, double* output_val)
{
  *output_val = input_val;
}
/*
 *+
 * FUNCTION NAME: set_and_verify_output_cad
 *
 * INVOCATION: set_and_verify_output_cad( struct cadRecord *pCad);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * None
 *
 * PURPOSE:
 * General purpose routine to validate input CAD values.
 * 
 * DESCRIPTION:
 * Called from each CAD snam routine when given a PRESET directive.
 *
 * Generally RSAA Gemini DC CADS have a direct mapping between input CAD
 * parameters and output links to SIR records. These SIR records are named
 * identically to the internal Parameter DB parameter values - which have
 * validity checking performed on them. Using this setup it is easy to
 * generalise a process that reads each input parameter and checks the linked
 * SIR in the Parameter DB to see if the input is valid.
 *
 * Specifically, the process goes through each input CAD parameter and if there
 * is a link to a SIR record on the corresponding CAD output, uses a mapping
 * from SIR record name to parameter database entry to validate the input
 * value. If valid the input value is copied to the CAD output.
 *
 * If there is no corresponding linked SIR nor an entry in the Parameter DB,
 * then this process cannot perform any validity checking - it simply copies the
 * input to the CAD output value.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * 
 *
 * DEFICIENCIES:
 * None
 *
 *- 
 */
void set_and_verify_output_cad(struct cadRecord *pCad)
{
  char* cad_argp = &pCad->a[0];
  void** cad_valp = &pCad->vala;
  struct link* cad_outp = &pCad->outa;
  unsigned short* cad_datatp = &pCad->ftva;
  char arg = 'A';
  int i=0;
  char msgbuf[MSGLEN];
  DType dtype = PARAM_STRING;
  Parameter *ir_statusp = NULL; 
  Parameter *paramdb = NULL;
  bool skip_verify, flag;
  string observeC;
  string cadname;
  string msg;
 
  logger::message(logger::Level::Full, "set_and_verify_output_cad: CAD name=%s, Nargs=%d", pCad->name, pCad->ctyp);

  if (ir_camera == NULL) {
    msg = "Cannot setup CAD - no camera object!";
    dc_status->put(CAD_START_MSG, msg);
    throw Error(msg.c_str(), E_ERROR, EINVAL, __FILE__, __LINE__);
  }

  ir_statusp = ir_camera->ir_statusp;

  /* Make sure we are not observing for most CADs - reject if so */
  dc_par->get(OBSERVEC, observeC);
  cadname = pCad->name;
  if ((observeC == "BUSY") && 
      ((cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:testPat")) || 
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:DSPDirectCmd")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:testSDSU")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:shutdownSDSU")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:resetSDSU")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:debugSDSU")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:cancelSDSU")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:initSDSU")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:seqObs")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:setDHSInfo")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:dataObs")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:datum")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:verify")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:endVerify")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:guide")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:endGuide")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:endObserve")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:pause")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:continue")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:observe")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:simulate")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:debug")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:park")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:test")) ||
       (cadname == GENERATE_CADNAME(GNIRS_TOP, "dc:init")))) {
    msg = "Cannot process CAD - OBSERVE running!";
    dc_status->put(CAD_START_MSG, msg);
    throw Error(msg.c_str(), E_ERROR, EINVAL, __FILE__, __LINE__);
  }

  // Clear the CAD_START_MSG
  msg = "";
  dc_status->put(CAD_START_MSG, msg);	

  // Load parameter DB with CAD parameters - use the linked SIR record to provide mapping to
  // parameter DB name. Loading the parameter DB also verifies and writes values through to SAD.
  // If verified ok then copy the value to the output.
  for (i=0; i<pCad->ctyp; i++) {
    logger::message(logger::Level::Full, "set_and_verify_output_cad: Arg%c='%s',Out link type=%d,datatype=%s", arg, cad_argp, cad_outp->type,
		  pamapdbfType[*cad_datatp].strvalue);

    // Construct the Parameter DB name from the SIR record name
    skip_verify = true;
    if (cad_outp->type == DB_LINK) {
      struct dbAddr* dbp = dbGetPdbAddrFromLink(cad_outp);  //(struct dbAddr*) cad_outp->value.db_link.pdbAddr;
      logger::message(logger::Level::Full, "set_and_verify_output_cad: linked record='%s'", dbp->precord->name);

      // All these records will have the string DB_PREFIX as first part
      // strip away to reveal parameter name
      string sir_name = dbp->precord->name;
      string prefix = DB_PREFIX;
      string suffix = DB_SUFFIX;
      int pos;

      // Ensure we get the record name of the out link only
      
      // See if there is a parameter DB entry that we can try to verify - catch any exception here
      try {
	      if ((pos = sir_name.find(prefix)) != -1) {
		      sir_name = sir_name.erase(0,pos+prefix.length());
		      logger::message(logger::Level::Full,"Verifying CAD input %c (%s) with value %s", arg, sir_name.c_str(), cad_argp);
	      } else {
		      sprintf(msgbuf,"Unexpected SIR name %s", dbp->precord->name);
		      throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	      }

	      paramdb = NULL;
	      try {
		      // Get the data type used in the ir status parameter DB 
		      dtype = ir_statusp->dType(sir_name.c_str());
		      paramdb = ir_statusp;
		      skip_verify = false;
	      } catch (Error& err) {
		      // Just drop through if not found in ir_statusp
	      }

	      // Now try the dc_status DB if not yet found
	      if ((paramdb == NULL) && (dc_status != NULL)) {
		      try {
			      // Try getting the data type used in the dc_status parameter DB 
			      dtype = dc_status->dType(sir_name.c_str());
			      paramdb = dc_status;
			      skip_verify = false;
		      } catch (Error& err) {
			      // Drop through to skip verification
		      }
	      }
      }
      catch (Error& err) {
	      // Drop through to skip verification
      }

      if (!skip_verify) {
	logger::message(logger::Level::Full,"Translated SIR record %s (datatype %s) to %s (datatype %d)", dbp->precord->name,
		      pamapdbfType[*cad_datatp].strvalue, sir_name.c_str(), paramdb->dType(sir_name.c_str()));
	
	// Try to put the value into the DB - this will ensure validation done
	switch (dtype) {
	case PARAM_BOOL: 
	  // make sure the EPICS database has a compatible data type. bools == long
	  if (*cad_datatp != DBR_LONG) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'bool' to be EPICS %s (not %s)", arg, 
		    pamapdbfType[DBR_LONG].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  
	  flag = (atol(cad_argp) == EPICS_TRUE);
	  paramdb->check(sir_name, flag);
	  copy_outval((long)(bool)atol(cad_argp), (long*)*cad_valp);
	  paramdb->put(sir_name, flag); 
	  break;
	case PARAM_USHORT:
	  // make sure the EPICS database has a compatible data type. ints == long
	  if (*cad_datatp != DBR_LONG) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'unsigned short' to be EPICS %s (not %s)", arg, 
		    pamapdbfType[DBR_LONG].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  paramdb->check(sir_name, (unsigned short)atoi(cad_argp)); 
	  copy_outval((long)(unsigned short)atoi(cad_argp), (long*)*cad_valp);
	  paramdb->put(sir_name, (unsigned short)atoi(cad_argp)); 
	  break;
	case PARAM_SHORT:
	  // make sure the EPICS database has a compatible data type. ints == long
	  if (*cad_datatp != DBR_LONG) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'short' to be EPICS %s (not %s)", arg, 
		    pamapdbfType[DBR_LONG].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  paramdb->check(sir_name, (short)atoi(cad_argp)); 
	  copy_outval((long)(short)atoi(cad_argp), (long*)*cad_valp);
	  paramdb->put(sir_name, (short)atoi(cad_argp)); 
	  break;
	case PARAM_UINT:
	  // make sure the EPICS database has a compatible data type. ints == long
	  if (*cad_datatp != DBR_LONG) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'unsigned int' to be EPICS %s (not %s)", arg, 
		    pamapdbfType[DBR_LONG].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  paramdb->check(sir_name, (unsigned int)atol(cad_argp)); 
	  copy_outval((long)(unsigned int)atol(cad_argp), (long*)*cad_valp);
	  paramdb->put(sir_name, (unsigned int)atol(cad_argp)); 
	  break;
	case PARAM_INT:
	  // make sure the EPICS database has a compatible data type. ints == long
	  if (*cad_datatp != DBR_LONG) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'int' to be EPICS %s (not %s)", arg, 
		    pamapdbfType[DBR_LONG].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  paramdb->check(sir_name, atoi(cad_argp)); 
	  copy_outval((long)atoi(cad_argp), (long*)*cad_valp);
	  paramdb->put(sir_name, atoi(cad_argp)); 
	  break;
	case PARAM_ULONG:
	  // make sure the EPICS database has a compatible data type. 
	  if (*cad_datatp != DBR_LONG) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'unsigned long' to be EPICS %s (not %s)", arg, 
		    pamapdbfType[DBR_LONG].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  paramdb->check(sir_name, (unsigned long)atol(cad_argp));
	  copy_outval(atol(cad_argp), (long*)*cad_valp);
	  paramdb->put(sir_name, (unsigned long)atol(cad_argp)); 
	  break;
	case PARAM_LONG:
	  // make sure the EPICS database has a compatible data type. 
	  if (*cad_datatp != DBR_LONG) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'long' to be EPICS %s (not %s)", arg, 
		    pamapdbfType[DBR_LONG].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  paramdb->check(sir_name, atol(cad_argp));
	  copy_outval(atol(cad_argp), (long*)*cad_valp);
	  paramdb->put(sir_name, atol(cad_argp)); 
	  break;
	case PARAM_FLOAT:
	  // make sure the EPICS database has a compatible data type. 
	  if (*cad_datatp != DBR_DOUBLE) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'float' to be EPICS %s (not %s)", arg, 
		    pamapdbfType[DBR_DOUBLE].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  paramdb->check(sir_name, (float)atof(cad_argp)); 
	  copy_outval(atof(cad_argp), (double*)*cad_valp);
	  paramdb->put(sir_name, (float)atof(cad_argp)); 
	  break;
	case PARAM_DOUBLE: {
	  // make sure the EPICS database has a compatible data type. 
	  if (*cad_datatp != DBR_DOUBLE) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'double' to be EPICS %s (not %s)", arg, 
		    pamapdbfType[DBR_DOUBLE].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  paramdb->check(sir_name, atof(cad_argp)); 
	  copy_outval(atof(cad_argp), (double*)*cad_valp);
	  paramdb->put(sir_name, atof(cad_argp)); 
	  break;
	}
	case PARAM_CHAR:
	  // make sure the EPICS database has a compatible data type. bools == long
	  if (*cad_datatp != DBR_STRING) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'char' array to be EPICS %s (not %s)", 
		    arg, pamapdbfType[DBR_STRING].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  paramdb->check(sir_name, cad_argp, 0, strlen(cad_argp)); 
	  copy_outval(cad_argp, (char*)*cad_valp);
	  paramdb->put(sir_name, cad_argp, 0, strlen(cad_argp)); 
	  break;
	case PARAM_STRING: {
	  // make sure the EPICS database has a compatible data type. bools == long
	  if (*cad_datatp != DBR_STRING) {
	    sprintf(msgbuf,"Data type mismatch (arg %c), expect 'string' to be EPICS %s (not %s)", arg, 
		    pamapdbfType[DBR_STRING].strvalue, pamapdbfType[*cad_datatp].strvalue);
	    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  }
	  string par = cad_argp;
	  paramdb->check(sir_name, par); 
	  copy_outval(cad_argp, (char*)*cad_valp);
	  paramdb->put(sir_name, par); 
	  break;
	}
	default:
	  // make sure the EPICS database has a compatible data type. 
	  sprintf(msgbuf,"Data type mismatch (arg %c), unsupported %d, when EPICS data type is %s)", 
		  arg, paramdb->dType(sir_name.c_str()), pamapdbfType[*cad_datatp].strvalue);
	  throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	  break;
	}
      }	  
    } else {
      // Cannot determine if there is a SIR record to update - so do not try to verify the input
      skip_verify = true;
    }

    // If we couldn't use the Parameter DB to verify the input then we just copy directly to output
    if (skip_verify) {
      switch (*cad_datatp) {
      case DBR_LONG:
	copy_outval(atol(cad_argp), (long*)*cad_valp);
	break;
      case DBR_DOUBLE:
	copy_outval(atof(cad_argp), (double*)*cad_valp);
	break;
      case DBR_STRING:
	copy_outval(cad_argp, (char*)*cad_valp);
	break;
      default:
	// make sure the EPICS database has a compatible data type. 
	sprintf(msgbuf,"Data type mismatch (arg %c), unsupported EPICS data type %s)", 
		arg, pamapdbfType[*cad_datatp].strvalue);
	throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	break;
      }
    }

    // Increment the cad field pointers to next argument
    arg++;
    cad_argp += MAX_STRING_SIZE;
    cad_valp++;
    cad_outp++;
    cad_datatp++;
  }

}
/*
 *+
 * FUNCTION NAME: load_input_cad
 *
 * INVOCATION: load_input_cad( struct cadRecord *pCad);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * None
 *
 * PURPOSE:

 * Goes through each input CAD parameter and if there is a link to a SIR record
 * on the corresponding CAD output, uses a mapping from SIR record name to setup
 * table entry to extract the input value. This value is then converted to a
 * string and loaded into the input CAD value.
 *
 * DESCRIPTION:
 * Called from each CAD inam routine when CAD database loaded.
 *
 * EXTERNAL VARIABLES:
 * IR_Setup *ir_setup
 *
 * PRIOR REQUIREMENTS:
 * Requies that the ir_setup record be loaded with initial setup values.
 * This is done through the function load_camera_setup
 *
 * DEFICIENCIES:
 * None
 *
 *- */
void load_input_cad(struct cadRecord *pCad)
{
  char* cad_argp = &pCad->a[0];
  struct link* cad_outp = &pCad->outa;
  char arg = 'A';
  int i;
  Table_Item *ir_setup_item;

  logger::message(logger::Level::Full, "load_input_cad: CAD name=%s Nargs=%d", pCad->name, pCad->ctyp);

  if (ir_setup == NULL) {
    throw Error("IR_Setup not loaded - cannot load CAD!", E_ERROR, EINVAL, __FILE__, __LINE__);
  } else {

    // Get the IR_Setup table item (we only have one for a preferences type table)
    ir_setup_item = ir_setup->find(0);
  }

  // Load input CAD parameters from IR_Setup table- use the linked SIR record to
  // provide mapping to table parameter name.
  for (i=0; i<pCad->ctyp; i++) {
    logger::message(logger::Level::Full, "load_input_cad: Arg%c='%s',Out link type=%d", arg, cad_argp, cad_outp->type);

    // Construct the Parameter DB name from the SIR record name
    if (cad_outp->type == DB_LINK) {
      struct dbAddr* dbp = dbGetPdbAddrFromLink(cad_outp); //(struct dbAddr*) cad_outp->value.db_link.pdbAddr;
      logger::message(logger::Level::Full, "load_input_cad: linked record=%s", dbp->precord->name);

      // All these records will have the string DB_PREFIX as first part
      // strip away to reveal parameter name
      string sir_name = dbp->precord->name;
      string prefix = DB_PREFIX;
      int pos;
      string val;

      // If we find a SIR record then use it to lookup a table value
      // Otherwise just move on to next input
      if ((pos = sir_name.find(prefix)) != -1) {
	sir_name = sir_name.erase(0,pos+prefix.length());
	
	// Look for the parameter in the table.
	// if found put the value into the CAD input string (up to maximum number of chars)
	if (ir_setup_item->find(sir_name, val)) {
	  strncpy(cad_argp, val.c_str(), MAX_STRING_SIZE-1);
	  logger::message(logger::Level::Full,"Loaded CAD input %c (%s) with %s from setup table", arg, sir_name.c_str(), cad_argp);
	} else
	  logger::message(logger::Level::Full,"Setup table value for CAD input %c (%s) not found!", arg, sir_name.c_str());
	  
      }
    } 
	  
    // Increment the cad field pointers to next argument
    arg++;
    cad_argp += MAX_STRING_SIZE;
    cad_outp++;
  }
}
