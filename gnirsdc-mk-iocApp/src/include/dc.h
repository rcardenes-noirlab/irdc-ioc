/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI PROJECT
 *
 * FILENAME dc.h
 * 
 * PURPOSE
 * Include file with GNIRS DC common definitions
 *
 * $Log: dc.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.20  2005/09/23 01:30:41  pjy
 * Moved DC_Cmd def to dc_state.h so that snl could pick up
 *
 * Revision 1.19  2005/09/11 23:21:03  pjy
 * Added prototypes for enum << overloads
 *
 * Revision 1.18  2005/09/08 23:18:07  pjy
 * 3 status messages
 *
 * Revision 1.17  2005/09/07 02:05:20  pjy
 * Added readmode and timemode input defs
 *
 * Revision 1.16  2005/09/07 00:45:42  pjy
 * Moved message_write to dc.cc
 *
 * Revision 1.15  2005/09/06 21:56:32  pjy
 * Added statusMsgs
 *
 * Revision 1.14  2005/08/18 03:17:16  pjy
 * Extended health records
 *
 * Revision 1.13  2005/08/02 04:45:17  pjy
 * DHS connection management changes
 *
 * Revision 1.12  2005/07/30 01:11:55  pjy
 * Fixed typo
 *
 * Revision 1.11  2005/07/29 23:43:43  pjy
 * Added controller_info, observec & cadStartmsg
 *
 * Revision 1.10  2005/07/28 09:33:47  pjy
 * Mods after DC AT day 1
 *
 * Revision 1.9  2005/07/26 06:13:53  pjy
 * Added nframes
 *
 * Revision 1.8  2005/07/24 11:57:47  pjy
 * Added reconnecting
 *
 * Revision 1.7  2005/07/19 06:33:12  pjy
 * Added bunit
 *
 * Revision 1.6  2005/07/18 00:32:24  pjy
 * Removed syntax error (extra comma)
 *
 * Revision 1.5  2005/07/14 06:45:03  pjy
 * Added "saving" param
 *
 * Revision 1.4  2005/07/12 06:32:10  pjy
 * Added simulation params.
 * Changed abortingSdsu to cancelingSdsu
 * Added gratName
 * Added test pattern verify
 *
 * Revision 1.3  2005/04/27 03:40:37  pjy
 * PCI DSP working
 *
 * Revision 1.2  2005/02/25 04:46:40  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.1  2004/12/22 22:43:28  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.6  2004/12/22 06:56:03  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.5  2004/08/20 02:18:26  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.4  2004/04/25 02:03:44  pjy
 * Pre T2.2 version
 *
 * Revision 1.3  2003/09/19 06:25:14  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.2  2003/08/05 02:26:38  pjy
 * Latest development version :)
 *
 * Revision 1.1  2003/05/22 02:43:53  pjy
 * GNIRS include files as at May o3
 *
 * Revision 1.3  2003/03/25 06:23:35  pjy
 * State of DC software at data task testing start
 *
 * Revision 1.2  2003/03/01 06:03:17  pjy
 * Changes up to GNIRS fire
 *
 * Revision 1.1  2001/10/19 00:24:42  pjy
 * Added for GNIRS DC
 *
 *
 */

#ifndef DC_H
#define DC_H

#include <string>
#include "dc_state.h"
#include "epicsTypes.h"
#include "exception.h"

/* put definitions here */
const unsigned long MAX_REF_PIXELS = 32;    //+ maximum reference pixels
const unsigned long MAX_QLS = 4;            //+ maximum number of QuickLookScreens
const int N_STATUS_STRS = 3;                //+ Number of status strings in DB

//+ DC message queues
static const char DC_SLAVE_MQ_NAME[] = "DCSlaveMQ";
static const char DC_DATA_MQ_NAME[] = "DCDataMQ";
static const char DC_ISR_MQ_NAME[] = "DCISRMQ";

//+ instrument parameter database name constants
static const char CONFIGNAME[] = "configName";
static const char DEBUG_MODE[] = "debug_mode";
static const char SIM_MODE[] = "sim_mode";
static const char STATE[] = "state";
static const char SLAVE_STATE[] = "slave_state";
static const char DATA_STATE[] = "data_state";
static const char ABORTING[] = "aborting";
static const char SAVING[] = "saving";
static const char OBSERVEC[] = "observeC";
static const char CAD_START_MSG[] = "cad_start_msg";
static const char SDSU_CANCELING[] = "cancelingSDSU";
static const char STOPPING[] = "stopping";
static const char CONNECTION[] = "connection";
static const char SDSU_INIT_DONE[] = "initSDSUDone";
static const char SDSU_CMD_RUNNING[] = "SDSUcmdRunning";
static const char OBS_DATADEST[] = "d_obs_dataDest";
static const char VIEW_DATADEST[] = "d_view_dataDest";
static const char DOCONNECT[] = "doConnect";
static const char DHS_AVAILABLE[] = "dhs_available";
static const char DHS_CONNECTED[] = "dhs_connected";
static const char DHS_SERVER_IP[] = "dhs_server_ip";
static const char DHS_SERVER_NAME[] = "dhs_server_name";
static const char DHS_DATA_LABEL[] = "data_label";
static const char BUNIT[] = "bunit";
static const char NFRAMES[] = "nframes";
static const char OBS_RAWQLS[] = "obs_rawQLS";
static const char VIEW_RAWQLS[] = "view_rawQLS";
static const char VIEW_CMPQLS[] = "view_cmpQLS";
static const char FITS_AVAILABLE[] = "fits_available";
static const char FITS_CONNECTED[] = "fits_connected";
static const char FITS_SERVER_NAME[] = "fits_server_name";
static const char FITS_SERVER_PORT[] = "fits_server_port";   
static const char DC_HEALTH[] = "dc_health";
static const char DC_HEALTH_MSG[] = "dc_health_msg";
static const char DHS_HEALTH[] = "dhs_health";
static const char DHS_HEALTH_MSG[] = "dhs_health_msg";
static const char DHS_PROBLEM[] = "dhs_problem";
static const char DHS_PROBLEM_MSG[] = "dhs_problem_msg";
static const char FITS_HEALTH[] = "fits_health";
static const char FITS_HEALTH_MSG[] = "fits_health_msg";
static const char FITS_PROBLEM[] = "fits_problem";
static const char FITS_PROBLEM_MSG[] = "fits_problem_msg";
static const char DATA_HEALTH[] = "data_health";
static const char DATA_HEALTH_MSG[] = "data_health_msg";
static const char SLAVE_HEALTH[] = "slave_health";
static const char SLAVE_HEALTH_MSG[] = "slave_health_msg";
static const char CAMERA_HEALTH[] = "camera_health";
static const char CAMERA_HEALTH_MSG[] = "camera_health_msg";
static const char CAMERA_OP[] = "camera_op";
static const char CAMERA_OP_MSG[] = "camera_op_msg";
static const char DETTYPE[] = "detType";
static const char DETID[] = "detID";
static const char TIMDSP[] = "timingDSP";
static const char INTERFACEDSP[] = "interfaceDSP";

static const char SDSU_BOARD[] = "sdsu_board";
static const char SDSU_CMD[] = "sdsu_cmd";
static const char SDSU_MEM_SPACE[] = "sdsu_mem_space";
static const char SDSU_MEM_ADDR[] = "sdsu_mem_addr";
static const char SDSU_ARG1[] = "sdsu_arg1";
static const char SDSU_ARG2[] = "sdsu_arg2";
static const char SDSU_DEBUG[] = "sdsu_debug";
static const char SDSU_NTESTS[] = "sdsu_ntests";
static const char SDSU_COMMS[] = "sdsu_comms";

static const char GRATING_MIN[] = "gratMin";
static const char GRATING_MAX[] = "gratMax";
static const char GRATING_NAME[] = "gratName";

static const char CONTROLLER_INIT_MESS[] = "initSDSU";
static const char CONTROLLER_TEST_MESS[] = "testSDSU";
static const char CONTROLLER_RESET_MESS[] = "resetSDSU";
static const char CONTROLLER_SHUTDOWN_MESS[] = "shutdownSDSU";
static const char CONTROLLER_DEBUG_MESS[] = "debugSDSU";
static const char CONTROLLER_CANCEL_MESS[] = "cancelSDSU";
static const char CONTROLLER_CMD_MESS[] = "DSPDirectCmd";

static const char DO_TEST_PATTERN[] = "doTestPat";
static const char TEST_PATTERN_CONSTANT[] = "testPatConst";
static const char TEST_PATTERN_SEED[] = "testPatSeed";
static const char TEST_PATTERN_INC[] = "testPatInc";
static const char TEST_PATTERN_AMP_INC[] = "testPatAmpInc";
static const char TEST_PATTERN_READ_INC[] = "testPatReadInc";
static const char TEST_PATTERN_OS[] = "testPatOS";
static const char TEST_PATTERN_VERIFY[] = "testPatVerify";

static const char STATUS_STR[] = "statusStr";

static const char IR_READ_MODE_IN[] = "readMode";
static const char IR_QUADRANTS_IN[] = "quadrants";
//static const char TIME_MODE_IN[] = "timeMode";


namespace DC {
	static const std::string SLAVE_READY_SEM = "DcSlaveReady";
	static const std::string DATA_READY_SEM = "DcDataReady";
	static const std::string SLAVE_MBOX = "SlaveIn";
	static const std::string DATA_MBOX = "DataIn";
}

//+ An EPICS string
typedef char EPICS_String[MAX_STRING_SIZE];

//+ EPICS parameter names

//+ Data destination
enum DC_Dest {
  DC_DEST_DHS,
  DC_DEST_FITS,
  DC_DEST_NONE
};
std::string& operator<<(std::string& str, DC_Dest dest);

//+ DC control message structure - NB POSIX mq seems to like at least 16 bytes
struct DC_Control_Msg {
  DC_Cmd cmd;
  int padding[3];
};

//+ DC slave message structure - NB POSIX mq seems to like at least 16 bytes
struct DC_Slave_Msg {
  DC_Cmd cmd;
  int padding[3];
};

//+ DC data message structure - NB POSIX mq seems to like at least 16 bytes
struct DC_Data_Msg {
  DC_Cmd cmd;
  long nreads;
  int padding[2];
};

//+ class declarations 


//+ Global function names
#ifdef  __cplusplus
extern "C" {
#endif
  void dc_set_debug_level();
  void dc_set_health(const char* health_valp, Error_Type health_val, const char* health_msgp, char* health_msgv,
		     const char* health_msg);
  void update_status_msg(const char* str);
  //+ Writes a timestamped message to stdout
  // > dl - debugging level
  // > format - format string for message
  // > ap - variable argument list
//  void message_write(Debug_Level dl, const char* format ...);
  void dc_slave(void *);
  void dc_data(void *);
#ifdef  __cplusplus
}
#endif

#endif /* DC_H */
