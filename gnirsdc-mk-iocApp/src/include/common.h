/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 *
 * FILENAME common.h
 * 
 * PURPOSE
 * common data structures, and class definitions
 *
 */
#ifndef _COMMON_DEF_H
#define _COMMON_DEF_H

#include "utility.h"
#include "exception.h"

// Constants 
const unsigned long IMAGE_BUF_SIZE = 0x80000; //+ Image readout buffer size

// typedefs 

typedef char *Clienthosttype;


//+ Debugging constants, each level is turned on in a bit mask
const int DEBUG_NOLOG = 0x80000000;          //+ Sign bit is used to turn off logging
const int DEBUG_ESSENTIAL = 1;               //+ Essential debugging info
const int DEBUG_ENTRYPOINTS = 2;             //+ Entry point messages
const int DEBUG_INDICATORS = 4;              //+ Tracewrites to indicate location
const int DEBUG_CALCS = 8;                   //+ Progressive calculations
const int DEBUG_STATUS = 16;                 //+ All status handling
const int DEBUG_STATE = 32;                  //+ Change state handling eg ABORT
const int DEBUG_SPECIAL = 64;                //+ Special case temporary debugging
const int DEBUG_GUI_STATUS = 128;            //+ GUI specific debugging

//+ Some common instrument parameter DB names
static const char PROGRESS[] = "progress";
static const char TARGET[] = "target";
static const char ITERATION[] = "iteration";

/* function declarations */

// Heartbeat function increment modulus 2^24.
int heartbeat(int& heart);

//+ Min max functions
template<class T> void sum_minmax(T* pix, double& sum, T& min, T& max, int n);

//+ Simple image statistics function
void imstat(void *ptr, int npix, short bitpix, double& sum, double& min, double& max);

//+ Strips a CVS Name tag string of CVS related info
char* get_version_string(const char* version_str);


//===========================================================================================================================
//   T E M P L A T E   D E F I N I T I O N S
//===========================================================================================================================
/*
 *+ 
 * FUNCTION NAME: sum_minmax
 * 
 * INVOCATION: sum_minmax(unsigned char* pix, double& sum, unsigned char& min, 
 *  		          unsigned char& max, int n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > pix - pointer to image
 * < sum - sum of image
 * < min - minimum pixel value
 * < max - maximum pixel value
 * > n - number of pixels
 * 
 * FUNCTION VALUE: int
 * 
 * PURPOSE: Computes sum, min and max of an image
 * 
 * DESCRIPTION: 
 * sum_minmax functions. Compute sum, min and max for array for different
 * data types
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *- 
 */
template<class T> void sum_minmax(T* pix, double& sum, T& min, T& max, int n)
{
  int i;
  sum = 0;

  if (n <= 0) {
    throw Error("Sum_minmax called with n <= 0!", E_ERROR, -1, __FILE__, __LINE__);
  }
  min = pix[0];
  max = pix[0];

  for (i=0; i<n; i++) {
    if (pix[i]>max) 
      max = pix[i];
    else if (pix[i]<min) 
      min = pix[i];
    sum += pix[i];
  }
}
#endif
