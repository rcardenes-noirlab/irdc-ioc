[schematic2]
uniq 191
[tools]
[detail]
w 2242 2603 100 0 n#190 ecad2.cmd.OCID 1424 2400 1760 2400 1760 2592 2784 2592 2784 2304 2896 2304 ecars.C.ICID
w 1698 2027 100 0 n#189 ecad2.cmd.STLK 1424 2016 2032 2016 eseq.Toggle.SLNK
w 874 2507 -100 0 CLID inhier.CLID.P 816 2496 992 2496 992 2464 1104 2464 ecad2.cmd.ICID
w 3250 2123 100 0 n#187 ecars.C.FLNK 3216 2112 3344 2112 3344 2192 3424 2192 efanouts.fan.SLNK
w 2642 2315 100 0 n#185 eseq.Toggle.LNK2 2608 2304 2736 2304 2736 2336 junction
w 2722 2347 100 0 n#185 eseq.Toggle.LNK1 2608 2336 2896 2336 ecars.C.IVAL
w 2002 2315 100 0 n#183 hwin.hwin#181.in 2032 2304 2032 2304 eseq.Toggle.DOL2
w 2002 2347 100 0 n#182 hwin.hwin#180.in 2032 2336 2032 2336 eseq.Toggle.DOL1
w 1540 2607 100 0 n#161 ecad2.cmd.VAL 1424 2496 1488 2496 1488 2596 1652 2596 1652 2592 outhier.VAL.p
w 1510 2467 100 0 n#159 ecad2.cmd.MESS 1424 2464 1656 2464 outhier.MESS.p
w 898 2603 100 0 n#156 inhier.DIR.P 820 2592 1036 2592 1036 2496 1104 2496 ecad2.cmd.DIR
s 848 1568 200 0 This schematic contains a CAD record which is
s 848 1520 200 0 replicated for each command (cmd). This is
s 848 1472 200 0 generalised so that it can be used for any command
s 848 1424 200 0 that the detector may legitimately ignore.
[cell use]
use inhier 804 2551 100 0 DIR
xform 0 820 2592
use inhier 800 2455 100 0 CLID
xform 0 816 2496
use efanouts 3424 2055 100 0 fan
xform 0 3544 2208
p 3280 1500 100 0 0 def(FLNK):$(top)combCars31.PROC
p 3280 1884 100 0 0 def(LNK1):$(top)combCars32.PROC
p 3536 2048 100 1024 0 name:$(top)$(cmd)$(I)
use hwin 1840 2263 100 0 hwin#181
xform 0 1936 2304
p 1843 2296 100 0 -1 val(in):0
use hwin 1840 2295 100 0 hwin#180
xform 0 1936 2336
p 1843 2328 100 0 -1 val(in):2
use ecars 2896 2055 100 0 C
xform 0 3056 2224
p 2992 2000 100 1024 1 name:$(top)$(cmd)$(I)
use eseq 2032 1927 100 0 Toggle
xform 0 2320 2224
p 2352 2334 100 0 1 DLY2:1.0e+00
p 2144 1856 100 1024 1 name:$(top)$(cmd)$(I)
p 2000 2336 75 1280 -1 pproc(DOL1):NPP
p 2000 2304 75 1280 -1 pproc(DOL2):NPP
p 2608 2336 75 768 -1 pproc(LNK1):PP
p 2608 2304 75 768 -1 pproc(LNK2):PP
use eborderC 608 799 100 0 eborderC#0
xform 0 2288 2104
p 3184 952 100 768 -1 author:Peter Young
p 3184 1088 40 0 -1 comment1:1997-04-24 Initial GMOS version. Janet Tvedt
p 3184 1068 40 0 -1 comment2:2000-12-12 Modified for GNIRS. Peter Young
p 3184 920 100 768 -1 date:2000-12-12
p 3408 1000 200 768 -1 file:dcNoOpCmd.sch
p 3680 952 100 0 -1 page:1
p 3792 952 100 0 -1 pages:1
p 3456 952 100 0 -1 revision:2
p 3408 1064 150 768 -1 system:Gemini GNIRS DC
use outhier 1624 2423 100 0 MESS
xform 0 1640 2464
use outhier 1620 2551 100 0 VAL
xform 0 1636 2592
use ecad2 1252 2540 100 0 cmd
xform 0 1264 2240
p 3476 2620 100 0 0 MFLG:THREE STATES
p 1192 1884 100 0 1 SNAM:dcCADnoOp
p 1192 1912 100 0 1 name:$(top)$(cmd)
[comments]
