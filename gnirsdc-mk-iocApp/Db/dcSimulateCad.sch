[schematic2]
uniq 26
[tools]
[detail]
w 4976 3323 100 0 n#25 elongins.simulateID.VAL 4912 3312 5088 3312 ecars.simulateC.ICID
w 4072 2795 100 0 n#22 ecad4.simulate.OUTA 4000 2784 4192 2784 hwout.hwout#21.outp
w 4078 3019 100 0 n#3 cadPlus.simulateP.STAT 4256 2336 4160 2336 4160 3008 4032 3008 junction
w 4028 3085 100 0 n#3 ecad4.simulate.VAL 4000 3008 4032 3008 4032 3172 4128 3172 outhier.VAL.p
w 4104 2379 100 0 n#20 ecad4.simulate.SPLK 4000 2368 4256 2368 cadPlus.simulateP.SPIN
w 4104 2411 100 0 n#19 ecad4.simulate.STLK 4000 2400 4256 2400 cadPlus.simulateP.STIN
w 3528 3067 -100 0 CLID inhier.CLID.P 3488 3056 3616 3056 3616 2976 3680 2976 ecad4.simulate.ICID
w 3120 2843 100 0 n#14 embbis.simulateMenu.FLNK 3088 2832 3200 2832 estringouts.simulateModeStr.SLNK
w 3136 2867 100 0 n#10 embbis.simulateMenu.VAL 3088 2800 3120 2800 3120 2864 3200 2864 estringouts.simulateModeStr.DOL
w 3550 2819 100 0 n#8 estringouts.simulateModeStr.OUT 3456 2816 3680 2816 ecad4.simulate.A
w 4060 3037 100 0 n#4 ecad4.simulate.MESS 4000 2976 4064 2976 4064 3108 4128 3108 outhier.MESS.p
w 3644 3069 100 0 n#2 inhier.DIR.P 3552 3140 3648 3140 3648 3008 3680 3008 ecad4.simulate.DIR
s 2480 1984 200 0 The CAD user subroutine sets to the desired simulation level. 
[cell use]
use elongins 4656 3255 100 0 simulateID
xform 0 4784 3328
use ecars 5152 3408 100 0 simulateC
xform 0 5248 3232
p 5104 3024 100 0 1 def(FLNK):$(top)combCars3.PROC
use hwout 4192 2743 100 0 hwout#21
xform 0 4288 2784
p 4288 2775 100 0 -1 val(outp):$(sadtop)simMode.VAL PP MS
use cadPlus 4288 2464 100 0 simulateP
xform 0 4336 2352
p 4256 2240 100 0 1 set1:cad simulateP
use inhier 3536 3099 100 0 DIR
xform 0 3552 3140
use inhier 3472 3015 100 0 CLID
xform 0 3488 3056
use embbis 2908 2876 100 0 simulateMenu
xform 0 2960 2816
p 2592 2832 100 0 1 ONST:FAST
p 2592 2800 100 0 1 TWST:FULL
p 2592 2864 100 0 1 ZRST:NONE
use estringouts 3292 2900 100 0 simulateModeStr
xform 0 3328 2832
p 3264 2752 100 0 1 OMSL:closed_loop
use outhier 4096 3131 100 0 VAL
xform 0 4112 3172
use outhier 4096 3067 100 0 MESS
xform 0 4112 3108
use ecad4 3772 3064 100 0 simulate
xform 0 3840 2688
p 3776 2816 100 0 1 FTVA:STRING
p 3776 2784 100 0 0 FTVB:STRING
p 3616 2016 100 0 0 FTVD:STRING
p 3616 1984 100 0 0 INAM:CADsimulateInit
p 3028 2604 100 0 0 MFLG:THREE STATES
p 3760 2304 100 0 1 SNAM:CADsimulate
p 4000 2410 75 0 -1 pproc(STLK):PP
use eborderC 2368 1403 100 0 eborderC#0
xform 0 4048 2708
p 4944 1556 100 768 -1 author:Peter Young
p 4944 1680 40 0 -1 comment1:2005-04-13 Original version Peer Young
p 4944 1524 100 768 -1 date:2001-02-05
p 5168 1604 200 768 -1 file:dcSimulateCad.sch
p 5440 1556 100 0 -1 page:1
p 5552 1556 100 0 -1 pages:1
p 5216 1556 100 0 -1 revision:0
p 5168 1668 150 768 -1 system:Gemini GNIRS DC
[comments]
