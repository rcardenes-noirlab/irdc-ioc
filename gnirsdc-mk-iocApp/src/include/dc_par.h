char parse_args(int argc, char **argv, char *inputfile, char *outputbase,
		char *darkfile, char *flatfile, float *offset, float *exptime,
		float *numimg, char *cosmicbool, char *readnoise, 
		char *cosmicout, float *dccurrent, float *initexp, 
		float *shieldcover, char *exptimefile, float *crrate, 
		float *rnvar, float *gain, PIXEL *P);
