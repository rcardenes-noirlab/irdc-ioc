#include <clock.h>
#include <utility.h>
#include <epicsTime.h>

using std::chrono::microseconds;
using std::chrono::nanoseconds;
using std::chrono::seconds;

Clock::Clock() {
	reset();
}

void
Clock::reset() {
	measurements.clear();
	sys_clock_ref = system_clock::now();
	sty_clock_ref = steady_clock::now();
}

void
Clock::set_timing_prefix(std::string new_t_prefix)
{
	t_prefix = new_t_prefix;
}

void
Clock::set_timing_index(unsigned new_t_index)
{
	t_index = new_t_index;
}

unsigned
Clock::timing_index() const
{
	return t_index;
}

void
Clock::add_measurement(sty_time_point measurement, bool increment_index)
{
	measurements[t_prefix + left_justify(to_string(t_index), 2, '0')] = measurement;
	if (increment_index)
		t_index++;
}

void
Clock::visit_measurements(std::function<void(std::string, double)> fn) const
{
	for (auto it=measurements.begin(); it != measurements.end(); it++) {
		auto diff = double(((*it).second - sty_clock_ref).count()) / 1000000000;
		fn((*it).first, diff);
	}
}

std::string
Clock::time_point_to_string(const sty_time_point &t) const
{
	auto point = sys_clock_ref + (t - sty_clock_ref);
	auto point_us = std::chrono::time_point_cast<microseconds>(point);
	auto point_s = std::chrono::time_point_cast<seconds>(point);
	auto us = point_us.time_since_epoch() - (point_s.time_since_epoch());
	const std::time_t t_c = system_clock::to_time_t(point);
	std::string time_string;

	char target[9];
	strftime(target, 9, "%H:%M:%S", std::gmtime(&t_c));

	time_string = std::string(target) + "." + left_justify(to_string(long(us.count())), 6, '0');

	return time_string;
}

void
Clock::set_period_start(const sty_time_point &&t)
{
	sty_period_start = t;
}

void
Clock::set_period_end(const sty_time_point &&t)
{
	sty_period_end = t;
}

sty_time_point
Clock::get_period_start() const
{
	return sty_period_start;
}

sty_time_point
Clock::get_period_end() const
{
	return sty_period_end;
}

const auto EPICS_epoch = system_clock::from_time_t(POSIX_TIME_AT_EPICS_EPOCH);

epicsTime
Clock::to_epicsTime(const sty_time_point& t) const
{
	auto point = sys_clock_ref + (t - sty_clock_ref);
	auto point_ns = std::chrono::time_point_cast<nanoseconds>(point);
	auto point_s = std::chrono::time_point_cast<seconds>(point);
	auto seconds = point_s.time_since_epoch();
	auto nseconds = point_ns.time_since_epoch() - seconds;

	epicsTimeStamp ets{static_cast<epicsUInt32>(seconds.count()),
			   static_cast<epicsUInt32>(nseconds.count())};

	return epicsTime(ets);
}

epicsTime
Clock::to_epicsTime(const sty_time_point&& t) const
{
	auto &lhs = t;
	return to_epicsTime(lhs);
}
