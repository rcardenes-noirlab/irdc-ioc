/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME ir_setup.cc
 * 
 * GENERAL DESCRIPTION
 *
 *  Implements a IR preferences class using base Preferences class
 *
 */

/* Include files */
#ifdef vxWorks
#include "configuration.h"
#endif
#include "camera.h"
#include "ir_setup.h"
/*
 *+ 
 * FUNCTION NAME: IR_Setup::IR_Setup():Preferences("ir.setup","")
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: A constructor for ir setup file
 * 
 * DESCRIPTION: Initialises setup values to sensible defaults
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
IR_Setup::IR_Setup():
  Preferences("ir.setup", 1, true, "")
{
  IR_Exposure_Info texpose = {
    DEFAULT_DO_IR_READOUT,
    DEFAULT_DO_RESET,
//    DEFAULT_SET_INTERVAL,
    DEFAULT_READ_MODE,
    DEFAULT_QUAD_MODE,
//    DEFAULT_TIME_MODE,
//    DEFAULT_NFOWLER,
//    DEFAULT_READ_TIME,
//    DEFAULT_READ_INTVAL,
//    DEFAULT_PERIOD,
//    DEFAULT_NPERIODS,
    DEFAULT_EXPTIME,
    DEFAULT_EXPTIME,
//    DEFAULT_NCOADDS,
//    DEFAULT_NRESETS,
//    DEFAULT_RESET_DELAY};
  };
  IR_Data_Info tdata = {
    DEFAULT_DO_SUBTRACT,
    DEFAULT_DO_SAVE,
    DEFAULT_DO_SAVE_VAR,
    DEFAULT_DO_SAVE_QUAL,
    DEFAULT_SAVE_EACH_NDR,
    DEFAULT_DISPLAY_EACH_NDR,
    DEFAULT_SAVE_EACH_COADD,
    DEFAULT_DISPLAY_EACH_COADD,
    DEFAULT_COSMIC_REJ,
    DEFAULT_DO_CMP_IM,
    DEFAULT_WAVE_MIN,
    DEFAULT_WAVE_MAX,
    DEFAULT_COSM_THRESH,
    DEFAULT_COSM_MIN,
    DEFAULT_SUB_FNAME,
  };

  view = obs = texpose;
  d_view = d_obs = tdata;
}
/*
 *+ 
 * FUNCTION NAME: void IR_Setup::put(void* itemp, int pos)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > itemp - item pointer - optionally put pars from this pointer arg
 * > pos - position in list to place item
 * Both above pars are ignored for ir setup files - only one internal item
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Puts a new setup structure into internal table 
 * 
 * DESCRIPTION: Uses put_ir_params method to place sets of parameters for each 
 * observing mode into the table
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void IR_Setup::put(void* itemp, int pos)
{
  Table_Item* item;

  if ((item = find("NONAME")) != NULL) {
    throw Error("Preferences already exist in table!",E_ERROR,-1,__FILE__,__LINE__);     
  }

  item = new Table_Item("NONAME"); 
  
  put_ir_params(IR_VIEW_MODE, item);
  put_ir_params(IR_OBSERVE_MODE, item);
}

/*
 *+ 
 * FUNCTION NAME: void IR_Setup::put_ir_params
 * 
 * INVOCATION: put_ir_params(IR_Mode ir_mode, Table_Item *item)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > ir_mode - IR observing mode
 * > item - table item to update
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Puts a new setup structure into internal table 
 * 
 * DESCRIPTION: Uses table put methods to place each setup par
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */

void IR_Setup::put_ir_params(IR_Mode ir_mode, Table_Item *item)
{
  string mode;
  IR_Exposure_Info *ir_einfo;           //+ Pointer to current ir exposure info structure
  IR_Data_Info *ir_dinfo;               //+ Pointer to current ir data info structure
  
  
  if (ir_mode == IR_VIEW_MODE) {
    mode = "view_";
    ir_einfo = &view;
    ir_dinfo = &d_view;
  } else {
    mode = "obs_";
    ir_einfo = &obs;
    ir_dinfo = &d_obs;
  }
  
  item->put(mode+DO_IR_READOUT, ir_einfo->doReadout);
  item->put(mode+DO_RESET, ir_einfo->doReset);
//  item->put(mode+SET_INTERVAL, ir_einfo->setIntvl);
  item->put(mode+IR_READ_MODE, (int)ir_einfo->readModeSet);
  item->put(mode+IR_QUAD_MODE, (int)ir_einfo->quadrantsSet);
//  item->put(mode+TIME_MODE, (int)ir_einfo->timeModeSet);
//  item->put(mode+NFOWLER, ir_einfo->nfowler);
//  item->put(mode+READ_TIME, ir_einfo->readTime);
//  item->put(mode+READ_INTVAL, ir_einfo->readIntvl);
//  item->put(mode+PERIOD, ir_einfo->period);
//  item->put(mode+NPERIODS, ir_einfo->nperiods);
  item->put(mode+EXPOSED, ir_einfo->exposed);
//  item->put(mode+NCOADDS, ir_einfo->ncoadds);
//  item->put(mode+NRESETS, ir_einfo->nresets);
//  item->put(mode+RESET_DELAY, ir_einfo->resetDelay);
  
  item->put(mode+DO_SUBTRACT, ir_dinfo->doSubFile);
  item->put(mode+DO_SAVE_VAR, ir_dinfo->saveVar);
  item->put(mode+DO_SAVE_QUAL, ir_dinfo->saveQual);
  item->put(mode+DO_SAVE_NDR, ir_dinfo->saveNdrs);
  item->put(mode+DO_SAVE_COADD, ir_dinfo->saveCoadds);
  item->put(mode+DO_COSM_REJ, ir_dinfo->cosmRej);
  item->put(mode+COSM_THRESH, ir_dinfo->cosmThrsh);
  item->put(mode+DO_CMP_IM, ir_dinfo->doCmpIm);
  item->put(mode+WAVE_MIN, ir_dinfo->waveMin);
  item->put(mode+WAVE_MAX, ir_dinfo->waveMax);  
  item->put(mode+SUB_FNAME, ir_dinfo->subFile);
  
  insert(item,0);
}	  
/*
 *+ 
 * FUNCTION NAME: void IR_Setup::get(Table_Item *item)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > item - get ir pars from this table item
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Gets a setup structure from internal table item
 * 
 * DESCRIPTION: Uses table find methods to fetch each setup par
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void IR_Setup::get(Table_Item *item)
{
  get_ir_params(IR_VIEW_MODE, item);
  get_ir_params(IR_OBSERVE_MODE, item);
}

/*
 *+ 
 * FUNCTION NAME: void IR_Setup::get_ir_params
 * 
 * INVOCATION: get_ir_params(IR_Mode ir_mode, Table_Item *item)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > ir_mode - IR observing mode
 * > item - table item to update
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Gets a setup structure from internal table item
 * 
 * DESCRIPTION: Uses table get_ir_params method to find params for each observing mode
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */

void IR_Setup::get_ir_params(IR_Mode ir_mode, Table_Item *item)
{
  string mode;
  IR_Exposure_Info *ir_einfo;           //+ Pointer to current ir exposure info structure
  IR_Data_Info *ir_dinfo;               //+ Pointer to current ir data info structure
  int t;
    
  if (ir_mode == IR_VIEW_MODE) {
    mode = "view_";
    ir_einfo = &view;
    ir_dinfo = &d_view;
  } else {
    mode = "obs_";
    ir_einfo = &obs;
    ir_dinfo = &d_obs;
  }
  
  // Now for each possible setup parameter try to find in table
  item->find(mode+DO_IR_READOUT, ir_einfo->doReadout);
  item->find(mode+DO_RESET, ir_einfo->doReset);
//  item->find(mode+SET_INTERVAL, ir_einfo->setIntvl);
  item->find(mode+IR_READ_MODE, t);
  ir_einfo->readModeSet = (IR_Readmode_Type) t;
  item->find(mode+IR_QUAD_MODE, t);
  ir_einfo->quadrantsSet = (IR_Quadrants_Type) t;
//  item->find(mode+TIME_MODE, t);
//  ir_einfo->timeModeSet = (IR_Timing_Mode) t;
//  item->find(mode+NFOWLER, ir_einfo->nfowler);
//  item->find(mode+READ_TIME, ir_einfo->readTime);
//  item->find(mode+READ_INTVAL, ir_einfo->readIntvl);
//  item->find(mode+PERIOD, ir_einfo->period);
//  item->find(mode+NPERIODS, ir_einfo->nperiods);
  item->find(mode+EXPOSED, ir_einfo->exposed);
  item->find(mode+EXPOSEDRQ, ir_einfo->exposedRQ);
//  item->find(mode+NCOADDS, ir_einfo->ncoadds);
//  item->find(mode+NRESETS, ir_einfo->nresets);
//  item->find(mode+RESET_DELAY, ir_einfo->resetDelay);

  item->find(mode+DO_SUBTRACT, ir_dinfo->doSubFile);
  item->find(mode+DO_SAVE, ir_dinfo->save);
  item->find(mode+DO_SAVE_VAR, ir_dinfo->saveVar);
  item->find(mode+DO_SAVE_QUAL, ir_dinfo->saveQual);
  item->find(mode+DO_SAVE_NDR, ir_dinfo->saveNdrs);
  item->find(mode+DO_SAVE_COADD, ir_dinfo->saveCoadds);
  item->find(mode+DO_COSM_REJ, ir_dinfo->cosmRej);
  item->find(mode+COSM_THRESH, ir_dinfo->cosmThrsh);
  item->find(mode+DO_CMP_IM, ir_dinfo->doCmpIm);
  item->find(mode+WAVE_MIN, ir_dinfo->waveMin);
  item->find(mode+WAVE_MAX, ir_dinfo->waveMax);  
  item->find(mode+SUB_FNAME, ir_dinfo->subFile);
}	  

/*
 *+ 
 * FUNCTION NAME: IR_Setup::~IR_Setup()
 * 
 * INVOCATION: delete ir
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
IR_Setup::~IR_Setup()
{
}
/*
 *+ 
 * FUNCTION NAME: irSetupTest()
 * 
 * INVOCATION: irSetupTest
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void irSetupTest()
{
  IR_Setup setup;
  try {
    cout << "Reading setup file " << IR_SETUP << endl;
    setup.read(IR_SETUP);
    setup.write("ir_setup.new");
  }
  catch (Error& ce) {
    ce.print_error(__FILE__,__LINE__);
  }
}  



