/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * CICADA PROJECT
 * 
 * FILENAME table.cc
 * 
 * GENERAL DESCRIPTION
 * Simplre preferences reader/writer. Derived from standard table class but
 * differentiated by 
 *
 * REQUIREMENTS
 * Requires use of STL classes "map", "set" and "string"
 * 
 */

/* Include files */
#ifdef vxWorks
#include <vxWorks.h>
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include "utility.h"
#include "exception.h"
#include "preferences.h"

/* defines */

/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/* global function definitions */

/*
 *+ 
 * FUNCTION NAME: Preferences::Preferences
 * 
 * INVOCATION: p = new Preferences(const string tname, const int il, const bool ns, const string ire, const string is, 
 *                                 const string ie, const string cc, const string sep, const string eop, const void* dp, 
 *                                 const void* dps, const int sz)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of this table
 * > il - number of items to format on single output line
 * > ns - Whether or not items are sorted by name
 * > ire - regular expression string that defines item name
 * > is - item name start delimiter
 * > ie - item name end delimiter
 * > cc - comment delimiter
 * > sep - parameter value separator string
 * > eop - parameter value ending strring/delimeter
 * > dp - pointer to item cache
 * > dps - pointer to item cache save
 * > sz - size of data cache
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Initialise a preference table
 * 
 * DESCRIPTION: 
 *  Preferences Table
 *  The preferences table file is a text file consisting of preference values.
 *  Text after the # symbol is ignored as a comment.
 *  Each preference set consists of a name in [] followed by a series
 *  param:value pairs, one per line
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Preferences::Preferences(const string tname, const int il, const bool ns, const string ire, const string is, const string ie,
			 const string cc, const string sep, const string eop, const void* dp, const void* dps, 
			 const int sz): 
  Table(tname,il,ns,ire,is,ie,cc,sep,eop,dp,dps,sz)
{
}

/*
 *+ 
 * FUNCTION NAME: void Table::write(const string tname)
 * 
 * INVOCATION: pref.write(tname)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of table file to write
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Writes preferences in standard format using base class method
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Preferences::write(const string tname)
{
  // Ensure local vars are written to hash table - always do write if a cache is not used
  if (need_update() || (data==NULL))
    update();
  Table::write(tname);
}
/*
 *+ 
 * FUNCTION NAME: Preferences::read(string tname)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of table file to read
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  Reads a preferences table into internal data structure
 * 
 * DESCRIPTION: Uses base class method to parse preferences file
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Preferences::read(const string tname)
{
  Table::read(tname);
  // Get the only preference set we have and place into member vars
  if (n_items>0)
    get_element(0);
}
/*
 *+ 
 * FUNCTION NAME: void Preferences::get(Table_Item *item)
 * 
 * INVOCATION: pref.get(item)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * item - pointer to data item to get from table
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: This function does nothing so derived class is expected to 
 *  provide functionality
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES:  
 *
 *- 
 */
void Preferences::get(Table_Item *item)
{
}          
/*
 *+ 
 * FUNCTION NAME: void Preferences::put(void* itemp, int pos)
 * 
 * INVOCATION: pref.put(itemp,pos)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * itemp - void*, pointer to data item to put into table
 * pos - index to place table item, defaults to append
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: This function does nothing so derived class is expected to 
 *  provide functionality
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES:  
 *
 *- 
 */
void Preferences::put(void* itemp, int pos)
{
}
/*
 *+ 
 * FUNCTION NAME: void Preferences::get_element(int idx)
 * 
 * INVOCATION: pref.get_element(idx)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * idx - index into preferences table
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE:  Gets element at index idx
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES:  
 *
 *- 
 */
void Preferences::get_element(int idx)
{
  Table_Item *item=find(idx);
  get_element(item->name);
}
/*
 *+ 
 * FUNCTION NAME: void Preferences::get_element(const string item_name)
 * 
 * INVOCATION: pref.get_element(item)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * item_name - name of element in preferences table
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE:  Gets element of name item_name
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES:  
 *
 *- 
 */
void Preferences::get_element(const string item_name)
{
  // Before overwriting internal buffer ensure any changes flushed
  if (last_name != item_name) {
    if (need_update()) 
      update();
    get(find(item_name));
    last_name = item_name;
    last_pos = index(item_name);
  }
}

/*
 *+ 
 * FUNCTION NAME: Preferences::~Preferences()
 * 
 * INVOCATION: destructor
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: simple destructor - does nothing
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Preferences::~Preferences()
{
}
