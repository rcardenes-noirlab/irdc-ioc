/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *   fits.h
 * 
 * PURPOSE
 *   Fits class definitions
 *
 * HISTORY
 *   July 1994 PJY    Created for RSAA CICADA package
 *   June 2002 PJY    Ported to VxWorks/GNIRS from RSAA CICADA package
 */
#ifndef _FITS_H
#define _FITS_H

// Include files 
#include <string>
#include <fcntl.h>
#include "utility.h"
#include "exception.h"
#include "ipc.h"

//+ defines and constants

// Convert bitpix to bytes per pixel
#define BYTE_PIX(bp) (abs(bp)/8)

const int FITSBLOCK = 2880;
const int FITSNAMELEN = 256;
const int FITSKEYWORDLEN = 80;
const int FITS_NOKEYWORD = 202;
const int FITS_MAX_EXTEND = 64;

//+ Max value of TFIELD: hence max number of TFORMn and TTYPEn keywords in a bintable extension
const int FITS_MAX_TFIELDS = 32; 
const unsigned long FITS_MAX_EXTEND_MASK = 0xFFFFFFFF;
const int FITS_KEYS_PER_BLOCK = FITSBLOCK/FITSKEYWORDLEN;

//+ Number of standard keywords written
const int FITS_STD_PHU_KEYWORDS = 9; //FIXME 
const int FITS_STD_IHU_KEYWORDS = 8; // FIXME is this right??

// typedefs 
typedef char Fitsname[FITSNAMELEN];
typedef char Fitsline[FITSKEYWORDLEN+1];

//+ Set of FITS extension types supported
enum Ext_Type { 
  EXT_IMAGE, 
  EXT_BINTABLE, 
  EXT_NOT_INITIALISED 
};

//+ Data for each FITS extension
class Extension {
  public:
    Extension();
    Ext_Type type;                        //+ Type of each extension
    int ihu;                              //+ Number of extension header blocks for each extension
    int bitpix;                           //+ Bits per pixel for each extension
    int nx;                               //+ X Axis values for each extension 
    int ny;                               //+ Y Axis values for each extension 
    int pcount;                           //+ Pcount values for each extension 
    int gcount;                           //+ Gcount values for each extension 
    double bzero;                         //+ BZERO value for each extension
    double bscale;                        //+ BSCALE value for each extension
    int detx;                             //+ x-coord in mosaic for each extension
    int dety;                             //+ y-coord in mosaic for each extension
    int headers_written;                  //+ Count of headers written in each extension
    int end_written;                      //+ Flag set if END header written
    int keysexist;                        //+ Count of number of keywords in header for each extension
    int keysextra;                        //+ Count of extra of keywords in header for each extension
    int hdr_size;                         //+ Size of header section in bytes
    int data_size;                        //+ Size of data section in bytes
    char* header;                         //+ Pointer to current header location for writing
    char* header_start;                   //+ Pointer to starting header location for each extension
    char* data;                           //+ pointer to data extensions
//    Virtual_Mem *vmheader;                //+ Mappings to FITS headers
//    Virtual_Mem *vmdata;                  //+ Mappings to FITS data parts
};

//+ data for binary table extensions
class Bintable {
  public:
    Bintable();

    int tfields;                          //+ TFIELD value required for BINTABLE extensions
    string ttype[FITS_MAX_TFIELDS+1];     //+ TTYPE value required for BINTABLE extensions
    string tform[FITS_MAX_TFIELDS+1];     //+ TFORM value required for BINTABLE extensions
};

// class declarations 

/*
CLASS
    Fits
    Class to maniupulate disk FITS files
DESCRIPTION  
    Class to maniupulate disk FITS files. Provides a set of methods for reading and writing
    FITS files. Supports efficient memory mapping acces to data where byte swapping or data
    scaling is not required. Handles MEF with IMAGE and BINTABLE extensions.
KEYWORDS
    FITS
*/
class Fits {
 private:
  // GROUP:   Member variables
  int fd;                                //+ file descriptor of opened FITS file
  char errmsg[MSGLEN];                      //+ Buffer to use for composing error messages in exceptions
  int file_open_read;                    //+ Flag set when file open for reading
  int file_open_write;                   //+ Flag set when file open for writing
  int file_open_update;                  //+ Flag set when file open for updating
  int fix_simple_keyword;                //+ Flag set when SIMPLE keyword requires attention before closing
  int headers_read;                      //+ Count of headers read when file opened
  int total_hdr_size;                    //+ Total header size
  int total_data_size;                   //+ Total data size
  int open_data;                         //+ Flag set if data sections of file opened
  int mosaic_data;                       //+ Flag set if required to load MEF file in mosaiced buffer
  Fitsline card;                         //+ Single instance of a FITS header line 
  char *mosaic_datap;                    //+ ptr to mosaiced data after loading

  // GROUP:   Member functions

  //+ Initialise FITS object with default values
  // > ne - number of extensions to initialise
  void initialise(int ne);

  //+ Allocate data for FITS extensions
  // > ne - number of extensions
  void alloc_xtension(int ne);

  //+ Reallocate data for extra FITS extensions
  // > old_ne - previous number of extensions
  // > new_ne - new number of extensions
  void realloc_xtension(int old_ne, int new_ne);

  //+ Checks for END keyword in header line
  // > hdr - header line to check
  int check_end(string hdr);

 public:
  // Constructors
  //+ Simple constructor - just initialises data
  Fits();

  //+ Initialises and opens existing file for access = mode (default RDONLY)
  // Optionally reads the file with default read parameters.
  // > filename - name of existing FITS file
  // > mode - access mode, default=O_RDONLY, others defined in sys/fcntl.h.
  Fits(char* filename, mode_t mode=O_RDONLY, int readin = FALSE);

  //+ Initialises, opens and reads existing file 
  // > filename - name of existing FITS file
  // > outbitpix - array of bitpix for each extension
  Fits(char* filename, int* outbitpix);

  //+ Copy constructor.
  //  Returns a new copy of a given Fits file.  Opens and closes the input
  //  image, which must have been instantiated first and must be
  //  deleted later.
  //  After copying all the keywords, pads the header block(s) with spaces
  //  (see the FITS standard at
  //  http://archive.stsci.edu/fits/users_guide/node42.html).
  //  At the end of the data, zeros are used for padding
  //  (see the standard at
  //  http://archive.stsci.edu/fits/users_guide/node24.html#s:PrimDat,
  //  and ftp://nssdcftp.gsfc.nasa.gov/standards/fits/blocking94.txt).
  // > input_image - reference to an existing Fits file. 
  // > new_name   - full pathname of the new file which holds the copy.
  // > mode - access mode, default=O_RDONLY, others defined in sys/fcntl.h.
  // > open_file - if TRUE, the Fits copy is read and left open;
  //               if FALSE, the copy is neither opened or read.
  Fits(Fits& input_image, Fitsname new_name, mode_t mode=O_RDONLY,
       bool open_file=TRUE);

  //+ Destructor
  ~Fits();

  // GROUP:   Member variables
  Fitsname fitsname;                     //+ Name of FITS file
  int phu;                               //+ Number of primary header blocks
  int naxis;                             //+ Number of axes
  int extend;                            //+ Flag set if file is a multi extension file
  int nextend;                           //+ Count of number of extensions in file
  int mosaic_able;                       //+ TRUE if DETSIZE and DETSEC keywords available for mosaicing
  int data_vm_mapped;                    //+ Flag set if virtual memory mapping possible
  int use_vm;                            //+ Flag set if virtual memory mapping wanted
  Fitsline key;                          //+ buffer for returning keywd values
  Extension *xt;                         //+ Image extension data
  Bintable *btable;                      //+ BINTABLE extension stuff
   
  // GROUP:   Member functions
  //+ Open an existing FITS file and read standard keywords.
  //  The file must be closed for both reading and writing,
  //  else this throws an exception.
  // > filename - name of existing FITS file
  void open(char* filename);

  //+ Read a FITS file
  //  The file must not be open for writing - throws and exception if it is.
  // > filename - name of existing FITS file
  // > outbitpix - bitpix for each extension
  // > do_scaling = flag set if scaling required when read
  // > lock_it - flag set when required to lock file
  // > od - flag set when required to read data sections
  // > md - flag set when required to form mosaic
  // > ext_mask - mask set to indicate which extensions to open
  void read(char *filename, int* outbitpix=NULL, int do_scaling = TRUE, int lock_it=FALSE, int od=TRUE, int md=FALSE, 
	    unsigned long ext_mask=FITS_MAX_EXTEND_MASK);  

  //+ Close already opened FITS file, check headers
  //  and append END keyword if missing.
  //  set abort parameter if requiring close without checking headers
  // > abort - if set then do not try to write file headers on close
  void close(int abort=0);

  //+ Create a new empty FITS file
  // > filename - name of new FITS file
  // > et - array of extension types
  // > bp - array of bitpix
  // > ne - array of axis lengths
  // > mode - access mode as in stat.h
  // > phk - number of primary header keys to reserve - multiple of 36
  // > ext - set to number of extensions
  // > ihk - number of image extension keys to reserve
  // > od - set if open data segments
  void create(char *filename,  Ext_Type *et, int* bp, int *ne, int mode, int phk=FITS_KEYS_PER_BLOCK, int ext=0, int ihk=0, 
	      int od=TRUE);

  //+ write standard header keywords
  // > et - array of extension types
  // > bs - array of BSCALEs
  // > bz - array of BZEROs
  // > pc - array of PCOUNTs
  // > gc - array of GCOUNTs
  // > decimals - number of decimal places for floating point
  void write_std_keywords(Ext_Type *et, double *bs, double *bz, int* pc=NULL, int* gc=NULL, int decimals=6);

  //+ Write a keyword line
  // > tline - line to write
  // > ext_num - extension number to write to
  void write_keyword_line(const char *tline, int ext_num=0);

  //+ Write an integer keyword line
  // > keyword - name of keyword
  // > keyval - value of keyword
  // > comment - comment for keyword
  // > ext_num - extension number to write to
  void write_ikeyword(const char *keyword, int keyval, const char* comment, int ext_num=0);

  //+ Update an integer keyword
  // > keyword - name of keyword
  // > keyval - value of keyword
  // > comment - comment for keyword
  // > ext_num - extension number to write to
  // > wr - 
  void update_ikeyword(const char *keyword, int keyval, const char* comment, int ext_num=0, int wr=FALSE);

  //+ Write a binary keyword line
  // > keyword - name of keyword
  // > keyval - value of keyword
  // > comment - comment for keyword
  // > ext_num - extension number to write to
  void write_bkeyword(const char *keyword, int keyval, const char* comment, int ext_num=0);

  //+ Update a binary keyword line
  // > keyword - name of keyword
  // > keyval - value of keyword
  // > comment - comment for keyword
  // > ext_num - extension number to write to
  // > wr - 
  void update_bkeyword(const char *keyword, int keyval, const char* comment, int ext_num=0, int wr=FALSE);

  //+ Write a floating keyword line
  // > keyword - name of keyword
  // > keyval - value of keyword
  // > comment - comment for keyword
  // > ext_num - extension number to write to
  void write_dkeyword(const char *keyword, double keyval, int decimals, const char* comment, int ext_num=0);

  //+ Update a floating keyword line
  // > keyword - name of keyword
  // > keyval - value of keyword
  // > comment - comment for keyword
  // > ext_num - extension number to write to
  // > wr - 
  void update_dkeyword(const char *keyword, double keyval, int decimals, const char* comment, int ext_num=0, int wr=FALSE);

  //+ Write a string keyword line
  // > keyword - name of keyword
  // > keyval - value of keyword
  // > comment - comment for keyword
  // > ext_num - extension number to write to
  void write_skeyword(const char *keyword, const char* keyval, 
		      const char* comment, int ext_num=0);

  //+ Update a string keyword line
  // > keyword - name of keyword
  // > keyval - value of keyword
  // > comment - comment for keyword
  // > ext_num - extension number to write to
  // > wr - 
  void update_skeyword(const char *keyword, const char* keyval, const char* comment, int ext_num=0, int wr=FALSE);

  //+ Read a named keyword - return value if found
  // > keyword - name of keyword
  // > ext_num - extension number to write to
  char* read_keyword(const char *keyword, int ext_num=0);

  //+ Find a named keyword - return address if found
  // > keyword - name of keyword
  // > ext_num - extension number to write to
  char* find_keyword(const char *keyword, int ext_num=0);

  //+ Write data to a FITS file
  // > src_data - data to write
  // > in_bitpix - 
  // > xo = starting x coord to write
  // > yo = starting y coord to write
  // > wid = row width
  // > hei = column height
  // > ext_num - extension number to write to  
  void write_data(char *src_data, int in_bitpix, int ext_num=0, int xo=0, int yo=0, int wid=-1, int hei=-1);

  //+ Get number of fits header keywords
  // > ext_num - extension number to write to  
  // > extra_only - set if only non-standard keywords required
  int num_keywords(int ext_num=0, int extra_only=0);

  //+ Get a header keyword
  // > key_no - number of key to get
  // > ext_num - extension number to write to  
  char* get_keyword(int key_no,int ext_num=0);

  //+ Copy headers from FITS extension
  // > Fits - FITS file to copy from
  // > ext_num - extension number to write to  
  // > old_ext_num - extension number to copy from
  // > extra_only - set if only non-standard keywords required  
  void copy_headers(Fits&, int ext_num = 0, int old_ext_num = 0,int extra_only=0);

  //+ FITS sift routine - used for finding FITS keywords when reading a file
  // > line - input header line
  // > par - parameter to look for
  char * keysift(const char *line, const char *par);
};
/* local template function definitions */

/*
 *+ 
 * FUNCTION NAME: template<class T> void scale_in
 * 
 * INVOCATION: scale_in(T* out_data, char* in_data, int owid, int wid, int hei, int bitpix, double bscale, double bzero)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * ! out_data - pointer to output data memory after bscale and bzero applied, o = i*bs+bz
 * > in_data - pointer to input data memory
 * > owid - width of output row
 * > wid - width of input row
 * > hei - height of input column
 * > bitpix - data type of each pixel
 * > bscale - bscale of data
 * > bzero - bzero of data
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Do data scaling
 * 
 * DESCRIPTION: Uses template data type to produce an output array of
 * desired data type from the input array after applying bscale and
 * bzero parameters. It also handles correct byte order for local
 * machine.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T> void scale_in(T* out_data, char* in_data, int owid, int wid, int hei, int bitpix, double bscale,
				double bzero)
{
  int r,c,i=0;
  char *bpix = in_data;
  short *spix = (short *) in_data;
  int *ipix = (int *) in_data;
  float *fpix = (float *) in_data;
  double *dpix = (double *) in_data;
  Byte_Order_Type local_byte_order=byte_order();  
  T* opix = out_data;

  switch (bitpix) {
  case BYTE_BITPIX:
    for (r=0; r<hei; r++) {
      for (c=0; c<wid; c++) 
	opix[c] = (T) (bpix[i++]*bscale + bzero);
      opix += owid;
    }
    break;
  case SHORT_BITPIX:
    if (local_byte_order == WORD_LITTLE_ENDIAN) {	
      for (r=0; r<hei; r++) {
	for (c=0; c<wid; c++) 
	  opix[c] = (T) (sbyte_swap(spix[i++])*bscale + bzero);
	opix += owid;
      }
    } else {
      for (r=0; r<hei; r++) {
	for (c=0; c<wid; c++) 
	  opix[c] = (T) (spix[i++]*bscale + bzero);
	opix += owid;
      }
    }
    break;
  case INT_BITPIX:
    if (local_byte_order == WORD_LITTLE_ENDIAN) {	
      for (r=0; r<hei; r++) {
	for (c=0; c<wid; c++) 
	  opix[c] = (T) (lbyte_swap(ipix[i++])*bscale + bzero);
	opix += owid;
      }
    } else {
      for (r=0; r<hei; r++) {
	for (c=0; c<wid; c++) 
	  opix[c] = (T) (ipix[i++]*bscale + bzero);
	opix += owid;
      }
    }
    break;
  case FLOAT_BITPIX:
    if (local_byte_order == WORD_LITTLE_ENDIAN) {	
      for (r=0; r<hei; r++) {
	for (c=0; c<wid; c++) 
	  opix[c] = (T) (fbyte_swap(fpix[i++]));
	opix += owid;
      }
    } else {
      for (r=0; r<hei; r++) {
	for (c=0; c<wid; c++) 
	  opix[c] = (T) (fpix[i++]);
	opix += owid;
      }
    }
    break;
  case DOUBLE_BITPIX:
    if (local_byte_order == WORD_LITTLE_ENDIAN) {
      for (r=0; r<hei; r++) {
	opix += r*owid;
	for (c=0; c<wid; c++) 
	  opix[c] = (T) (dbyte_swap(dpix[i++]));
      }
    } else {
      for (r=0; r<hei; r++) {
	opix += r*owid;
	for (c=0; c<wid; c++) 
	  opix[c] = (T) (dpix[i++]);
      }
    }
    break;
  default:
    break;
  }
}
/*
 *+ 
 * FUNCTION NAME: template<class T> void scale_out
 * 
 * INVOCATION: scale_out(T *src_data, char* data, int npix, int bitpix, double bscale, double bzero)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > src_data - pointer to fits data memory
 * ! data - pointer to output data memory o = (i-bz)/bs
 * > npix - number of pixels to process
 * > bitpix - data type of each pixel
 * > bscale - bscale of data
 * > bzero - bzero of data
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Applies bzero and bscale to fits pixel values to produce
 * scaled output array
 * 
 * DESCRIPTION: Uses template data type to handle each of the FITS
 * standard data types to produce a normalised data array from the
 * FITS pixels. It also handles correct byte order for local machine.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T> void scale_out(T *src_data, char* data, int npix, int bitpix, double bscale, double bzero)
{
  int i;
  char *bpix = data;
  short *spix = (short *) data;
  int *ipix = (int *) data;
  float *fpix = (float *) data;
  double *dpix = (double *) data;

  Byte_Order_Type local_byte_order=byte_order();  

  if (bscale == 0.0) {
    throw Error("Fits: Bscale must be greater than zero!", E_ERROR, IPC_ERR ,__FILE__, __LINE__); 
  }
  
  // Scale data and swap bytes to BIG_ENDIAN if necessary
  switch (bitpix) {
  case BYTE_BITPIX:
    for (i=0; i<npix; i++) {
      bpix[i] = (char) floor((src_data[i] - bzero)/bscale);
    }
    break;
  case SHORT_BITPIX:
    if (local_byte_order == WORD_LITTLE_ENDIAN) {	
      for (i=0; i<npix; i++) 
	spix[i] = sbyte_swap((src_data[i] - bzero)/bscale);
    }else {
      for (i=0; i<npix; i++) 
	spix[i] = (short) floor((src_data[i] - bzero)/bscale);
    }
    break;
  case INT_BITPIX:
    if (local_byte_order == WORD_LITTLE_ENDIAN) { 	
      for (i=0; i<npix; i++) 
	ipix[i] = lbyte_swap((src_data[i] - bzero)/bscale);
    } else {
      for (i=0; i<npix; i++) 
	ipix[i] = (int) floor((src_data[i] - bzero)/bscale);
    }
    break;
  case FLOAT_BITPIX:
    if (local_byte_order == WORD_LITTLE_ENDIAN) {	
      for (i=0; i<npix; i++) fpix[i] = fbyte_swap(src_data[i]);
    } else {
       memcpy((char*)fpix,(char*)src_data,npix*sizeof(float));
    }
   break;
  case DOUBLE_BITPIX:
    if (local_byte_order == WORD_LITTLE_ENDIAN) {	
      for (i=0; i<npix; i++) dpix[i] = dbyte_swap(src_data[i]);
    } else {
       memcpy((char*)dpix,(char*)src_data,npix*sizeof(double));
    }
    break;
  default:
    break;
  }
}
/*
 *+ 
 * FUNCTION NAME: template<class T> void copy_out
 * 
 * INVOCATION: copy_out(T *src_data, char* data, int npix, int bitpix)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > src_data - pointer to fits data memory
 * ! data - pointer to output data memory o = (i-bz)/bs
 * > npix - number of pixels to process
 * > bitpix - data type of each pixel
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Copies src data to output location
 * 
 * DESCRIPTION: Uses template data type to handle each of the FITS standard data
 * types. No need to worry about byte order here because the scale_out function
 * handled different byte orders.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- */
template<class T> void copy_out(T *src_data, char* data, int npix, int bitpix)
{
  int i;
  char *bpix = data;
  short *spix = (short *) data;
  int *ipix = (int *) data;
  float *fpix = (float *) data;
  double *dpix = (double *) data;

  // Copy each pixel
  switch (bitpix) {
  case BYTE_BITPIX:
    for (i=0; i<npix; i++) 
      bpix[i] = (char)src_data[i];
    break;
  case SHORT_BITPIX:
    for (i=0; i<npix; i++) 
      spix[i] = (short)src_data[i];
    break;
  case INT_BITPIX:
    for (i=0; i<npix; i++) 
      ipix[i] = (int)src_data[i];
    break;
  case FLOAT_BITPIX:
    for (i=0; i<npix; i++) 
      fpix[i] = (float)src_data[i];
   break;
  case DOUBLE_BITPIX:
    for (i=0; i<npix; i++) 
      dpix[i] = (double)src_data[i];
    break;
  default:
    break;
  }
}

#endif //_FITS_H 
