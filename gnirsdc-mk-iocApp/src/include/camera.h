/* Copyright (c) 1994-2004 by RSAA Computing Section 
 *
 * CICADA PROJECT
 *
 * FILENAME camera.h
 * 
 * PURPOSE
 *
 * Defines generic operations for a camera containing a modern digital detector.
 * Classes derived off this are used to interface directly to specific types of
 * camera and therefore contain all specific knowledge of that particular
 * camera. For example CCD cameras and IR cameras. Other types of camera such as
 * a CCD camera for tip tilt operation or driftscan operation could also be
 * supported.  This class will be used to control detector setup and operation, e.g.,
 * common methods for handling data flow are defined here. It also instantiates a
 * Controller object as one of its members so that it can interface directrly to
 * the specific detector controller electronics.
 *
 */
#ifndef _CAMERA_H
#define _CAMERA_H

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#include <limits.h>
#include "configuration.h"
#include "hardware.h"
#include "controller.h"
#include "detector_regions.h"
#include "parameter.h"
#include "fits.h"

using std::shared_ptr;

/*---------------------------------------------------------------------------
 External functions
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 External variables
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/

//+ Defaults for Camera_State structure
const int DEFAULT_GET_TEMPS = 1;
const int DEFAULT_GET_VOLTS = 0;


//+ Region defaults
const int DEFAULT_CAMERA_COORDS = TRUE;
const int DEFAULT_NREGIONS = 1;
const int DEFAULT_X0 = 0;
const int DEFAULT_Y0 = 0;
const int DEFAULT_WIDTH = 0;
const int DEFAULT_HEIGHT = 0;
const int DEFAULT_RCF = 1;
const int DEFAULT_CCF = 1;
const int DEFAULT_OVERSCAN = 0;
const unsigned long long DEFAULT_CHIP_MASK = ULONG_LONG_MAX;
#define DEFAULT_WINDOW ""

//+ Image defaults
const int DEFAULT_UNSIGNED_DATA = TRUE;
const int DEFAULT_PHU1 = 3;
const int DEFAULT_PHU2 = 2;
const int DEFAULT_IHU = 1;
const int DEFAULT_BITPIX = SHORT_BITPIX;
const double DEFAULT_BSCALE = 1.0;
const double DEFAULT_BZERO = 0.0;
const double DEFAULT_VERIFY = 0;

//+ Readout defaults
const int DEFAULT_DO_SAVE = TRUE;
const int DEFAULT_DO_DISPLAY = TRUE;
const int DEFAULT_RUN_NUMBER = 0;
const double DEFAULT_EXPTIME = 0.5;

//+ Defaults for reset structure
// const long DEFAULT_NRESETS = 1;
// const double DEFAULT_RESET_DELAY = 0.0;


//+ Database parameter names
static const char OP_ACTIVITY[] = "activity";
static const char CID[] = "cid";
static const char EXPOSURE_DONE[] = "exposure_done";
static const char VERIFIED[] = "verified";
static const char ELAPSED[] = "elapsed";
static const char SHUTTER_DURATION[] = "shutter_duration";
static const char EXPOSED[] = "exposed";
static const char EXPOSEDRQ[] = "exposedRQ";
static const char SHUTTER_OPEN_TIME[] = "shutter_open_time";
static const char DO_READOUT[] = "do_readout";
static const char TRANSFER_TIME[] = "transfer_time";
static const char TOTAL_OPERATION_TIME[] = "total_operation_time";
static const char READOUT_TIME[] = "readout_time";
static const char READOUT_WAIT_TIME[] = "readout_wait_time";
static const char CONTROLLER_SIMULATION[] = "controller_simulation";
static const char CONTROLLER_SIMULATION_DB[] = "simMenuOut";
static const char DATEOBS[] = "dateobs";
static const char UTSTART[] = "utstart";
static const char UTEND[] = "utend";
static const char TAI[] = "tai";
static const char LRNS[] = "lrns";
static const char NDAVGS[] = "ndavgs";
static const char DATEOBS_DB[] = "dateobs";
static const char UTSTART_DB[] = "utstart";
static const char UTEND_DB[] = "utend";
static const char TAI_DB[] = "wcsTai";
static const char LRNS_DB[] = "lrns";
static const char NDAVGS_DB[] = "ndavgs";

static const char CAMERA_MODE[] = "mode_status.mode";
static const char GUIDER_MODE[] = "guider_mode";
static const char OP_STATUS_TYPE[] = "op_status.type";

//+ Size to align frames memory
const int ALIGN_SIZE = sizeof(double);

/*--------------------------------------------------------------------------
 Typedefs 
 -------------------------------------------------------------------------*/

//+ State of Cicada image NB ready=0 and empty=1 mandatory for semaphore
enum Image_State {
  IMAGE_STALL=-1, 
  IMAGE_READY=0, 
  IMAGE_EMPTY=1
};

/*--------------------------------------------------------------------------
 Class declarations 
 -------------------------------------------------------------------------*/
/*
CLASS
    Camera

DESCRIPTION  
    Base Camera class definition - Actual camera definitions derived from this.
KEYWORDS
    Camera, Hardware
*/
class Camera: public Hardware {

public:

  static const std::string IMAGE_READY_SEM;
  static const std::string IMAGE_EMPTY_SEM;
  static const std::string DATA_XFER_SEM;
  static const std::string PARAMETER_SEM;

  //+ Simple constructor - used for setting up configuration.
  // > cam   - Camera_Desc - description of the camera
  // > camid - camera id
  // > c     - controller id
  Camera(Camera_Desc& cam, int camid, int c, Platform_Specific* ps);

  //+ Mid-level Constructor - used for accessing parameter information.
  // > cam   - Camera_Desc - description of the camera
  // > camid - camera id
  // > c 	  - controller id
  // > param_host - host on which the parameter database resides
  // > st	- status memory for this instrument
  // > pm	- parameter map
  Camera(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st,
	 Platform_Specific* ps);

  //+ Main constructor.
  // > st  - status memory for this instrument
  // > ip  - instrument status parameter object
  // > deb - debug level
  Camera(Instrument_Status *st, Parameter* ip, int deb, Platform_Specific* ps);

  //+ Camera class Destructor
  virtual ~Camera();  

  // GROUP:   Camera class Public Member functions

  //  Signals
  //+ Handle a pause signal (interrupt)
  virtual void handle_interrupt() = 0;

  //+ Interrupt handle for the RESET signal
  virtual void handle_reset();

  //+ Clear the RESET signal
  virtual void clear_reset();

  //+ Interrupt handler for the CHANGE_STATE signal
  virtual void handle_change_state() = 0;

  //+ Sets reporting object and debug level
  // > deb - debugging flag
  virtual void change_debug(int deb);

  //+ Given a region of interest spec, setup the region's readout definition object
  // > region_data - the region specification
  // > bitpix - bits per image pixel from detector
  virtual void setup_regions(Region_Data& region_data, int bitpix);

  // GROUP: Public Camera member data / variables.
  Camera_Desc camera;                   //+ Camera description
  Controller *controller;               //+ Detector controller attached to this camera
  int cam_id;                           //+ ID of this camera in instrument
  int cid;                              //+ ID of controller in instrument
  Camera_Status *camera_status;         //+ Status info for camera
  Detector_Image_Main_Header main_header;//+ Image main header, sent at start
  Detector_Image_Main_Header raw_header;//+ Image main header for raw data, sent at start
  Detector_Regions* rgn[MAX_CONTROLLERS];//+ Region spec from readout request
  Detector_Image *image;                //+ Image repository
  shared_ptr<Semaphore> image_ready_sem;//+ Semaphore controlling access to image
  shared_ptr<Semaphore> image_empty_sem;//+ Semaphore controlling access to image
  shared_ptr<Semaphore> data_xfer_sem;  //+ Semaphore controlling access to data xfer pipeline
  ParameterDB *camera_pDB;              //+ Cicada parameter DB object
  Parameter *camera_statusp;            //+ Cicada camera parameter database
  Parameter *guider_statusp;            //+ Cicada guider parameter database
  Parameter* inst_statusp;              //+ Cicada inst control parameter database
  double exptime;                       //+ Requested exposure time - sec

protected:

  // GROUP: Member methods overloaded or inherited from the parent class.

  //+ initialise class member variables - called from constructors
  virtual void init_vars();

  //+ Add status parameters to the status database - called from constructors
  // > param_host - hostname where parameters reside
  // > init - flag set if required to initialise added parameters
  virtual void add_status_parameters(const char* param_host, bool init);

  // GROUP:   Camera class Protected Member functions
  
  //+ compare actual data from controller with expected data for specified test pattern
  // > iptr - ptr to input data
  // > np - number of pixels to compare
  // > i - pixel value increment
  // > rinc - readout increment
  // ! valstr - string to write out first values that disagree
  virtual unsigned int compare_data(char* iptr, int np, int i, ostringstream& valstr);

  //+ Derived classes must provide a method that adjusts the image header for
  //  their particular needs
  virtual void adjust_image_header(Detector_Image_Main_Header& main_hdr) = 0;

  //+ Routine to transfer image data to image shared memory
  // >  nw = total pixels in one row from all amps after compress
  //   > (ie nw is rgn->nactive_amps*width)
  // > hei = number of rows after compression
  // > nser = number of serial regions
  // > rr = current parallel region
  // > inp = pointer to start of pixel data
  // > dt = data type of this transfer
  // > this_bitpix = Bitpix of data being transferred
  // > frame_skip = number of frames to skip to get to correct frame
  // > main_hdr = main image header describing output frames
  virtual void transfer_image_data(int nw, int hei, int nser, int rr, void* inp, Image_Data_Type dt, int this_bitpix, 
				   int frame_skip, Detector_Image_Main_Header& main_hdr);

  // GROUP:   Camera class Protected Member variables
  unsigned long requested_image_size;   //+ total bytes in requested readout
  int seq_id;                           //+ Instrument id
  Detector_Image_Sub_Header sub_header;//+ Image sub header, sent with each chunk
  Detector_Image_Close_Header close_header;//+ Image main header, sent at end
  unsigned long npix;                   //+ Total pixels requested in readout
  int all_amp_width;                    //+ Full width of current par region
  int nactive_amps;                     //+ Total number of active amps in region
  int bytepix;                          //+ Number of bytes per pixel
  int rows_processed;                   //+ Rows processed so far in readout
  int rowstep,readrows;                 //+ Rows to read in one pass before transferring
  int prog_row[MAX_IMAGE_FRAMES];       //+ progressive row position of readout
  int repeat_count;                     //+ Current command repeat count
  int verify_fail;                      //+ Counter of number of verification failures in an exposure set
  Camera_State state_set;               //+ Current settings for state info
  int IMAGE_MIN_WAIT;                   //+ wait time for image stuff - calibrated to timer resolution

private:

};

/*---------------------------------------------------------------------------
CLASS
    Camera_Test

DESCRIPTION  
    Simple test class for base Camera class - Does not do anything other
    than to declare empty stub methods for pure virtual Camera methods.
 
KEYWORDS
    Camera, Camera_Test
----------------------------------------------------------------------------*/
class Camera_Test: public Camera {

public:

  //+ Basic constructor - used for setting up configuration.
  // > cam   - Camera_Desc - description of the camera
  // > camid - camera id
  // > c     - controller id
  Camera_Test(Camera_Desc cam, int camid, int c, Platform_Specific* ps):
    Camera(cam, camid, c, ps) {};

  //+ Midlevel constructor - used for accessing parameter information.
  // > cam   - Camera_Desc - description of the camera
  // > camid - camera id
  // > c     - camera id
  // > param_host - host on which the parameter database resides
  // > st    - status memory for this instrument
  // > msg   - message
  // > pm    - parameter map
  Camera_Test(Camera_Desc cam, int camid, int c, const char* param_host, Instrument_Status *st,
		  Platform_Specific* ps):
    Camera(cam, camid, c, param_host, st, ps) {};
  
  //+ Main constructor
  // > st  - status memory for this instrument
  // > msg - message fifo buffer
  // > ip  - instrument status parameter object
  // > cp  -
  // > op  -
  // > deb - debug level
  Camera_Test(Instrument_Status *st, Parameter* ip, int deb, Platform_Specific* ps):
    Camera(st, ip, deb, ps) {};

   //+ Camera_Test class Destructor
  virtual ~Camera_Test() {};  

  // GROUP: Camera_Test class Public Member Functions.


  //  Signals
  //+ Handle a pause signal (interrupt)
  virtual void handle_interrupt() {};

  //+ Interrupt handler for the CHANGE_STATE signal
  virtual void handle_change_state() {};

protected:

  //+ Derived classes must provide a method that adjusts the image header for
  //  their particular needs
  virtual void adjust_image_header(Detector_Image_Main_Header& main_hdr) {};

private:

};

////////////////////////////////////////////////////////////////////////////////////////////////////
// Camera function declarations.
////////////////////////////////////////////////////////////////////////////////////////////////////

//+ Construct a Cicada image name from components dir+prefix+run+suffix.
// < iname - output buffer - must be at least n+1 size
// > n - size of buffer
// > dir - directory component of path name
// > prefix - prefix part of filename
// > cam_id - camera id
// > run_number - observation run number
// > suffix - suffix part of filename
// > ext - file extension 
// > ir_ndr - flag to indicate IR file name
// > ndr - IR NDR number
// > coadd - IR coadd number
void image_name(char *iname, int n, const char *dir, const char *prefix, int cam_id, int run_number, const char *suffix, 
		const char* ext = "fits", bool ir_ndr = false, int ndr = 0, int coadd = 0);

//+ Copy frames data to the supplied buffer
// > src_header - input image main header structure
// < dest_header - copied image main header structure
// > src_mem - frames memory
// < dest_mem - a copy of the frames memory
void copy_frame_references(Detector_Image_Main_Header& src_header, Detector_Image_Main_Header& dest_header, char* src_mem,
			   char* dest_mem, bool rel);

//+ Setup the image main header
// > id - identifier
// > cam_id - camera id
// > cid - controller id
// > verify - flag set if to verify readout
// > camera - camera description
// < main_header - main image header to setup
// > rgn - pointer to regions readout structure
// > regions - ROI structure
// > frames_mem - pointer to image frames buffer space
// > image_info - information about image, eg bitpix,bzero,bscale etc
// > save_var - flag set if variance data to be saved in this output file
// > save_qual - flag set if quality data to be saved in this output file
void setup_image_header(int id, int cam_id, int cid, int verify, Camera_Desc* camera, Detector_Image_Main_Header& main_header, 
			Detector_Regions** rgn, Region_Data& regions, char* frames_mem, Detector_Image_Info& image_info, 
			bool save_var, bool save_qual);


//////////////////////////////////////////////////////////////////////////////////////////////////////
// Template Functions
//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *+
 * FUNCTION NAME: check_data
 * 
 * INVOCATION: unsigned int check_data(T* vp, U* dp, int np, ostringstream& valstr)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > vp - pointer to verification data
 * > dp - pointer to input data
 * > np - number of pixels to check
 * < valstr - message indicating why verification falied
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Check input data with a verify image
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *-
 */
//+ 
template<class T, class U> unsigned int check_data(T* vp, U* dp, int np, ostringstream& valstr)
{
  int pix1,pix2,pixc;
  const int NEAR_PIX = 10;
  int i = 0;
  int n = np;

  while (i<n) {
    if (vp[i] != dp[i])
      n = i;
    else
      i++;
  }
  if (i!=np) {
    valstr << "Expected: " << vp[i] << " got: " << dp[i];

    // Print values of adjacent pixels
    pix1 = ((i-NEAR_PIX)>0)? i-NEAR_PIX:0;
    pix2 = ((i+NEAR_PIX)<np)? i+NEAR_PIX:np-1;
    valstr << ": Adjacent pixels (data): ";
    pixc = pix1;
    while (pixc <= pix2) {
      if (pixc == i)
	valstr << " *" << dp[pixc];
      else
	valstr << " " << dp[pixc];
      pixc++;
    } 
    valstr << ": (test): ";
   pixc = pix1;
    while (pixc <= pix2) {
      if (pixc == i)
	valstr << " *" << vp[pixc];
      else
	valstr << " " << vp[pixc];
      pixc++;
    } 
    valstr << ends;
  }
  return i;
}

 /*
 *+ 
 * FUNCTION NAME: unravel_pix
 * 
 * INVOCATION: unravel_pix(T *inp, T *outp, int namps, int fw, int nr,int nc, int xdir, int ydir)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > inp   = pointer to input pixels
 * < outp  = pointer to output pixels
 * > namps = number of frames interleaved in input buffer
 * > fw    = full width of an input row (multiple serials + overscan)
 * > nr,nc = number of rows and columns 
 * > xdir,ydir = either 1 or -1 indicating order of filling input
 *               buffer(see below)
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Unscramble interleaved pixels
 * 
 * DESCRIPTION: 
 *  Unscramble pixels from input buffer (readout coords) to output buffer (camera
 *  coordinates). Input pixels are interleaved from namps amplifiers. Overscan
 *  pixels are added to each output region
 *
 *  xdir and ydir indicate where on the chip the detector is read
 *  out. If xdir is 1, then it is on the left, -1 on the right. If ydir
 *  is 1 then it is on the bottom, and -1 on the top. So, for row-based
 *  reading out the four scenarios are [with (xdir,ydir)] :
 *
 *  (1,-1)           (-1,-1)           (1,1)         (-1,1) 
 *  
 *  ^                            ^
 *  |___________      ___________|     ____________   ____________
 *  ____________      ____________     ____________   ____________
 *                                     |                         |
 *                                     V                         V
 *
 *  and for column-based reading out the scenarios are:
 *
 *  (1,-1)          (1,1)             (-1,-1)            (-1,1)
 *
 *   <-||            ||                 ||->               ||
 *     ||            ||                 ||                 ||
 *     ||            ||                 ||                 ||
 *     ||            ||                 ||                 ||
 *     ||            ||                 ||                 ||
 *     ||            ||                 ||                 ||
 *     ||          <-||                 ||                 ||->
 *
 *  Note that the destination array is fixed, and has its origin at
 *  the bottom left, and reads across, i.e. it matches the (1,1) row-based
 *  scenario. Column based input stream will be re-oriented before being written 
 *  to final output location.
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T> void unravel_pix(T *inp, T *outp, int namps, int fw, int nr, int nc, int xdir, int ydir)
{
  int r,c;       // row and column indices
  int i;         // start of each input row
  int xo,yo;     // input x and y offsets
  int orow, ocol;// output row and column indices
	
  xo = (xdir<0)? nc-1:0;
  yo = (ydir<0)? nr-1:0; 
		
  for (r=0; r<nr; r++) {
    orow = yo + ydir * r;
    i = r*fw;
    for (c=0; c<nc; c++) {
      ocol = xo + xdir * c;
      outp[orow*nc+ocol] = inp[i];
      i += namps;   // increment input pointer by number of interleaved frames
    }
  }
}
/*
 *+
 * FUNCTION NAME: reorient_pix
 * 
 * INVOCATION: reorient_pix(T* ip, T* op, int xo, int fw, int height, int width, int xd, int yd)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > inp   = pointer to input pixels
 * < outp  = pointer to output pixels
 * > oc    = starting column in output buffer
 * > fw    = full width of an output row 
 * > nr,nc = number of rows and columns in input buffer
 * > xdir,ydir = either 1 or -1 indicating order of filling output buffer (see comments in unravel_pix)
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Reorients a working row-based array to original column base
 * 
 * DESCRIPTION: Works through output array row by row, picking pixels from input buffer
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: Pixels are assumed to be column oriented and preprocessed by unravel_pix
 * 
 * DEFICIENCIES: row and column dimensions must be the same for the full array for column orientation
 *
 *-
 */
template<class T> void reorient_pix(T* inp, T* outp, int oc, int fw, int nr, int nc, int xdir, int ydir)
{
  int r,c;       // row and column indices of output array
  int ir, ic;    // input row and column indices
  int ixo, iyo;  // input row and col offsets
  int i;
  int xo,yo;     // starting location in output buffer
  
  ixo = (xdir<0)? nc-1:0;
  iyo = (ydir<0)? nr-1:0;

  xo = (xdir<0)? fw-oc-1:oc;
  yo = (ydir<0)? nc-1:0;
	
  // Loop over output cols and rows - NB these are transposed wrt input
  // Also note that is is most efficient to operate over output rows in order to
  // prevent large memory strides. This assumes that the input array is relatively
  // small compared to the full output array, ie small buffers will be processed
  // at any one time.
  for (r=0; r<nc; r++) {
    ic = ixo + xdir * r;
    i = xo+((yo+r*ydir)*fw); // Starting pixel in output row
    for (c=0; c<nr; c++) {
      ir = iyo + ydir * c;
      outp[i] = inp[ir*nc+ic];
      i += xdir;
     }
  }
}
#endif //_CAMERA_H 

