[schematic2]
uniq 130
[tools]
[detail]
w 1066 -21 100 0 n#128 eapply.gnirsApply.OCLF 752 608 960 608 960 -32 1232 -32 dcNoOpApply.dcNoOpApply#46.CLID
w 858 683 100 0 n#127 eapply.gnirsApply.OCLE 752 672 1024 672 1024 192 1232 192 dcStateApply.dcStateApply#45.CLID
w 858 939 100 0 n#126 eapply.gnirsApply.OCLA 752 928 1024 928 1024 1376 1232 1376 dcSysApply.dcSysApply#44.CLID
w 890 747 100 0 n#125 eapply.gnirsApply.OCLD 752 736 1088 736 1088 512 1184 512 dcData.dcData#78.CLID
w 922 811 100 0 n#124 eapply.gnirsApply.OCLC 752 800 1152 800 1152 832 1184 832 dcSeq.dcSeq#100.CLID
w 890 875 100 0 n#123 eapply.gnirsApply.OCLB 752 864 1088 864 1088 1152 1184 1152 dcSDSUControl.dcSDSUControl#129.CLID
w 786 1867 100 0 n#122 dcData.dcData#78.MESS 1408 512 1664 512 1664 1856 -32 1856 -32 736 368 736 eapply.gnirsApply.INMD
w 786 1835 100 0 n#121 dcData.dcData#78.VAL 1408 544 1632 544 1632 1824 0 1824 0 768 368 768 eapply.gnirsApply.INPD
w 906 779 100 0 n#120 eapply.gnirsApply.OUTD 752 768 1120 768 1120 544 1184 544 dcData.dcData#78.DIR
w 786 1803 100 0 n#119 dcSeq.dcSeq#100.MESS 1408 832 1600 832 1600 1792 32 1792 32 800 368 800 eapply.gnirsApply.INMC
w 786 1771 100 0 n#118 dcSeq.dcSeq#100.VAL 1408 864 1568 864 1568 1760 64 1760 64 832 368 832 eapply.gnirsApply.INPC
w 786 1739 100 0 n#117 dcSDSUControl.dcSDSUControl#129.MESS 1408 1152 1536 1152 1536 1728 96 1728 96 864 368 864 eapply.gnirsApply.INMB
w 786 1707 100 0 n#116 dcSDSUControl.dcSDSUControl#129.VAL 1408 1184 1504 1184 1504 1696 128 1696 128 896 368 896 eapply.gnirsApply.INPB
w 786 1675 100 0 n#115 dcSysApply.dcSysApply#44.MESS 1392 1376 1472 1376 1472 1664 160 1664 160 928 368 928 eapply.gnirsApply.INMA
w 786 1643 100 0 n#114 dcSysApply.dcSysApply#44.VAL 1392 1408 1440 1408 1440 1632 192 1632 192 960 368 960 eapply.gnirsApply.INPA
w 786 2011 100 0 n#113 dcNoOpApply.dcNoOpApply#46.MESS 1392 -32 1792 -32 1792 2000 -160 2000 -160 608 368 608 eapply.gnirsApply.INMF
w 786 1979 100 0 n#112 dcNoOpApply.dcNoOpApply#46.VAL 1392 0 1760 0 1760 1968 -128 1968 -128 640 368 640 eapply.gnirsApply.INPF
w 906 843 100 0 n#108 eapply.gnirsApply.OUTC 752 832 1120 832 1120 864 1184 864 dcSeq.dcSeq#100.DIR
w 874 907 100 0 n#107 eapply.gnirsApply.OUTB 752 896 1056 896 1056 1184 1184 1184 dcSDSUControl.dcSDSUControl#129.DIR
w 842 971 100 0 n#106 eapply.gnirsApply.OUTA 752 960 992 960 992 1408 1232 1408 dcSysApply.dcSysApply#44.DIR
w 792 1899 100 0 n#91 dcStateApply.dcStateApply#45.VAL 1392 224 1696 224 1696 1888 -64 1888 -64 704 368 704 eapply.gnirsApply.INPE
w 786 1947 100 0 n#111 dcStateApply.dcStateApply#45.MESS 1392 192 1728 192 1728 1936 -96 1936 -96 672 368 672 eapply.gnirsApply.INME
w 394 1483 100 0 n#48 eapply.gnirsApply.CLID 368 1024 304 1024 304 1472 532 1472 dcSysCar.dcSysCar#47.CLID
w 848 651 100 0 n#19 eapply.gnirsApply.OUTF 752 640 992 640 992 0 1232 0 dcNoOpApply.dcNoOpApply#46.DIR
w 880 715 100 0 n#18 eapply.gnirsApply.OUTE 752 704 1056 704 1056 224 1232 224 dcStateApply.dcStateApply#45.DIR
s 1424 704 150 0 readout
s -544 -176 200 0 throughout the system.
s 1424 752 150 0 Setup
s 1424 1008 150 0 SDSU
s 1424 416 150 0 Setup data
s 1424 -144 150 0 pause,continue,datum
s 1424 -112 150 0 endGuide,endObserve,
s 1424 -80 150 0 Verify,endVerify,guide,
s 1424 1312 150 0 test,park,debug
s 1424 1344 150 0 Reboot,init,
s -550 60 200 0 This is the top level schematic for the GNIRS detector controller
s -550 0 200 0 adapted from the GMOS science ccd controller schematics.
s -550 -60 200 0 It contains the Apply, CAD and CAR database records. The client ID
s -550 -120 200 0 from the top level apply record is the only CLID maintained
s 1424 128 150 0 Observe,abort,stop
s 1424 1056 150 0 Control
s 1424 368 150 0 processing
[cell use]
use dcSDSUControl 1184 935 100 0 dcSDSUControl#129
xform 0 1296 1088
use dcSeq 1184 623 100 0 dcSeq#100
xform 0 1296 772
use dcData 1184 295 100 0 dcData#78
xform 0 1296 448
use dcSysCar 528 1319 100 0 dcSysCar#47
xform 0 608 1424
use dcNoOpApply 1232 -153 100 0 dcNoOpApply#46
xform 0 1312 -48
use dcStateApply 1232 71 100 0 dcStateApply#45
xform 0 1312 176
use dcSysApply 1232 1255 100 0 dcSysApply#44
xform 0 1312 1360
use eapply 444 1140 150 0 gnirsApply
xform 0 560 784
use eborderC -664 -405 100 0 eborderC#0
xform 0 1016 900
p 1912 -252 100 768 -1 author:Peter Young
p 1904 -112 40 0 -1 comment1:1997-09-29 Original GMOS version. Ken Ramey
p 1904 -122 40 0 -1 comment2:1999-01-19 Added sysCar. Janet Tvedt
p 1904 -132 40 0 -1 comment3:2000-11-02 Modified for GNIRS. Peter Young
p 1912 -284 100 768 -1 date:2000-11-02
p 2136 -204 200 768 -1 file:dc.sch
p 2408 -252 100 0 -1 page:1
p 2520 -252 100 0 -1 pages:1
p 2184 -252 100 0 -1 revision:0
p 2136 -140 150 768 -1 system:Gemini GNIRS DC
[comments]
