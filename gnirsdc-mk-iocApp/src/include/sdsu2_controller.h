/*
 * Copyright (c) 1994 - 2002 by MSSSO Computing Section 
 *
 * CICADA PROJECT
 *
 * FILENAME sdsu2_controller.h
 * 
 * PURPOSE
 * Header file defining the sdsu generation 2 controller class.
 * This is nearly equivalent to the generic SDSU class so is 
 * derived from that
 *
 */
#ifndef _SDSU2_CONTROLLER_H
#define _SDSU2_CONTROLLER_H

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#include "sdsu_controller.h"

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/
//+ Y memory - timing board
const int SDSU_Y_OPTIONS_WORD_PAR = 0x0;
const int SDSU_AB_SETTING_PAR = 0x8;
const int SDSU_TEST_PATTERN_OS_PAR = 0xB;
const int SDSU_TEST_PATTERN_INC_PAR = 0xC;
const int SDSU_TEST_PATTERN_SEED_PAR = 0xD;
const int SDSU_TEST_PATTERN_AMP_INC_PAR = 0xE;
const int SDSU_TEST_PATTERN_READ_INC_PAR = 0xF;
const int SDSU2_RGNS_START_PAR = 0x3F00;

//+ Anti-blooming bit
const int SDSU_AB_BIT = 0x0;

// Test pattern bits
const int SDSU_TEST_PATTERN_BIT = 0x2;
const int SDSU_TEST_PATTERN_CONST_BIT = 0x4;
const int SDSU_TEST_PATTERN_SOURCE_BIT = 0x8;

// Y memory Utility board

/*--------------------------------------------------------------------------
 Class declarations 
 -------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
CLASS
    Sdsu2_Controller

DESCRIPTION  
    SDSU Generation 2 hardware class definition.
 
KEYWORDS
    Sdsu2_Controller, Sdsu_Controller

-------------------------------------------------------------------------*/
class Sdsu2_Controller: public Sdsu_Controller {

public:
  //+ Basic constructor - used to set up configuration
  // > cam - Camera_Desc - description of the camera
  // > camid - controller id
  // > c - sleep time msecs
  Sdsu2_Controller(Camera_Desc& cam, int camid, int c, Platform_Specific* ps);

  //+ Constructor for parameters?
  // > cam        - Camera_Desc - description of the camera
  // > camid      - camera id
  // > c          - controller id
  // > param_host - host on which the parameter database resides
  // > st	  - status memory for this instrument
  // > pm         - parameter map
  Sdsu2_Controller(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st, Platform_Specific* ps);

  //+ This constructor is used on instrument machine
  // > st   - status memory for this instrument
  // > ip   - instrument status parameter object
  // > deb  - debug level
  Sdsu2_Controller(Instrument_Status *st, Parameter* ip, Parameter* cp, Parameter* op,
		  int deb, Platform_Specific* ps);

  //+ Destructor
  virtual ~Sdsu2_Controller();

  // GROUP: Private member functions

  //+ Set up the controller for dsp tests
  virtual void init_test();

  //+ Performs a series of tests by sending DSP commands to the controller
  virtual void run_dsp_tests();

  //+ Test the dsp commands that set voltages
  virtual void do_voltage_tests();

protected:

  // GROUP: Protected member functions

  //+ Sets controller gain according to configured setting.
  virtual void do_gain();  

  //+ Sets controller's time constant algorithm according to configured setting.
  virtual void do_tc();  

  //+ Sets controller's test pattern mode.  Generation 2.
  virtual void do_test_pattern();

  //+ Sets controller's anti-blooming mode.  Generation 2.
  virtual void do_ab();

  //+ Resets timing board.
  virtual void do_reset();

  //+ Returns address of DSP regions setup memory.
  virtual int get_regions_memory_address();

  //+ Read back current voltage settings
  virtual void read_current_voltage_settings() {};

  //+ setup the readout
  // > ns - number of controllers to sync
  // ! readout_rec - readout request structure
  // > trgn - readout regions object
  // ! total_bytes - total bytes in readout - may be modified by controller specific req
  // > exptime - last exposure time
  virtual void setup_readout(int ns, Detector_Readout* readout_rec, Detector_Regions* trgn, unsigned long& total_bytes,
			     double exptime);
  
  //+ Test that a DSP command is sent and acknowledged
  // > name - string to be used to idenitfy command name in report
  // > board_id - identifies board that command is sent to
  // > command - the SDSU command to be tested
  virtual void test_command(const char *name, int board_id, Sdsu_Cmd command);
  
  //+ Test that a DSP command is sent and acknowledged
  // > name - string to be used to idenitfy command name in report
  // > board_id - identifies board that command is sent to
  // > command - the SDSU command to be tested
  // > val - parameter that the SDSU command takes
  virtual void test_command(const char *name, int board_id, Sdsu_Cmd command, int val);

  //+ Test that a DSP command is sent and acknowledged
  // > name - string to be used to idenitfy command name in report
  // > board_id - identifies board that command is sent to
  // > command - the SDSU command to be tested
  // > val1 - first parameter that SDSU command takes
  // > val2 - second parameter that SDSU command takes
  virtual void test_command(const char *name, int board_id, Sdsu_Cmd command, int val1, int val2);

  //+ Test the DSP commands that are supported by code loaded from boot ROM
  virtual void test_boot_commands();

  //+ Test DSP commands controlling the coadder board
  virtual void test_coadder_commands();

  //+ Test the DSP commands controlling the timing board
  virtual void test_timing_board_commands();

};

#endif //_SDSU2_CONTROLLER_H
