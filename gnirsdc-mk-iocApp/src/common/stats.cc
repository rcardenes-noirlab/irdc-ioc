/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  stats.cc
 * 
 * GENERAL DESCRIPTION
 *  General image stats routines
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 2000
 *
 * HISTORY
 *   June 2000 PJY    Created for RSAA CICADA package
 *   June 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */

#include <math.h>
#include "stats.h"
#include "utility.h"
#include "exception.h"

/*
 *+ 
 * FUNCTION NAME: poidev
 * 
 * INVOCATION: poidev(float xm)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > xm - average number of successes occurring
 * 
 * FUNCTION VALUE: float
 * 
 * PURPOSE: Calculates poisson statistics
 * 
 * DESCRIPTION: Computes Poisson distributed random deviates
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * random number generator ia assumed to have been seeded with a call to srand
 * Actually, rand is not used anymore, so the call to srand is unnecessary
 * In fact, srand is MT-Unsafe (just like rand)
 * 
 * DEFICIENCIES: 
 *
 * ACKNOWLEDGEMENT:
 * Following two functions from numerical recipes.
 * Copyright (C) 1986-1997 Numerical Recipes Software.
 *- 
 */
float gammln(float xx);
float poidev(float xm)
{
  float gammln(float xx);
  static float sq,alxm,g,oldm=(-1.0);
  float em,t,y;
  
  if (xm < 12.0) {
    if (xm != oldm) {
      oldm=xm;
      g=exp(-xm);
    }
    em = -1;
    t=1.0;
    do {
      em++;
      t = t * RAND;
    } while (t > g);
  } else {
    if (xm != oldm) {
      oldm=xm;
      sq=sqrt(2.0*xm);
      alxm=log(xm);
      g=xm*alxm-gammln(xm+1.0);
    }
    do {
      do {
	y=tan(M_PI*RAND);
	em=sq*y+xm;
      } while (em < 0.0);
      em=floor(em);
      t=0.9*(1.0+y*y)*exp(em*alxm-gammln(em+1.0)-g);
    } while (RAND > t);
  }
  return em;
}
// compute the logarithm of gamma function
float gammln(float xx)
{
  double x,y,tmp,ser;
  static double cof[6]={76.18009172947146,-86.50532032941677,
			24.01409824083091,-1.231739572450155,
			0.1208650973866179e-2,-0.5395239384953e-5};
  int j;
  
  y=x=xx;
  tmp=x+5.5;
  tmp -= (x+0.5)*log(tmp);
  ser=1.000000000190015;
  for (j=0;j<=5;j++) ser += cof[j]/++y;
  return -tmp+log(2.5066282746310005*ser/x);
}

/* 
 *+ 
 * FUNCTION NAME: gauss_rand
 * 
 * INVOCATION: gauss_rand(double mean, double std_dev);
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > mean - the mean of the distribution
 * > std_dev - standard deviation of the distribution 
 *
 * FUNCTION VALUE: double
 * 
 * PURPOSE: returns a number picked randomly with a 
 * normal (gaussian) probability distribution with a
 * mean of "mean" and a standard deviation of "std_dev".
 * 
 * DESCRIPTION: 
 * Uses the Box-Muller method.  The Box Muller transform takes a 
 * pair of uniformly distributed random numbers (r and phi) and generates 
 * another pair.  Here we only ever use one so, if needed, this
 * function could be made more efficient by using both possible
 * numbers.
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * random number generator ia assumed to have been seeded
 * 
 * DEFICIENCIES: 
 *
 * ACKNOWLEDGEMENT:
 * 
 *-
 */
double gauss_rand(double mean, double std_dev) 
{
  // pick two numbers between [0,1)
  double r = RAND;
  double phi = RAND;
  // perform the transform
  double z_0 = cos( 2*M_PI*phi )*sqrt( -2.0*log(r) );
  return std_dev*z_0 + mean;
}

