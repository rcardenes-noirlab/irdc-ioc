/*
 * Copyright (c) 1994 - 2002 by MSSSO Computing Section 
 *
 * CICADA PROJECT
 *
 * FILENAME dummy_controller.h
 * 
 * PURPOSE
 * Dummy hardware data structures, and class definitions
 *
 */
#ifndef _DUMMY_CONTROLLER_H
#define _DUMMY_CONTROLLER_H

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#include "controller.h"
#include "fits.h"
#include "image.h"
#include <map>


/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/
// TODO: Determine a better place for this
#define DUMMY_DIR "cnf/dummy/"          // Default dummy configuration directory

const char GENERATE_DATA[] = "GENERATE_DATA"; // Arbitrary string to use to signal dummy 
const unsigned short BIAS_CONST=1000;   // Dummy bias value
const int DEFAULT_DUMMY_STEPS = 10;     // Number of steps in init



/*--------------------------------------------------------------------------
 Typedefs 
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Global variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Class declarations 
 -------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
CLASS
    Dummy_Controller

DESCRIPTION  
    The Dummy_Controller class allows creation of dummy controllers
    for simulation of actual hardware during testing of modules of Cicada.
 
KEYWORDS
    Dummy_Controller, Controller, Hardware.

-------------------------------------------------------------------------*/
class Dummy_Controller: public Controller {

public:

  static const std::string PARAMETER_NAME;

  //+ Basic constructor - used for setting up configuration.
  // > cam   - Camera_Desc - description of the camera
  // > camid - camera id
  // > c     - controller id
  Dummy_Controller(Camera_Desc& cam, int camid, int c, Platform_Specific* ps);

  //+ Midlevel constructor - used for accessing parameter information.
  // > cam   - Camera_Desc - description of the camera
  // > camid - camera id
  // > c 	  - controller id
  // > param_host - host on which the parameter database resides
  // > st	- status memory for this instrument
  // > pm	- parameter map
  Dummy_Controller(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st,
                   Platform_Specific* ps);

  //+ Full constructor.
  // > st  - status memory for this instrument
  // > ip  - instrument status parameter object
  // > cp  - camera status parameter object
  // > op  - ccd status parameter object
  // > deb - debug level
  Dummy_Controller(Instrument_Status* st, Parameter* ip, Parameter* cp, Parameter* op, int deb, Platform_Specific* ps);

  //+ Destructor
  virtual ~Dummy_Controller();


  // GROUP: public member functions

  //+ Return hardware configuration data
  // > read_configuration -
  virtual Controller_Config* getconfig(int read_configuration);


  //+ Do hardware initialisation 
  // > init_rec - initialisation record.
  virtual void init(Init init_rec);

  //+ Do a controller reset
  virtual void controller_reset();

  //+ Do a controller test
  virtual void controller_test(int ntests=10);

  //+ Do a controller shutdown
  virtual void controller_shutdown();

  //+ Reset detector hardware.
  //  > reset - reset parameters.
  //  > exp - reset in an exposure sequence.
  virtual void reset_detector(Detector_Reset reset, bool exp = false);

  //+ Prepares system for an exposure.
  // > ns - number of controllers to sync.
  virtual void prepare_exposure(int ns = 1);


  // Public clockout methods

  //+ initialise readout parameters and sets exposure time.
  // > s - sample number.
  // > r - read number.
  virtual void init_readout(int s, int r);

  //+ Initialise things for a dummy camera clockout.
  virtual void init_clockout();

  //+ Clockout a detector using the regions spec supplied.
  virtual void clockout();

  //+ Finish up the clockout.
  virtual void finish_clockout();

  //+ Creates an array of local Image objects from the input Fits image.
  //  This allows manipulation of the image contents without
  //  disturbing the fits file on disk.
  //  
  // PRIOR REQUIREMENTS: 
  //  Store the input file name in dummy_init.config_data.sim_image.
  void create_dummy_image();

  // GROUP: Dummy_Controller public Data members / Variables.

  Dummy_Init dummy_init;                //+ dummy init stuff - moved here to allow testing.
  Dummy_Config dummy_config;            //+ Currently used settings and configuration
  Filenametype dummy_config_file;       //+ Name of config file 
  Dummy_Readout dummy_readout;          //+ Readout spec
  Dummy_Status *dummy_status;           //+ Specific Dummy status
  Parameter *dummy_statusp;             //+ Cicada parameter status database for dummy
//  Fits *fits_image;                     //+ Fits image to read data from
  bool use_fits;                        //+ Set when using FITS files to simulate readout
  bool got_bias;                        //+ Set when using FITS files to simulate readout and bias estimate needed
  double    ctime;                      //+ Cumulative clocking time
  epicsTime clockout_stime;             //+ Clockout start time 
  int sleeptime;                        //+ Cumulative sleep time (msec)

protected:
  // GROUP: Protected member functions

  //+ initialise class member variables - called from constructors
  void init_vars();


  //+ Set up the readout of a detector, if successful return true.
  // > ns - number of controllers to sync.
  // > readout_rec - readout request structure.
  // > trgn - readout regions object.
  // ! isize - total bytes in readout 
  //   isize may be modified by controller specific req.
  // > exptime - last exposure time.
  virtual void setup_readout(int ns, Detector_Readout* readout_rec, Detector_Regions* trgn,
		  	     IR_Exposure_Info& ir_einfo, unsigned long& isize, 
			     double exptime);

  //+ Takes a copy of exposure info and calls generic routine to start
  //  thread for an IR camera.
  // > expose - pointer to exposure info.
  // > ir_tt - pointer to thread monitoring IR exposure timing
  virtual void start_ir_image_transfer_thread(IR_Expose& expose, Thread *ir_tt);

  //+ Starts image transfer thread if any readout region specified.
  virtual void start_image_transfer_thread();

  //+ Actions necessary before reading a row region.
  //  Nothing required for Dummy.
  //  > rr - row number of region.
  virtual void start_row_region(int rr);

  //+ Waits on thread to complete a segment readout.
  // DESCRIPTION: Looks at status of image transfer by monitoring 
  // . number of rows processed
  // . if image transfer is still going
  // . not aborted
  // > rows_processed - number of rows expected so far
  // > row - row number of region
  // > rowbytes - number of bytes per row
  // ! rt - current count of total rows transfered in readout
  virtual void wait_segment_readout(int rows_processed, int row, int rowbytes, int& rtfr);

  //+ Returns the number of readouts completed.
  virtual long get_readouts_done();

  //+ Returns the number of current readout.
  virtual long get_read_running();

  //+ Returns the full readout status
  virtual void get_readout_status(int rowbytes);

  //+ Stops the readouts by setting the stopt_readout flag.
  virtual void do_stop_readout();

  //+ Finish up after the readout.
  virtual void finish_readout();

  //+ Determine if data is to be generated or obtained from 
  //  FITS image.  Moved to Protected part to allow testing.
  void setup_sim_data();

  // Variables for reading a fits image to simulate an image.
  string image_file;                    //+ Name of Fits image
//  Image *in_img[MAX_AMPS];              //+ input image for each segment of a MEF.
//  Image *cosimg[MAX_AMPS];              //+ Cosmic ray image for each detector segment


private:
  // GROUP: Private member functions

  //+ Adds all dummy status parameters to the parameter database.
  // Uses the Parameter class to handle any Cicada status parameters. This class
  // does immediate write-thru operations to the Cicada status shared memory on
  // the observer computer any where on the net.
  //  > init - flag set if required to initialise added parameters
  void add_status_parameters(bool init);

  //+ Uses the supplied exposure time to calculate dummy
  //  image exptime rate and an estimation of the cosmic ray image.
  // > exptime - exposure time in msecs.
  // > inc_exptime - exposure time since last readout
  void setup_dummy_exposure(double exptime, double inc_exptime);

  //+ Copy pixels from FITS file.
  //  > row - row number to clockout.
  void clockout_fits_pixels(int row);


  //+ Clockout an individual row.
  //  > row_region - number of the row_region being worked on
  //  > row - row number to clockout
  void clockout_row(int row_region, int row);

  virtual epicsTime start_exposure() {
	  return exposure_clock.to_epicsTime(exposure_clock.set_period_start());
  }

  virtual double exposure_lapsed() { return 0; }

  // Do nothing!
  virtual size_t dump_to(FILE *dest) { return 0; }


  char *bd, *bdp;                     //+ data pointers used to handle different image data types
  short *sd, *sdp;
  int *id, *idp;
  float *fd, *fdp;
  double *dd, *ddp;

  //+ Private vars used during clockout
  char *bop;                            //+ pointer to output pixels - bytes
  short *sop;                           //+ pointer to output pixels - shorts
  unsigned short *uop;                  //+ pointer to output pixels - unsigned shorts
  int *iop;                             //+ pointer to output pixels - ints
  unsigned int *uiop;                   //+ pointer to output pixels - unsigned ints
  float *fop;                           //+ pointer to output pixels - floats
  double *dop;                          //+ pointer to output pixels - doubles
  int n,ln;                             //+ output pixel counters
  int xo,yo;                            //+ x and y offsets to data
  int width,height;                     //+ width and height of data
  int height_b;                         //+ binned height of data
  int rcf,ccf;                          //+ x and y binning
  int swidth;                           //+ total serial region width
  int swidth_b;                         //+ total binned serial region width
  int nser,npar;                        //+ number of serial and parallel regions
  int nsoverscan,npoverscan;            //+ total serial and parallel overscan - all amps
  int soverscan;                        //+ serial overscan per amp
  float exptime_scale;                  //+ Scale factor based on actual exptime
  unsigned short bv[MAX_AMPS];          //+ Preset bias value for each amp
  unsigned short dummy_val;             //+ used when generating pixels values
  unsigned short bias_val;              //+ calculated bias_val to use for overscan
  unsigned short rinc;                  //+ increment between readouts for generated data
  bool sim_data_ready;                  //+ Set when simulation data setup



}; // end class Dummy_Controller

/* Global function declarations */
//+ Make a dummy controller based on fits file
// > image_file - name of image file to use for simulation data
// > fits_image - name of fits image opened from image file
/* TODO: Redo this...
// Controller_Desc setup_dummy_controller(char* image_file, Fits* fits_image);
*/
Controller_Desc setup_dummy_controller(char* image_file);

//+ The thread that handles a clockout
// > arg - thread argument
extern "C" void* dummy_image_transfer_thread(void *arg);

//+ Setup pathname from input parts
// > name - name used to create pathname
// > chip_name - name of chip used in sim
char* dummy_path(const char* name, const char* chip_name);

//+ Routine to perform simple dummy controller tests
void dummyControllerTest();

#endif //_DUMMY_CONTROLLER_H 
