/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_slave.cc,v 1.3 2006/08/17 20:05:04 gemvx Exp $
 * GEMINI PROJECT
 *
 * FILENAME dc_slave.cc
 *
 * GENERAL DESCRIPTION
 *
 * This file implements the DC slave task. It is started by the control SNL task
 * "dc_ss" in response to an INIT command. After starting it will instantiate a
 * camera object based on the value of current initialisation parameters. Once
 * started the camera objects init method is invoked to perform necessary camera
 * initialisation.  The task then awaits further commands from control via the
 * slave message queue.
 *
 * $Log: dc_slave.cc,v $
 * Revision 1.3  2006/08/17 20:05:04  gemvx
 * - INHERIT keyword now set to F
 * - updated ipc (Thread) src to eliminate a deadlock we were seeing.
 * - updated grating constant to properly handle offsetting.
 * - moved many keywords from extended header to primary
 * - grating luts
 *
 * Revision 1.2  2006/07/18 21:06:59  gemvx
 * - added a debug message when a slave_thread thread needs to be deleted
 *
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.28  2005/09/26 04:56:10  pjy
 * Used << operator overload for debugging messages for enums.
 *
 * Revision 1.27  2005/09/24 00:48:56  pjy
 * Added support for extra debugging levels
 *
 * Revision 1.26  2005/09/23 01:52:11  pjy
 * Moved stop_view_mode functionality to SNL
 * Removed fatal error if ir_setup not saved
 * Added more debugging
 * Set comms=true after sdsu init
 *
 * Revision 1.25  2005/09/15 22:44:07  pjy
 * Fixed delete calls - made sure set to NULL.
 *
 * Revision 1.24  2005/09/11 22:52:05  pjy
 * Strings for enum types in messages
 *
 * Revision 1.23  2005/09/09 18:39:00  pjy
 * Changed ALWAYS_LOG to DEBUG_MIN for abort and stop reception
 *
 * Revision 1.22  2005/09/08 23:29:07  pjy
 * Fixed messages to string par
 * Only clear slave health when a successful command
 *
 * Revision 1.21  2005/09/06 22:46:58  pjy
 * Handle stop view mode when using SDSU commands
 * Clear statusMsgs when successful command
 *
 * Revision 1.20  2005/08/18 04:03:09  pjy
 * Fixed health handling (use SNL code)
 *
 * Revision 1.19  2005/08/02 05:42:12  pjy
 * Fix for health check - take 2
 *
 * Revision 1.18  2005/08/02 04:54:53  pjy
 * Check health before observe
 * Use simulation to set test patterns
 * Check controller type before using
 *
 * Revision 1.17  2005/07/29 23:54:18  pjy
 * Added controller_info
 *
 * Revision 1.16  2005/07/28 09:48:02  pjy
 * Added sdsu_ntests
 * Set health to warning when shutdown controller
 *
 * Revision 1.15  2005/07/14 06:56:41  pjy
 * Send endobserve message when done
 *
 * Revision 1.14  2005/07/12 07:12:57  pjy
 * sdsu aborting->canceling
 * added verify
 * removed data dest strings
 * removed calibrate frame read
 * debugged abort
 * removed nref and npixReads
 * moved test pattern params to readout req
 *
 * Revision 1.13  2005/05/18 02:06:37  pjy
 * Added cosmMin threshold parameter
 *
 * Revision 1.12  2005/04/27 03:51:21  pjy
 * Renamed sdsu_health to camera_health
 * On exception force viewEnable to false
 * Get test pattern params from status rather than par db
 *
 * Revision 1.11  2005/02/25 04:52:58  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.10  2004/12/23 22:46:40  pjy
 * Fixed calibrate frame read time code
 *
 * Revision 1.9  2004/12/22 06:14:44  pjy
 * DC slave src files ready for first Gemini release
 *
 * Revision 1.8  2004/08/22 23:36:20  pjy
 * Removed trigraph...
 *
 * Revision 1.7  2004/08/22 23:22:51  pjy
 * Fixed data type error for dc_slave_state put
 *
 * Revision 1.6  2004/08/20 02:37:29  pjy
 * Gem 8.6
 * Testing as at August 2004
 *
 * Revision 1.5  2004/04/25 02:28:12  pjy
 * Pre T2.2 version
 *
 * Revision 1.4  2003/09/19 06:24:28  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.3  2003/03/25 07:20:09  pjy
 * Removed nifs_readout, added fits_xfer
 *
 * Revision 1.2  2003/03/01 06:17:23  pjy
 * Progress up to NIFS fires - begginning of sdsu VME tests.
 *
 * Revision 1.1  2001/10/19 00:31:01  pjy
 * Added for NIFS DC
 *
 *
 */

#include <unistd.h>
#include "dc.h"
#include "logging.h"
#include "epicsParameterDB.h"
#include "parameter.h"
#include "ipc.h"
#include "utility.h"
#include "common.h"
#include "configuration.h"
#include "ir_camera.h"
#include "ir_setup.h"
#include "sdsu3_5_controller.h"
#include "config_table.h"

using std::shared_ptr;

// TODO: Deal with the threads and define this

// Constants
const int SPAWN_TASK_PRIORITY = epicsThreadPriorityScanLow;                        //+ In VxWorks it was 100
const int SPAWN_TASK_STACK = epicsThreadGetStackSize(epicsThreadStackMedium);      //+ Size of slave task stack
const int VIEW_MODE_STOP_TIMEOUT = 10*SECOND;     //+ Timeout for stopping view mode

// Globals
//+ Parameter databases
extern ParameterDB *dc_status_db;                 //+ EPICS interface to the SAD
extern Parameter *dc_status;                      //+ Internal status parameters
extern ParameterDB *dc_par_db;                    //+ EPICS interface to the CAD pars
extern Parameter *dc_par;                         //+ Internal control parameters

//+ Data buffers
// (NB dont call this shared_image - for some reason this breaks in the camera class)
extern Detector_Image *shared_image_buffer;       //+ image transfer buffer for camera parts

extern float* dc_variance[4];                     //+ variance data array
extern unsigned char* dc_quality[4];              //+ quality data array
extern float* dc_ref_data[4];                     //+ reference pixel array

//+ Control semaphores
static shared_ptr<Semaphore> sem_slave_ready;     //+ Semphore indicating slave is ready
//static Semaphore *sem_data_ready;               //+ Semphore indicating data ready for transfer

//+ State
extern DC_State dc_state;                         //+ DC global state
extern DC_State dc_slave_state;                   //+ state of slave task
extern Debug_Level debug_mode;                    //+ DC debugging mode
extern Nametype sim_mode_str;                     //+ simulation mode as seen in EPICS DB
extern IR_Mode ir_mode;                           //+ Mode of current observation OBSERVE or VIEW
extern bool aborting;                             //+ flag set if OBSERVE is aborted
extern bool sdsu_canceling;                        //+ flag set if SDSU ops canceled
extern bool stopping;                             //+ flag set if OBSERVE is stopped
extern bool sdsu_init_done;                       //+ flag set if sdsu init complete
extern bool sdsu_cmd_running;                     //+ flag set if sdsu command active
extern bool camera_setup_changed;                 //+ Flag set when any readout setting change

//+ SDSU DSP command SIRS
extern long sdsu_board;                           //+ SDSU direct command board id
extern long sdsu_cmd;                             //+ SDSU direct command
extern long sdsu_mem_space;                       //+ SDSU direct command memory space
extern long sdsu_mem_addr;                        //+ SDSU direct command memory address
extern long sdsu_arg1;                            //+ SDSU direct command argument 1
extern long sdsu_arg2;                            //+ SDSU direct command argument 2
extern int sdsu_debug;                            //+ SDSU debug command argument
extern int sdsu_ntests;                           //+ SDSU test command argument

//+ Configuration, status, control and reporting structures
extern Nametype configName;                       //+ Name of camera configuration
extern Instrument_Status *inst_status;            //+ Full instrument status - local copy
extern Config *shared_config;                     //+ Full camera configuration - read from disk
//extern Message *dc_message;                       //+ Message andling object
//extern Report *dc_report;                         //+ Reporting object
extern Verify_Spec verify_spec;                   //+ Readout verification/test pattern spec
extern bool do_verify;                            //+ Do readout verification

//+ DHS info
extern DC_Dest data_destination;                  //+ Destination of data - either dhs, fits or none

extern Hostnametype dhs_server_ip;                //+ DHS server IP address
extern Hostnametype dhs_server_name;              //+ DHS server host name
extern Nametype obs_ql_stream;                    //+ DHS Quick Look stream in use for obs mode
extern Nametype view_cmp_ql_stream;               //+ DHS Quick Look stream in use for obs mode compressed data
extern Nametype view_ql_stream;                   //+ DHS Quick Look stream in use for view mode
extern char *dc_qls_ptr[MAX_QLS];                 //+ Ptr to DHS Quick Look streams in use
extern int n_qlstreams;                           //+ Number of QL streams in use

//+ FITS server info
extern long fits_server_port;                     //+ FITS server socket port number
extern Hostnametype fits_server_name;             //+ FITS server host name

//+ System health records
extern Nametype dc_health;                        //+ DC health state
extern EPICS_String dc_health_msg;                //+ DC health state message
extern Nametype dhs_health;                       //+ DHS health state
extern EPICS_String dhs_health_msg;               //+ DHS health state message
extern Nametype fits_health;                      //+ FITS server health state
extern EPICS_String fits_health_msg;              //+ FITS server health state message
extern Nametype data_health;                      //+ Data task health state
extern EPICS_String data_health_msg;              //+ Data task health state message
extern Nametype slave_health;                     //+ Slave task health state
extern EPICS_String slave_health_msg;             //+ Slave task health state message
extern Nametype camera_health;                    //+ Camera health state
extern EPICS_String camera_health_msg;            //+ Camera health state message

//+ The input setup object
extern IR_Setup* ir_setup;                        //+ Used for retrieving and storing setup information from disk

//+ The camera and its configuration
extern IR_Camera *ir_camera;                      //+ The infra red camera object

//+ Local file variables
static shared_ptr<Mailbox> slave_mbox;            //+ Slave task message queue
static shared_ptr<Mailbox> data_mbox;             //+ data task message queue
static int debug_mask = 0;                        //+ mask for switching in debugging detail - works with CICS debug_mode
static bool test_mode = false;                    //+ Simple flag to set if just testing command reception

// Global functions
void dc_slave(void *);
void dc_slave_command(DC_Slave_Msg *smsg);
void save_camera_setup();

//+ Local Functions
static void dc_setup_observe_req(IR_Request& ir_req);
static void dc_setup_init_req(Init& init_req);
static void dc_camera_init();
static void dc_update_config_sirs();
static void do_controller_command(DC_Cmd cmd);
static void setup_debugging();

//+ Class declarations

/*
 *+
 * CLASS
 *   SlaveCommandThread
 *   Thread subclass to execute DC Slave commands in the background
 * DESCRIPTION
 *   Commands can be executed synchronously or asynchronously. This
 *   class provides the means to perform async executions.
 *
 *-
 */

class SlaveCommandThread : public Thread {
public:
	SlaveCommandThread(DC_Slave_Msg msg, const string& name,
			unsigned stackSize, unsigned priority, bool dolock);
	virtual void run();
private:
	DC_Slave_Msg smsg;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
//    G L O B A L    F U N C T I O N S
//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *+
 * FUNCTION NAME: void dc_slave()
 *
 * INVOCATION: sp dc_slave
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: exit status
 *
 * PURPOSE: implement the DC Slave task
 *
 * DESCRIPTION: See above for a description of the DC Slave task
 *   Start --> Initialising state --> Ready state --cmd--> Preparing ---> Monitoring Cmd
 *                                     |--------------<---------------Done/Abort----|
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

void dc_slave(void *)
{
  int istat;                         // status
  DC_Slave_Msg smsg;                 // Message received
  Thread* slave_command = nullptr;      // Thread used to execute long commands (observe, controller init etc)

  try {
    logger::message(logger::Level::Full, "In dc_slave");

    dc_status->put(SLAVE_STATE, int(DC_State::DC_INITIALIZING));

    // Initialise HEALTH to good
    dc_status->put(CAMERA_OP, true);

    // Connect to slave mailbox and setup to recieve notification when messages arrive
    slave_mbox = Mailbox::get(DC::SLAVE_MBOX);

    // Connect to data mailbox and setup to recieve notification when messages arrive
    data_mbox = Mailbox::get(DC::DATA_MBOX);

    logger::message(logger::Level::Full, "Message queues connected");

    // Clear any messages in queue as mq_notify will do nothing while they are there
    while (slave_mbox->messages_waiting() > 0) {
      if ((istat = slave_mbox->get_message((char*)&smsg, sizeof(DC_Slave_Msg), IPC_NOWAIT)) == IPC_ERR) {
        if (istat != EAGAIN) {
          throw Error("mq_receive failed while clearing slave mq!", E_ERROR, errno, __FILE__, __LINE__);
        }
      }
      logger::message(logger::Level::Full, "Read message %s", to_string(smsg.cmd).c_str());
    }

    logger::message(logger::Level::Full, "Messages cleared");

    dc_status->put(ABORTING, false);
    dc_status->put(STOPPING, false);
    dc_status->put(SDSU_CANCELING, false);
    dc_status->put(SDSU_INIT_DONE, false);

    // Give semaphore to indicate readiness to receive messages.
    // This semaphore created and empty from control task
    dc_status->put(SLAVE_STATE, int(DC_State::DC_SLAVE_WAITING));

    sem_slave_ready = Semaphore::get(DC::SLAVE_READY_SEM);

    logger::message(logger::Level::Full, "About to give sem_slave_ready");
    sem_slave_ready->give();
    logger::message(logger::Level::Full, "sem_slave_ready given");

    // Now wait for commands to be added to the slave message queue
    do {
      try {
        // Check for any requests from CONTROL
        logger::message(logger::Level::Full, "Waiting for command...");
        if (slave_mbox->get_message((char*)&smsg, sizeof(DC_Slave_Msg), IPC_WAIT)) {

                //pgroszko - don't want to print CONTROLLER_INFO...
                if (smsg.cmd != DC_Cmd::CONTROLLER_INFO)
                logger::message(logger::Level::Min, "dc_slave: Got request '%s'", to_string(smsg.cmd).c_str());

	  // TODO: This message has been here since the beginning of times. Maybe it should be removed????
          // Remove following after testing...
/*
          if (test_mode) {
            if (smsg.cmd == DC_Cmd::REBOOT)
              dc_status->put(SLAVE_STATE, int(DC_State::DC_STOPPED));
            else if (smsg.cmd == DC_Cmd::INIT)
              sem_slave_ready->give();
            continue;
          }
*/

          try {
            // Start a separate thread to execute the OBSERVE command
            switch (smsg.cmd) {
            case DC_Cmd::OBSERVE:
            case DC_Cmd::CONTROLLER_INIT:
              if (slave_command != nullptr) {
		   if (!slave_command->done()) {
		     // For all previous spawned commands wait to join here
		     logger::message(logger::Level::Min, "slave_command != nullptr: Previous command hasn't finished. Can't execute command %s right away. Waiting...", to_string(smsg.cmd).c_str());
		     slave_command->join();
		   }
                   delete slave_command;
                   slave_command = nullptr;
              }

              logger::message(logger::Level::Min, "Starting Thread for command %s", to_string(smsg.cmd).c_str());

              // Start a new thread so that this thread can listen for new commands.
	      slave_command = new SlaveCommandThread(smsg, "slave_thread",
			      		SPAWN_TASK_STACK, SPAWN_TASK_PRIORITY, true);
	      slave_command->start();
              logger::message(logger::Level::Min, "Thread started");
              logger::message(logger::Level::Full, "Request %s executing, state = %s", to_string(smsg.cmd).c_str(), to_string(dc_slave_state).c_str());
              break;
            default:
              dc_slave_command(&smsg);
              logger::message(logger::Level::Full, "Request %s done, state = %s", to_string(smsg.cmd).c_str(), to_string(dc_slave_state).c_str());
              break;
            }
          }
          catch (Error& dce) {
            logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
            dc_set_health(SLAVE_HEALTH, dce.type, SLAVE_HEALTH_MSG, slave_health_msg, (char*)dce.msg.c_str());
          }

        }
      }
      catch (Error& dce) {
        logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
        dc_set_health(SLAVE_HEALTH, dce.type, SLAVE_HEALTH_MSG, slave_health_msg, (char*)dce.msg.c_str());
      }

    } while (dc_slave_state != DC_State::DC_STOPPED);

    try {
      // Join any old thread to clean up
      if (slave_command != nullptr) {
        // For all previous command threads other than OBSERVE wait to join here
        delete slave_command;
        slave_command = nullptr;
        logger::message(logger::Level::Full, "Slave command thread deleted!");
      }
    }
    catch (Error& dce) {
      // Ignore any exceptions from trying to clear the message queue notifications
    }

    // Remove the mailboxes.
    // NOTE: Mailbox creation is now centralized, this WON'T delete the actual objects
    // TODO: Rethink the lifecycle of the managed objects.
    slave_mbox.reset();
    data_mbox.reset();
    logger::message(logger::Level::Full, "Mailboxes deleted!");

    if (ir_camera != nullptr) {
      delete ir_camera;
      ir_camera = nullptr;
      logger::message(logger::Level::Full, "IR camera deleted!");
    }

  }
  catch (Error& dce) {
    logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
    dc_set_health(SLAVE_HEALTH, dce.type, SLAVE_HEALTH_MSG, slave_health_msg, (char*)dce.msg.c_str());
  }
  logger::message(logger::Level::Full, "Slave task done!");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//    L O C A L    F U N C T I O N S
//////////////////////////////////////////////////////////////////////////////////////////////////////

SlaveCommandThread::SlaveCommandThread(DC_Slave_Msg msg, const string& name,
			unsigned stackSize, unsigned priority, bool dolock)
	: Thread(name.c_str(), priority, stackSize, dolock),
	  smsg(msg)
{}

void
SlaveCommandThread::run() {
	dc_slave_command(&smsg);
}

/*
 *+
 * FUNCTION NAME: void dc_slave_command(DC_Slave_Msg* smsg);
 *
 * INVOCATION: dc_slave_command(smsg);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > scmd - Slave command to execute
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Interprets and executes the given command
 *
 * DESCRIPTION: Calls the appropriate camera class method to execute the command.
 * Moves into MONITOR state for the observe command
 *
 * EXTERNAL VARIABLES: DC DC_Par setup parameters
 *
 * PRIOR REQUIREMENTS: This is called from DC_Slave in response to Gemini EPICS commands
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_slave_command(DC_Slave_Msg *smsg)
{
  IR_Request ir_req;                    // ir observe request data structure
  Init init_req;                        // Init request data structure
  DC_Data_Msg dmsg;                     // message structure for data task
  int t_ir_mode;                        // Used for stopping view mode
  string health_str, observe_msg, str;  // temporary strings

  logger::message(logger::Level::Full, "In dc_handle_command, cmd=%s", to_string(smsg->cmd).c_str());

  try {

    switch (smsg->cmd) {

    case DC_Cmd::REBOOT:
      if ((dc_slave_state != DC_State::DC_SLAVE_WAITING) && (ir_camera != nullptr)) {
        ir_camera->handle_reset();
      }

      logger::message(logger::Level::Full, "Rebooting - state = DC_STOPPED ");
      dc_status->put(SLAVE_STATE, int(DC_State::DC_STOPPED));
      break;

    case DC_Cmd::DEBUG:
      logger::message(logger::Level::Full, "DEBUG");
      if (ir_camera != nullptr) {
        setup_debugging();
        ir_camera->change_debug(debug_mask);
      }
      break;

    case DC_Cmd::STOP:
     // Set stop flag - then expect observe to finish normally at end of current NDR
      logger::message(logger::Level::Min, "STOP received - stopping");
      if ((dc_slave_state == DC_State::DC_SLAVE_MONITOR) && (ir_camera != nullptr)) {
        ir_camera->hardware_stop = true;
      }
      break;

    case DC_Cmd::ABORT:
      // Send reset to the camera - expect observe to then finish immediately
      logger::message(logger::Level::Min, "ABORT received - aborting");
      if ((dc_slave_state != DC_State::DC_SLAVE_WAITING) && (ir_camera != nullptr)) {
        logger::message(logger::Level::Min, "Sending a reset signal to the camera object");
        ir_camera->handle_reset();
      }
      break;

    case DC_Cmd::INIT:
      if ((dc_slave_state == DC_State::DC_SLAVE_WAITING) ||
          (dc_slave_state == DC_State::DC_SLAVE_READY)) {
        dc_status->put(SLAVE_STATE, int(DC_State::DC_INITIALIZING));

        dc_camera_init();

        logger::message(logger::Level::NoLog, "%s IR_Camera available.", shared_config->config_data.Config_Data_u.camera.desc.name);
        dc_status->put(SLAVE_STATE, int(DC_State::DC_SLAVE_READY));

        logger::message(logger::Level::Full, "About to give sem_slave_ready");
        sem_slave_ready->give();
        logger::message(logger::Level::Full, "sem_slave_ready given");
      } else
        logger::message(logger::Level::NoLog, "IR Camera Slave not READY or WAITING (%d)??? !", dc_slave_state);
      break;

    case DC_Cmd::OBSERVE:
      dc_status->get(DC_HEALTH, dc_health, 0, NAMELEN);
      health_str = dc_health;
      observe_msg = "";
      if (dc_slave_state == DC_State::DC_SLAVE_MONITOR) {
        observe_msg = "DC Slave OBSERVING - please wait!";
      } else if (health_str != "GOOD") {
        observe_msg = "DC health not GOOD - please fix!";
      } else if (dc_slave_state != DC_State::DC_SLAVE_READY) {
        observe_msg = "DC Slave not READY - send INIT first!";
      } else if (ir_camera == nullptr) {
        observe_msg = "IR Camera not READY - send INIT first!";
      }

      if (observe_msg != "") {
        logger::message(logger::Level::NoLog, observe_msg.c_str());
        dc_status->put(CAD_START_MSG, observe_msg);
        // Let control know that observing is done - do this by setting the ir observing mode flag
        ir_camera->ir_statusp->put(IR_MODE, int(IR_NULL_MODE));
        break;
      } else try {

        logger::message(logger::Level::Min, "dc_slave: IR Camera preparing for exposure...");

        // Must let data task know that we are starting  the observation
        dmsg.cmd = DC_Cmd::OBSERVE;
        data_mbox->send_message((char*)&dmsg, sizeof(DC_Data_Msg));
        logger::message(logger::Level::Min, "dc_slave: Sent %s message to data", to_string(dmsg.cmd).c_str());

        dc_status->put(SLAVE_STATE, int(DC_State::DC_SLAVE_MONITOR));
        ir_camera->clear_reset();

        // Setup the request structure from the EPICS parameters
        logger::message(logger::Level::Full, "dc_slave: IR Camera setting up observe request...");
        dc_setup_observe_req(ir_req);

        // Setup exposure parameters
        logger::message(logger::Level::Full, "dc_slave: IR Camera calling prepare exposure...");
        ir_camera->controller->prepare_exposure();

        // Run the IR exposure
        logger::message(logger::Level::Full, "dc_slave: IR Camera starting exposure...");

        ir_camera->exposure(ir_req.IR_Request_u.expose);

        logger::message(logger::Level::Min, "dc_slave: IR Camera exposure done.");

        // Dump the message buffer?
	/* TODO: Figure out what to do with messages
        if (debug_mask < 0) {
          unlink("dc_message.dump");
          dc_message->dump("dc_message.dump");
        }
	*/

        // Save current settings for next setup. This saves the engineer from
        // resetting all input parameters next time the systems starts up
        // pgroszko - lets disable.
        //if (camera_setup_changed)
        //  save_camera_setup();

        // Must let data task know that we have finished the observation
        dmsg.cmd = DC_Cmd::ENDOBSERVE;
        data_mbox->send_message((char*)&dmsg, sizeof(DC_Data_Msg));
        logger::message(logger::Level::Min, "Sent %s message to data", to_string(dmsg.cmd).c_str());

        // Command completed successfully - clear status messages
        update_status_msg(" ");
        dc_set_health(SLAVE_HEALTH, E_OK, SLAVE_HEALTH_MSG, slave_health_msg, " ");
        dc_status->put(CAMERA_OP, true);
      }
      catch (Error& dce) {
         logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
         dc_status->put(CAMERA_OP, false);
         dc_status->put(CAMERA_OP_MSG, dce.msg.c_str(), 0, min(int(dce.msg.length()),MAX_STRING_SIZE));

         // If in View mode then set view enabled to false - ie stop viewing
        ir_camera->ir_statusp->get(IR_MODE, t_ir_mode);
        if ((IR_Mode) t_ir_mode == IR_VIEW_MODE)
          ir_camera->ir_statusp->put(VIEW_ENABLED, false);

        // Must let data task know that we have exited
        dmsg.cmd = DC_Cmd::ABORT;
        data_mbox->send_message((char*)&dmsg, sizeof(DC_Data_Msg));
        logger::message(logger::Level::Min, "Sent %s message to data", to_string(dmsg.cmd).c_str());

      }

      // On our way out - clear abort  and stopping state
      // If we were aborting then reset the aborting flag so that the Control SNL can move on
      dc_status->get(ABORTING, aborting);

      if (aborting)
        // so clear the flag
        dc_status->put(ABORTING, false);

      dc_status->get(STOPPING, stopping);

      if (stopping)
        // so clear the flag
        dc_status->put(STOPPING, false);

      // Make sure the reset has been cleared
      ir_camera->clear_reset();

      dc_status->put(SLAVE_STATE, int(DC_State::DC_SLAVE_READY));

      // Let control know that observing is done - do this by setting the ir observing mode flag
      ir_camera->ir_statusp->put(IR_MODE, int(IR_NULL_MODE));

      break;

    case DC_Cmd::CONTROLLER_INIT:
      // Now expect to be in READY state before issuing an INIT SDSU command
      if (dc_slave_state == DC_State::DC_SLAVE_MONITOR) {
        logger::message(logger::Level::NoLog, "DC Slave OBSERVING - please wait!");
      } else if (dc_slave_state != DC_State::DC_SLAVE_READY) {
        logger::message(logger::Level::NoLog, "DC Slave not READY - send INIT first!");
      } else if (ir_camera == nullptr) {
        logger::message(logger::Level::NoLog, "IR Camera not READY - send INIT first!");
      } else {
        dc_status->put(SLAVE_STATE, int(DC_State::DC_SLAVE_CONFIGURE));

        try {
          dc_status->put(SDSU_INIT_DONE, false);
          dc_status->put(SDSU_CMD_RUNNING, true);

          // Set up an initialisation record
          dc_setup_init_req(init_req);

          // Initialise the camera
          ir_camera->init(init_req);

          // Now do the controller hardware initialisation
          ir_camera->controller->init(init_req);

          // Command completed successfully - clear status messages
          update_status_msg(" ");
          dc_set_health(SLAVE_HEALTH, E_OK, SLAVE_HEALTH_MSG, slave_health_msg, " ");
          dc_status->put(SDSU_INIT_DONE, true);
          dc_status->put(CAMERA_OP, true);
          dc_status->put(SDSU_COMMS, true);
        }
        catch (Error& dce) {
          logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
          dc_status->put(CAMERA_OP, false);
          dc_status->put(CAMERA_OP_MSG, dce.msg.c_str(), 0, min(int(dce.msg.length()),MAX_STRING_SIZE));
        }

        // On our way out - clear abort state
        // If we were canceling then reset the canceling flag so that the Control SNL can move on
        dc_status->get(SDSU_CANCELING, sdsu_canceling);

        if (sdsu_canceling)
          // so clear the flag
          dc_status->put(SDSU_CANCELING, false);

        ir_camera->controller->clear_reset();

        dc_status->put(SDSU_CMD_RUNNING, false);
        dc_status->put(SLAVE_STATE, int(DC_State::DC_SLAVE_READY));
      }
      break;

      // Following are all specialist controller commands - setup in different ways
    case DC_Cmd::CONTROLLER_SHUTDOWN:
    case DC_Cmd::CONTROLLER_TEST:
    case DC_Cmd::CONTROLLER_RESET:
    case DC_Cmd::CONTROLLER_DEBUG:
    case DC_Cmd::CONTROLLER_SIMULATE:
    case DC_Cmd::CONTROLLER_CMD:
    case DC_Cmd::CONTROLLER_INFO:
      if (dc_slave_state == DC_State::DC_SLAVE_MONITOR) {
        logger::message(logger::Level::NoLog, "DC Slave OBSERVING - please wait!");
      } else if (dc_slave_state != DC_State::DC_SLAVE_READY) {
        logger::message(logger::Level::NoLog, "DC Slave not READY - send INIT first!");
      } else if (ir_camera == nullptr) {
        logger::message(logger::Level::NoLog, "IR Camera not READY - send INIT first!");
      } else try {
        dc_status->put(SLAVE_STATE, int(DC_State::DC_SLAVE_BUSY));
        dc_status->put(SDSU_CMD_RUNNING, true);

        do_controller_command(smsg->cmd);

        // Command completed successfully - clear status messages
        if (smsg->cmd != DC_Cmd::CONTROLLER_INFO) {
          update_status_msg(" ");
          dc_set_health(SLAVE_HEALTH, E_OK, SLAVE_HEALTH_MSG, slave_health_msg, " ");
        }
        dc_status->put(CAMERA_OP, true);
      }
      catch (Error& dce) {
        logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
        dc_status->put(CAMERA_OP, false);
        dc_status->put(CAMERA_OP_MSG, dce.msg.c_str(), 0, min(int(dce.msg.length()),MAX_STRING_SIZE));
      }
      dc_status->put(SDSU_CMD_RUNNING, false);
      dc_status->put(SLAVE_STATE, int(DC_State::DC_SLAVE_READY));
      break;

    case DC_Cmd::CONTROLLER_CANCEL:
      logger::message(logger::Level::NoLog, "Controller CANCEL received - canceling");
      if ((dc_slave_state != DC_State::DC_SLAVE_BUSY) &&
          (dc_slave_state != DC_State::DC_SLAVE_CONFIGURE)) {
        logger::message(logger::Level::NoLog, "DC Slave controller not BUSY - cancel???");
      } else {
        if (ir_camera != nullptr) {
          logger::message(logger::Level::NoLog, "Sending a reset signal to the controller object");
          ir_camera->controller->handle_reset();
        }
      }
      break;

    default:
      logger::message(logger::Level::NoLog, "Unknown command (%s)???", to_string(smsg->cmd).c_str());
      break;
    }

  }
  catch (Error& dce) {
    logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
    dc_set_health(SLAVE_HEALTH, dce.type, SLAVE_HEALTH_MSG, slave_health_msg, (char*)dce.msg.c_str());
  }

  logger::message(logger::Level::Full, "In dc_handle_command, cmd=%s Done!", to_string(smsg->cmd).c_str());
}

 /*
 *+
 * FUNCTION NAME: dc_setup_observe_req
 *
 * INVOCATION: dc_setup_observe_req(IR_Request& ir_req)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! ir_req - infrared observation request data structure
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Gathers information from the parameter data base required to execute
 *          the observe command
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES: DC DC_Par setup parameters
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void dc_setup_observe_req(IR_Request& ir_req)
{
  char msgbuf[MSGLEN];
  int t_ir_mode;
  string mode_str,par,dest,str;
  bool flag;

  if (ir_camera == nullptr) {
    throw Error("Cannot setup observe req until camera object created!",
                E_ERROR, -1, __FILE__, __LINE__);
  }

  // Set local parameter ptr to cameras parameters
  Parameter *ir_statusp = ir_camera->ir_statusp;

  // Observing mode parameters
  ir_statusp->get(IR_MODE, t_ir_mode);
  ir_mode = (IR_Mode) t_ir_mode;
  ir_req.type = (ir_mode == IR_VIEW_MODE)? IR_CAMERA_VIEW: IR_CAMERA_EXPOSE;
  ir_req.IR_Request_u.expose.irMode = ir_mode;
  mode_str = (ir_mode == IR_VIEW_MODE)? "view_": "obs_";

  ir_statusp->get(VIEW_ENABLED, ir_req.IR_Request_u.expose.viewEnabled);

  // Data destination for chosen observing mode
  data_destination = DC_DEST_NONE;
  dest = "NONE";
  if (ir_mode == IR_OBSERVE_MODE) {
    dc_status->get(OBS_DATADEST, dest);
  } else {
    dc_status->get(VIEW_DATADEST, dest);
  }

  if (dest == "DHS")
    data_destination = DC_DEST_DHS;
  else if (dest == "FITS")
    data_destination = DC_DEST_FITS;
  logger::message(logger::Level::Min, "Data destination = %s", dest.c_str());

  // Reset information
/*
  ir_statusp->get(mode_str+NRESETS, ir_req.IR_Request_u.expose.reset.nresets);
  ir_statusp->get(mode_str+RESET_DELAY, ir_req.IR_Request_u.expose.reset.reset_delay);
*/

  // Exposure parameters - first the sequence set
  ir_req.IR_Request_u.expose.einfo.doReadout = TRUE;
  ir_req.IR_Request_u.expose.einfo.doReset = FALSE;

/*
  ir_req.IR_Request_u.expose.einfo.doReset = (ir_req.IR_Request_u.expose.reset.nresets > 0);
  ir_statusp->get(mode_str + SET_INTERVAL, ir_req.IR_Request_u.expose.einfo.setIntvl);
// Most probably we don't need most of these for the setup
  ir_statusp->get(mode_str + TIME_MODE, (long)ir_req.IR_Request_u.expose.einfo.timeModeSet);
  ir_statusp->get(mode_str + NFOWLER, ir_req.IR_Request_u.expose.einfo.nfowler);
  ir_statusp->get(mode_str + READ_TIME, ir_req.IR_Request_u.expose.einfo.readTime);
  ir_statusp->get(mode_str + READ_INTVAL, ir_req.IR_Request_u.expose.einfo.readIntvl);
  ir_statusp->get(mode_str + PERIOD, ir_req.IR_Request_u.expose.einfo.period);
  ir_statusp->get(mode_str + NPERIODS, ir_req.IR_Request_u.expose.einfo.nperiods);
  ir_statusp->get(mode_str + NCOADDS, ir_req.IR_Request_u.expose.einfo.ncoadds);
*/
  {
	  int rms;
	  ir_statusp->get(mode_str + IR_READ_MODE, rms);
	  ir_req.IR_Request_u.expose.einfo.readModeSet = static_cast<IR_Readmode_Type>(rms);
  }
  ir_statusp->get(mode_str + IR_EXPOSEDRQ, ir_req.IR_Request_u.expose.einfo.exposedRQ);

  // Now the data set
  mode_str = string("d_") + mode_str;
  ir_statusp->get(mode_str + DO_COSM_REJ, flag);
  ir_req.IR_Request_u.expose.dinfo.cosmRej = flag;
  ir_statusp->get(mode_str + COSM_THRESH, ir_req.IR_Request_u.expose.dinfo.cosmThrsh);
  ir_statusp->get(mode_str + COSM_MIN, ir_req.IR_Request_u.expose.dinfo.cosmMin);

  // Some OBSERVE mode only parameters
  if (ir_mode != IR_VIEW_MODE) {
    ir_statusp->get(mode_str + DO_SAVE, flag);
    ir_req.IR_Request_u.expose.dinfo.save = flag;
    ir_statusp->get(mode_str + DO_SAVE_NDR, flag);
    ir_req.IR_Request_u.expose.dinfo.saveNdrs = flag;
    ir_req.IR_Request_u.expose.dinfo.displayNdrs = ir_req.IR_Request_u.expose.dinfo.saveNdrs;
    ir_statusp->get(mode_str + DO_SAVE_COADD, flag);
    ir_req.IR_Request_u.expose.dinfo.saveCoadds = flag;
    ir_req.IR_Request_u.expose.dinfo.displayCoadds = ir_req.IR_Request_u.expose.dinfo.saveCoadds;
    ir_statusp->get(mode_str + DO_SAVE_VAR, flag);
    ir_req.IR_Request_u.expose.dinfo.saveVar = flag;
    ir_statusp->get(mode_str + DO_SAVE_QUAL, flag);
    ir_req.IR_Request_u.expose.dinfo.saveQual = flag;
    ir_req.IR_Request_u.expose.dinfo.doSubFile = FALSE;
    ir_req.IR_Request_u.expose.dinfo.doCmpIm = FALSE;
  } else {
    ir_req.IR_Request_u.expose.dinfo.saveNdrs = FALSE;
    ir_req.IR_Request_u.expose.dinfo.displayNdrs = FALSE;
    ir_req.IR_Request_u.expose.dinfo.saveCoadds = FALSE;
    ir_req.IR_Request_u.expose.dinfo.displayCoadds = FALSE;
    ir_statusp->get(mode_str + DO_SUBTRACT, flag);
    ir_req.IR_Request_u.expose.dinfo.doSubFile = flag;
    ir_statusp->get(mode_str + SUB_FNAME, ir_req.IR_Request_u.expose.dinfo.subFile, 0, FILENAMELEN);
    ir_statusp->get(mode_str + DO_CMP_IM, flag);
    ir_req.IR_Request_u.expose.dinfo.doCmpIm = flag;
    ir_statusp->get(mode_str + WAVE_MIN, ir_req.IR_Request_u.expose.dinfo.waveMin);
    ir_statusp->get(mode_str + WAVE_MAX, ir_req.IR_Request_u.expose.dinfo.waveMax);
  }

  // Now the readout parameters

  // Only do readout verification if using test patterns (ie sim mode)
  dc_status->get(SIM_MODE, sim_mode_str, 0, NAMELEN);
  str = sim_mode_str;
  verify_spec.do_test_pattern = ((str=="FAST") || (str=="FULL"));
  if (!verify_spec.do_test_pattern)
    do_verify = false;
  else {
    dc_status->get(TEST_PATTERN_VERIFY, flag);
    do_verify = flag;
  }
  ir_req.IR_Request_u.expose.readout.verify = int(do_verify);

  // Now override any config values with particular user options (ie from EPICS records)
  // verification parameters so far.
  // However this would allow each 'initialise controller' command to take new specific
  // configuration settings dynamically rather than from file
  dc_status->get(TEST_PATTERN_SEED, verify_spec.seed);
  dc_status->get(TEST_PATTERN_INC, verify_spec.increment);
  verify_spec.constant_val = (verify_spec.increment == 0);
  dc_status->get(TEST_PATTERN_AMP_INC, verify_spec.amp_increment);
  dc_status->get(TEST_PATTERN_READ_INC, verify_spec.readout_increment);
  dc_status->get(TEST_PATTERN_OS, verify_spec.overscan);

  ir_req.IR_Request_u.expose.readout.verify_spec = verify_spec;

  // The readout image information
  ir_req.IR_Request_u.expose.readout.image_info.unsigned_data = FALSE;
  ir_req.IR_Request_u.expose.readout.image_info.bitpix = FLOAT_BITPIX;
  ir_req.IR_Request_u.expose.readout.image_info.bscale = 1.0;
  ir_req.IR_Request_u.expose.readout.image_info.bscale = 0.0;

  // Now the readout region info - for DC the full detector - this is fixed;
  ir_req.IR_Request_u.expose.readout.regions.camera_coords = TRUE;
  ir_req.IR_Request_u.expose.readout.regions.nregions = 1;
  ir_req.IR_Request_u.expose.readout.regions.xo[0] = 0;
  ir_req.IR_Request_u.expose.readout.regions.yo[0] = 0;
  ir_req.IR_Request_u.expose.readout.regions.width[0] = ir_camera->controller[ir_camera->cid].controller.cols;
  ir_req.IR_Request_u.expose.readout.regions.height[0] = ir_camera->controller[ir_camera->cid].controller.rows;
  ir_req.IR_Request_u.expose.readout.regions.rcf = 1;
  ir_req.IR_Request_u.expose.readout.regions.ccf = 1;
  ir_req.IR_Request_u.expose.readout.regions.overscan_cols = 0;
  ir_req.IR_Request_u.expose.readout.regions.overscan_rows = 0;
  ir_req.IR_Request_u.expose.readout.regions.chip_mask = 1;
  strncpy(ir_req.IR_Request_u.expose.readout.regions.window,
          shared_config->config_data.Config_Data_u.camera.desc.name, FITSVALUELEN-1);

  // Now the chip readout information
  ir_req.IR_Request_u.expose.readout.data.type = ir_camera->controller[ir_camera->cid].controller.type;
  switch (ir_camera->controller[ir_camera->cid].controller.type) {
  case DUMMY:
    ir_req.IR_Request_u.expose.readout.data.Detector_Readout_Data_u.dummy.dummy = 0;
    break;

  case SDSU2:
  case SDSU3:
  case SDSU3_5:
    ir_req.IR_Request_u.expose.readout.data.Detector_Readout_Data_u.sdsu.read_mode = CHIP_READOUT_FAST;
    break;

  default:
    sprintf(msgbuf,"Detector controller of type %d unsupported?", ir_camera->controller[ir_camera->cid].controller.type);
    throw Error(msgbuf, E_ERROR, -1, __FILE__, __LINE__);
    break;
  } // switch for detector readout data

  logger::message(logger::Level::Full, "Observe request setup ok");
}
/*
 *+
 * FUNCTION NAME: dc_setup_init_req
 *
 * INVOCATION: dc_setup_init_req(Init& init_req)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! init_req - init request data structure
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Gathers information from the parameter data base required to execute
 *          the init command
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES: DC DC_Par setup parameters
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void dc_setup_init_req(Init& init_req)
{
  char msgbuf[MSGLEN];   // Message buffer

  // Use configuration parameters supplied with command
  // rather than from configuration shared memory
  init_req.use_config = FALSE;


  // camera init data
  init_req.camera.type = IR_CAMERA;

  // Read the controller configuration file - it may have been changed
  // Put it into the config shared memory location
  shared_config->config_data.Config_Data_u.camera.controller = *ir_camera->controller->getconfig(TRUE);

  // Controller init data
  init_req.controller.type = shared_config->config_data.Config_Data_u.camera.desc.controller[ir_camera->cid].type;

  // Load file configuration data into Init record
  switch (init_req.controller.type) {
  case DUMMY:
    init_req.controller.Controller_Init_Data_u.dummy.config_data =
      shared_config->config_data.Config_Data_u.camera.controller.Controller_Config_u.dummy.config_data;
    break;
  case SDSU2:
  case SDSU3:
  case SDSU3_5:
    init_req.controller.Controller_Init_Data_u.sdsu.config_data =
      shared_config->config_data.Config_Data_u.camera.controller.Controller_Config_u.sdsu.config_data;
    break;
  default:
    sprintf(msgbuf,"Detector controller of type %d unsupported?", ir_camera->controller[ir_camera->cid].controller.type);
    throw Error(msgbuf, E_ERROR, -1, __FILE__, __LINE__);
    break;
  }

}

/*
 *+
 * FUNCTION NAME: void dc_camera_init();
 *
 * INVOCATION: dc_camera_init();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Instantiates a new camera object
 *
 * DESCRIPTION:
 *
 * Uses the camera class to instantiate an object of the desired type. This may
 * be either a simulation camera or an interface to a real hardware camera - for
 * DC the SDSU IR camera.
 *
 * EXTERNAL VARIABLES: DC DC_Par setup parameters
 *
 * PRIOR REQUIREMENTS: This is called from DC_Slave once DC_Control has
 * initialised and in response to Gemini EPICS INIT command
 *
 * DEFICIENCIES:
 *
 *-
 */
static void dc_camera_init()
{
  int mode;
  Platform_Specific specific;         //+ platform specific data - for us nothing
  //Debug_Level saved_debug_mode;

  try {
    logger::message(logger::Level::Full, "In dc_camera_init...instantiating an IR_Camera");

    setup_debugging();

    logger::message(logger::Level::Full, "Shared image, address=0x%p", shared_image_buffer);

    logger::message(logger::Level::Full, "Shared config, address=0x%p", shared_config);

    if (ir_camera != nullptr) {
      delete ir_camera;
      ir_camera = nullptr;
    }

    ir_camera = new IR_Camera(inst_status, dc_status, debug_mask, &specific);

    dc_update_config_sirs();

    // Set the correct simulation mode
    dc_status->get(SIM_MODE, sim_mode_str, 0, NAMELEN);
    {
	    string str(sim_mode_str);
	    if (str=="FAST") mode=1;
	    else if (str=="FULL") mode=2;
	    else mode = 0;
    }
    ir_camera->controller->change_simulation_mode(mode);

    logger::message(logger::Level::Full, "In dc_camera_init...instantiation done ok.");

  }
  catch (Error &dce) {
    logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
    dc_status->put(CAMERA_OP, false);
    dc_status->put(CAMERA_OP_MSG, dce.msg.c_str(), 0, min(int(dce.msg.length()),MAX_STRING_SIZE));
    dce.rethrow();
  }

}

/*
 *+
 * FUNCTION NAME: void setup_debugging
 *
 * INVOCATION: setup_debugging
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sets up debugging
 *
 * DESCRIPTION:
 *
 * Checks the set debug mode - translates this to an equivalent camera
 * debug mask and creates the debugging report object if not already done
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void setup_debugging()
{
  // Need to translate CICS style debug_mode into a Camera class style debug mask
  // Would be much nicer if a mask could be set rather than a level - but
  // cannot be done with eh CICS model for debugging...
  switch (debug_mode) {
  case DEBUG_FULL:
  case ALWAYS_LOG:
    debug_mask = DEBUG_ESSENTIAL|DEBUG_ENTRYPOINTS|DEBUG_INDICATORS|DEBUG_CALCS|
      DEBUG_STATUS|DEBUG_STATE|DEBUG_SPECIAL|SDSU_SHOW_ERRORS|SDSU_SHOW_COMMANDS;
    break;
  case DEBUG_MIN:
    debug_mask = DEBUG_ESSENTIAL;
    break;
  case DEBUG_SDSU:
    debug_mask = DEBUG_ESSENTIAL|SDSU_SHOW_ERRORS|SDSU_SHOW_COMMANDS;
    break;
  case DEBUG_SPECIAL_G:
    debug_mask = DEBUG_SPECIAL;
    break;
  case DEBUG_STATE_G:
    debug_mask = DEBUG_ESSENTIAL|DEBUG_STATE;
    break;
  case DEBUG_CALCS_G:
    debug_mask = DEBUG_ESSENTIAL|DEBUG_CALCS;
    break;
  case DEBUG_NOT_SET:
  case NO_LOG:
  case DEBUG_NONE:
  default:
    debug_mask = 0;
    break;
  }

  // Turn off file logging and just use message buffer
  //      debug_mask = -debug_mask;

  // The camera class needs a reporting object when debug is set

  /* TODO: Figure out the debugging
  if ((debug_mask > 0) && (dc_report == NULL)) {
    logger::message(logger::Level::Min, "Creating report object.");
    dc_report = new Report("DC Debug Report");
    logger::message(logger::Level::Min, "Report object created = %p.", dc_report);
  }
  */
}


/*
 *+
 * FUNCTION NAME: void dc_update_config_sirs();
 *
 * INVOCATION: dc_update_config_sirs();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Update SIR records related to controller config
 *
 * DESCRIPTION:
 *
 * Uses the camera configuration information to update appropriate SIRS.
 * . detType
 * . detID
 * . timingDSP
 * . VMEDSP
 *
 * EXTERNAL VARIABLES: DC DC_Par setup parameters
 *
 * PRIOR REQUIREMENTS: This is called from DC_Slave once DC_Control has
 * initialised and in response to Gemini EPICS INIT command
 *
 * DEFICIENCIES:
 *
 *-
 */
static void dc_update_config_sirs()
{
  int cid = shared_config->config_data.Config_Data_u.camera.cid;
  Controller_Desc *controller_desc = &shared_config->config_data.Config_Data_u.camera.desc.controller[cid];
  Sdsu_Config *sdsu_config = &shared_config->config_data.Config_Data_u.camera.controller.Controller_Config_u.sdsu;
  Verify_Spec *verify = &controller_desc->verify_spec;
  Chip_Desc *this_chip = &controller_desc->chip[0];

  if (ir_camera != nullptr) {
    dc_status->put(DETTYPE, this_chip->manufacturer, 0, NAMELEN);
    dc_status->put(DETID, this_chip->manufacturer_serial, 0, NAMELEN);
    switch (controller_desc->type) {
    case SDSU2:
    case SDSU3:
    case SDSU3_5:
      dc_status->put(TIMDSP, sdsu_config->config_data.timing_filename, 0, MAX_STRING_SIZE);
      dc_status->put(INTERFACEDSP, sdsu_config->config_data.interface_filename, 0, MAX_STRING_SIZE);
      break;
    default:
      break;
    }
    // Fill in initial values for test pattern parameters
    dc_par->put(TEST_PATTERN_SEED, to_string(verify->seed));
    dc_par->put(TEST_PATTERN_INC, to_string(verify->increment));
    dc_par->put(TEST_PATTERN_AMP_INC, to_string(verify->amp_increment));
    dc_par->put(TEST_PATTERN_READ_INC, to_string(verify->readout_increment));
    dc_par->put(TEST_PATTERN_OS, to_string(verify->overscan));

    // And reflect to the SAD
    dc_status->put(TEST_PATTERN_SEED, verify->seed);
    dc_status->put(TEST_PATTERN_INC, verify->increment);
    dc_status->put(TEST_PATTERN_AMP_INC, verify->amp_increment);
    dc_status->put(TEST_PATTERN_READ_INC, verify->readout_increment);
    dc_status->put(TEST_PATTERN_OS, verify->overscan);
  }
}


/*
 *+
 * FUNCTION NAME: void dc_controller_cmd
 *
 * INVOCATION: do_controller_command(DC_Cmd cmd)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cmd - the controller command to execture
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Run a specific controller command
 *
 * DESCRIPTION:
 *
 * Sets up the command parameters and executes the controller command
 *   Shutdown - calls the detector controller shutdown method.
 *   test - calls the detector controller test method.
 *   reset - calls the detector controller reset method.
 *   cmd - gets the controller command details from the SAD and calls the
 *         direct controller command method.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void do_controller_command(DC_Cmd cmd)
{
  Sdsu_Dsp direct_dsp;
  int i, mode;
  long mem, lcmd;
  string msg,str;
  Controller *controller = ir_camera->controller;
  Sdsu3_5_Controller *sdsu_controller = ir_camera->sdsu_controller;

  logger::message(logger::Level::Full,"In do_controller_command, cmd=%d", cmd);

  // Do the command here
  switch (cmd) {
  case DC_Cmd::CONTROLLER_SHUTDOWN:
    try {
      dc_status->put(SDSU_INIT_DONE, false);
      controller->controller_shutdown();
      dc_status->put(SDSU_COMMS, true);
    }
    catch (Error& e) {
      logger::message(logger::Level::NoLog, e.record_error(__FILE__,__LINE__));
      dc_par->put(CONTROLLER_SHUTDOWN_MESS, e.msg);
      e.rethrow();
    }
    break;
  case DC_Cmd::CONTROLLER_TEST:
    try {
      dc_status->get(SDSU_NTESTS, sdsu_ntests);
      controller->controller_test(sdsu_ntests);
      dc_status->put(SDSU_COMMS, true);
    }
    catch (Error& e) {
      logger::message(logger::Level::NoLog, e.record_error(__FILE__,__LINE__));
      dc_par->put(CONTROLLER_TEST_MESS, e.msg);
      dc_status->put(SDSU_COMMS, false);
      e.rethrow();
    }
    break;
  case DC_Cmd::CONTROLLER_RESET:
    try {
      dc_status->put(SDSU_INIT_DONE, false);
      controller->controller_reset();
      dc_status->put(SDSU_COMMS, true);
    }
    catch (Error& e) {
      logger::message(logger::Level::NoLog, e.record_error(__FILE__,__LINE__));
      dc_par->put(CONTROLLER_RESET_MESS, e.msg);
      e.rethrow();
    }
    break;
  case DC_Cmd::CONTROLLER_DEBUG:
    if (ir_camera->controller[ir_camera->cid].controller.type != DUMMY) {
      try {
        dc_status->get(SDSU_DEBUG, sdsu_debug);
        sdsu_controller->set_debug_mode(sdsu_debug);
      }
      catch (Error& e) {
        logger::message(logger::Level::NoLog, e.record_error(__FILE__,__LINE__));
        dc_par->put(CONTROLLER_DEBUG_MESS, e.msg);
        e.rethrow();
      }
    }
    break;

  case DC_Cmd::CONTROLLER_SIMULATE:
    try {
      // Get the simulation setting - FAST|FULL == ON, NONE == OFF
      dc_status->get(SIM_MODE, sim_mode_str, 0, NAMELEN);
      str = sim_mode_str;
      if (str=="FAST") mode=1;
      else if (str=="FULL") mode=2;
      else mode = 0;
      controller->change_simulation_mode(mode);
    }
    catch (Error& e) {
      logger::message(logger::Level::NoLog, e.record_error(__FILE__,__LINE__));
      dc_par->put(CONTROLLER_DEBUG_MESS, e.msg);
      e.rethrow();
    }
    break;

  case DC_Cmd::CONTROLLER_INFO:
    try {
      logger::message(logger::Level::Full, "Getting controller info");
      controller->get_controller_info();
      dc_status->put(SDSU_COMMS, true);
    }
    catch (Error& e) {
      logger::message(logger::Level::NoLog, e.record_error(__FILE__,__LINE__));
      dc_par->put(CONTROLLER_DEBUG_MESS, e.msg);
      e.rethrow();
    }
    break;

  case DC_Cmd::CONTROLLER_CMD:
    try {
      if (ir_camera->controller[ir_camera->cid].controller.type != DUMMY) {
        Sdsu_Data_Cmd *data_cmd = &direct_dsp.dsp_data.Sdsu_Direct_u.data_cmd;
        Sdsu_Mem_Cmd *mem_cmd = &direct_dsp.dsp_data.Sdsu_Direct_u.mem_cmd;

        dc_status->get(SDSU_BOARD, direct_dsp.board);
        dc_status->get(SDSU_CMD, lcmd);
        direct_dsp.dsp_data.cmd = (Sdsu_Cmd) lcmd;

        switch (direct_dsp.dsp_data.cmd) {
        case SDSU_RDM:
        case SDSU_WRM:
          dc_status->get(SDSU_MEM_SPACE,mem);
          mem_cmd->memory_space = (Sdsu_Memory_Space) mem;
          dc_status->get(SDSU_MEM_ADDR, mem_cmd->address);
          dc_status->get(SDSU_ARG1, mem_cmd->value);
          break;
        default:
          for (i=0; i<MAX_DSP_DATA_ARGS; i++)
            data_cmd->data[i] = UNDEFINED;
          dc_status->get(SDSU_ARG1, data_cmd->data[0]);
          dc_status->get(SDSU_ARG2, data_cmd->data[1]);
          data_cmd->ndata = 2;
          break;
        }

        if (false) {
          printf("board: %d\n",direct_dsp.board);
          printf("cmd: %d\n", int(direct_dsp.dsp_data.cmd));
          printf("data_cmd.ndata: %d\n", int(direct_dsp.dsp_data.Sdsu_Direct_u.data_cmd.ndata));
          printf("data_cmd.arg1: %d\n", int(direct_dsp.dsp_data.Sdsu_Direct_u.data_cmd.data[0]));
          printf("data_cmd.arg2: %d\n", int(direct_dsp.dsp_data.Sdsu_Direct_u.data_cmd.data[1]));

          printf("\n\nsdsu_controller=%p, controller=%p\n\n", sdsu_controller, controller);
        }

        if (sdsu_controller->send_direct_sdsu_command(direct_dsp) == SDSU_ERROR) {
          throw Error("Error executing SDSU cmd, ERR reply!", E_ERROR, -1, __FILE__, __LINE__);
        }
        dc_status->put(SDSU_COMMS, true);
      }
    }
    catch (Error& e) {
      logger::message(logger::Level::NoLog, e.record_error(__FILE__,__LINE__));
      dc_par->put(CONTROLLER_CMD_MESS, e.msg);
      e.rethrow();
    }
    break;
  default:
    break;
  }

  // On our way out - clear cancel state
  // If we were canceling then reset the canceling flag so that the Control SNL can move on
  dc_status->get(SDSU_CANCELING, sdsu_canceling);

  if (sdsu_canceling)
    // so clear the flag
    dc_status->put(SDSU_CANCELING, false);

  ir_camera->controller->clear_reset();

}
/*
 *+
 * FUNCTION NAME: void save_camera_setup()
 *
 * INVOCATION: save_camera_setup()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Save CAD parameters to setup file on disk
 *
 * DESCRIPTION:
 *
 * Each CAD parameter current setting is saved tyo disk. This is to
 * simplify engineering operations. It will save reloading all parameters
 * manually after each restart. The OCS should override these setting during
 * noirmal operations.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void save_camera_setup()
{
  try {
    if ((ir_setup != nullptr) && (ir_camera != nullptr)) {
      logger::message(logger::Level::Full, "Writing setup file %s", IR_SETUP);

      // Copy current working values to setup structure
      ir_setup->view = ir_camera->ir_status->view;           //+ parameters for view mode
      ir_setup->obs = ir_camera->ir_status->obs;        //+ parameters for observe mode
      ir_setup->d_view = ir_camera->ir_status->d_view;          //+ data parameters for view mode
      ir_setup->d_obs = ir_camera->ir_status->d_obs;       //+ data parameters for observe mode

      // Now write the new values to table file
      ir_setup->write(IR_SETUP);
      camera_setup_changed = false;
    }
  }
  catch (Error& dce) {
    logger::message(logger::Level::NoLog, dce.record_error(__FILE__,__LINE__));
  }
}
