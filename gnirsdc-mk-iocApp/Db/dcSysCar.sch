[schematic2]
uniq 321
[tools]
[detail]
w 3490 2347 100 0 n#289 dcCombCar3.dcCombCar3#320.OUTC 3408 2336 3632 2336 3632 2912 3728 2912 egenSub.ovrAllCar.I
w 3458 2379 100 0 n#288 dcCombCar3.dcCombCar3#320.OUTB 3408 2368 3568 2368 3568 2976 3728 2976 egenSub.ovrAllCar.H
w 3586 3051 100 0 n#287 dcCombCar3.dcCombCar3#320.OUTA 3408 2400 3504 2400 3504 3040 3728 3040 egenSub.ovrAllCar.G
w 3490 3083 100 0 n#286 dcCombCar2.dcCombCar2#319.OUTC 3408 3072 3632 3072 3632 3104 3728 3104 egenSub.ovrAllCar.F
w 3458 3115 100 0 n#285 dcCombCar2.dcCombCar2#319.OUTB 3408 3104 3568 3104 3568 3168 3728 3168 egenSub.ovrAllCar.E
w 3586 3243 100 0 n#284 dcCombCar2.dcCombCar2#319.OUTA 3408 3136 3504 3136 3504 3232 3728 3232 egenSub.ovrAllCar.D
w 3586 3307 100 0 n#283 dcCombCar1.dcCombCar1#318.OUTC 3408 3744 3504 3744 3504 3296 3728 3296 egenSub.ovrAllCar.C
w 3458 3787 100 0 n#282 dcCombCar1.dcCombCar1#318.OUTB 3408 3776 3568 3776 3568 3360 3728 3360 egenSub.ovrAllCar.B
w 3490 3819 100 0 n#281 dcCombCar1.dcCombCar1#318.OUTA 3408 3808 3632 3808 3632 3424 3728 3424 egenSub.ovrAllCar.A
w 3434 3723 -100 0 w dcCombCar1.dcCombCar1#318.FLNK 3408 3712 3472 3712 3472 2720 junction
w 3434 3051 -100 0 w dcCombCar2.dcCombCar2#319.FLNK 3408 3040 3472 3040 3472 2720 junction
w 3594 2731 100 0 w dcCombCar3.dcCombCar3#320.FLNK 3408 2304 3472 2304 3472 2720 3728 2720 egenSub.ovrAllCar.SLNK
w 4074 2699 100 0 n#224 egenSub.ovrAllCar.FLNK 4016 2688 4192 2688 4192 2992 4304 2992 ecars.applyC.SLNK
w 4178 3131 100 0 n#226 egenSub.ovrAllCar.OUTC 4016 3264 4112 3264 4112 3120 4304 3120 ecars.applyC.IMSS
w 4194 3099 100 0 n#225 egenSub.ovrAllCar.OUTB 4016 3328 4144 3328 4144 3088 4304 3088 ecars.applyC.IERR
w 4072 3403 100 0 n#53 egenSub.ovrAllCar.OUTA 4016 3392 4176 3392 4176 3184 4304 3184 ecars.applyC.IVAL
w 4000 3643 -100 0 CLID inhier.CLID.P 3824 3632 4224 3632 4224 3152 4304 3152 ecars.applyC.ICID
s 2432 1888 200 0 reports the overall CAR status in the top level CAR.
s 2432 1936 200 0 message. The dcOvrAllCar gensub takes the intermediate results and
s 2432 1984 200 0 the CAR value that is the highest along with its corresponding error code and
s 2432 2032 200 0 individual CAR records. Each gensub takes upt to 3 CAR inputs and outputs
s 2432 2080 200 0 This schematic contains the records for hierarchically combining
[cell use]
use dcCombCar3 3248 2231 100 0 dcCombCar3#320
xform 0 3328 2336
use dcCombCar2 3248 2967 100 0 dcCombCar2#319
xform 0 3328 3072
use dcCombCar1 3248 3639 100 0 dcCombCar1#318
xform 0 3328 3744
use egenSub 3816 3472 100 0 ovrAllCar
xform 0 3872 3056
p 3828 3412 65 0 1 FTA:LONG
p 3828 3396 65 0 1 FTB:LONG
p 3828 3380 65 0 1 FTC:STRING
p 3828 3364 65 0 1 FTD:LONG
p 3828 3348 65 0 1 FTE:LONG
p 3828 3332 65 0 1 FTF:STRING
p 3828 3316 65 0 1 FTG:LONG
p 3828 3300 65 0 1 FTH:LONG
p 3828 3284 65 0 1 FTI:STRING
p 3828 3260 65 0 1 FTVA:LONG
p 3828 3244 65 0 1 FTVB:LONG
p 3828 3228 65 0 1 FTVC:STRING
p 3828 2680 65 0 1 PV:$(top)
p 3792 2608 100 768 1 SNAM:dcCombCars
use ecars 4424 3228 100 0 applyC
xform 0 4464 3072
p 4404 2948 100 0 1 PV:$(top)
p 4400 2832 100 0 0 SIMS:NO_ALARM
use inhier 3808 3591 100 0 CLID
xform 0 3824 3632
use eborderC 2280 1659 100 0 eborderC#0
xform 0 3960 2964
p 4856 1812 100 768 -1 author:Peter Young
p 4856 1924 40 0 -1 comment1:1997-10-01 GMOS version. Janet Tvedt
p 4856 1904 40 0 -1 comment2:2000-12-12 Modified for GNIRS. Peter Young
p 4856 1780 100 768 -1 date:2000-12-12
p 5080 1860 200 768 -1 file:dcSysCar.sch
p 5352 1812 100 0 -1 page:1
p 5464 1812 100 0 -1 pages:1
p 5080 1780 100 0 0 primitive:eborderD
p 5128 1812 100 0 -1 revision:0
p 5080 1924 150 768 -1 system:Gemini GNIRS DC
[comments]
