/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME config_table.cc
 * 
 * GENERAL DESCRIPTION
 *  Configuration classes implementation - uses generic table class as base.
 * RSAA's model of astronomical instrumentation is highly configurable. This
 * configurability is achieved through the use of configuration tables with sets
 * of parameter value pairs. This set of classes handles each of the configuration
 * tables for each component of an instrument.
 * 
 */

/* Include files */
#include <stdio.h>
#include "configuration.h"
#include "fits.h"
#include "config_table.h"

using namespace std;

/*
 *+ 
 * FUNCTION NAME: Config_Table::Config_Table
 * 
 * INVOCATION: ct = new Config_Table(const char *name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > name - name of config table
 * > dp - pointer to item cache
 * > dps - pointer to item cache save
 * > sz - size of data cache
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Consructor
 * 
 * DESCRIPTION: 
 *============================================================================
 *  Config Table
 *  The Config table file is a text file consisting of descriptions
 *  of individual item descriptions. Text after the # symbol is ignored as a comment.
 *  Each item description consists of a name in [] followed by a series
 *  param=value; pairs. These pairs can be more than one per line or span several
 *  lines until the next [name] item. An item description parameter set is 
 *  defined in the desc.x RPCL file.
 *===========================================================================
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Config_Table::Config_Table(const string name, const void* dp, const void* dps, const int sz)
  :Table(name)
{
  Table::set_data_cache(dp, dps, sz);
}

/*
 *+ 
 * FUNCTION NAME: Cicada_Table::Cicada_Table
 * 
 * INVOCATION: ct = new Cicada_Table(const char *name, const int il, const int ns)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Consructor
 * 
 * DESCRIPTION: Constructor for table that needs to set "items per line"
 * and/or "name sort"
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Config_Table::Config_Table(const string name, const int il, const bool ns, const void* dp, const void* dps, const int sz)
  :Table(name, il, ns)
{
  Table::set_data_cache(dp, dps, sz);
}

/*
 *+ 
 * FUNCTION NAME: void Config_Table::read
 * 
 * INVOCATION: read(const string tname)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of table
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Read in table in new format
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Config_Table::read(const string tname)
{
  // Read into hash table
  Table::read(tname);
}

/*
 *+ 
 * FUNCTION NAME: void Config_Table::write
 * 
 * INVOCATION: write(const string tname, bool do_owned_tables)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of table
 * > do_owned_tables - Set if required to write tables that are owned by this table
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Write out table
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Config_Table::write(const string tname, bool do_owned_tables)
{


  // First ensure that any cached data is written back
  if (need_update())
    update();

  // Now write the table file
  Table::write(tname);
}

/*
 *+ 
 * FUNCTION NAME: int Config_Table::remove
 * 
 * INVOCATION: remove(const string name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > name - name of item to remove
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Remove table item
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
int Config_Table::remove(const string name)
{
  int idx;
  idx = destroy(name);
  get_element(idx);
  return idx;
}
/*
 *+ 
 * FUNCTION NAME: void Config_Table::get_element
 * 
 * INVOCATION: get_element(int idx)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > idx - item index
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Gets element at index idx
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Config_Table::get_element(int idx)
{
  ostringstream ostr;

  // Find the item by index
  Table_Item *item=find(idx);
  if (item == NULL) {
    ostr << "Item " << idx << " not found in table " << table_name.c_str() << ends;
    throw Error(ostr, E_ERROR, -1 ,__FILE__, __LINE__);      
  }

  // If this is a new element then save last and load it
  if (last_name != item->name) {

    // Before overwriting internal buffer ensure any changes flushed
    if (need_update())
      update();

    // Get its current description
    get(item);

    // save name and position
    last_name = item->name;
    last_pos = index(item->name);
  }
}
/*
 *+ 
 * FUNCTION NAME: void Config_Table::get_element
 * 
 * INVOCATION: get_element(const char* item_name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > item_name - name of item to get
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Gets element named item_name
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Config_Table::get_element(const char* item_name)
{
  Table_Item *item;
  ostringstream ostr;

  if (last_name != item_name) {
    // Before overwriting internal buffer ensure any changes flushed
    if (need_update())
      update();

    // Now find the new item
    item = find(item_name);
    if (item == NULL) {
      ostr << "Item " << item_name << " not found in table '" << table_name.c_str() << "'!" << ends;
      throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);      
    }

    // Get its current description
    get(item);

    // save name and position
    last_name = item_name;
    last_pos = index(item_name);
  }
}
/*
 *+ 
 * FUNCTION NAME: Config_Table::~Config_Table
 * 
 * INVOCATION: ~Config_Table()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Config_Table::~Config_Table()
{
}
/*
 *+ 
 * FUNCTION NAME: Instrument_Table::Instrument_Table():Config_Table("Instrument Table")
 * 
 * INVOCATION: Instrument_Table::Instrument_Table
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Consructor
 * 
 * DESCRIPTION: 
 *  Instrument Table
 *  The Instrument table file is a text file consisting of single line descriptions
 *  of individual instruments. Text after the # symbol is ignored as a comment.
 *  Each instrument description is a set of par/value pairs separated by the ";".
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Instrument_Table::Instrument_Table():
  Config_Table("Instrument Table", (void*)&instrument, (void*)&instrument_save, sizeof(Instrument_Desc))
{
  instrument.name[0] = '\0';
  instrument_save = instrument;
}
/*
 *+ 
 * FUNCTION NAME: 
 * 
 * INVOCATION: read(const string tname)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of table
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Instrument_Table::read(const string tname)
{
  Filenametype fname;
  // Read "owned" tables
  strlcpy(fname,CAMERA_TABLE,FILENAMELEN);
  camera_table.read(fname);
  // Now read our table
  Config_Table::read(tname);
}
/*
 *+ 
 * FUNCTION NAME: void Instrument_Table::write
 * 
 * INVOCATION: write(const string tname, bool do_owned_tables)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of table
 * > do_owned_tables - Set if required to write tables that are owned by this table
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Instrument_Table::write(const string tname, bool do_owned_tables)
{
  Filenametype fname;
  if (do_owned_tables) {
    // Write "owned" tables
    strlcpy(fname,CAMERA_TABLE,FILENAMELEN);
    camera_table.write(fname);
  }
  // Now write our table
  Config_Table::write(tname);
}
/*
 *+ 
 * FUNCTION NAME: void Instrument_Table::put
 * 
 * INVOCATION: put(void* data_item, int pos)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Add an entry to the table
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Instrument_Table::put(void* data_item, int pos)
{
  Table_Item* item;
  Instrument_Desc* tinstrument= (Instrument_Desc*) data_item;
  Nametype pname;
  int i;
  ostringstream ostr;

  if ((item = find(tinstrument->name)) != NULL) {
    ostr << "Put: Instrument '" << tinstrument->name << "' already exists in table, cannot put!" << ends;
    throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);     
  }

  item = new Table_Item(tinstrument->name);
  
  // Now for each possible instrument parameter check that it differs from default
  item->put("n_cameras",tinstrument->n_cameras);
  for (i=0; i<tinstrument->n_cameras; i++) {
    snprintf(pname, NAMELEN,"camera%d",i+1);
    item->put(pname,tinstrument->camera[i].name);
  }
  insert(item,pos);

  // Cater for situations where put is called with external item
  // Usually will simply be putting back value in internal buffer
  if ((void*)&instrument != data_item) {
    instrument = instrument_save = *tinstrument;
  }
}

/*
 *+ 
 * FUNCTION NAME: void Instrument_Table::get
 * 
 * INVOCATION: get(Table_Item *item)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Instrument_Table::get(Table_Item *item)
{
  Instrument_Desc tinstrument;
  string str;
  int i,k;
  ostringstream ostr;
  Nametype pname;

  // Set instrument desc to null values
  memset((void*)&tinstrument, 0, sizeof(Instrument_Desc));

  strlcpy(tinstrument.name, item->name.c_str(), NAMELEN);
  // Now for each possible instrument parameter try to find in table

  item->find("n_cameras", tinstrument.n_cameras);
  for (i=0; i<tinstrument.n_cameras; i++) {
    snprintf(pname, NAMELEN,"camera%d",i+1);
    if (item->find(pname,str)) {
      if (camera_table.find(str) == NULL) {
	ostr << "Get: Failed to find camera: '" << str.c_str() << "'." << ends;
	throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
      } else {
	// Make sure this camera is not already part of the instrument,
	// ie no duplicates
	for (k=0; k<i; k++) {
	  if (str == tinstrument.camera[k].name) {
	    ostr << "Get: Duplicate camera '" << str.c_str() << "' for instrument '" << tinstrument.name << "'." << ends;
	    throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
	  }
	}
	tinstrument.camera[i] = camera_table[str.c_str()];
      }      
    }
  }

  instrument = instrument_save = tinstrument;
 
}
/*
 *+ 
 * FUNCTION NAME: Instrument_Desc& Instrument_Table::operator[]
 * 
 * INVOCATION: operator[](int idx)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: Instrument_Desc
 * 
 * PURPOSE: Return a reference to the ith element in the table - updates internal buffer
 * and sets flag
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Instrument_Desc& Instrument_Table::operator[](int idx)
{
  get_element(idx);
  return instrument;
}
/*
 *+ 
 * FUNCTION NAME: Instrument_Desc& Instrument_Table::operator[]
 * 
 * INVOCATION: operator[](const char* item_name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: Instrument_Desc
 * 
 * PURPOSE: Return a reference to the element named in the table - updates internal buffer
 * and sets flag
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Instrument_Desc& Instrument_Table::operator[](const char* item_name)
{
  get_element(item_name);
  return instrument;
}
/*
 *+ 
 * FUNCTION NAME: Instrument_Table::~Instrument_Table
 * 
 * INVOCATION: ~Instrument_Table()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Instrument_Table::~Instrument_Table()
{
  if (need_update()) {
    update();
  }
}
/*
 *+ 
 * FUNCTION NAME: CCD_Table::CCD_Table():Config_Table("CCD Table")
 * 
 * INVOCATION: CCD_Table::CCD_Table
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Consructor
 * 
 * DESCRIPTION: 
 *============================================================================
 *  CCD Table
 *  The CCD table file is a text file consisting of single line descriptions
 *  of individual CCDs. Text after the # symbol is ignored as a comment.
 *  Each CCD description is a set of fields separated by the ":".
 *  The format of a CCD line is
 *  serial:manufacturer:man_serial:cols:rows:n_amps:amp_mask:xo1:yo1:wid1:height1:prescan1:postscan1:xdir1:ydir1:..:comment
 *  where 
 *   serial: CCD serial number
 *   manufacturer: CCD manufacturer
 *   man_serial: manufacturer serial number (use an X to signify missing)
 *   cols: total columns on chip
 *   rows: total rows on chip
 *   n_amps: number of readout amplifiers
 *   Following fields only if n_amps>1 (except comment which is always last field)
 *   amp_mask: bit mask indicating which amplifiers are currently used
 *   xo,yo: position in chip coordinates of amplifier pixel 1
 *   wid,height: amplifier readout size
 *   prescan,postscan: amplifier serial register bias pixels
 *   xdir,ydir: amplifier readout direction (either 1 or -1)
 *   comment: Further description of the CCD
 *===========================================================================*
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Chip_Table::Chip_Table():
  Config_Table("CCD Table", (void*)&chip, (void*)&chip_save, sizeof(Chip_Desc))
{
  chip.name[0] = '\0';
  chip_save = chip;
}
/*
 *+ 
 * FUNCTION NAME: void Chip_Table::put
 * 
 * INVOCATION: put(void* data_item, int pos)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Chip_Table::put(void* data_item, int pos)
{
  Table_Item* item;
  ostringstream ostr;
  Chip_Desc* tchip = (Chip_Desc*) data_item;
  int i;
  Nametype pname;
  int *txo,*tyo,*twidth,*theight,*tprescan,*tpostscan,*txdir,*tydir,*torient; 

  if ((item = find(tchip->name)) != NULL) {
    ostr << "CHIP " << tchip->name << " already exists in table!" << ends;
    throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);     
  }

  item = new Table_Item(tchip->name);
  
  item->put("manufacturer",tchip->manufacturer);
  item->put("manufacturer_serial",tchip->manufacturer_serial);
  item->put("cols", tchip->cols);
  item->put("rows", tchip->rows);
  item->put("has_wcs", tchip->has_wcs);
  if (tchip->has_wcs) {
    for (i=0; i<2; i++) {
      snprintf(pname, NAMELEN,"ctype%d",i+1);
      item->put(pname,tchip->wcs[i].ctype);
      snprintf(pname, NAMELEN,"crval%d",i+1);
      item->put(pname,tchip->wcs[i].crval);
      snprintf(pname, NAMELEN,"cunit%d",i+1);
      item->put(pname,tchip->wcs[i].cunit);
      snprintf(pname, NAMELEN,"crpix%d",i+1);
      item->put(pname,tchip->wcs[i].crpix);
      snprintf(pname, NAMELEN,"cd%d",i+1);
      item->put(pname,tchip->wcs[i].cd,2);
    }
  }
  item->put("adu_increase", tchip->adu_increase);
  item->put("n_amps", tchip->n_amps);
  item->put("amp_mask", tchip->amp_mask);

  // Now handle the amp parameters - allocate some work space
  if (tchip->n_amps>0) {
    txo = new int[tchip->n_amps];
    tyo = new int[tchip->n_amps];
    twidth = new int[tchip->n_amps];
    theight = new int[tchip->n_amps];
    tprescan = new int[tchip->n_amps];
    tpostscan = new int[tchip->n_amps];
    txdir = new int[tchip->n_amps];
    tydir = new int[tchip->n_amps];
    torient = new int[tchip->n_amps];
    for (i=0; i<tchip->n_amps; i++) {
      snprintf(pname, NAMELEN,"amp_name%d",i+1);
      item->put(pname,tchip->amp[i].name);
      txo[i] = tchip->amp[i].xo;
      tyo[i] = tchip->amp[i].yo;
      twidth[i] = tchip->amp[i].width;
      theight[i] = tchip->amp[i].height;
      tprescan[i] = tchip->amp[i].prescan;
      tpostscan[i] = tchip->amp[i].postscan;
      txdir[i] = tchip->amp[i].xdir;
      tydir[i] = tchip->amp[i].ydir;
      torient[i] = (int)tchip->amp[i].orient;
    }
    item->put("xo", txo,tchip->n_amps);
    item->put("yo", tyo,tchip->n_amps);
    item->put("width", twidth,tchip->n_amps);
    item->put("height", theight,tchip->n_amps);
    item->put("prescan", tprescan,tchip->n_amps);
    item->put("postscan", tpostscan,tchip->n_amps);
    item->put("xdir", txdir,tchip->n_amps);
    item->put("ydir", tydir,tchip->n_amps);
    item->put("orient", torient, tchip->n_amps);

    // Now free workspace
    delete [] txo;
    delete [] tyo;
    delete [] twidth;
    delete [] theight;
    delete [] tprescan;
    delete [] tpostscan;
    delete [] txdir;
    delete [] tydir;
    delete [] torient;
  }
  item->put("comment",tchip->comment);
    
  insert(item,pos);

  // Cater for situations where put is called with external item
  // Usually will simply be putting back value in internal buffer "chip'
  if ((void*)&chip != data_item) {
    chip = chip_save = *tchip;
  }
}

// Get a table description - read_only
/*
 *+ 
 * FUNCTION NAME: void Chip_Table::get
 * 
 * INVOCATION: get(Table_Item *item)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Chip_Table::get(Table_Item *item)
{
  Chip_Desc tchip;
  int i;
  Nametype pname;
  int *txo,*tyo,*twidth,*theight,*tprescan,*tpostscan,*txdir,*tydir,*torient; 

  // Set chip desc to null values
  memset((void*)&tchip, 0, sizeof(Chip_Desc));

  strlcpy(tchip.name, item->name.c_str(), NAMELEN);
  // Now for each possible CHIP parameter try to find in table
  item->find("manufacturer", tchip.manufacturer, NAMELEN);
  item->find("manufacturer_serial", tchip.manufacturer_serial, NAMELEN);
  item->find("cols", tchip.cols);
  item->find("rows", tchip.rows);
  item->find("has_wcs", tchip.has_wcs);
  if (tchip.has_wcs) {
    for (i=0; i<2; i++) {
      snprintf(pname, NAMELEN,"ctype%d",i+1);
      item->find(pname,tchip.wcs[i].ctype, NAMELEN);
      snprintf(pname, NAMELEN,"crval%d",i+1);
      item->find(pname,tchip.wcs[i].crval);
      snprintf(pname, NAMELEN,"cunit%d",i+1);
      item->find(pname,tchip.wcs[i].cunit, NAMELEN);
      snprintf(pname, NAMELEN,"crpix%d",i+1);
      item->find(pname,tchip.wcs[i].crpix);
      snprintf(pname, NAMELEN,"cd%d",i+1);
      item->find(pname,tchip.wcs[i].cd,2);
    }
  }
  item->find("adu_increase", tchip.adu_increase);
  item->find("amp_mask", tchip.amp_mask);
  item->find("n_amps", tchip.n_amps);

  if (tchip.n_amps>0) {
    // Allocate some working space
    txo = new int[tchip.n_amps];
    memset((void*)txo, 0, tchip.n_amps*sizeof(int));
    tyo = new int[tchip.n_amps];
    memset((void*)tyo, 0, tchip.n_amps*sizeof(int));
    twidth = new int[tchip.n_amps];
    memset((void*)twidth, 0, tchip.n_amps*sizeof(int));
    theight = new int[tchip.n_amps];
    memset((void*)theight, 0, tchip.n_amps*sizeof(int));
    tprescan = new int[tchip.n_amps];
    memset((void*)tprescan, 0, tchip.n_amps*sizeof(int));
    tpostscan = new int[tchip.n_amps];
    memset((void*)tpostscan, 0, tchip.n_amps*sizeof(int));
    txdir = new int[tchip.n_amps];
    memset((void*)txdir, 0, tchip.n_amps*sizeof(int));
    tydir = new int[tchip.n_amps];
    memset((void*)tydir, 0, tchip.n_amps*sizeof(int));
    torient = new int[tchip.n_amps];
    memset((void*)torient, 0, tchip.n_amps*sizeof(int));

    item->find("xo",txo,tchip.n_amps);
    item->find("yo",tyo,tchip.n_amps);
    item->find("width",twidth,tchip.n_amps);
    item->find("height",theight,tchip.n_amps);
    item->find("prescan",tprescan,tchip.n_amps);
    item->find("postscan",tpostscan,tchip.n_amps);
    item->find("xdir",txdir,tchip.n_amps);
    item->find("ydir",tydir,tchip.n_amps);
    item->find("orient",torient,tchip.n_amps);

    for (i=0; i<tchip.n_amps; i++) {
      snprintf(tchip.amp[i].name, NAMELEN,"%c",'A'+i);//default constructed Amp Name
      snprintf(pname, NAMELEN,"amp_name%d",i+1); // now find real amp name
      item->find(pname, tchip.amp[i].name, NAMELEN);
      tchip.amp[i].xo = txo[i];
      tchip.amp[i].yo = tyo[i];
      tchip.amp[i].width = twidth[i];
      tchip.amp[i].height = theight[i];
      tchip.amp[i].prescan = tprescan[i];
      tchip.amp[i].postscan = tpostscan[i];
      tchip.amp[i].xdir = txdir[i];
      tchip.amp[i].ydir = tydir[i];
      tchip.amp[i].orient = (Readout_Orientation) torient[i];
    }

    // Now free workspace
    delete [] txo;
    delete [] tyo;
    delete [] twidth;
    delete [] theight;
    delete [] tprescan;
    delete [] tpostscan;
    delete [] txdir;
    delete [] tydir;
    delete [] torient;
  }
  item->find("comment", tchip.comment, COMMENTLEN);
  chip = chip_save = tchip; 
}
// Return a reference to the ith element in the table - updates internal buffer
// and sets flag
/*
 *+ 
 * FUNCTION NAME: Chip_Desc& Chip_Table::operator[]
 * 
 * INVOCATION: operator[](int idx)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Chip_Desc& Chip_Table::operator[](int idx)
{
  get_element(idx);
  return chip;
}
// Return a reference to the element named in the table - updates internal buffer
// and sets flag
/*
 *+ 
 * FUNCTION NAME: Chip_Desc& Chip_Table::operator[]
 * 
 * INVOCATION: operator[](const char* item_name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Chip_Desc& Chip_Table::operator[](const char* item_name)
{
  get_element(item_name);
  return chip;
}

/*
 *+ 
 * FUNCTION NAME: Chip_Table::~Chip_Table
 * 
 * INVOCATION: ~Chip_Table()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Chip_Table::~Chip_Table()
{
  if (need_update()) {
    update();
  }
}

/*
 *+ 
 * FUNCTION NAME: Camera_Table::Camera_Table():Config_Table("Camera Table")
 * 
 * INVOCATION: Camera_Table::Camera_Table
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Consructor
 * 
 * DESCRIPTION: 
 *  Camera Table
 *  The Camera table file is a text file consisting of single line descriptions
 *  of individual cameras. Text after the # symbol is ignored as a comment.
 *  Each Camera description is a set of par/values separated by the ";".
 *  Multiple CHIP cameras are clocked out in order of CHIPs listed in this table.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Camera_Table::Camera_Table():
  Config_Table("Camera Table", (void*)&camera, (void*)&camera_save, sizeof(Camera_Desc))
{
  camera.name[0] = '\0';
  camera_save = camera;
}
/*
 *+ 
 * FUNCTION NAME: Camera_Table::read
 * 
 * INVOCATION: read(const string tname)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of table
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Camera_Table::read(const string tname)
{
  Filenametype fname;
  // Read "owned" tables
  strlcpy(fname,CONTROLLER_TABLE,FILENAMELEN);
  controller_table.read(fname);


  // Now read our table
  Config_Table::read(tname);
}
/*
 *+ 
 * FUNCTION NAME: void Camera_Table::write
 * 
 * INVOCATION: write(const string tname, bool do_owned_tables)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of table
 * > do_owned_tables - Set if required to write tables that are owned by this table
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Camera_Table::write(const string tname, bool do_owned_tables)
{
  Filenametype fname;

  if (do_owned_tables) {
    // Write "owned" tables
    strlcpy(fname,CONTROLLER_TABLE,FILENAMELEN);
    controller_table.write(fname);

  }

  // Now write our table
  Config_Table::write(tname);
}
/*
 *+ 
 * FUNCTION NAME: void Camera_Table::put
 * 
 * INVOCATION: put(void* data_item, int pos)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Add an entry to the table
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Camera_Table::put(void* data_item, int pos)
{
  Table_Item* item;
  ostringstream ostr;
  Camera_Desc* tcamera= (Camera_Desc*) data_item;
  Nametype pname;
  string str;
  int i;

  if ((item = find(tcamera->name)) != NULL) {
    ostr << "Camera " << tcamera->name << " already exists in table!" << ends;
    throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);     
  }

  item = new Table_Item(tcamera->name);
  
  // Now for each possible camera parameter check that it differs from default
  switch (tcamera->type) {
  case CCD_CAMERA: str = "CCD_CAMERA"; break;
  case IR_CAMERA: str = "IR_CAMERA"; break;
  case CAMERA_UNKNOWN: str = "CAMERA_UNKNOWN"; break;
  }
  item->put("type",str);
  switch (tcamera->mode) {
  case SCIENCE_CAMERA: str = "SCIENCE_CAMERA"; break;
  case GUIDER_CAMERA: str = "GUIDER_CAMERA"; break;
  }
  item->put("mode",str);
  item->put("n_controllers",tcamera->n_controllers);
  for (i=0; i<tcamera->n_controllers; i++) {
    snprintf(pname, NAMELEN,"controller%d",i+1);
    item->put(pname,tcamera->controller[i].name);
    snprintf(pname, NAMELEN,"config%d",i+1);
    item->put(pname,tcamera->cont_config[i]);
  }
  if (tcamera->n_controllers>0) {
    item->put("cont_xo",tcamera->cont_xo,tcamera->n_controllers);
    item->put("cont_yo",tcamera->cont_yo,tcamera->n_controllers);
    item->put("cont_xgap",tcamera->cont_xgap,tcamera->n_controllers);
    item->put("cont_ygap",tcamera->cont_ygap,tcamera->n_controllers);
    item->put("cont_master",tcamera->cont_master,tcamera->n_controllers);
    item->put("cont_shutter",tcamera->cont_shutter,tcamera->n_controllers);
    item->put("cont_temp",tcamera->cont_temp,tcamera->n_controllers);
  }
  switch (tcamera->mosaic_mode) {
  case MOSAIC_NONE: str = "MOSAIC_NONE"; break;
  case MOSAIC_AMP: str = "MOSAIC_AMP"; break;
  case MOSAIC_CHIP: str = "MOSAIC_CHIP"; break;
  }
  item->put("mosaic_mode",str);
  item->put("n_buffers",tcamera->n_buffers);
  item->put("phu1",tcamera->phu1);
  item->put("phu2",tcamera->phu2);
  item->put("ihu",tcamera->ihu);

  insert(item,pos);

  // Cater for situations where put is called with external item
  // Usually will simply be putting back value in internal buffer 
  if ((void*)&camera != data_item) {
    camera = camera_save = *tcamera;
  }
}
/*
 *+ 
 * FUNCTION NAME: void Camera_Table::calc_rows_cols
 * 
 * INVOCATION: calc_rows_cols(Camera_Desc& tcamera)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Calculate total rows and cols for a camera
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Camera_Table::calc_rows_cols(Camera_Desc& tcamera)
{
  int i,j;
  tcamera.cols = 0;
  tcamera.rows = 0;
  for (i=0;i<tcamera.n_controllers;i++) {
    for (j=0; j<tcamera.controller[i].n_chips; j++) {
      // Calculate number of cols - use controller position, chip position and size
      tcamera.cols = (tcamera.cont_xo[i] + tcamera.controller[i].chip_xo[j]+
		      tcamera.controller[i].chip[j].cols > tcamera.cols)? 
	tcamera.cont_xo[i]+tcamera.controller[i].chip_xo[j]+tcamera.controller[i].chip[j].cols:
	tcamera.cols;
      // Calculate number of rows - use controller position, chip position and size
      tcamera.rows = (tcamera.cont_yo[i] + tcamera.controller[i].chip_yo[j]+
		      tcamera.controller[i].chip[j].rows > tcamera.rows)? 
	tcamera.cont_yo[i]+tcamera.controller[i].chip_yo[j]+tcamera.controller[i].chip[j].rows: 
	tcamera.rows;
    }
  }
}

/*
 *+ 
 * FUNCTION NAME: void Camera_Table::get
 * 
 * INVOCATION: get(Table_Item *item)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Camera_Table::get(Table_Item *item)
{
  Camera_Desc tcamera;
  string str;
  int i,k;
  ostringstream ostr;
  Nametype pname;

  // Set chip desc to null values
  memset((void*)&tcamera, 0, sizeof(Camera_Desc));

  strlcpy(tcamera.name, item->name.c_str(), NAMELEN);
  // Now for each possible camera parameter try to find in table

  item->find("n_controllers", tcamera.n_controllers);
  item->find("type",pname,NAMELEN);
  str=pname;
  if (str == "CCD_CAMERA") tcamera.type = CCD_CAMERA;
  else if (str == "IR_CAMERA") tcamera.type = IR_CAMERA;
  else tcamera.type = CAMERA_UNKNOWN;
  item->find("mode",pname,NAMELEN);
  str=pname;
  if (str == "GUIDER_CAMERA") tcamera.mode = GUIDER_CAMERA;
  else tcamera.mode = SCIENCE_CAMERA;
  item->find("cont_xo",tcamera.cont_xo,tcamera.n_controllers);
  item->find("cont_yo",tcamera.cont_yo,tcamera.n_controllers);
  item->find("cont_xgap",tcamera.cont_xgap,tcamera.n_controllers);
  item->find("cont_ygap",tcamera.cont_ygap,tcamera.n_controllers);
  item->find("cont_master",tcamera.cont_master,tcamera.n_controllers);
  item->find("cont_shutter",tcamera.cont_shutter,tcamera.n_controllers);
  item->find("cont_temp",tcamera.cont_temp,tcamera.n_controllers);
  for (i=0;i<tcamera.n_controllers;i++) {
    snprintf(pname, NAMELEN,"controller%d",i+1);
    str = "";
    if (item->find(pname,str)) {
      if (controller_table.find(str) == NULL) {
	ostr << "Failed to find controller: " << str.c_str() << ends;
	throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
      } else {
	// Make sure this controller is not already part of the camera,
	// ie no duplicates
	for (k=0; k<i; k++) {
	  if (str == tcamera.controller[k].name) {
	    ostr << "Duplicate controller " << str.c_str() << " for camera " << tcamera.name << ends;
	    throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
	  }
	}
	tcamera.controller[i] = controller_table[str.c_str()];
      }
    }      
    str = "";
    snprintf(pname, NAMELEN,"config%d",i+1);
    item->find(pname,str);
    strlcpy(tcamera.cont_config[i],str.c_str(),NAMELEN);
  }
  item->find("n_buffers",tcamera.n_buffers);
  if (tcamera.n_buffers<1)
    tcamera.n_buffers = 1;
  if (tcamera.n_buffers>MAX_READOUT_BUFFERS)
    tcamera.n_buffers = MAX_READOUT_BUFFERS;

  // Calculate rows and cols for camera
  calc_rows_cols(tcamera);


  item->find("mosaic_mode",pname,NAMELEN);
  str=pname;
  if (str == "MOSAIC_AMP") tcamera.mosaic_mode = MOSAIC_AMP;
  else if (str == "MOSAIC_CHIP") tcamera.mosaic_mode = MOSAIC_CHIP;
  else tcamera.mosaic_mode = MOSAIC_NONE;

  item->find("phu1",tcamera.phu1);
  item->find("phu2",tcamera.phu2);
  item->find("ihu",tcamera.ihu);

  camera = camera_save = tcamera; 
}
// Return a reference to the ith element in the table - updates internal buffer
// and sets flag
/*`
 *+ 
 * FUNCTION NAME: Camera_Desc& Camera_Table::operator[]
 * 
 * INVOCATION: operator[](int idx)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Camera_Desc& Camera_Table::operator[](int idx)
{
  get_element(idx);
  return camera;
}
/*
 *+ 
 * FUNCTION NAME: Camera_Desc& Camera_Table::operator[]
 * 
 * INVOCATION: operator[](const char* item_name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Return a reference to the element named in the table - updates internal buffer
 * and sets flag
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Camera_Desc& Camera_Table::operator[](const char* item_name)
{
  get_element(item_name);
  return camera;
}

/*
 *+ 
 * FUNCTION NAME: Camera_Table::~Camera_Table
 * 
 * INVOCATION: ~Camera_Table()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Camera_Table::~Camera_Table()
{
  if (need_update()) {
    update();
  }
}

/*
 *+ 
 * FUNCTION NAME: Controller_Table::Controller_Table():Config_Table("Controller Table")
 * 
 * INVOCATION: Controller_Table::Controller_Table
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Consructor
 * 
 * DESCRIPTION: 
 *  Controller Table
 *  The Controller table file is a text file consisting of single line descriptions
 *  of individual controllers. Text after the # symbol is ignored as a comment.
 *  Each Controller description is a set of fields separated by the ":".
 *  The format of a Controller line is
 *  controller_name:controller_type:device:addr:n_chips:chip1:chip_xo1:chip_yo1:..:chipN:chip_xoN:chip_yoN:comment
 *  where 
 *   controller_name: Name of this controller - text
 *   controller_type: Type of this controller - text
 *   device: Name of controller interface device - text
 *   addr: address of controller on bus
 *   n_chips: total CHIPs in camera
 *   chip1: name of CHIP 1 .. name of chipn
 *   chip_xo1: x offset of chip1 on camera
 *   chip_yo1: y offset of chip1 on camera
 *   comment: Further description of controller
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Controller_Table::Controller_Table():
  Config_Table("Controller Table", (void*)&controller, (void*)&controller_save, sizeof(Controller_Desc))
{
  controller.name[0] = '\0';
  controller_save = controller;
}
/*
 *+ 
 * FUNCTION NAME: 
 * 
 * INVOCATION: read(const string tname)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of table
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Controller_Table::read(const string tname)
{
  Filenametype fname;
  // Read "owned" tables
  strlcpy(fname,CHIP_TABLE,FILENAMELEN);
  chip_table.read(fname);
  // Now read our table
  Config_Table::read(tname);
}
/*
 *+ 
 * FUNCTION NAME: void Controller_Table::write
 * 
 * INVOCATION: write(const string tname, bool do_owned_tables)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - name of table
 * > do_owned_tables - Set if required to write tables that are owned by this table
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Controller_Table::write(const string tname, bool do_owned_tables)
{
  Filenametype fname;
  if (do_owned_tables) {
    // Write "owned" tables
    strlcpy(fname,CHIP_TABLE,FILENAMELEN);
    chip_table.write(fname);
  }
  // Now write our table
  Config_Table::write(tname);
}

/*
 *+ 
 * FUNCTION NAME: void Controller_Table::calc_size_and_mask
 * 
 * INVOCATION: calc_size_and_mask(Controller_Desc& tcontroller)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: calculate total size of detector controller reads, plus amp mask
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Controller_Table::calc_size_and_mask(Controller_Desc& tcontroller)
{
  int i;
  tcontroller.cols = 0;
  tcontroller.rows = 0;
  tcontroller.n_amps = 0;
  tcontroller.amp_mask = 0;
  for (i=0; i<tcontroller.n_chips; i++) {
    // Compute the amp_mask - use chip amp mask shifted to correct place
    tcontroller.amp_mask += (tcontroller.chip[i].amp_mask << tcontroller.n_amps);
    
    // Compute total number of amps
    tcontroller.n_amps += tcontroller.chip[i].n_amps;
    
    // Calculate number of cols - use chip position and size
    tcontroller.cols = (tcontroller.chip_xo[i]+tcontroller.chip[i].cols > 
		       tcontroller.cols)? 
      tcontroller.chip_xo[i]+tcontroller.chip[i].cols : tcontroller.cols;
    // Calculate number of rows - use chip position and size
    tcontroller.rows = (tcontroller.chip_yo[i]+tcontroller.chip[i].rows > 
		       tcontroller.rows)? 
      tcontroller.chip_yo[i]+tcontroller.chip[i].rows : tcontroller.rows;
  }
}

 // Add an entry to the table
/*
 *+ 
 * FUNCTION NAME: void Controller_Table::put
 * 
 * INVOCATION: put(void* data_item, int pos)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Controller_Table::put(void* data_item, int pos)
{
  Table_Item* item;
  ostringstream ostr;
  Controller_Desc* tcontroller= (Controller_Desc*) data_item;
  Nametype pname;
  int i;
  string str;

  if ((item = find(tcontroller->name)) != NULL) {
    ostr << "Controller " << tcontroller->name << " already exists in table!" << ends;
    throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);     
  }

  item = new Table_Item(tcontroller->name);
  
  // Now for each possible controller parameter check that it differs from default
  item->put("host",tcontroller->host);
  switch (tcontroller->type) {
  case APOGEE: str = "APOGEE"; break;
  case DUMMY: str = "DUMMY"; break;
  case SDSU2: str = "SDSU2"; break;
  case SDSU3: str = "SDSU3"; break;
  case SDSU3_5: str = "SDSU3_5"; break;
  case CONTROLLER_UNKNOWN: str = "CONTROLLER_UNKNOWN"; break;
  }
  item->put("type",str);
  item->put("device",tcontroller->device);
  switch (tcontroller->bus) {
  case BUS_SBUS: str = "SBUS"; break;
  case BUS_PCI: str = "PCI"; break;
  case BUS_VME: str = "VME"; break;
  case BUS_ISA: str = "ISA"; break;
  case BUS_PCIe: str = "PCIe"; break;
  default: str = "UNKNOWN"; break;
  }
  item->put("bus",str);
  item->put("address",tcontroller->address);
  item->put("hwv",tcontroller->hwv);
  item->put("mask_chips", tcontroller->mask_chips );
  item->put("n_chips",tcontroller->n_chips);
  for (i=0; i<tcontroller->n_chips; i++) {
    snprintf(pname, NAMELEN,"chip%d",i+1);
    item->put(pname,tcontroller->chip[i].name);
  }
  if (tcontroller->n_chips>0) {
    item->put("chip_xo",tcontroller->chip_xo,tcontroller->n_chips);
    item->put("chip_yo",tcontroller->chip_yo,tcontroller->n_chips);
    item->put("chip_xgap",tcontroller->chip_xgap,tcontroller->n_chips);
    item->put("chip_ygap",tcontroller->chip_ygap,tcontroller->n_chips);
  }
  item->put("verify_spec.constant_val",tcontroller->verify_spec.constant_val);            
  item->put("verify_spec.seed",tcontroller->verify_spec.seed);                     
  item->put("verify_spec.amp_increment",tcontroller->verify_spec.amp_increment);            
  item->put("verify_spec.increment",tcontroller->verify_spec.increment);                
  item->put("verify_spec.readout_increment",tcontroller->verify_spec.readout_increment);        
  item->put("verify_spec.overscan",tcontroller->verify_spec.overscan);                 
  if (strlen(tcontroller->comment)>0) item->put("comment",tcontroller->comment);

  insert(item,pos);

  // Cater for situations where put is called with external item
  // Usually will simply be putting back value in internal buffer 
  if ((void*)&controller != data_item) {
    controller = controller_save = *tcontroller;
  }
}

/*
 *+ 
 * FUNCTION NAME: void Controller_Table::get
 * 
 * INVOCATION: get(Table_Item *item)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Controller_Table::get(Table_Item *item)
{
  Controller_Desc tcontroller;
  string str;
  int i,k;
  ostringstream ostr;
  Nametype pname;

  // Set desc to null values
  memset((void*)&tcontroller, 0, sizeof(Controller_Desc));

  strlcpy(tcontroller.name, item->name.c_str(), NAMELEN);
  // Now for each possible controller parameter try to find in table

  item->find("host", tcontroller.host, NAMELEN);
  item->find("type",pname,NAMELEN);
  str=pname;
  if (str == "DUMMY") tcontroller.type = DUMMY;
  else if (str == "SDSU2") tcontroller.type = SDSU2;
  else if (str == "SDSU3") tcontroller.type = SDSU3;
  else if (str == "SDSU3") tcontroller.type = SDSU3;
  else if (str == "SDSU3_5") tcontroller.type = SDSU3_5;
  else tcontroller.type = CONTROLLER_UNKNOWN;
  item->find("device", tcontroller.device, NAMELEN);
  item->find("bus", pname, NAMELEN);
  str=pname;
  if (str == "SBUS") tcontroller.bus = BUS_SBUS;
  else if (str == "PCI") tcontroller.bus = BUS_PCI;
  else if (str == "PCIe") tcontroller.bus = BUS_PCIe;
  else if (str == "VME") tcontroller.bus = BUS_VME;
  else if (str == "ISA") tcontroller.bus = BUS_ISA;
  else tcontroller.bus = BUS_UNKNOWN;
  item->find("address", tcontroller.address);
  item->find("hwv",tcontroller.hwv, NAMELEN);
  item->find("mask_chips", tcontroller.mask_chips);
  item->find("n_chips", tcontroller.n_chips);
  item->find("chip_xo",tcontroller.chip_xo,tcontroller.n_chips);
  item->find("chip_yo",tcontroller.chip_yo,tcontroller.n_chips);
  item->find("chip_xgap",tcontroller.chip_xgap,tcontroller.n_chips);
  item->find("chip_ygap",tcontroller.chip_ygap,tcontroller.n_chips);
  for (i=0;i<tcontroller.n_chips;i++) {
    snprintf(pname, NAMELEN,"chip%d",i+1);
    if (item->find(pname,str)) {
      if (chip_table.find(str) == NULL) {
	ostr << "Failed to find chip: " << str.c_str() << ends;
	throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
      } else {
	// Make sure this chip is not already part of the controller,
	// ie no duplicates
	for (k=0; k<i; k++) {
	  if (str == tcontroller.chip[k].name) {
	    ostr << "Duplicate chip " << str.c_str() << " for controller " << tcontroller.name << ends;
	    throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
	  }
	}
	tcontroller.chip[i] = chip_table[str.c_str()];
      }      
    }
  }
  calc_size_and_mask(tcontroller);

  item->find("verify_spec.constant_val",tcontroller.verify_spec.constant_val);            
  item->find("verify_spec.seed",tcontroller.verify_spec.seed);                     
  item->find("verify_spec.amp_increment",tcontroller.verify_spec.amp_increment);            
  item->find("verify_spec.increment",tcontroller.verify_spec.increment);                
  item->find("verify_spec.readout_increment",tcontroller.verify_spec.readout_increment);        
  item->find("verify_spec.overscan",tcontroller.verify_spec.overscan);                 
  item->find("comment", tcontroller.comment, COMMENTLEN);

  controller = controller_save = tcontroller; 
}
// Return a reference to the ith element in the table - updates internal buffer
// and sets flag
/*
 *+ 
 * FUNCTION NAME: Controller_Desc& Controller_Table::operator[]
 * 
 * INVOCATION: operator[](int idx)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Controller_Desc& Controller_Table::operator[](int idx)
{
  get_element(idx);
  return controller;
}
// Return a reference to the element named in the table - updates internal buffer
// and sets flag
/*
 *+ 
 * FUNCTION NAME: Controller_Desc& Controller_Table::operator[]
 * 
 * INVOCATION: operator[](const char* item_name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Controller_Desc& Controller_Table::operator[](const char* item_name)
{
  get_element(item_name);
  return controller;
}

/*
 *+ 
 * FUNCTION NAME: Controller_Table::~Controller_Table
 * 
 * INVOCATION: ~Controller_Table()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Controller_Table::~Controller_Table()
{
  if (need_update()) {
    update();
  }
}

