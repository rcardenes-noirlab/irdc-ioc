[schematic2]
uniq 125
[tools]
[detail]
w 632 -357 100 0 n#87 estringouts.simStrOut.OUT 368 -432 432 -432 432 -368 880 -368 880 -464 864 -464 esirs.simMode.VAL
w 184 -517 100 0 n#86 embbis.simMenuOut.FLNK 288 -640 352 -640 352 -528 64 -528 64 -416 112 -416 estringouts.simStrOut.SLNK
w 152 -549 100 0 n#85 embbis.simMenuOut.VAL 288 -672 320 -672 320 -560 32 -560 32 -384 112 -384 estringouts.simStrOut.DOL
w 400 -581 100 0 n#84 estringouts.simStrOut.FLNK 368 -400 400 -400 400 -592 448 -592 esirs.simMode.SLNK
s 928 -1008 500 512 GNIRS DC System Status/Alarm Database
[cell use]
use dcHealthSad 1408 -137 100 0 dcHealthSad#124
xform 0 1520 0
use esirs -1280 -217 100 0 dataState
xform 0 -1072 -64
p -1184 -240 100 0 1 DESC:Data task state
p -1344 -416 100 0 0 EGU:0/1
p -1329 -336 100 0 0 FDSC:Data task state
p -1184 -304 100 0 1 FTVL:LONG
p -1184 -272 100 0 1 PV:$(sadtop)
p -1184 -336 100 0 1 SNAM:
use esirs 1328 727 100 0 sdsuSnlState
xform 0 1536 880
p 1424 704 100 0 1 DESC:Debugging mode
p 1279 608 100 0 0 FDSC:Debugging mode
p 1424 672 100 0 1 FTVL:STRING
p 1424 608 100 0 1 PV:$(sadtop)
p 1424 640 100 0 1 SNAM:
use esirs 816 727 100 0 obsSnlState
xform 0 1024 880
p 912 704 100 0 1 DESC:Debugging mode
p 767 608 100 0 0 FDSC:Debugging mode
p 912 672 100 0 1 FTVL:STRING
p 912 608 100 0 1 PV:$(sadtop)
p 912 640 100 0 1 SNAM:
use esirs 320 727 100 0 dcSnlState
xform 0 528 880
p 416 704 100 0 1 DESC:Debugging mode
p 271 608 100 0 0 FDSC:Debugging mode
p 416 672 100 0 1 FTVL:STRING
p 416 608 100 0 1 PV:$(sadtop)
p 416 640 100 0 1 SNAM:
use esirs 448 -681 100 0 simMode
xform 0 656 -528
p 544 -704 100 0 1 DESC:Simulation mode
p 399 -800 100 0 0 FDSC:Simulation mode
p 544 -736 100 0 1 FTVL:STRING
p 544 -800 100 0 1 PV:$(sadtop)
p 544 -768 100 0 1 SNAM:
use esirs -1296 -697 100 0 historyLog
xform 0 -1088 -544
p -1200 -720 100 0 1 DESC:History log record
p -1345 -816 100 0 0 FDSC:History log record
p -1200 -752 100 0 1 FTVL:STRING
p -1200 -816 100 0 1 PV:$(sadtop)
p -1200 -784 100 0 1 SNAM:
use esirs -1296 743 100 0 name
xform 0 -1088 896
p -1200 720 100 0 1 DESC:Detector Controller Name
p -1345 320 100 0 0 EGU: 
p -1200 720 100 0 0 FDSC:Detector Controller Name
p -1200 688 100 0 1 FTVL:STRING
p -1200 624 100 0 1 PV:$(sadtop)
p -1200 656 100 0 1 SNAM:
use esirs -1296 263 100 0 state
xform 0 -1088 416
p -1200 240 100 0 1 DESC:Detector Controller state
p -1345 -160 100 0 0 EGU: 
p -1345 144 100 0 0 FDSC:Detector Controller state
p -1200 208 100 0 1 FTVL:STRING
p -1200 144 100 0 1 PV:$(sadtop)
p -1200 176 100 0 1 SNAM:
p -1024 208 100 0 1 VAL:BOOTING
use esirs -768 -697 100 0 rdout
xform 0 -560 -544
p -672 -720 100 0 1 DESC:Detector readout flag
p -832 -896 100 0 0 EGU:0/1
p -817 -816 100 0 0 FDSC:Readout flag
p -672 -752 100 0 1 FTVL:LONG
p -672 -816 100 0 1 PV:$(sadtop)
p -672 -784 100 0 1 SNAM:
use esirs -768 -217 100 0 acq
xform 0 -560 -64
p -672 -240 100 0 1 DESC:Acquisition flag
p -832 -416 100 0 0 EGU:0/1
p -817 -336 100 0 0 FDSC:Acquisition flag
p -672 -272 100 0 1 FTVL:LONG
p -672 -336 100 0 1 PV:$(sadtop)
p -672 -304 100 0 1 SNAM:
use esirs -768 263 100 0 prep
xform 0 -560 416
p -672 240 100 0 1 DESC:Preparation flag
p -832 64 100 0 0 EGU:0/1
p -817 144 100 0 0 FDSC:Preparation flag
p -672 208 100 0 1 FTVL:LONG
p -672 144 100 0 1 PV:$(sadtop)
p -672 176 100 0 1 SNAM:
use esirs -768 743 100 0 heartBeat
xform 0 -560 896
p -672 720 100 0 1 DESC:Detector Controller Heart Beat
p -832 480 100 0 0 FDSC:Detector Controller Heart Beat
p -672 688 100 0 1 FTVL:LONG
p -672 624 100 0 1 PV:$(sadtop)
p -672 656 100 0 1 SNAM:
use esirs -192 727 100 0 debugMode
xform 0 16 880
p -96 704 100 0 1 DESC:Debugging mode
p -241 608 100 0 0 FDSC:Debugging mode
p -96 672 100 0 1 FTVL:STRING
p -96 608 100 0 1 PV:$(sadtop)
p -96 640 100 0 1 SNAM:
use esirs -204 263 100 0 configName
xform 0 4 416
p -108 240 100 0 1 DESC:Configuration name
p -268 64 100 0 0 EGU:0/1
p -253 144 100 0 0 FDSC:Camera configuration name
p -108 208 100 0 1 FTVL:STRING
p -108 144 100 0 1 PV:$(sadtop)
p -108 176 100 0 1 SNAM:
use esirs 320 263 100 0 slaveState
xform 0 528 416
p 416 240 100 0 1 DESC:Slave task state
p 256 64 100 0 0 EGU:0/1
p 271 144 100 0 0 FDSC:Slave task state
p 416 176 100 0 1 FTVL:LONG
p 416 208 100 0 1 PV:$(sadtop)
p 416 144 100 0 1 SNAM:
use estringouts 192 -352 100 0 simStrOut
xform 0 240 -416
p 160 -496 100 0 1 OMSL:closed_loop
p 160 -512 100 0 1 PV:$(sadtop)
use elongins -160 1143 100 0 connection
xform 0 -32 1216
p -80 1088 100 0 1 PV:$(sadtop)
use elongins -864 1143 100 0 stopping
xform 0 -736 1216
p -784 1088 100 0 1 PV:$(sadtop)
use elongins -1232 1143 100 0 aborting
xform 0 -1104 1216
p -1168 1104 100 0 1 PV:$(sadtop)
use elongins -512 1143 100 0 saving
xform 0 -384 1216
p -432 1088 100 0 1 PV:$(sadtop)
use embbis 96 -592 100 0 simMenuOut
xform 0 160 -656
p 96 -768 100 0 1 ONST:FAST
p 96 -832 100 0 1 PV:$(sadtop)
p 96 -800 100 0 1 TWST:FULL
p 96 -736 100 0 1 ZRST:NONE
use bc200tr -1504 -1192 -100 0 frame
xform 0 176 112
p 1072 -1024 100 0 1 author:P.Young
p 1296 -1040 100 0 -1 border:C
p 1072 -1056 100 0 1 checked:
p 1328 -1040 100 0 -1 date:2001-02-21
p 1312 -912 100 0 -1 project:Gemini GNIRS DC
p -1504 -1144 100 0 0 revision:1.0
p 1312 -976 150 0 -1 title:dcSysSad.sch
[comments]
