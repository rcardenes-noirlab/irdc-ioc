[schematic2]
uniq 121
[tools]
[detail]
s 688 -1056 500 512 GNIRS DC SDSU Status/Alarm Database
[cell use]
use esirs -128 455 100 0 SDSUpon
xform 0 80 608
p -48 416 100 0 1 DESC:SDSU power on flag
p -48 384 100 0 1 FTVL:LONG
p -48 352 100 0 1 PV:$(sadtop)
use esirs 1120 -41 100 0 SDSUntests
xform 0 1328 112
p 1200 -80 100 0 1 DESC:Number of TDL tests to run
p 1200 -112 100 0 1 FTVL:LONG
p 1200 -144 100 0 1 PV:$(sadtop)
use esirs -736 935 100 0 detType
xform 0 -528 1088
p -640 912 100 0 1 DESC:Detector array type
p -800 736 100 0 0 EGU:
p -800 672 100 0 0 FDSC:Detector array type
p -640 880 100 0 1 FTVL:STRING
p -640 816 100 0 1 PV:$(sadtop)
p -640 848 100 0 1 SNAM:
use esirs -736 455 100 0 detID
xform 0 -528 608
p -640 432 100 0 1 DESC:Detector chip identifier
p -800 256 100 0 0 EGU:ID code
p -800 192 100 0 0 FDSC:Detector chip identifier
p -640 400 100 0 1 FTVL:STRING
p -640 336 100 0 1 PV:$(sadtop)
p -640 368 100 0 1 SNAM:
use esirs -736 7 100 0 replyStatus
xform 0 -528 160
p -640 -32 100 0 1 DESC:Status of last SDSU operation
p -640 -96 100 0 1 EGU:number
p -800 -256 100 0 0 FDSC:Status of last SDSU operation
p -640 -64 100 0 1 FTVL:LONG
p -640 -160 100 0 1 PV:$(sadtop)
p -640 -128 100 0 1 SNAM:
use esirs -1312 935 100 0 initSDSUDone
xform 0 -1104 1088
p -1216 896 100 0 1 DESC:SDSU controller initialized
p -1216 832 100 0 1 EGU:number
p -1376 672 100 0 0 FDSC:SDSU controller initialized
p -1216 864 100 0 1 FTVL:LONG
p -1216 768 100 0 1 PV:$(sadtop)
p -1216 800 100 0 1 SNAM:
use esirs -1312 455 100 0 SDSUcmdRunning
xform 0 -1104 608
p -1216 416 100 0 1 DESC:SDSU controller command running
p -1216 352 100 0 1 EGU:number
p -1376 192 100 0 0 FDSC:SDSU controller initializing
p -1216 384 100 0 1 FTVL:LONG
p -1216 320 100 0 1 PV:$(sadtop)
p -1216 320 100 0 0 SNAM:
use esirs -1312 7 100 0 statusMsg
xform 0 -1104 160
p -1232 -32 100 0 1 DESC:SDSU DSP command status message
p -1232 -64 100 0 1 FTVL:STRING
p -1232 -96 100 0 1 PV:$(sadtop)
use esirs -128 -473 100 0 SDSUarg1
xform 0 80 -320
p -48 -512 100 0 1 DESC:SDSU DSP command argument 1
p -48 -544 100 0 1 FTVL:LONG
p -48 -576 100 0 1 PV:$(sadtop)
use esirs 544 -41 100 0 SDSUaddress
xform 0 752 112
p 624 -80 100 0 1 DESC:SDSU memory address
p 624 -112 100 0 1 FTVL:LONG
p 624 -144 100 0 1 PV:$(sadtop)
use esirs 544 455 100 0 SDSUmemSpace
xform 0 752 608
p 624 416 100 0 1 DESC:SDSU memory space identifier
p 624 384 100 0 1 FTVL:LONG
p 624 352 100 0 1 PV:$(sadtop)
use esirs 544 935 100 0 SDSUboard
xform 0 752 1088
p 624 896 100 0 1 DESC:SDSU board identifier
p 624 864 100 0 1 FTVL:LONG
p 624 832 100 0 1 PV:$(sadtop)
use esirs -128 23 100 0 timingDSP
xform 0 80 176
p -32 0 100 0 1 DESC:Name of timing DSP code
p -192 -176 100 0 0 EGU:DSP name
p -192 -240 100 0 0 FDSC:Name of timing DSP code
p -32 -32 100 0 1 FTVL:STRING
p -32 -96 100 0 1 PV:$(sadtop)
p -32 -64 100 0 1 SNAM:
use esirs -736 -473 100 0 DSPVer
xform 0 -528 -320
p -640 -496 100 0 1 DESC:Version of timing DSP code
p -800 -672 100 0 0 EGU:DSP version
p -800 -736 100 0 0 FDSC:Version of timing DSP code
p -640 -528 100 0 1 FTVL:STRING
p -640 -592 100 0 1 PV:$(sadtop)
p -640 -560 100 0 1 SNAM:
use esirs -128 935 100 0 intfDSP
xform 0 80 1088
p -32 912 100 0 1 DESC:Name of SDSU interface DSP code
p -192 736 100 0 0 EGU:DSP name
p -192 672 100 0 0 FDSC:Name of SDSU interface DSP code
p -32 880 100 0 1 FTVL:STRING
p -32 816 100 0 1 PV:$(sadtop)
p -32 848 100 0 1 SNAM:
use esirs 544 -473 100 0 SDSUarg2
xform 0 752 -320
p 624 -512 100 0 1 DESC:SDSU DSP command argument 2
p 624 -544 100 0 1 FTVL:LONG
p 624 -576 100 0 1 PV:$(sadtop)
use esirs 1120 -473 100 0 SDSUdebug
xform 0 1328 -320
p 1200 -512 100 0 1 DESC:SDSU debug command argument
p 1200 -544 100 0 1 FTVL:LONG
p 1200 -576 100 0 1 PV:$(sadtop)
use esirs 1120 455 100 0 SDSUcomms
xform 0 1328 608
p 1200 416 100 0 1 DESC:SDSU communications flag
p 1200 384 100 0 1 FTVL:LONG
p 1200 352 100 0 1 PV:$(sadtop)
use estringinval 624 -841 100 0 cameraOpMsg
xform 0 752 -768
p 678 -602 100 0 0 DESC:Message about camera operation
p 688 -880 100 0 1 PV:$(sadtop)
use elongins -96 -841 100 0 cancelingSDSU
xform 0 32 -768
p -32 -880 100 0 1 PV:$(sadtop)
use elongins 272 -841 100 0 cameraOp
xform 0 400 -768
p 61 -567 100 0 0 DESC:Flag -> successful camera operation
p 336 -880 100 0 1 PV:$(sadtop)
use dcSDSUDSPSad -1216 -409 100 0 dcSDSUDSPSad#98
xform 0 -1104 -304
use dcSDSUTestSad -1216 -825 -100 0 dcSDSUTestSad
xform 0 -1104 -720
use bc200tr -1504 -1192 -100 0 frame
xform 0 176 112
p 1072 -1024 100 0 1 author:P. Young
p 1296 -1040 100 0 -1 border:C
p 1072 -1056 100 0 1 checked:Peter Young
p 1328 -1040 100 0 -1 date:2001-02-21
p 1312 -912 100 0 -1 project:Gemini GNIRS DC
p -1504 -1144 100 0 0 revision:1.1
p 1312 -976 150 0 -1 title:dcSDSUSad.sch
[comments]
