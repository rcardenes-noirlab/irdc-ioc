/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_cadSeq.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI NIFS PROJECT
 *
 * FILENAME cad_seq.c
 *
 * GENERAL DESCRIPTION
 *
 * Handle CADS from commands that are ignored by the NIFS DC.  These are DATUM,
 * GUIDE, ENDGUIDE, VERIFY, ENDVERIFY and ENDOBSERVE.  Their equivalent CAR
 * records will be set to busy and then back to idle.
 *
 *
 * $Log: dc_cadSeq.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.13  2005/09/07 00:48:07  pjy
 * Moved message_write to dc.cc
 *
 * Revision 1.12  2005/09/06 22:00:35  pjy
 * Changed timemode to allow for string output SIR
 *
 * Revision 1.11  2005/06/07 07:44:00  pjy
 * include dc_epics_db
 *
 * Revision 1.10  2005/06/07 06:49:28  pjy
 * Remove hard coded nifs string
 *
 * Revision 1.9  2005/02/25 04:48:39  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.8  2004/12/22 06:09:56  pjy
 * DC cad src files ready for first Gemini release
 *
 * Revision 1.7  2004/08/20 02:27:04  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.6  2004/04/27 07:25:57  pjy
 * T2.2
 *
 * Revision 1.1  2004/03/23 01:59:52  pjy
 * Gem 8.6 port
 *
 * Revision 1.3  2003/09/19 06:24:22  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.2  2003/03/25 06:26:21  pjy
 * Consistent naming using _
 *
 * Revision 1.1  2003/03/11 01:29:31  pjy
 * Import modules
 *
 *
 */
/* VxWorks include files */
#include <cstdio>
#include <cstring>
#include <cmath>

#ifdef __cplusplus
extern "C" {
#endif
#include "genSubRecord.h"
#include "cadRecord.h"
#include "sirRecord.h"
#include "cad.h"
#include "dbAccess.h"
#include "dbCommon.h"
#ifdef __cplusplus
}
#endif

#include "logging.h"
#include "dc_epics_db.h"
#include "parameter.h"
#include "configuration.h"
#include "ir_setup.h"
#include "ir_camera.h"
#include "utility.h"

// Globals

//+ The camera
extern IR_Camera *ir_camera;                      //+ The infra red camera object - created by gnirsSlave task
extern bool camera_setup_changed;                 //+ Flag set when any readout setting change

/* Function declarations */
#ifdef __cplusplus
extern "C" long dcCADseq(struct cadRecord *pCad);
extern "C" long dcCADseqInit(struct cadRecord *pCad);
extern "C" void set_and_verify_output_cad(struct cadRecord *pCad);
extern "C" void load_input_cad(struct cadRecord *pCad);
extern "C" long timeModeCalc(struct sirRecord *pSir);
#endif

/*
 *+
 * FUNCTION NAME:
 * dcCADseq
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADseq( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for seq CAD records
 *
 * DESCRIPTION:
 * This routine is called whenever a sequence CAD record is processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADseq(struct cadRecord *pCad)
{
  long status;                          //+ return status

  status = CAD_ACCEPT;

  logger::message(logger::Level::Full,"dcCADseq: %d directive.", pCad->dir);

  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:
    break;

  case menuDirectivePRESET:
    /* Reject the command if the arguments don't validate */
    try {
      // Verify input parameters and set outputs
      set_and_verify_output_cad(pCad);
      camera_setup_changed = true;
      logger::message(logger::Level::Full,"dcCADseq: Seq Cad verified ok!");
    }
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;

  case menuDirectiveSTART:

    break;

  case menuDirectiveSTOP:			/* Really can't	*/
    strncpy(pCad->mess, "dcCADseq: Cannot stop!", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;

  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADseq: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  }
  return status;
}
/*
 *+
 * FUNCTION NAME:
 * dcCADseqInit
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADseqInit( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined inam function for seq CAD records
 *
 * DESCRIPTION:
 * This routine is called whenever an sequence CAD record is loaded.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADseqInit(struct cadRecord *pCad)
{
  long status;                     /* return status */

  status = CAD_ACCEPT;

  // Call the routine to load the input fields
  try {
    load_input_cad(pCad);
  }
  catch (Error& err) {
    status = CAD_REJECT;
    err.print_error(__FILE__, __LINE__);
    strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
  }

  return status;
}
/*
 *+
 * FUNCTION NAME:
 * timeModeCalc
 *
 * INVOCATION:
 * struct sirRecord *pSir;
 * long status;
 *
 * status = timeModeCalc(*pSir)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pSir   (struct sirRecord *)   pointer to SIR data structure
 *
 * FUNCTION VALUE:
 * long  Status value, 0 for success
 *
 * PURPOSE:
 * User defined function for calculating exposure time/period/nperiods based on
 * chosen timeMode method.
 *
 * DESCRIPTION:
 * This routine is called whenever the appropriate SIR records are processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * The Sir record needing this routine must have its SNAM property set to
 * timeModeCalc.
 *
 * DEFICIENCIES:
 * None known
 *
 * HISTORY (optional):
 *
 *-
 */


long timeModeCalc(struct sirRecord *pSir)
{
  long status = OK;
  long timeMode;
  Parameter *ir_statusp;                //+ IR parameter DB
  string mode;                          //+ mode either obs or view
  IR_Exposure_Info *ir_einfo;           //+ Pointer to current ir exposure info structure
  string ViewTimeMode = string(DCSADTOP)+string("view_timeMode");//+ Name of View mode timeMode parameter

  try {

    if (pSir->val == NULL)
      throw Error("NULL pointer found on SIR ", E_ERROR, -1, __FILE__, __LINE__);

    // Make sure data types have been specified correctly
    if (pSir->ftvl != DBF_STRING)
      throw Error("Wrong data type specified on SIR VAL", E_ERROR, -1, __FILE__, __LINE__);

    // Now work out exposure time/period/#periods depending on mode chosen
    if (ir_camera != NULL) {
      ir_statusp = ir_camera->ir_statusp;
      if (!strcmp(pSir->name, ViewTimeMode.c_str())) {
	mode = "view_";
	ir_einfo = &ir_camera->ir_status->view;
      } else {
	mode = "obs_";
	ir_einfo = &ir_camera->ir_status->obs;
      }
      // Get current values required to calculate timings
/*
      ir_statusp->get(mode + TIME_MODE, timeMode);
      ir_einfo->timeModeSet = (IR_Timing_Mode) timeMode;
      ir_statusp->get(mode + READ_TIME, ir_einfo->readTime);
      ir_statusp->get(mode + READ_INTVAL, ir_einfo->readIntvl);
      ir_statusp->get(mode + PERIOD, ir_einfo->period);
      ir_statusp->get(mode + NPERIODS, ir_einfo->nperiods);
      ir_statusp->get(mode + NFOWLER, ir_einfo->nfowler);
      ir_statusp->get(mode + SET_INTERVAL, ir_einfo->setIntvl);
*/
      ir_statusp->get(mode + IR_EXPOSEDRQ, ir_einfo->exposedRQ);

      // Do the calculation based on chosen mode
      ir_camera->calc_exposure_time(ir_einfo);

      // Put calculated values back to SAD
/*
      ir_statusp->put(mode + READ_INTVAL, ir_einfo->readIntvl);
      ir_statusp->put(mode + PERIOD, ir_einfo->period);
      ir_statusp->put(mode + NPERIODS, ir_einfo->nperiods);
*/
      ir_statusp->put(mode + IR_EXPOSEDRQ, ir_einfo->exposedRQ);
    }

  }
  catch (Error& err) {
    status = err.errnum;
    logger::message(logger::Level::NoLog, "timeModeCalc: Error calculating exposure times, SIR '%s'", pSir->name);
    logger::message(logger::Level::NoLog, "timeModeCalc: %s", err.record_error(__FILE__, __LINE__));
  }

  return status;
}
