/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * CICADA PROJECT
 * 
 * FILENAME table.cc
 * 
 * GENERAL DESCRIPTION
 * General config table reader/writer. Makes use of STL map and set collection
 * classes Each table is a collection consisting of an item name plus a map
 * of key value pairs
 *
 * REQUIREMENTS
 * Requires use of STL classes "map", "set" and "string"
 * 
 */

/* Include files */
// #include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "utility.h"
#include "exception.h"
#include "table.h"
#include <iterator>

/* defines */

/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/* global function definitions */

/*
 *+ 
 * FUNCTION NAME: Table_Item constructors
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > item_name - char string, name of item to be added to table
 * > sep - string - parameter value separator string
 * > eop - string - parameter value ending strring/delimeter
 * > ns - Whether or not items are sorted by name
 * > sk - Sort key if NOT sorted by name (the STL set class needs a sort key)
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Initialises objects of class Table_Item. Can either be constructed
 *  with or without a name. sep and eop take default values.
 * 
 * DESCRIPTION: Sets name, separator, end_of_par member vars and instantiates
 *  a new hash table for param/value pairs.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Table_Item::Table_Item(string sep, string eop, bool ns, int sk)
  :
  separator(sep),
  end_of_par(eop),
  name(""),
  namesort(ns),
  sortkey(sk)
{
}
Table_Item::Table_Item(const char* item_name, string sep, string eop, bool ns, int sk)
  :
  separator(sep),
  end_of_par(eop),
  name(item_name),
  namesort(ns),
  sortkey(sk)
{
  int i;
  // replace any whitespace characters - names must not have any!
  strip_str(name,STRIP_BOTH);
  strip_str(name,STRIP_BOTH, '\t');
  while ((i = name.find_first_of(" \t")) != -1)
    name.replace(i,1,"_");
}

static std::string trim(const std::string& str) {
	const std::string whitespace = " \t";
	const auto end = str.find_last_not_of(whitespace);

	return str.substr(0, end+1);
}

/*
 *+ 
 * FUNCTION NAME: void Table_Item::read(string line)
 * 
 * INVOCATION: item->read(line)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > line - string, input line of text to parse for param/value pairs
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Parses a line of text searching for param/value pairs to add 
 *  to hash table.
 * 
 * DESCRIPTION: Uses the separator and end_of_par delimeters to perform parsing.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Table_Item::read(string line)
{
  int pos, l;
  ostringstream ostr;
  string sep = "([" + separator + "])";

  Tokenizer tokens(trim(line));
  string parval = tokens(end_of_par);
  strip_str(parval, STRIP_BOTH);
  strip_str(parval, STRIP_BOTH, '\t');
  while (parval.length()>0) {
    if ((pos = regx_match(parval, sep, l)) == -1) {
      ostr << "Failed to find parameter - illegal table entry - " << parval << ends;
      throw Error(ostr,E_ERROR,pos,__FILE__,__LINE__);
    }
    string par = parval.substr(0,pos); // extract parameter name
    strip_str(par, STRIP_BOTH);// strip leading & trailing space
    strip_str(par, STRIP_BOTH, '\t');// strip leading & trailing space
    string val = parval.substr(pos+1,parval.length()-(pos + 1));// ditto for value
    strip_str(val, STRIP_BOTH);
    strip_str(val, STRIP_BOTH, '\t');


    // Now add to hash dictionary
    pars[str_tolower(par)] = val;
    // cout << par << "=" << val;
    // cout << " current size=" << pars.size() << " max_size=" << pars.max_size() << endl;

    // Get next par
    parval = tokens(end_of_par);
    strip_str(parval, STRIP_BOTH);
    strip_str(parval, STRIP_BOTH, '\t');
  } 
 }
/*
 *+ 
 * FUNCTION NAME: void Table_Item::write()
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: None 
 * 
 * DESCRIPTION: Unimplemented
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Table_Item::write()
{
}

/*
 *+ 
 * FUNCTION NAME: bool Table_Item::find(const string par, string& val)
 *
 * INVOCATION: t = item->find(par,val)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > par - char string, name of parameter to find in hash table
 * < val - ref to output value
 * 
 * FUNCTION VALUE: bool - set if par found
 * 
 * PURPOSE: Gets string value associated with parameter name
 * 
 * DESCRIPTION: Different version of this method for string values. As the maps
 * is based on the string data type - just return the corresponding value in the
 * map. Leaves val unmodified if parameter not found.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
bool Table_Item::find(const string par, string& val)
{
  Table_Parameter_Map::iterator iter;
  // We store all parms as lower case, ie ignore case
  string lpar = str_tolower(par);


  if ((iter = pars.find(lpar)) != pars.end()) {
    val = (*iter).second;
    return true;
  } else
    return false;
}

/*
 *+ 
 * FUNCTION NAME: Table_Item::print
 * 
 * INVOCATION: print()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Lists to stdout the contents of the table_items par map
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Table_Item::print()
{
  Table_Parameter_Map::iterator par_iter;
  
  par_iter = pars.begin();
  while (par_iter != pars.end()) {
    cout << (*par_iter).first << " = " << (*par_iter).second << endl;
    par_iter++;
  }
}
/*
 *+ 
 * FUNCTION NAME: Table_Item::~Table_Item()
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Destructor, clears hash and deletes object
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Table_Item::~Table_Item()
{
  pars.clear();
}

/*
 *+ 
 * FUNCTION NAME: Table class constructors
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > tname - char string, name of table
 * > il - number of items to format on single output line
 * > ns - Whether or not items are sorted by name
 * > ire - string - regular expression string that defines item name
 * > is - string - item name start delimiter
 * > ie - string - item name end delimiter
 * > cc - string - comment delimiter
 * > sep - string - parameter value separator string
 * > eop - string - parameter value ending strring/delimeter
 * > dp - pointer to item cache
 * > dps - pointer to item cache save
 * > sz - size of data cache
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Initialises objects of class Table. Can either be constructed
 *  with or without a name. ire,is,ie,cc,sep and eop take default values.
 * 
 * DESCRIPTION: Sets name, item regular exp, delimiters and separator, end_of_par member vars 
 *  and clears the item linked list. Defaults are item desc = [item], comment = "#", sep = "=", eop = ";"
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Table::Table(const string tname, const int il, const bool ns, 
	     const string ire,  const string is,  const string ie, 
	     const string cc,  const string sep, const string eop,
	     const void* dp, const void* dps, const int sz) 
{
  table_name = tname;
  item_regexp = ire;
  item_start = is;
  item_end = ie;
  comment_char = cc;
  separator = sep;
  end_of_par = eop;
  items_per_line = il;
  items.clear();
  last_name = "";
  last_pos = 0;
  n_items = 0;
  namesort = ns;
  set_data_cache(dp,dps,sz);
}
/*
 *+ 
 * FUNCTION NAME: Table::operator=(const Table t)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > fname - name of table file to copy
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * Assignment operator
 * 
 * DESCRIPTION: Make sure old items deleted, new items created and hash tables
 *  copied.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Table& Table::operator=(const Table& t)
{
  Table_Item *this_item, *new_item;
  Table_Parameter_Map::iterator par_iter;
  Table_Item_Set::const_iterator item_iter;
  ostringstream ostr;

  // Clear old list and copy new list
  // NB do not copy data address
  clear();
  table_name = t.table_name;
  last_name = t.last_name;
  last_pos = t.last_pos;
  for (item_iter=t.items.begin(); (item_iter != t.items.end()); item_iter++) {
    this_item = *item_iter;
    new_item = new Table_Item((char*)this_item->name.c_str(),separator,end_of_par,namesort,n_items);
    // Loop through hash table for item, inserting into copied hash table
    par_iter = this_item->pars.begin();
    while (par_iter != this_item->pars.end()) {
      new_item->put((*par_iter).first, (*par_iter).second);
      par_iter++;
    }
    items.insert(new_item);
    n_items++;
  }

  // Now make a copy of the data cache if not NULL
  if (t.data != NULL) {
    if (data != NULL) {
      memcpy(data, t.data, size);
    } else {
      ostr << "Must supply non NULL data cache area when copyig this object '!" << table_name << "'" << ends;
      throw Error(ostr,E_ERROR,errno,__FILE__,__LINE__);
    }
  }
  return *this;
}
/*
 *+ 
 * FUNCTION NAME: Table::set_data_cache
 * 
 * INVOCATION: set_data_cache(const void* dp; const void* dps; const int sz) 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > dp - pointer to item cache
 * > dps - pointer to item cache save
 * > sz - size of data cache
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Sets pointers and size to data cache 
 * 
 * DESCRIPTION: A data cache is a way of caching specific data for the current item in a 
 * data structure that is application dependent. The table class manages this structure in
 * a generic way.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Table::set_data_cache(const void* dp, const void* dps, const int sz) 
{
  data = (void*)dp;
  data_save = (void*)dps;
  if ((data != NULL ) && (data_save == NULL))
    throw Error("Must supply non NULL data cache save area when using data cache!",E_ERROR,errno,__FILE__,__LINE__);
  size = sz;
  if (data != NULL) {
    memset((void*)data, 0, size);
    memset((void*)data_save, 0, size);
  }
}
/*
 *+ 
 * FUNCTION NAME: Table::read(string fname)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > fname - name of table file to read
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  Reads a Table into internal data structure
 * 
 * DESCRIPTION: Uses table delimiters to parse table file
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Table::read(const string fname)
{
	int pos,pos1,pos2,l;
	Linetype fullline;
	string line,name;
	Table_Item *item;
	ostringstream ostr;
	struct stat buf;
	bool new_item,got_line,eof=false;

	// Make sure previous entries are cleared
	clear();

	// Open table file
	if (stat((char*)fname.c_str(), &buf) == -1) {
		ostr << "Failed to find table file: " << fname << ends;
		throw Error(ostr,E_ERROR,errno,__FILE__,__LINE__);
	}
	ifstream inf(fname.c_str());
	if (!inf) {
		ostr << "Failed to open table file: " << fname << ends;
		throw Error(ostr,E_ERROR,errno,__FILE__,__LINE__);
	}

	// Loop to read and parse table file
	if (!inf.eof()) {
		inf.getline(fullline, LINELEN);             // read in a line
		line = fullline;
		if ((pos = line.find(comment_char)) != -1) // Strip comments
			line = line.erase(pos,line.length());
	} else eof = true;
	while (!eof) {
		got_line = new_item = false;

		if (item_regexp != "") { // Look for a named item in table
			if ((pos1 = regx_match(line, item_regexp, l)) != -1) {// skip lines that dont have an item
				// Ok we have an item, get its name 
				if ((pos2 = line.find(item_end,pos1)) != -1) {
					name = line.substr(pos1+1,pos2-pos1-1);
					new_item = true;
				}
				// Done with this line - get next
				if (!inf.eof()) {
					inf.getline(fullline, LINELEN);             // read in a line
					line = fullline;
					if ((pos = line.find(comment_char)) != -1) // Strip comments
						line = line.erase(pos,line.length());
				} else eof = true;
			}
		} else {
			name = "NONAME";
			new_item = true;
		}

		if (new_item) {
			// instantiate the item
			item = new Table_Item(name.c_str(),separator,end_of_par,namesort,n_items);

			// Now read rest of item
			// continue looking for pars until next item
			while ((!eof) && ((item_regexp == "") || (regx_match(line, item_regexp, l) == -1))) {
				item->read(line);
				if (!inf.eof()) {
					inf.getline(fullline, LINELEN);           // read in a line
					line = fullline;
					if ((pos = line.find(comment_char)) != -1) // Strip comments
						line = line.erase(pos,line.length());
				} else eof = true;
			}

			// Insert into list of items
			items.insert(item);
			n_items++; 

			got_line = true;
		}
		if (!got_line) {
			if (!inf.eof()) {
				inf.getline(fullline, LINELEN);             // read in a line
				line = fullline;
				if ((pos = line.find(comment_char)) != -1) // Strip comments
					line = line.erase(pos,line.length());
			} else eof = true;
		}
	}
	inf.close();
}
/*
 *+ 
 * FUNCTION NAME: void Table::write(const string fname, const int mode)
 * 
 * INVOCATION: table.write(fname, mode)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > fname - name of table file to write
 * > prot - Unix file permissions to set for new file
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Writes a table in standard format using delimiters
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Table::write(const string fname, const int prot)
{
  ostringstream ostr;
  Table_Item *item;
  int n;
  string key,value,ep,spacing;
  mode_t cmask;

  ep = end_of_par;
  if (ep == "\n") 
    ep = "";

  // Set the umask to 0 so that mode is not affected by it
  // Save the old umask so that it can be restored later
  cmask = umask(0);

  ofstream tf(fname.c_str(), ios::out);
  if (!tf) {
    ostr << "Failed to open table file for writing: " << fname << ends;
    throw Error(ostr,E_ERROR,errno,__FILE__,__LINE__);
  }

  // Restore the old umask
  umask(cmask);

  tf << comment_char << "=========================================================================" << endl;
  tf << comment_char << " Configuration Table: " << table_name.c_str() << endl;
  tf << comment_char << " This table has been automatically generated and consists of a series of" << endl;
  tf << comment_char << " named items , eg [name], followed by a set of parameter and value" << endl;
  tf << comment_char << " pairs, which could be multiple per line. " << endl;
  tf << comment_char << " Default valued items might not appear. Take care if editing by hand." << endl;
  tf << comment_char << "=========================================================================" << endl;
  tf << endl;

  Table_Parameter_Map::iterator par_iter;
  Table_Item_Set::iterator item_iter;

  // Now sort all items into a linked list
  for (item_iter=items.begin(); (item_iter != items.end()); item_iter++) {
    item = *item_iter;
    if (item->name != "NONAME") {
      tf << item_start << item->name.c_str() << item_end << endl;
      spacing = "  ";
    } else spacing = "";

    // Iterate through hash table and write out
    par_iter = item->pars.begin();
    n = 0;
    while (par_iter != item->pars.end()) {
      tf << spacing << (*par_iter).first << separator << (*par_iter).second << ep;
      if ((++n%items_per_line) == 0) 
	tf << endl;
      par_iter++;
    }
    if ((n%items_per_line) != 0) 
      tf << endl;
  }
  
  tf.close();
  
  if (tf.rdstate()) {
    ostr << "Failed to write " << fname << ends;
    throw Error(ostr, E_ERROR, EIO, __FILE__, __LINE__);
  }
}

/*
 *+ 
 * FUNCTION NAME: Table_Item* Table::find(const string name) const
 * 
 * INVOCATION: item = table.find(name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > name - char string, name of item required
 * 
 * FUNCTION VALUE: table item with name
 * 
 * PURPOSE: Find item in table with name
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Table_Item* Table::find(const string name) const
{
  Table_Item tmp(name.c_str(),separator,end_of_par);
  Table_Item *found = NULL;
  Table_Item *item;
  Table_Item_Set::const_iterator item_iter;

  if (namesort)
    // items are sorted by name so this will work
    item_iter = items.find(&tmp);
  else {
    // items are not sorted by name, so must find it the long way
    for (item_iter=items.begin(); (item_iter != items.end()); item_iter++) {
      item = *item_iter;
      if (name == item->name)
	break;
    }
  }

  if (item_iter != items.end()) {
    found = *item_iter;
  }
  return found;
}   
// Return the ith item in the table
Table_Item* Table::find(int idx) const
{
  ostringstream ostr;
  Table_Item *item;
  Table_Item_Set::const_iterator item_iter;

  if (idx<n_items) {
    item_iter = items.begin();
    advance(item_iter, idx);
    item = *item_iter;
  } else {
    ostr << "Index " << idx << " out of bounds, max items=" << n_items << ends;
    throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
  }
  return item;
}
/*
 *+ 
 * FUNCTION NAME: int Table::index(const string name)
 * 
 * INVOCATION: i = table.index(name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > name - char string, name of item required
 * 
 * FUNCTION VALUE: index in table of item with name
 * 
 * PURPOSE: Find index of table item with name
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
int Table::index(const string name)
{
  Table_Item tmp(name.c_str(),separator,end_of_par);
  Table_Item_Set::iterator item_iter = items.find(&tmp);
  Table_Item_Set::difference_type idx = 0;  
  if (item_iter != items.end()) {
    idx = distance(items.begin(), item_iter);
    return idx;
  } else 
    return -1;
}   
/*
 *+ 
 * FUNCTION NAME: void Table::put(void* itemp, int pos)
 * 
 * INVOCATION: table.put(itemp,pos)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * itemp - void*, pointer to data item to put into table
 * pos - index to place table item, defaults to append
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: This function does nothing so derived class is expected to 
 *  provide functionality
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES:  
 *
 *- 
 */
void Table::put(void* itemp, int pos)
{
  void* dummy = itemp;
  int i = pos;
}
/*
 *+ 
 * FUNCTION NAME: void Table::insert(Table_Item* item, int pos)
 * 
 * INVOCATION: table.insert(item,pos)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * item - Table_Item*, table item to insert in table
 * pos - index to place table item, defaults to append
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Places item in table
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES:  
 *
 *- 
 */
void Table::insert(Table_Item* item, int pos)
{  
  ostringstream ostr;
  int l;

  if (item_regexp != "") {
    // Ensure name is legal
    string tname = item_start + item->name + item_end; 
    if (regx_match(tname, item_regexp, l) == -1) {
      ostr << "Illegal name " << item_start << item->name << item_end << ", must match regular expression " <<
	item_regexp << ends;
      throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);    
    }
  }
  items.insert(item);
  n_items++;
  last_name = item->name;
  last_pos = pos;
}   
/*
 *+ 
 * FUNCTION NAME: int Table::destroy(const string name)
 * 
 * INVOCATION: removed = table.destroy(name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > name - char string, name of item to remove
 * 
 * FUNCTION VALUE: index of item before removed item
 * 
 * PURPOSE: Removes table item with name
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
int Table::destroy(const string name)
{

  int retval=0;
  Table_Item *tmp=find(name.c_str());

  if (tmp != NULL) {
    items.erase(tmp);
    // This is important for tables that are not sorted by item name.
    // The sortkey is set up when the table is read in and so we need
    // to make sure we can re-use this sort key if we are destroying
    // an item just before replacing it (see the ::update method below).
    // This method will return the sortkey so that ::update can tell
    // the ::put method what the sortkey value must be in the new item
    // it will create.  eg. see Sdsu_Volts::put
    retval = tmp->sortkey;
    delete tmp;
    n_items--;
  }

  return retval;

}   
/*
 *+ 
 * FUNCTION NAME: void Table::update()
 * 
 * INVOCATION: table.update()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Updates the internal representation of the table with
 * the cached data pointed to by data.
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Table::update()
{
  if (data != NULL) {
    // The table class keeps a cached copy of the last selected
    // table item.  This copy may have been modified, so we have
    // to put it back in the 'items' set before we move on to
    // the next selected item (and so change the item in the cache).

    // Make sure that data_save is a copy of data to prevent further updates
    // of same data
    memcpy((void*)data_save, (void*)data, size);
  }

  // This call puts the table item pointed to by 'data' back into
  // the set at the position returned by the destroy call.  The destroy
  // call is removing the old version of the item from the set.
  // NB - no checks are performed on the validity of the 'data' cache - 
  // responsibility of derived method.
  put(data,destroy(last_name));   
}
/*
 *+ 
 * FUNCTION NAME: void Table::need_update()
 * 
 * INVOCATION: table.need_update()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Check to see if an update of the cached data is needed.
 * 
 * DESCRIPTION: 
 * Simply uses memcmp - should be overridden if data structures have pointers to
 * other memory blocks
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: Because it uses memcmp, cannot compare data structures that have pointers to 
 * other blocks of memeory as part of their makeup - override if this is the case.
 *
 *- 
 */
bool Table::need_update()
{
  if (data != NULL)
    return (memcmp((void*)data, (void*)data_save, size) != 0);
  else
    return false;
}
/*
 *+ 
 * FUNCTION NAME: void Table::clear()
 * 
 * INVOCATION: table.clear()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Clears table of all entries and frees memory
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Table::clear()
{
  Table_Item_Set::iterator iter;

  // Clear each items hash table
  for (iter=items.begin(); iter!=items.end(); iter++) {
    delete *iter;
  }
  items.clear();
  n_items = 0;
  last_name = "";
  if (data != NULL) {
    memset((void*)data, 0, size);
    memset((void*)data_save, 0, size);
  }
}
/*
 *+ 
 * FUNCTION NAME: Table::print
 * 
 * INVOCATION: print()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Lists to stdout the contents of the table_items par map
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Table::print()
{
  Table_Item_Set::iterator item_iter;
  Table_Item *item;
  
  item_iter = items.begin();
  while (item_iter != items.end()) {
    item = *item_iter;
    cout << "[" << item->name  << "]" << endl;
    item->print();
    item_iter++;
  }
}
/*
 *+ 
 * FUNCTION NAME: Table::~Table()
 * 
 * INVOCATION: destructor
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Destroys table - calls clear()
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Table::~Table()
{  
  clear();
}
