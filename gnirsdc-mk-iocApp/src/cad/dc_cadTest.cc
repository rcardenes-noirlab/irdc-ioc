/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_cadTest.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI NIFS PROJECT
 *
 * FILENAME cad_seq.c
 *
 * GENERAL DESCRIPTION
 *
 * Handle CADS from commands that are ignored by the NIFS DC.  These are DATUM,
 * GUIDE, ENDGUIDE, VERIFY, ENDVERIFY and ENDOBSERVE.  Their equivalent CAR
 * records will be set to busy and then back to idle.
 *
 *
 * $Log: dc_cadTest.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.8  2005/07/29 23:46:38  pjy
 * Use set_and_verify to check to see if observc is busy
 *
 * Revision 1.7  2005/06/13 04:45:15  pjy
 * Readded - really do need it...
 *
 * Revision 1.5  2004/08/20 02:27:04  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.4  2004/04/27 07:25:59  pjy
 * T2.2
 *
 * Revision 1.1  2004/03/23 01:59:55  pjy
 * Gem 8.6 port
 *
 * Revision 1.2  2003/09/19 06:24:22  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.1  2003/03/11 01:29:38  pjy
 * Import modules
 *
 *
 */

#include <cstdio>
#include <cstring>
#include <cmath>

#ifdef __cplusplus
extern "C" {
#endif
#include "cadRecord.h"
#include "cad.h"
#include "dbAccess.h"
#include "dbCommon.h"
#ifdef __cplusplus
}
#endif

#include "logging.h"

#include "configuration.h"
#include "dc.h"

/* Function declarations */
#ifdef __cplusplus
extern "C" long dcCADtest(struct cadRecord *pCad);
extern "C" void set_and_verify_output_cad(struct cadRecord *pCad);
#endif

/*
 *+
 * FUNCTION NAME:
 * dcCADtest
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADtest( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for test CAD record
 *
 * DESCRIPTION:
 * This routine is called whenever a test CAD record is processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADtest(struct cadRecord *pCad)
{
  long status;                     /* return status */

  status = CAD_ACCEPT;

  logger::message(logger::Level::Min, "dcCADtest: %d directive.", pCad->dir);

  /* Create name of record to process car from name of this record*/

  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:
  case menuDirectiveSTART:
  case menuDirectiveSTOP:
    break;

  case menuDirectivePRESET:
    /* Reject the command if the arguments don't validate */
    try {
      // Verify input parameters and set outputs
      set_and_verify_output_cad(pCad);
      logger::message(logger::Level::Full,"dcCADtest: Test Cad verified ok!");
    }
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;

  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADtest: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  }
  return status;
}


