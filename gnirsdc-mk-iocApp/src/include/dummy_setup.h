/*
 * Copyright (c) 1994 - 2002 by MSSSO Computing Section 
 *
 * CICADA PROJECT
 *
 * FILENAME dummy_setup.h
 * 
 * PURPOSE
 * Defines a class for handling dummy setup files
 *
 */
#ifndef DUMMY_SETUP_H
#define DUMMY_SETUP_H

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#include "preferences.h"
#include "configuration.h"

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/
#define DUMMY_SETUP "dummy.setup"
// TODO: Check what to do with the image name...
constexpr char DEFAULT_SIM_IMAGE[] = "foobar.fits";
const double DEFAULT_SIM_EXPTIME = 1.0; // seconds Default exposure time for inimage - should get from keyword
const double DEFAULT_PIXEL_READ_TIME = 3.41; // Default time for pixel readout to generate data (to simulate SDSU II)
const double DEFAULT_PIXEL_CLEAR_TIME = 0.2; // Default time for clearing a pixel 
const double DEFAULT_COSMIC_RATE = 4.0; // Number of cosmic rays/s/cm/cm.


//+ Parameter database name constants

/*--------------------------------------------------------------------------
 Class declarations 
 -------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
CLASS
    Dummy_Setup

DESCRIPTION  
    The Dummy_Setup class is a preferencec class used for holding all dummy
    controller preferences.
 
KEYWORDS
    Dummy_Setup, Preferences.

-------------------------------------------------------------------------*/
class Dummy_Setup:public Preferences {
public:
  Dummy_Config_Data config;
  
  //+ Basic constructor - used for setting up preferences.
  Dummy_Setup();

  //+ Destructor
  ~Dummy_Setup();

  // GROUP: public member functions

  //+ add a setup item, itemp is pointer to desc
  // > itemp - pointer to data to insert into table
  // > pos - position in table for insertion
  void put(void* data, int pos=999);

  //+ Get an item from the table and make current
  // > item - description of item to get
  void get(Table_Item *item);       
};

#endif // DUMMY_SETUP_H
