/*
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME hardware.cc
 *
 * GENERAL DESCRIPTION
 *  Hardware class implementation and functions.
 * This is the base level class for RSAA's OOP model of an astronomical
 * instrument. It is used to encapsulate all generic instrument data and
 * methods. Other more specific classes derived from this are used to model each
 * particular instrument type, eg a camera.
 *
 */

/* Include files */
//#include "cicada.h"
#include "ipc.h"
#include "common.h"
#include "hardware.h"
#include "table.h"
#include "epicsParameterDB.h"
#include "errlog.h"
#include <cstdarg>

/* typedefs */

/* class declarations */

/* global variables */
extern Config *shared_config;                     //+ Shared memory for configuration

const std::string Hardware::PARAMETER_NAME = "HardPar";

/* local function declarations */

/* local function definitions */

/* class function definitions */

/*
 *+
 * FUNCTION NAME: Hardware::Hardware
 *
 * INVOCATION: hard = new Hardware(Platform_Specific* ps)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ps - structure for specific platforms
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Simple Constructor
 *
 * DESCRIPTION:
 *  Basic constructor - most elements set to NULL - except platform specific data structure
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Hardware::Hardware(Platform_Specific* ps)
{
  // Set the platform specific data
  specific = ps;
  hardware_param_map = NULL;
  status = NULL;
  init_vars();
}

/*
 *+
 * FUNCTION NAME: Hardware::Hardware
 *
 * INVOCATION: hard = new Hardware(const char* param_host, Instrument_Status *st, ParamMapType *pm)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > param_host - name of host that hold parameter database
 * > st - pointer to full instrument status structure
 * > pm - pointer to parameter map
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Simple Constructor
 *
 * DESCRIPTION:
 *  Just initialises pointers into status structure. Not used when interfacing to
 * real hardware, but rather for lookup purposes for 'eavesdropping' instances.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Hardware::Hardware(const char* param_host, Instrument_Status *st, Platform_Specific* ps)
{
  // Initialise some member vars
  status = st;                   // Address for depositing status info

  // Set the platform specific data
  specific = ps;
  hardware_param_map = NULL;

  init_vars();

  // Now add all status parameters
  add_status_parameters(param_host, false);
}
/*
 *+
 * FUNCTION NAME: Hardware::Hardware
 *
 * INVOCATION: hard = new Hardware(Instrument_Status *st, int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > st - pointer to full instrument status structure
 * > deb - debugging flag
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Main Constructor
 *
 * DESCRIPTION:
 *  Initialises pointers into status structure and pointers to other hardware
 * structures required for interfacing to real hardware. This version is used
 * by derived hardware objects that actually talk to real hardware.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Hardware::Hardware(Instrument_Status *st, int deb, Platform_Specific* ps)
{

  // Initialise some member vars
  status = st;                   // Address for depositing status info

  // Set the platform specific data
  specific = ps;
  hardware_param_map = NULL;

  init_vars();

  debug = deb;                   // debugging level

  current_config = shared_config;

  if (debug&DEBUG_INDICATORS) {
    message_write("Configuration memory address=0x%p", current_config);
  }

  // Now add all status parameters
  add_status_parameters("", false);

}
/*
 *+
 * FUNCTION NAME: Hardware::init_vars
 *
 * INVOCATION: hard->init_vars()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Initialises pointers to different parts of instrument status.
 *
 * DESCRIPTION: Called by each of the constructor variants.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Hardware::init_vars()
{
  // Hardware specific abort flag
  hardware_abort = false;
  hardware_stop = false;
  hardware_statusp = NULL;
  hardware_pDB = NULL;


  if (status != NULL) {
    // Address for depositing ctl status info
    ctl_status = &status->inst_ctl_status;

    // Address for depositing hardware status info
    hardware_status = &status->hardware_status;
  }
}

/*
 *+
 * FUNCTION NAME: Hardware::add_status_parameters
 *
 * INVOCATION: add_status_parameters(const char* param_host, bool init)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > param_host - name of host that hold home version of parameters
 * > init - set if required to initialise variables
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Adds all generic hardware status parameters to the parameter database
 *
 * DESCRIPTION:
 *  Uses the Parameter class to handle any Cicada status parameters. This class
 * does immediate write-thru operations to the Cicada status shared memory on
 * the observer computer any where on the net.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Hardware::add_status_parameters(const char* param_host, bool init)
{
  // Instantiate the hardware parameter database.
  hardware_pDB = new EpicsParameterDB();
  hardware_statusp = new Parameter(hardware_pDB, PARAMETER_NAME, hardware_param_map, HARDWARE_PREFIX, DB_SUFFIX);

  // Each piece of hardware is either a Camera or a Simple object type - add the
  // type parameter here.
  hardware_statusp->add(STATUS_INFO_TYPE, STATUS_INFO_TYPE_DB, (void*)&hardware_status->type, init, 1,
			(int*)&hardware_status->type, VALIDITY_NONE, 0, (int*)NULL, UNPUBLISHED);
}
/*
 *+
 * FUNCTION NAME: Hardware::handle_reset
 *
 * INVOCATION: hard->handle_reset()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Handle a reset condition.  Can be overriden.
 *
 * DESCRIPTION: Sets the hardware_abort flag to true.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Hardware::handle_reset()
{
  hardware_abort = true;
  message_write("Hardware abort flag set!", status->pid);
}

/*
 *+
 * FUNCTION NAME: Hardware::clear_reset
 *
 * INVOCATION: hard->clear_reset()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Clear a reset condition.  Can be overriden.
 *
 * DESCRIPTION: Sets the hardware_abort flag to false.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Hardware::clear_reset()
{
  hardware_abort = false;
}

/*
 *+
 * FUNCTION NAME: Hardware::handle_stop
 *
 * INVOCATION: hard->handle_stop()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Set the stop flag.  Can be overriden.
 *
 * DESCRIPTION: Sets the hardware_stop flag to true.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Hardware::handle_stop()
{
  hardware_stop = true;
  message_write("Hardware stop flag set!", status->pid);
}

/*
 *+
 * FUNCTION NAME: Hardware::clear_stop
 *
 * INVOCATION: hard->clear_stop()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Clear the stop flag.  Can be overriden.
 *
 * DESCRIPTION: Sets the hardware_stop flag to false.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Hardware::clear_stop()
{
  hardware_stop = false;
}

/*
 *+
 * FUNCTION NAME: Hardware::change_debug
 *
 * INVOCATION: hard->change_debug(int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > deb - debugging flag
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sets reporting object and debug level
 *
 * DESCRIPTION: Sets the hardware debugging level and associated report object.
 * Expects calling program to deal with allocation/deallocation of object
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Hardware::change_debug(int deb)
{
  debug = deb;                   // debugging level
}

/*
 *+
 * FUNCTION NAME: Hardware::request_wrapup
 *
 * INVOCATION: hard->request_wrapup()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Wrap up request
 *
 * DESCRIPTION:
 *  Performs all standard actions at the end of a request.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Hardware::request_wrapup()
{
  // Previously this method set ctl_status->last_result
  // but this is done by slave which uses
  // inst_statusp->put("last_result", result)
  //
  // This method is still here in case anybody wants to override it
  // in a particular hardware class
}

/*
 *+
 * FUNCTION NAME: Hardware::~Hardware
 *
 * INVOCATION: delete hard
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Destructor
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Hardware::~Hardware()
{
  if (hardware_statusp != NULL)
    delete hardware_statusp;
  if (hardware_pDB != NULL)
    delete hardware_pDB;
}

/*
 *+
 * FUNCTION NAME: hardwareTest
 *
 * INVOCATION: hardwareTest()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Uses Hardware_Test class to test instantiation of Hardware object
 *
 * DESCRIPTION: Simple set of instantiations of Hardware_Test objects at
 * different constructor levels.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void hardwareTest()
{
  Hardware *hw;
  string ph;
  Instrument_Status st;
  ParamMapType test_pm;
  Config current_config;
  shared_config = &current_config;

  try {
    Platform_Specific spec;
    cout << "Running Hardware class tests." << endl;
    cout << "Creating object with simple constructor ..." << endl;
    hw = new Hardware_Test(&spec);
    cout << " Done" << endl;
    delete hw;

    cout << "Creating object with parameter only constructor ..." << endl;
    cout << "Enter parameter database host: ";
    cin >> ph;
    st.id=0;
    st.pid=0;
    hw = new Hardware_Test(ph.c_str(), &st, &spec);
    cout << " Done" << endl;
    delete hw;

    cout << "Creating object with full status and config structures ..." << endl;
    hw = new Hardware_Test(&st, 255, &spec);
    cout << " Done" << endl;
    delete hw;
  }
  catch (Error &e) {
    e.print_error(__FILE__,__LINE__);
  }
}
