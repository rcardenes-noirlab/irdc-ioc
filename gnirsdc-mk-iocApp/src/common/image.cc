/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  image.cc
 * 
 * GENERAL DESCRIPTION
 * Implementation of general purpose image handling class.  Both one and
 * two-dimensional Images are supported.  There are public member functions
 * allowing addition, subtraction, multiplication and division of Images.  There
 * are Image operator scalar, and scalar operator Image functions.  Functions
 * for taking the square root, logarithm base 10 and natural logarithm of an
 * Image are available.  An Image can be rebinned and/or rotated and translated.
 * Images can either be input as reaw data or taken from Fits files. There is
 * some support for multi-extension Fits.
 *
 * ORIGINAL AUTHOR: 
 *  Stephen Meatheringham - RSAA 1995
 *
 * HISTORY
 *   July 1995 SJM    Created for RSAA CICADA package
 *   Sept 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 * */

// Include files 
#include <libgen.h>
#include <errno.h>
#include <stdio.h>
#include <math.h>
#include "image.h"
#include "utility.h"
#include "exception.h"
#include "stats.h"

// defines and constants
#ifdef MIN
#undef MIN
#endif
#define MIN(m1,m2) ((m1<m2)? (m1):(m2))
#ifdef MAX
#undef MAX
#endif
#define MAX(m1,m2) ((m1>m2)? (m1):(m2))

// typedefs 

// class declarations

// global variable declarations
const int IMAGE_DISPLAY_COLOURS = 200;  // number of colours the image display uses
const int IMAGE_DISPLAY_FRAME = 1;      // image frame number to display to
const int IMAGE_DISPLAY_BSCALE = 1;     // bscale value for image display
const int IMAGE_DISPLAY_BZERO = 0;      // bzero value for image display
const int RESAMPLE_TYPE = 1;            // nearest neighbour interpolation

static char image_errmsg[MSGLEN];       // error messages for throws


// global function definitions

// local function declarations

//class function definitions


/*
 *+ 
 * FUNCTION NAME: Image::initialise()
 * 
 * INVOCATION: image->initialise()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Initialise Image member vars
 * 
 * DESCRIPTION: Set default values for all image member vars. Usually called
 * from a constructor.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Image::initialise()
{
  name = "";
  xstart = 1;
  ystart = 1;
  xincr = 1;
  fits = NULL;
  fits_open = FALSE;
  min = IMAGE_MAX_PIX_VALUE;
  max = -IMAGE_MAX_PIX_VALUE;
  mean = 0;
  median = 0;
  mode = 0;
  sdev = 0;

}
/*
 *+ 
 * FUNCTION NAME: Image::Image
 * 
 * INVOCATION: im = new Image(x,y,xorig,bp,data,us)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > x - number of pixels in x axis
 * > y - number of pixels in y axis
 * > xorig - first pixel to use in x axis
 * > bp - number of bits per pixel
 * > data - pointer to input data array
 * > us - flag indicating whether input data is unsigned
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Takes input data argument and creates an image object
 * 
 * DESCRIPTION: 
 *  This constructor copies the whole data array into an Image class object
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Image::Image(int x, int y, int xorig, int bp, char *data, int us)
{
  int n;

  initialise();
  nx = x;
  ny = y;
  bitpix = bp;
  unsigned_data = us;
  xstart = xorig;

  n = abs(bitpix)/8*x*y;
  pix = new char[n];
  if (!pix) {
    snprintf(image_errmsg, MSGLEN, "Unable to allocate memory (%d bytes) for Image", n);
    throw Error(image_errmsg, E_ERROR, ENOMEM, __FILE__, __LINE__);    
  }
  spix = (short*)pix; 
  upix = (unsigned short*)pix; 
  ipix = (int*)pix; 
  uipix = (unsigned int*)pix; 
  fpix = (float*)pix; 
  dpix = (double*)pix;

  memcpy(pix,data,n);
}


/*
 *+ 
 * FUNCTION NAME: Image::Image
 * 
 * INVOCATION: im = new Image(char* ff, int read_fits, int ext_num)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ff - name of Fits file
 * > read_fits - flag set if required to read data on open
 * > ext_num - use the ext_num Fits extension from a MEF file
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: This constructor uses Fits as its image data
 * 
 * DESCRIPTION: This constructor copies the whole data array from an input Fits
 *  file into an Image class object
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Image::Image(char* ff, int read_fits, int ext_num)
{
  initialise();

  fitsname = ff;
  fits = new Fits((char*)fitsname.c_str());
  fits_ext = ext_num;
  if (fits_ext > fits->nextend) {
    snprintf(image_errmsg, MSGLEN, "No such extension %d in Fits file with nextend=%d",
	    fits_ext,fits->nextend);    
    delete fits;
    throw Error(image_errmsg, E_ERROR, ENOMEM, __FILE__, __LINE__);    
  }
  nx = fits->xt[fits_ext].nx;
  ny = fits->xt[fits_ext].ny;
  mosaic_data = (fits->extend && fits->mosaic_able && (fits_ext == 0));
  if (fits_ext == 0) {
    if (mosaic_data) {
      nx = fits->xt[0].detx;
      ny = fits->xt[0].dety;
    } else if (fits->extend) {
      snprintf(image_errmsg, MSGLEN, "Unable to mosaic image - probably faulty FITS keywords!");
      throw Error(image_errmsg, E_ERROR, EINVAL, __FILE__, __LINE__);    
    }
  }
  bitpix = fits->xt[fits_ext].bitpix;
  unsigned_data = (((fits->xt[fits_ext].bitpix==SHORT_BITPIX)&&(fits->xt[fits_ext].bzero==((double)MAXS+1)))||
		   ((fits->xt[fits_ext].bitpix==INT_BITPIX)&&(fits->xt[fits_ext].bzero==((double)MAXI+1))));
  fits->close();
  if (read_fits) 
    open();
  else 
    pix = NULL;
  spix = (short*)pix; 
  upix = (unsigned short*)pix; 
  ipix = (int*)pix; 
  uipix = (unsigned int*)pix; 
  fpix = (float*)pix; 
  dpix = (double*)pix; 
}


/*
 *+ 
 * FUNCTION NAME: Image::Image
 * 
 * INVOCATION: im = new Image(int x, int y, int bp, int us)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > x - number of pixels in x axis
 * > y - number of pixels in y axis
 * > bp - number of bits per pixel
 * > us - flag indicating whether input data is unsigned
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: A constructor where the size is known but no data is available
 * 
 * DESCRIPTION: Creates an image object of right size but without any data.
 * Zeros the data array.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Image::Image(int x, int y, int bp, int us)
{
  int n;

  initialise();
  nx = x;
  ny = y;
  bitpix = bp;
  unsigned_data = us;

  n = abs(bitpix)/8*x*y;
  pix = new char[n];
  if (!pix) {
    snprintf(image_errmsg, MSGLEN, "Unable to allocate memory (%d bytes) for Image",n);
    throw Error(image_errmsg, E_ERROR, ENOMEM, __FILE__, __LINE__);    
  }
  // zero data
  memset(pix,0,n);
  spix = (short*)pix; 
  upix = (unsigned short*)pix; 
  ipix = (int*)pix; 
  uipix = (unsigned int*)pix; 
  fpix = (float*)pix; 
  dpix = (double*)pix; 
}


/*
 *+ 
 * FUNCTION NAME: Image::Image
 * 
 * INVOCATION: im = new Image()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: A constructor where the size is unknown
 * 
 * DESCRIPTION: Creates an empty image object.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Image::Image()
{
  initialise();
  pix = NULL;
  spix = (short*)pix; 
  upix = (unsigned short*)pix; 
  ipix = (int*)pix; 
  uipix = (unsigned int*)pix; 
  fpix = (float*)pix; 
  dpix = (double*)pix; 
}

/*
 *+ 
 * FUNCTION NAME: Image::Image
 * 
 * INVOCATION: im = new Image(const Image &ob)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ob - input image object
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: copy constructor
 * 
 * DESCRIPTION: Copies data from input image to new Image object.  If input
 * image data was from a Fits file, copies that data to new in-memory array.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Image::Image(const Image &ob)
{
  int n;
  nx = ob.nx; 
  ny = ob.ny;
  xstart = ob.xstart;   
  ystart = ob.ystart;   
  xincr = ob.xincr;
  min = ob.min;   
  max = ob.max;   
  mean = ob.mean;   
  sdev = ob.sdev;
  median = ob.median;
  mode = ob.mode;
  bitpix = ob.bitpix;
  unsigned_data = ob.unsigned_data;
  name = ob.name;


  // This is an in-memory copy constructor
  fits = NULL;  
  fits_open = FALSE;

  // Allocate space for new data
  n = abs(bitpix)/8*nx*ny;
  pix = new char[n];
  if (!pix) {
    snprintf(image_errmsg, MSGLEN, "Unable to allocate memory (%d bytes) for Image",n);
    throw Error(image_errmsg, E_ERROR, ENOMEM, __FILE__, __LINE__);    
  }
  memcpy(pix,ob.pix,n);

  spix = (short*)pix; 
  upix = (unsigned short*)pix; 
  ipix = (int*)pix; 
  uipix = (unsigned int*)pix; 
  fpix = (float*)pix; 
  dpix = (double*)pix; 
}


/*
 *+ 
 * FUNCTION NAME: Image::~Image
 * 
 * INVOCATION: ~Image()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Image::~Image()
{
  if (fits != NULL) {
    close();
    delete fits;
  } else if (pix != NULL) {
    delete[] pix;
    pix = NULL;
  }
}

/*
 *+ 
 * FUNCTION NAME: void Image::open
 * 
 * INVOCATION: open()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Load an image from its Fits file
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Image::open()
{
  int mask=0,i;
  int bp[FITS_MAX_EXTEND+1];

  // Set read mask to just extension we want to load.
  if ((fits_ext>0) && (!mosaic_data))
    mask = 1<<(fits_ext-1);

  if ((fits)&&(!fits_open)) {
    // Use image bitpix for opening all extensions in file
    for (i=0; i<FITS_MAX_EXTEND+1; i++) 
      bp[i]=bitpix;

    // Open fits file - open in mosaic mode if user
    // hase selected extension 0 in an MEF file
    fits->read((char*)fitsname.c_str(), bp, TRUE, FALSE, TRUE, mosaic_data, mask);
    pix = fits->xt[fits_ext].data;
    spix = (short*)pix; 
    upix = (unsigned short*)pix; 
    ipix = (int*)pix; 
    uipix = (unsigned int*)pix; 
    fpix = (float*)pix; 
    dpix = (double*)pix; 
    fits_open = TRUE;
  }
}

/*
 *+ 
 * FUNCTION NAME: void Image::close
 * 
 * INVOCATION: close()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Load an image from its Fits file
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Image::close()
{
  if ((fits)&&(fits_open)) {
    fits->close();
    fits_open = FALSE;
    pix = NULL;
  }
}

/*
 *+ 
 * FUNCTION NAME: void Image::minmax
 * 
 * INVOCATION: minmax()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Calculate the min, max and mean of the pixels in an Image.
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Image::minmax()
{
  // Call appropriate template version of function
  switch (bitpix) {
  case BYTE_BITPIX: 
    tminmax(pix,nx*ny,min,max,mean);
    mmin = MINC;
    mmax = MAXC;
    break;
  case SHORT_BITPIX:
    if (unsigned_data) {
      tminmax(upix,nx*ny,min,max,mean);
      mmin = MINU;
      mmax = MAXU;
    } else {
      tminmax(spix,nx*ny,min,max,mean);
      mmin = MINS;
      mmax = MAXS;
    }
    break;
  case INT_BITPIX:
    if (unsigned_data) {
      tminmax(uipix,nx*ny,min,max,mean);
      mmin = MINUI;
      mmax = MAXUI;
    } else {
      tminmax(ipix,nx*ny,min,max,mean);
      mmin = MINI;
      mmax = MAXI;
    }
    break;
  case FLOAT_BITPIX:
    tminmax(fpix,nx*ny,min,max,mean);
    mmin = MINF;
    mmax = MAXF;
    break;
  case DOUBLE_BITPIX:
    tminmax(dpix,nx*ny,min,max,mean);
    mmin = MIND;
    mmax = MAXD;
    break;
  }
}
/*
 *+ 
 * FUNCTION NAME: void Image::find_mode
 * 
 * INVOCATION: find_mode(int **freq, float **xpix)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Calculate the mode of the pixels in an Image.
 * 
 * DESCRIPTION: 
 * lower and upper threshold values are passed in - data values outside the
 * range are ignored
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Image::find_mode(int **freq, float **xpix)
{
  int max_freq = 0;
  double dmin;
  double dmax;
  double scale;
  double range;
  int i;
  int *fp;
  float *xp;

  nfreq = NFREQ;
 
  // Get data range
  minmax();
  range = max - min;
  dmin = min;
  dmax = max;

  // min==max in considered range -> mode = min = max
  if (range==0) {
    mode = dmin;
    return;
  }
    
  // Allocate space for frequency array
  *freq = fp = new int[NFREQ+1];
    
  // Barf if cannot allocate space
  if (!fp) {
    snprintf(image_errmsg, MSGLEN, "Unable to allocate memory (%ld bytes) for frequency histogram",
	    sizeof(int)*(NFREQ+1));
    throw Error(image_errmsg, E_ERROR, ENOMEM, __FILE__, __LINE__);    
  }
    
  // Zero the array
  memset((void*)fp, 0, (NFREQ+1)*sizeof(int));

  // Calculate the scaling factor - based on data type
  switch (bitpix) {
  case BYTE_BITPIX: 
  case SHORT_BITPIX:
  case INT_BITPIX:
    if (range<=nfreq && range>0) {
      scale = 1.0;
      nfreq = nint(range);
    } else
      scale = nfreq/range;
    break;
  case FLOAT_BITPIX:
  case DOUBLE_BITPIX:
    if (range>0)
      scale = nfreq/range;
    break;
   }


  switch (bitpix) {
  case BYTE_BITPIX: 
    freqsum(pix,nx*ny,dmin,dmax,fp,scale);
    break;
  case SHORT_BITPIX:
    if (unsigned_data) 
      freqsum(upix,nx*ny,dmin,dmax,fp,scale);
    else 
      freqsum(spix,nx*ny,dmin,dmax,fp,scale);
    break;
  case INT_BITPIX:
    if (unsigned_data) 
      freqsum(uipix,nx*ny,dmin,dmax,fp,scale);
    else 
      freqsum(ipix,nx*ny,dmin,dmax,fp,scale);
    break;
  case FLOAT_BITPIX:
    freqsum(fpix,nx*ny,dmin,dmax,fp,scale);
    break;
  case DOUBLE_BITPIX:
    freqsum(dpix,nx*ny,dmin,dmax,fp,scale);
    break;
  }
  
  // set mode - this does not take into account multi-moded frequencies
  // also setup x array
  *xpix = xp = new float [nfreq+1];

  if (scale == 0.0) {
    throw Error("Image: Scale is zero - will cause a divide by zero!", E_ERROR, ENOMEM, __FILE__, __LINE__);  
  }
  for (i=0; i<nfreq+1; i++) {
    xp[i] = i/scale+dmin;
    if (fp[i] > max_freq) {
      mode = xp[i];
      max_freq = fp[i];
    }
  }
}


/*
 *+ 
 * FUNCTION NAME: void Image::std_dev
 * 
 * INVOCATION: std_dev()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Calculate the standard deviation within an Image
 * 
 * DESCRIPTION: 
 * lower and upper threshold values are passed in - data values outside the
 * range are ignored
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Image::std_dev()
{
  switch (bitpix) {
  case BYTE_BITPIX: 
    tstd_dev(pix,nx*ny,sdev,min,max,mean);
    break;
  case SHORT_BITPIX:
    if (unsigned_data) {
      tstd_dev(upix,nx*ny,sdev,min,max,mean);
    } else {
      tstd_dev(spix,nx*ny,sdev,min,max,mean);
    }
    break;
  case INT_BITPIX:
    if (unsigned_data) {
      tstd_dev(uipix,nx*ny,sdev,min,max,mean);
    } else {
      tstd_dev(ipix,nx*ny,sdev,min,max,mean);
    }
    break;
  case FLOAT_BITPIX:
    tstd_dev(fpix,nx*ny,sdev,min,max,mean);
    break;
  case DOUBLE_BITPIX:
    tstd_dev(dpix,nx*ny,sdev,min,max,mean);
    break;
  }
}


/*
 *+ 
 * FUNCTION NAME: Image::hopt_range
 * 
 * INVOCATION: hopt_range(double &low, double &high, int sample, float contrast, int use_median)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Calculates histogram optimisation range
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * Given the pixel values in an Image, calculate 'low' and 'high' such
 * that low is nstd_l standard deviations less than median and high
 * is nstd_u above median. 'sample_percent' is used to say what fraction 
 * of the pixels in the Image are sampled.
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Image::hopt_range(double &low, double &high, int sample, float contrast, int use_median)
{
  double tstd_dev,tmedian,tmean,tmin,tmax,tmode,zmin,zmax,zslope;
  float *sample_pix = NULL;

  switch (bitpix) {
  case BYTE_BITPIX: 
    calc_stats(pix, nx*ny, bitpix, tstd_dev, tmedian, tmean, tmin, tmax, tmode, zmin, zmax,
	      zslope, sample_pix, sample, contrast, use_median, NULL, 0);
    break;
  case SHORT_BITPIX:
    if (unsigned_data) 
      calc_stats(upix, nx*ny, bitpix, tstd_dev, tmedian, tmean, tmin, tmax, tmode, zmin, zmax,
		  zslope, sample_pix, sample, contrast, use_median, NULL, 0);
    else 
      calc_stats(spix, nx*ny, bitpix, tstd_dev, tmedian, tmean, tmin, tmax, tmode, zmin, zmax,
		  zslope, sample_pix, sample, contrast, use_median, NULL, 0);
    break;
  case INT_BITPIX:
    if (unsigned_data)
      calc_stats(uipix, nx*ny, bitpix, tstd_dev, tmedian, tmean, tmin, tmax, tmode, zmin, zmax,
		  zslope, sample_pix, sample, contrast, use_median, NULL, 0);
    else 
      calc_stats(ipix, nx*ny, bitpix, tstd_dev, tmedian, tmean, tmin, tmax, tmode, zmin, zmax,
		  zslope, sample_pix, sample, contrast, use_median, NULL, 0);
    break;
  case FLOAT_BITPIX:
    calc_stats(fpix, nx*ny, bitpix, tstd_dev, tmedian, tmean, tmin, tmax, tmode, zmin, zmax,
		zslope, sample_pix, sample, contrast, use_median, NULL, 0);
    break;
  case DOUBLE_BITPIX:
    calc_stats(dpix, nx*ny, bitpix, tstd_dev, tmedian, tmean, tmin, tmax, tmode, zmin, zmax,
		zslope, sample_pix, sample, contrast, use_median, NULL, 0);
    break;
  }

  low = zmin;
  high = zmax;
  median = tmedian;
  mode = tmode;
}
/*
 *+ 
 * FUNCTION NAME: Image::stats
 * 
 * INVOCATION: stats(int sample_full, int sample, int** freq, float** xpix)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Calculate stats of an image
 * 
 * DESCRIPTION: 
 * Given the pixel values in an Image, calculate 'low' and 'high' such
 * that low is nstd_l standard deviations less than median and high
 * is nstd_u above median. 'sample_percent' is used to say what fraction 
 * of the pixels in the Image are sampled.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Image::stats(int sample_full, int sample, int** freq, float** xpix)
{
  double zmin,zmax,zslope,contrast=0.5;
  float *sample_pix = NULL;
  double scale,range;
  int i;
  int *fp;
  float *xp;

  // Setup frequency array
  *freq = fp = new int[NFREQ+1];   

  // Barf if cannot allocate space
  if (!freq) {
    snprintf(image_errmsg, MSGLEN, "Unable to allocate memory (%ld bytes) for frequency histogram",
	    sizeof(int)*(NFREQ+1));
    throw Error(image_errmsg, E_ERROR, ENOMEM, __FILE__, __LINE__);    
  }

  if (sample_full) sample = nx*ny;

  // Now call general calc routine to do sums
  switch (bitpix) {
  case BYTE_BITPIX: 
    calc_stats(pix, nx*ny, bitpix, sdev, median, mean, min, max, mode, zmin, 
		zmax, zslope, sample_pix, sample, contrast, TRUE, fp, NFREQ+1);
    break;
  case SHORT_BITPIX:
    if (unsigned_data) 
      calc_stats(upix, nx*ny, bitpix, sdev, median, mean, min, max, mode, zmin, 
		  zmax, zslope, sample_pix, sample, contrast, TRUE, fp, NFREQ+1);
    else 
      calc_stats(spix, nx*ny, bitpix, sdev, median, mean, min, max, mode, zmin, 
		  zmax, zslope, sample_pix, sample, contrast, TRUE, fp, NFREQ+1);
    break;
  case INT_BITPIX:
    if (unsigned_data)
      calc_stats(uipix, nx*ny, bitpix, sdev, median, mean, min, max, mode, zmin, 
		  zmax, zslope, sample_pix, sample, contrast, TRUE, fp, NFREQ+1);
    else 
      calc_stats(ipix, nx*ny, bitpix, sdev, median, mean, min, max, mode, zmin, 
		  zmax, zslope, sample_pix, sample, contrast, TRUE, fp, NFREQ+1);
    break;
  case FLOAT_BITPIX:
    calc_stats(fpix, nx*ny, bitpix, sdev, median, mean, min, max, mode, zmin, 
		zmax, zslope, sample_pix, sample, contrast, TRUE, fp, NFREQ+1);
    break;
  case DOUBLE_BITPIX:
    calc_stats(dpix, nx*ny, bitpix, sdev, median, mean, min, max, mode, zmin, 
		zmax, zslope, sample_pix, sample, contrast, TRUE, fp, NFREQ+1);
    break;
  }

  range = MAX(1,max-min);
  nfreq = NFREQ;
  // Calculate the scaling factor - based on data type
  switch (bitpix) {
  case BYTE_BITPIX: 
  case SHORT_BITPIX:
  case INT_BITPIX:
    if (range<=nfreq && range>0) {
      scale = 1.0;
      nfreq = nint(range);
    } else
      scale = nfreq/range;
    break;
  case FLOAT_BITPIX:
  case DOUBLE_BITPIX:
    if (range>0)
      scale = nfreq/range;
    break;
   }

  //setup x array
  *xpix = xp = new float [nfreq+1];

  for (i=0; i<nfreq+1; i++) {
    xp[i] = i/scale+min;
  }
}

// Square Root Function for an Image
// C++ is very nice it puts NaN in if we take the square root of a -ve number
// Output datatype (bitpix) is set to float if not already a real type
Image* image_sqrt(const Image* im)
{
  Image* imr = new Image(im->nx, im->ny, FLOAT_BITPIX);
   
  switch (im->bitpix) {
  case BYTE_BITPIX: 
    timage_sqrt(im->nx*im->ny, imr->fpix, im->pix);
    break;
  case SHORT_BITPIX:
    if (im->unsigned_data)
      timage_sqrt(im->nx*im->ny, imr->fpix, im->upix);
    else
      timage_sqrt(im->nx*im->ny, imr->fpix, im->spix);
    break;
  case INT_BITPIX:
    if (im->unsigned_data)
      timage_sqrt(im->nx*im->ny, imr->fpix, im->uipix);
    else
      timage_sqrt(im->nx*im->ny, imr->fpix, im->ipix);
    break;
  case FLOAT_BITPIX:
    timage_sqrt(im->nx*im->ny, imr->fpix, im->fpix);
    break;
  case DOUBLE_BITPIX:
    timage_sqrt(im->nx*im->ny, imr->fpix, im->dpix);
    break;
  }
  return imr;
}


// Natural Log Function for an Image
// Output datatype (bitpix) is set to float if not already a real type
Image* image_log(const Image* im)
{
  Image* imr = new Image(im->nx, im->ny, FLOAT_BITPIX);

  switch (im->bitpix) {
  case BYTE_BITPIX: 
    timage_log(im->nx*im->ny, imr->fpix, im->pix);
    break;
  case SHORT_BITPIX:
    if (im->unsigned_data)
      timage_log(im->nx*im->ny, imr->fpix, im->upix);
    else
      timage_log(im->nx*im->ny, imr->fpix, im->spix);
    break;
  case INT_BITPIX:
    if (im->unsigned_data)
      timage_log(im->nx*im->ny, imr->fpix, im->uipix);
    else
      timage_log(im->nx*im->ny, imr->fpix, im->ipix);
    break;
  case FLOAT_BITPIX:
    timage_log(im->nx*im->ny, imr->fpix, im->fpix);
    break;
  case DOUBLE_BITPIX:
    timage_log(im->nx*im->ny, imr->fpix, im->dpix);
    break;
  }
  return imr;
}


// Log (base 10) Function for an Image
// Output datatype (bitpix) is set to float if not already a real type
Image* image_log10(const Image* im)
{
  Image* imr = new Image(im->nx, im->ny, FLOAT_BITPIX);
  switch (im->bitpix) {
  case BYTE_BITPIX: 
    timage_log10(im->nx*im->ny, imr->fpix, im->pix);
    break;
  case SHORT_BITPIX:
    if (im->unsigned_data)
      timage_log10(im->nx*im->ny, imr->fpix, im->upix);
    else
      timage_log10(im->nx*im->ny, imr->fpix, im->spix);
    break;
  case INT_BITPIX:
    if (im->unsigned_data)
      timage_log10(im->nx*im->ny, imr->fpix, im->uipix);
    else
      timage_log10(im->nx*im->ny, imr->fpix, im->ipix);
    break;
  case FLOAT_BITPIX:
    timage_log10(im->nx*im->ny, imr->fpix, im->fpix);
    break;
  case DOUBLE_BITPIX:
    timage_log10(im->nx*im->ny, imr->fpix, im->dpix);
    break;
  }
  return imr;
}


//****************************************************************************
// Addition, Subtraction, Multiplication and Division functions for Images
// C++ is very nice it puts 0/0=NaN and num/0=Inf 

Image* image_add(Image* im1, Image* im2 )
{
  if (im1->min>im1->max) im1->minmax();
  if (im2->min>im2->max) im2->minmax();
  int bitpix = set_bitpix(im1->bitpix, im2->bitpix, (double)im1->max+im2->max,
			  MAX(im1->mmax,im2->mmax), MIN(im1->min,im2->min), 
			  MIN(im1->mmin,im2->mmin));
  Image* imr = new Image(im1->nx, im1->ny, bitpix, im1->unsigned_data & im1->unsigned_data);
  int n = im1->nx*im1->ny;
  Image *imp1,*imp2;

  // Set temporary image pointers to images so that they can be ordered in
  // template call
  if (im1->bitpix >= im2->bitpix) {
    imp1 = (Image*) im1;
    imp2 = (Image*) im2;
  } else {
    imp1 = (Image*) im2;
    imp2 = (Image*) im1;
  }
    
  // Need to take care of data type possibilities in each image
  switch (bitpix) {
  case BYTE_BITPIX: 
    pix_image_op_image(n, PLUS, imr->pix,imp1,imp2);
    break;
  case SHORT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_image(n, PLUS,imr->upix,imp1,imp2);
    else
      pix_image_op_image(n, PLUS,imr->spix,imp1,imp2);
    break;
  case INT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_image(n, PLUS,imr->uipix,imp1,imp2);
    else
      pix_image_op_image(n, PLUS,imr->ipix,imp1,imp2);
    break;
  case FLOAT_BITPIX:
    pix_image_op_image(n, PLUS,imr->fpix,imp1,imp2);
    break;
  case DOUBLE_BITPIX:
    pix_image_op_image(n, PLUS,imr->dpix,imp1,imp2);
    break;
  }
  return imr;
}

Image* image_add(Image* im, const Rawpix scalar)
{
  if (im->min>im->max) im->minmax();
  int sbitpix = scalar_bitpix(scalar);
  int bitpix = set_bitpix(im->bitpix, sbitpix, (double)im->max+scalar,
			  MAX(im->mmax,scalar), MIN(im->min,scalar), 
			  MIN(im->mmin,scalar));
  Image* imr = new Image(im->nx, im->ny, bitpix, im->unsigned_data);
  int n = im->nx*im->ny;
  char bscalar = scalar;
  short sscalar = scalar;
  unsigned short uscalar = scalar;
  int iscalar = scalar;
  unsigned int uiscalar = scalar;
  float fscalar = scalar;
  double dscalar = scalar;

  switch (bitpix) {
  case BYTE_BITPIX: 
    pix_image_op_scalar(n, PLUS, imr->pix, im, bscalar);
    break;
  case SHORT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_scalar(n, PLUS, imr->upix, im, uscalar);
    else
      pix_image_op_scalar(n, PLUS, imr->spix, im, sscalar);
    break;
  case INT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_scalar(n, PLUS, imr->uipix, im, uiscalar);
    else
      pix_image_op_scalar(n, PLUS, imr->ipix, im, iscalar);
    break;
  case FLOAT_BITPIX:
    pix_image_op_scalar(n, PLUS, imr->fpix, im, fscalar);
    break;
  case DOUBLE_BITPIX:
    pix_image_op_scalar(n, PLUS, imr->dpix, im, dscalar);
    break;
  }

  return imr;
}
Image* image_minus(Image* im1, Image* im2 )
{
  if (im1->min>im1->max) im1->minmax();
  if (im2->min>im2->max) im2->minmax();
  int bitpix = set_bitpix(im1->bitpix, im2->bitpix, MAX(im1->max,im2->max),
			  MAX(im1->mmax,im2->mmax), (double)im1->min-im2->min, 
			  MIN(im1->mmin,im2->mmin));
  Image* imr = new Image(im1->nx, im1->ny, bitpix, im1->unsigned_data & im2->unsigned_data);
  int n = im1->nx*im1->ny;

  // Need to take care of data type possibilities in each image
  switch (bitpix) {
  case BYTE_BITPIX: 
    pix_image_op_image(n, MINUS, imr->pix,im1,im2);
    break;
  case SHORT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_image(n, MINUS, imr->upix,im1,im2);
    else
      pix_image_op_image(n, MINUS, imr->spix,im1,im2);
    break;
  case INT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_image(n, MINUS, imr->uipix,im1,im2);
    else
      pix_image_op_image(n, MINUS, imr->ipix,im1,im2);
    break;
  case FLOAT_BITPIX:
    pix_image_op_image(n, MINUS, imr->fpix,im1,im2);
    break;
  case DOUBLE_BITPIX:
    pix_image_op_image(n, MINUS, imr->dpix,im1,im2);
    break;
  }
  return imr;
}

Image* image_minus(Image* im, const Rawpix scalar)
{
  return image_add(im,-scalar);
}

// Subtract image from scalar
//  ie    imr = scalar - im
Image* image_minus(const Rawpix scalar, Image* im)
{
  if (im->min>im->max) im->minmax();
  int sbitpix = scalar_bitpix(scalar);
  int bitpix = set_bitpix(im->bitpix, sbitpix, im->max, im->mmax, 
			  (double)im->min-scalar, im->mmin);
  Image* imr = new Image(im->nx, im->ny, bitpix, im->unsigned_data);
  int n = im->nx*im->ny;
  char bscalar = scalar;
  short sscalar = scalar;
  unsigned short uscalar = scalar;
  int iscalar = scalar;
  unsigned int uiscalar = scalar;
  float fscalar = scalar;
  double dscalar = scalar;

  switch (bitpix) {
  case BYTE_BITPIX: 
    pix_scalar_minus_image(n, imr->pix, im, bscalar);
    break;
  case SHORT_BITPIX:
    if (imr->unsigned_data)
      pix_scalar_minus_image(n, imr->upix, im, uscalar);
    else
      pix_scalar_minus_image(n, imr->spix, im, sscalar);
    break;
  case INT_BITPIX:
    if (imr->unsigned_data)
      pix_scalar_minus_image(n, imr->uipix, im, uiscalar);
    else
      pix_scalar_minus_image(n, imr->ipix, im, iscalar);
    break;
  case FLOAT_BITPIX:
    pix_scalar_minus_image(n, imr->fpix, im, fscalar);
    break;
  case DOUBLE_BITPIX:
    pix_scalar_minus_image(n, imr->dpix, im, dscalar);
    break;
  }
  return imr;
}


Image* image_mult(Image* im1, Image* im2)
{
  // For image multiple ops set to at size of max*max value
  if (im1->min>im1->max) im1->minmax();
  if (im2->min>im2->max) im2->minmax();
  int bitpix = set_bitpix(im1->bitpix, im2->bitpix, (double)im1->max*im2->max,
			  MAX(im1->mmax,im2->mmax), 
			  MIN(im1->min*im2->min,MIN(im1->min,im2->min)), 
			  MIN(im1->mmin,im2->mmin));
  int us = set_unsigned(bitpix,im1->unsigned_data,im2->unsigned_data);
  Image* imr = new Image(im1->nx, im1->ny, bitpix, us);
  int n = im1->nx*im1->ny;

  // Need to take care of data type possibilities in each image
  switch (bitpix) {
  case BYTE_BITPIX: 
    pix_image_op_image(n, MULTIPLY, imr->pix,im1,im2);
    break;
  case SHORT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_image(n, MULTIPLY, imr->upix,im1,im2);
    else
      pix_image_op_image(n, MULTIPLY, imr->spix,im1,im2);
    break;
  case INT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_image(n, MULTIPLY, imr->uipix,im1,im2);
    else
      pix_image_op_image(n, MULTIPLY, imr->ipix,im1,im2);
    break;
  case FLOAT_BITPIX:
    pix_image_op_image(n, MULTIPLY, imr->fpix,im1,im2);
    break;
  case DOUBLE_BITPIX:
    pix_image_op_image(n, MULTIPLY, imr->dpix,im1,im2);
    break;
  }
  return imr;
}


Image* image_mult(Image* im, const Rawpix scalar)
{
  if (im->min>im->max) im->minmax();
  int sbitpix = scalar_bitpix(im->max*scalar);
  int bitpix = set_bitpix(im->bitpix, sbitpix, (double)im->max*scalar,
			  im->mmax, im->min*scalar, im->mmin);
  int us = set_unsigned(bitpix,im->unsigned_data,sbitpix>0);
  Image* imr = new Image(im->nx, im->ny, bitpix, us);
  int n = im->nx*im->ny;
  char bscalar = scalar;
  short sscalar = scalar;
  unsigned short uscalar = scalar;
  int iscalar = scalar;
  unsigned int uiscalar = scalar;
  float fscalar = scalar;
  double dscalar = scalar;

  switch (bitpix) {
  case BYTE_BITPIX: 
    pix_image_op_scalar(n, MULTIPLY, imr->pix, im, bscalar);
    break;
  case SHORT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_scalar(n, MULTIPLY, imr->upix, im, uscalar);
    else
      pix_image_op_scalar(n, MULTIPLY, imr->spix, im, sscalar);
    break;
  case INT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_scalar(n, MULTIPLY, imr->uipix, im, uiscalar);
    else
      pix_image_op_scalar(n, MULTIPLY, imr->ipix, im, iscalar);
    break;
  case FLOAT_BITPIX:
    pix_image_op_scalar(n, MULTIPLY, imr->fpix, im, fscalar);
    break;
  case DOUBLE_BITPIX:
    pix_image_op_scalar(n, MULTIPLY, imr->dpix, im, dscalar);
    break;
  }
  return imr;
}

Image* image_div(const Image* im1, const Image* im2 )
{
  // Must be floating point result for division
  int bitpix = MIN(FLOAT_BITPIX,MIN(im1->bitpix, im2->bitpix));

  Image* imr = new Image(im1->nx, im1->ny, bitpix);
  int n = im1->nx*im1->ny;

  // Need to take care of data type possibilities in each image
  switch (bitpix) {
  case BYTE_BITPIX: 
    pix_image_op_image(n, DIVIDE, imr->pix,im1,im2);
    break;
  case SHORT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_image(n, DIVIDE, imr->upix,im1,im2);
    else
      pix_image_op_image(n, DIVIDE, imr->spix,im1,im2);
    break;
  case INT_BITPIX:
    if (imr->unsigned_data)
      pix_image_op_image(n, DIVIDE, imr->uipix,im1,im2);
    else
      pix_image_op_image(n, DIVIDE, imr->ipix,im1,im2);
    break;
  case FLOAT_BITPIX:
    pix_image_op_image(n, DIVIDE, imr->fpix,im1,im2);
    break;
  case DOUBLE_BITPIX:
    pix_image_op_image(n, DIVIDE, imr->dpix,im1,im2);
    break;
  }
  return imr;
}
// C++ is very nice it puts 0/0=NaN and num/0=Inf for floating point ops
Image* image_div(Image* im, const Rawpix scalar)
{
  if (scalar==0.0) 
    throw Error("Cannot divide image by 0.0!", E_ERROR, -1, __FILE__, __LINE__);  
  return image_mult(im,1.0/scalar);
}

Image* image_div(const Rawpix scalar, const Image* im)
{
  int bitpix = MIN(FLOAT_BITPIX,im->bitpix);

  Image* imr = new Image(im->nx, im->ny, bitpix);
  int n = im->nx*im->ny;

  // Need to take care of data type possibilities in each image
  // For division only float and double.
  switch (bitpix) {
  case FLOAT_BITPIX:
    if (im->bitpix == BYTE_BITPIX)
      scalar_div_image(n,imr->fpix,im->pix,scalar,MAXC);
    else if (im->bitpix == SHORT_BITPIX) {
      if (im->unsigned_data)
	scalar_div_image(n,imr->fpix,im->upix,scalar,MAXU);
      else 
	scalar_div_image(n,imr->fpix,im->spix,scalar,MAXS);
    } else if (im->bitpix == INT_BITPIX) {
      if (im->unsigned_data)
	scalar_div_image(n,imr->fpix,im->uipix,scalar,MAXUI);
      else 
	scalar_div_image(n,imr->fpix,im->ipix,scalar,MAXI);
    } else 
      scalar_div_image(n,imr->fpix,im->fpix,scalar,MAXF);
    break;
  case DOUBLE_BITPIX:
    if (im->bitpix == BYTE_BITPIX)
      scalar_div_image(n,imr->fpix,im->pix,scalar,MAXC);
    else if (im->bitpix == SHORT_BITPIX){
      if (im->unsigned_data)
	scalar_div_image(n,imr->dpix,im->upix,scalar,MAXU);
      else 
	scalar_div_image(n,imr->dpix,im->spix,scalar,MAXS);
    } else if (im->bitpix == INT_BITPIX){
      if (im->unsigned_data)
	scalar_div_image(n,imr->dpix,im->uipix,scalar,MAXUI);
      else 
	scalar_div_image(n,imr->dpix,im->ipix,scalar,MAXI);
    } else if (im->bitpix == FLOAT_BITPIX)
      scalar_div_image(n,imr->dpix,im->fpix,scalar,MAXF);
    else 
      scalar_div_image(n,imr->dpix,im->dpix,scalar,MAXD);
    break;
  }
  return imr;
}
//****************************************************************************


// determine the bitpix of an image that is made from one or two input 
// Images.  Valid numbers are : 8, 16, 32, -32, and -64
int set_bitpix(int bp1, int bp2, double max, double mmax, double min, double mmin)
{
  int bitpix;

  int sign = ((bp1 < 0) || (bp2 < 0)) ? -1 : 1;
  bitpix = (abs(bp2) > abs(bp1)) ? abs(bp2) : abs(bp1);
  bitpix = bitpix*sign;
  if (bitpix>0) { 
    // for integer bitpix make sure result fits in range
    if ((max>MAXI) || (min<MINI)) // promote to float
      bitpix = FLOAT_BITPIX;
    else if ((max>mmax) || (min<mmin)) // promote bitpix
      bitpix = (bitpix*2>INT_BITPIX)? INT_BITPIX:bitpix*2;
  }
  return bitpix;
}
// determine whether an image can be unsigned data 
int set_unsigned(int bp, int us1, int us2)
{
  int us;
  us = (bp>0)? us1&us2:0;
  return us;
}

// Determines minimum bitpix of scalar value
int scalar_bitpix(const Rawpix scalar)
{
  int bitpix;
  double x;

  if (modf(scalar, &x) == 0) {
    if (fabs(scalar) < MAXC) bitpix = BYTE_BITPIX;
    else if (fabs(scalar) < MAXS) bitpix = SHORT_BITPIX;
    else if (fabs(scalar) < MAXI) bitpix = INT_BITPIX;
    else if (fabs(scalar) < MAXF) bitpix = FLOAT_BITPIX;
    else bitpix = DOUBLE_BITPIX;
  } else {
    if (fabs(scalar) < MAXF) bitpix = FLOAT_BITPIX;
    else bitpix = DOUBLE_BITPIX;
  }

  return bitpix;
}

/*
 *+ 
 * FUNCTION NAME: Image* Image::translate
 * 
 * INVOCATION: translate(int dx, int dy)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > dx - pixels to translate in x direction
 * > dy - pixels to translate in y direction
 * 
 * FUNCTION VALUE: Returns pointer to a new translated image.
 * 
 * PURPOSE: rotate/shift/scale the input Image
 * 
 * DESCRIPTION: 
 * Given an input Image, this routine creates an output Image which is the
 * result of translating the input Image. 
 * The output image shift and size are such that the entire input Image is 
 * present. Pixels that are not assigned new values are left with
 * existing values. (near edges)
 * selects the direction of traversing the array required to ensure that old
 * values are transferred to their new positions without overwriting values
 * already copied.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */

Image* Image::translate(int dx, int dy)
{
  Image *tmp ;
  int i, j, axis ; // loop counters
  int xy_from[2], xy_to[2]; // Start and end points of the loops
  bool xy_neg[2] = {false, false}; // direction of traversing the array
  
  tmp = new Image(*this);
  // Find the direction of image shift and set the direction of
  // counting, the start point and end point of each axis accordingly.

  int xy_len[2] = {nx, ny}; int delta[2] = {dx, dy};
    for (axis=0; axis<2; axis++) {
      if (delta[axis] < 0) { // negative shift means count upwards
        xy_from[axis] = 0; xy_to[axis] = xy_len[axis]; xy_neg[axis] = TRUE;
      }
      else { // positive shift means count downwards
        xy_to[axis] = 0; xy_from[axis] = xy_len[axis] - 1; xy_neg[axis] = FALSE;
      } // end if
    } // end for

  if ((dx!=0) || (dy!=0)) {
    // y axis:
    for (j=xy_from[1]; (xy_neg[1]? j<xy_to[1] : j>=xy_to[1]); xy_neg[1]? j++:j--) {
      // x axis:
      for (i=xy_from[0]; (xy_neg[0]? i<xy_to[0] : i>=xy_to[0]);
           xy_neg[0]? i++:i--) {
	if (((i+dx)>=0) && ((i+dx)<nx) &&
	    ((j+dy)>=0) && ((j+dy)<ny)) {
	  switch (bitpix) {
	  case BYTE_BITPIX: 
	    tmp->pix[(i+dx)+(j+dy)*nx] = pix[i+j*nx];
	    break;
	  case SHORT_BITPIX:
	    if (unsigned_data)
	      tmp->upix[(i+dx)+(j+dy)*nx] = upix[i+j*nx];
	    else
	      tmp->spix[(i+dx)+(j+dy)*nx] = spix[i+j*nx];
	    break;
	  case INT_BITPIX:
	    if (unsigned_data)
	      tmp->uipix[(i+dx)+(j+dy)*nx] = uipix[i+j*nx];
	    else
	      tmp->ipix[(i+dx)+(j+dy)*nx] = ipix[i+j*nx];
	    break;
	  case FLOAT_BITPIX:
	    tmp->fpix[(i+dx)+(j+dy)*nx] = fpix[i+j*nx];
	    break;
	  case DOUBLE_BITPIX:
	    tmp->dpix[(i+dx)+(j+dy)*nx] = dpix[i+j*nx];
	    break;
	  } // end switch
	} // end if
      } // end for all points on x axis
    } // end for points on y axis
  }
  return tmp;
}

/*
 *+ 
 * FUNCTION NAME: Image::save_fits
 * 
 * INVOCATION: save_fits(char* fname, double* bscale, double* bzero)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > fname - pathname of new fits file
 * > bscale - array of bscale values for each extension
 * > bzero - array of bzero values for each extension
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Routine to write out image as Fits file
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: Does not yet handle MEF files
 *
 *- 
 */
void Image::save_fits(char* fname, double* bscale, double* bzero)
{
  char msgbuf[MSGLEN];
  int bp[1] = {bitpix};
  struct stat stat_buf;
  int naxes[2] = {nx, ny};
  Fits new_fits;
  // not writing a MEF here, so et[] is not used. initialise it to invalid
  // value so if it does find its way into fits file, fits class will
  // throw as soon as it is opened
  Ext_Type et[FITS_MAX_EXTEND+1] = {EXT_NOT_INITIALISED};
  
  if (stat(fname, &stat_buf) == 0){ // file already exists
    snprintf(msgbuf, MSGLEN, "File (%s) already exists", fname);
    throw Error(msgbuf, E_ERROR, errno, __FILE__, __LINE__);
   } 

  // Create and output the FITS file
  new_fits.create(fname, et, bp, naxes, S_IRUSR|S_IWUSR);
  
  new_fits.write_std_keywords(et, bscale, bzero);
  new_fits.write_data((char *)pix, bitpix);
  new_fits.close();
  
}  
 
//===================================================================================
// TEMPLATE FUNCTIONS
//===================================================================================
/*
 *+ 
 * FUNCTION NAME: template<class T> void tminmax
 * 
 * INVOCATION: tminmax(T *tpix, int n, double& min, double& max, double& mean)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T> void tminmax(T *tpix, int n, double& min, double& max, double& mean)
{
  double sum = 0;
  int i;

  min = IMAGE_MAX_PIX_VALUE;
  max = -IMAGE_MAX_PIX_VALUE;
  for (i = 0; i < n; i++) {
    if (min > tpix[i]) min = tpix[i];
    else if (max < tpix[i]) max = tpix[i];
    sum += tpix[i];
  }
  if (n > 0) mean = sum/n;
}
/*
 *+ 
 * FUNCTION NAME: template<class T> void freqsum
 * 
 * INVOCATION: freqsum(T *tpix, int n, double dmin, double dmax, int* freq, double scale)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  Template function to accumulate frequency histogram
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T> void freqsum(T *tpix, int n, double dmin, double dmax, int* freq, double scale)
{
  int i;
  
  for (i = 0; i < n; i++) {
    if ((tpix[i] <= dmin) && (tpix[i] >= dmax)) {
      freq[nint(scale*(tpix[i]-dmin))]++;
    }
  }
}
/*
 *+ 
 * FUNCTION NAME: template<class T> void tstd_dev
 * 
 * INVOCATION: void tstd_dev(T *tpix, int n, double& sdev, double& min, double& max, double& mean)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  template function for computing std_dev
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T> void tstd_dev(T *tpix, int n, double& sdev, double& min, double& max, double& mean)
{
  int i;
  double sum = 0, sqsum = 0;
  
  min = IMAGE_MAX_PIX_VALUE;
  max = -IMAGE_MAX_PIX_VALUE;
  for (i = 0; i < n; i++) {
    if (min > tpix[i]) min = tpix[i];
    else if (max < tpix[i]) max = tpix[i];
    sqsum += tpix[i]*tpix[i];
    sum += tpix[i];
  }
  sdev = sqrt((n * sqsum - sum*sum)/((double)n * (n - 1)));
  mean = sum/n;
}
/*
 *+ 
 * FUNCTION NAME: template<class T, class U> void timage_sqrt
 * 
 * INVOCATION: void timage_sqrt(int n, T *opix, U *ipix)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  Template function for taking sqrt of image
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T, class U> void timage_sqrt(int n, T *opix, U *ipix)
{
  int i;
  for (i = 0; i<n; i++) {
    opix[i] = sqrt((double)ipix[i]);
  }
}
/*
 *+ 
 * FUNCTION NAME: template<class T, class U> void timage_log
 * 
 * INVOCATION: timage_log(int n, T *opix, U *ipix)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  Template function for taking log of image
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T, class U> void timage_log(int n, T *opix, U *ipix)
{
  int i;
  for (i = 0; i<n; i++) {
    opix[i] = log((double)ipix[i]);
   }
}
/*
 *+ 
 * FUNCTION NAME: template<class T, class U> void timage_log10
 * 
 * INVOCATION: timage_log10(int n, T *opix, U *ipix)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  Template function for taking log base 10 of image
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T, class U> void timage_log10(int n, T *opix, U *ipix)
{
  int i;
  for ( i = 0; i<n; i++) {
    opix[i] = log10((double)ipix[i]);
  }
}
/*
 *+ 
 * FUNCTION NAME: template<class T> void pix_image_op_image
 *                template<class T, class U> void pix_pix_op_image
 *                template<class T, class U, class V> void image_op_image * 
 * INVOCATION: pix_image_op_image(int n, int op, T *opix, const Image* im1, const Image* im2)
 *             pix_pix_op_image(int n, int op, T *opix, U *ipix, const Image* im2)
 *             image_op_image(int n, int op, T *opix, U *ipix1, V *ipix2)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  1. template function that applies "op" to  im1 and im2 pixels with output to opix
 *  2. template function that applies op to ipix and im2->pix pixels with output to opix
 *  3. Template function for operating on two images of different data types
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T> void pix_image_op_image(int n, int op, T *opix, const Image* im1, const Image* im2)
{
  switch (im1->bitpix) {
  case BYTE_BITPIX: 
    pix_pix_op_image(n, op, opix,im1->pix,im2);
    break;
  case SHORT_BITPIX:
    if (im1->unsigned_data)
      pix_pix_op_image(n, op, opix,im1->upix,im2);
    else 
      pix_pix_op_image(n, op, opix,im1->spix,im2);
    break;
  case INT_BITPIX:
    if (im1->unsigned_data)
      pix_pix_op_image(n, op, opix,im1->uipix,im2);
    else 
      pix_pix_op_image(n, op, opix,im1->ipix,im2);
    break;
  case FLOAT_BITPIX:
    pix_pix_op_image(n, op, opix,im1->fpix,im2);
    break;
  case DOUBLE_BITPIX:
    pix_pix_op_image(n, op, opix,im1->dpix,im2);
    break;
  }
}

template<class T, class U> void pix_pix_op_image(int n, int op, T *opix, U *ipix, const Image* im2)
{
  switch (im2->bitpix) {
  case BYTE_BITPIX: 
    image_op_image(n, op ,opix,ipix,im2->pix, MAXC);
    break;
  case SHORT_BITPIX:
    if (im2->unsigned_data)
      image_op_image(n, op, opix,ipix,im2->upix, MAXU);
    else 
      image_op_image(n, op, opix,ipix,im2->spix, MAXS);
    break;
  case INT_BITPIX:
    if (im2->unsigned_data)
      image_op_image(n, op, opix,ipix,im2->uipix, MAXUI);
    else 
      image_op_image(n, op, opix,ipix,im2->ipix, MAXI);
    break;
  case FLOAT_BITPIX:
    image_op_image(n, op, opix,ipix,im2->fpix, MAXF);
    break;
  case DOUBLE_BITPIX:
    image_op_image(n, op, opix,ipix,im2->dpix, MAXD);
    break;
  }
}

template<class T, class U, class V> void image_op_image(int n, int op, T *opix, U *ipix1, V *ipix2, V max)
{
  int i;

  switch(op){
  case PLUS:
    for (i=0; i<n; i++) {
      opix[i] = (T) ipix1[i] + (T) ipix2[i];
    }
    break;
  case MINUS:
    for (i=0; i<n; i++) {
      opix[i] = (T) ipix1[i] - (T) ipix2[i];
    }
    break;
  case MULTIPLY:
    for (i=0; i<n; i++) {
      opix[i] = (T) ipix1[i] * (T) ipix2[i];
    }
    break;
  case DIVIDE: 
    for (i=0; i<n; i++) {
      if (ipix2[i]!=0)
	opix[i] = (T) ipix1[i]/(T) ipix2[i];
      else 
	opix[i] = (T) max;
    }
    break;
  default:
    break;
  }

}

/*
 *+ 
 * FUNCTION NAME: template<class T> void pix_image_op_scalar
 *                template<class T, class U, class V> void image_op_scalar
 * 
 * INVOCATION: pix_image_op_scalar(int n, T *opix, Image* im, T scalar)
 *             image_op_scalar(int n, T *opix, U *ipix1, V scalar)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  1. template function that applies  im1 "op" scalar, with output to opix
 *  2. Template function for applying "op" to image and a scalar of different data types
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T> void pix_image_op_scalar(int n, int op, T *opix, Image* im, T scalar)
{
  switch (im->bitpix) {
  case BYTE_BITPIX: 
    image_op_scalar(n, op, opix, im->pix, scalar);
    break;
  case SHORT_BITPIX:
    if (im->unsigned_data)
      image_op_scalar(n, op, opix, im->upix, scalar);
    else 
      image_op_scalar(n, op, opix, im->spix, scalar);
    break;
  case INT_BITPIX:
    if (im->unsigned_data)
      image_op_scalar(n, op, opix, im->uipix, scalar);
    else 
      image_op_scalar(n, op, opix, im->ipix, scalar);
    break;
  case FLOAT_BITPIX:
    image_op_scalar(n, op, opix, im->fpix, scalar);
    break;
  case DOUBLE_BITPIX:
    image_op_scalar(n, op, opix, im->dpix, scalar);
    break;
  }
}

template<class T, class U, class V> void image_op_scalar(int n, int op, T *opix, U *ipix1, V scalar)
{
  int i;
  // NB MINUS and DIV are implemented as reciprocals of PLUS and MULTIPLY
  switch(op){
  case PLUS:
    for (i=0; i<n; i++) {
      opix[i] = (T) ipix1[i] + (T) scalar;
    }
    break;
  case MULTIPLY:
    for (i=0; i<n; i++) {
      opix[i] = (T) ipix1[i] * (T) scalar;
    }
    break;
  default:
    break;
  }
}
/*
 *+ 
 * FUNCTION NAME: template<class T> void pix_scalar_minus_image
 *                template<class T, class U, class V> void scalar_minus_image
 * 
 * INVOCATION: pix_scalar_minus_image(int n, T *opix, const Image* im, T scalar)
 *             scalar_minus_image(int n, T *opix, U *ipix, V scalar)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  1. template function that subtracts im1 from scalar with output to opix
 *  2. Template function for subtracting an image from an scalar
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T> void pix_scalar_minus_image(int n, T *opix, const Image* im, T scalar)
{
  switch (im->bitpix) {
  case BYTE_BITPIX: 
    scalar_minus_image(n, opix, im->pix, scalar);
    break;
  case SHORT_BITPIX:
    if (im->unsigned_data)
      scalar_minus_image(n, opix, im->upix, scalar);
    else 
      scalar_minus_image(n, opix, im->spix, scalar);
    break;
  case INT_BITPIX:
    if (im->unsigned_data)
      scalar_minus_image(n, opix, im->uipix, scalar);
    else 
      scalar_minus_image(n, opix, im->ipix, scalar);
    break;
  case FLOAT_BITPIX:
    scalar_minus_image(n, opix, im->fpix, scalar);
    break;
  case DOUBLE_BITPIX:
    scalar_minus_image(n, opix, im->dpix, scalar);
    break;
  }
}
template<class T, class U, class V> void scalar_minus_image(int n, T *opix, U *ipix, V scalar)
{
  int i;
  for (i=0; i<n; i++) {
    opix[i] = (T) scalar - (T) ipix[i];
  }
}
/*
 *+ 
 * FUNCTION NAME: template<class T, class U, class V> void scalar_div_image
 * 
 * INVOCATION: scalar_div_image(int n, T *opix, U *ipix, V scalar, U max)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  Template function for dividing a scalar by an image of different data types
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T, class U, class V> void scalar_div_image(int n, T *opix, U *ipix, V scalar, U max)
{
  int i;
  for (i=0; i<n; i++) {
    if (ipix[i]!=0)
      opix[i] = (T)scalar/(T)ipix[i];
    else 
      opix[i] = (T)max;
  }
}

/*
 *+ 
 * FUNCTION NAME: template<class T, class U> void copy_image(int n, T *opix, U *ipix)
 * 
 * INVOCATION: copy_image(int n, T *opix, U *ipix)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  Template function for copying an image to an output image
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template<class T, class U> void copy_image(int n, T *opix, U *ipix)
{
  int i;
  for (i=0; i<n; i++) opix[i] = ipix[i];
}

/*
 *+ 
 * FUNCTION NAME: template<class T> int cosmic
 * 
 * INVOCATION: cosmic(T* image, int nx, int ny, double exposed, float shieldcover, 
 *                    float rate, bool mask, Image_Pixel P) 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * ! image - image buffer
 * > nx - number of pixels in x direction
 * > ny - number in y direction
 * > exposed - Number of seconds of integration for the image.
 * > shieldcover - Cutoff angle (in degrees) due to shield, assume shield is perfect 
 * > rate - rate at which cosmic rays are incident (rays/s/cm/cm)
 * > mask - set if required to add area of effect mask
 * > P - size of each pixel in microns in 3 dimensions
 * 
 * FUNCTION VALUE: Number of hits
 * 
 * PURPOSE: generate simulated cosmic ray hits on a detector
 * 
 * DESCRIPTION: Some assumptions:
 *  1.  All CR's liberate 100 (+/- 10) electrons per 0.1 micron travel.  10% 
 *      of the cosmic rays are He nuclei, and thus have 4x the effect.
 *
 *  2.  The plane of the detector (+/- some extra) is blocked by a shield.
 *
 *  3.  The CR shield stops 100% of CRs, regardless of energy.
 *
 *  4.  A CR event in the detector has no lasting effect.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 

 * ACKNOWLEDGMENT:
 *   Written by: Joel D. Offenberg, Raytheon ITSS
 *
 *- 
 */
template<class T> int cosmic(T* image, int nx, int ny, double exposed, float shieldcover, 
			      float rate, bool mask, Image_Pixel P) 
{
  const float deg2rad = M_PI/180.;        // Number of radians per degree
  float n_rays;                           // # cosmic rays on detector 
  int i;                     
  float xpos, ypos, zpos;                 // position of cosmic ray hit on detector 
  float theta, cosphi,sinphi;             // direction angles (and cosine thereof) of cosmic ray velocity 
  float dpath;                            // Number of microns per step.  In theory,
                                          // this is 1 by definition, except for the last
                                          // step, but instead we seem to be assuming 
                                          // even not-quite-micron steps. 
  float depath;                           // Number of electrons liberated per step. 
  float dx, dy, dz;                       // Amount of motion per step 
  float charge;                           // charge from a cosmic ray - use random number generator
  int NHe = 0;                            // count of He nucleii.
  float crmask[3][3] = {{1.06e-3,1.66e-2,1.06e-3}, // area of effect mask - the values are based on 
			{1.66e-2,1.00,1.66e-2},    // Bernie Rauscher's cosmic ray measurements.  
			{1.06e-3,1.66e-2,1.06e-3}};
  int xxpp,yypp;
  float crcont;

  // Calculate number of rays based on rate for time exposed
  n_rays = nx * P.x * 1e-4 * ny * P.y * 1e-4 * rate * exposed;

  n_rays = poidev(n_rays);            // Add poisson statistics.  
  if (n_rays < 0)                     // Correct for negative number 
    n_rays = 0; 

  for (i=0; i<n_rays; i++) {
    // Generate random numbers between 0..nx-1 and 0..ny-1. 
    xpos = RAND * (float) nx;
    ypos = RAND * (float) ny;
    zpos = 0.0;
    
    theta = RAND*2*M_PI;

    sinphi = sqrt(RAND);
    cosphi = sqrt(1.0 - sinphi*sinphi);

    if (asin(sinphi)/deg2rad > shieldcover) {

      // 10% of the time, the CR will be a He nucleus, with 4 times the effect 
      charge = (RAND > 0.9) ? 2. : 1.;  
      if (charge == 2.) 
	NHe++;
      
      depath = 100.;
      dpath = 0.1;
      
      dx = cos(theta) * dpath * sinphi;    // x element per step 
      dy = sin(theta) * dpath * sinphi;    // y element per step 
      dz = dpath * cosphi;                 // z element per step 
      
      // Compute position of cosmic ray at each step and add appropriate 
      // signal to detector array.  Stop when outside the detector. 
      for (zpos = 0.0; (zpos <= P.z) && (xpos>=0) && (ypos>=0) && (xpos<nx) && (ypos<ny); zpos += dz) {
	if (mask) {
	  // Add cosmic ray area-of-effect mask.  
	  crcont = depath*(charge*charge);
	  
	  for (xxpp=0; xxpp<=2; xxpp++) {
	    for (yypp=0; yypp<=2; yypp++) {
	      if ((xxpp+xpos-1 >= 0) && (yypp + ypos-1 >= 0) && 
		  (xxpp+xpos-1 < nx) && (yypp + ypos-1 < ny)) {
		image[(int) xpos + xxpp-1 + nx * ((int) ypos+yypp-1)] += (T) (poidev(crcont) * crmask[xxpp][yypp]);
	      }
	    }
	  } 
	} else {
	  image[(int) xpos + nx * (int) ypos] += (T) poidev(depath*(charge*charge));
	}
	xpos += dx/P.x;
	ypos += dy/P.y;
      }
      
      // If the CR hits the bottom of the detector the odds are it won't hit it
      // on an even increment in the steps above.  Thus, add an extra term for 
      // the last little bit.  The CR-area PSF is not applied here, since this
      // is just a little bit at the end.  [It probably should be, but we'll
      // live for now.]
      
      if ((xpos>=0) && (ypos>=0) && (xpos<nx) && (ypos<ny)) {
	image[(int) xpos + nx * (int) ypos] += (T) poidev(depath*(charge*charge)*(P.z-zpos+dz)/dz);
      }
    }
  }
  
  return (int)n_rays;

  //printf("\t%d CRs, %d He\n",(int) n_rays, NHe);
}
template int cosmic<unsigned char>(unsigned char*,int,int,double,float,float,bool,Image_Pixel);
template int cosmic<char>(char*,int,int,double,float,float,bool,Image_Pixel);
template int cosmic<short>(short*,int,int,double,float,float,bool,Image_Pixel);
template int cosmic<unsigned short>(unsigned short*,int,int,double,float,float,bool,Image_Pixel);
template int cosmic<int>(int*,int,int,double,float,float,bool,Image_Pixel);
template int cosmic<unsigned int>(unsigned int*,int,int,double,float,float,bool,Image_Pixel);
template int cosmic<float>(float*,int,int,double,float,float,bool,Image_Pixel);
template int cosmic<double>(double*,int,int,double,float,float,bool,Image_Pixel);
