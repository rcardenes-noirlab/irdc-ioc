/*
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME dummy_setup.cc
 *
 * GENERAL DESCRIPTION
 *
 *  Implements a DUMMY preferences class using base Preferences class
 *
 */

/* Include files */
#ifdef vxWorks
#include "configuration.h"
#endif
#include "common.h"
#include "dummy_setup.h"
/*
 *+
 * FUNCTION NAME: Dummy_Setup::Dummy_Setup():Preferences("dummy.setup","")
 *
 * INVOCATION:
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: A constructor for dummy setup file
 *
 * DESCRIPTION: Initialises setup values to sensible defaults
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Dummy_Setup::Dummy_Setup():
  Preferences("dummy.setup", 1, true, "")
{
  // TODO: Figure what to do with the sim image - doesn't get copied
  Dummy_Config_Data
    tmp_config_data = {
      "",//DEFAULT_SIM_IMAGE,
      DEFAULT_SIM_EXPTIME,
      DEFAULT_PIXEL_READ_TIME,
      DEFAULT_PIXEL_CLEAR_TIME,
      DEFAULT_COSMIC_RATE
    };
  config = tmp_config_data;
}
/*
 *+
 * FUNCTION NAME: void Dummy_Setup::put(void* itemp, int pos)
 *
 * INVOCATION:
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > itemp - item pointer - optionally put pars from this pointer arg
 * > pos - position in list to place item
 * Both above pars are ignored for dummy setup files - only one internal item
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Puts a new setup structure into internal table
 *
 * DESCRIPTION: Uses table put methods to place each setup par
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Setup::put(void* itemp, int pos)
{
  Table_Item* item;

  if ((item = find("NONAME")) != NULL) {
    throw Error("Preferences already exist in table!",E_ERROR,-1,__FILE__,__LINE__);
  }

  item = new Table_Item("NONAME");
  item->put("sim_image",config.sim_image);
  item->put("sim_exptime",config.sim_exptime);
  item->put("pixel_read_time",config.pixel_read_time);
  item->put("pixel_clear_time",config.pixel_clear_time);
  item->put("cosmic_rate",config.cosmic_rate);

  insert(item,pos);
}	
/*
 *+
 * FUNCTION NAME: void Dummy_Setup::get(Table_Item *item)
 *
 * INVOCATION:
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > item - get dummy pars from this table item
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Gets a setup structure from internal table item
 *
 * DESCRIPTION: Uses table find methods to fetch each setup par
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Setup::get(Table_Item *item)
{
  // Now for each possible setup parameter try to find in table
  // General preferences
  item->find("sim_image",config.sim_image,FILENAMELEN);
  item->find("sim_exptime",config.sim_exptime);
  item->find("pixel_read_time",config.pixel_read_time);
  item->find("pixel_clear_time",config.pixel_clear_time);
  item->find("cosmic_rate",config.cosmic_rate);
}
/*
 *+
 * FUNCTION NAME: Dummy_Setup::~Dummy_Setup()
 *
 * INVOCATION: delete dummy
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Destructor
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Dummy_Setup::~Dummy_Setup()
{
}
/*
 *+
 * FUNCTION NAME: dummySetupTest()
 *
 * INVOCATION: dummySetupTest
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Destructor
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void dummySetupTest()
{
  Dummy_Setup setup;
  try {
    cout << "Reading setup file " << DUMMY_SETUP << endl;
    setup.read(DUMMY_SETUP);
    cout << "Sim Image =" << setup.config.sim_image << endl;
    cout << "Sim exposure time =" << setup.config.sim_exptime << endl;
    cout << "Pixel read time =" << setup.config.pixel_read_time << endl;
    cout << "Pixel clear time =" << setup.config.pixel_clear_time << endl;
    cout << "Cosmic ray rate =" << setup.config.cosmic_rate << endl;
  }
  catch (Error& ce) {
    ce.print_error(__FILE__,__LINE__);
  }
}



