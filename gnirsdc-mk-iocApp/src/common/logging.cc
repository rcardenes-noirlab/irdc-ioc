
#include "logging.h"

namespace logger {

	static constexpr Level default_level(Level::Min);
	static Level current(default_level);

	void message(Level log_level, const std::string msg)
	{
		if (log_level > current)
			return;

		errlogPrintf("%s\n", msg.c_str());
	}

	void message(const std::string msg)
	{
		message(getLevel(), msg);
	}

	Level getDefaultLevel() noexcept
	{
		return default_level;
	}

	Level getLevel() noexcept
	{
		return current;
	}

	void setLevel(Level debug)
	{
		current = debug;
	}

#define X(name, text) case Level::name: return #name;
	const char* to_string(Level debug) noexcept {
		switch(debug) {
			LEVEL_VALUES_LIST
			default: return "UNKNOWN";
		}
	}
#undef X
}
