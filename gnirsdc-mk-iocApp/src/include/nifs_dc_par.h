/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: gnirs_dc_par.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GNIRS PROJECT
 *
 * FILENAME gnirs_dc_par.h
 * 
 * PURPOSE
 * Include file with GNIRS DC parameter class definition
 *
 * $Log: gnirs_dc_par.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.1  2004/12/22 22:43:55  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.1  2001/10/19 00:24:43  pjy
 * Added for GNIRS DC
 *
 *
 */

#ifndef GNIRS_DC_PARAMETER
#define GNIRS_DC_PARAMETER

#include <vxworks.h>
#include <hash_map.h>
#include <string>

/* put definitions here */

// DC CAD parameters - these are set when CAD record input fields are changed
class DC_Parameter {
 private:
  hash_map<char, const string> paramName;
  hash_map<const string, const string> paramString;
  hash_map<const string, long> paramLong;
  hash_map<const string, double> paramDouble;
  hash_map<const string, double> paramMin;
  hash_map<const string, double> paramMax;

 public:

  // Constructor
  DC_Parameter();

  // Reset method - resets pars to default
  reset();

  // Read method - reads parameter names with min/max from file
  read(const string inputFile);

  // Parameter set methods - overloaded to support 3 Gemini types
  void set(const string param, const string value);
  void set(const string param, long value);
  void set(const string param, double value);

  // Parameter get methods - overloaded to support 3 Gemini types
  string get(const string param);
  long get(const string param);
  double get(const string param);

  // Destructor
  ~DC_Parameter();
};

// Include any function declarations here
void gnirs_check_parameter(DC_Parameter *par);

#endif /* GNIRS_DC_PAR */
