/*
 *   FILENAME
 *   --------
 *   syscad.c
 *
 *   PURPOSE
 *   -------
 *   This file contains the source for all the functions used by the
 *   CICS SYSTEM CAD records. These functions are used to validate the arguments
 *   given to the CAD record
 *
 *
 *   FUNCTION NAME(S)
 *   ----------------
 *   CADinit        - Initialise
 *   CADdatum       - Datum
 *   CADpark        - Park
 *   CADobserve     - Observe
 *   CADpause       - Pause
 *   CADcontinue    - Continue
 *   CADstop        - Stop
 *   CADabort       - Abort
 *   CADendObserve  - End Observe
 *   CADdebugInit   - Initialise debug
 *   CADdebug       - Debug
 *   CADreboot      - Reboot
 *
 *   DEPENDENCIES
 *   ------------
 *   The names of the subroutines in this file should be identical to those
 *   declared in the SNAM field of each CAD record. If a change is made to
 *   the name of a subroutine, that change should be reflected in the SNAM
 *   field, and vice versa.
 *
 *   The ordering of the arguments within each CAD record (A, B, C...) is
 *   defined in the description of the interface between the CAD database
 *   and its clients. Changes to that interface should be reflected in this
 *   file.
 *
 *   The input arguments for each CAD record (A, B, C...) are all strings.
 *   However, the data type of each output argument (VALA, VALB, VALC...)
 *   is determined by the (FTVA, FTVB, FTVC....) fields in the CAD record.
 *   The data types assumed here must match those declared in the CAD
 *   record properties
 *
 *   LIMITATIONS
 *   ------------
 *
 *   AUTHOR
 *   ------
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   HISTORY
 *   -------
 *INDENT-OFF*
 *
 * $Log: sysCad.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:52  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.7  2005/06/26 02:38:02  jarnyk
 * Changed CADpark to reject if not all of the mechanisms are datumed.
 *
 * Revision 1.6  2005/06/07 01:18:11  jarnyk
 * Temporary fix for Auspace cooldown release
 *
 * Revision 1.5  2004/08/20 02:47:38  pjy
 * Gem 8.6
 * Use cicsGetDebug
 *
 * Revision 1.4  2004/07/22 07:19:52  pjy
 * Gem 8.6 port
 *
 * Revision 1.3  2001/07/17 23:30:19  jarnyk
 * Fixed bug with use of sscanf (%d for a long)
 *
 * Revision 1.2  2001/07/17 23:00:38  jarnyk
 * Re-enabled CADobserve
 *
 * Revision 1.1.1.1  2000/09/21 01:33:38  jarnyk
 * Niri sources
 *
 * Revision 1.5  1999/11/14 02:05:43  yamada
 * Reformatted logs.
 *
 *
 *INDENT-ON*
 *   28-Mar-1996: Original version with just CADfilterSelect.        (smb)
 *   16-Apr-1996: Add CADfilterEngMove, CADfilterDatum and
 *                CADfilterPark.                                     (smb)
 *   16-Apr-1996: CADfilterEngMove renamed due to 15 character
 *                subroutine name length restriction in the
 *                CAD record.                                        (smb)
 *   03-May-1996: Modified to use CAD_ACCEPT/REJECT constants.       (smb)
 *   07-May-1996: All subroutine names shortened to reflect shorter
 *                names for CAD records. (I was hitting the 29
 *                character EPICS record name length limit as well
 *                as the 15 character subroutine name limit).        (smb)
 *   15-May-1996: Documentation tidied up.                           (smb)
 *   06-Jun-1996: Headers converted to Gemini standard format, as
 *                described in SPE-C-G0009. Functions CADinit and
 *                CADreset added.                                    (smb)
 *   12-Jun-1996: EPICS 3.12.2.Gem3 installed, plus latest IGPO
 *                extensions. CAD records now have a "MARK"
 *                directive.                                         (smb)
 *   19-Jun-1996: Headers modified to make them compatible with
 *                the "wflman" utility.                              (smb)
 *   25-Jun-1996: CADfiltSel and CADfiltPark modified to generate
 *                engineering position as output (in preparation
 *                for the use of lookup tables later).               (smb)
 *   11-Jul-1996: CAD records are not allowed to report anything
 *                other than reasons for failure through the MESS
 *                field. Diagnostic messages removed.                (smb)
 *   02-Aug-1996: Modified to make use of "lutin" and "lutout"
 *                records, so the CAD records no longer need to
 *                process lookup tables themselves.                  (smb)
 *   11-Oct-1996: Split into "sysCad" and "filtCad". "filtCad" will
 *                eventually be made more generic.                   (smb)
 *   04-Dec-1996: "printf" replaced by "c"icsLogMessage" or
 *                "cicsLogString".                                   (smb)
 *   05-Dec-1996: Components Controller and Detector Controller
 *                versions merged and moved to general CICS library,
 *                since both may need to use the same sequence
 *                commands.                                          (smb)
 *   13-Feb-1997: Start removing the "reset" command. Add "datum"
 *                and "park".                                        (smb)
 *   21-Mar-1997: "reboot" and "endObserve" added. Data label added
 *                to "observe".                                      (smb)
 *   16-Apr-1997: Second input argument of CAD records used to
 *                sense whether the instrument is busy and the
 *                sequence command should be rejected.               (smb)
 */

#include  <math.h>
#include  <time.h>
#include  <stdlib.h>
#include  <stdio.h>
#include  <string.h>
#include  <ctype.h>

#include  <registryFunction.h>
#include  <epicsExport.h>
#include  <dbDefs.h>
#include  <cadRecord.h>
#include  <dbCommon.h>
#include  <recSup.h>
#include  <cad.h>

#include  "logging.h"

static logger::Level debug(logger::Level::None);   /* Required init debug level */


extern "C" {

long checkBusy( cadRecord * );
void upcase(char *s);
long CADinit( cadRecord *pcad );
long CADdatum( cadRecord *pcad );
long CADpark( cadRecord *pcad );
long CADobserve( cadRecord *pcad );
long CADpause( cadRecord *pcad );
long CADcontinue( cadRecord *pcad );
long CADstop( cadRecord *pcad );
long CADabort( cadRecord *pcad );
long CADendObserve( cadRecord *pcad );
long CADdebugInit( cadRecord *pcad );
long CADdebug( cadRecord *pcad );
long CADreboot( cadRecord *pcad );
long checkBusy( cadRecord *pcad );

}

/* ===================================================================== */

/*+
 *   Function name:
 *   CADinit
 *
 *   Purpose:
 *   User defined function for "init" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the init CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *
 *   init is the command for reinitializing the subsystem.
 *   The command has no arguments
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADinit( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *      > pcad->b     long        Instrument busy flag. The command will be
 *                                rejected if this flag happens to be exactly
 *                                equal to CICS_BUSY.
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   06-Jun-1996: Original version.                        (smb)
 *   16-Apr-1997: Check on instrument being busy added.    (smb)
 *-
 */

long CADinit( cadRecord *pcad )
{
  long status;         /* return status */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

      logger::message(logger::Level::Min, "init - MARK directive.");
      break;

    case menuDirectivePRESET:

/* CAD PRESET directive detected. There are no command-specific arguments
 * to be checked.
 */

      logger::message(logger::Level::Min, "init - PRESET directive.");

/*
 * Reject the command if the second argument indicates the instrument is
 * busy.
 */

      status = checkBusy( pcad );
      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

      logger::message(logger::Level::Min, "init - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Do nothing. The directive is being
 * monitored by the CICS sequence code, which will start the appropriate
 * action.
 */

      logger::message(logger::Level::Min, "init - START directive.");
      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. It is not possible to stop initialization
 * once it has started, so reject the directive.
 */
      logger::message(logger::Level::None, "init - STOP directive. Cannot be stopped.");

      strncpy( pcad->mess, "init - Cannot be stopped", MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "init - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

/* ===================================================================== */

/*+
 *   Function name:
 *   CADdatum
 *
 *   Purpose:
 *   This routine is called whenever the datum CAD record is
 *   processed. It checks that the command
 *   is acceptable and returns a status and a message, which are written
 *   to the VAL and MESS fields of the CAD record.
 *
 *   datum is the command for datuming the subsystem (i.e. making all the
 *   components locate their reference or index points).
 *   The command has no arguments.
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADdatum( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *      > pcad->b     long        Instrument busy flag. The command will be
 *                                rejected if this flag happens to be exactly
 *                                equal to CICS_BUSY.
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   13-Feb-1997: Original version.                        (smb)
 *   16-Apr-1997: Check on instrument being busy added.    (smb)
 *-
 */

long CADdatum( cadRecord *pcad )
{
  long status;         /* return status */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

     logger::message(logger::Level::Min, "datum - MARK directive.");
      break;

    case menuDirectivePRESET:

/* CAD PRESET directive detected. There are no command-specific arguments
 * to be checked.
 */

     logger::message(logger::Level::Min, "datum - PRESET directive.");

/*
 * Reject the command if the second argument indicates the instrument is
 * busy.
 */

      status = checkBusy( pcad );
      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected */

     logger::message(logger::Level::Min, "datum - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Do nothing. The directive is being
 * monitored by the CICS sequence code, which will start the appropriate
 * action.
 */

     logger::message(logger::Level::Min, "datum - START directive.");
      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. It is not possible to stop a datum
 * once it has started, so reject the directive.
 */
      logger::message(logger::Level::None,
		"datum - STOP directive. Cannot be stopped.");

      strncpy( pcad->mess, "datum - Cannot be stopped",  MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "datum - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

/* ===================================================================== */

/*+
 *   Function name:
 *   CADpark
 *
 *   Purpose:
 *   This routine is called whenever the park CAD record is
 *   processed. It checks that the command
 *   is acceptable and returns a status and a message, which are written
 *   to the VAL and MESS fields of the CAD record.
 *
 *   park is the command for parking all the components in the subsystem
 *   (i.e. moving them to a position where the instrument can be safely
 *   shut down).
 *   The command has no arguments.
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADpark( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *      > pcad->b     long        Instrument busy flag. The command will be
 *                                rejected if this flag happens to be exactly
 *                                equal to CICS_BUSY.
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   13-Feb-1997: Original version.                        (smb)
 *   16-Apr-1997: Check on instrument being busy added.    (smb)
 *-
 */


long CADpark( cadRecord *pcad )
{

	const char* ALL_DATUMED="1"; 	/* Value of input A if all mechanisms are datumed */
	long status;         /* return status */
 
	/* Initialise CAD status */
	status = CAD_ACCEPT;

	/* Switch according to the CAD directive in DIR field */
	switch (pcad->dir) {
		case menuDirectiveMARK:
		
			/* CAD MARK directive detected. Nothing needs to be done.*/
			logger::message(logger::Level::Min, "park - MARK directive.");
			break;

		case menuDirectivePRESET:

			/* CAD PRESET directive detected. There are no command-specific arguments
			 * to be checked.
			 */
			logger::message(logger::Level::Min, "park - PRESET directive.");

			/*
			 * Reject the command if not all mechanisms datumed.
			 */
			if (strcmp(pcad->a, ALL_DATUMED) == 0) {
				strncpy( pcad->mess,"Not all mechanisms are datumed",MAX_STRING_SIZE);
				logger::message(logger::Level::None,  "Park attempted when not all mechanisms datumed - command rejected");
				status = CAD_REJECT;
			} else
				status = CAD_ACCEPT;
	 
			break;

		case menuDirectiveCLEAR:

			/* CAD CLEAR directive detected */
			logger::message(logger::Level::Min, "park - CLEAR directive.");
			break;

		case menuDirectiveSTART:

			/* CAD START directive detected. Do nothing. The directive is being
			 * monitored by the CICS sequence code, which will start the appropriate
			 * action.
			 */
			logger::message(logger::Level::Min, "park - START directive.");
			break;

		case menuDirectiveSTOP:

			/* CAD STOP directive detected.
			 */
			logger::message(logger::Level::None, "park - STOP directive.");
			break;

		default:

			/* Unrecognised CAD directive detected. This is regarded as an error. */
			strncpy( pcad->mess, "park - Unrecognised CAD directive",
					 MAX_STRING_SIZE );
			status = CAD_REJECT;
			break;

		}
	return status;
}

/* ===================================================================== */

/*+
 *   Function name:
 *   CADobserve
 *
 *   Purpose:
 *   User defined function for "observe" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the observe CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *
 *   observe is the command for starting an observation.
 *   The command has one argument - the data label.
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADobserve( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *      > pcad->a     *string     CAD input argument A
 *                                = data label
 *      > pcad->b     long        Instrument busy flag. The command will be
 *                                rejected if this flag happens to be exactly
 *                                equal to CICS_BUSY.
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *      < pcad->vala  *string     CAD output value A
 *                                = data label
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   11-Nov-1996: Original version.                              (smb)
 *   21-Mar-1997: Data label and "applyC" arguments added.       (smb)
 *   16-Apr-1997: Check on instrument being busy added.    (smb)
 *-
 */

long CADobserve( cadRecord *pcad )
{
  long status;         /* return status */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

     logger::message(logger::Level::Min, "observe - MARK directive.");
      break;

    case menuDirectivePRESET:

/* CAD PRESET directive detected. The data label is assumed valid.
 * IS THERE A WAY OF CHECKING THE DATA LABEL?
 */

     logger::message(logger::Level::Min, "observe - PRESET directive.");

     printf("observe(sysCad) - Data label = %s\n", pcad->a);
     logger::message(logger::Level::Full, "Data label = %s", pcad->a);

/*
 * Reject the command if the second argument indicates the instrument is
 * busy.
 */

      status = checkBusy( pcad );

/*
 * Only copy the data label to the output if the command has not been
 * rejected
 */

      if ( status != CAD_REJECT ) strcpy( static_cast<char*>(pcad->vala), static_cast<char*>(pcad->a) );
      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

     logger::message(logger::Level::Min, "observe - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Do nothing. The directive is being
 * monitored by the CICS DC sequence code, which will start the appropriate
 * action.
 */

     logger::message(logger::Level::Min, "observe - START directive.");
      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. The normal way to stop an observation is
 * through the STOP sequence command, so reject this directive.
 */
      logger::message(logger::Level::None,
        "observe - STOP directive. Use STOP command instead.");

      strncpy( pcad->mess, "observe - Use STOP command instead.",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "observe - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

/* ===================================================================== */

/*+
 *   Function name:
 *   CADpause
 *
 *   Purpose:
 *   User defined function for "pause" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the pause CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *
 *   pause is the command for pausing an observation.
 *   The command has no arguments
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADpause( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   11-Nov-1996: Original version.                        (smb)
 *-
 */

long CADpause( cadRecord *pcad )
{
  long status;         /* return status */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

     logger::message(logger::Level::Min, "pause - MARK directive.");
      break;

    case menuDirectivePRESET:

/* CAD PRESET directive detected. There are no command arguments
   to be checked.
 */

     logger::message(logger::Level::Min, "pause - PRESET directive.");
      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

     logger::message(logger::Level::Min, "pause - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Do nothing. The directive is being
 * monitored by the CICS DC sequence code, which will start the appropriate
 * action.
 *
 * NOTE: This directive should be rejected if an observation is not taking
 * place. This will involve checking the "observeC" CAR record at this point.
 * I need to find out how to do this.
 *
 */

     logger::message(logger::Level::Min, "pause - START directive.");
      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. The normal way to restart a paused observation
 * is through the CONTINUE sequence command, so reject this directive.
 */
      logger::message(logger::Level::None,
        "pause - STOP directive. Use CONTINUE command instead.");

      strncpy( pcad->mess, "pause - Use CONTINUE command instead.",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "pause - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

/* ===================================================================== */

/*+
 *   Function name:
 *   CADcontinue
 *
 *   Purpose:
 *   User defined function for "continue" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the continue CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *
 *   continue is the command for continuing a paused observation.
 *   The command has no arguments
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADcontinue( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *      > pcad->b     long        Instrument busy flag. The command will be
 *                                rejected if this flag happens to be exactly
 *                                equal to CICS_BUSY.
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   11-Nov-1996: Original version.                        (smb)
 *   16-Apr-1997: Check on instrument being busy added.    (smb)
 *-
 */

long CADcontinue( cadRecord *pcad )
{
  long status;         /* return status */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

     logger::message(logger::Level::Min, "continue - MARK directive.");
      break;

    case menuDirectivePRESET:

/* CAD PRESET directive detected. There are no command-specific arguments
 * to be checked.
 */

     logger::message(logger::Level::Min, "continue - PRESET directive.");

/*
 * Reject the command if the second argument indicates the instrument is
 * busy.
 */

      status = checkBusy( pcad );
      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

     logger::message(logger::Level::Min, "continue - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Do nothing. The directive is being
 * monitored by the CICS DC sequence code, which will start the appropriate
 * action.
 *
 * NOTE: This directive should be rejected if an observation is not paused.
 * This will involve checking the "observeC" CAR record at this point.
 * I need to find out how to do this.
 *
 */

     logger::message(logger::Level::Min, "continue - START directive.");
      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. The normal way to stop an observation is
 * through the STOP sequence command, so reject this directive.
 */
      logger::message(logger::Level::None,
        "continue - STOP directive. Cannot be stopped.");

      strncpy( pcad->mess, "continue - Cannot be stopped", MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "continue - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

/* ===================================================================== */

/*+
 *   Function name:
 *   CADstop
 *
 *   Purpose:
 *   User defined function for "stop" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the stop CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *
 *   stop is the command for stopping an observation and keeping the data.
 *   The command has no arguments
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADstop( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   11-Nov-1996: Original version.                        (smb)
 *-
 */

long CADstop( cadRecord *pcad )
{
  long status;         /* return status */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

     logger::message(logger::Level::Min, "stop - MARK directive.");
      break;

    case menuDirectivePRESET:

/* CAD PRESET directive detected. There are no command arguments
   to be checked.
 */

     logger::message(logger::Level::Min, "stop - PRESET directive.");
      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

     logger::message(logger::Level::Min, "stop - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Do nothing. The directive is being
 * monitored by the CICS DC sequence code, which will start the appropriate
 * action.
 *
 * NOTE: This directive should be rejected if an observation is not taking
 * place. This will involve checking the "observeC" CAR record at this point.
 * I need to find out how to do this.
 *
 */

     logger::message(logger::Level::Min, "stop - START directive.");
      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. The normal way to stop an observation is
 * through the STOP sequence command, so reject this directive.
 */
      logger::message(logger::Level::None,
        "stop - STOP directive. STOP cannot be stopped.");

      strncpy( pcad->mess, "stop - Cannot be stopped", MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "stop - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

/* ===================================================================== */

/*+
 *   Function name:
 *   CADabort
 *
 *   Purpose:
 *   User defined function for "abort" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the abort CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *
 *   abort is the command for stopping an observation without keeping the data.
 *   The command has no arguments
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADabort( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   11-Nov-1996: Original version.                        (smb)
 *-
 */

long CADabort( cadRecord *pcad )
{
  long status;         /* return status */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

     logger::message(logger::Level::Min, "abort - MARK directive.");
      break;

    case menuDirectivePRESET:

/* CAD PRESET directive detected. There are no command arguments
   to be checked.
 */

     logger::message(logger::Level::Min, "abort - PRESET directive.");
      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

     logger::message(logger::Level::Min, "abort - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Do nothing. The directive is being
 * monitored by the CICS DC sequence code, which will start the appropriate
 * action.
 *
 * NOTE: This directive should be rejected if an observation is not taking
 * place. This will involve checking the "observeC" CAR record at this point.
 * I need to find out how to do this.
 *
 */

     logger::message(logger::Level::Min, "abort - START directive.");
      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. The normal way to abort an observation is
 * through the ABORT sequence command, so reject this directive.
 */
      logger::message(logger::Level::None,
        "abort - STOP directive. ABORT cannot be stopped.");

      strncpy( pcad->mess, "abort - Cannot be stopped", MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "abort - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

/* ===================================================================== */

/*+
 *   Function name:
 *   CADendObserve
 *
 *   Purpose:
 *   User defined function for "endObserve" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the endObserve CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *
 *   endObserve is the command for signalling the end of an observation,
 *   but is ignored by Instrument Control Systems.
 *   The command has no arguments
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADendObserve( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   18-Mar-1997: Original version.                        (smb)
 *-
 */

long CADendObserve( cadRecord *pcad )
{
  long status;         /* return status */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

     logger::message(logger::Level::Min, "endObserve - MARK directive.");
      break;

    case menuDirectivePRESET:

/* CAD PRESET directive detected. There are no command arguments
   to be checked.
 */

     logger::message(logger::Level::Min, "endObserve - PRESET directive.");
      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

     logger::message(logger::Level::Min, "endObserve - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Do nothing. The directive is being
 * monitored by the CICS DC sequence code, which will start the appropriate
 * action.
 *
 * NOTE: This directive should be rejected if an observation is not taking
 * place. This will involve checking the "observeC" CAR record at this point.
 * I need to find out how to do this.
 *
 */

      logger::message(logger::Level::Min, "endObserve - START directive (command ignored).");
      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. The normal way to endObserve an observation is
 * through the ABORT sequence command, so reject this directive.
 */
      logger::message(logger::Level::None, "endObserve - STOP directive - cannot be stopped.");

      strncpy( pcad->mess, "endObserve - Cannot be stopped", MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "endObserve - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

/* ===================================================================== */


/*+
 *   Function name:
 *   CADdebugInit
 *
 *   Purpose:
 *   Initialisation function for "debug" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the debug CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *   If a valid debug level is given the internal debug level is altered.
 *
 *   debug is the command for setting the desired debugging level.
 *   The command has no arguments
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADdebugInit( pcad );
 *
 *   Parameters in:
 *      None
 *
 *   Parameters out:
 *      None
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   16-Jan-1997: Original version.                        (smb)
 *-
 */

long CADdebugInit( cadRecord *pcad )
{
  long status;         /* return status */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Set the inital debug level to NONE. */

  debug = logger::Level::None;
  logger::setLevel( debug );

  return status;
}

/* ===================================================================== */


/*+
 *   Function name:
 *   CADdebug
 *
 *   Purpose:
 *   User defined function for "debug" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the debug CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *   If a valid debug level is given the internal debug level is altered.
 *
 *   debug is the command for setting the desired debugging level.
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADdebug( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *      > pcad->a     *string     CAD input argument A
 *                                = Requested debug level
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *      < pcad->vala  *string     CAD output value A
 *                                = Set debug level
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   17-Mar-1997: Original version.                        (smb)
 *-
 */
void upcase(char *s)
{
  char *c;

  for (c = s; *c != '\0'; c++) {
	*c = (char)toupper(*c);
  }
}

long CADdebug( cadRecord *pcad )
{
  long status;         /* return status */

/* The magic number 40 is that used in cadRecord.h.It is a #define
   owing to the fact that ANSI C rejects using a const variable for
   an array length. */
#define CAD_INPUT_LENGTH  40
 
  /* Buffer to which we copy the input before converting it to
     upper case */
  char buf[CAD_INPUT_LENGTH];

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

     logger::message(logger::Level::Min, "debug - MARK directive.");
      break;

    case menuDirectivePRESET:

	  /* Copy the input to a buf and then convert it to upper case
         for comparison. */
	  strncpy(buf,pcad->a, CAD_INPUT_LENGTH-1);
	  upcase(buf);

/* CAD PRESET directive detected. Check the debug level given is valid.
 */

     logger::message(logger::Level::Min, "debug - PRESET directive.");
     logger::message(logger::Level::Full, "Requested debug level = %s", pcad->a);

      /* Get the current debugLevel  - PJY */
      debug = logger::getLevel();

      if ( strcmp( buf, "NOLOG" ) == 0 )
	  {
        debug = logger::Level::NoLog;
        strcpy( static_cast<char*>(pcad->vala), static_cast<char*>(pcad->a) );
      }
      else if ( strcmp( buf, "NONE" ) == 0 )
      {
          debug = logger::Level::None;
          strcpy( static_cast<char*>(pcad->vala), static_cast<char*>(pcad->a) );
      }
      else if ( strcmp( buf, "MIN" ) == 0 )
      {
		debug = logger::Level::Min;
		strcpy( static_cast<char*>(pcad->vala), static_cast<char*>(pcad->a) );
	  } else if ( strcmp( buf, "FULL" ) == 0 )
	  {
		debug = logger::Level::Full;
		strcpy( static_cast<char*>(pcad->vala), static_cast<char*>(pcad->a) );
	  }
	  else
	  {
		strncpy(pcad->mess, "debug - Invalid debug level ", MAX_STRING_SIZE);
		status = CAD_REJECT;
	  }

      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

     logger::message(logger::Level::Min, "debug - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Set the required debug level.
 *
 */

      logger::message(logger::Level::Min, "debug - START directive. New level=%s", logger::to_string(debug) );

      logger::setLevel( debug );

      logger::message(logger::Level::Min, "debug - New level=%s", logger::to_string(logger::getLevel()) );

      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. The normal way to debug an observation is
 * through the ABORT sequence command, so reject this directive.
 */
      logger::message(logger::Level::None,
        "debug - STOP directive. debug cannot be stopped.");

      strncpy( pcad->mess, "debug - Cannot be stopped", MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "debug - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

/* ===================================================================== */


/*+
 *   Function name:
 *   CADreboot
 *
 *   Purpose:
 *   User defined function for "reboot" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the reboot CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *
 *   reboot is the command for rebooting the IOC.
 *   The command has no arguments
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADreboot( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure.
 *
 *   It is also assumed some other subroutine will do the actual rebooting.
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   16-Mar-1997: Original version.                        (smb)
 *-
 */

long CADreboot( cadRecord *pcad )
{
  long status;         /* return status */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

     logger::message(logger::Level::Min, "reboot - MARK directive.");
      break;

    case menuDirectivePRESET:

/* CAD PRESET directive detected. Check that a reboot is allowed at this time.
 * AT THE MOMENT IT IS ASSUMED A REBOOT IS ALWAYS ALLOWED.
 */

     logger::message(logger::Level::Min, "reboot - PRESET directive.");

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

     logger::message(logger::Level::Min, "reboot - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/*
 * CAD START directive detected.
 */

     logger::message(logger::Level::Min, "reboot - START directive." );

/* Note that "reboot()" is not called here. It is assumed a subroutine record
 * will be activated to do the actual rebooting once the CAR record has been
 * set BUSY.
 */

      break;

    case menuDirectiveSTOP:

      logger::message(logger::Level::None,
        "reboot - STOP directive. Reboot cannot be stopped.");

      strncpy( pcad->mess, "reboot - Cannot be stopped", MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "reboot - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

/* ===================================================================== */


/*+
 *   Function name:
 *   checkBusy
 *
 *   Purpose:
 *   Check whether instrument is busy, based on second CAD argument.
 *
 *   Purpose:
 *   This is a utility function, called by the above CAD routines, which
 *   looks at the second CAD argument, pcad->b, and determines whether
 *   a command should be rejected due to the instrument being busy.
 *
 *   Invocation:
 *   cadRecord *pcad;
 *   status = CADreboot( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *      > pcad->b     long        Instrument busy flag. The command will be
 *                                rejected if this flag happens to be exactly
 *                                equal to CICS_BUSY.
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *
 *   Return value:
 *      < status      long        Status value written to the CAD VAL field.
 *                                Returned CAD_REJECT is instrument is busy.
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure.
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   16-Apr-1997: Original version.                        (smb)
 *   16-Apr-1997: Check on instrument being busy added.    (smb)
 *-
 */

long checkBusy( cadRecord *pcad )
{
  long status;         /* return status */
  long cvstat;         /* conversion status */
  long busyFlag;       /* busy flag */

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Convert the second input argument to an integer. */

  cvstat = sscanf( pcad->b, "%ld", &busyFlag );

  logger::message(logger::Level::Full, "Busy flag =%ld\n", busyFlag );

/*
 * Check the input argument was successfully converted. (cvstat contains the
 * number of items that "sscanf" has successfully converted, and there
 * should be 1).
 * If the argument cannot be converted, just assume the command can go ahead.
 */

  if ( cvstat == 1 )
  {

/*
 * If the argument contains "CICS_BUSY" the instrument is busy and the
 * command should be rejected.
 */

    if ( busyFlag == 1 ) // CICS_BUSY
    {

      logger::message(logger::Level::None, "Instrument is busy - command rejected." );
      strncpy( pcad->mess, "Instrument is busy", MAX_STRING_SIZE );
      status = CAD_REJECT;
    }
  }

  return status;
}

/* ===================================================================== */
