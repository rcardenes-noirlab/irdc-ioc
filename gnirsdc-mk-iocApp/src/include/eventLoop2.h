#ifndef EL2_H
#define	EL2_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef	vxWorks
#include <dhsVxPthread.h>
#else	/* vxWorks */
#include <pthread.h>
#endif	/* vxWorks */


extern void		pjy_dhsEl( tEventLoop *, DHS_COND_FN, void *, 
				boolean, boolean, DHS_STATUS * );
extern boolean		pjy_dhsElNoExit( tEventLoop * );
extern boolean		pjy_dhsElStopped( tEventLoop * );
extern void		pjy_dhsElStart( tEventLoop *, DHS_STATUS * );
extern void		*pjy_dhsElStartThread( tEventLoop * );
extern void		pjy_dhsSystemEvent( IMP_MsgInfo *, tDhsConnect *,
				DHS_STATUS * );
extern void		pjy_dhsUserEvent( int, tDhsUsrMsg, void *, int,
				tDhsConnect *, DHS_STATUS * );

#ifdef __cplusplus
}
#endif
#endif /* el2 */
