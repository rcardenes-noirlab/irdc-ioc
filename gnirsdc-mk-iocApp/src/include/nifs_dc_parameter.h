/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: gnirs_dc_parameter.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GNIRS PROJECT
 *
 * FILENAME 
 * 
 * PURPOSE
 *
 * $Log: gnirs_dc_parameter.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.1  2004/12/22 22:43:56  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.1  2001/10/19 00:24:44  pjy
 * Added for GNIRS DC
 *
 *
 */

#ifndef PARAMETER_H
#define PARAMETER_H

#include <vxworks.h>
#include <hash_map.h>
#include <string>

// put definitions here 
const int MAXSTRLEN = 2048;

typedef enum ValidityType ValidityType;
enum validityType {
  VALIDITY_RANGE = 0,
  VALIDITY_SET = 1
};

struct ParameterData {
  string name;
  short dType;
  ValidityType vType;
  string dbName;
  void *addr;

  // Value - one of
  long lValue;
  double dValue;
  string sValue;

  // validity range or validity set
  int nValid; 
  long *lValid;
  double *dValid;
  string *sValid;
};

// DC parameters - these are set by DC processes
class Parameter {
 private:
  hash_map<const string, ParameterData> paramDict;

 public:

  // Constructor
  Parameter();

  // Reset method - resets status pars to default
  reset();

  // Parameter add methods - overload to support 3 gemini types
  void add(const char* param, const char* dbName, const char* defValue, ValidityType vType,
	   int n, string *sValid);
  void add(const char* param, const char* dbName, long defValue, ValidityType vType,
	   int n, long *lValid);
  void add(const char* param, const char* dbName, double defValue, ValidityType vType,
	   int n, double *dValid);

  // Parameter set methods - overloaded to support 3 Gemini types
  void set(const char* param, const char* value);
  void set(const char* param, long value);
  void set(const char* param, double value);

  // Parameter get methods - overloaded to support 3 Gemini types
  string get(const char* param);
  long get(const char* param);
  double get(const char* param);

  // Clear method - use to clear param flag once read
  void clear(const char* param);

  // Query data type method - use to find out data type of parameter
  short dType(const char* param);
  
  // Destructor
  ~Parameter();
};

#endif /* PARAMETER_H */
