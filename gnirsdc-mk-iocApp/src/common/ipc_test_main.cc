/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  ipc_test_main.cc
 * 
 * GENERAL DESCRIPTION
 *  Main program to test IPC C++ classes.
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 1993
 *
 * HISTORY
 *   Sept 1993 PJY    Created for RSAA CICADA package
 *   Sept 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */
/* Include files */

#ifdef vxWorks
#include <vxWorks.h>
#include <sysLib.h>
#endif
#include <time.h>
#include "ipc.h"
#include "test_ipc.h"

/* defines */


/* typedefs */

/* class declarations */

/* global variables */
Mailbox *mbox1; 
Mailbox *mbox2; 
int ready;

/* local function declarations */

/* local function definitions */

/* class function definitions */

/* Main program */
#ifdef vxWorks
int ipcMain()
#else
int main()
#endif
{
  try {
    Test_Smem* sm;
    int mwait;
    struct timespec ts;
    int result;
    time_t start_time, end_time;
    
    try {

      cout << "Master: starting..." << endl;

      // Establish mailbox for passing messages
      mbox1 = new Mailbox("mbox1",ESTABLISH,S_IRUSR|S_IWUSR); 
      mbox2 = new Mailbox("mbox2",ESTABLISH,S_IRUSR|S_IWUSR); 
      
      //Establish shared mem
      Shared_Mem smem("smem1", ESTABLISH, sizeof(Test_Smem),S_IRUSR|S_IWUSR);
      sm = (Test_Smem *) smem.address;


      //Establish a semaphore for controlling shared memory
      Semaphore sem1("sem1",ESTABLISH,RWALL);

      cout << "Master: Starting slave ..." << endl;

      int slave;
      if ((slave = taskSpawn("ipcSlave", 100, VX_FP_TASK, 4000, (FUNCPTR) ipcSlave, 
				 0,0,0,0,0,0,0,0,0,0)) == ERROR) {
	throw Error("taskSpawn of ipcSlave failed!", E_ERROR, errno, __FILE__, __LINE__);
      }    

      ts.tv_sec = 1;
      ts.tv_nsec = 0;
      nanosleep(&ts,NULL);

      /* Do some communicating */
      sm->a = 22;
      sm->b = 1.1;
      mwait = 500;
      

      sem1.set();  // Post indicating ready

      Semaphore sem2("sem2",CONNECT,RWALL);

      cout << "Master: Connected sem2 - waiting on sem2 ..." << endl;
      sem2.wait(); // Wait for semaphore to be 0 again

      cout << "Master: sem2 - taken, waiting again with timeout..." << endl;

      start_time = time(NULL);

      result = sem2.timed_wait(mwait);

      end_time = time(NULL);

      // Check timed_wait semaphore method
      if (result) {
	cout << "Master: got sem2 before timeout" << endl;
      } else {
	cout << "Master: didn't get sem2 before timeout" << endl;
      }
      
      cout << "Timeout took " << (end_time - start_time) << endl;

      cout << "Master: a=" << sm->a << " b=" << sm->b << endl;

      // Now use mailbox for sending info.
      ready = FALSE;
      mbox1->send_message((char *) &ready, sizeof(ready));

      do {
	rest(50);
	mbox2->get_message((char *)&ready, sizeof(ready), IPC_NOWAIT);
      } while (!ready);

      cout << "Master: Got ready message from slave, ready = " << ready << endl;
      

      sem2.set();  // Set semaphore 0 to 1 indicating ready
      sem1.wait(); // Wait for semaphore to be 0 again
      sem1.set();  // Set semaphore to 0 indicating finished

      sem1.clear(); 
      sem2.clear(); 

      ts.tv_sec = 3;
      ts.tv_nsec = 0;
      nanosleep(&ts,NULL);


      taskDelete(slave);

      cout << "Master: Running Thread class test..." << endl;
      threadTest();

      cout << "Master: Running Timer class test..." << endl;
      timerTest();

      cout << "Master: exiting" << endl;
    }

    catch (IPC_Error& e) {
      cout << "Master Error: " << endl;
      e.print_error(__FILE__, __LINE__);
    }
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return(0);
}
