/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * CICADA PROJECT
 *
 * FILENAME parameterDB.h
 * 
 * PURPOSE
 * Definitions for access to a generic database from simple class interface
 * This is the base class that should be used by specific database constructs
 * such as access to EPICS DB.
 *
 * REQUIREMENTS
 * Requires use of STL class "string"
 *
 */

#ifndef PARAMETER_DB_H
#define PARAMETER_DB_H

#include <string>

using namespace std;

// put definitions here 

/*-------------------------------------------------------------------------
CLASS
    ParameterDB
    Base parameterDB class
DESCRIPTION
    Base parameterDB class - this is basis for parameter DBs where
    mapping between a parameter name and actual storage is maintained.

    The ParameterDB class provides access to a parameter database from
    simple class interface.  It stores status and parameter values in
    shared memory and allows changes to their values to be propagated
    to other platforms needing access to the parameters, so that changes 
    made by a process will be available to other processes running remotely,
    and vice versa.

KEYWORDS
    ParameterDB

-------------------------------------------------------------------------*/

// A callback class that should be inherited from for any user defined callback
class Parameter_Callback {
  public:
    virtual void operator()() = 0;
};

class ParameterDB {
 public:

  //+ Constructor
  ParameterDB() {};

  //+ Destructor
  virtual ~ParameterDB() {};

  // GROUP:   Member functions
  //+ set parameter address from name - must be overridden by derived class
  // > dbName - name of database variable
  // > param - common name of parameter
  // > prefix - database prefix
  // > suffix - database suffix
  virtual void* setAddr(const char* dbName, const char* param, const char* prefix, const char* suffix) = 0;

  // ParameterDB put methods, overloaded to support multiple data types
  // > paramDBName - name of parameter db that has called this put
  // > dbName - name of parameter in backend database
  // > param - common name of parameter - may be same as dbName
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local addr - used for memcpy
  // > value - value of parameter to set
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to put - default 1
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const string* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const char* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr,
		   const unsigned char* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const short* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const unsigned short* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const int* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const unsigned int* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const long* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const unsigned long* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const float* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const double* value, unsigned int offset, unsigned int n) = 0;
  virtual void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   const bool* value, unsigned int offset, unsigned int n) = 0;

  // ParameterDB get methods, overloaded to support multiple data types
  // > paramDBName - name of parameter db that has called this put
  // > dbName - name of parameter in backend database
  // > param - common name of parameter - may be same as dbName
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local addr - used for memcpy
  // > value - value of parameter to set
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to put - default 1
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   string* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   char* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   unsigned char* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   short* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   unsigned short* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   int* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   unsigned int* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   long* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   unsigned long* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   float* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   double* value, unsigned int offset, unsigned int n) = 0;
  virtual void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
		   bool* value, unsigned int offset, unsigned int n) = 0;

  //+ Free address in use
  // > addr - pointer to DB addr structure to free
  virtual void freeAddr(const void* addr) = 0;

  //+ Request to publish a parameter
  //+ This is called from the Parameter class add method
  // > addr - pointer to DB addr structure 
  virtual void publish(const void* addr) = 0;

  //+ Request to attach the local and database addresses
  //+ This is called from the Parameter class add method
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local address
  virtual void attach(const void* addr, const void* localAddr) = 0;

  //+ Add a callback on a parameter
  // > addr - pointer to DB addr structure 
  // > callback - pointer ro callback function
  virtual int add_callback(const void* addr, Parameter_Callback* callback) = 0;

  //+ Remove the callback with the given cb_id
  // > cb_id - callback identifier
  virtual void remove_callback(int cb_id) = 0;

  //+ Wait for a parameter to change
  // > addr - pointer to DB addr structure 
  virtual void wait(const void* addr) = 0;

};

#endif /* PARAMETER_DB_H */
