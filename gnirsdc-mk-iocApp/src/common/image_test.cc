/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  image_test.cc
 * 
 * GENERAL DESCRIPTION
 *  Test routine for Image class
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 1996
 *
 * HISTORY
 *   Aug  1996 PJY    Created for RSAA CICADA package
 *   Sept 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "fits.h"
#include "image.h"
#include "exception.h"

#ifdef vxWorks
int imageTest()
#else
int main()
#endif
{
  try {
    Fitsname name;
    Image *img;
    double low,high;
    Image_Pixel P;
    float exp,rate;

    // Test algorithm
    // 1. Read an existing FITS file
    // 2. Create a new image instance
    // 3. Output min,max,std_dev and mean for image
    try {
      cout << "Enter name of FITS file: ";
      cin >> name;
      
      // Open Fits file and create an image
      img = new Image(name,TRUE);

      // Calc stats
      img->std_dev();

      cout << "Min:" << img->min << " Max:" << img->max << " Mean:" << img->mean 
	   << " Std dev:" << img->sdev << " Mode:" << img->mode <<endl;

      img->hopt_range(low,high);
      cout << "Sampling=10%,Cutoff low=" << low << " high=" << high << " median=" << img->median << endl;

      // Simulate some cosmic rays
      cout << "Enter exposure time for cosmic ray detection (seconds):";
      cin >> exp;
      cout << "Enter cosmic ray rate (rays/s/cm/cm):";
      cin >> rate;
      switch (img->bitpix) {
      case SHORT_BITPIX:
	if (img->unsigned_data)
	  cosmic(img->upix, img->nx, img->ny, exp, 1.0, rate, true, P);
	else 
	  cosmic(img->spix, img->nx, img->ny, exp, 1.0, rate, true, P);
	break;
      default:
	cout << "Sorry - this test program only handles 16 bit images..." << endl;
      }

      // Calc stats again
      img->std_dev();

      cout << "Min:" << img->min << " Max:" << img->max << " Mean:" << img->mean 
	   << " Std dev:" << img->sdev << " Mode:" << img->mode <<endl;
      img->hopt_range(low,high);
      cout << "Sampling=10%,Cutoff low=" << low << 
	" high=" << high << " median=" << img->median << endl;
    }
    catch (Error& e) {
      e.print_error(__FILE__,__LINE__);
    }
      
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return(0);
}
