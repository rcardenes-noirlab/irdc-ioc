[schematic2]
uniq 19
[tools]
[detail]
w 2832 1435 100 0 n#1 elongins.viewID.VAL 2816 1424 2896 1424 ecars.viewC.ICID
w 3656 1851 100 0 n#2 embbos.obsLock.OUT 3584 1840 3776 1840 hwout.hwout#57.outp
w 3616 2155 100 0 n#3 elongins.observeID.VAL 3600 2144 3680 2144 ecars.observeC.ICID
w 1968 2219 100 0 n#4 eapply.stateApply.OCLC 1904 2208 2080 2208 2080 1888 2224 1888 dcStopCad.dcStopCad#6.CLID
w 2000 2283 100 0 n#5 eapply.stateApply.OCLB 1904 2272 2144 2272 2144 2112 2224 2112 dcAbortCad.dcAbortCad#3.CLID
w 2040 2347 100 0 n#6 eapply.stateApply.OCLA 1904 2336 2224 2336 dcObserveCad.dcObserveCad#2.CLID
w 1392 2555 -100 0 CLID inhier.CLID.P 1376 2544 1456 2544 1456 2432 1520 2432 eapply.stateApply.CLID
w 1824 2907 100 0 n#7 dcStopCad.dcStopCad#6.MESS 2384 1888 2576 1888 2576 2896 1120 2896 1120 2208 1520 2208 eapply.stateApply.INMC
w 1824 2875 100 0 n#8 dcStopCad.dcStopCad#6.VAL 2384 1920 2544 1920 2544 2864 1152 2864 1152 2240 1520 2240 eapply.stateApply.INPC
w 1830 2827 100 0 n#9 dcAbortCad.dcAbortCad#3.MESS 2384 2112 2512 2112 2512 2816 1196 2816 1196 2272 1520 2272 eapply.stateApply.INMB
w 1830 2795 100 0 n#10 dcAbortCad.dcAbortCad#3.VAL 2384 2144 2480 2144 2480 2784 1228 2784 1228 2304 1520 2304 eapply.stateApply.INPB
w 1830 2763 100 0 n#11 dcObserveCad.dcObserveCad#2.MESS 2384 2336 2448 2336 2448 2752 1260 2752 1260 2336 1520 2336 eapply.stateApply.INMA
w 1830 2731 100 0 n#12 eapply.stateApply.INPA 1520 2368 1292 2368 1292 2720 2416 2720 2416 2364 2384 2364 2384 2368 dcObserveCad.dcObserveCad#2.VAL
w 1480 2555 100 0 n#13 inhier.DIR.P 1416 2656 1484 2656 1484 2464 1520 2464 eapply.stateApply.DIR
w 1974 2467 100 0 n#14 eapply.stateApply.VAL 1904 2464 2092 2464 2092 2592 2192 2592 outhier.VAL.p
w 1990 2435 100 0 n#15 eapply.stateApply.MESS 1904 2432 2124 2432 2124 2528 2192 2528 outhier.MESS.p
w 1984 2251 100 0 n#16 eapply.stateApply.OUTC 1904 2240 2112 2240 2112 1920 2224 1920 dcStopCad.dcStopCad#6.DIR
w 2022 2315 100 0 n#17 eapply.stateApply.OUTB 1904 2304 2176 2304 2176 2144 2224 2144 dcAbortCad.dcAbortCad#3.DIR
w 2046 2379 100 0 n#18 eapply.stateApply.OUTA 1904 2368 2224 2368 dcObserveCad.dcObserveCad#2.DIR
s 1152 1200 200 0 Top level system command APPLY records. Gemini
s 1152 1136 200 0 OCS systems commands OBSERVE, ABORT and STOP are
s 1152 1072 200 0 sequenced here. Commands not marked are ignored
s 3408 1648 100 0 This record is set by the SNL code. It sets the
s 3408 1616 100 0 Obs interlock in the CC database.
[cell use]
use elongins 2624 1520 100 0 viewID
xform 0 2688 1440
use ecars 2976 1520 100 0 viewC
xform 0 3056 1344
p 3184 1206 100 0 -1 def(FLNK):$(top)combCars11.PROC
use embbos 3392 1920 100 0 obsLock
xform 0 3456 1840
p 3072 1646 100 0 0 OMSL:supervisory
p 3392 1712 100 0 1 ONST:LOCKED
p 3392 1744 100 0 1 ZRST:UNLOCKED
p 3584 1840 75 768 -1 pproc(OUT):NPP
use hwout 3776 1799 100 0 hwout#57
xform 0 3872 1840
p 3872 1831 100 0 -1 val(outp):$(engtop)lockObsWr PP NMS
use elongins 3408 2240 100 0 observeID
xform 0 3472 2160
use ecars 3760 2240 100 0 observeC
xform 0 3840 2064
p 3648 1792 100 0 0 def(FLNK):$(top)combCars11.PROC
use inhier 1360 2503 100 0 CLID
xform 0 1376 2544
use inhier 1400 2615 100 0 DIR
xform 0 1416 2656
use outhier 2160 2551 100 0 VAL
xform 0 2176 2592
use outhier 2160 2487 100 0 MESS
xform 0 2176 2528
use dcStopCad 2228 1767 100 0 dcStopCad#6
xform 0 2304 1872
use dcAbortCad 2224 1991 100 0 dcAbortCad#3
xform 0 2304 2096
use dcObserveCad 2224 2215 100 0 dcObserveCad#2
xform 0 2304 2320
use eapply 1596 2540 150 0 stateApply
xform 0 1712 2192
use eborderC 896 623 100 0 eborderC#0
xform 0 2576 1928
p 3472 776 100 768 -1 author:Peter Young
p 3472 912 40 0 -1 comment1:1997-10-01 GMOS version. Ken Ramey
p 3472 892 40 0 -1 comment2:1999-01-22 Added seq and CAR records J. Tvedt
p 3472 872 40 0 -1 comment3:2000-11-02 Modified for GNIRS. Peter Young
p 3472 744 100 768 -1 date:2000-11-02
p 3696 824 200 768 -1 file:dcStateApply.sch
p 3968 776 100 0 -1 page:1
p 4080 776 100 0 -1 pages:1
p 3744 776 100 0 -1 revision:gnirs v1.0
p 3696 888 150 768 -1 system:Gemini GNIRS DC
[comments]
