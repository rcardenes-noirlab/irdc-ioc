/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *   fits.h
 * 
 * PURPOSE
 *   Stats function definitions
 *
 * HISTORY
 *   July 2000 PJY    Created for RSAA CICADA package
 *   June 2002 PJY    Ported to VxWorks/GNIRS from RSAA CICADA package
 */

#ifndef _STATS_H
#define _STATS_H

//include files
#include <math.h>
#include "exception.h"
#include "utility.h"

// Produce a random number using stdlib functions between 0 and 1.
#include <stdlib.h>
#define RAND ((double)rand()/RAND_MAX)
#define M_PI 3.14159265358979323846

/* put definitions here */

// Template function for gettting stats of a sampled image
template<class T> void calc_stats(T *pix, int npix, int bitpix, double& sigma, double& median, 
				   double& mean, double& min, double& max, double& mode, double& zmin, 
				   double& zmax, double& zslope, float* sample=NULL, int sample_pix=1000,
				   float contrast=0.5, int use_median=1, int *freq=NULL, int nfreq=0);

/*
 *+ 
 * FUNCTION NAME: calc_stats
 * 
 * INVOCATION: calc_stats(char* image_buf, int npix, int bitpix, double& std_dev, double& median, 
 *                         double& mean, double& min, double& max, double& mode, double& zmin, 
 *                         double& zmax, double& zslope, float* sample, int sample_pix, float contrast, 
 *                         int use_median) 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > image_buf - ptr to image data
 * > npix - number of pixels in image
 * > bitpix - number of bits per pixel
 * < std_dev - calculated standard deviation of sample
 * < median - calculated median of sample
 * < mode - calculated mode of sample
 * < min - calculated min of sample
 * < max - calculated max of sample
 * < zmin - calculated min of sample for scaling
 * < zmax - calculated max of sample for scaling
 * < zslope - calculated slope of fitted line through sorted sample
 * < sample - pointer to sorted sample array, if null then local array allocated
 * > sample_pix - number of pixels to use in sample
 * > contrast -  adjustment to slope of transfer function
 * > use_median - use median for computing zmin/zmax, otherwise mode
 * > freq - pointer to frequency histogram array - set to NULL if not required
 * > nfreq - number of elements in frequency histogram
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: 
 *  Sample data and compute data statisitics. Also compute suitable zscale values
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
// Template function prototypes for the compiler
/*  template void calc_stats<unsigned char>(unsigned char*,int,int,double&,double&,double&,double&,double&,double&,double&,  */
/*  					double&,double&,float*,int,float,int,int*,int); */
/*  template void calc_stats<char>(char*,int,int,double&,double&,double&,double&,double&,double&,double&,  */
/*  			       double&,double&,float*,int,float,int,int*,int); */
/*  template void calc_stats<short>(short*,int,int,double&,double&,double&,double&,double&,double&,double&,  */
/*  				double&,double&,float*,int,float,int,int*,int); */
/*  template void calc_stats<unsigned short>(unsigned short*,int,int,double&,double&,double&,double&,double&,double&,double&,  */
/*  					 double&,double&,float*,int,float,int,int*,int); */
/*  template void calc_stats<int>(int*,int,int,double&,double&,double&,double&,double&,double&,double&,  */
/*  			      double&,double&,float*,int,float,int,int*,int); */
/*  template void calc_stats<unsigned int>(unsigned int*,int,int,double&,double&,double&,double&,double&,double&,double&,  */
/*  				       double&,double&,float*,int,float,int,int*,int); */
/*  template void calc_stats<float>(float*,int,int,double&,double&,double&,double&,double&,double&,double&,  */
/*  				double&,double&,float*,int,float,int,int*,int); */
/*  template void calc_stats<double>(double*,int,int,double&,double&,double&,double&,double&,double&,double&,  */
/*  				 double&,double&,float*,int,float,int,int*,int); */

template<class T> void calc_stats(T *pix, int npix, int bitpix, double& sigma, double& median,
				  double& mean, double& min, double& max, double& mode, double& zmin, 
				  double& zmax, double& zslope, float* sample, int sample_pix, 
				  float contrast, int use_median, int *freq, int nfreq)
{
  double syy = 0, sy = 0, siy = 0, sxx = 0, sx = 0;
  double centre,nd,ksigma,dn;
  float *data,*data2;
  float cm,si,sd=0;
  float step=1.0;
  int n,i,j,k,cc,mc,nn;
  double dmax = RAND_MAX;
  double scale=1,range;
  
  if (npix == 0) {
    throw Error("Number of pixels to sample must be >0!",E_ERROR,-1,__FILE__,
		__LINE__);
  }

  // Set sample size
  n = (sample_pix>npix)? npix:sample_pix;

  // Adjust sample pix to ensure at least 2 pixels sampled
  if (n<2) 
    n = 2;  

  // Seed random number generator
  srand(n);

  // Allocate space for sample, need sample_pix pixels.
  // NB using new float[] caused occasional segv crashes!
  if (sample == NULL) 
    data = (float*) malloc(n*sizeof(float));
  else 
    data = sample;
  
  // Sample the data 
  min = MAXD;
  max = MIND;
  mode = MAXD;
  if (n<npix) {
    step = float(npix)/n;
    sd = (step-1)/dmax;
    i = 0;
  } else i = -1;

  for (j=0; j<n; j++) {
    if (n<npix) {
      // increment step a random amount
      si = sd*rand();
      k = (int) (j*step); // non random step position
      i += (int) si; // random step position
      // if random step falls more than step behind 
      // catch it up.
      if ((i+step) < k) 
	i = k; 
    } else 
      i++;
    data[j] = pix[i];
    sy += data[j];
  }

  // Initial mean
  mean = sy/n;

  // Second loop to calculate variance
  for (j=0; j<n; j++) {
    dn = data[j]-mean;
    syy += dn*dn;
  }  
   
  // Calculate standard deviation, median, mode, mean, min and max
  nd = n;
  sigma = sqrt(syy/(nd - 1));
  
  // Use quicksort to sort sample
  qsort(n,data);

  // Initial median value
  if ((n/2)*2 == n) median = (data[n/2] + data[n/2-1])/2.0;
  else median = data[n/2];

  // Data sample real min and max
  min = data[0];
  max = data[n-1];

  // Build up frequency histogram if required
  if ((freq!=NULL) && (nfreq>0)) {
    range = MAX(1,max-min);
    // Zero the frequency array
    memset((void*)freq, 0, nfreq*sizeof(int));
    // Calculate the scaling factor - based on data type
    switch (bitpix) {
    case BYTE_BITPIX: 
    case SHORT_BITPIX:
    case INT_BITPIX:
      if (range<=nfreq) {
	scale = 1.0;
	nfreq = nint(range);
      } else
	scale = nfreq/range;
      break;
    case FLOAT_BITPIX:
    case DOUBLE_BITPIX:
      scale = nfreq/range;
      break;
    }    
  }

  // Calculate mode value - use sorted sample
  // Reject pixels more than 2sigma from mean
  cc = mc = nn = 0;
  mode = cm = data[0];
  ksigma = 2*sigma;
  data2 = (float*) malloc(n*sizeof(float));
  for (j=0; j<n; j++) {
    if (fabs(mean-data[j])>ksigma) {
      sy -= data[j];
    } else {
      data2[nn] = data[j];
      if (data[j] == cm) cc++;
      else {
	if (cc>mc) {
	  mc = cc; 
	  mode = cm;
	}
	cm = data[j];
	cc = 1;
      }
      nn++;
      siy += nn*data[j];
    }
    // Add to frequency histogram
    if ((freq!=NULL) && (nfreq>0)) {
      freq[nint(scale*(data[j]-min))]++; 
    }
  }

  // Calculate slope of line fitted through sorted pixels
  nd = nn;
  sx = nd*(nd+1)/2.0;
  sxx = nd*(2*nd+1)*(nd+1)/6.0;
  zslope = (nd*siy-sx*sy)/(nd*sxx-sx*sx);

  // Calc median value
  if ((nn/2)*2 == nn) median = (data2[nn/2] + data2[nn/2-1])/2.0;
  else median = data2[nn/2];

  // Use IRAFs zscale type algorithm for getting zmin/zmax around central
  // value. We don't use pixel rejection and use more efficient algorithms.
  // NB IRAF zscale sampling is not very random - ours fares a bit better  
  if (contrast > 0)
    zslope = zslope / contrast;
  centre = (use_median)? median:mode;
  zmin = centre - nn/2 * zslope;
  zmin = (zmin<min)? min:zmin;
  zmax = centre + nn/2 * zslope;
  zmax = (zmax>max)? max:zmax;

  // mean
  mean = sy/nn;

  // only free sample array if locally allocated
  if (sample == NULL) {
    if (data) 
      free(data);
  }
  if (data2) 
    free(data2);

}


// Computes Poisson distributed random deviates
float poidev(float xm);

//returns a number picked randomly with a gaussian probability distribution
double gauss_rand( double mean, double std_dev );

#endif  // _STATS_H
