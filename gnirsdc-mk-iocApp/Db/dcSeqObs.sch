[schematic2]
uniq 18
[tools]
[detail]
w 1040 1075 100 0 n#1 ecad20.seqObs.OUTK 944 1072 1136 1072 hwout.hwout#398.outp
w 1040 1139 100 0 n#2 ecad20.seqObs.OUTJ 944 1136 1136 1136 hwout.hwout#440.outp
w 1040 1203 100 0 n#3 ecad20.seqObs.OUTI 944 1200 1136 1200 hwout.hwout#437.outp
w 1040 1267 100 0 n#4 ecad20.seqObs.OUTH 944 1264 1136 1264 hwout.hwout#408.outp
w 984 1843 100 0 n#5 ecad20.seqObs.OCID 944 1840 1024 1840 1024 336 1168 336 cadCar.secObsCC.ICID
w 1016 1939 100 0 n#6 ecad20.seqObs.VAL 944 1936 1088 1936 1088 2064 1280 2064 outhier.VAL.p
w 1059 1152 100 0 n#6 junction 1056 1936 1056 368 1168 368 cadCar.secObsCC.STAT
w 1056 307 100 0 n#7 ecad20.seqObs.STLK 944 304 1168 304 cadCar.secObsCC.SLNK
w 64 1843 100 0 n#8 embbos.obs_readMode.OUT -320 1840 448 1840 448 1744 624 1744 ecad20.seqObs.A
w 1168 1907 100 0 MESS ecad20.seqObs.MESS 944 1904 1392 1904 outhier.MESS.p
w 227 2056 100 0 DIR inhier.DIR.P 224 2064 224 2048 432 2048 432 1936 624 1936 ecad20.seqObs.DIR
w 464 1907 100 0 CLID inhier.CLID.P 304 1904 624 1904 ecad20.seqObs.ICID
w 1000 1331 100 0 n#9 ecad20.seqObs.OUTG 944 1328 1056 1328 hwout.hwout#350.outp
w 1000 1395 100 0 n#10 ecad20.seqObs.OUTF 944 1392 1056 1392 hwout.hwout#348.outp
w 1000 1459 100 0 n#11 ecad20.seqObs.OUTE 944 1456 1056 1456 hwout.hwout#344.outp
w 1000 1587 100 0 n#12 ecad20.seqObs.OUTC 944 1584 1056 1584 hwout.hwout#340.outp
w 1000 1715 100 0 n#13 ecad20.seqObs.OUTA 944 1712 1056 1712 hwout.hwout#406.outp
w 252 1635 100 0 n#14 estringouts.obs_exposed.OUT 232 1632 272 1632 272 1296 624 1296 ecad20.seqObs.H
w 104 1259 100 0 n#15 embbos.obs_quadrants.OUT -16 1256 224 1256 224 1040 624 1040 ecad20.seqObs.L
w 954 1010 -100 0 n#16 ecad20.seqObs.OUTL 944 1008 1136 1008 hwout.hwout#442.outp
w 1000 1523 100 0 n#17 ecad20.seqObs.OUTD 944 1520 1056 1520 hwout.hwout#342.outp
w 1000 1651 100 0 n#6 ecad20.seqObs.OUTB 944 1648 1056 1648 junction
s 1680 1632 100 0 happens when the Observe CAD is processed.
s 1680 1664 100 0 when this CAD is processed. An operation only
s 1680 1696 100 0 Unlike the view mode, a sequence is not triggered
s -640 -128 200 0 They are stored in the gnirs_dc_par class object.
s -640 -64 200 0 Up to 20 observation parameters can be set here
s -640 16 200 0 Obs mode sequence parameters Apply/CAD record
[cell use]
use cadCar 1168 215 100 0 secObsCC
xform 0 1248 320
p 1188 188 100 0 1 set:cad seqObs
use hwout 1136 1095 100 0 hwout#440
xform 0 1232 1136
p 1232 1127 100 0 -1 val(outp):$(sadtop)obs_readIntvl PP NMS
use hwout 1136 1159 100 0 hwout#437
xform 0 1232 1200
p 1232 1191 100 0 -1 val(outp):$(sadtop)obs_readTime PP NMS
use hwout 1056 1671 100 0 hwout#406
xform 0 1152 1712
p 1152 1703 100 0 -1 val(outp):$(sadtop)obs_readModeSet PP NMS
use hwout 1136 1031 100 0 hwout#398
xform 0 1232 1072
p 1232 1063 100 0 -1 val(outp):$(sadtop)obs_timeModeSet PP NMS
use hwout 1056 1287 100 0 hwout#350
xform 0 1152 1328
p 1152 1319 100 0 -1 val(outp):$(sadtop)obs_ncoadds PP NMS
use hwout 1056 1351 100 0 hwout#348
xform 0 1152 1392
p 1152 1383 100 0 -1 val(outp):$(sadtop)obs_nperiods PP NMS
use hwout 1056 1415 100 0 hwout#344
xform 0 1152 1456
p 1152 1447 100 0 -1 val(outp):$(sadtop)obs_period PP NMS
use hwout 1056 1543 100 0 hwout#340
xform 0 1152 1584
p 1152 1575 100 0 -1 val(outp):$(sadtop)obs_resetDelay PP NMS
use hwout 1136 1223 100 0 hwout#408
xform 0 1232 1264
p 1232 1255 100 0 -1 val(outp):$(sadtop)obs_exposedRQ PP NMS
use estringouts -24 1575 100 0 obs_exposed
xform 0 104 1648
use embbos -544 1936 100 0 obs_readMode
xform 0 -448 1840
p -656 1696 100 0 1 ONST:BRIGHT
p -656 1632 100 0 1 THST:VERY_FAINT
p -656 1664 100 0 1 TWST:FAINT
p -656 1728 100 0 1 ZRST:VERY_BRIGHT
use inhier 208 2023 100 0 DIR
xform 0 224 2064
use inhier 288 1863 100 0 CLID
xform 0 304 1904
use ecars 1872 752 150 0 seqObsC
xform 0 1968 592
p 1792 384 100 0 1 def(FLNK):$(top)combCars12.PROC
use ecad20 704 1984 150 0 seqObs
xform 0 784 1104
p 768 1312 100 0 0 CTYP:12
p 720 1680 100 0 1 FTVA:LONG
p 720 1648 100 0 1 FTVB:LONG
p 720 1616 100 0 1 FTVC:DOUBLE
p 720 1584 100 0 1 FTVD:LONG
p 720 1552 100 0 1 FTVE:DOUBLE
p 720 1520 100 0 1 FTVF:LONG
p 720 1488 100 0 1 FTVG:LONG
p 720 1456 100 0 1 FTVH:DOUBLE
p 720 1424 100 0 1 FTVI:DOUBLE
p 720 1392 100 0 1 FTVJ:DOUBLE
p 720 1360 100 0 1 FTVK:LONG
p 720 1330 100 0 1 FTVL:LONG
p 720 1296 100 0 0 FTVM:LONG
p 720 1264 100 0 0 FTVN:LONG
p 720 1040 100 0 0 INAM:dcCADseqInit
p 720 204 100 0 1 SNAM:dcCADseq
p 464 112 100 0 0 def(INPA):0
p 464 80 100 0 0 def(INPB):0
p 464 48 100 0 0 def(INPC):0.0
p 464 16 100 0 0 def(INPD):1
p 464 -16 100 0 0 def(INPE):5
p 464 -48 100 0 0 def(INPF):1
p 464 -80 100 0 0 def(INPG):1
p 464 -112 100 0 0 def(INPH):0
p 464 -144 100 0 0 def(INPI):1
p 464 -176 100 0 0 def(INPJ):5
p 464 -208 100 0 0 def(INPK):5
p 464 -240 100 0 0 def(INPL):0
p 464 -272 100 0 0 def(INPM):0
p 1216 1712 75 768 -1 pproc(OUTA):NPP
use outhier 1248 2023 100 0 VAL
xform 0 1264 2064
use outhier 1360 1863 100 0 MESS
xform 0 1376 1904
use eborderC -784 -313 100 0 eborderC#87
xform 0 896 992
p 1792 -160 100 768 -1 author:Peter Young
p 1792 -20 40 0 -1 comment1:2001-02-02 Modified for GNIRS. Peter Young
p 1792 -192 100 768 -1 date:2000-11-02
p 2016 -112 200 768 -1 file:dcSeqObs.sch
p 2288 -160 100 0 -1 page:1
p 2400 -160 100 0 -1 pages:1
p 2064 -160 100 0 -1 revision:0
p 2016 -48 150 768 -1 system:Gemini GNIRS DC
use embbos -251 1164 100 0 obs_quadrants
xform 0 -144 1256
p -475 1146 100 0 1 ONST:1_QUADRANT
p -476 1172 100 0 1 ZRST:FULL_FRAME
use hwout 1136 992 100 0 hwout#442
xform 0 1232 1008
p 1232 999 100 0 -1 val(outp):$(sadtop)obs_quadrantsSet PP NMS
use hwout 1056 1479 100 0 hwout#342
xform 0 1152 1520
p 1152 1511 100 0 -1 val(outp):$(sadtop)obs_nfowler PP NMS
use hwout 1056 1607 100 0 hwout#346
xform 0 1152 1648
p 1152 1639 100 0 -1 val(outp):$(sadtop)obs_nresets PP NMS
[comments]
