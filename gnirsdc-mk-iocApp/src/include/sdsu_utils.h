/*
 * Copyright (c) 1994 - 2002 by MSSSO Computing Section 
 *
 * CICADA PROJECT
 *
 * FILENAME sdsu_utils.h
 * 
 * PURPOSE
 * SDSU controller utility functions
 *
 */
#ifndef _SDSU_UTILS_H
#define _SDSU_UTILS_H

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#include <stdlib.h>
#include <ctype.h>
#include <string>
#include "configuration.h"

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/
#define SDSU_DIR "cnf/sdsu/"          // Default sdsu configuration directory
#define SDSU_SETUP "sdsu.setup"
#define SDSU_PAR_SETUP "parameter.asm"
#define SDSU_TIMING_FILE "timing.lod"
#define SDSU_UTILITY_FILE "utility.lod"

/*  #ifndef TRUE */
/*  #define	TRUE		         1 */
/*  #endif */
/*  #ifndef FALSE */
/*  #define	FALSE		         0 */
/*  #endif */


/*--------------------------------------------------------------------------
 Class declarations 
 -------------------------------------------------------------------------*/

// Function definitions
//+ compute the full sdsu config file pathname
// > name - name of the configuration
// > chip_name - name of the chip used
std::string sdsu_path(const char* name, const char* chip_name);

//+ Calculate an ADU value from a voltage
// > vd  - a volts description struct from which the adu value will be calculated 
// > i - whether to do the calc for the low or high voltage
// > v - voltage to be converted to ADUs
int calc_adu_val(Volts_Desc vd, Sdsu_Voltage_Index i, double v);

//+ calculate a voltage value
// > vd  - a volts description struct from which the adu value
// will be calculated 
// > i - whether to do the calc for the low or high voltage
// > adu_val - ADU value to be converted to a voltage
double calc_volt_val(Volts_Desc vd, Sdsu_Voltage_Index i, int adu_val);

#endif // _SDSU_UTILS_H
