/*
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME dummy_controller.cc
 *
 * GENERAL DESCRIPTION
 *
 *  Implements the dummy controller class to simulate a detector
 * controller. This is a derived class of the Controller class and provides
 * readout data from either a MEF FITS file (each extension represents one
 * readout amplifier and is assumed the same size) or from generated data.
 *
 */

/* Include files */
#include <math.h>
#include <stdlib.h>
#include "stats.h"
#include "parameter.h"
#include "image.h"
#include "common.h"
#include "camera.h"
#include "config_table.h"
#include "dummy_controller.h"
#include "dummy_setup.h"
#include "epicsThread.h"

using std::shared_ptr;

/* defines */

/* typedefs */

/* class declarations */

/* global variables */
static double tt1,tt2,tt3,tt4;

const std::string Dummy_Controller::PARAMETER_NAME = "DummyPar";

/* local function declarations */

// Copy a pixel from an image with binning - handle data types T
template<class T> void copy_pixel(T* inp, T& outp, T* cp, T max, int rcf, int ccf, int xo, int yo, int width,
				  int height, int full_width, float exptime_scale);

/* local function definitions */

/* global function definitions */

/*============================================================================
 *  Cicada Dummy Hardware class Definition
 *===========================================================================*/
/*
 *+
 * FUNCTION NAME: Dummy_Controller::Dummy_Controller
 *
 * INVOCATION: dummy = new Dummy_Controller(cam,c,st)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *           > cam - Camera_Desc - description of the camera
 *           > camid - camera id
 *           > c - controller id
 * FUNCTION VALUE: Void
 *
 * PURPOSE: Basic constructor for the dummy Controller
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Dummy_Controller::Dummy_Controller(Camera_Desc& cam, int camid, int c, Platform_Specific* ps)
  :Controller(cam, camid, c, ps)
{
  Dummy_Controller::init_vars();
}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::Dummy_Controller
 *
 * INVOCATION: dummy_controller = new
 *   Dummy_Controller(Camera_Desc& cam, int camid, int c,
 *   const char* param_host, Instrument_Status* st,
 *   ParamMapType *pm)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam 	- Camera_Desc - description of the camera
 * > camid 	- camera id
 * > c 		- controller id
 * > param_host	- host on which the parameter database resides
 * > st		- status memory for this instrument
 * > pm		- parameter map
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Middle constructor for the Dummy controller
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Dummy_Controller::Dummy_Controller(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st,
				   Platform_Specific* ps)
  :Controller(cam, camid, c, param_host, st, ps)
{
  // Set type of controller to DUMMY
  controller_statusp->put(CONTROLLER_TYPE, (int) DUMMY);
  dummy_status = &controller_status->Controller_Status_u.dummy;

  // Initialise some member vars
  Dummy_Controller::init_vars();

  // Add parameters to the database but don't initialise them
  add_status_parameters(false);
}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::Dummy_Controller
 *
 * INVOCATION: dummy_controller = new Dummy_Controller(Instrument_Status *st,
 *                                Parameter *ip, Parameter *cp, int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > st		- status memory for this instrument
 * > ip		- instrument status parameter object
 * > cp		- camera status parameter object
 * > op		- operation status parameter object
 * > deb	- debug level
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Full constructor for the Dummy Controller
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Dummy_Controller::Dummy_Controller(Instrument_Status* st, Parameter* ip, Parameter* cp, Parameter* op,
				   int deb, Platform_Specific* ps)
  :Controller(st,ip,cp,op,deb,ps)
{

  // Set type of controller to DUMMY
  controller_statusp->put(CONTROLLER_TYPE, (int) DUMMY);
  dummy_status = &controller_status->Controller_Status_u.dummy;

  // Initialise some member vars
  Dummy_Controller::init_vars();

  // Add parameters to the database - this time initialising them
  add_status_parameters(true);

  // Use configuration setup in configuration status memory
  dummy_config = current_config->config_data.Config_Data_u.camera.controller.Controller_Config_u.dummy;
  dummy_init.config_data = dummy_config.config_data;

  // Setup the simulation data
  setup_sim_data();

}


/*
 *+
 * FUNCTION NAME: Dummy_Controller::init_vars
 *
 * INVOCATION: dummy->init_vars()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Initialise any dummy vars
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::init_vars()
{
  int i;
  Dummy_Config_Data
    tconfig = {
      "", // TODO: Figure this one out: DEFAULT_SIM_IMAGE,
      DEFAULT_SIM_EXPTIME,
      DEFAULT_PIXEL_READ_TIME,
      DEFAULT_PIXEL_CLEAR_TIME,
      DEFAULT_COSMIC_RATE
    };

  Dummy_Readout treadout = {0};
  dummy_init.dummy_steps = DEFAULT_DUMMY_STEPS;
  dummy_init.config_data = tconfig;
  dummy_config.config_data = tconfig;
  dummy_readout = treadout;

  // Initialise arrays of images
  /*
   * TODO: Redo
  for (i=0; i<MAX_AMPS; i++) {
    in_img[i] = NULL;
    cosimg[i] = NULL;
  }
  */

  // Initialise status parameter DB
  dummy_statusp = NULL;

  // Setup default config file
  strlcpy(dummy_config_file,dummy_path(DUMMY_SETUP,controller_dir),FILENAMELEN);

  // Setup default input image to NULL with exposure time scale to 1
//  fits_image = NULL;
  exptime_scale = 1;
  sim_data_ready = false;

  // Setup default row anc col compression
  rcf = ccf = 1;

}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::add_status_parameters
 *
 * INVOCATION: add_status_parameters(bool init)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Adds all dummy status parameters to the parameter database
 *
 * DESCRIPTION: Uses the Parameter class to handle any Cicada status
 * parameters. This class does immediate write-thru operations to the
 * Cicada status shared memory on the observer computer any where on the net.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::add_status_parameters(bool init)
{
  // Create the status parameter DB
  dummy_statusp = new Parameter(controller_pDB,PARAMETER_NAME,controller_param_map, DUMMY_PREFIX, DB_SUFFIX);

  // Now add all the dummy controller Status parameters
  dummy_status->exposure = 0;
  dummy_statusp->add(EXPOSURE, NULL, (void*)&dummy_status->exposure, init, 1, &dummy_status->exposure,
		     VALIDITY_NONE, 0, (long*)NULL, PUBLISHED);

}


/*
 *+
 * FUNCTION NAME: Dummy_Controller::getconfig
 *
 * INVOCATION: config = dummy->getconfig(int read_configuration)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > read_configuration - if true configuration will be reread from disk
 *
 * FUNCTION VALUE: Config_Data*
 *
 * PURPOSE: Return hardware configuration
 *
 * DESCRIPTION: This function merely stores the configuration type and
 * controller description as Dummy has no configuration data.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Controller_Config* Dummy_Controller::getconfig(int read_configuration)
{

  // Dummy has no configuration data - just return type
  config.Config_Data_u.camera.controller.type = DUMMY;
  if (read_configuration) {
    Dummy_Setup setup;
    try {
      message_write("Reading setup file %s", dummy_config_file);
      setup.read(dummy_config_file);
    }
    catch (Error& ce) {
      // Add error message and then continue - setup will have default values
      msg_severity = ce.type;
      message_write(ce.record_error(__FILE__,__LINE__));
    }

    // Update config command memory with new config
    dummy_config.config_data = setup.config;
    dummy_init.config_data = dummy_config.config_data;

    // Open the default sim image
    setup_sim_data();
  }
  config.Config_Data_u.camera.controller.Controller_Config_u.dummy = dummy_config;
  message_write("Dummy configuration loaded...");

  return &config.Config_Data_u.camera.controller;
}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::setup_sim_data
 *
 * INVOCATION: dummy->setup_sim_data()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Determine if data is to be generated or obtained from
 * FITS image
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::setup_sim_data()
{
  struct stat stat_buf;
  string sim_image_name;

  // Set readout parameters
  if (verify_spec.do_test_pattern)
    sim_image_name = "GENERATE_DATA";
  else
    sim_image_name = dummy_init.config_data.sim_image;

  // By default assume we will be generating data
  use_fits = false;
  got_bias = false;
  image_info.unsigned_data = TRUE;
  image_info.bitpix = SHORT_BITPIX;
  image_info.bzero = (double)MAXS+1;
  image_info.bscale = 1.0;

  // Force image name to upcase for generate data check
  if (str_toupper(sim_image_name) != GENERATE_DATA) {

    // Check that specified image name exists
    if (stat(dummy_init.config_data.sim_image,&stat_buf) != 0) {
	message_write("Image %s does not exist or no dummy image specified - generating data",
		      dummy_init.config_data.sim_image);
    } else {
      image_file = dummy_init.config_data.sim_image;
      use_fits = true;

      // Create an Image object for the input image data.
      // Copy the input fits data into this.
      create_dummy_image();

      message_write("Using image %s for simulation data.", image_file.c_str());
    }
  } else {
    message_write("Generating simulation data");
  }
}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::init
 *
 * INVOCATION: dummy->init(Init init_rec)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > init - initialisation record
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Do hardware initialisation
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::init(Init init_rec)
{
  int i=0;

  dummy_init = init_rec.controller.Controller_Init_Data_u.dummy;


  // Call base class init for setting up common data
  Controller::init(init_rec);

  // Enter Hardware init sequence
  // For dummy just loop a few steps
  while ((i++<dummy_init.dummy_steps) && (!controller_abort)){
    rest(HALF_SECOND);
    message_write("Dummy init - step %d", i);
  }

  get_controller_info();

}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::controller_reset
 *
 * INVOCATION: dummy->controller_reset
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Do a controller reset
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::controller_reset()
{
  message_write("Dummy Controller reset");
}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::controller_test
 *
 * INVOCATION: dummy->controller_test(int ntests)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > ntests - number of tests to run
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Do a controller test
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::controller_test(int ntests)
{
  message_write("Dummy Controller test");
}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::controller_shutdown
 *
 * INVOCATION: dummy->controller_shutdown
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Do a controller shutdown
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::controller_shutdown()
{
  message_write("Dummy Controller shutdown");
}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::reset_detector
 *
 * INVOCATION: dummy->reset_detector(Detector_Reset reset, bool exp)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > reset - reset parameters
 * > exp - reset in an exposure sequence
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Reset detector hardware
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::reset_detector(Detector_Reset reset, bool exp)
{
  int i;
  long pixels = camera.cols * camera.rows;
  long reset_time;                        // simulated reset time in msec
  int delay;

  // Total time to reset detector (msecs)
  reset_time = (long) nint(pixels * dummy_init.config_data.pixel_clear_time + reset.reset_delay * THOUSAND);


  // Enter reset sequence sequence
  for (i=0; (i < reset.nresets) && !controller_abort; i++) {
    while ((reset_time > 0) && !controller_abort) {
      delay = (reset_time > HALF_SECOND)? HALF_SECOND:reset_time;
      rest(delay);
      reset_time = reset_time - delay;
    }
  }
}
/*
 *+
 * FUNCTION NAME:  Dummy_Controller::prepare_exposure()
 *
 * INVOCATION: prepare_exposure(int ns)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: prepares system for an exposure
 *
 * DESCRIPTION: Sets number of multiple controllers to sync.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES: None
 *
 *-
 */
void Dummy_Controller::prepare_exposure(int ns)
{
  // Set multiple controllers to sync
  n_sync = (ns>0)? ns:1;

}


/*
 *+
 * FUNCTION NAME: template<class T> void copy_pixel
 *
 * INVOCATION: copy_pixel(T* inp, T& outp, T* cp, T max, int rcf, int ccf,
 *			  int xo, int yo, int width, int height,
 *			  int full_width, float exptime_scale)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > inp - pointer to input pixel
 * < outp - output pixel
 * > cp - pointer to cosmic ray pixels
 * > max - maximum value of output pixel
 * > rcf - row compression factor
 * > ccf - column compression factor
 * > xo - x offset (columns)
 * > yo - y offset (rows)
 * > width - width of detector area
 * > height - height of detector area
 * > full_width - full width of detector
 * > exptime_scale - value to scale ouput based on actual exposure time
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Copy a pixel from an image with binning - handle data types T
 *
 * DESCRIPTION: Just add neighbouring pixels to simulate binning and
 * save to output array.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
template<class T> void copy_pixel(T* inp, T& outp, T* cp, T max, int rcf, int ccf,
				  int xo, int yo, int width, int height,
				  int full_width, float exptime_scale)
{
  int x,y,i;
  double val;

  // The following loop bins pixels as required
  val=0;
  for (y=0; ((y<ccf) && ((y+yo)<height)); y++)
    for (x=0; ((x<rcf) && ((x+xo)<width)); x++) {
      i = y*full_width+x;
      val += inp[i] * exptime_scale + cp[i];
    }
  // Adjust binning for pixels close to edge of region
  // when binning is not a multiple of width and height
  if (x*y>0)
    val = val*(float(ccf*rcf)/float(x*y));
  outp = (T) ((val>max)? max:val);
}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::setup_readout
 *
 * INVOCATION: setup_readout(int ns, Detector_Readout* readout_rec, Detector_Regions* trgn,
 *                           int& isize, double exptime)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ns - number of controllers to sync
 * > readout_rec - readout request structure
 * > trgn - readout regions object
 * ! isize - total bytes in readout - may be modified by controller specific req
 * > exptime - last exposure time
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: setup the readout
 *
 * DESCRIPTION: For Dummy, data are either taken from a Fits file
 * on disk or are generated. This is set up here so that other methods
 * can use the data.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::setup_readout(int ns, Detector_Readout* readout_rec, Detector_Regions* trgn,
				     IR_Exposure_Info& ir_einfo, unsigned long& isize, double exptime)
{

  // Call generic setup stuff
  Controller::setup_readout(ns, readout_rec, trgn, ir_einfo, isize, exptime);

  // Setup simulation data
  setup_sim_data();

  // Dummy hardware readout, just reads FITS file and returns requested region
  dummy_readout = readout_rec->data.Detector_Readout_Data_u.dummy;
  rcf = readout_rec->regions.rcf;
  ccf = readout_rec->regions.ccf;

  if (use_fits) {


    //Create an Image object for the input image data.
    // preserving original bitpix and no output scaling
    if (!sim_data_ready) {
      create_dummy_image();
    }

/**
 * TODO: Redo
    if ((rgn->nactive_amps>1) &&
	(rgn->nactive_amps > fits_image->nextend)) {
      throw Error("Configuration error - active_amps > fits extensions!",
		  E_ERROR, -1 ,__FILE__, __LINE__); ;
    }


    message_write("Read image %s", image_file.c_str());
    */

  } else {
    message_write("Using generated data!");
  }
}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::setup_dummy_exposure
 *
 * INVOCATION: setup_dummy_exposure(double exptime, double inc_exptime)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > exptime - total exposure time in secs
 * > inc_exptime - exposure time since last readout
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: prepares the exptime scale factor and cosmic ray image
 *
 * DESCRIPTION: Uses the supplied exposure time to calculate dummy
 * image exptime rate and an estimation of the cosmic ray image
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::setup_dummy_exposure(double exptime, double inc_exptime)
{
  Image_Pixel P;                 // For now use default pixel size - should make configurable
  Image* cosimg_save=NULL;       // Need to save old image if required to remember previous exposures
  int i,cr=0;

  // make the cosmic ray images when using a fits file or generated stars
  if (use_fits) {

    // Calculate exptime scale factor.
    exptime_scale = exptime/dummy_init.config_data.sim_exptime;

    // Calculate cosmic ray image to add to output
    /*
    for (i=0; i<=fits_image->nextend; i++) {
      if ((i==0) && (fits_image->extend))
	continue;
      // Save the previous cosmic rays
      if (cosimg[i])
 	cosimg_save = cosimg[i];

      // Always create a cosmic image even if none required - as this will be used during copy_pixel time
	cosimg[i] = new Image(in_img[i]->nx, in_img[i]->ny, in_img[i]->bitpix, image_info.unsigned_data);

      if (dummy_init.config_data.cosmic_rate > 0) {

	  switch (in_img[i]->bitpix) {
	  case BYTE_BITPIX:
	    cr = cosmic(cosimg[i]->pix, cosimg[i]->nx, cosimg[i]->ny, inc_exptime, 1.0, dummy_init.config_data.cosmic_rate, true, P);
	    break;
	  case SHORT_BITPIX:
	    if (image_info.unsigned_data)
	      cr = cosmic(cosimg[i]->upix, cosimg[i]->nx, cosimg[i]->ny, inc_exptime, 1.0, dummy_init.config_data.cosmic_rate, true, P);
	    else
	      cr = cosmic(cosimg[i]->spix, cosimg[i]->nx, cosimg[i]->ny, inc_exptime, 1.0, dummy_init.config_data.cosmic_rate, true, P);
	    break;
	  case INT_BITPIX:
	    if (image_info.unsigned_data)
	      cr = cosmic(cosimg[i]->uipix, cosimg[i]->nx, cosimg[i]->ny, inc_exptime, 1.0, dummy_init.config_data.cosmic_rate, true, P);
	    else
	      cr = cosmic(cosimg[i]->ipix, cosimg[i]->nx, cosimg[i]->ny, inc_exptime, 1.0, dummy_init.config_data.cosmic_rate, true, P);
	    break;
	  case FLOAT_BITPIX:
	    cr = cosmic(cosimg[i]->fpix, cosimg[i]->nx, cosimg[i]->ny, inc_exptime, 1.0, dummy_init.config_data.cosmic_rate, true, P);
	    break;
	  case DOUBLE_BITPIX:
	    cr = cosmic(cosimg[i]->dpix, cosimg[i]->nx, cosimg[i]->ny, inc_exptime, 1.0, dummy_init.config_data.cosmic_rate, true, P);
	    break;
	  }

      }

      if (debug & DEBUG_CALCS)
	message_write("setup_dummy_exposure: Cosmic rays detected=%d", cr);

      // Add the new cosmic arrays to ones previously recorded
      if (cosimg_save != NULL) {
 	Image* cosimg_added = image_add(cosimg[i], cosimg_save);
        // The add function creates a new image, so delete the operand images
	// and set cosimg to the result
	delete cosimg_save;
        Image* tmp = cosimg[i];
	cosimg[i] = cosimg_added;
	delete tmp;
      }
    }
    */
  } else {
    exptime_scale = 1;

    if (dummy_init.config_data.cosmic_rate > 0) {
      for (i=0; i<rgn->nactive_amps; i++) {
/*
	// Save the previous cosmic rays
	if (cosimg[i])
	  cosimg_save = cosimg[i];
	cosimg[i] = new Image(rgn->amp_width[i], rgn->amp_height[i], SHORT_BITPIX, image_info.unsigned_data);
	memset(cosimg[i]->upix, 0, cosimg[i]->nx*cosimg[i]->ny*sizeof(unsigned short));
	cr = cosmic(cosimg[i]->upix, cosimg[i]->nx, cosimg[i]->ny, inc_exptime, 1.0, dummy_init.config_data.cosmic_rate, true, P);

	if (debug & DEBUG_CALCS)
	  message_write("setup_dummy_exposure: Cosmic rays detected=%d", cr);

	// Add the new cosmic arrays to ones previously recorded
	if (cosimg_save != NULL) {
	  Image* cosimg_added = image_add(cosimg[i], cosimg_save);
	  delete cosimg_save;
	  Image* tmp = cosimg[i];
	  cosimg[i] = cosimg_added;
	  delete tmp;
	}
*/
      }
    }
  }
}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::init_readout
 *
 * INVOCATION: init_readout()int s, int r
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > s - sample number
 * > r - read number
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: initialise readout parameters
 *
 * DESCRIPTION: Calls generic init stuff and then sets up exptime for dummy
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::init_readout(int s, int r)
{
  int i;

  // Timestamp the start of the readout
  clockout_stime = epicsTime::getCurrent();

  // Call standard init routine
  Controller::init_readout(s,r);


  // initialise the buffer counters if on first read
  /*
   * TODO: Redo
  if ((r==0) && (s==0)) {
    n = ln = 0;
    for (i=0; i<MAX_AMPS; i++) {
      if (cosimg[i]) {
	delete cosimg[i];
	cosimg[i] = NULL;
      }
    }
  }
  */


  // Now adjust the dummy exposure for this readout - this depends on type of
  // camera
  if (camera.type == IR_CAMERA) {
    double total_exposure_time, exposure_time_since_last;

    // So far the progressive exposure time is the time taken to do the number
    // of periods so far + the time taken for the number of samples so far taken in
    // current set.
    // TODO: Fix this
    /*
    total_exposure_time = ir_einfo.readTime*s + ir_einfo.period*r;
    exposure_time_since_last = (s>0)? ir_einfo.readTime:ir_einfo.readTime+ir_einfo.readIntvl;
    */
    total_exposure_time = 0;
    exposure_time_since_last = 0;
    setup_dummy_exposure(total_exposure_time, exposure_time_since_last);

  } else {

    // Simply use exposure time just taken
    setup_dummy_exposure(last_exptime, last_exptime);
  }

  // count of readout
  // rinc = (r*ir_einfo.nfowler + s) * verify_spec.readout_increment;
  rinc = 1;

  read_running = r+1;

}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::start_ir_image_transfer_thread()
 *
 * INVOCATION: ummy->start_image_transfer_thread(IR_Expose& expose, Thread* ir_tt)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > expose - pointer to exposure info
 * > ir_tt - pointer to thread monitoring IR exposure timing
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: starts image transfer thread
 *
 * DESCRIPTION: Take copy of exposure info and call generic routine to start thread
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::start_ir_image_transfer_thread(IR_Expose& expose, Thread* ir_tt)
{
  ir_einfo = expose.einfo;
  ir_timer_thr = ir_tt;
  start_image_transfer_thread();
}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::start_image_transfer_thread()
 *
 * INVOCATION: dummy->start_image_transfer_thread()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: starts image transfer thread
 *
 * DESCRIPTION: Start thread if any readout region specified
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::start_image_transfer_thread()
{
  // To transfer the image - start a separate thread
  stop_readout = false;

  // Start thread if any readout region specified
  if (actual_image_size > 0) {
	  /* TODO: Figure this one out
    image_transfer_thr = new Thread("dummy_image_transfer", dummy_image_transfer_thread, (void*)this, true);
    image_transfer_thr->unlock();
    */
  } else {
    image_transfer_thr = NULL;
  }
}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::start_row_region
 *
 * INVOCATION: dummy->start_row_region(int rr)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > rr - row number of region
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Actions necessary before reading a row region
 *
 * DESCRIPTION: Nothing required for Dummy
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::start_row_region(int rr)
{
}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::wait_segment_readout
 *
 * INVOCATION: dummy->wait_segment_readout(int rows_processed, int row, int rowbytes, int& rt)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > rows_processed - number of rows expected so far
 * > row - row number of region
 * > rowbytes - number of bytes per row
 * ! rt - current count of total rows transfered in readout
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Wait on thread to complete a segment readout
 *
 * DESCRIPTION: Looks at status of image transfer by monitoring
 * . number of rows processed
 * . if image transfer is still going
 * . not aborted
 *
 * EXTERNAL VARIABLES:
 * Member variables of class Controller
 *  bool doing_image_transfer;      // Flag indicating image transfer started
 *  bool finished_image_transfer;   // Flag indicating image transfer finished
 *  pthread_mutex_t image_mutex;    // Mutex lock for image
 *  int rows_transfered;            // Number of rows transfered in readout
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::wait_segment_readout(int rows_processed, int row, int rowbytes, int& rt)
{
  bool dit;

  if (image_transfer_thr) {
    // Wait for mutex before checking vars
    image_transfer_thr->lock();
    rt = rows_transfered;
    dit = doing_image_transfer;

    // unlock mutex so that read thread can access vars
    image_transfer_thr->unlock();

    while ((rt<rows_processed) && !controller_abort) {//&& dit
      rest(IMAGE_WAIT);

      // Wait for mutex before checking vars
      image_transfer_thr->lock();
      rt = rows_transfered;
      dit = doing_image_transfer;

      // unlock mutex so that read thread can access vars
      image_transfer_thr->unlock();
    }
  }
}
/*
 *+
 * FUNCTION NAME: get_readouts_done
 *
 * INVOCATION: rd = get_readouts_done(int rowbytes)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > rowbytes - number of bytes per row
 *
 * FUNCTION VALUE: int
 *
 * PURPOSE:
 *  Returns the number of readouts completed
 *
 * DESCRIPTION: Simple returns counter
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
long Dummy_Controller::get_readouts_done()
{
  return readouts_done;
}
/*
 *+
 * FUNCTION NAME: get_read_running
 *
 * INVOCATION: rd = get_read_running
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: int
 *
 * PURPOSE:
 *  Returns the number of current readout
 *
 * DESCRIPTION: Simple returns counter
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
long Dummy_Controller::get_read_running()
{
  return read_running;
}
/*
 *+
 * FUNCTION NAME: get_readout_status
 *
 * INVOCATION: get_readout_status()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE:
 *  Gets a readouts full status - currently nothing to do for dummy
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::get_readout_status(int rowbytes)
{
}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::do_stop_readout
 *
 * INVOCATION: do_stop_readout()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE:
 *  stop the readouts
 *
 * DESCRIPTION: Simply sets stopt_readout flag
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::do_stop_readout()
{
  stop_readout = true;
}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::finish_readout
 *
 * INVOCATION: dummy->finish_readout();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Finish up after the readout
 *
 * DESCRIPTION:  Finishes the clockout.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::finish_readout()
{
  finish_clockout();
}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::init_clockout()
 *
 * INVOCATION: dummy->init_clockout()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Initialise things for a dummy camera clockout
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::init_clockout()
{
  double std_dev,median,mean,min,max,mode,zmin,zmax,zslope;
  int poverscan,j,np,bytepix;

  // Some local variables
  ctime = 0;
  sleeptime = 0;

  // Number of output overscan pixels to be added artificially to each region
  if (!rcf || !ccf) {
    throw Error("Dummy_Controller: Row and column compression factors must be greater than zero!",
		E_ERROR, -1 ,__FILE__, __LINE__);
  }
  soverscan = (int)(ceil(((float)rgn->overscan_cols)/rcf)); // serial OS
  poverscan = (int)(ceil(((float)rgn->overscan_rows)/ccf)); // parallel OS

  if (use_fits) {

    // Check that the input image has been read; if not, read it.
    if (!sim_data_ready) {

      create_dummy_image();

      if (debug & DEBUG_ESSENTIAL) {
	message_write("init_clockout: Dummy image created ok %s", image_file.c_str());
      }
    }

  }


  if ((soverscan>0) || (poverscan>0) || !use_fits) {
	  /*
	   * TODO: Redo this...
    if (use_fits && !got_bias) {

      // set a bias value from image stats - 1% sample the image to get median
      j = (fits_image->extend)? 1:0;
      np = in_img[j]->nx*in_img[j]->ny;
      switch (image_info.bitpix) {
      case BYTE_BITPIX:
	calc_stats(in_img[j]->pix, np, image_info.bitpix, std_dev, median, mean, min,
		   max, mode, zmin, zmax, zslope);
	break;
      case SHORT_BITPIX:
	if (image_info.unsigned_data)
	  calc_stats(in_img[j]->upix, np, image_info.bitpix, std_dev, median,
		     mean, min, max, mode, zmin, zmax, zslope);
	else
	  calc_stats(in_img[j]->spix, np, image_info.bitpix, std_dev, median, mean,
		     min, max, mode, zmin, zmax, zslope);
	break;
      case INT_BITPIX:
	if (image_info.unsigned_data)
	  calc_stats(in_img[j]->uipix, np, image_info.bitpix, std_dev, median,
		     mean, min, max, mode, zmin, zmax, zslope);
	else
	  calc_stats(in_img[j]->ipix, np, image_info.bitpix, std_dev, median, mean,
		     min, max, mode, zmin, zmax, zslope);
	break;
      case FLOAT_BITPIX:
	calc_stats(in_img[j]->fpix, np, image_info.bitpix, std_dev, median, mean,
		   min, max, mode, zmin, zmax, zslope);
	break;
      case DOUBLE_BITPIX:
	calc_stats(in_img[j]->dpix, np, image_info.bitpix, std_dev, median, mean,
		   min, max, mode, zmin, zmax, zslope);
	break;
      }
      bias_val = (unsigned short) (median * exptime_scale * rcf * ccf);
      got_bias = true;
    } else {
    */
      // set a constant bias value
      bias_val = verify_spec.overscan * rcf * ccf;
      /*
    } // end if-use_fits-else
    */
  } // end if soverscan...

  // Initialise pixel output counters
  // We use a cyclic image buffer that may be able to contain more than one image
  // frame - especially in case of IR cameras. Reset output counter to zero if
  // we cannot fit a new frame
  bytepix = BYTE_PIX(image_info.bitpix);
  if (n * bytepix  + actual_image_size > image_buffer_size) {
    n = ln = 0;
  }

  // Set pointer to image buffer to receive the multiplexed data
  if (use_fits) {
    switch (image_info.bitpix) {
    case BYTE_BITPIX:
      bop = (char*) image_buffer_ptr;
      break;
    case SHORT_BITPIX:
      if (image_info.unsigned_data)
	uop = (unsigned short*) image_buffer_ptr;
      else
	sop = (short*) image_buffer_ptr;
      break;
    case INT_BITPIX:
      if (image_info.unsigned_data)
	uiop = (unsigned int*) image_buffer_ptr;
      else
	iop = (int*) image_buffer_ptr;
      break;
    case FLOAT_BITPIX:
      fop = (float*) image_buffer_ptr;
      break;
    case DOUBLE_BITPIX:
      dop = (double*) image_buffer_ptr;
      break;
    default:
      break;
    }
  } else {
    uop = (unsigned short*) image_buffer_ptr;
  }

  npar = rgn->npar_regions;

}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::clockout_fits_pixels
 *
 * INVOCATION: dummy->clockout_fits_pixels(int row)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > row - row number to clockout
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Copy pixels from FITS file
 *
 * DESCRIPTION:
 * Clocks out a row of pixels into a buffer. Note that a "row" refers
 * to the line of pixels which are clocked out, regardless of whether
 * they are in a column (for column oriented chips) or a row (for row-
 * oriented chips).
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::clockout_fits_pixels(int row)
{
  int i,j,a,c,nc;                       // loop counter vars
  int axd,ayd,awid,ahei,start,fwidth;   // indexing variables for picking pixels out of input image
  Readout_Orientation aorient;          // amplifier orientation

  // input image pointers, cosmic ray img pointers - for each data type:
  char *bfp[MAX_IMAGE_FRAMES];
  char *bcp[MAX_IMAGE_FRAMES];
  short *sfp[MAX_IMAGE_FRAMES];
  short *scp[MAX_IMAGE_FRAMES];
  unsigned short *ufp[MAX_IMAGE_FRAMES];
  unsigned short *ucp[MAX_IMAGE_FRAMES];
  int *ifp[MAX_IMAGE_FRAMES];
  int *icp[MAX_IMAGE_FRAMES];
  unsigned int *uifp[MAX_IMAGE_FRAMES];
  unsigned int *uicp[MAX_IMAGE_FRAMES];
  float *ffp[MAX_IMAGE_FRAMES];
  float *fcp[MAX_IMAGE_FRAMES];
  double *dfp[MAX_IMAGE_FRAMES];
  double *dcp[MAX_IMAGE_FRAMES];
  int next_pix;                         // Offset to next pixel

  epicsTime t0, t1, t2, t3;


  // Update fits data pointers to new column location
  /* TODO: Redo
  j = (fits_image->extend)? 1:0;
  */

  for (i=0; i<rgn->nactive_amps; i++) {

    t0=epicsTime::getCurrent();

    if (controller_abort)
      break;

    // Calculate which amp/fits extension this frame belongs to
    a = rgn->get_amp(i);
    axd = rgn->amp_xdir[a];
    ayd = rgn->amp_ydir[a];
    awid = rgn->amp_width[a];
    ahei = rgn->amp_height[a];
    aorient = rgn->amp_orient[a];

    // Depending on which orientation the chip is read out, set up
    // the indexing into the data arrays appropriately
    if (aorient == ORIENT_ROW) {
      if (axd == 1)
	start = (ayd == 1)? (xo+row*awid): (xo+(ahei-row-1)*awid);
      else
	start = (ayd == 1)? ((awid-xo-1)+row*awid): ((awid-xo-1)+(ahei-row-1)*awid);
    } else { // ORIENT_COL
      if (axd == 1)
	start = (ayd == 1)? (row+(xo*awid)): ((ahei-1)*awid+row - (xo*awid));
      else
	start = (ayd == 1)? (awid-1-row+(xo*awid)): (awid*ahei-1-row-(xo*awid));
    }

    // Now set the starting input pixels for each data type for
    // both the input image and the cosmic ray image
    /*
     * TODO: Redo
    bfp[i] = in_img[a+j]->pix + start;
    sfp[i] = in_img[a+j]->spix + start;
    ufp[i] = in_img[a+j]->upix + start;
    ifp[i] = in_img[a+j]->ipix + start;
    uifp[i] = in_img[a+j]->uipix + start;
    ffp[i] = in_img[a+j]->fpix + start;
    dfp[i] = in_img[a+j]->dpix + start;
    bcp[i] = cosimg[a+j]->pix + start;
    scp[i] = cosimg[a+j]->spix + start;
    ucp[i] = cosimg[a+j]->upix + start;
    icp[i] = cosimg[a+j]->ipix + start;
    uicp[i] = cosimg[a+j]->uipix + start;
    fcp[i] = cosimg[a+j]->fpix + start;
    dcp[i] = cosimg[a+j]->dpix + start;
    */
  }

  tt1+=epicsTime::getCurrent()-t0;

  // Loop through the serial row pixel by pixel
  c = xo;
  for (nc=0; nc<swidth_b; nc++) {

    if (controller_abort)
      break;

    // Copy a pixel emulate the multiplex delivery of data from detectors
    for (i=0; i<rgn->nactive_amps; i++) {
      t1=epicsTime::getCurrent();

      if (controller_abort)
	break;

      // Calculate which amp/fits extension this frame belongs to
      t3=epicsTime::getCurrent();
      //a = rgn->get_amp(i);
      a = rgn->amp_list[i];
      tt4+=epicsTime::getCurrent()-t3;
      fwidth = rgn->amp_width[a];
      axd = rgn->amp_xdir[a];
      ayd = rgn->amp_ydir[a];
      ahei = rgn->amp_height[a];
      aorient = rgn->amp_orient[a];

      // Set index differently according to readout orientation
      if (aorient == ORIENT_ROW)
	next_pix = axd*rcf;
      else
	next_pix = ayd*awid*ccf;

      tt2+=epicsTime::getCurrent()-t1;
      t2=epicsTime::getCurrent();

      // Now copy the next pixel to the output array - simulate a chip pixel read
      // Handle each data type.
      switch (image_info.bitpix) {
      case BYTE_BITPIX:
	copy_pixel(bfp[i],bop[n++],bcp[i],MAXC,rcf,ccf,c-xo,row-yo,swidth,height,fwidth,exptime_scale);
	bfp[i] += next_pix;
	bcp[i] += next_pix;
	break;
      case SHORT_BITPIX:
	if (image_info.unsigned_data) {
	  copy_pixel(ufp[i],uop[n++],ucp[i],MAXU,rcf,ccf,c-xo,row-yo,swidth,height,fwidth,exptime_scale);
	  ufp[i] += next_pix;
	  ucp[i] += next_pix;
	} else {
	  copy_pixel(sfp[i],sop[n++],scp[i],MAXS,rcf,ccf,c-xo,row-yo,swidth,height,fwidth,exptime_scale);
	  sfp[i] += next_pix;
	  scp[i] += next_pix;
	}
	break;
      case INT_BITPIX:
	if (image_info.unsigned_data) {
	  copy_pixel(uifp[i],uiop[n++],uicp[i],MAXUI,rcf,ccf,c-xo,row-yo,swidth,height,fwidth,exptime_scale);
	  uifp[i] += next_pix;
	  uicp[i] += next_pix;
	} else {
	  copy_pixel(ifp[i],iop[n++],icp[i],MAXI,rcf,ccf,c-xo,row-yo,swidth,height,fwidth,exptime_scale);
	  ifp[i] += rcf*next_pix;
	  icp[i] += rcf*next_pix;
	}
	break;
      case FLOAT_BITPIX:
	copy_pixel(ffp[i],fop[n++],fcp[i],MAXF,rcf,ccf,c-xo,row-yo,swidth,height,fwidth,exptime_scale);
	ffp[i] += rcf*next_pix;
	fcp[i] += rcf*next_pix;
	break;
      case DOUBLE_BITPIX:
	copy_pixel(dfp[i],dop[n++],dcp[i],MAXD,rcf,ccf,c-xo,row-yo,swidth,height,fwidth,exptime_scale);
	dfp[i] += rcf*next_pix;
	dcp[i] += rcf*next_pix;
	break;
      default:
	break;
      }
      tt3+=epicsTime::getCurrent()-t2;
      t3=epicsTime::getCurrent();
    }// End of frame pixel copy loop

    c+=rcf;
  }

}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::clockout_row
 *
 * INVOCATION: dummy->clockout_row(int row_region, int row)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > row_region - number of the row_region being worked on
 * > row - row number to clockout
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Clockout an individual row
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::clockout_row(int row_region, int row)
{
  int cr;
  char bval;                           // pointers for different data types
  short sval;
  unsigned short uval;
  int ival;
  unsigned int uival;
  float fval;
  double dval;
  int ai = verify_spec.amp_increment;
  int inc = (verify_spec.constant_val)? 0:verify_spec.increment;

  bval = uval = sval = uival = uval = bias_val;
  fval = dval = bias_val;

  // Loop over each serial region - simulate a detector readout
  for (cr=0; cr<nser; cr++) {

    if (controller_abort)
      break;

    // Set starting pixel x address
    xo = rgn->par_regions[row_region].ser_regions[cr].xo;
    swidth = rgn->par_regions[row_region].ser_regions[cr].width;
    swidth_b = rgn->par_regions[row_region].ser_regions[cr].width_b;

    // Is this overscan?
    if (((rgn->overscan_rows) && (row_region == npar - 1)) ||
	((rgn->overscan_cols) && (cr == nser - 1))) {
      if (use_fits) {
	switch (image_info.bitpix) {
	case BYTE_BITPIX:
	  generate_pixels(bop,bval,MAXC,n,0,ai,0,rgn->nactive_amps,swidth,swidth_b,rgn->rcf);
	  break;
	case SHORT_BITPIX:
	  if (image_info.unsigned_data)
	    generate_pixels(uop,uval,MAXU,n,0,ai,0,rgn->nactive_amps,swidth,swidth_b,rgn->rcf);
	  else
	    generate_pixels(sop,sval,MAXS,n,0,ai,0,rgn->nactive_amps,swidth,swidth_b,rgn->rcf);
	  break;
	case INT_BITPIX:
	  if (image_info.unsigned_data)
	    generate_pixels(uiop,uival,MAXUI,n,0,ai,0,rgn->nactive_amps,swidth,swidth_b,rgn->rcf);
	  else
	    generate_pixels(iop,ival,MAXI,n,0,ai,0,rgn->nactive_amps,swidth,swidth_b,rgn->rcf);
	  break;
	case FLOAT_BITPIX:
	  generate_pixels(fop,fval,MAXF,n,0,ai,0,rgn->nactive_amps,swidth,swidth_b,rgn->rcf);
	  break;
	case DOUBLE_BITPIX:
	  generate_pixels(dop,dval,MAXD,n,0,ai,0,rgn->nactive_amps,swidth,swidth_b,rgn->rcf);
	  break;
	}
      } else {

	// Generate the test pattern pixels for this row
	generate_pixels(uop,uval,MAXU,n,0,ai,0,rgn->nactive_amps,swidth,swidth_b,rgn->rcf);

      }

    } else if (use_fits) {

      // normal pixels - are they from a fits file?
      clockout_fits_pixels(row);

    } else {
      int j;

      // normal pixels - generated.
      j = n;
	// Test pattern pixels
	generate_pixels(uop,dummy_val,MAXU,n,inc,ai,rinc,rgn->nactive_amps,swidth,swidth_b,rgn->rcf);

      // Add in cosmic rays if required
      if (dummy_init.config_data.cosmic_rate > 0) {
	int a,axd,ayd,awid,ahei,start,i,nc,fwidth,next_pix;
	Readout_Orientation aorient;          // amplifier orientation
	unsigned short *ufp[MAX_IMAGE_FRAMES];
	unsigned short *ucp[MAX_IMAGE_FRAMES];
	/* TODO: Redo
	int jj = (fits_image->extend)? 1:0;
	*/

	for (i=0; i<rgn->nactive_amps; i++) {
	  a = rgn->get_amp(i);
	  axd = rgn->amp_xdir[a];
	  ayd = rgn->amp_ydir[a];
	  awid = rgn->amp_width[a];
	  ahei = rgn->amp_height[a];
	  aorient = rgn->amp_orient[a];
	  if (aorient == ORIENT_ROW) {
	    if (axd == 1)
	      start = (ayd == 1)? (xo+row*awid): (xo+(ahei-row-1)*awid);
	    else
	      start = (ayd == 1)? ((awid-xo-1)+row*awid): ((awid-xo-1)+(ahei-row-1)*awid);
	  } else { // ORIENT_COL
	    if (axd == 1)
	      start = (ayd == 1)? (row+(xo*awid)): ((ahei-1)*awid+row - (xo*awid));
	    else
	      start = (ayd == 1)? (awid-1-row+(xo*awid)): (awid*ahei-1-row-(xo*awid));
	  }
	  ufp[i] = uop;
// TODO: Redo
//	  ucp[i] = cosimg[a+jj]->upix + start;
	}

	for (nc=0; nc<swidth_b; nc++) {
	  for (i=0; i<rgn->nactive_amps; i++) {
	    a = rgn->amp_list[i];
	    fwidth = rgn->amp_width[a];
	    axd = rgn->amp_xdir[a];
	    ayd = rgn->amp_ydir[a];
	    ahei = rgn->amp_height[a];
	    aorient = rgn->amp_orient[a];

	    // Set index differently according to readout orientation
	    if (aorient == ORIENT_ROW)
	      next_pix = axd*rcf;
	    else
	      next_pix = ayd*awid*ccf;

	    uop[j++] += *ucp[i];

	    ucp[i] += next_pix;
	  }
	}
      }
    } // End if overscan_cols
  } // End of serial region loop
}

/*
 *+
 * FUNCTION NAME: Dummy_Controller::clockout
 *
 * INVOCATION: dummy->clockout()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Clockout a detector using the regions spec supplied
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::clockout()
{
  int rr,r,rp, asleep,nr;
  double sim_time, dctime;               // Simulation time required for each row and actual time for rows (seconds)
  int tsleep;                            // time to sleep to simulate clocking time - msec
  int total_height_b = 0;                // total binned height

  tt1=tt2=tt3=tt4=0;

  dummy_val = (verify_spec.constant_val)? verify_spec.seed:0;

  if (!rgn->nactive_amps) {
    throw Error("Dummy_Controller: Number of active amps must be greater than zero!",
		E_ERROR, -1 ,__FILE__, __LINE__);
  }


  // Loop over each row region
  for (rr=0; rr<rgn->npar_regions; rr++) {

    if (controller_abort)
      break;

    // Set local vars for size of this region
    height = rgn->par_regions[rr].height;
    height_b = rgn->par_regions[rr].height_b;
    total_height_b += height_b;
    width = rgn->par_regions[rr].width;
    yo = rgn->par_regions[rr].yo;
    nser = rgn->par_regions[rr].nser_regions;

    // Total overscan pixels to be added for one row of output
    nsoverscan = (nser - 1) * soverscan * rgn->nactive_amps;

    // Report progress at 10% interval
    rp = height_b/10;

    // Loop over each row in parallel region
    r = yo;


    for (nr=0; nr<height_b; nr++) {

      if (controller_abort) {
	break;
      }


      // Clockout one row
      clockout_row(rr,r);

      // Simulate time to clock all pixels so far - usecs
      ctime = epicsTime::getCurrent() - clockout_stime;

      // What should have been the time for pixels in this clockout - in seconds
      sim_time = dummy_init.config_data.pixel_read_time*(n-ln);

      // Check difference with actual time taken - convert to msec and wait if > 0
      // Also add sleep time from previous iteration (ie lower than clock resolution)
      dctime = ctime/(double)timestamp_res();
      tsleep = nint((sim_time - dctime)*THOUSAND);
      if (tsleep > 0) {
	asleep = rest(tsleep);
	sleeptime += asleep;
      }

      if ((debug & DEBUG_CALCS) && (r%rp == 0))
	message_write("clockout: Clocked row %d, totalwait=%f", r, sleeptime/THOUSAND);

      // Now update external progress variable
      // Wait for mutex before proceeding with var update
      image_transfer_thr->lock();

      rows_transfered++;

      // unlock mutex so that main thread can access vars
      image_transfer_thr->unlock();

      r+=ccf;

    } // End of row loop


  } // End of row region loop


  // Update the progress so far counter (ie last n)
  ln = n;

  // For debugging print out timing for different patrs of readout
  if (debug & DEBUG_CALCS)
    message_write("tt1=%f,tt2=%f,tt3=%f,tt4=%f,sleep=%f", tt1/(double)timestamp_res(), tt2/(double)timestamp_res(),
		  tt3/(double)timestamp_res(),tt4/(double)timestamp_res(),sleeptime/THOUSAND);
}
/*
 *+
 * FUNCTION NAME: Dummy_Controller::finish_clockout
 *
 * INVOCATION: dummy->finish_clockout()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Finish up the clockout
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Dummy_Controller::finish_clockout()
{
  // we don't want to close the Fits image until a new one is read by
  // create_dummy_image.
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: create_dummy_image()
 *
 * INVOCATION: create_dummy_image();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none.
 *
 * PURPOSE: To create a local, writeable fits image which is initially a copy
 * of the input image.  This allows manipulation of the image contents
 * without disturbing the fits file on disk.
 *
 * DESCRIPTION:
 * Open the input file and read it.
 * Create a new fits image file in memory.
 * Write the read data into the new file.
 * Close the input file.
 * Return a pointer to the new file.
 *
 * The user must close or delete the new image after use, because
 * this leaves it open for reading or update.
 * EXTERNAL VARIABLES: none
 *
 * PRIOR REQUIREMENTS: Delete existing occurrences of the local image file.
 *
 * DEFICIENCIES:
 *
 *---------------------------------------------------------------------------
 */
void Dummy_Controller::create_dummy_image()
{
  double sum,min,max;
  int ixt;

  // if image already exists, delete it entirely.
  /* TODO: Redo
  if (fits_image) {
    delete fits_image;
  }

  // Create the input fits image and read it.
  try {

    // Read the input image with scaling (default 3rd. parameter).
    fits_image = new Fits((char*)image_file.c_str());

      fits_image->read((char*)image_file.c_str());

    // Create a new dummy fits image file for processing the image, using the
    // Fits copy constructor.

    ixt = fits_image->nextend ? 1 : 0;

    // Assume data is unsigned if integer type with bzero =2**bitpix-1
    image_info.unsigned_data = (((fits_image->xt[ixt].bitpix==SHORT_BITPIX) &&
                                 (fits_image->xt[ixt].bzero==((double)MAXS+1))) ||
                                ((fits_image->xt[ixt].bitpix==INT_BITPIX) &&
                                 (fits_image->xt[ixt].bzero==((double)MAXI+1))));
    image_info.bitpix = fits_image->xt[ixt].bitpix;
    image_info.bscale = fits_image->xt[ixt].bscale;
    image_info.bzero = fits_image->xt[ixt].bzero;

    // Create an Image copy of the Fits data in each extension.
    for (ixt=0; ixt<=fits_image->nextend; ixt++) {

      if ((ixt==0) && (fits_image->extend))
	continue;

      /* TODO: Redo
      if (in_img[ixt])
        delete in_img[ixt];

      in_img[ixt] = new Image(fits_image->xt[ixt].nx, fits_image->xt[ixt].ny,
                              fits_image->extend == 0 ? 1 : fits_image->xt[ixt].detx,
                              fits_image->xt[ixt].bitpix,
                              fits_image->xt[ixt].data,
                              image_info.unsigned_data);

      in_img[ixt]->fits_ext = ixt;
      in_img[ixt]->name = fits_image->fitsname;

      if (debug&DEBUG_CALCS) {
	int np = in_img[ixt]->nx*in_img[ixt]->ny;
	if (np>0) {
	  imstat(in_img[ixt]->pix, np, in_img[ixt]->bitpix, sum, min, max);
	  message_write("Image section %d mean =% , max=%f, min=%f", ixt, sum/np, max, min);
	} else
	  message_write("No pixels in extension %d of simulation image?", ixt);
      }
    } // end for

    if (debug & DEBUG_ESSENTIAL) {
      message_write("Using image %s for simulation data.", in_img[fits_image->extend]->name.c_str());
    }

    sim_data_ready = true;

  } // end try
  catch (Error& ce) {
    message_write("create_dummy_image: Unable to read Fits file %s", image_file.c_str());
    message_write("%s", ce.record_error(__FILE__, __LINE__));
    ce.rethrow();
  } // end catch
  */

} // create_dummy_image


/*
 *+
 * FUNCTION NAME: Dummy_Controller::~Dummy_Controller()
 *
 * INVOCATION: delete dummy
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Destructor
 *
 * DESCRIPTION:
 * Free fits_image object if required
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

Dummy_Controller::~Dummy_Controller()
{
  int i;

  // Free dummy image space that was used in test mode
  /* TODO: Redo
  if (fits_image)
    delete fits_image;

  // Clean up temporary images
  for (i=0; i<MAX_AMPS; i++) {
    if (in_img[i])
      delete in_img[i];
    if (cosimg[i])
      delete cosimg[i];
  }
  */

  // clean up parameter db
  if (dummy_statusp)
    delete dummy_statusp;
}

/*
 *+
 * FUNCTION NAME: dummy_image_transfer_thread
 *
 * INVOCATION: use pthread_create
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > arg - pointer to Dummy_controller object calling this thread
 *
 * FUNCTION VALUE: Pointer to user data - NULL if success
 *
 * PURPOSE: Perform dummy readout in separate thread
 *
 * DESCRIPTION:
 * This readout simulation takes as input a chosen FITS image and then
 * models a detector readout that clocks out the serial register a pixel at
 * a time, interleaving pixels from multiple amplifiers (multiple FITS
 * extensions). Support for binning and arbitrary region selection is
 * also provided. Overscan pixels (row and col) are also supported and a
 * constant (arbitrary) bias value is added artificially to the FITS data.
 * Image transfer routine
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void* dummy_image_transfer_thread(void *arg)
{
	// TODO: Figure this one out (threading)
/*
  const int DUMMY_MIN_WAIT = 500;        // minimum time left before calliong next read - msec
  Thread_Arg* ta = (Thread_Arg*) arg;    // Argument passed to routines started from Thread class
  Thread* image_transfer_thr = ta->thread;// Pointer to Thread object used to start this thread
  Dummy_Controller *dc  = (Dummy_Controller*) ta->arg; // Expect Thread_Arg.arg to be a pointer to Dummy_Controller
  int r,s;                               // read counter
  epicsTime rtime, stime;                // Timestamps
  long nreads, nsamples;                 // counters used for IR camera multiple read requirements
  long wtime;                            // time to wait for next readout - msec
  double reset_wait;                     // time to wait for reset - sec
  double rt;                             // latest readout time
  bool mlock = false;

  try {
    dc->image_readout_thread_abort = false;
    dc->image_readout_errno = 0;

    // Wait for mutex before proceeding
    image_transfer_thr->lock();
    mlock = true;


    // unlock mutex so that main thread can access vars
    image_transfer_thr->unlock();
    mlock = false;

    // depending on the camera type
    if (dc->camera.type == IR_CAMERA) {
      nreads = dc->ir_einfo.nperiods + 1;
      nsamples = dc->ir_einfo.nfowler;

      // Simulate reset time by waiting
      stime = epicsTime::getCurrent();
      reset_wait = (dc->ir_einfo.nresets)? dc->ir_einfo.nresets*dc->ir_einfo.readTime+dc->ir_einfo.resetDelay:0.0;
      if (dc->debug & DEBUG_ESSENTIAL)
	dc->message_write("Image readout: Reset wait=%f (nresets=%d,readTime=%f,resetdelay=%f)", reset_wait,
			  dc->ir_einfo.nresets, dc->ir_einfo.readTime,dc->ir_einfo.resetDelay);
      if (reset_wait>0)
	rest((int)nint(reset_wait*THOUSAND));

    } else {
      nreads = 1;
      nsamples = 1;
    }

    dc->exposure_done = false;

    // Loop for a number of reads
    for (r=0; r<nreads; r++) {

      if (dc->debug & DEBUG_ESSENTIAL)
	dc->message_write("Image readout: Readout set %d of %d", r+1, nreads);

      // Stop if STOP command received before starting the read
      if (dc->stop_readout)
	break;

      // For 2nd and subsequent reads wait here for read interval period before starting the
      // next read
      if (dc->camera.type == IR_CAMERA) {
	if (r>0) {
	  wtime = nint((dc->ir_einfo.readIntvl - (epicsTime::getCurrent() - rtime)/(double)timestamp_res())*THOUSAND);
	  if (dc->debug & DEBUG_ESSENTIAL)
	    dc->message_write("Image readout: waiting %d msec", wtime);

	  while (wtime > DUMMY_MIN_WAIT) {
	    rest(DUMMY_MIN_WAIT);
	    wtime = nint((dc->ir_einfo.readIntvl - (epicsTime::getCurrent() - rtime)/(double)timestamp_res())*THOUSAND);
	  }
	  if (wtime > 0)
	    rest(wtime);
	}
	dc->read_running = dc->readout_no;
      }

      // Now take the number of samples required
      for (s=0; s<nsamples; s++) {

	if (dc->controller_abort)
	  break;

	if (dc->debug & DEBUG_ESSENTIAL)
	  dc->message_write("Image readout: Readout sub-sample %d of %d", s+1, nsamples);

	// Wait for mutex before proceeding to initialise clockout
	image_transfer_thr->lock();
	mlock = true;

	// Initialise controller readout parameters
	dc->init_readout(s, r);

	// Unblock waiting calling thread
	image_transfer_thr->signal();

	// Get ready for the dummy clockout
	dc->init_clockout();

	// unlock mutex so that main thread can access vars
	image_transfer_thr->unlock();
	mlock = false;

	// Now perform the clockout
	dc->clockout();
	rtime = epicsTime::getCurrent();

	rt = (rtime - dc->clockout_stime)/(double)timestamp_res();
	dc->camera_statusp->put(READOUT_TIME, &rt, dc->buffer_number, 1);
	snprintf(dc->image_readout_thread_msg, MSGLEN, "Image read %d pixel clockout time %f secs, sleep time %f secs",
		r, dc->ctime/(double)timestamp_res(), dc->sleeptime/THOUSAND);

	if (dc->debug & DEBUG_ESSENTIAL)
	  dc->message_write("Image readout: %s", dc->image_readout_thread_msg);

	// Wait for mutex before proceeding
	image_transfer_thr->lock();
	mlock = true;

	// Unset flag so that slave can proceed
	dc->finalise_readout();

	// update buffer number
	dc->transfer_buffer++;

	if (s+1==nsamples)
	  dc->readouts_done++;

	image_transfer_thr->signal();

	// unlock mutex so that main thread can access vars
	image_transfer_thr->unlock();
	mlock = false;
      }
    }

  }
  catch (Error& ce) {
    dc->message_write("Exception caught in image readout thread: %s", ce.record_error(__FILE__,__LINE__));

    if (!mlock)
	image_transfer_thr->lock();

    // Unset flag so that slave can proceed
    dc->doing_image_transfer = false;
    dc->finished_image_transfer = true;
    dc->handle_reset();
    dc->image_readout_thread_abort = true;

    // unlock mutex now that read done and signal cv
    image_transfer_thr->signal();
    image_transfer_thr->unlock();
    strlcpy(dc->image_readout_thread_msg,ce.record_error(__FILE__,__LINE__),MSGLEN);
  }

  // Before exiting - make sure that camera class has finished with this thread
//   image_transfer_thr->lock();
//   timeout_wait = 0;
//   while (!dc->exposure_done && !dc->controller_abort && (timeout_wait < IMAGE_WAIT_TIMEOUT)) {
//     image_transfer_thr->unlock();
//     timeout_wait += rest(100);
//     image_transfer_thr->lock();
//   }
//   image_transfer_thr->unlock();


  if (dc->debug & DEBUG_ESSENTIAL)
    dc->message_write("Image readout: Done.");
  */
  return NULL;
}
/*
 *+
 * FUNCTION NAME: Controller_Desc setup_dummy_controller
 *
 * INVOCATION: setup_dummy_controller(char* image_file, Fits* fits_image)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > image_file - name of image file to use as controller name
 * > fits - Fits object to use
 *
 * FUNCTION VALUE: Controller_Desc
 *
 * PURPOSE: Setup a dummy controller description based on input FITS file
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Controller_Desc setup_dummy_controller(char* image_file, Fits* fits_image)
{
  Controller_Desc controller;
  int i,j;

  // Number of detectors in dummy camera equals the number of FITS extensions
  controller.n_chips = (fits_image->extend)? fits_image->nextend:1;

  // Modify camera description to match FITS file
  j = (fits_image->extend)? 1:0;
  strlcpy(controller.name,image_file,NAMELEN);
  controller.type = DUMMY;
  for (i=0; i<controller.n_chips; i++) {
    snprintf(controller.chip[i].name, NAMELEN,"FITS_CHIP%d",i);
    controller.chip[i].n_amps = 1;
    controller.chip[i].amp_mask = 1;
    controller.chip[i].cols = fits_image->xt[j].nx;
    controller.chip[i].rows = fits_image->xt[j].ny;
    controller.chip[i].amp[0].xo = 0;
    controller.chip[i].amp[0].yo = 0;
    controller.chip[i].amp[0].xdir = 1;
    controller.chip[i].amp[0].ydir = 1;
    controller.chip[i].amp[0].width = fits_image->xt[j].nx;
    controller.chip[i].amp[0].height = fits_image->xt[j].ny;

    // Use the DETSEC fits header for positioning info - to come...
    // for now align all CHIPs on one row.
    controller.chip_xo[i] = (i==0)? 0:
      controller.chip_xo[i-1]+controller.chip[i-1].amp[0].width;
    controller.chip_yo[i] = 0;
    j++;
  }
  return controller;
}
/*
 *+
 * FUNCTION NAME: dummy_path
 *
 * INVOCATION: dummy_path(const char* name, const char* chip_name)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > name - basename of file
 * > chip_name -
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Construct full pathname for DUMMY 1 configuration files
 *
 * DESCRIPTION:
 * Rules:
 * 1. If environment variable DUMMY_DIR available then it is used
 * 2. else constant DUMMY_DIR is relative to CICADA_HOME
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
char* dummy_path(const char* name, const char* chip_name)
{
  static Filenametype path_buf;
  char *cp;
  // Use static buffer for storing generated path.
  char *fname = path_buf;

  if ((cp = getenv("DUMMY_DIR")) == NULL) {
    strlcpy(fname,DUMMY_DIR,FILENAMELEN);
  } else {
    strlcpy(fname,cp,FILENAMELEN);
  }

  if (fname[strlen(fname)-1] != '/')
    strlcat(fname,"/",FILENAMELEN);

  // name might be specific to controller setup
  if (strlen(chip_name)>0) {
    strlcat(fname,chip_name,FILENAMELEN);
    strlcat(fname,"/",FILENAMELEN);
  }
  strlcat(fname,name,FILENAMELEN);
  return fname;
}

/*
 *+
 * FUNCTION NAME: dummyControllerTest
 *
 * INVOCATION: dummyControllerTest()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: SImple test harness for dummy controller
 *
 * DESCRIPTION:
 * Performs following tests of Dummy_Controller:
 * 1.
 *
 * EXTERNAL VARIABLES: None.
 *
 * PRIOR REQUIREMENTS: None.
 *
 * DEFICIENCIES:
 *
 *-
 */
static void dummyControllerTest_task(void*);
void dummyControllerTest()
{
  epicsThreadId tid;

  // Start this in a spawned task with an appropriately sized stack
  tid = epicsThreadCreate("dummyTest", epicsThreadPriorityMedium, epicsThreadGetStackSize(epicsThreadStackBig),
		  (EPICSTHREADFUNC)dummyControllerTest_task, NULL);

  if (tid == 0) {
    errlogMessage("taskSpawn of dummyControllerTest_task failed");
  }
}

static void dummyControllerTest_task(void *parm)
{
  (void)parm; // Ignore
  Controller *controller=NULL;
  Instrument_Status *st=NULL;
  Config *cf=NULL;
  Camera_Desc cam;
  shared_ptr<Semaphore> cs;
  Parameter *ip=NULL;
  Parameter cp;
  Parameter op;
  Detector_Regions *rgn;
  Dummy_Setup setup;
  Camera_Table ctable;
  Filenametype fname;
  string ph;
  string name;
  string cdir;
  double sum,min,max;
  unsigned long asize;
  int n;
  Region_Data regions;

  try {
    cout << "Running Dummy Controller class tests." << endl;

    // read in the camera configuration table
    Platform_Specific specific;
    strlcpy(fname,CAMERA_TABLE,FILENAMELEN);
    ctable.read(fname);

    name = "DummyGNIRS";

    cam = ctable[name.c_str()];

    cout << "Camera configuration " << name << " found ok" << endl;

    cout << "Creating dummy controller with simple constructor ..." << endl;
    controller = new Dummy_Controller(cam, 0, 0, &specific);
    delete controller;
    cout << " Done" << endl;

    cout << "Creating dummy controller with parameter only constructor ..." << endl;
    ph = "maestro";
    st = new Instrument_Status;
    cf = new Config;

    st->id = 0;
    st->pid = 0;
    controller = new Dummy_Controller(cam, 0, 0, ph.c_str(), st, &specific);
    cdir = controller->controller_dir;
    delete controller;
    cout << " Done" << endl;

    cout << "Setting up for full dummy constructor " << endl;
    // Semaphores
    cs = Semaphore::create("DummySlaveCrt");
    cf->config_data.Config_Data_u.camera.desc = cam;
    cf->config_data.Config_Data_u.camera.cid = 0;
    ip = new Parameter();
    st->hardware_status.Hardware_Status_u.camera_status.controller_status.type = DUMMY;

    cout << "Reading dummy setup file " << dummy_path(DUMMY_SETUP,cdir.c_str()) << endl;
    setup.read(dummy_path(DUMMY_SETUP,cdir.c_str()));
    cf->config_data.Config_Data_u.camera.controller.Controller_Config_u.dummy.config_data = setup.config;
    cout << " Done" << endl;

    cout << "Creating dummy controller with full status and config structures ..." << endl;
    controller = new Dummy_Controller(st, ip, &cp, &op, 0, &specific);
    cout << " Done" << endl;

    // Test setting up an image readout buffer
    regions.xo[0] = 0;
    regions.yo[0] = 0;
    regions.width[0] = cam.controller[0].cols;
    regions.height[0] = cam.controller[0].rows;
    regions.rcf = regions.ccf = 1;
    cout << "Creating readout region ..." << endl;
    rgn = new Detector_Regions(cam, 0, true, MOSAIC_NONE, true, 1, regions.xo, regions.width,
			       regions.yo, regions.height, 1, 1, 0, 0, 255);
    cout << " Done" << endl;

    Detector_Readout readout_rec;
    readout_rec.regions = regions;
    asize = 0x800000;
    cout << "Setting up readout ..." << endl;
    IR_Exposure_Info einfo;

    controller->setup_readout(1, &readout_rec, rgn, einfo, asize, 1);
    cout << " Done" << endl;

    cout << "Creating an image buffer for 2x8MB images ..., actual im size=" << controller->actual_image_size << endl;
    controller->create_image_buffer(0x1000000);
    cout << " Done" << endl;

    // Now test setting up a verify image, first create a default readout regions spec
    cout << "Setting up verify data ..." << endl;
    controller->setup_verify_data(0);
    cout << "Done." << endl;

    cout << "Starting an image transfer thread ..." << endl;

    // Now start the image readout thread and do a readout.
    if (cam.type == IR_CAMERA) {
//      controller->ir_einfo.nperiods = 0;
//      controller->ir_einfo.nfowler = 1;
      controller->readout_no = 0;
    }
    controller->start_image_transfer_thread();

    cout << "Image transfer thread started - waiting fot it to complete " << endl;
    if (controller->image_transfer_thr != NULL)
      delete controller->image_transfer_thr;
    cout << "Done!" << endl;

    cout << "Computing stats on image readout buffer" << endl;
    n = regions.width[0]*regions.height[0];

    imstat(controller->image_buffer_ptr, n, controller->image_info.bitpix, sum, min, max);
    cout << "Full image mean =" << sum/n << ", max=" << max << ", min=" << min << ", bitpix=" <<
      controller->image_info.bitpix << ", n=" << n << endl;

    if (rgn) delete rgn;
    if (controller) delete controller;
    if (st) delete st;
    if (cf) delete cf;
    if (ip) delete ip;
    cout << "All Done" << endl;
  }
  catch (Error &e) {
    e.print_error(__FILE__,__LINE__);
  }

}

