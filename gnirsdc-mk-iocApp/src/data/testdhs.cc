/*
/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 * $Id
 * GNIRS PROJECT
 * 
 * FILENAME 
 *   testdhs.cc
 * 
 * GENERAL DESCRIPTION
 *   Program to provide a simple test of the DHS system with timings.
 *   Sends data of same size of GNIRS array to DHS, ie 4 1kx1k quadrants of
 *   intensity, variance and quality pixels. Optionally sends data in 
 *   a set of small chunks
 *
 * ORIGINAL AUTHOR: 
 *   Peter Young
 *
 * HISTORY

 *INDENT-ON
 *
 * $Log: testdhs.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Gnirs; first gemini-modified release
 *
 * Revision 1.1  2004/03/19 01:51:52  pjy
 * Added
 *
 *
 -------------------------------------------------------------------------*/
#ifdef vxWorks
#include <vxWorks.h>
#endif

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/

#ifdef vxWorks
#include <stdioLib.h>
#include <sysLib.h>
#include <timers.h>
#else
#include <stdio.h>
#define printErr printf
#endif
#include <time.h>
#include "dhs.h"

/*---------------------------------------------------------------------------
 External functions
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 External variables
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Static variables
 -------------------------------------------------------------------------*/

static DHS_CONNECT dhsConnection;  // DHS connection pointer

/*--------------------------------------------------------------------------
 Global variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Class definitions
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Non-member functions
 -------------------------------------------------------------------------*/

static void checkDhs(const char* label, int dhsResult, const char* fname, int lineno);
static void dhsErrorCallback(DHS_CONNECT,DHS_STATUS,DHS_ERR_LEVEL,char *,DHS_TAG,void *);
static void sendDhs(char* label, DHS_BD_DATASET dataSet, DHS_BOOLEAN last);

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: testDhsInit()
 * 
 * INVOCATION: testDhsInit
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Initialises a connection to the DHS
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void testDhsInit(char *hostip, char *server)
{
  DHS_STATUS dhsResult;              // DHS library status value
  DHS_THREAD dhsThreadId;            // DHS thread ID. 
  bool eventLoop = false;             // set TRUE if eventLoop is started
//    const char hostip[] = "150.203.89.131";
    
  try {
    printErr("In dhsInitTest...\n");  

    dhsResult = DHS_S_SUCCESS;

    // Now initialise the DHS library with space for 1 connection
    printErr("calling dhsInit...\n");  
    dhsInit("gnirsData", 10, &dhsResult);
    checkDhs("dhsInit", dhsResult, __FILE__, __LINE__);
    
    // Set up callback functions for errors and puts
    printErr("calling dhsCallbackSet...\n");  
    dhsCallbackSet(DHS_CBT_ERROR, (DHS_CB_FN_PTR) &dhsErrorCallback, &dhsResult);
    checkDhs("dhsCallbackSet:ErrorCallback", dhsResult, __FILE__, __LINE__);
    
    // Start the eventloop
    printErr("calling dhsEventLoop...\n");  
    dhsEventLoop(DHS_ELT_THREADED, &dhsThreadId, &dhsResult);
    checkDhs("dhsEventLoop", dhsResult, __FILE__, __LINE__);
    eventLoop = true;
    
    // Make a connection to the DHS data server    
    printErr("calling dhsConnect with IP=%s, name=%s\n",hostip, server);  
    dhsConnection = dhsConnect(hostip, server, NULL, &dhsResult);
    checkDhs("dhsConnect", dhsResult, __FILE__, __LINE__);
    printErr("connection made = %d\n",dhsConnection);
  }
  catch (int& dce) {
    printErr("dhsInit caught error at line %d...\n",dce);  
    if (eventLoop) dhsEventLoopEnd(&dhsResult);
    dhsExit(&dhsResult);
  }
  
  printErr("dhsInitTest done...\n");  

}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: transferDhs()
 * 
 * INVOCATION: transferDhs
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Transfers a data set to the DHS
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void transferDhs(unsigned long rows, unsigned long cols, int frames, int chunks)
{
  DHS_STATUS dhsResult;              // DHS library status value
  char *contrib[1];                  // List of contributors to the data
  char* dataLabel;                   // Unique name returned by DHS
  char *qlStreams[2];                // List of streams to send data to
  char label[50];                    // Dataset name
  DHS_BD_DATASET hdr_dataSet;        // Id of the dataset for headers
  DHS_BD_DATASET dataSet;             // Id of the dataset
  DHS_BD_FRAME intensity[4];         // intensity frames for each quadrant
  unsigned long axisSize[2];         // Size of a frame
  unsigned long dims[1];             // Array to contain data dimensions
  char *axisLabel[2];                // Labels for each axis
  float *intensityData[4];
  float *varianceData[4];
  float *junk;
  unsigned char *qualityData[4];
  long origin[2];                    // Origin of frame

  DHS_BD_FRAME variance[4];          // variance frames for each quadrant
  DHS_BD_FRAME quality[4];           // quality frames for each quadrant

  unsigned int i,j,k,lk,r,srows;
  struct timespec tps,tpf;           // Timestamps
  double ts,tf;                      // times in seconds
  int status,q;
  unsigned long xOrigin[4] = {1, cols+1, 1, cols+1};
  unsigned long yOrigin[4] = {1, 1, rows+1, rows+1};
  bool do_quality = false;
  bool do_variance = false;
  bool do_ql = false;
  DHS_BOOLEAN last = DHS_FALSE;
  char fname[32];

  try {
    printErr("In transferDhs...\n"); 

    if (frames>4) {
      printErr("Cannot handle %d frames - max is 4\n", frames); 
      return;
    }
    
    // Get timestamp
    status = clock_gettime(CLOCK_REALTIME,&tps);

    dhsResult = DHS_S_SUCCESS;
    
    // Get a unique name for this dataset from the DHS
    dataLabel = dhsBdName(dhsConnection, &dhsResult);
    checkDhs("dhsBdName", dhsResult, __FILE__, __LINE__);
    
    printErr("got dataSet label: %s\n", dataLabel);  

    // Set up the control information for the dataset
    dhsBdCtl(dhsConnection, DHS_BD_CTL_LIFETIME, dataLabel, DHS_BD_LT_PERMANENT, &dhsResult);
    checkDhs("dhsbdCtl:DHS_BD_CTL_LIFETIME", dhsResult, __FILE__, __LINE__);

    contrib[0] = (char*)"GNIRS";
    dhsBdCtl(dhsConnection, DHS_BD_CTL_CONTRIB, dataLabel, 1, contrib, &dhsResult);
    checkDhs("dhsbdCtl:DHS_BD_CTL_CONTRIB", dhsResult, __FILE__, __LINE__);

    if (do_ql) {
      qlStreams[0] = (char*)"gnirsScience";
      dhsBdCtl(dhsConnection, DHS_BD_CTL_QLSTREAM, dataLabel, 1, qlStreams, &dhsResult);
      checkDhs("dhsbdCtl:DHS_BD_CTL_QLSTREAM", dhsResult, __FILE__, __LINE__);
    }
   
    printErr("Control information setup ok...\n");  

    // Create a new dataset for transferring FITS header information
    hdr_dataSet = dhsBdDsNew(&dhsResult);
    checkDhs("dhsBdDsNew", dhsResult, __FILE__, __LINE__);
    
    printErr("Created a new dataset...\n");  

    // Fill in the dataset attributes for main header
    dhsBdAttribAdd(hdr_dataSet, "instrument", DHS_DT_STRING, 0, NULL, "GNIRS", &dhsResult);
    checkDhs("dhsBdAttribAdd:instrument", dhsResult, __FILE__, __LINE__);

    sprintf(fname,"[%d:%ld,%d:%ld]",1,cols*2,1,rows*2);
    dhsBdAttribAdd(hdr_dataSet, "DETSIZE", DHS_DT_STRING, 0, NULL, fname, &dhsResult);
    checkDhs("dhsBdAttribAdd:DETSIZE", dhsResult, __FILE__, __LINE__);

    dhsBdAttribAdd(hdr_dataSet, "NCCDS", DHS_DT_INT32, 0, NULL, 1, &status);
    checkDhs("dhsBdAttribAdd:NCCDS", dhsResult, __FILE__, __LINE__);

    dhsBdAttribAdd(hdr_dataSet, "NAMPS", DHS_DT_INT32, 0, NULL, frames, &status);
    checkDhs("dhsBdAttribAdd:NAMPS", dhsResult, __FILE__, __LINE__);

    // Create DHS frames for each of the image frames - these contain no data
    // but do contain hdrs for the IHUs
    axisSize[0] = cols;
    axisSize[1] = rows;
    sprintf(label, "%s.0.0", dataLabel);
    for (q=0; q<frames; q++) {
      // Following call copied out of the GMOS DC code from Richard Wolfe
      // I don't really understand this, other than I realise that these header
      // frames cannot created without allocating some space.
      dims[0] = 4;
      intensity[q] = dhsBdFrameNew(hdr_dataSet, "Intensity", q, DHS_DT_FLOAT,
				   1, dims, (const void **) &junk, &dhsResult);

      // Now add the basic IHU information 
      dims[0] = 2;
      sprintf(fname, "%d", q);
      dhsBdAttribAdd(intensity[q], "frameId", DHS_DT_STRING, 0, NULL, fname, &dhsResult);
      checkDhs("dhsBdAttribAdd:frameId", dhsResult, __FILE__, __LINE__);

      dhsBdAttribAdd(intensity[q], "dataType", DHS_DT_STRING, 0, NULL, "Intensity", &dhsResult);
      checkDhs("dhsBdAttribAdd:dataType", dhsResult, __FILE__, __LINE__);

      dhsBdAttribAdd(intensity[q], "units", DHS_DT_STRING, 0, NULL, "Photons", &dhsResult);
      checkDhs("dhsBdAttribAdd:units", dhsResult, __FILE__, __LINE__);

      dhsBdAttribAdd(intensity[q], "axisSize", DHS_DT_UINT32, 1, dims, axisSize, &dhsResult);
      checkDhs("dhsBdAttribAdd:axisSize", dhsResult, __FILE__, __LINE__);

      sprintf(fname,"[%d:%ld,%d:%ld]",1,cols*2,1,rows*2);
      dhsBdAttribAdd(hdr_dataSet, "CCDSIZE", DHS_DT_STRING, 0, NULL, fname, &dhsResult);
      checkDhs("dhsBdAttribAdd:CCDSIZE", dhsResult, __FILE__, __LINE__);

      dhsBdAttribAdd(hdr_dataSet, "CCDSEC", DHS_DT_STRING, 0, NULL, fname, &dhsResult);
      checkDhs("dhsBdAttribAdd:CCDSEC", dhsResult, __FILE__, __LINE__);

      dhsBdAttribAdd(hdr_dataSet, "DETSEC", DHS_DT_STRING, 0, NULL, fname, &dhsResult);
      checkDhs("dhsBdAttribAdd:DETSEC", dhsResult, __FILE__, __LINE__);

      dhsBdAttribAdd(hdr_dataSet, "DATASEC", DHS_DT_STRING, 0, NULL, fname, &dhsResult);
      checkDhs("dhsBdAttribAdd:DATASEC", dhsResult, __FILE__, __LINE__);
    }

    // Now send the header data and wait for the DHS to complete. Indicate that
    // this is not the last data that will be sent
    sendDhs(label, hdr_dataSet, DHS_FALSE);

    // Free the header dataset
    dhsBdDsFree(hdr_dataSet, &dhsResult);    
  
    // Send the data in row based chunks
    srows = rows/chunks;
    for (r=0; r<rows; r+=srows) {

      // make sure we don't overrun
      if ((r+srows) > rows)
	srows = rows - r;

	// Create a new chunk dataset
	dataSet = dhsBdDsNew(&dhsResult);
	checkDhs("dhsBdDsNew", dhsResult, __FILE__, __LINE__);
	printErr("Created a new dataset for rows %d - %d ...\n",r,r+srows);  

	// Add a frame for each quadrant of the detector
	for (q=0; q<frames; q++) {
	  
	  // Create the intensity frame
	  axisSize[0] = srows;
	  axisSize[1] = cols;
	  intensity[q] = dhsBdFrameNew(dataSet, "Intensity", q+frames, DHS_DT_FLOAT, 2, axisSize,
				       (const void **) &intensityData[q], &dhsResult);
	  checkDhs("dhsBdFrameNew:intensity", dhsResult, __FILE__, __LINE__);
      
	  // Fill in the frame attributes
	  origin[0] = 1;
	  origin[1] = r;
	  sprintf(fname, "%d", q+frames);
	  dhsBdAttribAdd(intensity[q], "frameId", DHS_DT_STRING, 0, NULL, fname, &dhsResult);
	  checkDhs("dhsBdAttribAdd:frameId", dhsResult, __FILE__, __LINE__);

	  dhsBdAttribAdd(intensity[q], "origin", DHS_DT_INT32, 1, dims, origin, &dhsResult);
	  checkDhs("dhsBdAttribAdd:origin", dhsResult, __FILE__, __LINE__);
	  printErr("Created intensity frame %d\n", q);  
	  
	  // Put the intensity data into the data array
	  for (i=0; i<axisSize[0]; i++) {
	    for (j=0; j<axisSize[1]; j++) {
	      k = (i*cols)+j;
	      intensityData[q][k] = (float) (k+lk)*(q+1);
	    }
	  }
	  
	}

	lk = k;
	if (r+srows>=rows)
	  last = DHS_TRUE;
	
	printErr("DataSet size=%ld\n",dhsBdDsSize(dataSet,&dhsResult));
	
	sendDhs(label, dataSet, last);

	dhsBdDsFree(dataSet, &dhsResult);    
	checkDhs("dhsBdDsFree", dhsResult, __FILE__, __LINE__);
    }

     // Create the variance sub frame
    if (do_variance) {
      sprintf(label, "%s.sci.1:%d.2", dataLabel, q+1);
      
      variance[q] = dhsBdFrameNew(intensity[q], "variance", 1, DHS_DT_FLOAT, 2, axisSize,
				  (const void **) &varianceData[q], &dhsResult);
      checkDhs("dhsBdFrameNew:variance", dhsResult, __FILE__, __LINE__);
      
      // Fill in the variance frame attributes
      dhsBdAttribAdd(variance[q], "dataType", DHS_DT_STRING, 0, NULL, "Variance", &dhsResult);
      checkDhs("dhsBdAttribAdd:dataType", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(variance[q], "units", DHS_DT_STRING, 0, NULL, "Photon rate", &dhsResult);
      checkDhs("dhsBdAttribAdd:units", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(variance[q], "origin", DHS_DT_INT32, 1, dims, origin, &dhsResult);
      checkDhs("dhsBdAttribAdd:origin", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(variance[q], "axisSize", DHS_DT_UINT32, 1, dims, axisSize, &dhsResult);
      checkDhs("dhsBdAttribAdd:axisSize", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(variance[q], "axisLabel", DHS_DT_STRING, 1, dims, axisLabel, &dhsResult);
      checkDhs("dhsBdAttribAdd:axisLabel", dhsResult, __FILE__, __LINE__);
      
      printErr("Created variance subframe %d\n", q);  
      
      // Put the variance data into the data array
      for (i=0; i<axisSize[0]; i++)
	for (j=0; j<axisSize[1]; j++) {
	  k = (i*axisSize[1])+j;
	  varianceData[q][k] = (float) k;
	}
    }
    
    // Create the qualitysub frame
    if (do_quality) {
      sprintf(label, "%s.sci.1:%d.3", dataLabel, q+1);
      
      quality[q] = dhsBdFrameNew(intensity[q], "quality", 2, DHS_DT_INT8, 2, axisSize,
				 (const void **) &qualityData[q], &dhsResult);
      checkDhs("dhsBdFrameNew:quality", dhsResult, __FILE__, __LINE__);
      
      // Fill in the quality frame attributes
      dhsBdAttribAdd(quality[q], "dataType", DHS_DT_STRING, 0, NULL, "Quality", &dhsResult);
      checkDhs("dhsBdAttribAdd:dataType", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(quality[q], "units", DHS_DT_STRING, 0, NULL, "NDR", &dhsResult);
      checkDhs("dhsBdAttribAdd:units", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(quality[q], "origin", DHS_DT_INT32, 1, dims, origin, &dhsResult);
      checkDhs("dhsBdAttribAdd:origin", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(quality[q], "axisSize", DHS_DT_UINT32, 1, dims, axisSize, &dhsResult);
      checkDhs("dhsBdAttribAdd:axisSize", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(quality[q], "axisLabel", DHS_DT_STRING, 1, dims, axisLabel, &dhsResult);
      checkDhs("dhsBdAttribAdd:axisLabel", dhsResult, __FILE__, __LINE__);
      
      printErr("Created quality subframe\n");  
      
      // Put the variance data into the data array
      for (i=0; i<axisSize[0]; i++)
	  for (j=0; j<axisSize[1]; j++) {
	    k = (i*axisSize[1])+j;
	    qualityData[q][k] = 1;
	  }
    }   
    
    // Now report time taken for full operation
    status = clock_gettime(CLOCK_REALTIME,&tpf);
    tf = (double)tpf.tv_sec + tpf.tv_nsec/1000000000.0;
    ts = (double)tps.tv_sec + tps.tv_nsec/1000000000.0;
    printErr("Time for DHS transfer= %e seconds, tpf=%d,tps=%d\n", tf-ts, tpf.tv_sec, tps.tv_sec);
  }
  catch (int& dce) {
    printErr ("TransferDhs failure at line %d\n", dce);
  }
  printErr("Done transfer to DHS\n");        
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: checkDhs
 * 
 * INVOCATION: checkDhs(char* label, int dhsResult, char* fname, int lineno)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Checks DHS status value and reports if error
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
static void sendDhs(char* label, DHS_BD_DATASET dataSet, DHS_BOOLEAN last)
{
  DHS_TAG putTag;                    // tag returned by the put function
  DHS_CMD_STATUS sendStatus;         // Final status of the put
  DHS_STATUS dhsResult;              // DHS library status value
  char *msg;                         // failure message from server

  try {
    // Send the data to the DHS server. If this is the last data for the dataset, the last
    // parameter is set to TRUE.
    dhsResult = DHS_S_SUCCESS;
    putTag = dhsBdPut(dhsConnection, label, DHS_BD_PT_DS, last, dataSet, NULL, &dhsResult);
    checkDhs("dhsBdPut", dhsResult, __FILE__, __LINE__);
    
    printErr("Sent data to the DHS server for label %s\n", label);  
    
    // Wait for the data to be received
    dhsWait(1, &putTag, &dhsResult);    
    checkDhs("dhsWait", dhsResult, __FILE__, __LINE__);
    
    sendStatus = dhsStatus(putTag, &msg, &dhsResult);    
    checkDhs("dhsStatus", dhsResult, __FILE__, __LINE__);
    
    if (sendStatus != DHS_CS_DONE) {
      printErr ("DHS put failed: connection=%d label=%s\n",
		(int) dhsConnection, label);
      if (msg != NULL)
	printErr ("DHS put reason: %s\n", msg);
    } else
      printErr("Data sent OK\n");        
    
    // Free tag and data
    dhsTagFree(putTag, &dhsResult);    
    checkDhs("dhsTagFree", dhsResult, __FILE__, __LINE__);
  }
  catch (int& dce) {
    printErr ("TransferDhs failure at line %d\n", dce);
    dhsTagFree(putTag, &dhsResult); 
    throw dce;
  }
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: checkDhs
 * 
 * INVOCATION: checkDhs(char* label, int dhsResult, char* fname, int lineno)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Checks DHS status value and reports if error
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
static void checkDhs(const char* label, int dhsResult, const char* fname, int lineno)
{
  if (dhsResult != DHS_S_SUCCESS) {
    printErr("Error in DHS call %s, status=%d\n", label, dhsResult);
    throw lineno;
  }
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: dhsErrorCallback
 * 
 * INVOCATION: dhsErrorCallback(DHS_CONNECT     connect,
			  DHS_STATUS      errorNum,
			  DHS_ERR_LEVEL   errorLev,
			  char *          msg,     
			  DHS_TAG         tag,     
			  void *          userData)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Callback for DHS to use on error
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
static void dhsErrorCallback(DHS_CONNECT     connect,
			     DHS_STATUS      errorNum,
			     DHS_ERR_LEVEL   errorLev,
			     char *          msg,     
			     DHS_TAG         tag,     
			     void *          userData)
{
   printErr ("DHS error callback: connection=%d errNum=%d level=%d \"%s\"\n",
             (int) connect, (int) errorNum, (int) errorLev, msg);
}
// Get a dataset name
void dhsName()
{
  DHS_STATUS dhsResult;              // DHS library status value
  char* dataLabel;                   // Unique name returned by DHS

  try {
    printErr("In dhsName...\n"); 
    
    // Get a unique name for this dataset from the DHS
    dataLabel = dhsBdName(dhsConnection, &dhsResult);
    checkDhs("dhsBdName", dhsResult, __FILE__, __LINE__);
    
    printErr("datalabel = %s", dataLabel);
  }
  catch (int& dce) {
    printErr("dhsName caught error at line %d...\n",dce);  
  }
}
#ifndef vxWorks
int main() 
{
  testDhsInit((char*)"150.203.89.131", (char*)"dataServerNS");
  transferDhs(1024,1024,4,4);
  return 0;
   
}
#endif
