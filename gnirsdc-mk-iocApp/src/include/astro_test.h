/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: astro_test.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GSAOI PROJECT
 * 
 * FILENAME astro_test.h
 * 
 * GENERAL DESCRIPTION 
 *
 * This is a VxWorks version of the device driver to communicate with
 * the SDSU  interface board. 

 * This file contains private implementation details for VxWorks device driver
 * test routines. This is based on the structures used in the mreadout_vme.h code
 * by Scott Streit.

 * Original Author: Scott Streit (SDSU) - Solaris version 1.0 - 12 Nov 1999
 * Ported by: peter Young (RSAA) - For the Gemini Telescope Project (GNIRS) - January 2004
 *
 * $Log: astro_test.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.3  2005/06/07 06:10:04  pjy
 * Added do_verification
 *
 * Revision 1.2  2005/04/27 03:40:27  pjy
 * PCI DSP working
 *
 * Revision 1.1  2005/02/25 04:47:45  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.1  2004/12/22 22:43:19  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.3  2004/12/22 06:55:54  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.2  2004/08/20 02:18:25  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.1  2004/04/25 02:03:33  pjy
 * Pre T2.2 version
 *
 *   
 */
#ifndef __ASTRO_TEST_H__
#define __ASTRO_TEST_H__

/* Typedefs */
/* Boolean constants */
#define FALSE		0
#define TRUE		1

/************************************************* 
*        Define General Constants 
*************************************************/ 
#define UNDEFINED	-1 
#define MAX_DATA         6 
 
/****************************************************************************** 
*	Vector Commands 
******************************************************************************/ 
#define READ_PIXEL_COUNT		0x8075 
#define PCI_PC_RESET			0x8077 
#define ABORT_READOUT			0x8079 
#define BOOT_EEPROM			0x807B 
#define READ_NUMBER_OF_FRAMES_READ      0x807D 
#define READ_HEADER			0x81 
#define RESET_CONTROLLER		0x87 
#define WRITE_NUM_IMAGE_BYTES		0x8F 
#define INITIALIZE_IMAGE_ADDRESS	0x91 
#define WRITE_COMMAND			0xB1 
 
/*
 * DSP Replies
 */
#define DON	 0x00444F4E
#define RDR	 0x00524452
#define ERR	 0x00455252
#define SYR	 0x00535952
#define TIMEOUT	 0x544F5554 /* TOUT */
#define READOUT	 0x524F5554 /* ROUT */
 
/************************************************* 
*        Controller configuration bit masks. 
*************************************************/ 
#define VIDEO_PROCESSOR_MASK		0x000007 
#define TIMING_BOARD_MASK		0x000018 
#define UTILITY_BOARD_MASK		0x000060 
#define SHUTTER_MASK			0x000080 
#define TEMPERATURE_READOUT_MASK	0x000300 
#define SUBARRAY_READOUT_MASK		0x000400 
#define BINNING_MASK			0x000800 
#define READOUT_MASK			0x003000 
#define MPP_MASK			0x004000 
#define CLOCK_DRIVER_MASK		0x018000 
#define SPECIAL_MASK			0x0E0000 
 
/************************************************* 
*        Define Controller Configuration Bits 
*************************************************/ 
#define CCD_REV3B		0x000000 
#define CCD_GENI		0x000001 
#define CCD			0x000000 
#define IR_REV4C		0x000002 
#define IR_COADDER		0x000003 
#define CLOCK_DRIVER		0x000000 
#define TIM_REV4B		0x000000 
#define TIM_GENI		0x000008 
#define NO_UTIL_BOARD		0x000000 
#define UTILITY_REV3		0x000020 
#define SHUTTER			0x000080 
#define NO_TEMPERTURE_CONTROL	0x000000 
#define NONLINEAR_TEMP_CONV	0x000100 
#define LINEAR_TEMP_CONV	0x000200 
#define SUBARRAY		0x000400 
#define BINNING			0x000800 
#define SERIAL			0x001000 
#define PARALLEL		0x002000 
#define BOTH_READOUTS		0x003000 
#define MPP_CAPABLE		0x004000 
#define SDSU_MLO		0x020000 
#define NGST			0x040000 
#define CONTINUOUS_READOUT	0x100000 
#define NIRIM			(IR_REV4C | TIM_GENI) 
#define DEFAULT_CONFIG_WORD	(NONLINEAR_TEMP_CONV | TIM_REV4B | UTILITY_REV3 | SHUTTER);  /* 0x0001A0 */ 
 
/************************************************* 
*	Readout modes 
*************************************************/ 
#define A_AMP	0x5F5F41	/* Ascii __A amp A.					*/ 
#define B_AMP	0x5F5F42	/* Ascii __B amp B.					*/ 
#define C_AMP	0x5F5F43	/* Ascii __C amp C.					*/ 
#define D_AMP	0x5F5F44	/* Ascii __D amp D.					*/ 
#define L_AMP	0x5F5F4C	/* Ascii __L left amp.				*/ 
#define R_AMP	0x5F5F52	/* Ascii __R left amp.				*/ 
#define LR_AMP	0x5F4C52	/* Ascii _LR right two amps.		*/ 
#define AB_AMP	0x5F4142	/* Ascii _AB top two amps A & B.	*/ 
#define CD_AMP	0x5F4344	/* Ascii _CD bottom two amps C & D. */ 
#define ALL_AMP	0x414C4C	/* Ascii ALL four amps (quad).		*/ 
 
/************************************************* 
*	Define gain and speed constants 
*************************************************/ 
#define ONE		1 
#define TWO		2 
#define FIVE	5 
#define TEN		10 
#define SLOW_READOUT	0 
#define FAST_READOUT	1 
 
/************************************************* 
*	Define shutter positions 
*************************************************/ 
#define  _OPEN_SHUTTER_ 	(1 << 11) 
#define  _CLOSE_SHUTTER_ 	~(1 << 11) 
 
/************************************************* 
*        Define command replies 
*************************************************/ 
#define TOUT	0x544F5554 
#define DON     0x00444F4E 
#define ERR		0x00455252 
#define SYR     0x00535952 
#define RST		0x00525354 
 
/************************************************* 
*	Define Readout Constants 
*************************************************/ 
#define READ_TIMEOUT	200 
 
/****************************************************************************** 
*	Board Id Constants 
******************************************************************************/ 
#define INTERFACE_ID	1 
#define TIM_ID		2 
#define UTIL_ID		3 
 
/****************************************************************************** 
*	Memory Location Id Constants 
*		R	(Bit 20)  ROM 
*		P	(Bit 21)  DSP program memory space 
*		X	(Bit 22)  DSP X memory space 
*		Y	(Bit 23)  DSP Y memory space 
******************************************************************************/ 
#define P	0x100000 
#define X 	0x200000 
#define Y	0x400000 
#define R	0x800000 
 
/****************************************************************************** 
*  Masks to set the Host Control Register HCTR. 
* 
*       Only three bits of this register are used. Two are control bits to set 
*  the mode of the PCI board (bits 8 and 9)  and  the  other (bit 3) is a flag 
*  indicating the progress of image data transfer to the user's application. 
* 
*       Bit 3   = 1     Image buffer busy transferring to user space. 
*               = 0     Image buffer not  transferring to user space. 
* 
*       Bit 8= 0 & Bit 9= 1   PCI board set to slave mode for PCI file download. 
*       Bit 8= 0 & Bit 9= 0   PCI board set to normal processing. 
* 
*       Note that the HTF_MASK, sets the HTF bits 8 and 9 to transfer mode. 
* 
******************************************************************************/ 
#define HTF_MASK        0x200 
#define HTF_CLEAR_MASK  0xFFFFFCFF 
#define BIT3_CLEAR_MASK 0xFFFFFFF7 
#define BIT3_SET_MASK   0x00000008 
#define HTF_BITS	0x00000038 

/******************************************************************************* 
*  Wildcards in the dsp file. 
*******************************************************************************/ 
#define SEARCH_STRING   "_DATA P" 
#define END_STRING      "_END" 
 
/******************************************************************************* 
*  A valid start address must be less than 0x4000 for the load DSP file in 
*  timming or utility boards. 
*******************************************************************************/ 
#define MAX_DSP_START_LOAD_ADDR	0x4000 
 
/******************************************************************************* 
*  For check number of words is less 4096 (0x1000), for the interface boot file. 
*******************************************************************************/ 
#define MAX_WORD_NUM_FILE	0x1000 
 
/******************************************************************************* 
*  The valid start address to load a into the interface board. 
*******************************************************************************/ 
#define LOAD_START_ADDR	0x0000 
 

/******************************************************************************
	Define Deinterlace Modes 
*******************************************************************************/ 
#define NO_DEINTERLACE		0 
#define SPLIT_PARALLEL      	1 
#define SPLIT_SERIAL    	2 
#define QUAD_CCD		3 
#define QUAD_IR			4 
#define CDS_QUAD_IR		5 

/* max TDL tests */
#define HARDWARE_DATA_MAX     1000000

const int SDSU_ALIGN = 0x400;                /* align buffers to 1024 byte boundary*/


/* CONTROLLER SETUP STRUCTURE */
typedef struct {
  int do_simulation;
  int do_interface_file;
  int do_reset;
  int do_hardware_test;
  int do_interface_hardware_test;
  int do_tim_hardware_test;
  int do_util_hardware_test;
  int do_tim_app;
  int do_tim_file;
  int do_util_app;
  int do_util_file;
  int do_power_on;
  int do_dimensions;
  int do_testpattern;
  int do_verification;
  int bits_per_pixel;
  char interface_file[80];
  char tim_file[80];
  char util_file[80];
  int tim_app;
  int util_app;
  int num_interface_tests;
  int num_tim_tests;
  int num_util_tests;
  int cols;
  int rows;
  int nframes;
  int controllerMaster;
  int testpattern_const;
  int testpattern_seed;
  int testpattern_inc;
  int nreads;
  int read_time;
  int read_interval;
  int nsamples;
  int byteSwapping;
  int namps;
  int nresets;
  int reset_delay;
  int copy_data;
} Astro_Test_Setup;

/* CONTROLLER PARAMETERS STRUCTURE */
typedef struct {
  int configWord;
  int do_mpp;
  int do_readout_mode;
  int do_gain;
  int do_temperature;
  int do_continuousReadout;
  int mpp;
  int amp;
  int gain;
  int speed;
  int temperature;
  char temperature_algorithm[20];
  double linear_temp_coeff0;
  double linear_temp_coeff1;
  int framesPerBuffer;
  int numberOfFrames;
} Astro_Controller_Params;

/* EXPOSURE STRUCTURE */
typedef struct {
  int do_open_shutter;
  int do_image_save;
  int do_deinterlace;
  int number_of_exposures;
  int exptime;
  char image_directory[30];
  char image_filename[20];
  int deinterlace_mode;
} Astro_Expose;

/****************************************************************************** 
*       definition of fits header struct 
******************************************************************************/ 

#define MAX_FITS_CARD_LENGTH	40 
#define MAX_FITS_HEADER_CARDS	36 
#define BITS_PER_BYTE			8 
#define FITS_BLOCK_SIZE			2880 
 
#define HEADER_DEFAULT_SIMPLE	'T' 
#define HEADER_DEFAULT_BITPIX	(int)16 
#define HEADER_DEFAULT_NAXIS	(int)2 
#define FILL_DATA				(unsigned short)0 

typedef struct { 
  char simple; 
  int  bitpix; 
  int  naxis; 
  int  naxis1; 
  int  naxis2; 
  int  exp_time; 
} Fits_Header; 
 
#endif
