#include "debugging.h"
#include <time.h>
#include <cstring>
#include <stdio.h>

void stamped_puts(const char *msg) {
	time_t now;
	char stamp[30];

	now = time(NULL);
	if (ctime_r(&now, stamp) == NULL) {
		printf("<NO STAMP>: %s\n", msg);
	}
	else {
		stamp[strlen(stamp) - 2] = '\0';
		printf("%s: %s\n", stamp, msg);
	}
}

void stamped_puts(const std::string& msg)
{
	stamped_puts(msg.c_str());
}
