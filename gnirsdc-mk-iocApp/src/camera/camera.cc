/*
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME camera.cc
 *
 * GENERAL DESCRIPTION
 *
 *  CICADA camera class implementation and functions
 * Implements generic operations for a camera containing a modern digital detector.
 * Classes derived off this are used to interface directly to specific types of
 * camera and therefore contain all specific knowledge of that particular
 * camera. For example CCD cameras and IR cameras. Other types of camera such as
 * a CCD camera for tip tilt operation or driftscan operation could also be
 * supported.  This class will be used to control detector setup and operation, e.g.,
 * common methods for handling data flow are defined here. It also instantiates a
 * Controller object as one of its members so that it can interface directrly to
 * the specific detector controller electronics.
 *
 */

/* Include files */
#include <errno.h>
#include <iomanip>
#include <math.h>
#include "common.h"
#include "logging.h"
#include "epicsParameterDB.h"
#include "config_table.h"
#include "camera.h"
#include "ir_camera.h"

using std::shared_ptr;

/* Constants */

const std::string Camera::IMAGE_READY_SEM = "SlaveImR";
const std::string Camera::IMAGE_EMPTY_SEM = "SlaveImE";
const std::string Camera::DATA_XFER_SEM = "Dataxfer";
const std::string Camera::PARAMETER_SEM = "CamPar";

/* typedefs */

/* class declarations */

/* global variables */

//+ Pointer to global image buffer used during readout after pixels are unravelled
extern Detector_Image *shared_image_buffer;        //+ image transfer buffer for camera parts

/* local function declarations */
//+ Adjust address to next alignment boundary
// > addr - input addr
// > align - alignment size
static char* aligned_mem(char* addr, int align);

//+ Adds subframe data to the main_header structure from the regions detail
// > c - controller id
// > f - readout frame index
// > j - output frame index
// <> k - cumultive subframe index
// > rgn - pointer to region spec
// > main_header - main header structure to fill
// > this_rgn_frame - readout frame spec
// < frame_storage_len - total amount of storage requored for frame spec
// > frame_addr - starting address of frame spec
// > display_min - display min scaling value
// > display_max - display max scaling val
static void add_frame_to_main_header(int c, int f, int j, int& k, Detector_Regions* rgn, Detector_Image_Main_Header& main_header,
				     Frame_Spec this_rgn_frame, int& frame_storage_len, char* frame_addr, double display_min,
				     double display_max);

/* local function definitions */

/*
 *+
 * FUNCTION NAME: aligned_mem
 *
 * INVOCATION: addr=aligned_mem(char* addr, int align)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > addr - input addr
 * > align - alignment size
 *
 * FUNCTION VALUE: Aligned address
 *
 * PURPOSE:
 *  Adjust address to next alignment boundary
 *
 * DESCRIPTION:
 *  Simply moves address to next alignment boundary
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static char* aligned_mem(char* addr, int align)
{
  unsigned long addr_p;
  addr_p = (unsigned long) addr;
  addr_p = (addr_p%align)? (addr_p/align+1)*align: addr_p;
  return (char*) addr_p;
}

/* class function definitions */

/*============================================================================
 *  Camera
 *===========================================================================*/
/*
 *+
 * FUNCTION NAME: Cicada_Camera::Cicada_Camera()
 *
 * INVOCATION: camera = new Cicada_Camera(Camera_Desc& cam, int camid, int c)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam - description of this camera
 * > camid - identification number of camera
 * > c - identification number of the controller to be used by camera.
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 * Simple Constructor used for initialising configuration variables
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generic camera
 * object.  It is never used directly but is called by a derived version of this
 * class that handles a specific camera type.
 *
 * This version of the camera is used just for holding a cameras
 * configuration information. It is not used to interface with real hardware. It
 * is, therefore, only useful in modules that need to know about a configuration
 * but do not actually interface with hardware.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Camera::Camera(Camera_Desc& cam, int camid, int c, Platform_Specific* ps)
	: Hardware(ps)
{

  cam_id = camid;
  camera = cam;
  cid = c;

  // Initialise some member vars
  Camera::init_vars();
}
/*
 *+
 * FUNCTION NAME: Cicada_Camera::Cicada_Camera()
 *
 * INVOCATION: camera = new Cicada_Camera(Camera_Desc& cam, int camid, int c, const char* param_host,
 *                          Instrument_Status *st, paramMapType *pm)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam - description of camera.
 * > camid - identification number of camera
 * > c - identification number of controller owned by camera.
 * > param_host - name of host where parameter database resides.
 * > st - pointer to instrument status data structure for all instrument status
 * > msg - pointer to ring buffer message object used for holding system messages
 * > pm - pointer to parameter map type
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 * Constructor used for initialising configuration variables and setting up the
 * parameter database
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generic camera
 * object.  It is never used directly but is called by a derived version of this
 * class that handles a specific camera type.
 *
 * This version of the camera is used just for holding a cameras
 * configuration information and also its status parameter database. It is not
 * used to interface with real hardware. It is, therefore, only useful in
 * modules that need to know about a configuration and be able to read/write
 * status parameters but do not actually interface with hardware.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Camera::Camera(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st, Platform_Specific* ps)
  :Hardware(param_host, st, ps)
{
  cam_id = camid;
  camera = cam;
  cid = c;
  camera_status = &hardware_status->Hardware_Status_u.camera_status;

  // Initialise some member vars
  Camera::init_vars();

  // Now add all status parameters
  add_status_parameters(param_host, false);

}
/*
 *+
 * FUNCTION NAME: Cicada_Camera::Cicada_Camera()
 *
 * INVOCATION: camera = new Cicada_Camera(Instrument_Status *st, Parameter* ip, int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > st - pointer to instrument status data structure for all instrument status
 * > msg - pointer to ring buffer message object used for holding system messages
 * > ip - pointer to the parameter database for the instrument
 * > deb - debug flag
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Constructor used for interfacing to real camera hardware
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generic camera
 * object.  It is never used directly but is called by a derived version of this
 * class that handles a specific camera type.
 *
 * This version of the camera is used for interfacing with real hardware. It
 * is, therefore, required for components that are attached to the hardware. Its
 * derived classes know how to communicate with specific cameras through the controller
 * member, while this parent class can be used in a generic way to deal with all camera
 * types.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Camera::Camera(Instrument_Status *st, Parameter* ip, int deb, Platform_Specific* ps)
  :Hardware(st,deb,ps)
{
  // Set status union to type CAMERA
  hardware_status->type = CAMERA;

  // Setup a pointer to the camera status structure in shared mem
  camera_status = &hardware_status->Hardware_Status_u.camera_status;

  // Copy the camera and controller descriptixon
  camera = current_config->config_data.Config_Data_u.camera.desc;
  cam_id = current_config->config_data.Config_Data_u.camera.cam_id;
  cid = current_config->config_data.Config_Data_u.camera.cid;

  // Initialise some member vars
  Camera::init_vars();


  // add all status parameters
  add_status_parameters("", true);

  inst_statusp = ip;

  //Connect to shared mem for image
  image = shared_image_buffer;
  if (debug&DEBUG_INDICATORS) {
    logger::message(logger::Level::Min, "Slave%d: image address=0x%p", status->pid, shared_image_buffer);
  }

  //Connect to a semaphores for controlling image shared memory
  image_ready_sem = Semaphore::get(IMAGE_READY_SEM);
  if (debug&DEBUG_INDICATORS) {
    logger::message(logger::Level::Min, "Slave%d: Semaphore %s", status->pid, image_ready_sem->name().c_str());
  }

  image_empty_sem = Semaphore::get(IMAGE_EMPTY_SEM);
  if (debug&DEBUG_INDICATORS) {
    logger::message(logger::Level::Min, "Slave%d: Semaphore %s", status->pid, image_empty_sem->name().c_str());
  }

  data_xfer_sem = Semaphore::get(DATA_XFER_SEM);
  if (debug&DEBUG_INDICATORS) {
    logger::message(logger::Level::Min, "Slave%d: Semaphore %s", status->pid, data_xfer_sem->name().c_str());
  }
}
/*
 *+
 * FUNCTION NAME: Camera::init_vars()
 *
 * INVOCATION: init_vars()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Initialise some member vars for constructors
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Camera::init_vars()
{
  int i;

  repeat_count = 1;
  camera_statusp = NULL;
  guider_statusp = NULL;
  camera_pDB = NULL;
  image_ready_sem = NULL;
  image_empty_sem = NULL;
  data_xfer_sem = NULL;
  image = NULL;
  controller = NULL;

  for (i=0; i<camera.n_controllers; i++) {
    rgn[i]=NULL;
  }

  // Make sure cid and mode are set in camera status
  if (camera_status != NULL) {
    camera_status->cid = cid;
    camera_status->mode_status.mode = camera.mode;
  }

  // Calibrate wait timer interval
  struct timespec tsr;
  int msec_resolution;

  if (clock_getres(CLOCK_REALTIME, &tsr) == -1) {
    throw Error("Error getting CLOCK_REALTIME resolution",E_WARNING,errno,__FILE__,__LINE__);
  }

  msec_resolution = tsr.tv_sec*1000 + tsr.tv_nsec/1000000;
  IMAGE_MIN_WAIT = (IMAGE_WAIT<msec_resolution)? msec_resolution:IMAGE_WAIT;

}
/*
 *+
 * FUNCTION NAME: Camera::add_status_parameters
 *
 * INVOCATION: add_status_parameters(const char* param_host, bool init)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > param_host - name of host that hold home version of parameters
 * > init - set if required to initialise variables
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Adds all camera status parameters to the parameter database
 *
 * DESCRIPTION: Uses the Parameter class to handle any Cicada status
 * parameters. This class does immediate write-thru operations to the
 * Cicada status shared memory on the observer computer any where on the net.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Camera::add_status_parameters(const char* param_host, bool init)
{
  int i;
  int *iValid;
  long *lVal,*lValid;
  unsigned short *sVal,*sValid;
  double *dVal, *dValid;
  bool *bValid, *bVal;
  unsigned short *usVal, *usValid;
//  char cVal[NAMELEN];

  // Create the Parameter DB for holding status parameters
  camera_pDB = new EpicsParameterDB();
  camera_statusp = new Parameter(camera_pDB, PARAMETER_SEM, hardware_param_map, CAMERA_PREFIX, DB_SUFFIX);

  // Now add all the Status parameters - first set up some verification work space
//  cVal[0] = '\0';
  iValid = new int[2];
  lValid = new long[2];
  sValid = new unsigned short[2];
  bValid = new bool[2];
  dValid = new double[MAX_READOUT_BUFFERS*2];
  bVal = new bool[1];
  dVal = new double[MAX_READOUT_BUFFERS];
  lVal = new long[1];
  sVal = new unsigned short[1];
  usValid = new unsigned short[2];
  usVal = new unsigned short[1];

  iValid[0] = 0;
  iValid[1] = MAX_CONTROLLERS;
  camera_statusp->add(CID, NULL, (void*)&camera_status->cid, init, 1, &camera_status->cid,
		      VALIDITY_RANGE, 2, iValid, UNPUBLISHED);

  bValid[0] = false;
  bValid[1] = true;
  camera_status->exposure_done = false;
  camera_statusp->add(EXPOSURE_DONE, NULL, (void*)&camera_status->exposure_done, init,
		      1, (bool*)&camera_status->exposure_done, VALIDITY_SET, 2, bValid, PUBLISHED);
  camera_status->verified = false;
  camera_statusp->add(VERIFIED, VERIFIED, (void*)&camera_status->verified, init,
		      1, (bool*)&camera_status->verified, VALIDITY_SET, 2, bValid, PUBLISHED);

  for (i=0; i<MAX_READOUT_BUFFERS; i++) {
    dValid[i*2] = 0;
    dValid[i*2+1] = MAXD;
    dVal[i] = 0.0;
  }
  camera_statusp->add(ELAPSED, ELAPSED, (void*)&camera_status->elapsed, false,
		      1, dVal, VALIDITY_RANGE, 2, dValid, PUBLISHED);
  camera_statusp->add(SHUTTER_DURATION, NULL, (void*)&camera_status->shutter_duration, false,
		      1, dVal, VALIDITY_RANGE, 2, dValid, PUBLISHED);
  camera_statusp->add(EXPOSED, NULL, (void*)&camera_status->exposed, false, 1, dVal, VALIDITY_RANGE, 2, dValid, PUBLISHED);
  camera_statusp->add(EXPOSEDRQ, NULL, (void*)&camera_status->exposedRQ, false, 1, dVal, VALIDITY_RANGE, 2, dValid, PUBLISHED);
  lValid[0] = 0;
  lValid[1] = MAXI;
  camera_statusp->add(SHUTTER_OPEN_TIME, NULL, (void*)&camera_status->shutter_open_time, false,
		      1, lVal, VALIDITY_RANGE, 2, lValid, PUBLISHED);
  camera_statusp->add(DO_READOUT, NULL, (void*)&camera_status->do_readout, false,
		      1, lVal, VALIDITY_RANGE, 2, lValid, PUBLISHED);
  camera_statusp->add(TRANSFER_TIME, NULL, (void*)&camera_status->transfer_time, false,
		      1, dVal, VALIDITY_RANGE, 2, dValid, PUBLISHED);
  camera_statusp->add(TOTAL_OPERATION_TIME, NULL, (void*)&camera_status->total_operation_time, false,
		      1, dVal, VALIDITY_RANGE, 2, dValid, PUBLISHED);
  camera_statusp->add(READOUT_TIME, NULL, (void*)&camera_status->readout_time, false,
		      MAX_READOUT_BUFFERS, dVal, VALIDITY_RANGE, 2, dValid, PUBLISHED);
  camera_statusp->add(READOUT_WAIT_TIME, NULL, (void*)&camera_status->readout_wait_time, false,
		      1, dVal, VALIDITY_RANGE, 2, dValid, PUBLISHED);

  lValid[0] = 0;
  lValid[1] = 10;
  camera_statusp->add(CONTROLLER_SIMULATION, CONTROLLER_SIMULATION_DB, (void*)&camera_status->controller_simulation, false,
		      1, lVal, VALIDITY_RANGE, 2, lValid, PUBLISHED);

  camera_statusp->add(DATEOBS, DATEOBS_DB, (void*)camera_status->dateobs, false,
		      FITSVALUELEN, camera_status->dateobs, VALIDITY_NONE, 0, (char*)NULL, UNPUBLISHED);
  camera_statusp->add(UTSTART, UTSTART_DB, (void*)camera_status->utstart, false,
		      FITSVALUELEN, camera_status->utstart, VALIDITY_NONE, 0, (char*)NULL, UNPUBLISHED);
  camera_statusp->add(UTEND, UTEND_DB, (void*)camera_status->utend, false,
		      FITSVALUELEN, camera_status->utend, VALIDITY_NONE, 0, (char*)NULL, UNPUBLISHED);
  camera_statusp->add(TAI, TAI_DB, (void*)&camera_status->tai, false,
		      1, &camera_status->tai, VALIDITY_NONE, 0, (double*)NULL, UNPUBLISHED);

  // Set op status type
  camera_statusp->add(OP_STATUS_TYPE, NULL, (void*)&camera_status->op_status.type, false,
		      1, (int*)&camera_status->op_status.type, VALIDITY_NONE, 0, (int*)NULL, PUBLISHED);

  // Camera mode
  camera_statusp->add(CAMERA_MODE, NULL, (void*)&camera_status->mode_status.mode, init, 1,
		      (int*)&camera_status->mode_status.mode, VALIDITY_NONE, 0, (int*)NULL, PUBLISHED);


  // TODO: This DOES NOT WORK. CAMERA points to $(top)sad:dc:type
  //       which does not exist. Figure out if we need it at all..
  /*

  // Set hardware status type - fixed.
  hardware_statusp->put(STATUS_INFO_TYPE, (int) CAMERA);

  */

  // Both the op_status and controller_status parts are initialised in relevant camera and
  // controller classes.
  delete [] iValid;
  delete [] lValid;
  delete [] sValid;
  delete [] bValid;
  delete [] dValid;
  delete [] usValid;
  delete [] lVal;
  delete [] sVal;
  delete [] bVal;
  delete [] dVal;
  delete [] usVal;
}

/*
 *+
 * FUNCTION NAME:  Camera::setup_regions()
 *
 * INVOCATION: setup_regions(Region_Data& region_data, int bitpix)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > region_data - Cicada Region data
 * > bitpix - bits per pixel
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: sets up the regions of interest selected
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES: None
 *
 *-
 */
void Camera::setup_regions(Region_Data& region_data, int bitpix)
{
  unsigned long long chip_mask;                   // Chips to save from clockout
  int c,namps;
  unsigned long this_npix;

  bytepix = BYTE_PIX(bitpix);
  chip_mask = region_data.chip_mask;
  nactive_amps = npix = namps = 0;
  for (c=0; c<camera.n_controllers; c++) {
    if (rgn[c] != NULL) {
      delete rgn[c];
    }
    rgn[c] = new Detector_Regions(camera, c, region_data.camera_coords, camera.mosaic_mode, true,
				  region_data.nregions, region_data.xo, region_data.width, region_data.yo, region_data.height,
				  region_data.rcf, region_data.ccf, region_data.overscan_rows, region_data.overscan_cols,
				  chip_mask);
    // Shift mask to remove chips for this controller
    chip_mask >>= camera.controller[c].n_chips;
    nactive_amps += rgn[c]->nactive_amps;
    npix = MAX(npix, rgn[c]->total_binned_readout_pixels());
    namps = MAX(namps, rgn[c]->nactive_amps);
  }

  this_npix = rgn[cid]->total_binned_readout_pixels();
  if (this_npix > 0) {
    requested_image_size = rgn[cid]->nactive_amps * this_npix * bytepix;
  } else {
    // Unable to mask CHIPs in hardware - need to readout pixels as for max sized controller
    requested_image_size = namps * npix * bytepix;
  }
}


 /*
 *+
 * FUNCTION NAME: Camera::transfer_image_data
 *
 * INVOCATION: transfer_image_data(int nw, int hei, int nser, int rr, void* inp, Image_Data_Type dt, int this_bitpix,
 *                                 int frame_skip, Detector_Image_Main_Header& main_hdr)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > nw = total pixels in one row from all amps after compression (ie rgn->nactive_amps*width)
 * > hei = number of rows after compression
 * > nser = number of serial regions
 * > rr = current parallel region
 * > inp = pointer to start of pixel data
 * > dt = data type of this transfer
 * > this_bitpix = Bitpix of data being transferred
 * > frame_skip = number of frames to skip to get to correct frame
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 * Routine to transfer image data to image shared memory.
 *
 * DESCRIPTION:
 *
 * This method gets called during a readout when a buffer of pixels is ready for
 * transfer and unravelling. The pixels are taken from the readout buffer,
 * unravelled and stored in correct location in the shared image buffer ready
 * for the Data task to handle.  When the pixles are all moved the image
 * semaphore is set full and Data process freed for its operation.
 *
 * NB - This routine will block until the share Image buffer is empty. It is
 * therefore necessary that the cooperating data task process the pixels ina
 * timely fashion.
 *
 * This operation should be common to any camera
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *- */
void Camera::transfer_image_data(int nw, int hei, int nser, int rr, void* inp, Image_Data_Type dt, int this_bitpix,
				 int frame_skip, Detector_Image_Main_Header& main_hdr)
{

  ostringstream ostr;                   // output message buffer
  int sr,i,f,fi,sf,sfi,tsfi;            // loop counters and indices
  int width_serial_xfer;                // width of serial region transferred so far
  int amp_xdir,amp_ydir;                // amp x and y readout direction
  int ser_width;                        // current serial region width
  int output_width, output_height;      // output width and height
  unsigned long subframe_width, subframe_height;// frame width and height
  int nframe_pix;                       // number of pixels in the frame
  int full_row_width;                   // full output width including overscan
  int amp;                              // current amp being processed
  unsigned long long frame_mask1;       // frame masks of current and next serial regions
  unsigned long long frame_mask2;
  // int start_sr;                         // start serial region for this frame/amp
  double sum,min,max;                   // image segment stats
  char* bop = (char*) inp;              // input data pointer cast to each supported data type
  short* sop = (short*) inp;
  int* iop = (int*) inp;
  float* fop = (float*) inp;
  double* dop = (double*) inp;
  void* imagep;                         // pointer to image data area
  double wtime = 0.0;                   // waiting time
  double utime = 0.0;
  epicsTime stime;                      // Reference timestamp
  int prog_col[MAX_IMAGE_FRAMES];       // progressive column position of readout
  unsigned long long subframe_mask;
  unsigned long long region_mask;
  bool in_overscan_rows;                // flag set if handling overscan row par region
  bool in_overscan_cols;                // flag set if handling overscan column region
  int soverscan;                        // Serial overscan
  int chunks_sent;                      // count of chunks sent multiple times
  Frame_Data *this_frame;               // Output frame currently being processed
  Sub_Frame_Data *this_subframe;        // Output subframe currently being looked at

  // Transfer the data a frame at a time
  soverscan = nint(ceil(((float)rgn[cid]->overscan_cols)/main_hdr.regions.rcf)); // serial OS
  full_row_width = nw + soverscan*rgn[cid]->nactive_amps; // Full width in one row
  in_overscan_rows = (rgn[cid]->overscan_rows && (rr == rgn[cid]->npar_regions - 1));

  // Loop over the number of amplifiers in the readout
  for (i=0; i<rgn[cid]->nactive_amps; i++) {

    if (hardware_abort)
      break;

    amp = rgn[cid]->get_amp(i);

    if (!(mask_from_bitpos(amp) & rgn[cid]->par_regions[rr].amp_mask))
      continue;

    // Now loop over each serial region - a separate output frame
    width_serial_xfer = 0;
    amp_xdir = rgn[cid]->amp_xdir[amp];
    amp_ydir = rgn[cid]->amp_ydir[amp];
    sub_header.width = output_width = 0;
    sub_header.height = output_height = hei;
    // start_sr = 0;

    for (f=0; f<MAX_IMAGE_FRAMES; f++)
      prog_col[f]=0;

    for (sr=0; sr<nser; sr++) {

      if (hardware_abort)
	break;

      in_overscan_cols = (rgn[cid]->overscan_cols && (sr == nser - 1));

      // Set the output dimensions
      ser_width = rgn[cid]->par_regions[rr].ser_regions[sr].width_b;

      // See if this serial region is on this amp - true if region masks and amp
      // mask intersect or in overscan
      region_mask = rgn[cid]->get_region_mask(amp,rr,sr);

      if (in_overscan_rows ||
	  ((region_mask & rgn[cid]->par_regions[rr].ser_regions[sr].region_mask) &&
	   (mask_from_bitpos(amp) & rgn[cid]->par_regions[rr].ser_regions[sr].amp_mask))) {

	// Wait for image semaphore, before touching shared mem
	// Set to paused because we have to wait on RPC svc (subject to timeouts)
	if (debug&DEBUG_STATE)
	  logger::message(logger::Level::Min, "Slave%d: Image_empty_sem wait %s:%d, abort=%d",cid,__FILE__,__LINE__,hardware_abort);
	stime = epicsTime::getCurrent();
	image_empty_sem->wait();
	wtime += epicsTime::getCurrent() - stime;

	// Check to see if the image header was sent ok
	if (!image->header_ok) {
	  logger::message(logger::Level::Min, "Slave%d: Attempt to transfer image data without a successful image header transfer - resetting!",
			cid);
	  image_empty_sem->give();
	  handle_reset();
	}

	// Set back to running
	if (hardware_abort)
	  break;


	// OK - now setup image data to be sent
	image->body.type = IMAGE_BODY_DATA;

	// Set pointer to shared memory for image - this data follows immediately after the image structure in a
	// contiguous memory buffer.
	imagep = (void *)((unsigned long)image + sizeof(Detector_Image));
	image->body.Detector_Image_Body_u.data.pixel_data.pixel_data_val = (char*) imagep;

	// Increment the output width
	output_width += ser_width;

	// Decide whether we should transfer this ser region this time
	// through - no if another adjacent serial region follows in same region
	// but yes if that serial region needs copying to multiple frames
	if (sr<nser-1) {
	  frame_mask1 = rgn[cid]->get_frame_mask(amp,rr,sr);
	  frame_mask2 = rgn[cid]->get_frame_mask(amp,rr,sr+1);
	}

	// Unravel the pixels if last region, or not adjacent, or last before overscan
	if ((sr == nser - 1) || (frame_mask1 != frame_mask2)) {

	  stime = epicsTime::getCurrent();
	  switch (this_bitpix) {
	  case BYTE_BITPIX:
	    unravel_pix(bop+width_serial_xfer*rgn[cid]->nactive_amps+i, (char*)imagep, rgn[cid]->nactive_amps, full_row_width,
			output_height, output_width, amp_xdir, amp_ydir);
	    break;
	  case SHORT_BITPIX:
	    unravel_pix(sop+width_serial_xfer*rgn[cid]->nactive_amps+i, (short*)imagep, rgn[cid]->nactive_amps, full_row_width,
			output_height, output_width, amp_xdir, amp_ydir);
	    break;
	  case INT_BITPIX:
	    unravel_pix(iop+width_serial_xfer*rgn[cid]->nactive_amps+i, (int*)imagep, rgn[cid]->nactive_amps, full_row_width,
			output_height, output_width, amp_xdir, amp_ydir);
	    break;
	  case FLOAT_BITPIX:
	    unravel_pix(fop+width_serial_xfer*rgn[cid]->nactive_amps+i, (float*)imagep, rgn[cid]->nactive_amps, full_row_width,
			output_height, output_width, amp_xdir, amp_ydir);
	    break;
	  case DOUBLE_BITPIX:
	    unravel_pix(dop+width_serial_xfer*rgn[cid]->nactive_amps+i, (double*)imagep, rgn[cid]->nactive_amps, full_row_width,
			output_height, output_width, amp_xdir, amp_ydir);
	    break;
	  default:
	    break;
	  }
	  utime += epicsTime::getCurrent() - stime;

	  // total serial width so far
	  width_serial_xfer += output_width;

	  // Only transfer overscan pixels if this is not VARIANCE or QUALITY data
	  if ((in_overscan_cols || in_overscan_rows) && ((dt == VARIANCE_DATA) || (dt == QUALITY_DATA))) {
	    // set the semaphore - so that next iteration doesn't block
	    image_empty_sem->give();
	    continue;
	  }

	  // Fill sub header structure
	  sub_header.width = output_width;
	  sub_header.xdir = amp_xdir;
	  sub_header.ydir = amp_ydir;
	  sub_header.orient = rgn[cid]->amp_orient[amp];

	  // Loop over the subframe mask getting the subframe index of this subframe
	  // Some chunks belong to more than one subframe (eg in multi-region overscan)
	  subframe_mask = rgn[cid]->get_subframe_mask(amp,rr,sr);
	  chunks_sent = 0;
	  for (sf=0; subframe_mask>0; sf++) {

	    if (hardware_abort)
	      break;

	    // set sfi to the subframe in all the readout subframes
	    sfi = sf;
	    if (subframe_mask & 1) {

	      // Using the full subframe idx - extract the frame and subframe idxs for this frame
	      sub_header.frame = fi = main_hdr.frame_idx[cid][sfi] + frame_skip;
	      sub_header.subframe = rgn[cid]->subframes[sfi].subframe;

	      this_frame = &main_hdr.frames.frames_val[fi];
	      this_subframe = &this_frame->subframes.subframes_val[sub_header.subframe];

	      // Work out which subframe in total of all output subframes this one is
	      tsfi = this_subframe->subframe_idx;
	      sub_header.subframe_idx = tsfi;

	      if (this_subframe->orient == ORIENT_COL) {
		subframe_width = this_subframe->height;
		subframe_height = this_subframe->width;
	      } else {
		subframe_width = this_subframe->width;
		subframe_height = this_subframe->height;
	      }

	      sub_header.x = (amp_xdir==1)? prog_col[tsfi]:(subframe_width-prog_col[tsfi]-output_width);
	      prog_col[tsfi] += output_width;
	      sub_header.y = (amp_ydir==1)? prog_row[tsfi]:(subframe_height-prog_row[tsfi]-output_height);
	      prog_row[tsfi] += output_height;

	      nframe_pix = sub_header.width * sub_header.height;

	      // Wait until previous chunk processed
	      if ((chunks_sent>0) && !hardware_abort) {
		if (debug&DEBUG_STATE)
		  logger::message(logger::Level::Min, "Slave%d: Image_empty_sem wait %s:%d, abort=%d",cid,__FILE__,__LINE__,hardware_abort);
		stime = epicsTime::getCurrent();
		image_empty_sem->wait();
		wtime += epicsTime::getCurrent() - stime;
	      }

	      // copy sub header to image structure
	      image->body.Detector_Image_Body_u.data.sub_header = sub_header;

	      // Set the number of bytes in this transfer
	      image->body.Detector_Image_Body_u.data.pixel_data.pixel_data_len = abs(this_frame->image_info.bitpix)/8 * nframe_pix;

	      if (debug&DEBUG_CALCS)  {
		logger::message(logger::Level::Min, "Slave%d: %dx%d at y=%d, frame=%d, subframe=%d",
			      cid, sub_header.width, sub_header.height, sub_header.y, sub_header.frame, sub_header.subframe);
	      }

	      // Now set the image ready semaphore - this chunk is ready for the Data task
	      image_ready_sem->signal();
	      chunks_sent++;

	      if (debug&DEBUG_CALCS)  {
		imstat(imagep,nframe_pix,this_frame->image_info.bitpix,sum,min,max);
		if (nframe_pix>0) {
		  logger::message(logger::Level::Min, "Transferred image mean=%f,max=%f,min=%f", sum/nframe_pix, max, min);
		}
	      }
	    }
	    subframe_mask >>= 1;
	  }

	  if (chunks_sent == 0) {
	    // Nothing transferred - so restore semaphore
	    image_empty_sem->give();
	  }

	  // reset output_width as this gets accumulated until sent
	  output_width = 0;

	  // record next starting serial region for transfer
	  // start_sr = sr+1;

	} else {
	  // Because we didn't transfer an image block this time, restore
	  // the semaphore so it can be taken again
	  image_empty_sem->give();
	}
      } else // End if (i&amp_mask)

	// Increment width processed so far
	width_serial_xfer += ser_width;

    } // End for sr=0..nser
  } // End for i=0..rgn[cid]->nactive_amps

  if (debug&DEBUG_CALCS)  {
    logger::message(logger::Level::Min, "Wait time =%f, Unravel time=%f", wtime/(double)timestamp_res(), utime/(double)timestamp_res());
  }
}

/*
 *+
 * FUNCTION NAME:  Camera::compare_data
 *
 * INVOCATION: compare_data(char* iptr, int np, int i, ostringstream& valstr)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > iptr - pointer to input pixels
 * > np - number of pixels to process
 * > i - starting index into verification array
 * < valstr - string containing values of first mismatched data item
 *
 * FUNCTION VALUE: index to last pixel compared
 *
 * PURPOSE: Does readout verification
 *
 * DESCRIPTION: compares each pixel in the readout packet with their expected value
 * in a verification buffer. The verification buffer would have been prepared
 * earlier using predetermined formulae
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES: None
 *
 *-
 */
unsigned int Camera::compare_data(char* iptr, int np, int i, ostringstream& valstr)
{
  short *sip = (short*) iptr;                   // pointer to input pixels - different data types
  unsigned short *uip = (unsigned short*) iptr;
  int *iip = (int*) iptr;
  unsigned int *uiip = (unsigned int*) iptr;
  float *fip = (float*) iptr;
  double *dip = (double*) iptr;

  char* vptr = controller->verify_buf;

  short *svp = (short*) vptr;           // pointer to verification pixels - different data types
  unsigned short *uvp = (unsigned short*) vptr;
  int *ivp = (int*) vptr;
  unsigned int *uivp = (unsigned int*) vptr;
  float *fvp = (float*) vptr;
  double *dvp = (double*) vptr;
  unsigned int p = 0;

  // Verify that pixels are as expected - take care of each pixel input type
  switch (controller->image_info.bitpix) {
  case BYTE_BITPIX:
    p = check_data(vptr+i,iptr,np,valstr);
    break;
  case SHORT_BITPIX:
    if (controller->image_info.unsigned_data)
      p = check_data(uvp+i,uip,np,valstr);
    else
      p = check_data(svp+i,sip,np,valstr);
    break;
  case INT_BITPIX:
    if (controller->image_info.unsigned_data)
      p = check_data(uivp+i,uiip,np,valstr);
    else
      p = check_data(ivp+i,iip,np,valstr);
    break;
  case FLOAT_BITPIX:
    p = check_data(fvp+i,fip,np,valstr);
    break;
  case DOUBLE_BITPIX:
    p = check_data(dvp+i,dip,np,valstr);
    break;
  }

  return i+p;
}
/*
 *+
 * FUNCTION NAME: Camera::handle_reset
 *
 * INVOCATION: handle_reset()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Interrupt handler for the RESET signal
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Camera::handle_reset()
{

  if (debug&DEBUG_STATE) {
    logger::message(logger::Level::Min, "Slave%d: Got RESET signal, setting hardware_abort, %s, %d", status->pid, __FILE__,__LINE__);
  }

  Hardware::handle_reset();

  // Clear semaphore blocks when abort is set
  if (data_xfer_sem && data_xfer_sem->waiting) {
    if (debug&DEBUG_STATUS) {
      logger::message(logger::Level::Min, "Slave%d: Setting data_xfer sem in handle_reset", status->pid);
    }
    data_xfer_sem->give();
  }
  if (image_empty_sem && image_empty_sem->waiting) {
    if (debug&DEBUG_STATUS) {
      logger::message(logger::Level::Min, "Slave%d: Setting image_empty sem in handle_reset", status->pid);
    }
    image_empty_sem->give();
  }

  // Call controller specific reset method
  controller->handle_reset();

}

/*
 *+
 * FUNCTION NAME: Camera::clear_reset
 *
 * INVOCATION: clear_reset()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Clear the reset flag
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Camera::clear_reset()
{
  Hardware::clear_reset();
  controller->clear_reset();
}

/*
 *+
 * FUNCTION NAME: Camera::change_debug
 *
 * INVOCATION: hard->change_debug(int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > deb - debugging flag
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sets reporting object and debug level
 *
 * DESCRIPTION: Sets the camera debugging level and associated report object.
 * Expects calling program to deal with allocation/deallocation of object
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Camera::change_debug(int deb)
{
  Hardware::change_debug(deb);
  if (controller != NULL)
    controller->change_debug(deb);
}

/*
 *+
 * FUNCTION NAME: Camera::~Camera
 *
 * INVOCATION: delete camera
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Destructor
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Camera::~Camera()
{
  int c;

  for (c=0; c<camera.n_controllers; c++) {
    if (rgn[c]!=NULL)
      delete rgn[c];
  }
  if (camera_statusp != NULL)
    delete camera_statusp;
  if (guider_statusp != NULL)
    delete guider_statusp;
  if (camera_pDB != NULL)
    delete camera_pDB;
}
/*
 *+
 * FUNCTION NAME: image_name
 *
 * INVOCATION: image_name(char *iname, int n, const char *dir,
 *                        const char *prefix, int cam_id, int run_num,
 *                        const char *suffix, char* ext, bool ir_ndr, int ndr,
 *                        int coadd)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * < iname - output buffer - must be at least n+1 size
 * > n - size of buffer
 * > dir - directory component of path name
 * > prefix - prefix part of filename
 * > cam_id - camera id
 * > run_num- observation run number
 * > suffix - suffix part of filename
 * > ext - file extension
 * > ir_ndr - flag to indicate IR file name
 * > ndr - IR NDR number
 * > coadd - IR coadd number
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Function to create an image name from components
 *
 * DESCRIPTION:
 *  Construct image name using a combination of dir+prefix+run+suffix
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void image_name(char *iname, int n, const char *dir, const char *prefix, int cam_id, int run_num, const char *suffix, const char* ext, bool ir_ndr, int ndr, int coadd)
{
  ostringstream ostr;

  if (!ir_ndr) {
    // Construct file name of form dir/prefixc0001suffix.fits - maximum size is n chars
    ostr << dir << "/" << prefix << cam_id << setfill('0') << setw(4) << run_num << suffix << "." << ext << ends;
  } else {
    // Construct file name of form dir/prefix0001suffix_ndr.fits - maximum size is n chars
    ostr << dir << "/" << prefix << setfill('0') << setw(4) << run_num << suffix <<
      setw(4) << coadd << setw(4) << ndr << "." << ext << ends;
  }
  strlcpy(iname, ostr.str(), n);
}
/*
 *+
 * FUNCTION NAME: copy_frame_references
 *
 * INVOCATION:  copy_frame_references(Detector_Image_Main_Header& src_header, Detector_Image_Main_Header& dest_header,
 *                                    char* src_mem, char* dest_mem, bool rel)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > src_header - the input image main header structure
 * > dest_header - the output image main header structure
 * > src_mem - original frames data
 * < dest_mem - a copy of the frames memory
 * > rel - is src_mem constructed from relative offsets or actual addresses
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 * Dynamically allocated arrays for frame data need their pointers adjusted for
 * the local copy of this data. This routine copies the header structures and
 * corrects the frame memory references.
 *
 * DESCRIPTION:
 *
 * Frame data arrays are allocated dynamically bu the slave process.  This data
 * is available in shared memory but have pointers that are pertinent to the
 * slave process only. A local copy of this data is taken here and these
 * pointers are updated with local references.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void copy_frame_references(Detector_Image_Main_Header& src_header, Detector_Image_Main_Header& dest_header, char* src_mem,
			   char* dest_mem, bool rel)
{
  int offset,f,size;
  char* fp;

  // First copy header structure
  dest_header = src_header;

  // Now adjust the references
  if (dest_header.frames.frames_len > 0) {

    dest_header.frames.frames_val = (Frame_Data*) aligned_mem(dest_mem, ALIGN_SIZE);
    offset = 0;
    size = sizeof(Frame_Data) * dest_header.frames.frames_len;
    size = (size%ALIGN_SIZE)? (size/ALIGN_SIZE+1)*ALIGN_SIZE: size;
    fp = (rel)? aligned_mem(src_mem, ALIGN_SIZE):(char*)src_header.frames.frames_val;
    memcpy(aligned_mem(dest_mem, ALIGN_SIZE), fp, size);

    for (f=0; f<(int)dest_header.frames.frames_len; f++) {
      if (dest_header.frames.frames_val[f].subframes.subframes_len > 0) {
	offset += size;
	dest_header.frames.frames_val[f].subframes.subframes_val = (Sub_Frame_Data*) (aligned_mem(dest_mem, ALIGN_SIZE) + offset);
	fp = (rel)? aligned_mem(src_mem, ALIGN_SIZE)+offset:(char*)src_header.frames.frames_val[f].subframes.subframes_val;
	size = sizeof(Sub_Frame_Data) * dest_header.frames.frames_val[f].subframes.subframes_len;
	size = (size%ALIGN_SIZE)? (size/ALIGN_SIZE+1)*ALIGN_SIZE: size;
	memcpy(aligned_mem(dest_mem, ALIGN_SIZE)+offset, fp, size);
      }
    }
  }
}
/*
 *+
 * FUNCTION NAME: setup_image_header
 *
 * INVOCATION: setup_image_header(int id, int cam_id, int cid, int verify, Camera_Desc& camera,
 *                                Detector_Image_Main_Header& main_header, Detector_Regions** rgn, Region_Data& regions,
 *                                char* frames_mem, Detector_Image_Info& image_info, bool save_var, bool save_qual)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > id - identifier
 * > cam_id - camera id
 * > cid - controller id
 * > verify - flag set if to verify readout
 * > camera - camera description
 * < main_header - main image header to setup
 * > rgn - pointer to regions readout structure
 * > regions - ROI structure
 * > frames_mem - pointer to image frames buffer space
 * > image_info - information about image, eg bitpix,bzero,bscale etc
 * > save_var - flag set if variance data to be saved in this output file
 * > save_qual - flag set if quality data to be saved in this output file
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Setup up image header values
 *
 * DESCRIPTION:
 * Set up an image header based on the readout request and the region sepecification
 * Assumes the region specification has been sorted.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void setup_image_header(int id, int cam_id, int cid, int verify, Camera_Desc* camera, Detector_Image_Main_Header& main_header,
			Detector_Regions** rgn, Region_Data& regions, char* frames_mem, Detector_Image_Info& image_info,
			bool save_var, bool save_qual)
{
  ostringstream ostr;                      // output message buffer
  int a,c,f,j,k,na,cf,sf;               // counters and indices
  char *frame_addr;                     // pointer to address of frames mem
  int frame_storage_len;                // Lenght of frame storage
  Regions_Frame_Map::iterator rgn_iter; // Regions frame iterator
  Frame_Data *this_frame;               // Current output frame
  Frame_Spec this_rgn_frame;            // Current region frame
  Sub_Frame_Spec *this_rgn_subframe;    // Current region subframe
  double display_min;                   // Default display scaling values
  double display_max;

  // Set constant values for current image
  main_header.id = id;

  main_header.byte_order = byte_order();
  main_header.camera_width = camera->cols;
  main_header.camera_height = camera->rows;
  main_header.mosaic_mode = camera->mosaic_mode;

  image_name(main_header.client_image_name, FILENAMELEN, "./", "ir", 0, 0, "");

  main_header.saveVar = save_var;
  main_header.saveQual = save_qual;

  main_header.verify = verify;
  main_header.regions = regions;


  // Get frame dimensions from region class
  main_header.ncontrollers = camera->n_controllers;
  main_header.cam_id = cam_id;
  main_header.cid = cid;
  main_header.nsframes = 0;
  main_header.ndframes = 0;
  if (!main_header.regions.rcf || !main_header.regions.ccf) {
    throw Error("Camera: Row and column compression factors must be greater than zero!",
		       E_ERROR, -1 ,__FILE__, __LINE__);
  }

  j=k=na=0;
  main_header.controller_mask = 0;

  // Loop to calculate number of output frames
  main_header.frames.frames_len = 0;
  for (c=0; c<camera->n_controllers; c++) {
    main_header.frames.frames_len += rgn[c]->nframes;

    // Modify number of frames if required to save variance and quality data
    if (main_header.saveVar)
      main_header.frames.frames_len += rgn[c]->ndframes;
    if (main_header.saveQual)
      main_header.frames.frames_len += rgn[c]->ndframes;

    if (rgn[c]->nframes > 0)
      main_header.controller_mask |= mask_from_bitpos(c);
  }

  main_header.frames.frames_val = NULL;

  // Must have at least 1 frame
  if (main_header.frames.frames_len == 0) {
    ostr << "Camera: Bad regions spec - there are no frames to output!" << ends;
    throw Error(ostr, E_ERROR, -1 ,__FILE__, __LINE__);
  }

  // Make sure the frames data storage is allocated in shared memory
  frame_addr = aligned_mem(frames_mem, ALIGN_SIZE);
  frame_storage_len = main_header.frames.frames_len * sizeof(Frame_Data);

  // For Solaris under optimisation we get memory alignment errors with frame_addr
  // calculations if the frame_storage_len increment is not an even alignment multiple
  // Make sure it is.
  frame_storage_len = (frame_storage_len%ALIGN_SIZE)? (frame_storage_len/ALIGN_SIZE+1)*ALIGN_SIZE:
    frame_storage_len;

  if (frame_storage_len > FRAMES_MEM_SIZE) {
    ostr << "Camera: Overflowed frame storage area of " << FRAMES_MEM_SIZE <<
      "B with attempt to allocate " << frame_storage_len << "B!" << ends;
    throw Error(ostr, E_ERROR, -1 ,__FILE__, __LINE__);
  }

  main_header.frames.frames_val = new (frame_addr) Frame_Data[main_header.frames.frames_len];

  // Update the shared memory pointer to the next free frames storage area
  frame_addr = aligned_mem(frames_mem, ALIGN_SIZE) + frame_storage_len;

  for (c=0; c<camera->n_controllers; c++) {

    // Sum the total number of subframes to output
    main_header.nsframes += rgn[c]->nsframes;
    if (main_header.saveVar)
      main_header.nsframes += rgn[c]->ndsframes;
    if (main_header.saveQual)
      main_header.nsframes += rgn[c]->ndsframes;

    main_header.ndframes += rgn[c]->ndframes;

    cf = j;

    // Iterate over the frames map
    rgn_iter = rgn[c]->frames.begin();
    while (rgn_iter != rgn[c]->frames.end()) {
      f = (*rgn_iter).first;

      this_rgn_frame = (*rgn_iter).second;
      this_frame = &main_header.frames.frames_val[j];

      // Add the frames to the main header structures
      add_frame_to_main_header(c,f,j,k, rgn[c], main_header, this_rgn_frame, frame_storage_len,
			       frame_addr, display_min, display_max);

      // Add image info to this frame
      this_frame->image_info = image_info;
      this_frame->image_info.data_type = this_rgn_frame.data_type;

      // Update the shared memory pointer to the next free frames storage area
      frame_addr = aligned_mem(frames_mem, ALIGN_SIZE) + frame_storage_len;

      // Remember where each region subframe gets put - we need to do this
      // because we may be inserting extra frames here (ie var and qual)
      for (sf=0; sf<(int)this_rgn_frame.subframes.size(); sf++) {
	this_rgn_subframe = &this_rgn_frame.subframes[sf];
	main_header.frame_idx[c][this_rgn_subframe->subframe_idx] = j;
      }

      // Increment the main frame index and the total subframe counter
      j++;

      // Now add equivalent sized frames for variance and quality data if required and
      // if this current frame is raw data
      if (this_rgn_frame.data_type == RAW_DATA) {
	if (main_header.saveVar) {
	  add_frame_to_main_header(c,f,j,k, rgn[c], main_header, this_rgn_frame, frame_storage_len,
				   frame_addr, display_min, display_max);
	  frame_addr = aligned_mem(frames_mem, ALIGN_SIZE) + frame_storage_len;
	  this_frame = &main_header.frames.frames_val[j];
	  this_frame->image_info = image_info;
	  this_frame->image_info.data_type = VARIANCE_DATA;
	  this_frame->image_info.bitpix = FLOAT_BITPIX;
	  this_frame->image_info.unsigned_data = false;
	  j++;
	}
	if (main_header.saveQual) {
	  add_frame_to_main_header(c,f,j,k, rgn[c], main_header, this_rgn_frame, frame_storage_len,
				   frame_addr, display_min, display_max);
	  frame_addr = aligned_mem(frames_mem, ALIGN_SIZE) + frame_storage_len;
	  this_frame = &main_header.frames.frames_val[j];
	  this_frame->image_info = image_info;
	  this_frame->image_info.data_type = QUALITY_DATA;
	  this_frame->image_info.bitpix = BYTE_BITPIX;
	  this_frame->image_info.unsigned_data = true;
	  j++;
	}
      }

      // increment the region frames iterator
      rgn_iter++;
    }

    // Count the number of active amps in use for this configuration
    for (k=0; k<camera->controller[c].n_chips; k++) {
      for (a=0; a<camera->controller[c].chip[k].n_amps; a++)
	if (camera->controller[c].chip[k].amp_mask & (a+1))
	  na++;
    }

    // Count frames for this controller
    main_header.controller_nframes[c] = j - cf;
  }

  // Write MEF fits for any inst with multi-amps unless choosing to mosaic the
  // data for output
  main_header.extend = (main_header.frames.frames_len>1)? main_header.frames.frames_len:0;

  // Set the size of a FITS header - depends on type of file
  main_header.phu = (main_header.extend)? camera->phu2:camera->phu1;
  main_header.ihu = camera->ihu;

}


/*
 *+
 * FUNCTION NAME: add_frame_to_main_header
 *
 * INVOCATION: add_frame_to_main_header(int c, int f, int j, int& k, Detector_Regions* rgn,Detector_Image_Main_Header& main_header,
 *                                      Frame_Spec this_rgn_frame, int& frame_storage_len, char* frame_addr,
 *                                      double display_min, double display_max)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > c - controller id
 * > f - readout frame index
 * > j - output frame index
 * <> k - cumultive subframe index
 * > rgn - pointer to region spec
 * > main_header - main header structure to fill
 * > this_rgn_frame - readout frame spec
 * < frame_storage_len - total amount of storage requored for frame spec
 * > starting address of frame spec
 * > display_min - display min scaling value
 * > display_max - display max scaling val
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:  Adds subframe data to the main_header structure from the regions detail
 *
 * DESCRIPTION:
 * For each output frame add detail about the frame and its subframes from the regions readout spec.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void add_frame_to_main_header(int c, int f, int j, int& k, Detector_Regions* rgn, Detector_Image_Main_Header& main_header,
				     Frame_Spec this_rgn_frame, int& frame_storage_len, char* frame_addr, double display_min,
				     double display_max)
{
  ostringstream ostr;
  Frame_Data *this_frame;
  Sub_Frame_Data *this_subframe;
  Sub_Frame_Spec *this_rgn_subframe;
  int sf;

  this_frame = &main_header.frames.frames_val[j];

  // Full width/height of mosaiced frame
  this_frame->width = this_rgn_frame.width_binned;
  this_frame->height = this_rgn_frame.height_binned;
  this_frame->width_unbinned = this_rgn_frame.width;
  this_frame->height_unbinned = this_rgn_frame.height;
  this_frame->data_width = this_rgn_frame.data_width_binned;
  this_frame->data_height = this_rgn_frame.data_height_binned;
  this_frame->data_width_unbinned = this_rgn_frame.data_width;
  this_frame->data_height_unbinned = this_rgn_frame.data_height;

  // x and y camera origin
  this_frame->camera_xo = this_rgn_frame.camera_xo;
  this_frame->camera_yo = this_rgn_frame.camera_yo;

  this_frame->subframes.subframes_len = this_rgn_frame.subframes.size();
  this_frame->subframes.subframes_val = NULL;

  // Must have at least one subframe
  if (this_frame->subframes.subframes_len == 0) {
    ostr << "Camera: Bad regions spec - there are no subframes for frame %d!" << j << ends;
    throw Error(ostr, E_ERROR, -1 ,__FILE__, __LINE__);
  }

  // Make sure the subframes data storage is allocated in shared memory
  frame_storage_len += this_frame->subframes.subframes_len * sizeof(Sub_Frame_Data);

  // For Solaris under optimisation we get memory alignment errors with frame_addr
  // calculations if the frame_storage_len increment is not an even alignment multiple
  // Make sure it is.
  frame_storage_len = (frame_storage_len%ALIGN_SIZE)? (frame_storage_len/ALIGN_SIZE+1)*ALIGN_SIZE:
    frame_storage_len;

  if (frame_storage_len > FRAMES_MEM_SIZE) {
    ostr << "Camera: Overflowed frame storage area of " << FRAMES_MEM_SIZE <<
      "bytes with attempt to allocate " << frame_storage_len << "bytes!" << ends;
    throw Error(ostr, E_ERROR, -1 ,__FILE__, __LINE__);
  }
  this_frame->subframes.subframes_val = new (frame_addr) Sub_Frame_Data[this_frame->subframes.subframes_len];

  for (sf=0; sf<(int)this_frame->subframes.subframes_len; sf++) {

    this_subframe = &this_frame->subframes.subframes_val[sf];
    this_rgn_subframe = &this_rgn_frame.subframes[sf];

    // What controller, chip and amp does this frame belong to
    this_subframe->controller_idx = c;
    this_subframe->chip_idx = rgn->chip_index(f,sf);
    this_subframe->amp_idx = rgn->amp_index(f,sf);
    this_subframe->frame_idx = j;
    this_subframe->subframe_idx = k;

    // NB truncate following divisions because we get overflow if round up.
    this_subframe->camera_xo = this_rgn_subframe->camera_xo/main_header.regions.rcf;
    this_subframe->camera_yo = this_rgn_subframe->camera_yo/main_header.regions.ccf;
    this_subframe->frame_xo = this_rgn_subframe->frame_xo/main_header.regions.rcf;
    this_subframe->frame_yo = this_rgn_subframe->frame_yo/main_header.regions.ccf;
    this_subframe->xo = this_rgn_subframe->xo;
    this_subframe->yo = this_rgn_subframe->yo;

    // readout direction, cpmpression and orientation of each frame
    this_subframe->xdir = this_rgn_subframe->xdir;
    this_subframe->ydir = this_rgn_subframe->ydir;
    this_subframe->rcf = this_rgn_subframe->rcf;
    this_subframe->ccf = this_rgn_subframe->ccf;
    this_subframe->orient = this_rgn_subframe->orient;

    // Width and height
    if (this_subframe->orient == ORIENT_COL) {
      this_subframe->width = this_rgn_subframe->height_binned;
      this_subframe->height = this_rgn_subframe->width_binned;
      this_subframe->width_unbinned = this_rgn_subframe->height;
      this_subframe->height_unbinned = this_rgn_subframe->width;
      this_subframe->data_width = this_rgn_subframe->data_height_binned;
      this_subframe->data_height = this_rgn_subframe->data_width_binned;
      this_subframe->data_width_unbinned = this_rgn_subframe->data_height;
      this_subframe->data_height_unbinned = this_rgn_subframe->data_width;
      // if there is a single subframe and it is column oriented - need to switch
      // frame width and height
      if (this_frame->subframes.subframes_len == 1) {
	this_frame->width = this_rgn_frame.height_binned;
	this_frame->height = this_rgn_frame.width_binned;
	this_frame->width_unbinned = this_rgn_frame.height;
	this_frame->height_unbinned = this_rgn_frame.width;
      }
    } else {
      this_subframe->width = this_rgn_subframe->width_binned;
      this_subframe->height = this_rgn_subframe->height_binned;
      this_subframe->width_unbinned = this_rgn_subframe->width;
      this_subframe->height_unbinned = this_rgn_subframe->height;
      this_subframe->data_width = this_rgn_subframe->data_width_binned;
      this_subframe->data_height = this_rgn_subframe->data_height_binned;
      this_subframe->data_width_unbinned = this_rgn_subframe->data_width;
      this_subframe->data_height_unbinned = this_rgn_subframe->data_height;
    }

    // Prescan and postscan, trim width, overscan
    this_subframe->prescan = this_rgn_subframe->prescan_binned;
    this_subframe->postscan = this_rgn_subframe->postscan_binned;
    this_subframe->trim_width = this_rgn_subframe->trim_width;
    this_subframe->overscan_cols = this_rgn_subframe->overscan_cols_binned;
    this_subframe->overscan_rows = this_rgn_subframe->overscan_rows_binned;

    // Increment the main subframe index
    k++;
  }
}

/*
 *+
 * FUNCTION NAME: cameraTest
 *
 * INVOCATION: cameraTest()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Uses Camera_Test class to test instantiation of Camera object
 *
 * DESCRIPTION: Simple set of instantiations of Camera_Test objects at
 * different constructor levels.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void cameraTest()
{
  Camera_Test *camera;
  Filenametype fname;
  string name;
  Camera_Desc camera_desc;
  string ph;
  Instrument_Status st;
  ParamMapType test_pm;
  Config cf;
  shared_ptr<Semaphore> irs;
  shared_ptr<Semaphore> ies;
  Parameter ip;
  Camera_Table ctable;

  try {
    cout << "Running Camera class tests." << endl;

    // read in the camera configuration table
    Platform_Specific spec;
    strlcpy(fname,CAMERA_TABLE,FILENAMELEN);
    ctable.read(fname);

    name = "GNIRS";
    cout << "Camera table read ok." << endl;

    camera_desc = ctable[name.c_str()];

    cout << "Camera " << name << " found in table ok." << endl;

    cout << "Creating object with simple constructor ..." << endl;
    camera = new Camera_Test(camera_desc,0,0,&spec);
    cout << " Done" << endl;
    delete camera;

    cout << "Creating object with parameter only constructor ..." << endl;
    ph = "maestro";

    st.id = 0;
    st.pid = 0;
    camera = new Camera_Test(camera_desc, 0, 0, ph.c_str(), &st, &spec);
    cout << " Done" << endl;
    delete camera;

    cout << "Creating object with full status and config structures ..." << endl;
    cf.config_data.Config_Data_u.camera.desc = camera_desc;
    cf.config_data.Config_Data_u.camera.cid = 0;
    st.hardware_status.Hardware_Status_u.camera_status.op_status.type = CAMERA_UNKNOWN;
    irs = Semaphore::create(Camera::IMAGE_READY_SEM);
    ies = Semaphore::create(Camera::IMAGE_EMPTY_SEM);
    camera = new Camera_Test(&st, &ip, 0, &spec);

    delete camera;
    cout << " Done" << endl;
  }
  catch (Error &e) {
    e.print_error(__FILE__,__LINE__);
  }
}
