[schematic2]
uniq 7
[tools]
[detail]
w 5048 2315 100 0 n#1 elongins.testID.VAL 4992 2304 5152 2304 ecars.testC.ICID
w 3728 3195 -100 0 CLID inhier.CLID.P 3648 3232 3712 3232 3712 3184 3792 3184 ecad4.test.ICID
w 4214 3227 100 0 n#2 junction 4176 3216 4288 3216 4288 2640 4320 2640 cadPlus.testP.STAT
w 4180 3291 100 0 n#2 ecad4.test.VAL 4112 3216 4176 3216 4176 3376 4304 3376 outhier.VAL.p
w 4160 2587 100 0 n#3 ecad4.test.SPLK 4112 2576 4256 2576 4256 2672 4320 2672 cadPlus.testP.SPIN
w 4144 2619 100 0 n#4 ecad4.test.STLK 4112 2608 4224 2608 4224 2704 4320 2704 cadPlus.testP.STIN
w 4204 3243 100 0 n#5 ecad4.test.MESS 4112 3184 4208 3184 4208 3312 4304 3312 outhier.MESS.p
w 3764 3259 100 0 n#6 inhier.DIR.P 3696 3312 3760 3312 3760 3216 3792 3216 ecad4.test.DIR
s 2608 1856 200 0 The TEST CAD/CAR schematic. SNL code monitors the state of the testP
s 2608 1792 200 0 START and STOP input links to perform the required action.
[cell use]
use elongins 4736 2247 100 0 testID
xform 0 4864 2320
use inhier 3632 3191 100 0 CLID
xform 0 3648 3232
use inhier 3680 3271 100 0 DIR
xform 0 3696 3312
use cadPlus 4320 2752 150 0 testP
xform 0 4400 2656
p 4320 2544 100 0 1 set1:cad testP
use ecars 5216 2384 150 0 testC
xform 0 5312 2224
p 5120 2000 100 0 1 def(FLNK):$(top)combCars21.PROC
use outhier 4272 3335 100 0 VAL
xform 0 4288 3376
use outhier 4272 3271 100 0 MESS
xform 0 4288 3312
use ecad4 3872 3260 150 0 test
xform 0 3952 2896
p 3872 2448 100 0 1 FTVA:STRING
p 3728 2288 100 0 0 FTVB:LONG
p 3728 2256 100 0 0 FTVC:LONG
p 3728 2224 100 0 0 FTVD:LONG
p 3476 2620 100 0 0 MFLG:THREE STATES
p 3888 2496 100 0 1 SNAM:dcCADtest
use eborderC 2384 1511 100 0 eborderC#0
xform 0 4064 2816
p 4960 1664 100 768 -1 author:Peter Young
p 4960 1776 40 0 -1 comment1:1997-10-01 GMOS version. Ken Ramey
p 4960 1756 40 0 -1 comment2:2000-12-11 Modified for GNIRS. Peter Young
p 4960 1632 100 768 -1 date:2000-12-11
p 5184 1712 200 768 -1 file:dcTestCad.sch
p 5456 1664 100 0 -1 page:1
p 5568 1664 100 0 -1 pages:1
p 5232 1664 100 0 -1 revision:0
p 5184 1776 150 768 -1 system:Gemini GNIRS DC
[comments]
