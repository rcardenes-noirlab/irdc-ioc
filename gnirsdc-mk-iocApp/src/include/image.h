/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *   image.h
 * 
 * PURPOSE
 *  Definition of general purpose image handling class.  Both one and *
 *  two-dimensional Images are supported.  There are public member functions *
 *  allowing addition, subtraction, multiplication and division of Images.
 *  There * are Image operator scalar, and scalar operator Image functions.
 *  Functions * for taking the square root, logarithm base 10 and natural
 *  logarithm of an * Image are available.  An Image can be rebinned and/or
 *  rotated and translated.  * Images can either be input as reaw data or taken
 *  from Fits files. There is * some support for multi-extension Fits.
 *
 * ORIGINAL AUTHOR: 
 *  Stephen Meatheringham - RSAA 1995
 *
 * HISTORY
 *   July 1995 SJM    Created for RSAA CICADA package
 *   June 2002 PJY Ported to VxWorks/GNIRS from RSAA CICADA package */

#ifndef _IMAGE_H
#define _IMAGE_H
 
// Include files 
#include <sys/types.h>
#include <float.h>

#include "utility.h"
#include "fits.h"

// defines and constants
const int IMAGE_STRING_SIZE = 256;// size of strings used in various places
const float IMAGE_MAX_PIX_VALUE = FLT_MAX; // max pixel value
const int NFREQ = 0x10000;        // allocate a 64k array

// typedefs 
enum operator_type {PLUS, MINUS, MULTIPLY, DIVIDE};
enum function_type {LINEAR, MODE, LN, SQR, INVERT, SQRT, LOG};
enum operand_type  {SCALAR, IMAGE, FUNCTION};
enum Vector_Modify_Option {VECTOR_AUTOSCALE,VECTOR_FWHM,VECTOR_STATS,
			   VECTOR_FLIP,VECTOR_SMOOTH,VECTOR_UNZOOM,
			   VECTOR_ZOOM,VECTOR_RIGHT,VECTOR_LEFT};

// what your application thinks pixels are, real or integer.
// This will be removed later on
typedef float Rawpix;


/*--------------------------------------------------------------------------
 Class declarations 
 -------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
CLASS
    Image_Pixel

DESCRIPTION  
    The Image_Pixel class defines a pixel with 3 dimensions, x, y and z.
 
KEYWORDS
    Image_Pixel, pixel, Image

-------------------------------------------------------------------------*/
class Image_Pixel 
{
 public:
  float x;                              //+ x dimensions of pixel in microns
  float y;                              //+ y dimensions of pixel in microns
  float z;                              //+ z dimensions of pixel in microns

  //+ Constructor with default dimensions
  Image_Pixel() 
    { 
      x = 27.0; 
      y = 27.0; 
      z = 10.0; 
    }
  //+ Constructor with input dimensions
  // > a - input x size
  // > b - input y size
  // > c - input z size
  Image_Pixel(float a, float b, float c) 
    { 
      x = a; 
      y = b; 
      c = z; 
    }
};

/*-------------------------------------------------------------------------
CLASS
    Image

DESCRIPTION 
    Implementation of general purpose image handling class.  Both one and
    two-dimensional Images are supported.  There are public member functions
    allowing addition, subtraction, multiplication and division of Images.
    There are Image operator scalar, and scalar operator Image functions.
    Functions for taking the square root, logarithm base 10 and natural
    logarithm of an Image are available.  An Image can be rebinned and/or
    rotated and translated.  Images can either be input as reaw data or taken
    from Fits files. There is some support for multi-extension Fits.
 
KEYWORDS
    Image, FITS, pixel
-------------------------------------------------------------------------*/
class Image {

public:

  //+ Constructor - copies input data for Image data
  // > x - number of pixels in x axis
  // > y - number of pixels in y axis
  // > xorig - first pixel to use in x axis
  // > bp - number of bits per pixel
  // > data - pointer to input data array
  // > us - flag indicating whether input data is unsigned
  Image(int x, int y, int xorig, int bp, char *data, int us = 0);


  //+ Constructor -  uses Fits as its image data
  // > ff  - Filename of the fits image to become part of the Image.
  // > read_fits  - if set, reads the Fits image and sets the Image data
  //   pointer (pix) to the Fits data in extension number ext_num.
  //   Else clears the Image data pointer.
  // > ext_num - Number of Fits extension to be mapped in the Image.
  Image(char* ff, int read_fits = FALSE, int ext_num = 0);


  //+ A constructor where the size is known but no data is available.  Data is
  //  set to zeros.
  // > x  - size of data array in x axis
  // > y  - size of data array in y axis
  // > bp - bits per pixel
  // > us - If set, use unsigned data.
  Image(int x, int y, int bp, int us = 0);

  //+ Default Constructor, where the size is unknown.
  Image();

  //+ In-memory copy constructor (no fits file).
  //  > im - input Image object.
  Image(const Image &im);


  //+ Destructor.
  ~Image();

  // GROUP: General public member variables
  int xstart;                           //+ first pixel number x dir
  int ystart;                           //+ first pixel number y dir
  int xincr;                            //+ increment per pixel
  int nx;                               //+ x size of image
  int ny;                               //+ y size of image
  string name;                          //+ name of image
  int bitpix;                           //+ contains the number of bits per data pixel
  char *pix;                            //+ image data values
  short *spix;                          //+ pointer to pixels - shorts
  unsigned short *upix;                 //+ pointer to pixels - unsigned shorts
  int *ipix;                            //+ pointer to pixels - ints
  unsigned int *uipix;                  //+ pointer to pixels - unsigned ints
  float *fpix;                          //+ pointer to pixels - floats
  double *dpix;                         //+ pointer to pixels - doubles
  int unsigned_data;                    //+ TRUE if data type is unsigned

  // GROUP: Fits related public member variables
  int fits_ext;                         //+ Fits image extension to load from
  int mosaic_data;                      //+ Has MEF FITS file been mosaiced for load
  Fits* fits;                           //+ Image is loaded from a Fits file directly

  // GROUP: Statistics related public member variables
  double min,mmin;                      //+ minimum pixel value
  double max,mmax;                      //+ maximum pixel value
  double mean;                          //+ mean pixel value
  double mode;                          //+ mode pixel value
  double median;                        //+ median pixel value
  double sdev;                          //+ standard deviation of pixel values
  int nfreq;                            //+ number of pts in frequency histogram


  // GROUP: Public member functions
  //+ Open/close an image
  void open();
  void close();

  
  // GROUP: Statistics functions
  void minmax();
  void find_mode(int **freq, float **xpix);
  void stats(int sample_full, int sample, int** freq, float** xpix);
  void std_dev();
  void hopt_range(double &low, double &high, int sample=1000, float contrast=0.5, 
		  int use_median=TRUE);


  //+ translate an Image
  Image* translate(int dx, int dy);

  //+ save image as Fits file
  void save_fits(char* fname, double* bscale, double* bzero);

private:
  // GROUP: Private member variables
  string fitsname;                      //+ Name of Fits file - if using fits to load
  int fits_open;                        //+ TRUE if fits file opened and read

  //+ Initialise Image member vars
  void initialise();

};


//+ Image Descriptor is a structure describing an image posted on a bulletin board
struct Image_Descriptor{
  int new_image;                        //+ Set TRUE when image is new
  Fitsname image_name;                  //+ name of image 
  int adaptive_scaling;                 //+ True if using hopt adaptive scaling
  int use_median;                       //+ True if using median for adaptive scaling
  double min;                           //+ min scaling value
  double max;                           //+ max scaling value
  int sample_pix;                       //+ image sample size
  double contrast;                      //+ Current image contrast for display
  int disp_xo,disp_yo;                  //+ xo and yo of image in display
  int disp_xbin,disp_ybin;              //+ xbin and ybin of image in display
};

// global variable declarations

// global function definitions

// image arithmetic
Image* image_add(Image* im1, Image* im2);
Image* image_add(Image* im, const Rawpix scalar);
Image* image_minus(Image* im1, Image* im2 );
Image* image_minus(Image* im, const Rawpix scalar);
Image* image_minus(const Rawpix scalar, Image* im);
Image* image_mult(Image* im1, Image* im2 );
Image* image_mult(Image* im, const Rawpix scalar);
Image* image_div(const Image* im1, const Image* im2 );
Image* image_div(Image* im, const Rawpix scalar);
Image* image_div(const Rawpix scalar, const Image* im);
Image* image_sqrt(const Image* im);
Image* image_log(const Image* im);
Image* image_log10(const Image* im);


// local function declarations
//+ determine the bitpix of an image that is made from one or two input 
//+ Images.  Valid numbers are : 8, 16, 32, -32, and -64
int set_bitpix(int bp1, int bp2, double max, double mmax,
               double min, double mmin);
int set_unsigned(int bp, int us1, int us2);
//+ Determines minimum bitpix of scalar value
int scalar_bitpix(const Rawpix scalar);

//+ Template function for getting minmax of an image
template<class T> void tminmax(T *tpix, int n,
                               double& min, double& max, double& mean);

//+ Template function to accumulate frequncy histogram
template<class T> void freqsum(T *tpix, int n, double dmin, double dmax,
                               int* freq, double scale);

//+ template function for computing std_dev
template<class T> void tstd_dev(T *tpix, int n, double& sdev, double& min,
                                double& max, double& mean);

//+ Template function for copying an image to an output image
template<class T, class U> void copy_image(int n, T *opix, U *ipix);
//+ Template function for taking sqrt of image
template<class T, class U> void timage_sqrt(int n, T *opix, U *ipix);
//+ Template function for taking log of image
template<class T, class U> void timage_log(int n, T *opix, U *ipix);
//+ Template function for taking log base 10 of image
template<class T, class U> void timage_log10(int n, T *opix, U *ipix);
//+ template function that adds/subs/mults/divs im1 and im2 pixels with output to opix
template<class T> void pix_image_op_image(int n, int op, T *opix,
                                            const Image* im1,
                                            const Image* im2);
//+ template function that adds/subs/mults/divs ipix and im2->pix pixels with output to opix
template<class T, class U> void pix_pix_op_image(int n, int op, T *opix, 
						 U *ipix, const Image* im2);
// Template function for operating on two images
template<class T, class U, class V> void image_op_image(int n, int op, 
						T *opix, U *ipix1, V *ipix2, V max);

//+ template function that applies op to im1 and scalar with output to opix
template<class T> void pix_image_op_scalar(int n, int op, T *opix, Image* im,
                                             T scalar);
//+ template function that subtracts im1 from scalar with output to opix
template<class T> void pix_scalar_minus_image(int n, T *opix, const Image* im,
                                              T scalar);
//+ Template function for subtracting an image from an scalar
template<class T, class U, class V> void scalar_minus_image(int n, T *opix,
                                                            U *ipix, V scalar);
//+ Template function for operating on an image with a scalar of different data types
template<class T, class U, class V> void image_op_scalar(int n, int op, 
					 T *opix, U *ipix1, V scalar);

//+ Template function for dividing two images of different data types
template<class T, class U, class V> void scalar_div_image(int n, T *opix,
                                                          U *ipix, V scalar, U max);

//+ Simulate cosmic rays
template<class T> int cosmic(T* image, int nx, int ny, double exposed, float shieldcover, 
			      float rate, bool mask, Image_Pixel P);
#endif  // _IMAGE_H
