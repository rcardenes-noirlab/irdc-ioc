/*
 * Copyright (c) 1994-2001 by MSSSO Computing Section 
 * CICADA PROJECT
 * 
 * FILENAME sdsu_vme_test.cc
 * 
 * GENERAL DESCRIPTION
 * Simple program that tests comms to SDSU VME interface cards and runs
 * a set of TDL tests to both timing and utility boards.
 *
 */

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#ifndef vxWorks
#error This code only runs under VxWorks
#endif 

#include "vxWorks.h"
#include <vxLib.h>
#include <ioLib.h>
#include <vme.h>
#include <sysLib.h>
#include <cacheLib.h>
#include <intLib.h>
#include <iv.h>
#include <semLib.h>
#include <timers.h>
#include <private/timerLibP.h>
#include <msgQLib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <symbol.h>
#include <sysSymTbl.h>

//+ Following needed for intdisconnect code (google search for intdisc.c)
/*
#include <drv/intrCtl/i82378zb.h>
*/
typedef struct intHandlerDesc           /* interrupt handler desciption */
    {
    VOIDFUNCPTR                 vec;    /* interrupt vector */
    int                         arg;    /* interrupt handler argument */
    struct intHandlerDesc *     next;   /* next interrupt handler & argument */
    } INT_HANDLER_DESC;


extern void* sysIntTbl[];
extern void* sysVmeIntTable[];
STATUS intDisconnect(int);
static int handlerDelete(INT_HANDLER_DESC *);


const int MSGLEN=256;
const int SECOND=1000;

const int VME_ID = 1;
const int TIM_ID = 2;
const int UTIL_ID= 3;
const int TDL = 0x54444c;
const int SRA = 0x535241;
const int WRM = 0x57524D;
const int RDM = 0x52444D;
const int SDSU_VME_BOARD_REPLY_TIMEOUT = 3; //+ How long (milliseconds) to wait for reply
const int SDSU_VME_BOARD_RESET_TIME = 1000; //+ How long (milliseconds) to wait after reset
const int SDSU_VME_BOARD_WAIT_TIME = 200; //+ How long (milliseconds) to wait after SRA command 
const int SDSU_VME_BOARD_SETTLE_TIME = 0; //+ How long (milliseconds) to wait after command to settle
const int SDSU_INTERRUPT_LEVEL = 6;     //+ VME bus interrupt level (set by card jumper)
const int SDSU_REPLY_INT_NUM = 240;     //+ interrupt vector
const int SDSU_REPLY_BUF_LEN = 32;
const int REPLY_MASK = 0x00ffffff;
const int SDSU_REPLY_SIZE = 8;
const int SDSU_REPLY_ALIGN = 0x100;

/*---------------------------------------------------------------------------
  Local vars
  --------------------------------------------------------------------------*/
static unsigned long* reply_buf = NULL;
static int reply_idx;
//+ For the Synergy vgm5 direct vme address space is 0xC000000 + 0x1000000 (by default)
//+ the first 0x02000000 is for VME slave devices. The SDSU VME board needs to become
//+ bus master so board is jumpered beyond 0xC2000000
static unsigned long vmeAddress = 0xC2000000;     //+ VME board jumpered address
//+ For the MV2700 the VME master address base is 0x08000000
//static unsigned long vmeAddress = 0x08000000;   //+ VME board jumpered address
static unsigned long* pVmeAddress;                //+ converted local address
static unsigned long* resetAddress;               //+ converted local address, 4 bytes on from cmd address
static MSG_Q_ID reply_mq = NULL;                  //+ SDSU reply message queue
static SEM_ID command_sem = NULL;                 //+ semaphore guarding SDSU access
static unsigned long iCount;                      //+ reply interrupt counter
static int debug = 0;                             //+ global debug flag

/*---------------------------------------------------------------------------
 Local functions
 --------------------------------------------------------------------------*/
static void rest(int interval);
static void tdl_test(int board, int n);
static int send_cmd(int board_id, int cmd, int nargs, int *val);
static void print_reply_buf(int n);
extern "C" void handle_sdsu_reply_intrpt(int arg);

// Main entry point
int sdsuComms(int dbg)
{
  int ret = 0;
  int a = 0;
  int i;
  unsigned long* rb[0x100] = {NULL};
  bool mapped;
  unsigned long ival;
  unsigned long* ptr_vme_bus_addr;
  int narg[10];
  unsigned long msg;
  int c = 0;
  bool sdsu_int_enable = false;

  debug = dbg;

  // Allocate a reply buffer that the interface board can write into - must be 0x100 aligned
  // for VME case
  if (debug)
    printf("Allocating %d aligned bytes for reply buffer\n", SDSU_REPLY_BUF_LEN * sizeof (unsigned long));
  reply_buf = (unsigned long *) memalign (SDSU_REPLY_ALIGN, SDSU_REPLY_BUF_LEN * sizeof (unsigned long));
  if (reply_buf == NULL) {
    printf("Unable to allocate aligned memory for SDSU replys\n");
    return -1;
  }
  printf("Reply buffer of size %d allocated ok, addr=%p\n", SDSU_REPLY_BUF_LEN * sizeof (unsigned long), reply_buf);

  reply_idx = 0;

  try {
    
    // Map the address of the SDSU/VME interface card on the bus
    if (debug)
      printf("Mapping SDSU VME interface card at VME address %lx\n", vmeAddress);
    if (sysBusToLocalAdrs (VME_AM_EXT_USR_DATA , (char *) vmeAddress, (char**)&pVmeAddress) == ERROR) {
      printf("Failed to map address (%lx) of SDSU interface on VME bus\n",vmeAddress);
      throw errno;
    }
    
    printf("SDSU command register is mapped to local address %p\n", pVmeAddress);
    
    // Check that the card is present by writing to the command register    
    if (debug)
      printf("Probe command register with write...");
    if (vxMemProbe((char *) pVmeAddress, VX_WRITE, sizeof(unsigned long), (char *) &ival) == ERROR) {
      perror("SDSU interface not present on VME bus");
      throw errno;
    }
    if (debug)
      printf("OK\n");
    
    if (debug)
      printf("Mapping SDSU VME reset address at VME address %lx\n", vmeAddress+4);
    if (sysBusToLocalAdrs (VME_AM_EXT_USR_DATA, (char *) vmeAddress+4, (char**)&resetAddress) == ERROR) {
      printf("Failed to map reset address %lx of SDSU interface on VME bus\n",vmeAddress+4);
      throw errno;
    }
    
    printf("SDSU reset register is mapped to local address %p\n", resetAddress);
    
    // Check that the card is present by writing to the command register    
    if (debug)
      printf("Probe reset register with write...");
    if (vxMemProbe((char *) resetAddress, VX_WRITE, sizeof(unsigned long), (char *) &ival) == ERROR) {
      perror("Could not probe reset address!");
      throw errno;
    }
    if (debug)
      printf("OK\n");

    // Wait for the board reset to work
    rest(SDSU_VME_BOARD_RESET_TIME);

    if (debug)
      printf("Setting up the reply buffer...");
    
    mapped = false;
    do {
      // Map local reply buffer address to VME bus, ready to be
      // passed to the VME board to set the host's base address
      if (sysLocalToBusAdrs(VME_AM_EXT_USR_DATA, (char *) reply_buf, (char **)
			    &ptr_vme_bus_addr) == ERROR) {
	// Try allocating another buffer - first save pointer to previously failed allocation
	rb[a++] = reply_buf;
	reply_buf = (unsigned long *) memalign (SDSU_REPLY_ALIGN, SDSU_REPLY_BUF_LEN * sizeof (unsigned long));
	if (reply_buf == NULL) {
	  printf("Unable to allocate aligned memory for SDSU replys\n");
	  break;
	}
      } else
	mapped = true;
    } while (!mapped && (a<0x100));

    // Free failed reply buf allocations
    for (i=0; i<a; i++)
      free(rb[i]);

    if (!mapped) {
      printf("Failed to map local reply buffer %p address to VME bus address (%d attempts)!\n", reply_buf, a);
      throw -1;
    }
	
    // Clear reply buffer
    memset((void*)reply_buf,0,SDSU_REPLY_BUF_LEN*sizeof(unsigned long));

    // flush the cache to ensure these values are up to date.
    if (cacheFlush(DATA_CACHE, reply_buf, SDSU_REPLY_BUF_LEN*sizeof(unsigned long)) == ERROR) {
      printf("Cache flush for reply buffer failed\n");
    }

    if (debug)
      printf("Reply buffer mapped to: %p (%d attempts required)\n", ptr_vme_bus_addr, a+1);
    
    // Create a message queue for handling replys
    if ((reply_mq = msgQCreate(10, sizeof(unsigned long), MSG_Q_FIFO)) == NULL) {
      perror("Unable to create message queue for SDSU replys");
      throw errno;
    }

    if ((command_sem = semMCreate (SEM_Q_PRIORITY | SEM_DELETE_SAFE | SEM_INVERSION_SAFE)) == NULL) {
      perror("Unable to create semaphore for SDSU access");
      throw errno;
    }
    semGive(command_sem);

   // Connect the reply interrupt handler, first disable any interrupts
    sysIntDisable(SDSU_INTERRUPT_LEVEL);

    iCount = 0;

    if (intConnect(INUM_TO_IVEC(SDSU_REPLY_INT_NUM), (VOIDFUNCPTR) handle_sdsu_reply_intrpt, 0) == ERROR) {
      perror("Unable to install VME reply interrupt handler!");
      throw errno;
    }
    sdsu_int_enable = true;

    // Now reenable interrupts
    if (sysIntEnable(SDSU_INTERRUPT_LEVEL) == ERROR) {
      perror("Unable to re-enable interrupts after setting SDSU reply handler!");
      throw errno;
    }
   
    // Now send an SRA command to set the reply buffer address
    
    // pass top 16 bits of address and bottom 16 bits of addres as two args to SRA
    narg[0] = ((int) ptr_vme_bus_addr >> 16) & 0xffff;
    narg[1] = (int) ptr_vme_bus_addr & 0xffff;
 
    if (debug)
      printf("Sending SRA to board with args 0x%x and 0x%x\n", narg[0], narg[1]);
    
    if (send_cmd(VME_ID, SRA, 2, narg) == -1) 
      throw -1;
    else if (debug)
      printf("OK\n");    

    // Clear the reply mq
    c = 0;
    while (msgQNumMsgs(reply_mq) > 0) {
      msgQReceive(reply_mq, (char*)&msg, sizeof(msg), NO_WAIT);
      c++;
    }

    if (debug)
      printf("%d messages cleared\n", c);
    
    if (debug)
      printf("Sending WRM to board with option to enable interrupts\n");

    // Tell the VME board to interrupt on replys. This is done by setting bit 1 of options word x:1
    narg[0] = 0x200001;
    narg[1] = 1;
    if (send_cmd(VME_ID, WRM, 2, narg) == -1) 
      throw -1;
    else if (debug)
      printf("OK\n");
   
    tdl_test(VME_ID, 10);
    tdl_test(TIM_ID, 10);
  }
  catch (int& err) {
    ret = err;
  }
  if (reply_buf != NULL)
    free(reply_buf);

  if (reply_mq != NULL)
    msgQDelete(reply_mq);

  if (command_sem != NULL)
    semDelete(command_sem);

  if (sdsu_int_enable) {
    sysIntDisable(SDSU_INTERRUPT_LEVEL);
    intDisconnect(SDSU_REPLY_INT_NUM);
  }

  return ret;
}
/*
 *+ 
 * FUNCTION NAME: rest
 * 
 * INVOCATION: rest(int interval)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > interval - reset period msec
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Sleep for interval ms
 * 
 * DESCRIPTION: Uses nanosleep to sleep for requested time.
 * Ensures the system clock can handle by adjusting and then
 * resetting after rest period over.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
static void rest(int interval)
{
  struct timespec ts,tsr;
  int status;
  int clock;

  if (interval>0) {
    // make sure clock is running at sufficient resolution
    clock = sysClkRateGet();
    if (interval < 1000*1.0/clock) {
      sysClkRateSet(1000/interval);
      tsr.tv_sec = 0;
      tsr.tv_nsec = 1000000000/(1000/interval);
      clock_setres(CLOCK_REALTIME, &tsr);
    }
    tsr.tv_sec = interval/1000;
    tsr.tv_nsec = (interval - tsr.tv_sec*1000)*1000000;
    // Sleep for required period - if interrupted - then go back to sleep until
    // timer expires (or is < 1ms)
    while ((tsr.tv_sec>0) || (tsr.tv_nsec>1000000)) {
      ts = tsr;
      if ((status = nanosleep(&ts,&tsr)) == -1) {
	if (errno != EINTR) { 
	  perror("Error during rest (nanosleep)");
	}
      }
    }
    // Restore clock
    if (interval < 1000*1.0/clock) {
      sysClkRateSet(clock);
      tsr.tv_sec = 0;
      tsr.tv_nsec = 1000000000/clock;
      clock_setres(CLOCK_REALTIME, &tsr);
    }
  }
}

/*
 *+ 
 * FUNCTION NAME: tdl_test
 * 
 * INVOCATION: tdl_test() 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Runs a set of TDL tests to both TIMING and UTILITY boards
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
static void tdl_test(int board, int n) 
{
  int val;
  int i,err;

  printf("Running %d TDL tests to board %d ...\n", n, board);
  err = 0;
  for (i=0; i<n; i++) {
    val = send_cmd(board, TDL, 1, &i);
    if (val == -1) {
      err += n-i;
      break;
    } else if (val != i) {
      err++;
      printf("value sent:     %08x\n", i);
      printf("value received: %08x\n", val);
    }
  }
  printf("Done board %d tests %d/%d errors\n", board,err,n);
}
/*
 *+ 
 * FUNCTION NAME: send_cmd
 * 
 * INVOCATION: send_cmd(int board_id, int cmd, vint nargs, int* val)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Runs a SDSU command and returns reply
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
static int send_cmd(int board_id, int cmd, int nargs, int* val)
{
  int header;
  int dsp_last_header;
  int dsp_last_reply; 
  int cmd_buf[16];
  int i,timer;
  unsigned long msg;

  // Take the command buffer semaphore to ensure noone else
  // trys to talk to the SDSU while we are
  semTake(command_sem, WAIT_FOREVER);

  header = (board_id << 8) | (2 + nargs);

  cmd_buf[0] = header;
  cmd_buf[1] = cmd;

  if (debug)
    printf("In send_cmd, board_id=0x%x, cmd=0x%x, nargs=%d, header=0x%x, Args=",
	   board_id, cmd, nargs, header);

  for (i=0; i<nargs; i++) {
    cmd_buf[i+2] = val[i];
    if (debug) {
      if (i<nargs-1)
	printf("0x%x,",val[i]);
      else
	printf("0x%x",val[i]);	     
    }
  }
  if (debug)
    printf("\n");

  for (i=0; i<nargs+2; i++)
    *(pVmeAddress) = cmd_buf[i];

  if (cmd != SRA) {
    if (msgQReceive(reply_mq, (char*)&msg, sizeof(unsigned long), SDSU_VME_BOARD_REPLY_TIMEOUT * sysClkRateGet()) == -1) {
      perror("Error during mq receive");
    } else if (debug)
      printf("Got reply ok, msg=%ld, reply_buf flag=0x%lx\n", msg, (reply_buf[reply_idx] & 0xff000000));
  } else 
    rest(SDSU_VME_BOARD_RESET_TIME);

  // flush the cache to ensure these values are up to date.
  if (cacheInvalidate(DATA_CACHE, reply_buf, SDSU_REPLY_BUF_LEN*sizeof(unsigned long)) == ERROR) {
    perror("Cache invalidate for reply buffer failed");
  }

  timer=0;
  while (((reply_buf[reply_idx] & 0xff000000) == 0) && (timer < SDSU_VME_BOARD_WAIT_TIME*10)) {
    rest(SDSU_VME_BOARD_WAIT_TIME);
    if (cacheInvalidate(DATA_CACHE, reply_buf, SDSU_REPLY_BUF_LEN*sizeof(unsigned long)) == ERROR) {
      perror("Cache invalidate for reply buffer failed");
    }
    timer+=SDSU_VME_BOARD_WAIT_TIME;
  }

  header = (board_id << 16) | 2;
  if (debug)
    printf("header=%x,board_id=%d\n",header,board_id);
  dsp_last_header = (reply_buf[reply_idx] & REPLY_MASK);
  dsp_last_reply = (reply_buf[reply_idx+1] & REPLY_MASK);

  if ((dsp_last_header&0xffff00) != (header&0xffff00)) {
    printf("BAD REPLY HEADER, cmd: 0x%x, expected header:%08x, actual header:%08x, actual reply:%08x\n",
	   cmd, header, dsp_last_header, dsp_last_reply);
    print_reply_buf(SDSU_REPLY_BUF_LEN);
    rest(SDSU_VME_BOARD_SETTLE_TIME);
    semGive(command_sem);
    return -1;
  } else {

    reply_idx = (reply_idx+(dsp_last_header&0xff))%SDSU_REPLY_BUF_LEN;

    if (debug)
      printf("GOOD REPLY HEADER, expected header:%08x, actual header:%08x, actual reply:%08x, idx=%d\n",
	     header, dsp_last_header, dsp_last_reply, reply_idx);

    // Wait for SDSU_VME_BOARD_SETTLE_TIME msec for the SDSU to ready itself for next command
    // This time has been found by trial and error - if no wait then system will hang
    // if next command comes too quickly
    rest(SDSU_VME_BOARD_SETTLE_TIME);
    semGive(command_sem);
    return dsp_last_reply;
  } 
}
/*
 *+ 
 * FUNCTION NAME: print_reply_buf
 * 
 * INVOCATION: print_reply_buf(int n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: print the reply buffer
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
static void print_reply_buf(int n)
{
  int i;
  unsigned long w;

  printf("Reply Buffer Contents (idx=%d):\n", reply_idx);

  // flush the cache to ensure these values are up to date.
  if (cacheFlush(DATA_CACHE, reply_buf, SDSU_REPLY_BUF_LEN*sizeof(unsigned long)) == ERROR) {
    printf("Cache flush for reply buffer failed\n");
  }

  for (i=0; i<n; i++) {
    w = reply_buf[i];
    printf(" 0x%06lx(%ld)\n", w&REPLY_MASK, w&REPLY_MASK);
  }
}
/*
 *+
 * FUNCTION NAME:
 * handle_sdsu_reply
 *
 * INVOCATION:
 * handle_sdsu_reply(int arg)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > arg - argument to handler
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: VME interrupt handler for VME interface card replys
 *
 * DESCRIPTION:
 *  A message is sent indicating a reply has been deposited in the 
 *  SDSU reply buffer
 *
 * EXTERNAL VARIABLES:
 *  
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void handle_sdsu_reply_intrpt(int arg)
{
  if (debug)
    printf("In reply interrupt handler, arg = %d, iCount=%ld\n", arg, ++iCount);
  if (reply_mq != NULL) 
    msgQSend(reply_mq, (char*)&iCount, sizeof(unsigned long), NO_WAIT, MSG_PRI_NORMAL);
  return;
}



/******************************************************************************
 *
 * intDisconnect - un-install an ISR
 *
 * This routine un-installs an ISR from the interrupt vector table used by
 * the local and VMEbus interrupt dispatchers.

 * Following routines were found on the web - the VxWorks/TornadoII FAQ
 * and allow an ISR to be taken out of the ISR table.
 *
 *****************************************************************************/

STATUS intDisconnect(int vector)
{
  INT_HANDLER_DESC *handler;

  if (vector < 16) {
    printf("ERROR: cannot disconnect a local bus ISR with intDisconnect\n");
    return(ERROR);
  }
  if (vector > 255) {
    printf("ERROR: interrupt vector out of range\n");
    return(ERROR);
  }

  handler = (INT_HANDLER_DESC *)sysIntTbl[(int)vector];

  if (handler != NULL) {
    if (debug)
      printf("disconnecting vector %d \n", vector);

    handlerDelete(handler);
    sysIntTbl[(int)vector] = NULL;
  }
  return(OK);
}

/******************************************************************************
 *
 * handlerDelete() - recursively deletes ISRs from sysIntTbl vector
 *
 *****************************************************************************/

static int handlerDelete(INT_HANDLER_DESC *handler)
{
  if (handler->next != NULL) {
    handlerDelete(handler->next);
  }

  free(handler);
  handler = NULL;

  return(OK);
}

