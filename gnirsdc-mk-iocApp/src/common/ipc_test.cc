/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * NIFS PROJECT
 * 
 * FILENAME 
 *  ipc_test.cc
 * 
 * GENERAL DESCRIPTION
 *  Simple test routine for ipc_test tasks.
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 2002
 *
 * HISTORY
 *   Sept 2002 PJY    Created for VxWorks/NIFS 
 *
 * HISTORY
 * $Id: ipc_test.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * $Log: ipc_test.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.13  2005/07/12 06:57:58  pjy
 * moved cvs id
 *
 *
 */
/* Include files */
#include <vxWorks.h>
#include <time.h>
#include <iostream>
#include "ipc.h"
#include "test_ipc.h"

int ipcTest()
{
  Semaphore sem1("sem1",CONNECT,RWALL);
  Semaphore sem2("sem2",CONNECT,RWALL);
  int v,post;

  cout << "sem1 getval=" << sem_getvalue(sem1.semid,&v) << " v=" << v << endl;
  cout << "Post?";
  cin >> post;
  if (post)
    sem_post(sem1.semid);
  cout << "sem2 getval=" << sem_getvalue(sem2.semid,&v) << " v=" << v << endl;
  cout << "Post?";
  cin >> post;
  if (post)
    sem_post(sem2.semid);
  return 0;
}
