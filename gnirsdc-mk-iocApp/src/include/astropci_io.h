/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: astropci_io.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GSAOI PROJECT
 * 
 * FILENAME astropci_impl.h
 * 
 * GENERAL DESCRIPTION 
 *
 * This is a VxWorks version of the device driver to communicate with
 * the SDSU PCI interface board (PMC format). 

 * This file contains I/O control commands for the VxWorks device driver for the
 * SDSU PCI Interface Card. These commands are for use with ioctl(). This is
 * based on the Solaris version of this file.

 * Original Author: Scott Streit (SDSU) - Solaris version 1.0 - 12 Nov 1999
 * Ported by: peter Young (RSAA) - For the Gemini Telescope Project (GSAOI) - December 2003
 *
 * $Log: astropci_io.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.3  2005/06/07 06:12:10  pjy
 * Added PCI error defs
 *
 * Revision 1.2  2005/02/25 04:45:53  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.2  2004/03/04 03:26:20  pjy
 * Astro pci port work
 *
 * Revision 1.1  2004/02/16 22:10:09  pjy
 * Initial commit of vxWorks astropci device driver code.
 * This code successfully performs the basic SDSU operations exercised by the test code (ie reset,download(PCI,TIM),readout(RDC) etc).
 *
 *
 * 
 */
#ifndef __ASTROPCI_IO_H__
#define __ASTROPCI_IO_H__

#include <cstdlib>
#include <cstdint>

/*
 * Add IOCTL command definitions for Sdsu_Controller class usage 
 */

#define ASTRO_READ_STATUS            0xA01
#define ASTRO_GET_PROGRESS          0xA02
#define ASTRO_READS_DONE            0xA03
#define ASTRO_READ_RUNNING          0xA04
#define ASTRO_GET_SETUP             0xA05
#define ASTRO_READ_TIME              0xA06
#define ASTRO_GET_RDC               0xA07
#define ASTRO_PRINT_REPLY_BUF        0xA08

/*
 * Action commands
 */
#define ASTRO_COMMAND               0xA015
#define ASTRO_STOP                          0xA016
#define ASTRO_RESET                 0xA017
#define ASTRO_ABORT                         0xA018
#define ASTRO_FLUSH                         0xA019

/*
 * Setup commands
 */
#define ASTRO_SET_TIMEOUT           0xA107
#define ASTRO_SETUP                 0xA110
#define ASTRO_SIMULATE              0xA111
#define ASTRO_DEBUG                 0xA112

#define ASTRO_CMD_MAX               6
#define ASTRO_MAX_GW                4

#ifndef _ASTRO_SETUP
#define _ASTRO_SETUP
/* 
 * some setup parameters 
 */
typedef struct {
  uint32_t buffer_size;                 /* Buffer size in bytes */
  uint32_t image_size;                  /* image size in bytes */
  uint32_t nreads;                      /* Number of read sets in this exposure */
  uint32_t nsamples;                    /* Number of samples in a read set */
  uint32_t read_interval;               /* Time interval between read sets - msec */
  uint32_t read_time;                   /* Time to read one frame - msec */
  uint32_t nresets;                     /* Number of resets to perform */
  uint32_t reset_delay;                 /* Time interval after reset - msec */
  uint32_t namps;                       /* Number of amps in readout */
} Astro_Setup;

/* 
 * some guide setup parameters 
 */
typedef struct {
  uint32_t buffer_addr;                 /* GW buffer address */
  uint32_t buffer_size;                 /* Buffer size in bytes */
  uint32_t nsamples;                    /* Number of fowler samples in a CDS read*/
  uint32_t read_interval;               /* Minimum time interval between read sets - msec */
  uint32_t read_time;                   /* Expected time to read one frame - msec */
  uint32_t gw_mask;                     /* Which GWs are to be used */
  uint32_t gw_size;                     /* Number of rows&cols in each GW */
  uint32_t gw_mult[ASTRO_MAX_GW];       /* Multiple of read_interval for each GW */
  uint32_t gw_xo[ASTRO_MAX_GW];         /* X offset to start of each GW */                 
  uint32_t gw_yo[ASTRO_MAX_GW];         /* Y offset to start of each GW */                 
} Astro_Guiding_Setup;

#endif 

#ifndef _ASTRO_READOUT_STATUS
#define _ASTRO_READOUT_STATUS
/*
 * Device driver readout status structure
 */
typedef struct {
  uint64_t bytes_remaining;             /* bytes remaining in full read */
  uint32_t readouts_done;               /* readouts done so far in multiple IR read */
  uint32_t read_running;                /* number of read running */
  uint64_t last_readout_time;           /* time required for last readout (usecs)*/
} Astro_Readout_Status;
#endif

#define N_BOARDS  4                          /* interface,timing,utility,coadder*/
#define N_MEMS 3                             /* P,X,Y */
#define MAX_MEM 0x3fff                       /* 16k words */

/*
 * DSP/PCI Control Registers
 */
typedef struct {
  volatile uint32_t reserved1;          /* DSP Reserved register */
  volatile uint32_t reserved2;	        /* DSP Reserved register */
  volatile uint32_t reserved3;	        /* DSP Reserved register */
  volatile uint32_t reserved4;	        /* DSP Reserved register */
  volatile uint32_t hctr;	        /* 0x10 Host interface control register on DSP */
  volatile uint32_t hstr;	        /* 0x14 Host interface status register on DSP */
  volatile uint32_t hcvr;	        /* 0x18 Host command vector register on DSP */
  volatile uint32_t reply_buffer;       /* 0x1C Command reply buffer */
  volatile uint32_t cmd_data;	        /* 0x20 DSP command data register */
  volatile uint32_t unused1;	        /* 0x24 Unused */
  volatile uint32_t unused2;	        /* 0x28 Unused */
  volatile uint32_t unused3;	        /* 0x2C Unused */
  volatile uint32_t unused4;	        /* 0x30 Unused */
  volatile uint32_t unused5;	        /* 0x34 Unused */
  volatile uint32_t unused6;	        /* 0x38 Unused */
  volatile uint32_t unused7;	        /* 0x3C Unused */
  volatile uint32_t unused8;  	        /* 0x40 Unused */
  volatile uint32_t unused9;	        /* 0x44 Unused */
} Astropci_Regs ;

/*
 * Astropci_State - Driver state structure.  All state variables related
 * to the state of the driver are kept in here.
 */
typedef struct {
  Astropci_Regs *control_registers;     /* DSP/PCI control registers */
  
  /* operation flags */
  int open;		                /* Flag set when driver opened              */
  int busy;		                /* Set when transfers in progress           */
  int intr_busy;		        /* Set when interrupt service in progress   */
  int ioctl_busy;                       /* set when an ioctl operation in progress */
  int read_busy;                        /* set when a read is running */
  int rdc_sent;                         /* set when and RDC command has been trapped */
  int stop;                             /* TRUE if STOP ioctl received */
  int abort;                            /* TRUE if ABORT ioctl received */
  int simulate;                         /* TRUE if in simulation mode */
  int sim_dsp_mem[N_BOARDS][N_MEMS][MAX_MEM];/* simulate DSP memory */
  int timeout_mult;                     /* standard timeout multiplier */
 
  /* Current DMA address */
  uint32_t image_address;               /* Current image DMA address */

  /* Readout setup information */
  uint32_t nreads;                      /* Number of reads to do in multiple IR read*/
  uint32_t nsamples;                    /* Number of samples to do in multiple IR read*/
  uint32_t namps;                       /* Number of readout amps */
  uint32_t readouts_done;               /* readouts done so far in multiple IR read */
  uint32_t read_running;                /* number of the current read running */
  uint32_t read_wait;                   /* length of time to wait for first read msecs*/
  uint32_t read_interval;               /* length of time between succesive IR readouts msecs*/
  uint32_t nresets;                     /* Number of resets to perform */
  uint32_t reset_delay;                 /* length of time to delay before expecting first data after a reset msec*/
  uint32_t buffer_size;                 /* size of cyclic output buffer in bytes - at least 2ximage_size*/
  uint32_t image_size;                  /* size of one image frame */
  uint32_t exptime;                     /* exposure time set with SET command */
  uint32_t read_time;                   /* Time expected to read one frame - msec */
  uint64_t readout_time;                /* Time actually required for a readout usec */
  uint32_t pixel_counter;               /* pixel counter */
  uint32_t last_pixel_counter;          /* last value of pixel counter */
  uint32_t last_retries;                /* last value of retry counter */
  uint32_t last_disconnects;            /* last value of disconnect counter */

  /* VxWorks device information */
  uint16_t vendor;
  int device;
  int devFn;
  uint8_t devno;   
  int bus;
  int instance;                         /* pci board instance number */
  int devAdd;                           /* set when iosDevAdd successful */

  /* interrupt control info*/
  int isr_tid;                          /* interrupt handling task */
  int intr_line;                        /* interrupt line       */
  int intr_count;                       /* coun of number of interrupts received */
  int sdsu_int_enable;                  /* interrupts enabled flag */
} Astropci_State;

#endif
