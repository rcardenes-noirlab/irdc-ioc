/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 * $Id
 * GNIRS PROJECT
 * 
 * FILENAME 
 *   gnirs_compressed_image.cc
 * 
 * GENERAL DESCRIPTION
 *   Implementation of gnirs_compressed_image class.
 *
 * ORIGINAL AUTHOR: 
 *   Peter Young
 *
 * HISTORY
 *
 *  November 2004 - PJY Created.
 *
 *INDENT-ON
 *
 * $Log: gnirs_compressed_image.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Gnirs; first gemini-modified release
 *
 * Revision 1.3  2005/07/21 04:06:19  pjy
 * Added support for expanding the compressed image so that it is square (as per ICD)
 *
 * Revision 1.2  2005/07/12 07:07:08  pjy
 * Adjusted to fit actual GNIRS data
 *
 * Revision 1.1  2004/12/22 06:13:50  pjy
 * DC data src files ready for first Gemini release
 *
 *
 -------------------------------------------------------------------------*/
#ifdef vxWorks
#include <vxWorks.h>
#endif

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/

#include <errno.h>
#include <math.h>
#include "gnirs_compressed_image.h"
#include "fits.h"
#include "exception.h"

/*---------------------------------------------------------------------------
 External functions
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 External variables
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Static variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Global variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Class definitions
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Non-member functions
 -------------------------------------------------------------------------*/

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: GNIRS_Compressed_Image
 * 
 * INVOCATION: cmp = new GNIRS_Compressed_Image(double _grating_min, double _grating_max, double _wave_min, double _wave_max, 
 *                                             int _nslices, int _slice_height, int _spectral_width, int _first_row)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > _grating_min - min grating setting
 * > _grating_max - max grating setting 
 * > _wave_min - min wavelength range to sum
 * > _wave_max - max wavelength range to sum
 * > _nslices - number of slices to consider
 * > _slice_height - height of each slice
 * > _spectral_width - width of spectral data
 * > _first_row - first raw data row with slice data
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Normal constructor to use to create compressed image
 * 
 * DESCRIPTION: 
 *
 * This routine uses approximate values for each GNIRS slit size and position. In reality the slitlets vary in
 * size by fractions of a pixel, this is not accounted for. For purposes of QL display the approximation used
 * here is close enough
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
GNIRS_Compressed_Image::GNIRS_Compressed_Image(double _grating_min, double _grating_max, double _wave_min, double _wave_max, 
					     int _nslices, int _slice_height, int _spectral_width, int _first_row)
{
  char msgbuf[256];

  grating_min = _grating_min;
  grating_max = _grating_max;
  wave_min = _wave_min;
  wave_max = _wave_max;
  nslices = _nslices;
  slice_height = _slice_height;
  spectral_width = _spectral_width;
  first_row = _first_row;
  image = NULL;

  // Some checks

  // Size of compressed image = vector height
  nx = nslices;
  ny = slice_height;
  raw_npix = nx*ny;
  npix = raw_npix;

  // Are the slice parameters sensible
  if (first_row + raw_npix > GNIRS_HEIGHT) {
    snprintf(msgbuf,255,"Slices don't fit on detector, 1st=%d, n=%d, size=%d, (1st+n*size>%d)!",
	     first_row, nslices, slice_height, GNIRS_HEIGHT);
    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
  }

  if (spectral_width > GNIRS_SPECTRAL_WIDTH) {
    snprintf(msgbuf,255,"Width doesn't fit on detector, w=%d > size=%d!",
	     spectral_width, GNIRS_SPECTRAL_WIDTH);
    throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
  }

  // Fix min and max wavelength chosen to available range
  if (wave_min < grating_min)
    wave_min = grating_min;
  if (wave_max > grating_max)
    wave_max = grating_max;

  // Does wmin and wmax fit within spectral range?
  if ((wave_min>grating_max) || (wave_max<grating_min) || (wave_min>wave_max)) 
    throw Error("Wavelength selected outside available range!", E_ERROR, EINVAL, __FILE__, __LINE__);
  
  // Compute min and max column for summing
  xmin = (int) floor(spectral_width*((wave_min-grating_min)/(grating_max-grating_min)));
  xmax = (int) ceil(spectral_width*((wave_max-grating_min)/(grating_max-grating_min)));

  // Create the raw image
  raw_image = new float[raw_npix];

  // Set final image to point to raw_image by default
  image = raw_image;

  // Set values to zero
  memset((void*)image, 0, sizeof(float)*raw_npix);
  
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: GNIRS_Compressed_Image::add
 * 
 * INVOCATION: cmp->add(float* fp, int x, int y, int width, int height)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > fp - ptr to floating point pixel buffer
 * > x - x offset of chunk in detector coordinates
 * > y - y offset of chunk in detector coordinates
 * > width - width of chunk
 * > height - height of chunk
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Add a chunk of pixels to the compressed image
 * 
 * DESCRIPTION: 

 * Each GNIRS slice occupies SLICE_HEIGHT rows of the input image
 * and is spectral_width wide. Each slice is summed along the spectral
 * access to produce a compressed spectral column. Each summed slice is then
 * stacked side by side to form an image SLICE_HEIGHTxNSLICES in size.
 *
 * This routine adds an arbitrary rectangular part of the raw image to the 
 * constructed compressed image.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void GNIRS_Compressed_Image::add(float* fp, int x, int y, int width, int height)
{
  int s,h,r,c,i;

  for (r=y; r<y+height; r++) {
    if (r>=first_row) {
      // What slice are we in
      s = (r-first_row)/slice_height;
      // what row of the slice are we summing
      h = (r-first_row)%slice_height;
      // work out compressed image index
      i = h*nslices+s;

      // Sum this image slice
      for (c=x; c<x+width; c++) {
	if ((c>=xmin) && (c<=xmax)) {
	  image[i] += fp[(r-y)*width+(c-x)];
	}
      } // column loop
    } // if above first row
  } // row loop
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: GNIRS_Compressed_Image::expand
 * 
 * INVOCATION: cmp->expand(int xf, int yf)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > xf - width of chunk
 * > yf - height of chunk
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Expand the raw_image by x and y factors
 * 
 * DESCRIPTION: 
 * 
 * This routine allows for the rewriting of the raw_image into one
 * that is blown up in x and y directions with the aim to produce an
 * output image that is approximately square. Each pixl of the raw
 * image is simply copied by the expansion factors.
 *
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void GNIRS_Compressed_Image::expand(int xf, int yf)
{
  int r,c,r2,c2;

  if ((image != raw_image) && (image != NULL))
    delete image;

  nx = nslices*xf;
  ny = slice_height*yf;
  npix = nx*ny;

  image = new float[npix];

  for (r=0; r<slice_height; r++) {
    for (r2=0; r2<yf; r2++) {
      for (c=0; c<nslices; c++) {
	for (c2=0; c2<xf; c2++) {
	  image[(r*yf+r2)*nslices*xf+(c*xf+c2)] = raw_image[r*nslices+c];
	}
      }
    }
  }
	  
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: GNIRS_Compressed_Image::~GNIRS_Compressed_Image
 * 
 * INVOCATION: delete cmp;
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Destructor - frees memory
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
GNIRS_Compressed_Image::~GNIRS_Compressed_Image()
{
  if ((image != raw_image) && (image != NULL)) {
    delete [] image;
    image = NULL;
  }

  if (raw_image != NULL) {
    delete [] raw_image;
    raw_image = NULL;
  }
}

