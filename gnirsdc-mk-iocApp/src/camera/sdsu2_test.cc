/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 * 
 * CICADA PROJECT
 * 
 * FILENAME 
 *   sdsu2_test.cc
 * 
 * PURPOSE
 *   To exercise the interface between the cicada code and the dsp code, as well
 *   as exercise the dsp code itself.
 *
 * HISTORY
 * 
 */

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#include "common.h"
#include "config_table.h"
#include "sdsu2_controller.h"
#include "sdsu_setup.h"
#include "parameter.h"
#include "exception.h"


#ifdef vxWorks
  static int sdsu2Test_task();
  int sdsu2Test()
{
  int tid;

  // Start this in a spawned task with an appropriately sized stack
  if ((tid = taskSpawn("sdsu2Test", 100, VX_FP_TASK, 0x1000000, (FUNCPTR) sdsu2Test_task, 
		       0,0,0,0,0,0,0,0,0,0)) == ERROR) {
    printErr("taskSpawn of sdsu2Test_task failed, errno=%d!\n", errno,0,0,0,0,0);
  }

  return 0;
}
static int sdsu2Test_task()
#endif
{
  try {
    Sdsu2_Controller *sdsu_controller;
    Controller *controller=NULL;
    Instrument_Status *st=NULL;
    Config *cf=NULL;
    Camera_Desc cam;
    Semaphore *cs=NULL;
    Message *msg=NULL;
#ifdef vxWorks
    extern Config *shared_config;
#endif
    Parameter *ip=NULL;
    Parameter cp;
    Parameter op;
    Sdsu_Setup setup;
    Camera_Table ctable;
    char sname[IPCNAMELEN];
    Filenametype fname;
    string ph;
    string name;
    string cdir;                 
    Init init = {0};             // Prevents init() from re-reading the configuration 
    Report *report=NULL;
    double sum,min,max;
    unsigned long asize;
    int n;
    Region_Data regions;
    Detector_Regions *rgn=NULL;

    try {
      cout << "Running SDSU2 tests" << endl;

      // read in the camera configuration table    
#ifdef vxWorks
      Platform_Specific specific;
      strlcpy(fname,CAMERA_TABLE,FILENAMELEN);
#endif
      ctable.read(fname);
      
#ifdef vxWorks
      name = "GNIRS";
#endif
      
      cam = ctable[name.c_str()];

      cout << "Camera configuration " << name << " found ok" << endl;
      
      cout << "Creating SDSU controller with simple constructor ..." << endl;
      controller = new Sdsu2_Controller(cam, 0, 0, &specific);
      delete controller;
      controller = NULL;
      cout << " Done" << endl;

      cout << "Creating SDSU controller with parameter only constructor ..." << endl;
#ifdef vxWorks
      ph = "maestro";
      msg = new Message(0x4000,"Test:",false);
      st = new Instrument_Status;
      cf = new Config;
      shared_config = cf;
#endif
      
      st->id = 0;
      st->pid = 0;
      controller = new Sdsu2_Controller(cam, 0, 0, ph.c_str(), st, msg, &specific);
      cdir = controller->controller_dir;
      delete controller;
      controller = NULL;
      cout << " Done" << endl;

      cout << "Setting up for full SDSU constructor " << endl;
      // Semaphores
      ipc_name(sname, "Slave", "Crt", 0, 0);
      cs = new Semaphore(sname,ESTABLISH,RWOWN);
      cf->config_data.Config_Data_u.camera.desc = cam;
      cf->config_data.Config_Data_u.camera.cid = 0;
      ip = new Parameter();
      st->hardware_status.Hardware_Status_u.camera_status.controller_status.type = SDSU2;

      cout << "Reading sdsu2 setup file " << sdsu_path(SDSU_SETUP, cdir.c_str()) << endl;
      setup.read(sdsu_path(SDSU_SETUP,cdir.c_str()));
      setup.config.do_comms_test_at_startup = false;
      cf->config_data.Config_Data_u.camera.controller.Controller_Config_u.sdsu.config_data = setup.config;
      cout << " Done" << endl;

      // Needed by the constructor when debugging on.
      report = new Report("SDSU2_Test");

      cout << "Creating SDSU controller with full status and config structures ..." << endl;
      controller = sdsu_controller = new Sdsu2_Controller(st, msg, ip, &cp, &op, report, 
					0x00FF | SDSU_SHOW_ERRORS | SDSU_SHOW_COMMANDS, &specific);

      cout << "Done" << endl;

      init.controller.Controller_Init_Data_u.sdsu.config_data = setup.config;
      
      // Load the SDSU configuration and load the timing code into memory
      controller->message_write("Calling getconfig to read the DSP code");
      sdsu_controller->getconfig(TRUE);
      controller->message_write("Done");
     
      // Download the timing code to the controller
      sdsu_controller->init_test();
      
      // Do the tests
      sdsu_controller->run_dsp_tests();
      
      // Do voltage setup commands
      sdsu_controller->do_voltage_tests();
      
      // Test setting up an image readout buffer
      regions.xo[0] = 0;
      regions.yo[0] = 0;
      regions.width[0] = cam.controller[0].cols;
      regions.height[0] = cam.controller[0].rows;
      regions.rcf = regions.ccf = 1;
      controller->message_write("Creating readout region ...");
      rgn = new Detector_Regions(cam, 0, true, MOSAIC_NONE, true, 1, regions.xo, regions.width, 
				 regions.yo, regions.height, 1, 1, 0, 0, 255);
      controller->message_write("Done");

      Detector_Readout readout_rec;
      readout_rec.regions = regions;
      asize = regions.width[0]*regions.height[0]*2;
      controller->message_write("Setting up readout, image size=%ld", asize);
      controller->setup_readout(1, &readout_rec, rgn, asize, 1);
      controller->message_write("Done");

      controller->message_write("Creating an image buffer for %ld images, actual im size=%ld",
				cam.n_buffers, controller->actual_image_size);
      controller->create_image_buffer(controller->actual_image_size*cam.n_buffers);
      controller->message_write("Done");

      // Now test setting up a verify image, first create a default readout regions spec
      controller->message_write("Setting up verify data");
      controller->setup_verify_data(0);
      controller->message_write("Done.");

      controller->message_write("Starting an image transfer thread ...");

      // Now start the image readout thread and do a readout.
      if (cam.type == IR_CAMERA) {
	controller->ir_einfo.nperiods = 0;
	controller->ir_einfo.nfowler = 1;
	controller->readout_no = 0;
      }
      controller->start_image_transfer_thread();

      controller->message_write("Image transfer thread started - waiting fot it to complete ");
      if (controller->image_transfer_thr != NULL)
	delete controller->image_transfer_thr;
      controller->message_write("Image transfer Done!");

      controller->message_write("Computing stats on image readout buffer");
      n = regions.width[0]*regions.height[0];
      
      imstat(controller->image_buffer_ptr, n, controller->image_info.bitpix, sum, min, max);
      controller->message_write("Full image mean =%f, max=%f, min=%f, bitpix=%d, n=%ld",sum/n,max,min,
				controller->image_info.bitpix,n);
      cout << "SDSU2 test completed successfully - all tests passed" << endl;
    } 
    catch (Error &e) {
      if (controller != NULL)
	controller->message_write(e.record_error(__FILE__,__LINE__));
      else
	e.print_error(__FILE__,__LINE__);
      cout << ">>>>>>>> Abandoning SDSU2 test" << endl;
    }
    if (rgn) delete rgn;
    if (controller) delete controller;
    if (report) delete report;
#ifdef vxWorks
    if (st) delete st;
    if (cf) delete cf;
#endif
    if (cs) delete cs;
    if (ip) delete ip;
    if (msg) delete msg;
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return 0;
}
