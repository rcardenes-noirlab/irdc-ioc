/*
 * Copyright (c) 1994-1998 by MSSSO Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME sdsu3_controller.cc
 *
 * GENERAL DESCRIPTION
 * This module implements the SDSU generation 3 hardware class.
 * It is derived from the SDSU generation 2 class.
 */

/*---------------------------------------------------------------------------
 Include files
 --------------------------------------------------------------------------*/
#include "common.h"
#include "sdsu3_controller.h"

/* defines */

/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/* class function definitions */
/*============================================================================
 *  Sdsu3_Controller
 *===========================================================================*/
/*
 *+
 * FUNCTION NAME: Sdsu3_Controller constructors
 *
 * INVOCATION:
 *  sdsu_controller = new Sdsu3_Controller(cam,camid,c); or
 *  sdsu_controller = new Sdsu3_Controller(cam,camid,c,param_host,st)
 *  sdsu_controller = new Sdsu3_Controller(st,im,is,ss,ps,msg,cf,rep,deb);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > cam - Camera_desc - a description of the camera
 *  > camid - camera id
 *  > c - camera controller number
 *  > param_host - name of host holding parameter database
 *  > st - pointer to instrument status structure shared mem
 *  > im - pointer to image shared memory
 *  > is - pointer to image semaphore
 *  > ss - pointer to status semaphore
 *  > ps - pointer to process state semaphore
 *  > msg - pointer to message shared memory
 *  > cf - pointer to camera configuration shared memory
 *  > rep - pointer to debugging report
 *  > deb - debug level mask
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Simple constructor used for handling configuration without action
 *  Normal constructor used on active camera on instrument computer
 *
 * DESCRIPTION:
 * The Sdsu3_Controller class implements a Cicada camera interface to the Sdsu II
 * controller. THis inherits the basic functions of the Sdsu I controller and
 * adds some extensions.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu3_Controller::Sdsu3_Controller(Camera_Desc& cam, int camid, int c, Platform_Specific* ps)
  :Sdsu2_Controller(cam,camid,c,ps)
{
}
/*
 *+
 * FUNCTION NAME: Sdsu_Controller::Sdsu_Controller()
 *
 * INVOCATION: controller = new Sdsu_Controller(Camera_Desc cam, int camid, int c, const char* param_host,
 *                          Instrument_Status *st, paramMapType *pm)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam - description of camera that is parent of this controller. Also holds
 *         controller description
 * > camid - identification umber of parent camera
 * > c - identification number of this controller within the camera.
 * > param_host - name of host where parameter database resides.
 * > st - pointer to instrument status data structure for all instrument status
 * > pm - pointer to parameter map type
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 * Constructor used for initialising configuration variables and setting up the
 * parameter database
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generation II SDSU controller
 * object.
 *
 * This version of the controller is used just for holding a controllers
 * configuration information and also its status parameter database. It is not
 * used to interface with real hardware. It is, therefore, only useful in
 * modules that need to know about a configuration and be able to read/write
 * status parameters but do not actually interface with hardware.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu3_Controller::Sdsu3_Controller(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st,
				   Platform_Specific* ps)
  :Sdsu2_Controller(cam, camid, c, param_host, st, ps)
{
  controller_statusp->put(CONTROLLER_TYPE, (int) SDSU3);
}
/*
 *+
 * FUNCTION NAME: Sdsu_Controller::Sdsu_Controller()
 *
 * INVOCATION: controller = new Sdsu_Controller(Instrument_Status *st, Parameter* ip, Parameter* cp,
                                                Parameter* op, int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > st - pointer to instrument status data structure for all instrument status
 * > msg - pointer to ring buffer message object used for holding system messages
 * > ip - pointer to the parameter database for the instrument
 * > cp - pointer to the parameter database for the camera
 * > op - pointer to the operation parameter database
 * > deb - debug flag
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Constructor used for interfacing to real controller hardware
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generation II SDSU controller
 * object.
 *
 * This version of the controller is used for interfacing with real hardware. It
 * is, therefore, required for components that are attached to the hardware. Its
 * derived classes know how to communicate with specific controllers, while this
 * parent class can be used in a generic way to deal with all controller types.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu3_Controller::Sdsu3_Controller(Instrument_Status *st, Parameter* ip, Parameter* cp, Parameter* op,
				 int deb, Platform_Specific* ps)
  :Sdsu2_Controller(st,ip,cp,op,deb,ps)
{
  controller_statusp->put(CONTROLLER_TYPE, (int) SDSU3);

  // TIMING board is used for shutter control
  shutter_control_board = TIM_ID;
}

/*
 *+
 * FUNCTION NAME: Sdsu_Controller::get_dsp_version
 *
 * INVOCATION: ver = get_dsp_version(int board_id)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - DSP board to send command to
 *
 * FUNCTION VALUE: char*
 *
 * PURPOSE:
 *  Returns a version string of form Va.b.c
 *
 * DESCRIPTION:
 *
 * Retrieves the DSP version number located in a memory location in DSP memory.
 * This is a 24 bit number with the LS 8 bits the low order version number (c),
 * the middle 8 bits (b) and the top 8 bits the high order (a)
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
char* Sdsu3_Controller::get_dsp_version(int board_id)
{
  int swv,swv1,swv2,swv3;

  if (debug&DEBUG_INDICATORS)
    message_write("Getting DSP version number from board %d...", board_id);

  // Get software version number from controller address
  // This is a 24 bit number with the lower 12 bits is the controller
  // revision number and the upper 12 bits is the basic timing file revision number
  strcpy(sdsu_status->controller_swv,"");
  if ((swv = sdsu_command(board_id, SDSU_RDM, 0, SDSU_MEM_Y, SDSU_SW_VERSION_PAR)) != SDSU_ERR) {
    swv1 = swv >> 16;
    swv2 = (swv&0xFF00) >> 8;
    swv3 = swv&0xFF;
    snprintf(sdsu_status->controller_swv, NAMELEN, "V%d.%d.%d", swv1, swv2, swv3);
    if (debug&DEBUG_INDICATORS)
      message_write("DSP version (0x%x) V%d.%d.%d", swv, swv1, swv2, swv3);
  } else
    message_write("DSP version unknown - check??");

  return sdsu_status->controller_swv;
}
/*
 *+
 * FUNCTION NAME: Sdsu_Controller::do_set_boards()
 *
 * INVOCATION: do_set_boards()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: set board ids for various control functions
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_Controller::do_set_boards()
{
  // Some hardware configurations don't have a utility card - utility cmds
  // are handled by the timing card - set this up here
  if (sdsu_config.config_data.has_utility_board)
    util_id = UTIL_ID;
  else {
    util_id = TIM_ID;
  }

  // SDSU3 always does shutter control
  shutter_control_board = TIM_ID;
}

/*
 *+
 * FUNCTION NAME: Sdsu_Controller::do_power_on()
 *
 * INVOCATION: do_power_on()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: do power on
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_Controller::do_power_on()
{
  message_write("Turning SDSU controller power on...");
  if (sdsu_command(TIM_ID, SDSU_PON) != SDSU_OK)
    message_write("Power on can not be completed.");
  else
    message_write("     power on complete.");
}
/*
 *+
 * FUNCTION NAME: Sdsu3_Controller::controller_shutdown
 *
 * INVOCATION: sdsu->controller_shutdown
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Do a controller shutdown
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_Controller::controller_shutdown()
{
  message_write("Turning SDSU controller power off...");
  if (sdsu_command(TIM_ID, SDSU_POF) != SDSU_OK)
    message_write("Power off can not be completed.");
  else
    message_write("     power off complete.");

}

/*
 *+
 * FUNCTION NAME: get_regions_memory_address()
 *
 * INVOCATION:
 *  addr = get_regions_memory_address()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *  Regions start address
 *
 * PURPOSE:
 *  returns addres of DSP regions setup memory
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Sdsu3_Controller::get_regions_memory_address()
{
  const int SDSU_MIN_REGIONS_TABLE_SIZE = 0x10;
  int addr_size;
  char msgbuf[MSGLEN];   // Temporary message buffer

  addr_size = sdsu_command(TIM_ID, SDSU_RRT);

  // Top 12 bits are size of regions table available
  // while bottom 12 bits are actual Y mem address
  // For now do not use table size - we expect it to be ample
  // Just throw if way too small
  if (((addr_size&0xFFF000) >> 12) < SDSU_MIN_REGIONS_TABLE_SIZE) {
    sprintf(msgbuf,"Sdsu3_Controller: Regions memory area too small, is %d bytes, should be at least %d!",
	    (addr_size&0xFFF000) >> 12, SDSU_MIN_REGIONS_TABLE_SIZE);
    throw Error(msgbuf, E_ERROR, ENOMEM,__FILE__, __LINE__);
  }

  return addr_size&0xFFF;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_Controller::get_controller_info()
 *
 * INVOCATION: get_controller_info()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Queries the controller for its current info
 *
 * DESCRIPTION:
 *
 * Reads DSP version number
 * Reads status word and checks for power on
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_Controller::get_controller_info()
{
  int options_word = 0;
  bool pon_bit = false;

  // Get the DSP version number
  sdsu_statusp->put(CONTROLLER_SWV, get_dsp_version(util_id), 0, NAMELEN);

  // Check options word

  // Read the current SDSU_X:STATUS
  options_word = sdsu_command(TIM_ID, SDSU_RSW);
  if ((options_word != SDSU_ERR) && (options_word != (SDSU_TOUT&REPLY_MASK)))
    pon_bit = ((options_word & SDSU3_PON_BIT) == SDSU3_PON_BIT);

  sdsu_statusp->put(CONTROLLER_PON,  pon_bit);

}

/*
 *+
 * FUNCTION NAME: do_test_pattern
 *
 * INVOCATION:
 *  do_test_pattern();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Sets controllers test pattern mode
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_Controller::do_test_pattern()
{
  int ret_value = SDSU_OK;
  int options_word = 0;

  // Read the current SDSU_Y_OPTIONS_WORD_PAR
  options_word = sdsu_command(TIM_ID, SDSU_ROP);

  if (verify_spec.do_test_pattern) {
    message_write("Enabling test pattern...");

    // Set enable bit
    options_word |= SDSU3_TEST_PATTERN_BIT;

    // Set/clear constant type bit
    if (verify_spec.constant_val) {
      options_word |= SDSU3_TEST_PATTERN_CONST_BIT;
    } else if (options_word & SDSU3_TEST_PATTERN_CONST_BIT) {
      options_word = options_word ^ SDSU3_TEST_PATTERN_CONST_BIT;
    }

    // Set/clear test pattern source bit
//      if (sdsu_config.config_data.has_coadder_board && sdsu_config.config_data.test_pattern_by_coadder) {
//        options_word |= SDSU_TEST_PATTERN_SOURCE_BIT;
//      } else if (options_word & SDSU_TEST_PATTERN_SOURCE_BIT) {
//        options_word = options_word ^ SDSU_TEST_PATTERN_SOURCE_BIT;
//      }

    // Now write the new options word
    ret_value = sdsu_command(TIM_ID, SDSU_WOP, options_word);
    if (ret_value != SDSU_OK)
      message_write("Failed to enable test pattern options bits, err=%d", ret_value);

    // Now set the test pattern constant, seed and overscan values
    ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.seed, SDSU_MEM_Y, SDSU_TEST_PATTERN_SEED_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to set test pattern constant, err=%d", ret_value);

    ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.increment, SDSU_MEM_Y, SDSU_TEST_PATTERN_INC_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to set test pattern seed, err=%d", ret_value);

    ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.amp_increment, SDSU_MEM_Y, SDSU_TEST_PATTERN_AMP_INC_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to set test pattern amp increment, err=%d", ret_value);

    ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.readout_increment, SDSU_MEM_Y, SDSU_TEST_PATTERN_READ_INC_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to set test pattern readout increment, err=%d", ret_value);

    ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.overscan, SDSU_MEM_Y, SDSU_TEST_PATTERN_OS_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to set test pattern seed, err=%d", ret_value);
  } else if (options_word & SDSU_TEST_PATTERN_BIT) {
    message_write("Disabling test pattern...");
    options_word = options_word ^ SDSU3_TEST_PATTERN_BIT;
    ret_value = sdsu_command(TIM_ID, SDSU_WOP, options_word);
    if (ret_value != SDSU_OK)
      message_write("Failed to disable test pattern bit, err=%d", ret_value);
  }
}
/*
 *+
 * FUNCTION NAME:  Sdsu3_Controller::set_exposure_time
 *
 * INVOCATION: Sdsu3_Controller::set_exposure_time(int msec)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > msec - exposure time in msec to set
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Sets the exposure time
 *
 * DESCRIPTION: Uses the SET command to set the controllers exposure
 * time counter.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES: None
 *
 *- */
void Sdsu3_Controller::set_exposure_time(int msec)
{
  sdsu_command(shutter_control_board, SDSU_SET, msec);
}
/*
 *+
 * FUNCTION NAME:  Sdsu3_Controller::get_exposure_time
 *
 * INVOCATION: msec = Sdsu3_Controller::get_exposure_time()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: exposure time in msec
 *
 * PURPOSE: Gets the exposure time counter value
 *
 * DESCRIPTION: Uses the RET command to fetch the controllers exposure
 * time counter. If an SDSU_ERR is returned then it returns -1;
 * The time returned is the current value of the elapsed time counter which counts
 * up from zero. So actual exposed = this result + 1.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES: SDSU_ERR could be a legitimate exposure time???
 *
 *-
 */
int Sdsu3_Controller::get_exposure_time()
{
  int dspmsecs;
  if ((dspmsecs = sdsu_command(shutter_control_board, SDSU_RET)) == SDSU_ERR)
    throw Error("Error getting exposure time from SDSU3", E_ERROR, EFAULT,__FILE__, __LINE__);
  else
    return dspmsecs+1;
}

/*
 *+
 * FUNCTION NAME:  Sdsu3_Controller::read_current_voltage_settings
 *
 * INVOCATION: msec = Sdsu3_Controller::read_current_voltage_settings()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Read back voltage settings
 *
 * DESCRIPTION: Uses the RDM command to fetch the controllers voltage settings.
 * These voltages are described by the sdsu_volts configuration and include the
 * DSP address of the current setting. Each setting is converted from ADUs to
 * floating point voltage value using V = Vmax * ADU/ADU_Max.
 * The status value is then updated with converted voltage.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_Controller::read_current_voltage_settings()
{
  int v;
  int adu;

  for (v=0; v<sdsu_config.n_volts; v++ ) {
    // If in full simulation write - expected value first
    if (camera_status->controller_simulation == 2)
      sdsu_command(TIM_ID, SDSU_WRM,  calc_adu_val(sdsu_config.volts[v], V_LO, sdsu_config.volts[v].value[0]), SDSU_MEM_Y,
		   sdsu_config.volts[v].dsp_addr);
    if ((adu = sdsu_command(TIM_ID, SDSU_RDM, 0, SDSU_MEM_Y, sdsu_config.volts[v].dsp_addr)) != SDSU_ERR) {
      sdsu_status->voltage_value[v] = calc_volt_val(sdsu_config.volts[v], V_LO, adu);
      if (debug & DEBUG_INDICATORS)
	message_write("Read voltage '%s', adu=%d, value=%f", sdsu_config.volts[v].name, adu, sdsu_status->voltage_value[v]);
      sdsu_statusp->put(sdsu_config.volts[v].name, sdsu_status->voltage_value[v]);
    } else
      message_write("Unable to get voltage '%s'", sdsu_config.volts[v].name);
  }

}

/*
 *+
 * FUNCTION NAME: Sdsu3_Controller::~Sdsu3_Controller
 *
 * INVOCATION: delete sdsu3
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Destructor
 *
 * DESCRIPTION: Clear dsp hash tables
 *
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu3_Controller::~Sdsu3_Controller()
{
}
