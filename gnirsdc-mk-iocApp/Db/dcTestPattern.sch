[schematic2]
uniq 18
[tools]
[detail]
w 994 1163 100 0 n#1 ecad8.testPat.OUTC 960 1152 1088 1152 hwout.hwout#549.outp
w 994 1099 100 0 n#2 ecad8.testPat.OUTD 960 1088 1088 1088 hwout.hwout#550.outp
w 994 971 100 0 n#3 ecad8.testPat.OUTF 960 960 1088 960 hwout.hwout#563.outp
w 994 1035 100 0 n#4 ecad8.testPat.OUTE 960 1024 1088 1024 hwout.hwout#551.outp
w 994 1227 100 0 n#5 ecad8.testPat.OUTB 960 1216 1088 1216 hwout.hwout#548.outp
w 994 1291 100 0 n#6 ecad8.testPat.OUTA 960 1280 1088 1280 hwout.hwout#547.outp
w 314 939 100 0 n#7 ebos.testPatVerify.OUT 144 928 544 928 544 992 640 992 ecad8.testPat.F
w 506 1019 100 0 n#8 estringouts.testPatOS.OUT 496 1008 576 1008 576 1056 640 1056 ecad8.testPat.E
w 282 1115 100 0 n#9 estringouts.testPatReadInc.OUT 144 1104 480 1104 480 1120 640 1120 ecad8.testPat.D
w 570 1195 100 0 n#10 estringouts.testPatAmpInc.OUT 496 1168 560 1168 560 1184 640 1184 ecad8.testPat.C
w 362 1259 100 0 n#11 estringouts.testPatInc.OUT 144 1248 640 1248 ecad8.testPat.B
w 538 1323 100 0 n#12 estringouts.testPatSeed.OUT 496 1312 640 1312 ecad8.testPat.A
w 1026 667 100 0 n#13 cadPlus.testPatP.STAT 1120 656 992 656 992 1504 junction
w 1010 1723 100 0 n#13 ecad8.testPat.VAL 960 1504 992 1504 992 1712 1088 1712 outhier.VAL.p
w 978 651 100 0 n#14 ecad8.testPat.STLK 960 640 1056 640 1056 720 1120 720 cadPlus.testPatP.STIN
w 578 1483 -100 0 CLID inhier.CLID.P 528 1552 576 1552 576 1472 640 1472 ecad8.testPat.ICID
w 994 619 100 0 n#15 ecad8.testPat.SPLK 960 608 1088 608 1088 688 1120 688 cadPlus.testPatP.SPIN
w 562 1659 100 0 n#16 inhier.DIR.P 576 1648 608 1648 608 1504 640 1504 ecad8.testPat.DIR
w 962 1483 100 0 n#17 ecad8.testPat.MESS 960 1472 1024 1472 1024 1632 1088 1632 outhier.MESS.p
s 448 16 200 0 The initialisation operation is done
s -576 16 200 0 Test pattern initialisation CAD record.
s -576 -64 200 0 when the attached CADplus is triggered. SNL code monitors the CADPlus records.
[cell use]
use cadPlus 1120 768 100 0 testPatP
xform 0 1200 672
p 1216 656 100 0 1 set1:cad testPatP
use hwout 1088 983 100 0 hwout#551
xform 0 1184 1024
p 1184 1015 100 0 -1 val(outp):$(sadtop)testPatOS PP NMS
use hwout 1088 1047 100 0 hwout#550
xform 0 1184 1088
p 1184 1079 100 0 -1 val(outp):$(sadtop)testPatReadInc PP NMS
use hwout 1088 1111 100 0 hwout#549
xform 0 1184 1152
p 1184 1143 100 0 -1 val(outp):$(sadtop)testPatAmpInc PP NMS
use hwout 1088 1175 100 0 hwout#548
xform 0 1184 1216
p 1184 1207 100 0 -1 val(outp):$(sadtop)testPatInc PP NMS
use hwout 1088 1239 100 0 hwout#547
xform 0 1184 1280
p 1184 1271 100 0 -1 val(outp):$(sadtop)testPatSeed PP NMS
use hwout 1088 919 100 0 hwout#563
xform 0 1184 960
p 1184 951 100 0 -1 val(outp):$(sadtop)testPatVerify PP NMS
use ebos -48 864 100 0 testPatVerify
xform 0 16 960
p -432 814 100 0 0 ONAM:No
p -432 846 100 0 0 ZNAM:Yes
use estringouts 240 951 100 0 testPatOS
xform 0 368 1024
use estringouts -112 1047 100 0 testPatReadInc
xform 0 16 1120
use estringouts 240 1111 100 0 testPatAmpInc
xform 0 368 1184
use estringouts -112 1191 100 0 testPatInc
xform 0 16 1264
use estringouts 240 1255 100 0 testPatSeed
xform 0 368 1328
use ecad8 704 1552 100 0 testPat
xform 0 800 1056
p 736 1440 100 0 0 CTYP:6
p 736 1312 100 0 1 FTVA:LONG
p 736 1264 100 0 1 FTVB:LONG
p 736 1200 100 0 1 FTVC:LONG
p 736 1136 100 0 1 FTVD:LONG
p 736 1072 100 0 1 FTVE:LONG
p 736 1008 100 0 1 FTVF:LONG
p 736 944 100 0 1 FTVG:LONG
p 736 896 100 0 1 FTVH:LONG
p 704 544 100 0 1 SNAM:dcCADtestPat
use inhier 512 1511 100 0 CLID
xform 0 528 1552
use inhier 560 1607 100 0 DIR
xform 0 576 1648
use outhier 1056 1671 100 0 VAL
xform 0 1072 1712
use outhier 1056 1591 100 0 MESS
xform 0 1072 1632
use eborderC -784 -313 100 0 eborderC#87
xform 0 896 992
p 1792 -160 100 768 -1 author:Peter Young
p 1792 -20 40 0 -1 comment1:2020-05-01 Modified for GNIRS. Peter Young
p 1792 -192 100 768 -1 date:2020-05-01
p 2016 -112 200 768 -1 file:dcTestPattern
p 2288 -160 100 0 -1 page:1
p 2400 -160 100 0 -1 pages:1
p 2064 -160 100 0 -1 revision:0
p 2016 -48 150 768 -1 system:Gemini GNIRS DC
[comments]
