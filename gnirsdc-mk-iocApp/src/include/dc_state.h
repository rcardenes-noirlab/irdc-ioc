/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_state.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI PROJECT
 *
 * FILENAME dc_state.h
 *
 * PURPOSE
 * Include file with GNIRS DC state definitions
 *
 * $Log: dc_state.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.3  2005/09/23 01:30:42  pjy
 * Moved DC_Cmd def to dc_state.h so that snl could pick up
 *
 * Revision 1.2  2005/09/11 23:21:03  pjy
 * Added prototypes for enum << overloads
 *
 * Revision 1.1  2004/12/22 22:43:32  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.1  2004/08/20 02:18:26  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 *
 */

#ifndef DC_STATE_H
#define DC_STATE_H

#ifdef  __cplusplus
#include <string>
#endif

/* State each task can exist in */
enum class DC_State {
  DC_STOPPED,
  DC_INITIALIZING,
  DC_RUNNING,
  DC_ABORTING,
  DC_CONTROL_WAITING,
  DC_CONTROL_READY,
  DC_SLAVE_WAITING,
  DC_SLAVE_READY,
  DC_SLAVE_MONITOR,
  DC_SLAVE_BUSY,
  DC_SLAVE_CONFIGURE,
  DC_DATA_READY,
  DC_DATA_TRANSFERRING
};

/* Gemini OCS sequence commands */
enum class DC_Cmd {
  NOCMD,
  TEST,
  REBOOT,
  INIT,
  DATUM,
  PARK,
  APPLY,
  VERIFY,
  ENDVERIFY,
  GUIDE,
  ENDGUIDE,
  OBSERVE,
  ENDOBSERVE,
  PAUSE,
  CONTINUE,
  STOP,
  ABORT,
  DEBUG,
  DO_CONNECTION,
  CONTROLLER_INIT,
  CONTROLLER_CMD,
  CONTROLLER_SHUTDOWN,
  CONTROLLER_RESET,
  CONTROLLER_TEST,
  CONTROLLER_DEBUG,
  CONTROLLER_SIMULATE,
  CONTROLLER_INFO,
  CONTROLLER_CANCEL
};

#ifdef  __cplusplus
std::string to_string(DC_Cmd cmd);
std::string to_string(DC_State state);
#endif

#endif /* DC_STATE_H */
