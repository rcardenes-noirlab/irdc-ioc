/*
 * Copyright (c) 1994-1998 by MSSSO Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME sdsu_volts.cc
 * 
 * GENERAL DESCRIPTION
 * Implements an SDSU preferences class for bias and clock volts using base Cicada_Table class
 *
 */
#include "sdsu_volts.h"
#include "sdsu_controller.h"
/*
 *+ 
 * FUNCTION NAME: Sdsu_Volts::Sdsu_Volts():Config_Table("sdsu.volts","")
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: A constructor for sdsu volts file
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Sdsu_Volts::Sdsu_Volts():
  Config_Table("SDSU Volts", 3, false, (char*)&volt_desc, (char*)&volt_desc_save, sizeof(Volts_Desc))
{
  volt_desc.name[0] = '\0';
  volt_desc_save = volt_desc;
}
/*
 *+ 
 * FUNCTION NAME: void Sdsu_Volts::read(char* fname)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > fname - name of voltages config file
 * ! emsgs - list of error messages
 *
 * FUNCTION VALUE: TRUE if errors found in file, else FALSE
 * 
 * PURPOSE: to read a file of voltages into voltages table
 * 
 * DESCRIPTION: Uses base class ::read, but then checks
 *              contents of table for error conditions
 * 
 * EXTERNAL VARIABLES: pointer to emsgs STL list must be passed in
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: Doesn't check for every possible error that 
 *               Mark can dream up :-)
 *
 *- 
 */
int Sdsu_Volts::read(const string fname, list<string>* emsgs)
{
  
  Table_Item *item;
  Table_Item_Set::iterator item_iter;
  int i, j, nvoltages;
  char msgbuf[MSGLEN];
  int set_order[SDSU_MAX_VOLTS*2] = {0};

  // store pointer value for other functions to access
  em = emsgs;

  // call base class read method
  Config_Table::read(fname);

  // OK, have read table, now some sanity checks.  We need to do this because
  // the voltages config file is edited by hand for the addition and removal of
  // voltages and we need to check that the voltage set order has no duplicates,
  // values are within range, etc.  Writing the table out isn't a problem
  // because the GUI doesn't allow for set order changing or bounds breaking
  error = 0;
  for (item_iter=items.begin(), i=0, j=0; (item_iter != items.end()); item_iter++, i++) {
    item = *item_iter;
    // Now need to extract the individual fields
    get(item);
    if (i == SDSU_MAX_VOLTS) {
      snprintf(msgbuf, MSGLEN,"Too many voltages in %s - max allowed is %d", fname.c_str(), SDSU_MAX_VOLTS);
      throw Error(msgbuf,E_ERROR,-1,__FILE__,__LINE__);     
    } else {
      // store this set order position for later
      set_order[j] = volt_desc.set_order[0];
      j++;
      check_value(V_LO);
      // BIAS voltage on VID board has no "high" values, so don't check 'em
      if ((volt_desc.type != SDSU_BIAS) || 
	  (volt_desc.board_type != SDSU_VIDEO_BOARD)) {
	set_order[j] = volt_desc.set_order[1];
	j++;
	check_value(V_HI);
      }
    }
  }
  // store number of voltages for next loop
  nvoltages = j;

  // Have built up an array of set orders, now look for duplicates
  for (i=0; i < (nvoltages - 1); i++) {
    for (j=i+1; j < nvoltages; j++) {
      if (set_order[i] == set_order[j]) {
	snprintf(msgbuf, MSGLEN,"VOLTS ERROR: duplicate value for set order: %d",set_order[i]);
	report_error(msgbuf);
      }
    }
  }
  
  // error is incremented in report_error 
  return error;
}
/*
 *+ 
 * FUNCTION NAME: void Sdsu_Volts::check_value
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > i - index to low/high value in table item
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: to check voltages are within min/max bounds
 * 
 * DESCRIPTION: checks low/high voltages to ensure they are
 *              within the min/max bounds specified.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Sdsu_Volts::check_value(Sdsu_Voltage_Index i)
{
  int aduval;
  char msgbuf[MSGLEN];

  // Need to make sure that the voltage values are
  // within the bounds
  if (volt_desc.value[i] < volt_desc.min_volt[i]) {
    snprintf(msgbuf, MSGLEN,"VOLTS ERROR: value for %s < minvolt",volt_desc.name);
    report_error(msgbuf);
  }
  if (volt_desc.value[i] > volt_desc.max_volt[i]) {
    snprintf(msgbuf, MSGLEN,"VOLTS ERROR: value for %s > maxvolt",volt_desc.name);
    report_error(msgbuf);
  }
  
  // Check calibration values
  if (volt_desc.cal1_volt[i] == volt_desc.cal2_volt[i]) {
    snprintf(msgbuf, MSGLEN,"VOLTS ERROR: cal1volt = cal2volt for %s",volt_desc.name);
    report_error(msgbuf);
  }
  if (volt_desc.cal1_adu[i] == volt_desc.cal2_adu[i]) {
    snprintf(msgbuf, MSGLEN,"VOLTS ERROR: cal1adu = cal2adu for %s",volt_desc.name);
    report_error(msgbuf);
  }
  
  // Need to make sure that the computed ADU value
  // is within ADU bounds
  aduval = calc_adu_val(volt_desc, i, volt_desc.value[i]);
  if (aduval < volt_desc.min_adu_phys[i]) {
    snprintf(msgbuf, MSGLEN,"VOLTS ERROR: ADU value for %s < minaduphys",volt_desc.name);
    report_error(msgbuf);
  }
  if (aduval > volt_desc.max_adu_phys[i]) {
    snprintf(msgbuf, MSGLEN,"VOLTS ERROR: ADU value for %s > maxaduphys",volt_desc.name);
    report_error(msgbuf);
  }
  
  // Check the set order
  if (volt_desc.set_order[i] < 0) {
    snprintf(msgbuf, MSGLEN,"VOLTS ERROR: Set order for %s < 0",volt_desc.name);
    report_error(msgbuf);
  }
  
  // Check the set delay
  if (volt_desc.set_delay[i] < 0) {
    snprintf(msgbuf, MSGLEN,"VOLTS ERROR: Set delay for %s < 0",volt_desc.name);
    report_error(msgbuf);
  }
    
}
/*
 *+ 
 * FUNCTION NAME: void Sdsu_Volts::report_error(char* msgbuf)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > msgbuf - error message to be reported
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Print an error message 
 * 
 * DESCRIPTION: Add error message to list
 * 
 * EXTERNAL VARIABLES: both error and em are private variables
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Sdsu_Volts::report_error(char* msgbuf)
{
  error++;
  em->push_back (msgbuf);
}
/*
 *+ 
 * FUNCTION NAME: void Sdsu_Volts::put(void* data, int pos)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > data - item pointer - optionally put pars from this pointer arg
 * > pos - position in list to place item
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Puts a new volts structure into internal table 
 * 
 * DESCRIPTION: Uses table put methods to place each volts parameter
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Sdsu_Volts::put(void* data_item, int pos)
{
  Table_Item* item;
  char msgbuf[MSGLEN];
  Volts_Desc* tvolts = (Volts_Desc*) data_item;
  string str;
  int nitems;

  if ((item = find(tvolts->name)) != NULL) {
    snprintf(msgbuf, MSGLEN,"Voltage %s already exists in table!",tvolts->name);
    throw Error(msgbuf,E_ERROR,-1,__FILE__,__LINE__);     
  }

  item = new Table_Item(tvolts->name, "=", ";", FALSE, pos);
  
  switch (tvolts->set_this_voltage) {
  case TRUE: str = "yes"; break;
  default: str = "no";
  }
  item->put("set",str);
  switch (tvolts->type) {
  case SDSU_CLOCK: str = "CLOCK"; break;
  case SDSU_BIAS: str = "BIAS"; break;
  }
  item->put("volttype",str);
  item->put("boardaddr",tvolts->board_addr);
  item->put("dspaddr",tvolts->dsp_addr);
  switch (tvolts->board_type) {
  case SDSU_VIDEO_BOARD: str = "VID"; break;
  case SDSU_CLOCK_BOARD: str = "CLK"; break;
  }
  item->put("boardtype",str);
  item->put("precision",tvolts->precision);
  item->put("comment",tvolts->comment);

  // BIAS voltage on VID board has no "high" value
  if ((tvolts->type != SDSU_BIAS) || 
      (tvolts->board_type != SDSU_VIDEO_BOARD)) 
    nitems = SDSU_MAX_VOLT_VALS;
  else
    nitems = 1;
  item->put("value",tvolts->value,nitems);
  item->put("dacaddr",tvolts->dac_addr,nitems);
  item->put("setorder",tvolts->set_order,nitems);
  item->put("setdelay",tvolts->set_delay,nitems);
  item->put("cal1volt",tvolts->cal1_volt,nitems);
  item->put("cal2volt",tvolts->cal2_volt,nitems);
  item->put("minvolt",tvolts->min_volt,nitems);
  item->put("maxvolt",tvolts->max_volt,nitems);
  item->put("cal1adu",tvolts->cal1_adu,nitems);
  item->put("cal2adu",tvolts->cal2_adu,nitems);
  item->put("minaduphys",tvolts->min_adu_phys,nitems);
  item->put("maxaduphys",tvolts->max_adu_phys,nitems);

  insert(item,pos);
  // Cater for situations where put is called with external item
  // Usually will simply be putting back value in internal buffer 
  if ((void*)&volt_desc != data_item) {
    volt_desc = volt_desc_save = *tvolts;
  }
}	  
/*
 *+ 
 * FUNCTION NAME: void Sdsu_Volts::get(Table_Item *item)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > item - get sdsu pars from this table item
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Gets a volts structure from internal table
 * 
 * DESCRIPTION: Uses table find methods to fetch each volts par
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: parameter names passwd to find must be
 *               all lower case.
 *
 *- 
 */
void Sdsu_Volts::get(Table_Item *item)
{
  Volts_Desc tvolts;
  Nametype pname;
  string str;
  int nitems;

  // ** DON'T USE MIXED CASE IN PARAM NAMES HERE
  memset((void*)&tvolts, 0, sizeof(Volts_Desc));

  strlcpy(tvolts.name, item->name.c_str(), NAMELEN);
  tvolts.type = SDSU_CLOCK;
  tvolts.board_type = SDSU_VIDEO_BOARD;

  // Now for each possible volt parameter try to find in table
  item->find("set",pname,NAMELEN);
  str=pname;
  if (str == "yes") tvolts.set_this_voltage = TRUE;
  else tvolts.set_this_voltage = FALSE;
  item->find("volttype",pname,NAMELEN);
  str = pname;
  if (str == "CLOCK") tvolts.type = SDSU_CLOCK;
  else if (str == "BIAS") tvolts.type = SDSU_BIAS;
  item->find("boardaddr",tvolts.board_addr);
  item->find("dspaddr",tvolts.dsp_addr);
  item->find("boardtype",pname,NAMELEN);
  str = pname;
  if (str == "CLK") tvolts.board_type = SDSU_CLOCK_BOARD;
  else if (str == "VID") tvolts.board_type = SDSU_VIDEO_BOARD;
  item->find("precision",tvolts.precision);
  // BIAS voltage on VID board has no "high" value
  if ((tvolts.type != SDSU_BIAS) || 
      (tvolts.board_type != SDSU_VIDEO_BOARD)) 
    nitems = SDSU_MAX_VOLT_VALS;
  else
    nitems = 1;
  item->find("comment",tvolts.comment,COMMENTLEN);
  item->find("value",tvolts.value,nitems);
  item->find("dacaddr",tvolts.dac_addr,nitems);
  item->find("setorder",tvolts.set_order,nitems);
  item->find("setdelay",tvolts.set_delay,nitems);
  item->find("cal1volt",tvolts.cal1_volt,nitems);
  item->find("cal2volt",tvolts.cal2_volt,nitems);
  item->find("minvolt",tvolts.min_volt,nitems);
  item->find("maxvolt",tvolts.max_volt,nitems);
  item->find("cal1adu",tvolts.cal1_adu,nitems);
  item->find("cal2adu",tvolts.cal2_adu,nitems);
  item->find("minaduphys",tvolts.min_adu_phys,nitems);
  item->find("maxaduphys",tvolts.max_adu_phys,nitems);
  
  volt_desc = volt_desc_save = tvolts;
}
/*
 *+ 
 * FUNCTION NAME: Volts_Desc& Sdsu_Volts::operator[]
 * 
 * INVOCATION: operator[](int idx)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: overload [] operator
 * 
 * DESCRIPTION: used to lookup table entries by integer index (like an array lookup)
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Volts_Desc& Sdsu_Volts::operator[](int idx)
{
  get_element(idx);
  return volt_desc;
}
// Return a reference to the element named in the table - updates internal buffer
// and sets flag
/*
 *+ 
 * FUNCTION NAME: Volts_Desc& Sdsu_Volts::operator[]
 * 
 * INVOCATION: operator[](const char* item_name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: overload [] operator
 * 
 * DESCRIPTION: used to lookup table entries by item name
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Volts_Desc& Sdsu_Volts::operator[](const char* item_name)
{
  get_element(item_name);
  return volt_desc;
}
/*
 *+ 
 * FUNCTION NAME: Sdsu_Volts::dump
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * >! pointer to array where table will be dumped to
 *
 * FUNCTION VALUE: returns nuber of voltages put into array
 * 
 * PURPOSE: Put the contents of the table into an array
 * 
 * DESCRIPTION: This is used to dump the table of voltage
 *              descriptions into an array so we can then 
 *              pass the array over the network to the slave
 *              computer hosting the SDSU controller.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
int Sdsu_Volts::dump(Volts_Desc *v)
{

  Table_Item *item;
  Table_Item_Set::iterator item_iter;
  int i;
  char msgbuf[MSGLEN];

  for (item_iter=items.begin(), i=0; (item_iter != items.end()); item_iter++, i++) {
    item = *item_iter;
    // Now need to extract the individual fields
    get(item);
    // Add this voltage description to the array
    if (i == SDSU_MAX_VOLTS) {
      snprintf(msgbuf, MSGLEN,"Too many voltages - max allowed is %d", SDSU_MAX_VOLTS);
      throw Error(msgbuf,E_ERROR,-1,__FILE__,__LINE__);     
    } else 
      v[i] = volt_desc;
  }

  return i;
}

/*
 *+ 
 * FUNCTION NAME: Sdsu_Volts::~Sdsu_Volts
 * 
 * INVOCATION: ~Sdsu_Volts()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Sdsu_Volts::~Sdsu_Volts()
{
}


