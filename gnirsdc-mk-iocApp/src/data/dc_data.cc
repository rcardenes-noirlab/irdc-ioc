/*
 * Copyright (c) 2000-2005 by RSAA
 * $Id: dc_data.cc,v 1.3 2006/03/29 07:03:50 gemvx Exp $
 * GEMINI PROJECT
 *
 * FILENAME dc_data.cc
 *
 * GENERAL DESCRIPTION
 *
 * This file implements the DC Data task. It is started by the control task
 * in response to an INIT command. After starting it will connect to the DHS if
 * required and then await the raising of a semaphore to indicate data is
 * ready for transfer. The data are moved to DHS data structures and the semaphore
 * is then released. Transfer to the DHS is then initiated. Once the transfer has
 * completed, the cycle is repeated.
 *
 * $Log: dc_data.cc,v $
 * Revision 1.3  2006/03/29 07:03:50  gemvx
 * - changed how dhs/nifs interact
 *
 * Revision 1.2  2006/03/28 19:37:44  gemvx
 * - moved nifsStart to scripts dir
 *
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Gnirs; first gemini-modified release
 *
 * Revision 1.35  2005/09/27 19:59:22  pjy
 * Turned off view mode if sky image bad
 *
 * Revision 1.34  2005/09/26 04:55:26  pjy
 *  Introduced new semaphore (dc_data_observe) to handle handshaking with OBSERVE
 * threads. Previous use of (dc_data_ready) was being entangled with other operations.
 *
 * Revision 1.33  2005/09/23 01:46:59  pjy
 * Added code to clean out old connection thread
 * Added more code to clean out old observe thread after an abort
 * More debugging info
 * Moved IMP startup/shutdown to DHS connect/disconnect routines
 *
 * Revision 1.32  2005/09/15 22:43:00  pjy
 * Fixed sub_file error where wasn't set to NULL after deleting.
 *
 * Revision 1.31  2005/09/13 04:28:22  pjy
 * Fixed bug with dosubfile
 *
 * Revision 1.30  2005/09/11 22:51:19  pjy
 * More effort to handle abort
 * string output for states in messages
 *
 * Revision 1.29  2005/09/11 02:48:57  pjy
 * Removed throw if already handling a connection - now handled by thread
 *
 * Revision 1.28  2005/09/11 02:04:19  pjy
 * Made do_connection a separate thread
 *
 * Revision 1.26  2005/09/10 20:01:28  pjy
 * Initialised handling_dhs_connection
 *
 * Revision 1.25  2005/09/10 19:58:48  pjy
 * Added check for case when already doing a DHS connection but would have
 * timed out - prevent second attempt
 *
 * Revision 1.24  2005/08/18 03:55:54  pjy
 * Better health management (use SNL code)
 *
 * Revision 1.23  2005/08/02 04:51:49  pjy
 * DHS connection fixes.
 * Extract sky frame from first nifs extension (ie compat with GNIRS file format)
 *
 * Revision 1.22  2005/07/29 23:52:57  pjy
 * Extra log message
 *
 * Revision 1.21  2005/07/28 09:46:25  pjy
 * Added 3 second delay after dhsExit
 *
 * Revision 1.20  2005/07/26 08:19:49  pjy
 * Correct type cast for nframes
 *
 * Revision 1.19  2005/07/26 06:15:25  pjy
 * Added nframes
 *
 * Revision 1.18  2005/07/24 12:03:45  pjy
 * Added connection callback
 * Moved disconnect health state to before disconnect (in case disconnect hangs...)
 * Added semGive after reconnection success
 *
 * Revision 1.17  2005/07/21 11:52:50  pjy
 * Handle waveMin and wavemax from CC
 *
 * Revision 1.16  2005/07/21 04:06:19  pjy
 * Added support for expanding the compressed image so that it is square (as per ICD)
 *
 * Revision 1.15  2005/07/19 06:36:27  pjy
 * Added bunit
 *
 * Revision 1.14  2005/07/19 01:53:38  pjy
 * Pre AT adjustments
 *
 * Revision 1.13  2005/07/18 00:41:28  pjy
 * Raw data saving
 * Checking for coadd count
 *
 * Revision 1.12  2005/07/14 06:54:34  pjy
 * Added "saving" param
 * Added end_observe flag
 * Dont do image_close unless an image header has been received
 * Only do state change after end_observe set
 * Adjustments for saiving each NDR
 *
 * Revision 1.11  2005/07/12 07:06:30  pjy
 * Removed ref to CC DB
 * Added gratingName
 * Added GNIRS optics preferences
 *
 * Revision 1.10  2005/04/27 03:47:39  pjy
 * Removed dhs connected message
 *
 * Revision 1.9  2004/12/23 00:15:15  pjy
 * Fixed address of exposedRQ
 *
 * Revision 1.8  2004/12/22 06:13:43  pjy
 * DC data src files ready for first Gemini release
 *
 * Revision 1.7  2004/08/22 23:21:04  pjy
 * Added more health messages related to DHS and FITS connections
 *
 * Revision 1.6  2004/08/20 02:31:48  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.5  2004/04/25 02:25:57  pjy
 * Pre T2.2 version
 *
 * Revision 1.4  2003/09/19 06:24:26  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.3  2003/03/25 07:12:55  pjy
 * Mods to start of testing with Fits server
 *
 * Revision 1.2  2003/03/01 06:20:12  pjy
 * Progress up to GNIRS fires - beginning of sdsu VME tests.
 *
 * Revision 1.1  2001/10/19 00:29:07  pjy
 * Added for GNIRS DC
 *
 *
 */

//#include <types.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <cstdio>
#include <string.h>
#include <mqueue.h>
#include <fcntl.h>
// #include <sockLib.h>
// #include <inetLib.h>
// #include <hostLib.h>
// #include <ioLib.h>
// #include <drv/timer/timerDev.h>

#include <errlog.h>
#include "cicsLib.h"
// #include "dhs.h"
#include "dc.h"
#include "dc_data.h"
#include "ipc.h"
#include "parameter.h"
#include "utility.h"
#include "common.h"
#include "configuration.h"
#include "ir_camera.h"
#include "ir_setup.h"
#include "config_table.h"
#include "logging.h"
#include "uuid/uuid.h"
#include "product_conf.h"
#include "dc_epics_db.h"
#include <dbAccess.h>
// #include "dhs_client.h"
// #include "gnirs_compressed_image.h"

// TODO: THIS IS TO BE HARDCODED ONLY DURING TESTS
static std::string data_dump_path("/home/readout_data/new");

using std::shared_ptr;

// #define USE_DHS
// #define XFER_DATA

//+ Dhs debug flag
int DC_DATA_DHS_DEBUG=1;

//+ Constants
static const int DC_AMPS = 4;                     //+ Number of readouts on DC detector
const int DATA_TASK_PRIORITY = epicsThreadPriorityScanLow;               //+ VxWorks priority to run data command task
const int DATA_TASK_STACK = epicsThreadGetStackSize(epicsThreadStackMedium);             //+ Size of data task stack
static const int DATA_ABORT_WAIT = 10;            //+ Time to wait for data to connect to the data handling system

//+ External Functions
extern "C" {
  extern void ImpMaster();
  extern void taskclose(char *TaskName, char *MachineName);
}

// Globals
//+ Parameter databases
extern ParameterDB *dc_status_db;                 //+ EPICS interface to the DC SAD
extern Parameter *dc_status;                      //+ Internal DC status parameters
extern ParameterDB *dc_par_db;                    //+ EPICS interface to the CAD pars
extern Parameter *dc_par;                         //+ Internal control parameters

//+ Data buffers
extern Detector_Image *shared_image_buffer;       //+ Buffer used to transfer image data between data and slave tasks

//+ Control semaphores
shared_ptr<Semaphore> sem_data_ready;             //+ Semaphore indicating data ready for transfer
shared_ptr<Semaphore> sem_data_empty;             //+ semaphore indicating data area empty
shared_ptr<Semaphore> sem_data_observe;           //+ Semaphore indicating previous observe finished

//+ State
extern DC_State dc_state;                         //+ DC global state
extern DC_State dc_data_state;                    //+ current Data task run state
extern Debug_Level debug_mode;                    //+ DC debugging mode
extern IR_Mode ir_mode;                           //+ Mode of current observation OBSERVE or VIEW
extern bool aborting;                             //+ flag set if OBSERVE is aborted
extern bool stopping;                             //+ flag set if OBSERVE is stopped
extern bool saving;                               //+ flag set when saving OBSERVE data

//+ Configuration, status, control and reporting structures
extern Nametype configName;                       //+ Name of camera configuration
extern Instrument_Status *inst_status;            //+ Full instrument status - local copy
extern Config *shared_config;                     //+ Full camera configuration - read from disk
//extern Message *dc_message;                       //+ Message handling object
//extern Report *dc_report;                         //+ Reporting object

//+ Data destination
extern DC_Dest data_destination;                  //+ Destination of data - either dhs, fits or none

//+ Data handler connection
extern bool doConnect;                            //+ Connect/disconnect from data handler

//+ DHS server
/*
extern bool dhs_available;                        //+ flag set if DHS is available
extern bool dhs_connected;                        //+ flag set when the dhs is connected
extern bool handling_dhs_connection;              //+ flag set when the dhs connection is being handled
extern Hostnametype dhs_server_ip;                //+ DHS server IP address
extern Hostnametype dhs_server_name;              //+ DHS server host name
extern Nametype obs_ql_stream;                    //+ DHS Quick Look stream in use for obs mode
extern Nametype view_cmp_ql_stream;               //+ DHS Quick Look stream in use for obs mode compressed data
extern Nametype view_ql_stream;                   //+ DHS Quick Look stream in use for view mode
extern char* dc_qls_ptr[MAX_QLS];                 //+ Ptr to DHS Quick Look streams in use
extern int n_qlstreams;                           //+ Number of QL streams in use
extern char data_label[DHS_DATA_LABEL_LEN];       //+ Datalable provided with OBSERVE or unique name returned by DHS
*/

//+ FITS server info
/*
extern bool fits_available;                       //+ flag set if FITS server is available
extern bool fits_connected;                       //+ flag set when fits server connected
extern long fits_server_port;                     //+ FITS server socket port number
extern Hostnametype fits_server_name;             //+ FITS server host name
*/

//+ System health records
extern Nametype dhs_health;                       //+ DHS health state
extern EPICS_String dhs_health_msg;               //+ DHS health state message
extern bool dhs_problem;                          //+ DHS problem flag
extern EPICS_String dhs_problem_msg;              //+ DHS problem message
extern Nametype fits_health;                      //+ FITS server health state
extern EPICS_String fits_health_msg;              //+ FITS server health state message
extern bool fits_problem;                         //+ FITS problem flag
extern EPICS_String fits_problem_msg;             //+ FITS problem message
extern Nametype data_health;                      //+ Data task health state
extern EPICS_String data_health_msg;              //+ Data task health state message
extern Nametype slave_health;                     //+ Slave task health state
extern EPICS_String slave_health_msg;             //+ Slave task health state message

//+ The camera
extern IR_Camera *ir_camera;                      //+ The infra red camera object

//+ Parameters required for image spectral compression and stacking
extern double gratingMin;                         //+ Grating min wavelength (microns)
extern double gratingMax;                         //+ Grating max wavelength (microns)
extern Nametype gratingName;                      //+ Name of current grating

// Local file variables

//+ Image transfer variables
static shared_ptr<Semaphore> image_ready_sem;     //+ semaphore indicating image trasfer buffer ready
static shared_ptr<Semaphore> image_empty_sem;     //+ semaphore indicating image trasfer buffer empty
static shared_ptr<Semaphore> data_xfer_sem;       //+ semaphore controlling access to data_xfer pipeline
static shared_ptr<Mailbox> data_mbox;             //+ Data task message queue
static Detector_Image_Header header;              //+ Current image header - could be either main,sub or close
static Detector_Image_Main_Header main_header;    //+ Current image main header
static Detector_Image_Sub_Header *sub_header;     //+ Pointer to sub header variant of image header record
//static char *frames_mem = NULL;                   //+ Local copy of frames_data stuctures
// static char intensityp[IMAGE_BUF_SIZE];           //+ Local copy of pixel buffer

//+ DHS control variables
#ifdef USE_DHS
static DHS_BD_LIFETIME dc_data_lifetime;          //+ DHS data lifetime
#endif

//+ FITS server variables
static unsigned int image_seq;                    //+ Image part sequence number

#ifdef USE_DHS
static bool dhs_init_done;                        //+ Flag to indicate whether DHS initialised
int dc_imp_master;                                //+ Task id for IMP master task
static DHS_Client *dhs_client = NULL;             //+ DHS client object for science and view frames
#endif
//static bool data_transfer_done;                   //+ Flag to idicate data transfer is done
static bool last_ndr = false;                     //+ Set false when doing last NDR for this exposure
static bool image_header_received;                //+ flag set when a new image header has been received
static bool ir_stop;                              //+ flag set when a STOP command received
static long nreads;                               //+ count of current NDR
static long nreads_done;                          //+ count of NDRs completed
static long ncoadds_done;                         //+ count of coadds completed
static bool end_observe;                          //+ set when an ENDOBSERVE message received

// static bool test_mode = false;                    //+ Simple flag to set if just testing command reception

//static bool doCmpIm = false;                      //+ flag set if image compression along spectral axis operation required
// static GNIRS_Compressed_Image *compressed_image;   //+ GNIRS multi sliced spectral image compressed to form image of object

//static bool doSubtract = false;                   //+ flag set if sky subtraction to be done
//static Filenametype subFname = "";                //+ name of FITS file to subtract
//static Fits* sub_fits = NULL;                     //+ Fits file object that is created when subFname is opened

static Thread* data_command = NULL;               //+ Thread used to run OBSERVE command

static epicsTime saving_timestamp;

static std::string current_uuid;

//+ Local Functions

// Command handling
void* dc_data_command(DC_Data_Msg* dmsg);
//  static void data_mq_notification(int, siginfo_t *, void *);

// Image transfer functions
static void dc_handle_new_image();
static void dc_handle_image_segment();
static void dc_handle_close_image(bool set_empty);
// static void dc_copy_column(char* outp, char* pixels);

//+ Class declarations

/*
 *+
 * CLASS
 *   DataCommandThread
 *   Thread subclass to execute DC Data commands in the background
 * DESCRIPTION
 *   Commands can be executed synchronously or asynchronously. This
 *   class provides the means to perform async executions.
 *
 *-
 */

class DataCommandThread : public Thread {
public:
	DataCommandThread(DC_Data_Msg msg, const string& name,
			unsigned stackSize, unsigned priority, bool dolock);
	virtual void run();
private:
	DC_Data_Msg dmsg;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
//    G L O B A L    F U N C T I O N S
//////////////////////////////////////////////////////////////////////////////////////////////////////

const std::string DATA_LABEL_PV = std::string(DCSADTOP) + "dataLabel.VAL";

static std::string get_datalabel() {
	dbAddr dba;

	if (dbNameToAddr(DATA_LABEL_PV.c_str(), &dba) != 0) {
		throw std::runtime_error("Can't obtain the address for the DataLabel variable");
	}

	char pbuffer[MAX_STRING_SIZE];
	long options = 0;
	long nv = 1;

	if (dbGetField(&dba, DBR_STRING, (void*)pbuffer, &options, &nv, NULL) != 0) {
		throw std::runtime_error("Error trying to retrieve the DataLabel variable");
	}

	return std::string(pbuffer);
}

/*
 *+
 * FUNCTION NAME: void dc_data(void)
 *
 * INVOCATION: sp dc_data
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: implement the DC DC Data task
 *
 * DESCRIPTION:
 *
 * See above for a description of the DC Data task
 *
 * The control flow is rather straight forward:
 *
 *   Start --> Connect DHS --> Ready state --OBSERVE cmd--> --Copying ---> Transferring
 *                            |--------------<---------------Done/Abort------|
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

void dc_data(void *)
{
  int istat;                         // IO return status
  DC_Data_Msg dmsg;                  // Command to forward to data
//  Thread* connect_command = NULL;    // Thread used to run DO_CONNECTION command

  try {
    logger::message(logger::Level::Full, "In dc_data");

    dc_status->put(DATA_STATE, int(DC_State::DC_INITIALIZING));

    dc_set_health(DATA_HEALTH, E_OK, DATA_HEALTH_MSG, data_health_msg, " ");

    // Attach to the image handling semaphores (created by dc_control)
    image_ready_sem = Semaphore::get(Camera::IMAGE_READY_SEM);
    image_empty_sem = Semaphore::get(Camera::IMAGE_EMPTY_SEM);
    data_xfer_sem = Semaphore::get(Camera::DATA_XFER_SEM);
    sem_data_ready = Semaphore::get(DC::DATA_READY_SEM);

    logger::message(logger::Level::Full, "Semaphores connected");

    // Create a binary semaphore to handle aborting observe threads
    sem_data_observe = Semaphore::create(DC_Data::DATA_OBSERVE_SEM);

    // Connect to data mailbox and setup to recieve notification when messages arrive
    data_mbox = Mailbox::get(DC::DATA_MBOX);


    logger::message(logger::Level::Full, "Message queue connected");

    // Clear any stale messages in queue
    while (data_mbox->messages_waiting() > 0) {
      if ((istat = data_mbox->get_message((char*)&dmsg, sizeof(DC_Data_Msg), IPC_NOWAIT)) == IPC_ERR) {
        if (istat != EAGAIN) {
          throw Error("mq_receive failed while clearing data mq!", E_ERROR, errno, __FILE__, __LINE__);
        }
      }
      logger::message(logger::Level::Full, "Read message %s", to_string(dmsg.cmd).c_str());
    }

    logger::message(logger::Level::Full, "Messages cleared");

    /* TODO: Review from here on
    // Allocate aligned space for local copy of frames mem
    frames_mem = (char*) memalign(ALIGN_SIZE, FRAMES_MEM_SIZE);
    */

    dc_status->put(SAVING, false);

    /*
    dhs_init_done = false;
    fits_init_done = false;
    handling_dhs_connection = false;
    imp_started = false;

    dhs_available = false;

    // Find out if the FITS server is available
    dc_status->get(FITS_AVAILABLE, fits_available);

    // Haven't connected yet - so set the flag
dc_status->put(FITS_CONNECTED, false);
    dc_status->put(FITS_PROBLEM, false);
    */

    // Give semaphore to indicate readiness to receive messages.
    // This semaphore created and empty from control task
    logger::message(logger::Level::Full, "About to give sem_data_ready");
    sem_data_ready->give();
    logger::message(logger::Level::Full, "sem_data_ready given");

    // Now wait for commands to be added to the data message queue
    // These come from the CONTROL task
    // Continue until data state set to STOPPED
    do {
      try {
        // Check for any requests from CONTROL
        logger::message(logger::Level::Full, "Waiting for command...");
        if (data_mbox->get_message((char*)&dmsg, sizeof(DC_Data_Msg), IPC_WAIT)) {
          // New message received
          logger::message(logger::Level::Min, "dc_data: Got request '%s'", to_string(dmsg.cmd).c_str());

          try {
            // Start a separate thread to execute the OBSERVE command
            switch (dmsg.cmd) {
	    case DC_Cmd::OBSERVE:
		    if (data_command != NULL) {
			    if (!sem_data_observe->try_wait(DATA_ABORT_WAIT * 1000)) {
				    logger::message(logger::Level::Min, "Timeout while waiting to join previous observe thread - killing it...");
				    dc_status->put(SAVING, false);
				    // Delete the old task - don't wait on it
				    logger::message(logger::Level::Min, "Deleting old observe task");
				    // TODO: Fix this...
				    // taskDelete(data_command->tid);
			    }
			    delete data_command;
		    }

		current_uuid = DC_Data::get_uuid();

		    // Give the data xfer semaphore
		    data_xfer_sem->give();
		    logger::message(logger::Level::Min, "Starting Thread for the OBSERVE command!");

              // Start a new thread so that this thread can listen for new commands.
		    data_command = new DataCommandThread(dmsg, "data_observe", DATA_TASK_STACK, DATA_TASK_PRIORITY, true);
		    data_command->start();

              logger::message(logger::Level::Full, "Thread started");
              break;

	    case DC_Cmd::DO_CONNECTION:
              logger::message(logger::Level::Min, "Got DO_CONNECTION");
/* TODO: Analyze this block and do something
              if (connect_command != NULL) {
                // Delete the old task - don;t wait on it
                logger::message(logger::Level::Min, "Deleting old connection task");
                taskDelete(connect_command->tid);
                logger::message(logger::Level::Min, "Deleting old connection thread");
                delete connect_command;
                connect_command = NULL;
              }
*/

              // Start a new thread so that this thread can listen for new commands.
//              logger::message(logger::Level::Min, "Starting new Thread for the DO_CONNECTION command!");
/* TODO: Do something with this
#ifdef _POSIX_PTHREAD_SEMANTICS
              connect_command = new Thread("data_connect", dc_data_command, (void*)&dmsg, true);
#else
              connect_command = new Thread("data_connect", dc_data_command, (void*)&dmsg, true, DATA_TASK_PRIORITY,
                                        DATA_TASK_STACK);
#endif
*/
              logger::message(logger::Level::Full, "Thread started");
              break;
            default:
              // Execute the command synchronously
              dc_data_command(&dmsg);
              break;
            }
          }
          catch (Error& dce) {
            logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
            dc_set_health(DATA_HEALTH, dce.type, DATA_HEALTH_MSG, data_health_msg, (char*)dce.msg.c_str());
          }

          logger::message(logger::Level::Full, "Request %s Done, state = %s", to_string(dmsg.cmd).c_str(), to_string(dc_data_state).c_str());
        }
      }
      catch (Error& dce) {
        logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
        dc_set_health(DATA_HEALTH, dce.type, DATA_HEALTH_MSG, data_health_msg, (char*)dce.msg.c_str());
      }

      // Check if STOPPED - this would have been set by a REBOOT command
    } while (dc_data_state != DC_State::DC_STOPPED);

/*
 *  TODO: Finish the clenaup

    try {
      // Join any old thread to clean up
      if (data_command != NULL) {
        // For all previous command threads other than OBSERVE wait to join here
        delete data_command;
        data_command = NULL;
      }
    }
    catch (Error& dce) {
      // Ignore any exceptions from trying to clear the message queue notifications
    }

    logger::message(logger::Level::Full, "Exited command wait loop...");

    // Clean up any FITS server interface
    if (fits_init_done)
      dc_fits_shutdown();

    logger::message(logger::Level::Full, "Deleting frames memory");
    if (frames_mem != NULL) {
      free(frames_mem);
      frames_mem = NULL;
    }

    logger::message(logger::Level::Full, "Deleting compressed image");
    if (compressed_image != NULL) {
      delete compressed_image;
      compressed_image = NULL;
    }

    logger::message(logger::Level::Full, "Deleting sky FITS file");
    if (sub_fits != NULL) {
      delete sub_fits;
      sub_fits = NULL;
    }

    logger::message(logger::Level::Full, "Deleting semaphores.");

    // Delete the semaphores before exiting
    if (image_ready_sem != NULL) {
      delete image_ready_sem;
      image_ready_sem = NULL;
    }

    if (image_empty_sem != NULL) {
      delete image_empty_sem;
      image_empty_sem = NULL;
    }

    if (data_xfer_sem != NULL) {
      delete data_xfer_sem;
      data_xfer_sem = NULL;
    }

    semDelete(sem_data_observe);
    logger::message(logger::Level::Full, "Semaphores deleted.");

    logger::message(logger::Level::Full, "Deleting data mbox.");
    delete data_mbox;
    data_mbox = NULL;
    logger::message(logger::Level::Full, "Data mbox deleted.");

*/
  }
  catch (Error& dce) {
	  /*
    logger::message(logger::Level::NoLog, dce.record_error(__FILE__,__LINE__));
    dc_set_health(DATA_HEALTH, dce.type, DATA_HEALTH_MSG, data_health_msg, (char*)dce.msg.c_str());
    */
  }
  // logger::message(logger::Level::Full, "Data task done!");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//    L O C A L    F U N C T I O N S
//////////////////////////////////////////////////////////////////////////////////////////////////////

DataCommandThread::DataCommandThread(DC_Data_Msg msg, const string& name,
			unsigned stackSize, unsigned priority, bool dolock)
	: Thread(name.c_str(), priority, stackSize, dolock),
	  dmsg(msg)
{}

void
DataCommandThread::run() {
	dc_data_command(&dmsg);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//    C O M M A N D    H A N D L I N G    R O U T I N E S
//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *+
 * FUNCTION NAME: void* dc_data_command(DC_Data_Msg& dmsg);
 *
 * INVOCATION: dc_data_command(dmsg);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > scmd - Data command to execute
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Interprets and executes the given command
 *
 * DESCRIPTION:
 *
 * Calls the appropriate camera class method to execute the command.  Moves into
 * MONITOR state for the observe command.
 *
 * For an OBSERVE command an image is transferred progressively using a series
 * of headers prepared by the SLAVE task:
 *
 * Expect to get a series of image headers in following pattern:
 *
 * 1.   Main header - describes image
 * 2.   Image segment - a part of the image during readout
 * 3.   Image segment - next part
 * ...
 * N-1. Image segment - last part
 * N.   Close header - confirmation that image transfer is complete
 *
 * As each image segment is received (already unravelled by the SLAVE task) it
 * is reoriented for column-oriented readouts and placed in the DHS or FITS_XFER
 * data buffers.
 *
 * EXTERNAL VARIABLES: DC DC_Par setup parameters
 *
 * PRIOR REQUIREMENTS: This is called from DC_Data in response to Gemini EPICS commands
 *
 * DEFICIENCIES:
 *
 *- */
void* dc_data_command(DC_Data_Msg* dmsg)
{
  ostringstream ostr;                                // Message buffer
//  DC_State old_dc_data_state;                     // Saved state before command processed
  string ddest;                                   // Temporary string var
  Detector_Image_Body_Type image_header_type;     // Type of image message just received
  bool transfer_failed;                           // Set if there was an error detected during data transfer
  int segment_count=0;                            // Counter of image segments received
//  char msgbuf[MAX_STRING_SIZE];

  logger::message(logger::Level::Full, "In dc_data_command, cmd=%s", to_string(dmsg->cmd).c_str());

  // handle command
  switch (dmsg->cmd) {

  case DC_Cmd::INIT:
    // Go into initialising state
    dc_status->put(DATA_STATE, int(DC_State::DC_INITIALIZING));

    // Set state to ready - ie ready to receive and transfer an image
    dc_status->put(DATA_STATE, int(DC_State::DC_DATA_READY));

    logger::message(logger::Level::Full, "About to give sem_data_ready");
    sem_data_ready->signal();
    logger::message(logger::Level::Full, "sem_data_ready given");

    break;

  case DC_Cmd::REBOOT:
    // Wind up operations to exit cleanly

//    old_dc_data_state = dc_data_state;
    dc_status->put(DATA_STATE, int(DC_State::DC_STOPPED));
    logger::message(logger::Level::Full, "Rebooting - state = DC_STOPPED ");

    // If we were in midst of transferring an image, set semaphore so that
    // loop can unwind neatly
    /*
    if (image_ready_sem->waiting) {
      logger::message(logger::Level::Full,"Setting image_ready_sem sem in REBOOT");
     image_ready_sem->set();
    }
    if (data_xfer_sem->waiting) {
      logger::message(logger::Level::Full,"Setting data_xfer sem in REBOOT");
      data_xfer_sem->set();
    }
    */
    break;

  case DC_Cmd::STOP:
    // Just set the stop flag and wait for a cleanup to occur at next NDR
    logger::message(logger::Level::Min, "STOP received - stopping");
    ir_stop = true;
    break;

  case DC_Cmd::ABORT:
    logger::message(logger::Level::Min, "ABORT received - aborting");

    // Wind up operations to exit cleanly
//    old_dc_data_state = dc_data_state;
    dc_status->put(DATA_STATE, int(DC_State::DC_ABORTING));

    // If we were in midst of transferring an image, set semaphores so that
    // loop can unwind neatly
    if (image_ready_sem->waiting) {
      logger::message(logger::Level::Full,"Giving image_ready_sem sem in ABORT");
      image_ready_sem->give();
    }
    if (data_xfer_sem->waiting) {
      logger::message(logger::Level::Full,"Giving data_xfer sem in ABORT");
      data_xfer_sem->give();
    }

    // Wait for the data task to be ready - with timeout. If this times out then kill
    // the observe thread
    if (data_command != NULL) {
      logger::message(logger::Level::Min, "Waiting for the observe task to complete...");
      if (!sem_data_observe->try_wait(DATA_ABORT_WAIT * 1000)) {
        logger::message(logger::Level::Min, "Timeout during abort while saving - killing thread...");
        dc_status->put(SAVING, false);
        // Delete the old task - don't wait on it
        logger::message(logger::Level::Min, "Deleting old observe task");
	// TODO: We can't do this with OSI EPICS...
        // taskDelete(data_command->tid);
        // dc_status->put(DHS_PROBLEM, true);
        // strncpy(msgbuf,"Timeout handling ABORT - check DHS?",MAX_STRING_SIZE);
        // dc_status->put(DHS_PROBLEM_MSG,msgbuf,0,strlen(msgbuf));
      }
      logger::message(logger::Level::Min, "Deleting old observe thread");
      delete data_command;
      data_command = NULL;
    }
    logger::message(logger::Level::Min, "ABORT completed.");

    break;

  case DC_Cmd::DO_CONNECTION:
    logger::message(logger::Level::Min, "Data handler DO_CONNECTION command received: ignoring");
    break;

  case DC_Cmd::OBSERVE:
    // During the handling of an OBSERVE command we could expect STOP, ABORT or
    // REBOOT commands.

    printf("dc_data_command: Handling OBSERVE\n");
    logger::message(logger::Level::Full, "Handling OBSERVE");
    end_observe = false;

    // First of all reject an OBSERVE command if we are already handling one
    if (dc_data_state == DC_State::DC_DATA_TRANSFERRING) {
      printf("dc_data_command: Already handling one: rejecting\n");
      logger::message(logger::Level::NoLog, "- Already in handling a previous OBSERVE command - rejecting new one!");
      break;
    }

    // Move to the TRANSFERRING state
    dc_status->put(DATA_STATE, int(DC_State::DC_DATA_TRANSFERRING));

    // clear image header flag - expect to get a main header describing image
    // first up
    image_header_received = false;
    ir_stop = false;

    // Expect to get a series of image headers in following pattern:
    // 1.   Main header - describes image
    // 2.   Image segment - a part of the image during readout
    // 3.   Image segment - next part
    // ...
    // N-1. Image segment - last part
    // N.   Close header - confirmation that image transfer is complete
    //
    // Loop until the image is fully transferred or aborted
    //
    transfer_failed = false;
    while (dc_data_state == DC_State::DC_DATA_TRANSFERRING) {
	    try {
		    logger::message(logger::Level::Full, "Waiting for image_ready_sem");
		    // Wait here for the image ready semaphore to be set
		    image_ready_sem->wait();

		    // Break from the image wait loop if we have aborted or something...
		    if (dc_data_state !=  DC_State::DC_DATA_TRANSFERRING) {
			    logger::message(logger::Level::Min, "Exiting Transferring state - state = %s", to_string(dc_data_state).c_str());
			    if (image_header_received) {
				    data_xfer_sem->give();
			    }

			    dc_status->put(SAVING, false);
			    break;
		    }

		    // Otherwise go ahead and process the available header
		    image_header_type = shared_image_buffer->body.type;
		    logger::message(logger::Level::Full, "Got ready semaphore, image header type = %d", image_header_type);

		    // Comment this out if really wanting do transfers - otherwise leave
		    // to test without transfer.
		    //data_destination = DC_DEST_NONE;

		    // If the data destination is to nowhere then skip transfer but set the semaphore
		    if ((data_destination == DC_DEST_NONE) || transfer_failed) {
			    image_empty_sem->give();
			    logger::message(logger::Level::Full, "No transfer - image_empty_sem set");

			    // Pretend that we transfered the header so that slave doesn't jam
			    if (image_header_type == IMAGE_HEADER)
				    shared_image_buffer->header_ok = true;

			    logger::message(logger::Level::Full, "Image header type = %d, not transferred (dd=%s, fail=%d).",
					    image_header_type, to_string(data_destination).c_str(), transfer_failed);
		    } else {

			    printf("image_header_type: %d\n", image_header_type);
			    switch (image_header_type) {

				    // A new image - handle the image main header
				    case IMAGE_HEADER:
					    // Take the data_xfer semaphore to synchronise access to data pipeline
					    logger::message(logger::Level::Full, "Waiting for data_xfer_sem");
					    data_xfer_sem->wait();
					    logger::message(logger::Level::Full, "Got data_xfer_sem, xfering header");

					    dc_status->put(SAVING, true);
					    saving_timestamp = epicsTime::getCurrent();

					    dc_handle_new_image();

					    segment_count = 0;
					    break;

					    // An image chunk - handle it
				    case IMAGE_BODY_DATA:
					    dc_handle_image_segment();
					    segment_count++;
					    break;

					    // Last - we have a close header - cleanup
				    case IMAGE_CLOSE:
					    if (image_header_received) {
						    dc_handle_close_image(true);

						    //cout << "Time taken to save data =" <<  (timestamp()- saving_timestamp)/(double)timestamp_res() << endl;

						    // Set the data xfer semaphore
						    data_xfer_sem->give();

						    logger::message(logger::Level::Full,"Image closed, %d segments received", segment_count);
					    }
					    break;

					    // Ignore anything else - but report the unexpected
				    default:
					    logger::message(logger::Level::Min, "Bad header, image header type = %d", image_header_type);
					    break;
			    }

			    // Report that the header has been processed
			    logger::message(logger::Level::Full, "Image header type = %d, handled ok.", image_header_type);
		    }

		    // done so Switch out of TRANSFERRING state.
		    // Only do this if the full exposure has finished - ie we may expect to receive
		    // more than one images to transfer.
		    printf("dc_data: done, switching out to TRANSFERRING state\n");
		    if ((image_header_type == IMAGE_CLOSE) && end_observe)
			    dc_status->put(DATA_STATE, int(DC_State::DC_DATA_READY));
	    }
	    catch (Error& dce) {
		    logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
		    dc_set_health(DATA_HEALTH, dce.type, DATA_HEALTH_MSG, data_health_msg, (char*)dce.msg.c_str());
		    transfer_failed = true;
	    }
    }


    dc_status->put(SAVING, false);

    // Give semaphore in case someone is waiting - eg the ABORT thread
    sem_data_observe->give();
    break;

  case DC_Cmd::ENDOBSERVE:
    end_observe = true;

    // Handle the case where an IMAGE_CLOSE has been received already
    // Set the semaphore to signal the OBSERVE thread to cleanup
    if ((!image_header_received) && (dc_data_state ==  DC_State::DC_DATA_TRANSFERRING)) {
      dc_status->put(DATA_STATE, int(DC_State::DC_DATA_READY));
      image_ready_sem->give();
    }
    break;

  default:
    // Ignore other commands
    logger::message(logger::Level::Full, "Command (%s), ignored.", to_string(dmsg->cmd).c_str());
    break;
  }

  // Done handling received command - report
  logger::message(logger::Level::Full, "In dc_data_command, cmd=%s Done!", to_string(dmsg->cmd).c_str());
  dc_set_health(DATA_HEALTH, E_OK, DATA_HEALTH_MSG, data_health_msg, " ");

  return NULL;
}

/*
 *+
 * FUNCTION NAME: void dc_handle_new_image();
 *
 * INVOCATION: dc_handle_new_image();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Handles a new image header
 *
 * DESCRIPTION:
 *
 * We have just taken the imageReady semaphore and the shared image buffer has
 * a main image header ready for processing. This routine copies the header and
 * sets us up for transferring a new image. The imageEmpty semaphore is given as
 * soon as the copy is made.
 *
 * EXTERNAL VARIABLES: DC DC_Par setup parameters, fitpix array
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void dc_handle_new_image()
{
  int tir_mode;

  // Set local parameter ptr to cameras parameters
  Parameter *ir_statusp = ir_camera->ir_statusp;

  logger::message(logger::Level::Full, "In dc_new_image");

  // Restart the header sequence counter
  image_seq = 0;

  try {
    // Indicate image header copied - allow slave to refill shared_image_buffer
    // First make sure any previous images are closed properly before handling a new one
    if (image_header_received) {
      logger::message(logger::Level::Full, "Closing previous image");
      dc_handle_close_image(false);
    }

    // Work out whether this is the final NDR. For a multiple read exposure, (ie DCS,
    // Fowler or liner fitting) we might only have to save the data from the last
    // NDR - check here.
    ir_statusp->get(NREADS, nreads);
    ir_statusp->get(NREADS_DONE, nreads_done);
    ir_statusp->get(NCOADDS_DONE, ncoadds_done);
    if (ir_mode == IR_VIEW_MODE) {
//      ir_statusp->get((string)"view_"+NCOADDS, ncoadds);
      ir_statusp->get((string)"view_"+IR_READ_MODE, tir_mode);
    } else {
//      ir_statusp->get((string)"obs_"+NCOADDS, ncoadds);
      ir_statusp->get((string)"obs_"+IR_READ_MODE, tir_mode);
    }

    // Hardcoded for the time being. We're having only one...
    last_ndr = true;

    // Copy header and frames data to local structures
    /*
     * TODO: Not needed
    if (last_ndr)
      copy_frame_references(shared_image_buffer->body.Detector_Image_Body_u.main_header, main_header,
                            shared_image_buffer->frames_mem, frames_mem, true);
    else
      copy_frame_references(shared_image_buffer->body.Detector_Image_Body_u.main_header, main_header,
                            shared_image_buffer->raw_frames_mem, frames_mem, true);
    */

    main_header.image_seq = image_seq++;

//    dc_status->put(BUNIT, "ADU/s", 0, NAMELEN);

//    dc_status->put(NFRAMES, (long) main_header.frames.frames_len);

    // Set the header ok flag in the shared image buffer, so that slave knows
    // everything is ok.
    shared_image_buffer->header_ok = true;
    image_header_received = true;
  }
  catch (Error& dce) {
    logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
    dc_set_health(DATA_HEALTH, dce.type, DATA_HEALTH_MSG, data_health_msg, (char*)dce.msg.c_str());
    image_empty_sem->give();
    logger::message(logger::Level::Min, "Failed header transfer - image_empty_sem set");
    dce.rethrow();
  }

  // Lastly set the empty semaphore - needed to wait until the header correctly
  // processed before any more image transferring can continue
  image_empty_sem->give();
  logger::message(logger::Level::Full, "Successful header transfer - image_empty_sem set");
}

/*
 *+
 * FUNCTION NAME: void dc_handle_image_segment();
 *
 * INVOCATION: dc_handle_image_segment();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Handles a new image header
 *
 * DESCRIPTION:
 *
 * We have just taken the imageReady semaphore and the shared image buffer has
 * an image segment ready for processing. This routine copies the image segment
 * data and then transfers the data to either the DHS or FITS server. The
 * imageEmpty semaphore is given as soon as the copy is made.
 *
 * EXTERNAL VARIABLES: DC DC_Par setup parameters, fitpix array
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void dc_handle_image_segment()
{
  ostringstream ostr;                             // Message buffer
//  int f;                                          // frame,subframe indices
//  unsigned int nbytes;                            // Number of bytes of pixel data
  char *pixels;                                   // Pointer to input pixels
//  double sum,minv,maxv;                           // simple stats of image chunk for debugging
//  unsigned long t;                                // temp var for holding x,y during transpose
//  Frame_Data *this_frame;                         // Ptr to frame being handled
  Sub_Frame_Data* this_subframe;                  // Ptr to subframe being handled
  bool clear_empty_sem = false;                   // Local flag used to ensure image empty semaphore cleared

  try {
    logger::message(logger::Level::Full, "In dc_handle_image_segment");

    if (!image_header_received) {
      image_empty_sem->give();
      logger::message(logger::Level::Full, "No image header received, cannot transfer segment - image_empty_sem set");
      return;
    }

    // Copy image data structure to local storage. NB the image pixels are still
    // in the shared_image_buffer block. These data have to be copied separately in next
    // step following this one.
    header.Detector_Image_Header_u.sub_header = shared_image_buffer->body.Detector_Image_Body_u.data.sub_header;

    // Pointer to current subframe
    this_subframe = &main_header.frames.frames_val[sub_header->frame].subframes.subframes_val[sub_header->subframe];

    // Get the number of pixel bytes
//    nbytes = shared_image_buffer->body.Detector_Image_Body_u.data.pixel_data.pixel_data_len;

    // Pointer to pixels
    pixels = shared_image_buffer->body.Detector_Image_Body_u.data.pixel_data.pixel_data_val;

    // Now free the semaphore to indicate image copy done
    image_empty_sem->give();
    logger::message(logger::Level::Full, "Successful segment transfer - image_empty_sem set");
    clear_empty_sem = true;

    // Add frame offsets to sub_header
    sub_header->x += this_subframe->frame_xo;
    sub_header->y += this_subframe->frame_yo;

    /*
    // Handle sky subtraction?
    if (doSubtract && (this_frame->image_info.data_type == RAW_DATA)) {
      dc_subtract_sky((float*)intensityp, sub_header->x, sub_header->y, sub_header->width, sub_header->height);
    }
    */

    // Transfer this data to the DHS or FITS server

  }
  catch (Error &dce) {
    logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
    dc_set_health(DATA_HEALTH, dce.type, DATA_HEALTH_MSG, data_health_msg, (char*)dce.msg.c_str());
    if (!clear_empty_sem) {
      image_empty_sem->give();
      logger::message(logger::Level::Min, "Failed segment transfer - image_empty_sem set");
    }
    dce.rethrow();
  }

}
/*
 *+
 * FUNCTION NAME: dc_copy_column
 *
 * INVOCATION: dc_copy_column(char* outp, char* pixels)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * < outp - output image base address
 * > pixels - input image buffer
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Handles reorienting a column oriented slice of the output image.
 *
 * DESCRIPTION:
 * This data is received as a transposed column and it requires reorienting
 * as it is put into the output column. This routine is a front-end to the
 * template function 'reorient_pix' in the . Parameters to reorient_pix are organised
 * according to data type.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
/*
static void dc_copy_column(char* outp, char* pixels)
{
  // Setup pointers to the pixel data for each data type we support
  short *sipix = (short *) pixels;
  short *sopix = (short *) outp;
  int *iipix = (int *) pixels;
  int *iopix = (int *) outp;
  float *fipix = (float *) pixels;
  float *fopix = (float *) outp;
  double *dipix = (double *) pixels;
  double *dopix = (double *) outp;
  Frame_Data *this_frame;               // Ptr to frame being handled
  int fw;                               // full width of output frame

  // We are transposing a set or rows into a set of columns with the output buffer
  // width exactly equal to the height of the input buffer.. Both buffers are the
  // same size
  fw = sub_header->height;
  this_frame = &main_header.frames.frames_val[sub_header->frame];

  switch (this_frame->image_info.bitpix) {
  case BYTE_BITPIX:
     reorient_pix(pixels, outp, 0, fw, sub_header->height, sub_header->width,
                 sub_header->xdir, sub_header->ydir);
   break;
  case SHORT_BITPIX:
    reorient_pix(sipix, sopix, 0, fw, sub_header->height, sub_header->width,
                 sub_header->xdir, sub_header->ydir);
    break;
  case INT_BITPIX:
    reorient_pix(iipix, iopix, 0, fw, sub_header->height, sub_header->width,
                 sub_header->xdir, sub_header->ydir);
    break;
  case FLOAT_BITPIX:
    reorient_pix(fipix, fopix, 0, fw, sub_header->height, sub_header->width,
                 sub_header->xdir, sub_header->ydir);
    break;
  case DOUBLE_BITPIX:
    reorient_pix(dipix, dopix, 0, fw, sub_header->height, sub_header->width,
                 sub_header->xdir, sub_header->ydir);
    break;
  default:
    break;
  }
}
*/
/*
 *+
 * FUNCTION NAME: void dc_handle_close_image();
 *
 * INVOCATION: dc_handle_close_image(bool set_empty);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > set_empty - if this flag is set then clear the empty semaphore
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Handles a new image header
 *
 * DESCRIPTION:
 *
 * We have just taken the imageReady semaphore and the shared image buffer has
 * an image segment ready for processing. This routine copies the image segment
 * data and then transfers the data to either the DHS or FITS server. The
 * imageEmpty semaphore is given as soon as the copy is made.
 *
 * EXTERNAL VARIABLES: DC DC_Par setup parameters, fitpix array
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *- */
static void dc_handle_close_image(bool set_empty)
{
//  char* camera_name = shared_config->config_data.Config_Data_u.camera.desc.name;
//  double sum,minv,maxv;                 // simple stats of image chunk for debugging
  string inst;                          // temporary var for instrument name

  try {
    logger::message(logger::Level::Full, "In dc_handle_close_image, set_empty=%d", set_empty);

    // Let slave know that header received ok
    if (set_empty) {
      image_empty_sem->give();
      logger::message(logger::Level::Full, "Got closing header - image_empty_sem set");
    }

    if (!image_header_received) {
      return;
    }

    /* TODO: Not applicable any longer
    if ((data_destination == DC_DEST_DHS) && dhs_available) {
#ifdef USE_DHS
      // Setup the DHS to handle a new dataset for this image
      dhs_client->setup_end_dataset();

      // Send the main header stuff
      dhs_client->send_dataset(DHS_TRUE);

      logger::message(logger::Level::Full, "END dataset sent to DHS");

      if (doCmpIm) {
        if (compressed_image != NULL) {
          // Send completed compressed image
          dc_status->get(VIEW_CMPQLS, view_cmp_ql_stream, 0, NAMELEN);
          dc_qls_ptr[0] = view_cmp_ql_stream;
          n_qlstreams = 1;

          logger::message(logger::Level::Full, "Sending compressed image dataset sent to DHS");

          // First expand the image to make it square, ie 5x in x and 2x in y
          compressed_image->expand(5,2);

          inst = camera_name + string("_C");
          dhs_client->setup_simple_dataset(inst.c_str(), DHS_BD_LT_TRANSIENT, camera_name, "", n_qlstreams,
                                           dc_qls_ptr, compressed_image->nx, compressed_image->ny,
                                           FLOAT_BITPIX);

          // Copy the pixel data to the DHS frame
          if (debug_mode == DEBUG_FULL) {
            imstat((void*)compressed_image->image, compressed_image->npix, FLOAT_BITPIX, sum, minv, maxv);
            logger::message(logger::Level::Full,"Compressed image, sum=%f,min=%f,max=%f", sum,minv,maxv);
          }

          memcpy((char*) dhs_client->pixel_data[0], compressed_image->image,
                 compressed_image->nx*compressed_image->ny*BYTE_PIX(FLOAT_BITPIX));

          // Send the dataset
          dhs_client->send_dataset(DHS_TRUE);
          logger::message(logger::Level::Full, "Compressed image dataset sent to DHS");
        }
      }
#endif

    } else if ((data_destination == DC_DEST_FITS) && fits_available) {

      dc_close_fits_server();

    }
    */

  }
  catch (Error &dce) {
    logger::message(logger::Level::NoLog, "%s", dce.record_error(__FILE__,__LINE__));
    dc_set_health(DATA_HEALTH, dce.type, DATA_HEALTH_MSG, data_health_msg, (char*)dce.msg.c_str());
  }

  // Indicate that the current image has been successfully closed
  image_header_received = false;
}

void dc_save_product_configuration(ProductConf* product_configuration) {
	(*product_configuration)["PDU"].add_card("DATALABE", get_datalabel());

	auto temp_path = DC_Data::get_full_uuid_path_with_extension("tmp_head");
	auto final_path = DC_Data::get_full_uuid_path_with_extension("header");
	{
		std::ofstream output(temp_path);
		product_configuration->save_to(output);
	}
	std::rename(temp_path.c_str(), final_path.c_str());
}

std::string DC_Data::get_data_path() {
	return data_dump_path;
}

std::string DC_Data::get_uuid() {
	uuid_t uuid;
	char uuid_string[40];

	uuid_generate(uuid);
	uuid_unparse(uuid, uuid_string);

	return std::string(uuid_string);
}

static
std::string combine_data_path_and_file(std::string fname)
{
	return data_dump_path + "/" + fname;
}

std::string DC_Data::get_full_uuid_path_with_extension(std::string extension)
{
	string filename(current_uuid);

	if (extension.length() > 0) {
		filename = filename + "." + extension;
	}

	// TODO: Ideally, this should be done using C++17's filesystem facilities
	return combine_data_path_and_file(filename);
}

size_t DC_Data::dump_to(std::string path, Controller *controller) {
	size_t ret = 0;
	FILE *fp;

	printf("Dumping data to %s\n", path.c_str());
	fp = fopen(path.c_str(), "w");
	if (fp != NULL) {
		ret = controller->dump_to(fp);
		fclose(fp);
	}
	else {
		std::string error_msg = "Could not open for writing: ";
		throw std::runtime_error(error_msg + path);
	}

	return ret;
}
