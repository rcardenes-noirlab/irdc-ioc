/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * CICADA PROJECT
 * 
 * FILENAME table_tester.cc
 * 
 * GENERAL DESCRIPTION
 * Tests the Table class
 *
 * REQUIREMENTS
 * 
 * 
 */

/* Include files */
#include <iostream>
#include "utility.h"
#include "exception.h"
#include "table.h"

#ifdef vxWorks
int tableTest()
#endif
{
  try {
    Table table;
    Table table2;
    char tname[255],ntname[255];
    string str,val;
    Table_Item *item;

    cout << "Enter table name to test>";
    cin >> tname;

    try{
      table.read(tname);
      cout << table.n_items << " items read" << endl;

      table.print();

      cout << "Enter item name: ";
      cin >> str;
      strip_str(str,STRIP_BOTH);
      while (!cin.eof() && str.length() > 0) {
	item = table.find(str.c_str());
	if (item != NULL) {
	  item->print();
	}
	cout << "Enter parameter name: ";
	cin >> str;
	strip_str(str,STRIP_BOTH);
	while (!cin.eof() && str.length() > 0) {
	  val = item->pars[str];
	  strip_str(val,STRIP_BOTH);
	  if (val.length()>0) {
	    cout << str << "=" << val << endl;
	    cout << "Enter new val: ";
	    cin >> val;	  
	    strip_str(val,STRIP_BOTH);
	    if (val.length()>0) 
	      item->pars[str] = val;
	  } else
	    cout << "No value for that parameter???" << endl;	

	  // Now use find method of table class
	  item->find(str, val);
	  if (val.length()>0) {
	    cout << str << "=" << val << endl;
	  } else
	    cout << "No value for that parameter when using table find method???" << endl;	

	  cout << "Enter parameter name: ";
	  cin >> str;
	}
	cin.clear();
	cout << endl << "Enter item name: ";
	cin >> str;
	strip_str(str,STRIP_BOTH);
      }
      cin.clear();
      cout << endl << "Enter new table name>";
      cin >> ntname;
      if (!cin.eof())
	table.write(ntname);
      cin.clear();

      table2 = table;
      cout << endl << "Enter copied table name>";
      cin >> ntname;
      if (!cin.eof())
	table2.write(ntname);    
    }
    catch (Error&e) {
      e.print_error(__FILE__,__LINE__);
    }
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return 0;
}
 
