#ifndef PRODUCT_CONF_H
#define PRODUCT_CONF_H

#include <map>
#include <string>
#include <fstream>
#include <json.h>

class HeaderConf {
public:
	void add_card(const std::string& name, const std::string& value);
	void save_to(json_object* target) const;
private:
	std::map<std::string, std::string> cards;
};

class ProductConf {
public:
	void add_unit(const std::string name);
	HeaderConf& operator[](const std::string name);

	void save_to(std::ofstream& output) const;

private:
	std::map<std::string, HeaderConf> units;
};

void dc_save_product_configuration(ProductConf* product_configuration);

#endif // PRODUCT_CONF_H
