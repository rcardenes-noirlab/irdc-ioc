/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  fits_test.cc
 * 
 * GENERAL DESCRIPTION
 *  Simple test routine for FITS class
 *   
 * ORIGINAL AUTHOR: 
 *   Peter Young - RSAA 1993
 *
 * HISTORY
 *   Sept 1993 PJY    Created for RSAA CICADA package
 *   June 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "fits.h"
#include "exception.h"

#ifdef vxWorks
int fitsTest()
#else
int main()
#endif
{
  try {
    Fits *old_fits[FITS_MAX_EXTEND];
    Fits *new_fits;
    Fitsname old_name;
    Fitsname new_name;
    int extend,i;
    int naxes[2*FITS_MAX_EXTEND+2];
    Ext_Type etype[FITS_MAX_EXTEND+1];
    char *value;
    int bp[FITS_MAX_EXTEND+1];
    int pc[FITS_MAX_EXTEND+1];
    int gc[FITS_MAX_EXTEND+1];
    double bz[FITS_MAX_EXTEND+1];
    double bs[FITS_MAX_EXTEND+1];
    int ii,nf, j, k, numkw=6;
    const char * kw[6] = {"COMMENT","EXPOSED","JUNK","BLANK","QUOTES","BOOLEAN"};
    
    // Test algorithm
    // 1. Read an existing FITS file
    // 2. Create a new FITS file
    // 3. Write read data into new multi-extension file
    try {

      // Collect filenames of old and new
      cout << "Enter number of image extensions [0]: ";
      cin >> extend; 
      if (extend>FITS_MAX_EXTEND) 
	extend = FITS_MAX_EXTEND;
      if (extend==1) 
	extend = 0;

      cout << "Enter name of new FITS file: ";
      cin >> new_name;

      // Collect input parameters
      nf = (extend>0)? extend:1;
      j = (extend>0)? 1:0;
      naxes[0] = naxes[1] = 0;
      for (i=0; i<nf; i++)  { 
	cout << "Enter name of next FITS file" << i << ": ";
	cin >> old_name;
	old_fits[i] = new Fits();
	old_fits[i]->read(old_name);
	k = (old_fits[i]->extend>0)? 1:0;
	naxes[2*j] = old_fits[i]->xt[k].nx;
	naxes[2*j+1] = old_fits[i]->xt[k].ny;
	bp[j] = old_fits[i]->xt[k].bitpix;
	bz[j] = old_fits[i]->xt[k].bzero;
	bs[j] = old_fits[i]->xt[k].bscale;
	pc[j] = old_fits[i]->xt[k].pcount;
	gc[j] = old_fits[i]->xt[k].gcount;
	etype[j]=old_fits[i]->xt[k].type;
	j++;
      }
      bp[0] = old_fits[0]->xt[0].bitpix;
      bs[0] = old_fits[0]->xt[0].bscale;
      bz[0] = old_fits[0]->xt[0].bzero;
      new_fits = new Fits();

      // Now create the file
      new_fits->create(new_name, etype, bp, naxes,S_IRUSR|S_IWUSR,36, extend,36);
      
      cout << "Fits file " << new_name << " created." << endl;

      // Write standard keywords
      new_fits->write_std_keywords(etype, bs, bz, pc, gc);

      cout << "Fits file " << new_name << " standard keywords written." << endl;
      
      // write out data extensions
      j = (extend>0)? 1:0;
      for (i=0; i<nf; i++) {

	cout << "Writing extension " << j << endl;

	k = (old_fits[i]->extend>0)? 1:0;

	// Write a set of sample optional keywords to each header unit
	new_fits->write_skeyword("COMMENT", "XO1 = 0", "Region 1 X origin",j);
	new_fits->write_dkeyword("EXPOSED", 100.123, 6,"Requested exposure in secs",j);
	new_fits->write_ikeyword("JUNK", 1,"Junk integer keyword",j);
	new_fits->write_bkeyword("BOOLEAN", 1,"Test boolean keyword",j);
	new_fits->write_skeyword("BLANK","", "a blank KW",j);
	new_fits->write_skeyword("QUOTES","John O''Byrne", "propid with quotes",j);
	cout << "All keywords written = " << new_fits->num_keywords(j) << endl;
       
	new_fits->write_data(old_fits[i]->xt[k].data, old_fits[i]->xt[0].bitpix, j);

	cout << "Data written " << endl;

	old_fits[i]->close();
	delete old_fits[i];
	j++;
      }
      new_fits->close();

      cout << "Fits file " << new_name << " closed."  << endl;

      // Now test reading
      new_fits->open(new_name);

      cout << "Fits file " << new_name << " opened again ok."  << endl;

      j = (new_fits->extend>0)? 1:0;

      for (ii=0; ii<numkw; ii++) {
	if ((value = new_fits->read_keyword(kw[ii],j)) != NULL) {
	  cout << "Testing " << kw[ii] <<" keyword = |" << value <<"|"<< endl; 
	  free(value);
	} else 
	  cout << "*** Failed to read "<< kw[ii]<<" keyword!" << endl;
      }

      new_fits->close();

      cout << "New fits file closed" << endl;

      delete new_fits;
    }
    catch (Error& e) {
      e.print_error(__FILE__,__LINE__);
    }
      
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return 0;
}
