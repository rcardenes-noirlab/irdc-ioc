/*
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME ir_camera.cc
 *
 * GENERAL DESCRIPTION
 *
 * CICADA InfraRed camera class implementation.
 *
 * Implements specific operations for an infrared camera.  This class
 * understands how to operate and control an infrared camera. It interfaces to
 * an infrared detector through a detector controller and acquires data from the
 * detector using different infrared exposure modes. Data are then processed in
 * usable form before being transferred to final storage.
 *
 */

/* Include files */
#include <errno.h>
#include <math.h>
#include "dummy_controller.h"
#include "sdsu3_5_controller.h"
#include "config_table.h"
#include "common.h"
#include "ir_setup.h"
#include "ir_camera.h"
#include "dc_data.h"
#include <stdexcept>
#include "product_conf.h"

#include "dc.h"
extern Parameter *dc_status;
ProductConf* product_configuration = nullptr;

/* typedefs */

/* class declarations */

/*
 *+
 * CLASS
 *   IrTimerThread
 *   Thread subclass for exposure timing.
 * DESCRIPTION
 *   This thread simply update the status database with timer values in
 *   a regular fashion - ie every 0.5 seconds. It waits for the exposure
 *   to be started before beginning its count and stops when the count reaches
 *   zero.
 *
 *-
 */

class IrTimerThread : public Thread {
public:
	IrTimerThread(IR_Camera *cam, const string &name,
			unsigned int priority=epicsThreadPriorityLow);
	virtual void run();
private:
	IR_Camera *ir_camera;
};

/* global variables */
static char IR_DEBUG_DUMP_FILE[] = "/tmp/ir_debug.dump";
const std::string IR_Camera::PARAMETER_NAME = "IRPar";

/* local function declarations */

/*============================================================================
 *  Ancillary code
 *===========================================================================*/
/*
 *+
 *
 * The following templates instantiate controllers based on the choice of
 * controller type and set of arguments.
 *
 * These are specializations that have been factored out from the IR_Camera
 * constructors to avoid code repetition.
 *-
 */

template<typename T>
T* build_controller(Camera_Desc &cam, int cmid, int c, const char* param_host,
		    Instrument_Status *st, Platform_Specific *ps)
{
	return new T(cam, cmid, c, param_host, st, ps);
}

template<typename T>
T* build_controller(Instrument_Status *st, Parameter *ip,
		    Parameter *csp, Parameter* irsp, int deb,
		    Platform_Specific *ps)
{
	return new T(st, ip, csp, irsp, deb, ps);
}


template<typename... Ts>
Controller* get_controller(Controller_Type ct, Ts... args) {
	switch (ct) {
		case DUMMY:
			return build_controller<Dummy_Controller>(args...);
			break;
		case SDSU3_5:
			return build_controller<Sdsu3_5_Controller>(args...);
			break;
		default:
			throw std::invalid_argument("Selected controller type not implemented");
	}

	ostringstream ostr;
	ostr << "Controller type " << ct << " unsupported!" << ends;
	throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);
}

/* local function definitions */
/*============================================================================
 *  Camera
 *===========================================================================*/
/*
 *+
 * FUNCTION NAME: IR_Camera::IR_Camera()
 *
 * INVOCATION: camera = new IR_Camera(Camera_Desc& cam, int camid, int c)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam - description of this camera
 * > camid - identification number of camera
 * > c - identification number of the controller to be used by camera.
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 * Simple Constructor used for initialising configuration variables
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of an IR camera
 * object.  It is specifically used to handle an IR camera.
 *
 * This version of the constructor is used just for holding an IR cameras
 * configuration information. It is not used to interface with real hardware. It
 * is, therefore, only useful in modules that need to know about a configuration
 * but do not actually interface with hardware.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
IR_Camera::IR_Camera(Camera_Desc& cam, int camid, int c, Platform_Specific* ps)
	: Camera(cam,camid,c,ps),
	  dummy_controller(nullptr),
	  sdsu_controller(nullptr)
{
	// No status for this constructor
	ir_status = nullptr;

	// Initialise some member vars
  	IR_Camera::init_vars();
}
/*
 *+
 * FUNCTION NAME: IR_Camera::IR_Camera()
 *
 * INVOCATION: camera = new IR_Camera(Camera_Desc& cam, int camid, int c, const char* param_host,
 *                          Instrument_Status *st, paramMapType *pm)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam - description of camera.
 * > camid - identification number of camera
 * > c - identification number of controller owned by camera.
 * > param_host - name of host where parameter database resides.
 * > st - pointer to instrument status data structure for all instrument status
 * > pm - pointer to parameter map type
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 * Constructor used for initialising configuration variables and setting up the
 * parameter database
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of an IR camera
 * object.  It is used specifically for an IR camera.
 *
 * This version of the constructor is used just for holding a cameras
 * configuration information and also its status parameter database. It is not
 * used to interface with real hardware. It is, therefore, only useful in
 * modules that need to know about a configuration and be able to read/write
 * status parameters but do not actually interface with hardware.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

IR_Camera::IR_Camera(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st, Platform_Specific* ps)
	: Camera(cam,camid,c,param_host,st,ps),
	  dummy_controller(nullptr),
	  sdsu_controller(nullptr)
{
	// Set local pointer to IR part of status structure
	ir_status = &camera_status->op_status.Camera_Op_Status_u.ir;

	// Set the camera operation type to IR
	camera_statusp->put(OP_STATUS_TYPE, (int)IR_CAMERA);

	// Initialise some member vars
	IR_Camera::init_vars();

	// Setup status parameters but don't initialise them (done in major constructor)
	add_status_parameters(false);

	controller = get_controller(camera.controller[cid].type,
			cam, camid, c, param_host, st, ps);
}
/*
 *+
 * FUNCTION NAME: IR_Camera::IR_Camera()
 *
 * INVOCATION: camera = new IR_Camera(Instrument_Status *st, Config *cf, Parameter* ip, Parameter* cp,
 *                                    Parameter* op, int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > st - pointer to instrument status data structure for all instrument status
 * > cf - pointer to the configuration structure for this instrument
 * > ip - pointer to the parameter database for the instrument
 * > cp - pointer to the parameter database for the camera
 * > op - pointer to the operation parameter database
 * > deb - debug flag
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Constructor used for interfacing to real IR camera hardware
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of an IR camera
 * object.  It is used directly to interface to an IR camera.
 *
 * This version of the constructor is used for interfacing with real
 * hardware. It is, therefore, required for components that are attached to the
 * hardware. Its knows how to communicate with an IR camera through the
 * detector controller member.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
IR_Camera::IR_Camera(Instrument_Status *st, Parameter* ip, int deb, Platform_Specific* ps)
	: Camera(st,ip,deb,ps),
	  dummy_controller(nullptr),
	  sdsu_controller(nullptr)
{
	// Set local pointer to IR part of status structure
	ir_status = &camera_status->op_status.Camera_Op_Status_u.ir;

	// Set the camera operation type to IR
	camera_statusp->put(OP_STATUS_TYPE, (int)IR_CAMERA);

	// Initialise some member vars
	IR_Camera::init_vars();

	// Setup status parameters
	add_status_parameters(true);

	// Create controller object
	controller = get_controller(camera.controller[cid].type,
			st, ip, camera_statusp, ir_statusp, deb, ps);
}
/*
 *+
 * FUNCTION NAME: IR_Camera::init_vars()
 *
 * INVOCATION: init_vars()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Initialise some member vars for constructors
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void IR_Camera::init_vars()
{
  IR_Expose texpose = {
    IR_VIEW_MODE,                  // irMode
    DEFAULT_VIEW_ENABLED,          // viewEnabled
    {DEFAULT_DO_IR_READOUT, // einfo: doReadout
     DEFAULT_DO_RESET,             // doReset
//     DEFAULT_SET_INTERVAL,         // setIntvl       << OUT
     DEFAULT_READ_MODE,            // readModeSet
     DEFAULT_QUAD_MODE,
//     DEFAULT_TIME_MODE,            // timeModeSet       << OUT
     DEFAULT_NFOWLER,              // nfowler       << OUT
//     DEFAULT_READ_TIME,            // readTime       << OUT
//     DEFAULT_READ_INTVAL,          // readIntvl       << OUT
//     DEFAULT_PERIOD,               // period       << OUT
//     DEFAULT_NPERIODS,             // nperiods       << OUT
     DEFAULT_EXPTIME,              // exposedRQ
     DEFAULT_EXPTIME,              // exposed
//     DEFAULT_NCOADDS,              // ncoadds       << OUT
     DEFAULT_NRESETS,              // nresets       << OUT
//     DEFAULT_RESET_DELAY},         // resetDelay       << OUT
     },
    {DEFAULT_DO_SUBTRACT,   // dinfo: doSubFile
     DEFAULT_DO_SAVE,              // save
     DEFAULT_DO_SAVE_VAR,          // saveVar
     DEFAULT_DO_SAVE_QUAL,         // saveQual
     DEFAULT_SAVE_EACH_NDR,        // saveNdrs
     DEFAULT_DISPLAY_EACH_NDR,     // displayNdrs
     DEFAULT_SAVE_EACH_COADD,      // saveCoadds
     DEFAULT_DISPLAY_EACH_COADD,   // displayCoadds
     DEFAULT_COSMIC_REJ,           // cosmRej
     DEFAULT_DO_CMP_IM,            // doCmpIm
     DEFAULT_WAVE_MIN,             // waveMin
     DEFAULT_WAVE_MAX,             // waveMax
     DEFAULT_COSM_THRESH,          // cosmThrsh
     DEFAULT_COSM_MIN,             // cosmMin
     DEFAULT_SUB_FNAME             // subFile
    }
  };

  expose_set = texpose;
  view_einfo = texpose.einfo;
  observe_einfo = texpose.einfo;
  nview = 0;

  // Initialise the ir status parameters
  if (ir_status != nullptr) {
    ir_status->view = texpose.einfo;
    ir_status->obs = texpose.einfo;
    ir_status->d_view = texpose.dinfo;
    ir_status->d_obs = texpose.dinfo;

    ir_status->prep = ir_status->acq = ir_status->rdout = false;
    ir_status->nreads = ir_status->nreadsDone = 0;
    ir_status->ncoaddsDone = ir_status->nfsDone = 0;
    ir_status->irProgress = ir_status->irTarget = 0;
    ir_status->timeLeft = ir_status->perTimeLeft = 0.0;
    ir_status->irMode = IR_NULL_MODE;
    ir_status->viewEnabled = false;
  }

  // Initialise pointers to work arrays
  ir_statusp = nullptr;
  fitPix = nullptr;
  fitPixSave = nullptr;
  zeroPixSave = nullptr;
  coaddPix = nullptr;
  pixSum = nullptr;
  fpixSum = nullptr;
  fit_sy = nullptr;
  fit_siy = nullptr;
  fit_sy2= nullptr;
  variance = nullptr;
  variance1 = nullptr;
  badPix = nullptr;
  cosmicPix = nullptr;
  ir_timer_thr = nullptr;
  ir_einfo = nullptr;
  ir_dinfo = nullptr;

  // Initialise controller object
  dummy_controller = nullptr;
  sdsu_controller = nullptr;
}
/*
 *+
 * FUNCTION NAME: IR_Camera::add_status_parameters
 *
 * INVOCATION: add_status_parameters(bool init)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Adds all IR camera status parameters to the parameter database
 *
 * DESCRIPTION: Uses the Parameter class to handle any Cicada status
 * parameters. This class does immediate write-thru operations to the
 * Cicada status shared memory on the observer computer any where on the net.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void IR_Camera::add_status_parameters(bool init)
{
  int *iValid;
  long *lValid;
  double* dValid;
  bool bValid[2];

  // Create the IR parameter DB
  ir_statusp = new Parameter(camera_pDB, PARAMETER_NAME, hardware_param_map, IR_PREFIX, DB_SUFFIX);

  // Now add all the Cicada Status parameters
  ir_status->req_type = IR_CAMERA_INVALID;
  ir_statusp->add(IR_REQ_TYPE, nullptr, (void*)&ir_status->req_type, init, 1, (int*)&ir_status->req_type,
		  VALIDITY_NONE, 0, (int*) nullptr, PUBLISHED);

  ir_statusp->add(IR_ACTIVITY, nullptr, (void*)&ir_status->activity, init, 1, (int*)&ir_status->activity,
		  VALIDITY_NONE, 0, (int*)nullptr, PUBLISHED);

  // Add view mode status parameters
  add_ir_parameters(IR_VIEW_MODE, init);

  // Add observe mode status parameters
  add_ir_parameters(IR_OBSERVE_MODE, init);

  // Add extra status parameters
  bValid[0]=false;
  bValid[1]=true;
  ir_status->prep = false;
  ir_statusp->add(PREP, PREP, (void*)&ir_status->prep, init, 1, (bool*)&ir_status->prep, VALIDITY_SET, 2, bValid,
		  PUBLISHED);

  ir_status->acq = false;
  ir_statusp->add(ACQ, ACQ, (void*)&ir_status->acq, init, 1, (bool*)&ir_status->acq, VALIDITY_SET, 2, bValid, PUBLISHED);

  ir_status->rdout = false;
  ir_statusp->add(RDOUT, RDOUT, (void*)&ir_status->rdout, init, 1, (bool*)&ir_status->rdout, VALIDITY_SET, 2, bValid,
		  PUBLISHED);


  lValid = new long[3];
  lValid[0] = 0;
  lValid[1] = MAXI;
  ir_statusp->add(IR_PROGRESS, IR_PROGRESS, (void*)&ir_status->irProgress, init,
		  1, &ir_status->irProgress, VALIDITY_RANGE, 2, lValid, PUBLISHED);

  ir_statusp->add(IR_TARGET, IR_TARGET, (void*)&ir_status->irTarget, init,
		  1, &ir_status->irTarget, VALIDITY_RANGE, 2, lValid, PUBLISHED);

  ir_statusp->add(NCOADDS_DONE, NCOADDS_DONE, (void*)&ir_status->ncoaddsDone, init,
		  1, &ir_status->ncoaddsDone, VALIDITY_RANGE, 2, lValid, PUBLISHED);

  ir_statusp->add(NREADS, NREADS, (void*)&ir_status->nreads, init,
		  1, &ir_status->nreads, VALIDITY_RANGE, 2, lValid, PUBLISHED);

  ir_statusp->add(NREADS_DONE, NREADS_DONE, (void*)&ir_status->nreadsDone, init,
		  1, &ir_status->nreadsDone, VALIDITY_RANGE, 2, lValid, PUBLISHED);

  ir_statusp->add(NFS_DONE, NFS_DONE, (void*)&ir_status->nfsDone, init,
		  1, &ir_status->nfsDone, VALIDITY_RANGE, 2, lValid, PUBLISHED);

  dValid = new double[2];
  dValid[0] = 0;
  dValid[1] = MAXD;
  ir_status->timeLeft = ir_status->perTimeLeft = ir_status->expElapsed = 0.0;
  ir_statusp->add(TIME_LEFT, TIME_LEFT, (void*)&ir_status->timeLeft, init,
		  1, &ir_status->timeLeft, VALIDITY_RANGE, 2, dValid, PUBLISHED);

//  ir_statusp->add(PER_TIME_LEFT, PER_TIME_LEFT, (void*)&ir_status->perTimeLeft, init,
//		  1, &ir_status->perTimeLeft, VALIDITY_RANGE, 2, dValid, PUBLISHED);

  ir_statusp->add(EXPOSURE_ELAPSED, EXPOSURE_ELAPSED, (void*)&ir_status->expElapsed, init,
		  1, &ir_status->expElapsed, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);

  iValid = new int[3];
  iValid[0] = (int)IR_NULL_MODE;
  iValid[1] = (int)IR_VIEW_MODE;
  iValid[2] = (int)IR_OBSERVE_MODE;
  ir_statusp->add(IR_MODE, IR_MODE, (void*)&ir_status->irMode, init,
		  1, (int*)&ir_status->irMode, VALIDITY_SET, 3, iValid, UNPUBLISHED);

  ir_statusp->add(VIEW_ENABLED, VIEW_ENABLED, (void*)&ir_status->viewEnabled, init,
		  1, (bool*)&ir_status->viewEnabled, VALIDITY_NONE, 0, (bool*)nullptr, UNPUBLISHED);

  delete [] lValid;
  delete [] iValid;
  delete [] dValid;
}
/*
 *+
 * FUNCTION NAME: IR_Camera::add_ir_parameters
 *
 * INVOCATION: add_ir_parameters(IR_Mode ir_mode, bool init)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *
 * DESCRIPTION:
 *
 * Uses the Parameter class to handle all IR status parameters for the specified
 * read mode. This class does immediate write-thru operations to the Cicada
 * status shared memory on the observer computer any where on the net.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *- */

struct ModePrefix {
	string pref;

	string dbName(const string& suff) {
		return pref + "_" + suff;
	}

	void prepend(string s) {
		pref = s + pref;
	}
};

void IR_Camera::add_ir_parameters(IR_Mode ir_mode, bool init)
{
  int *iValid;
  long *lValid;
  double* dValid;
  bool bValid[2];
  ModePrefix mode;
  string db_mode,par;

  if (ir_mode == IR_VIEW_MODE) {
    mode.pref = "view";
    ir_einfo = &ir_status->view;
    ir_dinfo = &ir_status->d_view;
  } else {
    mode.pref = "obs";
    ir_einfo = &ir_status->obs;
    ir_dinfo = &ir_status->d_obs;
  }

  // Now for each parameter, encode name and update value

  iValid = new int[6];
  lValid = new long[6];
  dValid = new double[2];

  bValid[0] = true;
  bValid[1] = false;
  lValid[0] = 0;
  lValid[1] = 1;
  ir_statusp->add(mode.dbName(DO_IR_READOUT), nullptr, (void*)&ir_einfo->doReadout, init,
		  1, &ir_einfo->doReadout, VALIDITY_RANGE, 2, lValid, UNPUBLISHED);

  ir_statusp->add(mode.dbName(DO_RESET), nullptr, (void*)&ir_einfo->doReset, init,
		  1, &ir_einfo->doReset, VALIDITY_RANGE, 2, lValid, UNPUBLISHED);

//  ir_statusp->add(mode.dbName(SET_INTERVAL), nullptr, (void*)&ir_einfo->setIntvl, init,
//		  1, &ir_einfo->setIntvl, VALIDITY_RANGE, 2, lValid, UNPUBLISHED);

  ir_einfo->readModeSet = IR_VERY_BRIGHT_READOUT;
  iValid[0] = (int)IR_VERY_BRIGHT_READOUT;
  iValid[1] = (int)IR_BRIGHT_READOUT;
  iValid[2] = (int)IR_FAINT_READOUT;
  iValid[3] = (int)IR_VERY_FAINT_READOUT;
  iValid[4] = (int)IR_READOUT_NULL;
  ir_statusp->add(mode.dbName(IR_READ_MODE), mode.dbName(IR_READ_MODE), (void*)&ir_einfo->readModeSet, init,
		  1, (int*)&ir_einfo->readModeSet, VALIDITY_SET, 5, iValid, UNPUBLISHED);

  ir_einfo->quadrantsSet = IR_FULL_FRAME;
  iValid[0] = (int)IR_FULL_FRAME;
  iValid[1] = (int)IR_1_QUADRANT;
  iValid[2] = (int)IR_QUADRANT_NULL;
  ir_statusp->add(mode.dbName(IR_QUAD_MODE), mode.dbName(IR_QUAD_MODE), (void*)&ir_einfo->quadrantsSet, init,
		  1, (int*)&ir_einfo->quadrantsSet, VALIDITY_SET, 3, iValid, UNPUBLISHED);

/*
  ir_einfo->timeModeSet = IR_CALC_EXPTIME;
  iValid[0] = (int)IR_CALC_EXPTIME;
  iValid[1] = (int)IR_CALC_READPERIOD;
  iValid[2] = (int)IR_CALC_NPERIODS;
  ir_statusp->add(mode.dbName(TIME_MODE), nullptr, (void*)&ir_einfo->timeModeSet, init,
		  1, (int*)&ir_einfo->timeModeSet, VALIDITY_SET, 3, iValid, UNPUBLISHED);
		  */

  lValid[0] = 1;
  lValid[1] = MAX_NFOWLER;
  ir_statusp->add(mode.dbName(NFOWLER), mode.dbName(NFOWLER), (void*)&ir_einfo->nfowler, init,
		  1, &ir_einfo->nfowler, VALIDITY_RANGE, 2, lValid, UNPUBLISHED);

  /*

  dValid[0] = MIN_READ_TIME;
  dValid[1] = MAX_READ_TIME;
  ir_statusp->add(mode.dbName(READ_TIME), mode.dbName(READ_TIME), (void*)&ir_einfo->readTime, init,
		  1, &ir_einfo->readTime, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);

  dValid[0] = 0;
  dValid[1] = MAX_INTERVAL;
  ir_statusp->add(mode.dbName(READ_INTVAL), mode.dbName(READ_INTVAL), (void*)&ir_einfo->readIntvl, init,
		  1, &ir_einfo->readIntvl, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);

  dValid[0] = 0;
  dValid[1] = MAX_PERIOD;
  ir_statusp->add(mode.dbName(PERIOD), mode.dbName(PERIOD), (void*)&ir_einfo->period, init,
		  1, &ir_einfo->period, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);

  lValid[0] = 0;
  lValid[1] = MAX_NPERIODS;
  ir_statusp->add(mode.dbName(NPERIODS), mode.dbName(NPERIODS), (void*)&ir_einfo->nperiods, init,
		  1, &ir_einfo->nperiods, VALIDITY_RANGE, 2, lValid, UNPUBLISHED);
*/

  dValid[0] = 0;
  dValid[1] = MAX_EXPOSED;
  ir_statusp->add(mode.dbName(IR_EXPOSEDRQ), mode.dbName(EXPOSEDRQ), (void*)&ir_einfo->exposedRQ, init,
		  1, &ir_einfo->exposedRQ, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);

  ir_statusp->add(mode.dbName(IR_EXPOSED), mode.dbName(IR_EXPOSED), (void*)&ir_einfo->exposed, init,
		  1, &ir_einfo->exposed, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);

/*
  lValid[0] = 1;
  lValid[1] = MAX_NCOADDS;
  ir_statusp->add(mode.dbName(NCOADDS), mode.dbName(NCOADDS), (void*)&ir_einfo->ncoadds, init,
		  1, &ir_einfo->ncoadds, VALIDITY_RANGE, 2, lValid, UNPUBLISHED);

*/

  lValid[0] = 0;
  lValid[1] = MAXI;
  ir_statusp->add(mode.dbName(NRESETS), mode.dbName(NRESETS), (void*)&ir_einfo->nresets, init,
		  1, &ir_einfo->nresets, VALIDITY_RANGE, 2, lValid, PUBLISHED);

/*
  dValid[0] = 0;
  dValid[1] = MAX_RESET_DELAY;
  ir_statusp->add(mode.dbName(RESET_DELAY), mode.dbName(RESET_DELAY), (void*)&ir_einfo->resetDelay, init,
		  1, &ir_einfo->resetDelay, VALIDITY_RANGE, 2, dValid, PUBLISHED);
*/

  // Add the data info parameters
  mode.prepend("d_");

  ir_statusp->add(mode.dbName(DO_COSM_REJ), mode.dbName(DO_COSM_REJ), (void*)&ir_dinfo->cosmRej, init,
		  1, (bool*)&ir_dinfo->cosmRej, VALIDITY_SET, 2, bValid, UNPUBLISHED);

  dValid[0] = 0;
  dValid[1] = MAXD;
  ir_statusp->add(mode.dbName(COSM_THRESH), mode.dbName(COSM_THRESH), (void*)&ir_dinfo->cosmThrsh, init,
		  1, &ir_dinfo->cosmThrsh, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);

  ir_statusp->add(mode.dbName(COSM_MIN), mode.dbName(COSM_MIN), (void*)&ir_dinfo->cosmMin, init,
		  1, &ir_dinfo->cosmMin, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);

  // Following two only available in observe mode
  if (ir_mode == IR_OBSERVE_MODE) {
    ir_statusp->add(mode.dbName(DO_SAVE), mode.dbName(DO_SAVE), (void*)&ir_dinfo->save, init,
		    1, (bool*)&ir_dinfo->save, VALIDITY_SET, 2, bValid, UNPUBLISHED);

    ir_statusp->add(mode.dbName(DO_SAVE_NDR), mode.dbName(DO_SAVE_NDR), (void*)&ir_dinfo->saveNdrs, init,
		    1, (bool*)&ir_dinfo->saveNdrs, VALIDITY_SET, 2, bValid, UNPUBLISHED);

    ir_statusp->add(mode.dbName(DO_SAVE_COADD), mode.dbName(DO_SAVE_COADD), (void*)&ir_dinfo->saveCoadds, init,
		    1, (bool*)&ir_dinfo->saveCoadds, VALIDITY_SET, 2, bValid, UNPUBLISHED);

    ir_statusp->add(mode.dbName(DO_SAVE_VAR), mode.dbName(DO_SAVE_VAR), (void*)&ir_dinfo->saveVar,
		    init, 1, (bool*)&ir_dinfo->saveVar, VALIDITY_SET, 2, bValid, UNPUBLISHED);

    ir_statusp->add(mode.dbName(DO_SAVE_QUAL), mode.dbName(DO_SAVE_QUAL), (void*)&ir_dinfo->saveQual,
		    init, 1, (bool*)&ir_dinfo->saveQual, VALIDITY_SET, 2, bValid, UNPUBLISHED);
  } else {
    ir_statusp->add(mode.dbName(DO_SUBTRACT), mode.dbName(DO_SUBTRACT), (void*)&ir_dinfo->doSubFile,
		    init, 1, (bool*)&ir_dinfo->doSubFile, VALIDITY_SET, 2, bValid, UNPUBLISHED);

    ir_statusp->add(mode.dbName(DO_CMP_IM), mode.dbName(DO_CMP_IM), (void*)&ir_dinfo->doCmpIm, init,
		    1, (bool*)&ir_dinfo->doCmpIm, VALIDITY_SET, 2, bValid, UNPUBLISHED);

    ir_statusp->add(mode.dbName(SUB_FNAME), mode.dbName(SUB_FNAME), (void*)ir_dinfo->subFile, init,
		    FILENAMELEN, ir_dinfo->subFile, VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

    dValid[0] = DEFAULT_WAVE_MIN;
    dValid[1] = DEFAULT_WAVE_MAX;
    ir_statusp->add(mode.dbName(WAVE_MIN), mode.dbName(WAVE_MIN), (void*)&ir_dinfo->waveMin, init,
		    1, &ir_dinfo->waveMin, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);

    ir_statusp->add(mode.dbName(WAVE_MAX), mode.dbName(WAVE_MAX), (void*)&ir_dinfo->waveMax, init,
		    1, &ir_dinfo->waveMax, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);
  }
  delete [] iValid;
  delete [] lValid;
  delete [] dValid;
}
/*
 *+
 * FUNCTION NAME: IR_Camera::init()
 *
 * INVOCATION: camera->init(Init init_rec)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > init_rec - initialisation record
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Do generic IR camera operation initialisation
 *
 * DESCRIPTION: Init is essentially a reset operation - so
 * reset the system here.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void IR_Camera::init(Init init_rec)
{

}


/*
 *+
 * FUNCTION NAME:  prepare_readout()
 *
 * INVOCATION: prepare_readout(int n_sync, IR_Expose& expose)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > n_sync - number of controllers to sync
 * > expose - Exposure record
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: prepares system for a readout
 *
 * DESCRIPTION:
 * . Takes input regions spec and produces a detector readout region spec.
 * . Calls the controller setup readout method
 * . Sets up the image header for data transfer
 * . sizes and creates the image readout buffer
 * . prepares and initialises workspace arrays
 * . starts the image readout thread
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES: None
 *
 *-
 */
void IR_Camera::prepare_readout(int n_sync, IR_Expose& expose)
{
  ostringstream ostr;
  int cur_id;
  int ibs;
  unsigned long np;
  Detector_Image_Info image_info;
  Detector_Readout *readout_rec = &expose.readout;
  IR_Exposure_Info *tir_einfo = &expose.einfo;
  IR_Data_Info *tir_dinfo = &expose.dinfo;

  // Copy region spec to Region structure
  // Repeat for each controller on camera

  // For IR cameras the number of reference pixels are handled as overscan cols.
  setup_regions(readout_rec->regions, controller->image_info.bitpix);

  cur_id = cid;

  // Call the specific controller method for setting up the readout
  controller->setup_readout(n_sync, readout_rec, rgn[cur_id], expose.einfo, requested_image_size, exptime);

  if (debug&DEBUG_INDICATORS) {
    message_write("Finished setup readout");
  }

  // Use the generic Camera method for setting up main_header
  seq_id = status->id;

  // For IR cameras we use floating point fitPix buffer for transfer
  image_info.bitpix = FLOAT_BITPIX;
  image_info.bscale = 1.0;
  image_info.bzero = 0.0;
  image_info.data_type = RAW_DATA;
  image_info.unsigned_data = false;

  setup_image_header(seq_id, cam_id, cid, readout_rec->verify, &camera, main_header, rgn, readout_rec->regions,
		     (char*)&image->frames_mem, image_info, tir_dinfo->saveVar, tir_dinfo->saveQual);

  // Anything non-standard can be changed separately
  adjust_image_header(main_header);

  if (debug&DEBUG_INDICATORS) {
    message_write("Finished setup image header");
  }

  // Setup the header info for the raw data
  image_info = controller->image_info;
  image_info.data_type = RAW_DATA;
  setup_image_header(seq_id, cam_id, cid, false, &camera, raw_header, rgn, readout_rec->regions, (char*)&image->raw_frames_mem,
		     image_info, false, false);

  // Anything non-standard can be changed separately
  adjust_image_header(raw_header);

  if (debug&DEBUG_INDICATORS) {
    message_write("Finished setup raw image header");
  }


  // Prepare IR readout buffer - make it an integral number of images
  ibs = (camera.n_buffers>0)? camera.n_buffers*requested_image_size:requested_image_size;
  if (controller->create_image_buffer(ibs) == nullptr) {
    ostr << "Failed to create image buffer of size " << ibs << " bytes for readout!" << ends;
    throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);	
  }

  if (debug&DEBUG_INDICATORS) {
    message_write("Finished create image buffer");
  }

  // Number of bits per pixels must be > 0?
  if (!bytepix) {
    throw Error("IR_Camera: Bytes per pixel (bytepix) must be greater than zero!",
		E_ERROR, -1, __FILE__, __LINE__);
  }
  np = requested_image_size/bytepix;


/* TODO: This used to depend on readout mode. But now that doesn't matter.
 *       Redo.
 */
  /*
  // For linear fitting prepare intermediate calculation buffers
  if (tir_einfo->readModeSet == IR_LINEAR_READOUT) {

    // Allocate new summing buffers for each pixel
    if (fit_sy != nullptr)
      delete [] fit_sy;
    fit_sy = new double[np];
    if (!fit_sy) {
      ostr << "Failed to create buffer 'fit_sy' for linear fitting calculations!" << ends;
      throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);	
    }
    memset((void*)fit_sy, 0, np * sizeof(double));

    if (fit_sy2 != nullptr)
      delete [] fit_sy2;
    fit_sy2 = new double[np];
    if (!fit_sy2) {
      ostr << "Failed to create buffer 'fit_sy2' for linear fitting calculations!" << ends;
      throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);	
    }
    memset((void*)fit_sy2, 0, np * sizeof(double));

    if (fit_siy != nullptr)
      delete [] fit_siy;
    fit_siy = new double[np];
    if (!fit_siy) {
      ostr << "Failed to create buffer 'fit_siy' for linear fitting calculations!" << ends;
      throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);	
    }
    memset((void*)fit_siy, 0, np * sizeof(double));

    if (variance != nullptr)
      delete [] variance;
    variance = new float[np];
    if (!variance) {
      ostr << "Failed to create buffer 'variance' for linear fitting calculations!" << ends;
      throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);	
    }
    memset((void*)variance, 0, np * sizeof(float));

    if (tir_dinfo->cosmRej) {
      if (variance1 != nullptr)
	delete [] variance1;
      variance1 = new float[np];
      if (!variance1) {
	ostr << "Failed to create buffer 'variance1' for linear fitting calculations!" << ends;
	throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);	
      }
      memset((void*)variance1, 0, np * sizeof(float));

    }

    if (cosmicPix != nullptr)
      delete [] cosmicPix;
    cosmicPix = new unsigned char[np];
    if (!cosmicPix) {
      ostr << "Failed to create buffer to hold cosmic pixels!" << ends;
      throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);	
    }
    memset((void*)cosmicPix, 0, np * sizeof(unsigned char));

    if (zeroPixSave != nullptr)
      delete [] zeroPixSave;
    zeroPixSave = new unsigned short[np];
    if (!zeroPixSave) {
      ostr << "Failed to create buffer 'zeroPixSave' for linear fitting calculations!" << ends;
      throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);	
    }
    memset((void*)zeroPixSave, 0, np * sizeof(unsigned short));
  }

  // Prepare fitPixSave buffer if needed
  if ((tir_einfo->readModeSet == IR_DCS_READOUT) ||
      (tir_einfo->readModeSet == IR_FOWLER_READOUT) ||
      ((tir_einfo->readModeSet == IR_LINEAR_READOUT) && tir_dinfo->cosmRej)) {
    if (fitPixSave != nullptr)
      delete [] fitPixSave;
    fitPixSave = new float[np];
    if (!fitPixSave) {
      ostr << "Failed to create buffer to hold the saved fitted pixels!" << ends;
      throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);	
    }
    memset((void*)fitPixSave, 0, np * sizeof(float));

  }
  */

  // Allocate space for zeroth NDR for linear fitting


  if (debug&DEBUG_INDICATORS) {
    message_write("Finished setting up buffers ");
  }

  // Start the image transfer thread - specific for each camera
  rows_processed = 0;
  controller->doing_image_transfer = false;
  controller->finished_image_transfer = false;
  controller->readout_no = -1;
  controller->readouts_done = 0;
  controller->read_running = 0;

  // Begin the exposure timer thread. It will track time for reporting to status
  // database - it is not the accurate timer but rather a close indicator
  ir_timer_thr = new IrTimerThread(this, "ir_timer");
  ir_timer_thr->start();

  controller->start_ir_image_transfer_thread(expose, ir_timer_thr);

  if (debug&DEBUG_INDICATORS) {
    message_write("Image transfer thread started");
  }

}

/*
 *+
 * FUNCTION NAME:  initialise_readout_buffers()
 *
 * INVOCATION: initialise_readout_buffers()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Initialise pixSum and other calculation buffers to zero
 *
 * DESCRIPTION: For readout modes when nfs>0 pixels are summed in the pixSum array
 * Intermediate results are accumulated in the fitPix array.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES: None
 *
 *-
 */
void IR_Camera::initialise_readout_buffers()
{
// TODO: Redo, if needed
/*
  unsigned long np;

  // Protect against divide by zero
  if (!bytepix) {
    throw Error("IR_Camera: Bytes per pixel (bytepix) must be greater than zero!",
		E_ERROR, -1, __FILE__, __LINE__);
  }
  np = requested_image_size/bytepix;

  // Zero pixSum array
  memset((void*)pixSum, 0, np * sizeof(unsigned long));

  // Zero fpixSum array
  memset((void*)fpixSum, 0, np * sizeof(float));
  */
}
/*
 *+
 * FUNCTION NAME:  readout()
 *
 * INVOCATION: readout(Detector_Readout readout_rec, int ndr)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > readout_rec - Cicada Readout record
 * > ndr - non-destructive readout number
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: IR readout method
 *
 * DESCRIPTION:
 *
 * The readout method handles a full individual IR readout.  For an IR exposure,
 * this method will be called multiple times to handle each NDR.  It processes
 * the readout data according to the chosen readout region spec and read mode.
 * As data are accumulated in the readout buffer by the readout thread, this
 * method will take progressive chunks for processing and, eventually, data
 * transfer. Therefore this method operates in parallel with the reading out of
 * data.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: prepare_readout needs to have been called
 *
 * DEFICIENCIES: None
 *
 *-
 */
void IR_Camera::readout(Detector_Readout readout_rec, int ndr)
{
  ostringstream ostr;                           // Message stream
  int i,rr,sr,nser,fs,bufno;                 // Loop counters & indices
  int fwidth,fheight;                        // full wid and hei of current region
  int segno,row;                             // row counters
  epicsTime btime,stime;                     // timestamps
  double wtime, ttime,vtime;
  int timeout_wait;                          // Timeout to ensure that loops don't block forever
  int image_buf_size;                        // Size of transfer subimage
  int rows_transfered;                       // number of rows actually transferred
  unsigned long fp_idx;                      // index into fitPix buffer
  char* image_ptr;                           // Pointer to current position in readout image buffer
  unsigned long npixels_read;                // Number of pixels read
  double sum,min,max;                        // image segment statisitics for debugging
  int processed_buffer;                      // buffer number currently consuming data
  int total_buffers;                         // total number of buffers to process in full observation
  int transfer_buffer;                       // buffer number currently producing data from controller
  int nref_pixels;                           // binned reference pixels
  float* pix;                                // pointer to data to transfer
  float* var_pix;                            // pointer to the variance data to transfer
  unsigned char* qual_pix;                   // Pointer to the quality data to transfer
  bool last_read, last_coadd, last_fs;       // flags indicating current status of readouts
  int rd=0;                                  // reads done tmp var
  int frame_skip;                            // output frames to skip to find correct frame
  unsigned long cpix;                        // index of last pixel compared to verify image
  int save_verify;                           // flag set to verify state at beginning and then restored
  int transfer_packets = 0;                  // Count of number of image packets transferred
  double rawt;                               // current time UT


  // Initalise progress parameters
  inst_statusp->put("progress", 0L);
  // TODO: Calculate IR_PROGRESS in a meaningful way
//  ir_statusp->put(IR_PROGRESS, (long)rgn[cid]->get_progress_counter()*ndr*ir_einfo->nfowler);
  ir_statusp->put(IR_PROGRESS, 0L);
  camera_statusp->put(READOUT_WAIT_TIME, 0.0);
  camera_statusp->put(TRANSFER_TIME, 0.0);

  // Set targets
  //  TODO: Calculate "target" in a meaningfull way
//  inst_statusp->put("target", (long)rgn[cid]->get_progress_counter()*ir_einfo->nfowler);
//  ir_statusp->put(IR_TARGET, (long)rgn[cid]->get_progress_counter()*(ir_einfo->nperiods+1)*ir_einfo->nfowler);
  inst_statusp->put("target", 1L);

  // Image header sent in prepare_reaout method - set header type to sub
  sub_header.id = seq_id;

  // Readout occurs using following algorithm
  // 1) start reading out
  // 2) Poll the Byte Remaining Register for indication of progress of readout

  // Number of output reference pixels found on each row of output from all amps
  // Protect against div by zero
  if (!rgn[cid]->rcf) {
    throw Error("IR_Camera: Row compression factor (rgn[cid]->rcf) must be greater than zero!",
		E_ERROR, -1, __FILE__, __LINE__);
  }
  nref_pixels = (int)ceil(((float)rgn[cid]->overscan_cols)/rgn[cid]->rcf) * rgn[cid]->nactive_amps;

  segno = 0;
  rows_transfered = 0;
  ttime = vtime = 0L;

  // Initialise frame output positions
  for (i=0; i<main_header.nsframes; i++)
    prog_row[i] = 0;

  // Initialise fitPix and other calc buffers
  // TODO: Use to copy the common buffer?
  //       That's quite wasteful...
  // initialise_readout_buffers();

  // Initialise the byte counter so that we can set the image ptr to start of
  // ring buffer
  if (ndr == 0) {
    byte_counter = 0;
  }

  // Is this the last read of a set?
  /* TODO: At the moment we only have one read. Arguably, this won't ever change
   *       See if there's a case to get rid of last_read
   */
  // last_read = (ndr == ir_einfo->nperiods);
  last_read = true;

  // Is this the last coadd of a set?
  // TODO: At the moment we read all the coadds in one go. Later this will be
  //       meaningful, but not a the time
  // last_coadd = (ir_einfo->ncoadds == (ir_status->ncoaddsDone+1));
  last_coadd = true;

  // Do we want to transfer the resultant image from this readout?
  // TODO: Hardcoded so far..
  xfer_image = true;

  // Get mutex to update read number
  controller->image_transfer_thr->lock();

  //    cout << "Readout: - setting read number " << ndr << " waiting for image transfer flag" << endl;

  // Set the readout number
  controller->readout_no = ndr;

  // Now wait for read thread to be ready - this is a polling loop because not all
  // controllers will cv_signal when this condition changes!
  if (!hardware_abort)
    rd = controller->get_read_running();

  // Unblock waiting read thread
  controller->image_transfer_thr->signal();

  // unlock mutex so that read thread can go
  controller->image_transfer_thr->unlock();

  message_write("IR readout - waiting for NDR %d to start.", ndr+1);

  timeout_wait = 0;
  while ((rd < ndr+1) && !hardware_abort && !controller->image_readout_thread_abort &&
	 (timeout_wait < IMAGE_WAIT_TIMEOUT)) {

    timeout_wait += rest(IMAGE_MIN_WAIT);

    // Get mutex to update read number
    controller->image_transfer_thr->lock();

    rd = controller->get_read_running();

    // unlock mutex so that read thread can go
    controller->image_transfer_thr->unlock();
  }

  if (hardware_abort)
    return;

  // If the read thread is still not delivering data after the timeout period
  // throw a timeout exception
  if ((rd < ndr+1) && (timeout_wait >= IMAGE_WAIT_TIMEOUT)) {
    ostr << "Image readout thread has failed to deliver data within timeout period of " << IMAGE_WAIT_TIMEOUT << " msec!" << ends;
    throw Error(ostr, E_ERROR, ETIME, __FILE__, __LINE__);
  } else {
    // Set timer thread going if this is first NDR
    if ((ir_timer_thr != nullptr) && (ndr == 0)) {
      ir_timer_thr->lock();
      // Timestamp at start of readout - inaccurate (error ~= IMAGE_MIN_WAIT/2)
      // but just used as indicator. Also get UTstart here - best we can do!
      if (ir_status->irMode == IR_OBSERVE_MODE) {
	ir_statusp->put(ACQ, true);
	camera_statusp->put(DATEOBS, to_ut_date(controller->get_start_exposure()).c_str(), 0, FITSVALUELEN);
	camera_statusp->put(UTSTART, to_ut_time(controller->get_start_exposure()).c_str(), 0, FITSVALUELEN);
	camera_statusp->put(TAI, camera_status->tai);
      }
      exp_timestamp = epicsTime::getCurrent();
//      ir_timer_thr->signal();
//      ir_timer_thr->unlock();
//    } else if (last_read) {
      last_timestamp = epicsTime::getCurrent();
    }
  }

  // We are now reading out - time the readout and set activity status
  btime = epicsTime::getCurrent();
  ir_statusp->put(IR_ACTIVITY, (int)IR_CAMERA_ACT_READOUT);
  if (ir_status->irMode == IR_OBSERVE_MODE)
    ir_statusp->put(RDOUT, true);

  // Start the image pointer - assume byte counter was initialised
  /*
  image_ptr = controller->image_buffer_ptr + byte_counter;
  processed_buffer = ndr * ir_einfo->nfowler;
  total_buffers = (ir_einfo->nperiods+1) * ir_einfo->nfowler;
  */
  save_verify = main_header.verify;

  // TODO: Maybe we should update this somehow
  // camera_statusp->put(READOUT_WAIT_TIME, wtime/(double)timestamp_res());
  // TODO: Update for STOP/ABORT
  try {

	  // Loop over each Fowler sample
	  ir_statusp->put(NFS_DONE, 0L);

	  // TODO: Write the header to disk -- Should have been done by now, actually

	  if (xfer_image) {
		  /*
		  // Prepare header message - when shared memory is empty
		  // First wait until we have access to the data transfer task
		  if (debug&DEBUG_STATE)
			  message_write("Slave%d: Data_xfer_sem wait %s:%d, abort=%d",cid,__FILE__,__LINE__,hardware_abort);
		  // TODO: Check this better, because it should work...
		  // data_xfer_sem->wait();

		  // TODO: Replace this with writing the info

		  // Now wait until the image area is empty
		  //if (debug&DEBUG_STATE)
		  //message_write("Slave%d: Image_empty_sem wait %s:%d, abort=%d",cid,__FILE__,__LINE__,hardware_abort);

		  image_empty_sem->wait();

		  // Set the header-ok flag to false at the beginning of the header transfer
		  // The data task will set it true if the transfer succeeds
		  image->header_ok = false;
		  image->body.type = IMAGE_HEADER;
		  if (last_read && last_coadd) {
		  image->body.Detector_Image_Body_u.main_header = main_header;
		  } else {
		  image->body.Detector_Image_Body_u.main_header = raw_header;
		  }
		  transfer_packets = 0;

		  // Indicate that main image header is ready for transfer
		  image_ready_sem->signal();

		  // Give back the data xfer semaphore
		  data_xfer_sem->signal();
		  */
		  DC_Data::dump_to(DC_Data::get_full_uuid_path_with_extension("raw"), controller);
	  }


	  // Loop over each row region in the readout spec

	  // Make sure producer has not caught up to consumer - otherwise we will be overwriting
	  // data before it can be processed. First get latest transfer count. Also get current read counter
	  // Use as an example if we want to do progressive saves
	  /*
	     controller->image_transfer_thr->lock();
	     controller->get_readout_status(all_amp_width*bytepix);
	     transfer_buffer = controller->transfer_buffer;
	     rows_transfered = controller->rows_transfered;
	     controller->image_transfer_thr->unlock();
	     */


	  // Call the general unravelling routine (part of the generic camera)
	  // if ready to transfer
	  if (last_fs && xfer_image && !hardware_abort) {	

		  /* TODO: Update the transfer time?
		     stime = epicsTime::getCurrent();
		     transfer_image_data(all_amp_width - nref_pixels, readrows, nser, rr, image_ptr, RAW_DATA,
		     controller->image_info.bitpix, 0, raw_header);

		     ttime +=  epicsTime::getCurrent() - stime;
		     camera_statusp->put(TRANSFER_TIME, ttime/(double)timestamp_res());
		     */
	  }

	  // Update progress parameters
	  /* TODO: Use this during readout to update progress...
	     inst_statusp->put(PROGRESS, ctl_status->progress + readrows);
	     ir_statusp->put(IR_PROGRESS, ir_status->irProgress + readrows);
	     */


	  ir_statusp->put(NFS_DONE, 1L);

	  processed_buffer++;

  }
  catch (Error &e) {
    // make sure raw file is closed
    e.rethrow();
  }

  // The readout thread may be blocked waiting for readout to complete (in case of
  // PCI or VME bus) free it here because we have finished abnormally
  /*
  if (hardware_abort || hardware_stop) {
    controller->image_transfer_thr->lock();
    controller->image_transfer_thr->signal();
    controller->image_transfer_thr->unlock();
  }
  */

  // Get mutex to check readout done
  controller->image_transfer_thr->lock();

  //    cout << "Readout: - Waiting for readouts_done " << ndr+1 << endl;

  // Now wait for read thread to be ready - try a few times
  timeout_wait = 0;
  while (!hardware_abort && !controller->image_readout_thread_abort && (controller->get_readouts_done() <= ndr)) {
    try {
      controller->image_transfer_thr->timed_wait(IMAGE_MIN_WAIT);
    }
    catch (Timeout_Error& te) {
      timeout_wait += IMAGE_MIN_WAIT;
      if (timeout_wait >= IMAGE_WAIT_TIMEOUT) {
	ostr << "Image readout thread has failed to finish readout within timeout period of " << IMAGE_WAIT_TIMEOUT << " msec!" << ends;
	throw Timeout_Error(ostr,__FILE__, __LINE__);
      }
    }
  }

  // unlock mutex so that read thread can go
  controller->image_transfer_thr->unlock();

  // Check that thread has not aborted
  if (controller->image_readout_thread_abort) {
    message_write(controller->image_readout_thread_msg,E_ERROR);
  }

  // Set flags
  if (last_read && last_coadd)
    ir_statusp->put(IR_ACTIVITY, (int)IR_CAMERA_ACT_NULL);
  else
    ir_statusp->put(IR_ACTIVITY, (int)IR_CAMERA_ACT_EXPOSING);

  if (ir_status->irMode == IR_OBSERVE_MODE)
    ir_statusp->put(RDOUT, false);

  // Again check abort flag
  if (hardware_abort) {
    if (debug&DEBUG_STATE) {
      message_write("Aborting after readout loops done, %s,%s", __FILE__, __LINE__);
    }
    return;
  }

  // Wrap up with timings etc
  camera_statusp->put(TOTAL_OPERATION_TIME, (epicsTime::getCurrent() - btime)/(double)timestamp_res());

  if (debug&DEBUG_INDICATORS) {
    if (xfer_image)
      message_write("Image readout done, transfer time=%.1f s. #packets=%d", ttime/(double)timestamp_res(), transfer_packets);

    // Add any readout thread message here
    message_write(controller->image_readout_thread_msg);
  }

}
/*
 *+
 * FUNCTION NAME:  IR_Camera::process_ir_data
 *
 * INVOCATION: process_ir_data(char* iptr, int npix, int fs, int fi)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > iptr - pointer to input pixels
 * > npix - number of pixels to process
 * > fs - fowler sample number
 * > fi - starting index into fitPix array
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Does readout processing
 *
 * DESCRIPTION:
 * Sums each sample read into pixSum buffer (using associated template routine
 * to cater for data types). It then moves values to fitPix buffer if on last
 * fowler sample and (for linear fitting) does a linear regression.
 *
 * For a LINEAR_FITTING readout, cosmic rays can optionally be tracked and rejected.
 * Algorithm to use is taken from
 * "Cosmic-Ray Rejection and Readout Efficiency for Large-Area Arrays", Fixen D.J,,
 * Offenberg, J.D., Hanisch, R.J., Mather, J.C., Nieto-Santisteban, M.A., Sengupta, R. &
 * Stockman, H.S. 2000, Astronomical Society of the Pacific.
 *
 * if (Di > (Pn-P0)/n + noise threshold) then reject pixel. Where Di is
 * difference between sample Pi and sample Pi-1. ie if the latest increase in
 * flux is greater than the average flux rate plus noise (given constant
 * sampling frequency) then a cosmic ray event is deemed to have happened and
 * sampling stopped.
 *
 * This algorithm caters for the possibility of two cosmic ray per
 * exposure. Once the first is detected the progressive results are saved and
 * then a second set of values computed (for the 2nd ramp). At the end of the
 * exposure (or at a 2nd cosmic ray event) the final data are calculated as a
 * weighted mean of the two ramps.
 *
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES: None
 *
 *- */
void IR_Camera::process_ir_data(char* iptr, int np, int fs, int fi)
{
  short *sip = (short*) iptr;                   // pointer to input pixels - shorts
  unsigned short *uip = (unsigned short*) iptr; // pointer to input pixels - unsigned shorts
  int *iip = (int*) iptr;                       // pointer to input pixels - ints
  unsigned int *uiip = (unsigned int*) iptr;    // pointer to input pixels - unsigned ints
  float *fip = (float*) iptr;                   // pointer to input pixels - floats
  double *dip = (double*) iptr;                 // pointer to input pixels - doubles

  float *pix;                                   // pointer into float pix sum array
  float *fptr;                                  // pointer into fitPix array
  float *cptr;                                  // pointer into coaddPix array
  float *fsptr;                                 // pointer into fitPixSave array
  unsigned long *sptr;                          // pointer into pixSum array
  unsigned short *zptr;                         // pointer to zeroth array

  float saturated = 0.0;                        // Value of a saturated pixel
  float n,m,ns,diff,dy;                         // working variables
  double k,k0,k1,k2,sy,y,yavg,ys;               // constants or slope calcs
  double mdivmpn,ndivmpn;                       // work variables
  int pn = controller->readout_no+1;            // Set to indicate which set of nfs reads we are on
  int i;
  int adu_increase = controller->controller.chip[0].adu_increase; // All chips must be same on focal plane
  bool skipPix;                                 // for linear fitting set if pix has cosmic ray detected

  static int rejects;
  static int early_rejects;
  static int seconds;

  /* TODO: Deal with the different modes
  if (ir_einfo->readModeSet == IR_SINGLE_READOUT) {
    pix = &fitPix[fi];
  } else {
    pix = &fpixSum[fi];
  }

  fptr = &fitPix[fi];
  fsptr = &fitPixSave[fi];
  zptr = &zeroPixSave[fi];
  sptr = &pixSum[fi];
  cptr = &coaddPix[fi];

  // Accumulate in pixSum - take care of each pixel input type
  switch (controller->image_info.bitpix) {
  case BYTE_BITPIX:
    if (ir_einfo->nfowler > 1)
      sum_ir_data(sptr,iptr,np);
    else
      copy_ir_data(pix,iptr,np);
    saturated = MAXUC;
    break;
  case SHORT_BITPIX:
    if (controller->image_info.unsigned_data) {
      if (ir_einfo->nfowler > 1)
	sum_ir_data(sptr,uip,np);
      else
	copy_ir_data(pix,uip,np);
      saturated = MAXU;
    } else {
      if (ir_einfo->nfowler > 1)
	sum_ir_data(sptr,sip,np);
      else
	copy_ir_data(pix,sip,np);
      saturated = MAXS;
    }
    break;
  case INT_BITPIX:
    if (controller->image_info.unsigned_data) {
      if (ir_einfo->nfowler > 1)
	sum_ir_data(sptr,uiip,np);
      else
	copy_ir_data(pix,uiip,np);
      saturated = MAXUI;	
    } else {
      if (ir_einfo->nfowler > 1)
	sum_ir_data(sptr,iip,np);
      else
	copy_ir_data(pix,iip,np);
      saturated = MAXI;
    }
    break;
  case FLOAT_BITPIX:
    if (ir_einfo->nfowler > 1)
      sum_ir_data(pix,fip,np);
    else
      copy_ir_data(pix,fip,np);
    saturated = MAXF;
    break;
  case DOUBLE_BITPIX:
    if (ir_einfo->nfowler > 1)
      sum_ir_data(pix,dip,np);
    else
      copy_ir_data(pix,dip,np);
    saturated = MAXD;
    break;
  }

  // If last sample do computations and place in fitPix
  if (fs < ir_einfo->nfowler-1)
    return;

  // for linear fitting do linear regression
  switch (ir_einfo->readModeSet) {
  case IR_LINEAR_READOUT:
    // integer image values - summed in pixSum array - move to fitpix array
    if ((ir_einfo->nfowler > 1) && (controller->image_info.bitpix > 0)) {
      copy_ir_data(pix,sptr,np);
    }

    // For the first NDR - just record the frame - this is our zero point
    if (pn==1) {

      copy_ir_data(zptr,pix,np);
      rejects = early_rejects = seconds = 0;

    } else {
      // Pointer into the fitPix array holding pixel data, then to store calculated slopes
      pix = fpixSum;
      fptr = fitPix;
      fsptr = fitPixSave;
      zptr = zeroPixSave;

      for (i=fi; i<fi + np; i++) {

	// Ignore pixels already marked as bad
	if (badPix[i])
	  continue;

	// Is this pixel saturated - if so move to bad pixel array
	if (pix[i]>=saturated)
	  badPix[i] = pn;
	else {
	  // calculate number of samples in sy - we want average delta y to last
	  // sample remember the first sample is at T0 and is zero (or just noise -
	  // or perhaps a cosmic ray?). Also subtract the number of samples taken
	  // since a possible first cosmic ray detected
	  n = pn-(int)cosmicPix[i]-1;
	  ns = (n-1)/2*n; // ns is the actual number of summed pixel accumulations
	  	
	  skipPix = false;

	  // Subtract zero point - it may have been a cosmic ray hit?
	  y = (adu_increase)? pix[i] - zptr[i]: zptr[i] - pix[i];
	
	  if (ir_dinfo->cosmRej && (n>1)) {
	    // Otherwise - handle cosmic rays?
	    // Do this by checking that the new photon rate hasn't changed
	    // significantly more than the previous average rate
	    yavg = fit_sy[i]/ns;
	
	    // calculate new delta y, remember that each new data point is cumulated charge
	    dy = y - yavg*(n-1);
	
	    // difference
	    diff = fabs(dy-yavg);
	
	    // Use min of two to check threshold
	    ys = (dy<yavg)? dy:yavg;
	
	    // Threshold is given as factor of yavg - we want to check slope
	    // change is less than the threshold and it is as least as much as a
	    // minimum
	    if ((diff>ys+variance[i]*ir_dinfo->cosmThrsh) && (diff>ir_dinfo->cosmMin)) {
	      // In case of 3rd NDR has reduced signifying that a cosmic ray may
	      // have been captured during the 2nd NDR
	      if ((n==2) && (dy<yavg)) {
		if (cosmicPix[i] == 0) {
		  // Just ignore first NDR and make 2nd NDR the zero point
		  zptr[i] = (unsigned short) pix[i];
		  // Re initialise the cumulative sums
		  fit_sy[i] = fit_sy2[i] = fit_siy[i] = 0.0;
		  cosmicPix[i] = (unsigned char) n;
		  seconds++;
		} else {
		  badPix[i] = pn-1;
		  early_rejects++;
		}
	      } else {
		if (cosmicPix[i] == 0) {
		  // Save the first ramp calculations
		  zptr[i] = (unsigned short) pix[i];
		  variance1[i] = variance[i];
		  fsptr[i] = fptr[i];
		  fit_sy[i] = fit_sy2[i] = fit_siy[i] = variance[i] = 0.0;
		  cosmicPix[i] = (unsigned char) n;
		  seconds++;
		} else {
		  badPix[i] = pn;
		  rejects++;
		}
	      }
	      // Continue to next loop iteration - found cosmic ray
	      skipPix = true;
	    }
	  }
	
	  if (!skipPix) {
	    // Pixel ok - do cumulative sums and calculate slope & variance
	    // Calculate average pixel value
	    // Now time calculations using simplified formulae
	    k0 = (n + 1)/2.0;
	    k1 = (n / 12.0) * (n * n - 1);
	
	    // photon rate is in units ADU/s - set this here (period is in seconds)
	    k2 = ir_einfo->period * k1;
	
	    // Make sure values are sensible - ie ensure no div by zero
	    if (n>1 && (k1==0.0 || k2==0.0)) {
	      throw Error("IR_Camera: Linear fitting constants (k1&k2) must be greater than zero!",
			  E_ERROR, -1, __FILE__, __LINE__);
	    }
	
	    fit_sy[i] += y;
	    fit_sy2[i] += y*y;
	    fit_siy[i] += n * y;
	
	    if (n>1) {
	      k = (adu_increase)? fit_siy[i] - k0 * fit_sy[i] : k0 * fit_sy[i] - fit_siy[i];
	      fptr[i] = k / k2;
	      sy = fit_sy[i];
	      variance[i] = fit_sy2[i] - (sy*sy / n) - (k*k / k1);
	    }
	  }

	  if ((controller->readout_no == ir_einfo->nperiods) && (cosmicPix[i]>0)) {
	    // We have had two cosmic ray events at this pixel!
	    // Compute the weighted mean of the two fits and also
	    // the combined variance (v(ax+by) = a^2v(x) + b^2v(y) Walpole 1974 pp70)
	    m = cosmicPix[i];
	    mdivmpn = m/(m+n);
	    ndivmpn = n/(m+n);
	    fptr[i] = mdivmpn*fsptr[i] + ndivmpn*fptr[i];
	    variance[i] = mdivmpn*mdivmpn*variance1[i] + ndivmpn*ndivmpn*variance[i];
	  }

	}
      } // End pass over data to trap saturated, cosmic rays and do sums and fits

    } // if pn==1 else pn>1
    break;
  case IR_DCS_READOUT:
  case IR_FOWLER_READOUT:
    // if last read for DCS or fowler sampling take difference between first and last
    // sets of images.
    if (ir_einfo->nfowler == 0) {
      throw Error("IR_Camera: Number of Fowler samples (ir_einfo->nfowler) must be greater than zero!",
		  E_ERROR, -1, __FILE__, __LINE__);
    }
    if ((controller->readout_no == ir_einfo->nperiods) || hardware_stop) {

      // integer image values - summed in pixSum array - move to fitpix array
      if ((ir_einfo->nfowler > 1) && (controller->image_info.bitpix > 0)) {
	copy_ir_data(pix,sptr,np);
      }

      // Depending on whether ADUs increase or decrease over time
      // Subtract first set of fowler samples from this set and divide by number of samples
      if (adu_increase)
	for (i=0; i<np; i++) {
	  fptr[i] = (pix[i] - fsptr[i])/ir_einfo->nfowler;
	}
      else
	for (i=0; i<np; i++) {
	  fptr[i] = (fsptr[i] - pix[i])/ir_einfo->nfowler;
	}	
    } else {
      // Move data into fitPixSave buffer for later differencing
      if ((ir_einfo->nfowler > 1) && (controller->image_info.bitpix > 0)) {
	// integer image values - summed in pixSum array - move to fitpixSave array
	copy_ir_data(fsptr,sptr,np);
      } else {
	// Simply copy fitPix array to fitPixSave array
	copy_ir_data(fsptr,pix,np);
      }

      // Is this intermediate data required now? If so then calculate average values and move to fitPix
      if (ir_dinfo->saveNdrs) {
	if (ir_einfo->nfowler > 1) {
	  for (i=0; i<np; i++) {
	    fptr[i] = fsptr[i]/ir_einfo->nfowler;
	  }
	}
      }
    }
    break;
default:
    break;
  }

  // on last period and doing a series of coadds? - sum in coaddPix array - move to fitpix array if last
  if ((controller->readout_no == ir_einfo->nperiods) && (ir_einfo->ncoadds > 1)) {
    sum_ir_data(cptr,fptr,np);
    copy_ir_data(fptr,cptr,np);
    for (i=0; i<np; i++) {
      fptr[i] = fptr[i]/(ir_status->ncoaddsDone+1);
    }
  }
  if ((ir_einfo->readModeSet==IR_LINEAR_READOUT) && (controller->readout_no == ir_einfo->nperiods) && (debug&DEBUG_CALCS)) {
    message_write("Cosmic rejections=%d, early rejections=%d, seconds=%d", rejects, early_rejects, seconds);
  }
  */
}


/*
 *+
 * FUNCTION NAME:  cleanup_readout()
 *
 * INVOCATION: cleanup_readout(bool finish)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > finish - set if this is the last readout of a set
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Cleans up after readout finished
 *
 * DESCRIPTION:
 * . joins the readout thread
 * . handles abort condition
 * . closes a clean transfer
 * . cleans up regions workspace
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES: None
 *
 *-
 */
void IR_Camera::cleanup_readout(bool finish)
{
	// TODO: STUDY AND UNDERSTAND

  // Now join the readout thread - this is a derived method
  if (requested_image_size>0 && (finish || hardware_abort)) {
    // Unblock waiting read thread after telling it to stop its readouts
    if (hardware_abort)
      controller->image_transfer_thr->signal();
    controller->finish_readout();
  }

  // If not aborting and actually were transferring data - send a close header
  if (xfer_image && !hardware_abort) {
    // Block waiting for header to be sent before setting close flag
    // Set wstate to waiting here so that heartbeat not checked
    if (debug&DEBUG_STATE)
      message_write("Slave%d: Image_empty_sem wait %s:%d, abort=%d",cid,__FILE__,__LINE__,hardware_abort);
    image_empty_sem->wait();
    image->body.type = IMAGE_CLOSE;

    // Indicate that close image header is ready for transfer
    image_ready_sem->signal();

  }

  if (finish || hardware_abort) {
    // Must cleanup the image transfer thread before deleting
    // the regions structures.
    controller->cleanup_exposure();
    // Remove the region structure
    if (rgn[cid]!=nullptr) {
      delete rgn[cid];
      rgn[cid] = nullptr;
    }

    // Wrap up request
    ir_statusp->put(IR_ACTIVITY, (int)IR_CAMERA_ACT_NULL);
    request_wrapup();
  }
}
/*
 *+
 * FUNCTION NAME: IR_Camera::update_ir_status_info
 *
 * INVOCATION: dummy->update_ir_status_info(IR_Mode ir_mode)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ir_mode - either observe or view mode
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Update either observe or view mode status parameters with ir_einfo
 *
 * DESCRIPTION:
 * Sets up parameter name depending on read mode.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void IR_Camera::update_ir_status_info(IR_Mode ir_mode)
{
  ModePrefix mode;
  string par;
  if (ir_mode == IR_VIEW_MODE)
    mode.pref = "view";
  else
    mode.pref = "obs";

  // Now for each parameter, encode name and update value
  ir_statusp->put(mode.dbName(DO_IR_READOUT), ir_einfo->doReadout);
  ir_statusp->put(mode.dbName(DO_RESET), ir_einfo->doReset);
//  ir_statusp->put(mode.dbName(SET_INTERVAL), ir_einfo->setIntvl);
  ir_statusp->put(mode.dbName(IR_READ_MODE), (int)ir_einfo->readModeSet);
  ir_statusp->put(mode.dbName(IR_QUAD_MODE), (int)ir_einfo->quadrantsSet);
//  ir_statusp->put(mode.dbName(TIME_MODE), (int)ir_einfo->timeModeSet);
//  ir_statusp->put(mode.dbName(NFOWLER), ir_einfo->nfowler);
//  ir_statusp->put(mode.dbName(READ_TIME), ir_einfo->readTime);
//  ir_statusp->put(mode.dbName(READ_INTVAL), ir_einfo->readIntvl);
//  ir_statusp->put(mode.dbName(PERIOD), ir_einfo->period);
//  ir_statusp->put(mode.dbName(NPERIODS), ir_einfo->nperiods);
//  ir_statusp->put(NREADS, ir_einfo->nperiods+1);
  ir_statusp->put(mode.dbName(IR_EXPOSEDRQ), ir_einfo->exposedRQ);
  ir_statusp->put(mode.dbName(IR_EXPOSED), ir_einfo->exposed);
//  ir_statusp->put(mode.dbName(NCOADDS), ir_einfo->ncoadds);
//  ir_statusp->put(mode.dbName(NRESETS), ir_einfo->nresets);
//  ir_statusp->put(mode.dbName(RESET_DELAY), ir_einfo->resetDelay);

  // Add the data info parameters
  mode.prepend("d_");
  ir_statusp->put(mode.dbName(DO_COSM_REJ), (bool) ir_dinfo->cosmRej);
  ir_statusp->put(mode.dbName(COSM_THRESH), ir_dinfo->cosmThrsh);
  ir_statusp->put(mode.dbName(COSM_MIN), ir_dinfo->cosmMin);

  if (ir_mode == IR_VIEW_MODE) {
    ir_statusp->put(mode.dbName(DO_CMP_IM), (bool) ir_dinfo->doCmpIm);
    ir_statusp->put(mode.dbName(DO_SUBTRACT), (bool) ir_dinfo->doSubFile);
    ir_statusp->put(mode.dbName(SUB_FNAME), ir_dinfo->subFile, 0, FILENAMELEN);
    ir_statusp->put(mode.dbName(WAVE_MIN), ir_dinfo->waveMin);
    ir_statusp->put(mode.dbName(WAVE_MAX), ir_dinfo->waveMax);
  } else {
    ir_statusp->put(mode.dbName(DO_SAVE), (bool) ir_dinfo->save);
    ir_statusp->put(mode.dbName(DO_SAVE_NDR), (bool) ir_dinfo->saveNdrs);
    ir_statusp->put(mode.dbName(DO_SAVE_COADD), (bool) ir_dinfo->saveCoadds);
  }
}
/*
 *+
 * FUNCTION NAME: IR_Camera::cleanup_exposure
 *
 * INVOCATION: dummy->cleanup_exposure()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Cleans up after an IR exposure
 *
 * DESCRIPTION:
 *
 * This method cleans up after and exposure It calls the controller specific
 * cleanup method and then removes the ir_timer_thread
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void IR_Camera::cleanup_exposure()
{
  // Now do cleanup things
  controller->cleanup_exposure();

  if (debug&DEBUG_INDICATORS)
    message_write("Exposure cleaned up");

  // Wait to join image timer thread - thread timeout prevents permanent block
  // TODO: Work on this to actually join the thread...
  if (ir_timer_thr != nullptr) {
    ir_timer_thr->signal();
    delete ir_timer_thr;
    ir_timer_thr = nullptr;
    if (debug&DEBUG_INDICATORS)
      message_write("Timer thread finished.");
  }
}


static std::string status_to_string(Parameter *statusp, const std::string& pname, DType dtype) {
	std::string tmp;

	switch (dtype) {
		case PARAM_STRING:
			statusp->get(pname, tmp);
			break;
		case PARAM_LONG:
			{
				long ltmp = 0;
				statusp->get(pname, ltmp);
				tmp = std::to_string(ltmp);
			}
			break;
		case PARAM_DOUBLE:
			{
				double dtmp;
				statusp->get(pname, dtmp);
				tmp = std::to_string(dtmp);
			}
			break;
		default:
			throw std::runtime_error(std::string("status_to_string, unhandled type for param ") + pname);
			break;
	}
	return tmp;
}

/*
 *+
 * FUNCTION NAME: IR_Camera::exposure
 *
 * INVOCATION: dummy->exposure(IR_Expose expose)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > expose - exposure info
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Perform an IR exposure
 *
 * DESCRIPTION:
 * This method handles each exposure mode
 * . initialises exposure parameters
 * . checks parameter inter relationships for sanity
 * . loops over the number of coadds
 * . executes each coadd:
 *   1. Prepare the readout
 *   2. Start the exposure on the controller
 *   3. Perform an initial set of nfs non destructive reads, initiated
 *      by the controller specific start_exposure command
 *   4. Sum and average each of the frames from above - note that this has to keep
 *      up with the data output from the controller due to limited size of the image_buffer
 *   5a For Fowler sampling perform one for set of nfs reads after a read_interval
 *      period. Subtract resulatant image from 1 from this and save result.
 *   5b For Linear Fitting perform nPeriods sets of additional nfs reads and
 *      compute the photon rate for each pixel on the averaged image. In this case it might
 *      be required to reject cosmic rays based on a rate change threshold.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void IR_Camera::exposure(IR_Expose expose)
{
  epicsTime stime, etime;
  double elapsed_time;
  double te;
//  double reset_wait;
  double zero_val[MAX_READOUT_BUFFERS] = {0.0};
  bool stopped;
  ostringstream ostr;
  double rawt, tai;                            // Current UT and TAI


  // Make sure that we're going to be able to write the headers
  product_configuration = new ProductConf();
  product_configuration->add_unit("PDU");

  ir_einfo = &expose.einfo;
  ir_dinfo = &expose.dinfo;
//  ir_einfo->nresets = 0;
//  ir_einfo->resetDelay = expose.reset.reset_delay;

  controller->hr_exptime = {0, 0};
  clear_stop();

  image_empty_sem->give();

  // Get the observing mode
  {
	  int tmp_mode;

	  ir_statusp->get(IR_MODE, tmp_mode);
	  ir_status->irMode = static_cast<IR_Mode>(tmp_mode);

	  // TODO: Hardcoded "obs" so far
	  ir_statusp->get(std::string("obs_") + IR_READ_MODE, tmp_mode);
	  ir_einfo->readModeSet = static_cast<IR_Readmode_Type>(tmp_mode);

	  ir_statusp->get(std::string("obs_") + IR_QUAD_MODE, tmp_mode);
	  ir_einfo->quadrantsSet = static_cast<IR_Quadrants_Type>(tmp_mode);
  }

  // Set initial values for standard status parameters
  if (ir_status->irMode == IR_OBSERVE_MODE)
    ir_statusp->put(PREP, true);
  inst_statusp->put(PROGRESS, 0L);
  ir_statusp->put(IR_PROGRESS, 0L);
  ir_statusp->put(EXPOSURE_ELAPSED, 0.0);
  camera_statusp->put(EXPOSURE_DONE, false);
  camera_statusp->put(VERIFIED, false);
  camera_statusp->put(ELAPSED, 0.0);
  camera_statusp->put(SHUTTER_DURATION, 0.0);
  camera_statusp->put(EXPOSED, 0.0);
  ir_statusp->put(IR_ACTIVITY, (int) IR_CAMERA_ACT_EXPOSING);
  if (ir_status->irMode == IR_VIEW_MODE)
    ir_statusp->put(IR_REQ_TYPE, (int) IR_CAMERA_VIEW);
  else
    ir_statusp->put(IR_REQ_TYPE, (int) IR_CAMERA_EXPOSE);


  // NOTE: exposedRQ is now set directly, not calculated
  // We need to setup exposure timing parameters here.
  // calc_exposure_time(ir_einfo);

  // camera_statusp->put(EXPOSEDRQ, ir_einfo->exposedRQ);
  // camera_statusp->get(EXPOSEDRQ, ir_einfo->exposedRQ);

  // Set initial actual exposure time to zero so that this is put in first update.
  ir_einfo->exposed = 0.0;

  message_write("exposedRQ = %f", ir_einfo->exposedRQ);

  // Repeat the exposure sequence ncoadds times
  ir_statusp->put(NCOADDS_DONE, 0L);
  verify_fail = 0;

  // Now setup the status info for selected mode
  update_ir_status_info(ir_status->irMode);

//  for (c=0; c<ir_einfo->ncoadds && !hardware_abort && !hardware_stop; c++) {
  if (!hardware_abort && !hardware_stop) {

    try {

      // Set exptime to be exposedRQ to start
      exptime = ir_einfo->exposed = ir_einfo->exposedRQ;
      elapsed_time = 0.0;

      // Record a timestamp here before starting a reset
      etime = epicsTime::getCurrent();

      // Do a reset if required
      // TODO: Do we want resets before every exposure? Discuss with Andy & John.
      //       Luc didn't want the controller being reboot every time
      //
      //       2020-10-07: After talking to Andy & John, we agreed that resetting the
      //                   controller regularly is not a good idea. It will be left
      //                   maybe for an explicit command.
      /*
      if (expose.reset.nresets > 0) {
	message_write("IR Camera resetting the detector...");
	controller->reset_detector(expose.reset, true);
      }
      */
      ir_statusp->put(TIME_LEFT, exptime);
//      ir_statusp->put(PER_TIME_LEFT, ir_einfo->period);
      camera_statusp->put(READOUT_TIME, zero_val, 0, MAX_READOUT_BUFFERS);

      // Lets start the exposure cycle
      // Algorithm
      // 1. Prepare the readout
      // 2. Start the exposure on the controller
      // 3. Perform an initial set of nFowlerSamples non destructive reads, initiated
      //    by the controller specific start_exposure command
      // 4. Sum and average each of the frames from above - note that this has to keep
      //    up with the data output from the controller due to limited size of the image_buffer
      // 5a For Fowler sampling perform one for set of nFowlerSamples reads after a read_interval
      //    period. Subtract resulatant image from 1 from this and save result.
      // 5b For Linear Fitting perform nPeriods sets of additional nFowlerSamples reads and
      //    compute the photon rate for each pixel on the averaged image. In this case it might
      //    be required to reject cosmic rays based on a rate change threshold.

      // Prepare for the readouts
	prepare_readout(controller->n_sync, expose);
	controller->start_exposure();

      // Initialise
      ir_statusp->put(IR_ACTIVITY, (int)IR_CAMERA_ACT_EXPOSING);
      ir_statusp->put(NREADS_DONE, 0L);
      ir_statusp->put(NFS_DONE, 0L);

      // Get mutex to update read number - must do this here because of the
      // following reset wait

      controller->image_transfer_thr->lock();

      // Set the readout number
      controller->readout_no = 0;

      // Unblock waiting read thread
      controller->image_transfer_thr->signal();

      // unlock mutex so that read thread can go
      controller->image_transfer_thr->unlock();

      // Now delay for reset time if any
      /*
      stime = epicsTime::getCurrent();
      te = 0;
      reset_wait = (ir_einfo->nresets)? ir_einfo->nresets*ir_einfo->readTime+ir_einfo->resetDelay:0.0;

      if (reset_wait>0) {
	message_write("Waiting %f s for detector reset to complete...", reset_wait);

	printf("IMAGE_MIN_WAIT = %d\n", IMAGE_MIN_WAIT);
	while (te < reset_wait) {
	  rest(IMAGE_MIN_WAIT);
	  printf(">>> %f\n", epicsTime::getCurrent() - stime);
	  te = epicsTime::getCurrent() - stime;
	}
      }
      */

      // Indicate we are preparing the observation
      if (ir_status->irMode == IR_OBSERVE_MODE) {
	ir_statusp->put(PREP, false);
      }

      // And now do the first readout
      readout(expose.readout, 0);

      ir_statusp->put(IR_ACTIVITY, (int)IR_CAMERA_ACT_EXPOSING);
      ir_statusp->put(NREADS_DONE, (long)(ir_status->nreadsDone+1));

      // NOTE: Fixed  at this time
      cleanup_readout(true);

      // Now loop until we have handled each of the read periods
      stopped = false;
      /*
       * TODO: This goes away. Replaced by a loop collecting info about exposure?
      for (p=0; p<ir_einfo->nperiods && !hardware_abort && !stopped; p++) {

	// Wait until the next readout is about to start
	  te = (epicsTime::getCurrent() - exp_timestamp)/(double)timestamp_res();
	  while ((te < (ir_einfo->period*(p+1))) && !hardware_abort) {
	    // If we receive a stop during a DCS or Fowler finish immediately
	    if (hardware_stop && (ir_einfo->nperiods == 1))
	      break;
	    rest(IMAGE_MIN_WAIT);
	    te = (epicsTime::getCurrent() - exp_timestamp);
	  }

	// Issue a STOP command to the controller if necessary
	if (hardware_stop) {
	  controller->do_stop_readout();
	  stopped = true;
	  // For linear fits - make this the last NDR.
	  if (ir_einfo->nperiods>1)
	    ir_einfo->nperiods = p+1;
	  controller->image_transfer_thr->signal();
	}
	
	if (!hardware_abort) {
	
	  printf("IR_Camera::exposure: issuing the next readout\n");
	  // Get the data for the next readout of the sequence
	  readout(expose.readout, p+1);
	
	  if (debug&DEBUG_INDICATORS)
	    message_write("Finished NDR %d", p+1);
	
	  ir_statusp->put(IR_ACTIVITY, (int)IR_CAMERA_ACT_EXPOSING);
	  ir_statusp->put(NREADS_DONE, ir_status->nreadsDone+1);

	    cleanup_readout(p+1==ir_einfo->nperiods);
	
	  if (debug&DEBUG_INDICATORS)
	    message_write("NDR %d cleaned up", p+1);
	}
      }
      */

      // Set acq flag off
      if (ir_status->irMode == IR_OBSERVE_MODE) {
	ir_statusp->put(ACQ, false);
	camera_statusp->put(UTEND, to_ut_time(controller->get_end_exposure()).c_str(), 0, FITSVALUELEN);
      }

      if (!hardware_abort) {
	      // TODO: So far, forcing to the VB mode

	      // All new exposures are "single readout", so this applies
	      // Ask the controller for remaining exposure time and work out what actual is
	      // Units are seconds. Controller call retuns msec
	      try {
		      double exptime = controller->get_exposure_time()/THOUSAND;
		      /*
		       * TODO: Keep an eye on this, but ignore it so far

		      // We note that there is a glitch with fetching the exposure time from the controller sometimes
		      // and that this is not understood at this time. However trying more than once sometimes helps...
		      // Insert a simple check that tests for a sensible upper limit (cannot test for lower limit
		      // because a STOP may have been executed...)
		      const int N_RETRYS = 3;
		      int attempt = 0;
		      while ((exptime > (ir_einfo->exposedRQ*1.1)) && (attempt<N_RETRYS)) {
			      message_write("Exposure time silly: %f, trying again...", exptime);
			      rest(FIFTY_MSEC);
			      exptime = controller->get_exposure_time()/THOUSAND;
			      attempt++;
		      }
		      */

		      if (exptime > (ir_einfo->exposedRQ*1.1))
			      throw Error("Value returned from controller larger than exposedRQ?", E_ERROR,  EINVAL, __FILE__, __LINE__);

		      ir_einfo->exposed = exptime;
		      if (ir_einfo->exposed < 0.0)
			      ir_einfo->exposed = 0.0;
	      } catch (Error& e) {
		      message_write("Problem fetching exposure time '%s'", e.record_error(__FILE__,__LINE__));
	      }

	      update_ir_status_info(ir_status->irMode);

	      // Record shutter times in database
	      camera_statusp->put(EXPOSED, ir_einfo->exposed);

	      // elapsed time = time from reset of first pixel to time of final NDR of first pixel.
	      // Definition decide on by Peter McGregor
	      elapsed_time = (last_timestamp - etime)/(double)timestamp_res();
	      camera_statusp->put(ELAPSED, elapsed_time);
      }


      camera_statusp->put(EXPOSURE_DONE, true);
      ir_statusp->put(IR_ACTIVITY, (int)IR_CAMERA_ACT_NULL);

      try
      {
	      ModePrefix mode;
	      /**
	       * TODO: Hardcoded for the time being...
	       **/
	      mode.pref = "obs";

	      if (product_configuration == nullptr)
		      throw std::runtime_error("Product configuration is null!");

	      HeaderConf& header = (*product_configuration)["PDU"];


	      header.add_card("CAMERA", status_to_string(dc_status, CONFIGNAME, PARAM_STRING));
	      header.add_card("DATEOBS", status_to_string(camera_statusp, DATEOBS, PARAM_STRING));
	      header.add_card("UTSTART", status_to_string(camera_statusp, UTSTART, PARAM_STRING));
	      header.add_card("UTEND", status_to_string(camera_statusp, UTEND, PARAM_STRING));
	      header.add_card("LRNS", status_to_string(ir_statusp, get_sensor_parameter_name(LRNS), PARAM_LONG));
	      header.add_card("NDAVGS", status_to_string(ir_statusp, get_sensor_parameter_name(NDAVGS), PARAM_LONG));
	      header.add_card("EXPTIME", status_to_string(ir_statusp, mode.dbName(IR_EXPOSEDRQ), PARAM_DOUBLE));
	      header.add_card("ELAPSED", status_to_string(ir_statusp, EXPOSURE_ELAPSED, PARAM_DOUBLE));

	      dc_save_product_configuration(product_configuration);
	      delete product_configuration;
      }
      catch (std::runtime_error &e) {
	      printf("IR_Camera::exposure - Exception: ********* %s ********* \n", e.what());
      }
      catch (std::out_of_range &e) {
	      printf("IR_Camera::exposure - Exception: ********* %s ********* \n", e.what());
      }
    }
    catch (Error &e) {
      e.print_error(__FILE__,__LINE__);
      cleanup_exposure();
      e.rethrow();
    }

    cleanup_exposure();

    // Update the coadd counter
    if (!hardware_abort)
      ir_statusp->put(NCOADDS_DONE, 1L);
  }

  if (main_header.verify && controller->verify_spec.do_test_pattern && !hardware_abort) {
	  camera_statusp->put(VERIFIED, !verify_fail);
	  if (verify_fail) {
		  message_write("Data verification failed");
	  } else {
		  message_write("Full data verification success!");
	  }
	  message_write(ostr.str());
  }
}

/*
 *+
 * FUNCTION NAME:  IR_Camera::calc_exposure_time()
 *
 * INVOCATION: calc_exposure_time(IR_Exposure_Info *ir_einfot)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ir_einfot - Pointer to ir exposure info structure
 *
 * FUNCTION VALUE: bool
 *
 * PURPOSE: Calculate exposure time
 *
 * DESCRIPTION: Calculates exposure time according to calculation mode chosen:
 * 1. number of periods - Uses supplied exposure time and period to compute
 *    number of periods. Adjusts periods to integral value and therefore adjusts
 *    total exposure time.
 * 2. Exposure time - Uses number of periods and period to calculate exposure time
 * 3. Period - Uses number of periods and exposure time to calculate the period.
 *
 *    NOTE: With new GNIRS modes none of these are relevant. The old
 *          "nfowler", "nperiods", "exposedRQ", and "period" are set
 *          to fixed values.
 *
 *          We're keeping the code just in case it's needed in the future.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES: None
 *
 *-
 */
void IR_Camera::calc_exposure_time(IR_Exposure_Info *ir_einfot)
{
  /* NOTE: We have simplified this
   *       Configured as the old "Single Read Out"
   */

//  ir_einfot->nfowler = 1;
//  ir_einfot->nperiods = 1;
  // TODO: Calculate this
  // ir_einfot->exposedRQ = 0;
//  ir_einfot->period = ir_einfot->nfowler*ir_einfot->readTime;
//  printf("calc_exposure_time: readTime(%ld)\n", ir_einfot->readTime);

  /* TODO: Make sure the exposure shows up properly */
}



/*
 *+
 * FUNCTION NAME: IR_Camera::handle_interrupt
 *
 * INVOCATION: handle_interrupt()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Handle pause signal.
 *
 * DESCRIPTION:
 * Received a signal to pause. If shutter open close it then wait for
 * semaphore to continue - onces set reopen shutter if closed.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void IR_Camera::handle_interrupt()
{
  if (debug&DEBUG_ENTRYPOINTS)
    errlogMessage("Entered handle_interrupt");
  if (debug&DEBUG_ENTRYPOINTS)
    errlogMessage("Left handle_interrupt");
}

/*
 *+
 * FUNCTION NAME: IR_Camera::handle_change_state
 *
 * INVOCATION: handle_change_state()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Interrupt handler for the CHANGE_STATE signal
 *
 * DESCRIPTION:
 *  Does nothing
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void IR_Camera::handle_change_state()
{
  if (debug&DEBUG_ENTRYPOINTS)
    errlogMessage("IR_Camera::Entered handle_change_state");
  if (debug&DEBUG_ENTRYPOINTS)
    errlogMessage("IR_Camera::Left handle_change_state");
}


/*
 *+
 * FUNCTION NAME: IR_Camera::update_ir_times()
 *
 * INVOCATION: update_ir_times
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
*
 * FUNCTION VALUE:
 *
 * PURPOSE: Updates the exposure time left and period time left counters
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void IR_Camera::update_ir_times()
{
  epicsTime stime;
  double te, tp, tl; //, p;

  stime = epicsTime::getCurrent();

  if (hardware_abort)
    return;

  // Because we can't rely on accurately timestamped period starts use a simple modulus.
  // work out elapsed time in nth period

  // work out time since exposure start - seconds.
  tp = te = controller->exposure_lapsed();
  ir_statusp->put(EXPOSURE_ELAPSED, te);

  /*
  p = te/ir_einfo->period;
  if (ir_einfo->period > 0) {
    // work out time msec through nth period
    tp = ir_einfo->period * (ceil(p) - p);
  } else
    tp = 0.0;
    */

  // If a STOP has been issued, recalculate period time left
  if (hardware_stop) {
/* TODO: Deal with the different modes
    if (ir_einfo->readModeSet != IR_LINEAR_READOUT)
      // if DCS or FOWLER then halt exposure now
      tp = tp - ir_einfo->readIntvl;
    else if (p>=ir_einfo->nperiods)
      // Else - only modify period time left if finished period within which stop issued
      tp = 0.0;
*/
  }

  if (tp < 0.0)
    tp = 0.0;

//  ir_statusp->put(PER_TIME_LEFT, tp);

  // now time left
  if (hardware_stop) {
    // When a stop received timleft equals period time left
    tl = tp;
  } else
    tl = ir_einfo->exposedRQ - te;

  if (tl < 0.0)
    tl = 0.0;

  ir_statusp->put(TIME_LEFT, tl);

  if (debug & DEBUG_STATUS)
    message_write("Time_left in exposure=%f, time left this period=%f", tl, tp);
}

static std::map<std::string, std::string>
sensor_param_mapping{
	{NDAVGS, NFOWLER},
	{LRNS, NRESETS}
};

std::string
IR_Camera::get_sensor_parameter_name(const std::string& param) const
{
	ModePrefix mode;
	/**
	 * TODO: Hardcoded for the time being...
	 **/
	mode.pref = "obs";

	return mode.dbName(sensor_param_mapping.at(param));
}

void
IR_Camera::add_raw_parameter(const std::string& unit, const std::string& param, const std::string value)
{
	HeaderConf& header = (*product_configuration)[unit];

	header.add_card(param, value);
}

void
IR_Camera::add_config_unit(const std::string& unit_name)
{
	product_configuration->add_unit(unit_name);
}

/*
 *+
 * FUNCTION NAME: IR_Camera::~IR_Camera
 *
 * INVOCATION: delete ir_camera
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Destroys all objects in IR_camera
 *
 * DESCRIPTION:
 *
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
IR_Camera::~IR_Camera()
{
  if (ir_statusp != nullptr)
    delete ir_statusp;
  if (fitPix != nullptr)
    delete [] fitPix;
  if (pixSum != nullptr)
    delete [] pixSum;
  if (fit_sy != nullptr)
    delete [] fit_sy;
  if (fit_sy2 != nullptr)
    delete [] fit_sy2;
  if (fit_siy != nullptr)
    delete [] fit_siy;
  if (variance != nullptr)
    delete [] variance;
  if (variance1 != nullptr)
    delete [] variance1;
  if (zeroPixSave != nullptr)
    delete [] zeroPixSave;
  if (fitPixSave != nullptr)
    delete [] fitPixSave;
  if (coaddPix != nullptr)
    delete [] coaddPix;
  if (badPix != nullptr)
    delete [] badPix;
  if (cosmicPix != nullptr)
    delete [] cosmicPix;
  // Destroy controller object
  if (dummy_controller != nullptr)
    delete dummy_controller;
  else if (sdsu_controller != nullptr)
    delete sdsu_controller;
}

IrTimerThread::IrTimerThread(IR_Camera *cam, const string &name,
				unsigned int priority)
	: Thread(name.c_str(), priority, epicsThreadGetStackSize(epicsThreadStackSmall)),
	  ir_camera(cam)
{}

void
IrTimerThread::run() {
	double time_left;

	// Wait here until exposure starts - monitor timestamp
	lock();
	while (!ir_camera->exp_timestamp &&
	       !ir_camera->hardware_abort &&
	       !ir_camera->hardware_stop)
	{
		wait();
	}
	unlock();

	// now get the counter and count it down
	time_left = ir_camera->camera_status->op_status.Camera_Op_Status_u.ir.timeLeft;

	while ((time_left > 0.0) && !ir_camera->hardware_abort) {
		rest(min(HALF_SECOND, nint(time_left * THOUSAND)));
		ir_camera->update_ir_times();
		time_left = ir_camera->camera_status->op_status.Camera_Op_Status_u.ir.timeLeft;
	}
	ir_camera->update_ir_times();
	if (ir_camera->debug & DEBUG_INDICATORS)
		ir_camera->message_write("timer thread completed ok.");
}
