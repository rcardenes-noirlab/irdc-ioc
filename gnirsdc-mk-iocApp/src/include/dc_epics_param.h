/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_epics_param.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GNIRS PROJECT
 *
 * FILENAME dc_epics_param.h
 * 
 * PURPOSE
 * Include file with GNIRS DC EPICS parameter definitions
 *
 * $Log: dc_epics_param.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.15  2005/09/08 23:18:26  pjy
 * Renamed health to controlHealth
 *
 * Revision 1.14  2005/09/06 21:56:32  pjy
 * Added statusMsgs
 *
 * Revision 1.13  2005/08/18 03:17:26  pjy
 * Extended health records
 *
 * Revision 1.12  2005/08/02 04:45:25  pjy
 * DHS connection management changes
 *
 * Revision 1.11  2005/07/30 01:11:55  pjy
 * Fixed typo
 *
 * Revision 1.10  2005/07/29 23:44:05  pjy
 * *** empty log message ***
 *
 * Revision 1.9  2005/07/28 09:33:47  pjy
 * Mods after DC AT day 1
 *
 * Revision 1.8  2005/07/26 06:14:09  pjy
 * Added nframes
 *
 * Revision 1.7  2005/07/24 11:57:48  pjy
 * Added reconnecting
 *
 * Revision 1.6  2005/07/19 06:33:46  pjy
 * Added bunit
 *
 * Revision 1.5  2005/07/14 06:45:19  pjy
 * Added "saving" param
 *
 * Revision 1.4  2005/07/12 06:32:31  pjy
 * Added simulation params.
 * Changed abortingSdsu to cancelingSdsu
 * Added gratName
 * Added test pattern verify
 *
 * Revision 1.3  2005/04/27 03:40:39  pjy
 * PCI DSP working
 *
 * Revision 1.2  2005/02/25 04:46:41  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.1  2004/12/22 22:43:30  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.3  2004/12/22 06:56:05  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.2  2004/08/20 02:18:26  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.1  2004/04/25 02:03:45  pjy
 * Pre T2.2 version
 *
 *
 *
 */

#ifndef DC_EPICS_PARAM_H
#define DC_EPICS_PARAM_H

static const char CONFIG_NAME_DB[] = "configName.VAL";
static const char DEBUG_MODE_DB[] = "debugMode.VAL";
static const char SIM_MODE_DB[] = "simMode.VAL";
static const char STATE_DB[] = "state.VAL";
static const char SLAVE_STATE_DB[] = "slaveState.VAL";
static const char DATA_STATE_DB[] = "dataState.VAL";
static const char OBS_DATA_DEST_DB[] = "d_obs_dataDest.VAL";
static const char VIEW_DATA_DEST_DB[] = "d_view_dataDest.VAL";
static const char ABORTING_FLAG_DB[] = "aborting.VAL";
static const char SAVING_FLAG_DB[] = "saving.VAL";
static const char SDSU_CANCELING_FLAG_DB[] = "cancelingSDSU.VAL";
static const char STOPPING_FLAG_DB[] = "stopping.VAL";
static const char OBSERVEC_DB[] = "observeC.VAL";
static const char CAD_START_MSG_DB[] = "cadStartMsg.VAL";
static const char CONNECTION_DB[] = "connection.VAL";
static const char SDSU_INIT_DONE_FLAG_DB[] = "initSDSUDone.VAL";
static const char SDSU_CMD_RUNNING_DB[] = "SDSUcmdRunning.VAL";
static const char DOCONNECT_DB[] = "doConnect.VAL";
static const char DHS_AVAILABLE_DB[] = "dhsAvailable.VAL";
static const char DHS_CONNECTED_DB[] = "dhsConnected.VAL";
static const char DC_HEALTH_DB[] = "controlHealth.VAL";
static const char DC_HEALTH_MSG_DB[] = "controlHealth.IMSS";
static const char DHS_HEALTH_DB[] = "dhsHealth.VAL";
static const char DHS_HEALTH_MSG_DB[] = "dhsHealth.IMSS";
static const char DHS_PROBLEM_DB[] = "dhsProblem.VAL";
static const char DHS_PROBLEM_MSG_DB[] = "dhsProblemMsg.VAL";
static const char FITS_HEALTH_DB[] = "fitsHealth.VAL";
static const char FITS_HEALTH_MSG_DB[] = "fitsHealth.IMSS";
static const char FITS_PROBLEM_DB[] = "fitsProblem.VAL";
static const char FITS_PROBLEM_MSG_DB[] = "fitsProbMsg.VAL";
static const char DATA_HEALTH_DB[] = "dataTaskHealth.VAL";
static const char DATA_HEALTH_MSG_DB[] = "dataTaskHealth.IMSS";
static const char SLAVE_HEALTH_DB[] = "slaveTaskHealth.VAL";
static const char SLAVE_HEALTH_MSG_DB[] = "slaveTaskHealth.IMSS";
static const char CAMERA_HEALTH_DB[] = "cameraHealth.VAL";
static const char CAMERA_HEALTH_MSG_DB[] = "cameraHealth.IMSS";
static const char CAMERA_OP_DB[] = "cameraOp.VAL";
static const char CAMERA_OP_MSG_DB[] = "cameraOpMsg.VAL";
static const char DHS_SERVER_IP_DB[] = "svAddr.VAL"; 
static const char DHS_SERVER_NAME_DB[] = "svName.VAL";
static const char DHS_DATA_LABEL_DB[] = "dataLabel.VAL";
static const char BUNIT_DB[] = "bunit.VAL";
static const char NFRAMES_DB[] = "nframes.VAL";
static const char FITS_AVAILABLE_DB[] = "fitsAvailable.VAL";
static const char FITS_CONNECTED_DB[] = "fitsConnected.VAL";
static const char FITS_SERVER_NAME_DB[] = "fitsServer.VAL";
static const char FITS_SERVER_PORT_DB[] = "fitsPort.VAL"; 
static const char OBS_RAWQLS_DB[] = "d_obs_rawQLS.VAL";
static const char VIEW_RAWQLS_DB[] = "d_view_rawQLS.VAL";
static const char VIEW_CMPQLS_DB[] = "d_view_cmpQLS.VAL";
static const char DETTYPE_DB[] = "detType.VAL";
static const char DETID_DB[] = "detID.VAL";
static const char TIMDSP_DB[] = "timingDSP.VAL";
static const char INTERFACEDSP_DB[] = "intfDSP.VAL";

static const char SDSU_BOARD_DB[] = "SDSUboard";
static const char SDSU_CMD_DB[] = "SDSUcommand";
static const char SDSU_MEM_SPACE_DB[] = "SDSUmemSpace";
static const char SDSU_MEM_ADDR_DB[] = "SDSUaddress";
static const char SDSU_ARG1_DB[] = "SDSUarg1";
static const char SDSU_ARG2_DB[] = "SDSUarg2";
static const char SDSU_DEBUG_DB[] = "SDSUdebug";
static const char SDSU_NTESTS_DB[] = "SDSUntests";
static const char SDSU_COMMS_DB[] = "SDSUcomms";

static const char DO_TEST_PATTERN_DB[] = "doTestPat";
static const char TEST_PATTERN_CONSTANT_DB[] = "testPatConst";
static const char TEST_PATTERN_SEED_DB[] = "testPatSeed";
static const char TEST_PATTERN_INC_DB[] = "testPatInc";
static const char TEST_PATTERN_AMP_INC_DB[] = "testPatAmpInc";
static const char TEST_PATTERN_READ_INC_DB[] = "testPatReadInc";
static const char TEST_PATTERN_OS_DB[] = "testPatOS";
static const char TEST_PATTERN_VERIFY_DB[] = "testPatVerify";

static const char DO_CMP_IM_DB[] = "doCmpIm";
static const char WAVE_MIN_DB[] = "waveMin";
static const char WAVE_MAX_DB[] = "waveMax";
static const char GRATING_MIN_DB[] = "gratMin";
static const char GRATING_MAX_DB[] = "gratMax";
static const char GRATING_NAME_DB[] = "gratName";

static const char CONTROLLER_INIT_MESS_DB[] = "initSDSU.MESS";
static const char CONTROLLER_TEST_MESS_DB[] = "testSDSU.MESS";
static const char CONTROLLER_RESET_MESS_DB[] = "resetSDSU.MESS";
static const char CONTROLLER_SHUTDOWN_MESS_DB[] = "shutdownSDSU.MESS";
static const char CONTROLLER_DEBUG_MESS_DB[] = "debugSDSU.MESS";
static const char CONTROLLER_CANCEL_MESS_DB[] = "cancelSDSU.MESS";
static const char CONTROLLER_CMD_MESS_DB[] = "DSPDirectCmd.MESS";

static const char STATUS_STR_DB[] = "statusMsg";

#endif
