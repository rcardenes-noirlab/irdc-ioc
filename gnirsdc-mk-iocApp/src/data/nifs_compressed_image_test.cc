/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 * $Id
 * GNIRS PROJECT
 * 
 * FILENAME 
 *   gnirs_compressed_image.cc
 * 
 * GENERAL DESCRIPTION
 *   Implementation of gnirs_compressed_image class.
 *
 * ORIGINAL AUTHOR: 
 *   Peter Young
 *
 * HISTORY
 *
 *  November 2004 - PJY Created.
 *
 *INDENT-ON
 *
 * $Log: gnirs_compressed_image_test.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Gnirs; first gemini-modified release
 *
 * Revision 1.4  2005/09/07 00:48:29  pjy
 * Moved message_write to dc.cc
 *
 * Revision 1.3  2005/07/21 04:06:19  pjy
 * Added support for expanding the compressed image so that it is square (as per ICD)
 *
 * Revision 1.2  2005/07/12 07:07:36  pjy
 * Adjusted to fit actual GNIRS data
 *
 * Revision 1.1  2004/12/22 06:13:51  pjy
 * DC data src files ready for first Gemini release
 *
 *
 -------------------------------------------------------------------------*/
#ifdef vxWorks
#include <vxWorks.h>
#endif

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/

#include <math.h>
#include "gnirs_compressed_image.h"
#include "fits.h"
#include "preferences.h"
#include "exception.h"
#include "dc.h"

static void get_pixels(Fits* fits, int j, float* fbuf, int xo, int yo, int height, int width);
static void create_new_fits(char* fits_name, char* buf, int width, int height, int bp);

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: compressedImageTest
 * 
 * INVOCATION: compressedImageTest(fits_name)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > fits_name - name of simulation image
 *
 * FUNCTION VALUE: integer status
 * 
 * PURPOSE: Test GNIRS compressed image class
 * 
 * DESCRIPTION: Uses GNIRS simulation image and compresses it
 * with class.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
#ifdef vxWorks
int compressedImageTest(Fitsname fits_name, int rows, int cols, int xf, int yf)
{
#else
int main(int argc, char *argv[])
{
  Fitsname fits_name;
  int rows = 64;
  int cols = 2048; // default rows and columns for 512k transfer with bitpix = -32
  int xf = 1;
  int yf = 1;
  if (argc < 1) {
    printf("Usage: gnirs_compressed_image_test fits_name [rows cols xf yf]\n");
    return -1;
  }
  strncpy(fits_name, argv[1], FITSNAMELEN);
  if (argc > 2)
    rows = atoi(argv[2]);
  if (argc > 3)
    cols = atoi(argv[3]);
  if (argc > 4)
    xf = atoi(argv[4]);
  if (argc > 5)
    yf = atoi(argv[5]);
#endif

  GNIRS_Compressed_Image *cmp_im;
  Fits *fits = NULL;
  int first_row = 0;
  int j,r,c,scols,srows;
  int sh = GNIRS_SLICE_HEIGHT;
  int slices = GNIRS_SLICES;
  int ret = 0;
  float *buf = NULL;

  try {
    // Open and read Fits file
    cout << "Opening input FITS file " << fits_name << endl;
    fits = new Fits(fits_name, O_RDONLY, TRUE);
    cout << "Done" << endl;
    j = (fits->extend)? 1:0;

    try {
      Preferences gnirs_optics("gnirs_optics",1,true,"");
      Table_Item* gnirs_optics_item;
      string val,par;
      float fval;
      string pref_fname = (string)"cnf/" + (string)"gnirs_optics.table";
      
      gnirs_optics.read(pref_fname);
      gnirs_optics_item = *gnirs_optics.items.begin();
      
      // Get the number of slices
      gnirs_optics_item->find("nslices", val);
      stringtonum(val.c_str(), slices);
      message_write(ALWAYS_LOG, "nslices = '%s'", val.c_str());
      
      // Get the slice height
      gnirs_optics_item->find("slitheight", val);
      stringtonum(val.c_str(), fval);
      sh = (int) ceil(fval);
      message_write(ALWAYS_LOG, "slitheight = '%s'", val.c_str());
      
      // Now for the selected grating - choose the starting row
      par = (string)"K" + (string)"y1";
      gnirs_optics_item->find(par, val);
      stringtonum(val.c_str(), fval);
      first_row = (int) floor(fval);
      message_write(ALWAYS_LOG, "%s = '%s'", par.c_str(), val.c_str());
      
    }
    catch (Error& e) {
      message_write(ALWAYS_LOG, "Error with optics prefs '%s'?", e.record_error(__FILE__,__LINE__));
    }

    // Create a compressed image object
    cmp_im = new GNIRS_Compressed_Image(0.94, 2.5, 0.94, 2.5, slices, sh, fits->xt[j].nx, first_row); 

    cout << "Created compress image " << slices << "x" << sh << endl;

    // Pixels are transferred rows*cols at a time starting at 0,0
    // Always transfer first extension
    cols = (cols>fits->xt[j].nx)? fits->xt[j].nx: cols;
    buf = new float[rows*cols];

    srows = rows;
    for (r=0; r<fits->xt[j].ny; r+=rows) {
      if (r+srows>fits->xt[j].ny)
	srows = fits->xt[j].ny-r;
      scols = cols;
      
      for (c=0; c<fits->xt[j].nx; c+=cols) {
       if (c+scols>fits->xt[j].nx)
	 scols = fits->xt[j].nx-c;

       // Fetch the Fits pixels into the buffer
       get_pixels(fits,j,buf,c,r,srows,scols);
	
       // Add them to the compressed image
       cmp_im->add(buf,c,r,scols,srows);
      }
    }

    // Expand the output image to make it square
    if ((xf>1) || (yf>1))
      cmp_im->expand(xf,yf);
   
    // Now create a new Fits file with compressed image
    create_new_fits((char*)"cmp_im.fits", (char*)cmp_im->image, cmp_im->nx, cmp_im->ny, fits->xt[j].bitpix);
    
  }
  catch (Error&e) {
    e.print_error(__FILE__, __LINE__);
    ret = -1;
  }

  if (fits) delete fits;
  if (cmp_im) delete cmp_im;
  if (buf) delete [] buf;
  return ret;
}
/*
 *+ 
 * FUNCTION NAME: get_pixels
 * 
 * INVOCATION: get_pixels(Fits* fits, int j, char* buf, int xo, int yo, int height, int width)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > fits - fits file to use for building main header
 * > j - fits extension
 * < buf - transfer buffer - filled with fits pixels
 * > xo - offset into frame
 * > yo - offset into frame
 * > height - height of sub frame to transfer
 * > width - width of sub frame to transfer
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Fills transfer buffer with pixels from Fits file
 * 
 * DESCRIPTION: 
 * Simply copies from fits file the rectangular area of pixels - no bounds
 * checking performed - assumed done externally.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
static void get_pixels(Fits* fits, int j, float* fbuf, int xo, int yo, int height, int width)
{
  short* spix = (short*) fits->xt[j].data;
  int* ipix = (int*) fits->xt[j].data;
  float* fpix = (float*) fits->xt[j].data;
  double* dpix = (double*) fits->xt[j].data;
  int k,c,r;
  
  k = 0;	
  switch (fits->xt[j].bitpix) {
  case SHORT_BITPIX:
    for (r=yo; r<yo+height; r++)
      for (c=xo; c<xo+width; c++)
	fbuf[k++] = spix[r*fits->xt[j].nx+c];
    break;
  case INT_BITPIX:
    for (r=yo; r<yo+height; r++)
      for (c=xo; c<xo+width; c++)
	fbuf[k++] = ipix[r*fits->xt[j].nx+c];
    break;
  case FLOAT_BITPIX:
    for (r=yo; r<yo+height; r++)
      for (c=xo; c<xo+width; c++)
	fbuf[k++] = fpix[r*fits->xt[j].nx+c];
    break;
  case DOUBLE_BITPIX:
    for (r=yo; r<yo+height; r++)
      for (c=xo; c<xo+width; c++)
	fbuf[k++] = dpix[r*fits->xt[j].nx+c];
    break;
  default:
    break;
  }
}
/*
 *+ 
 * FUNCTION NAME: create_new_fits
 * 
 * INVOCATION: create_new_fits(const char* fname, char* buf, int width, int height, int bp)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > fname - outpur directory
 * < fits - created fits file 
 * > buf - data buffer
 * > width- width of data
 * > height - height of data
 * > bp - bitpix
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Create a new Fits file with standard headers
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
static void create_new_fits(char* fits_name, char* buf, int width, int height, int bp)
{
  int naxes[2];
  Ext_Type etype[1];
  int pc[1],gc[1];
  double bz[1],bs[1];
  int bitpix[1];
  Fits* new_fits;
 
  new_fits = new Fits();
  
  // Set up Fits data structures 
  naxes[0] = width;
  naxes[1] = height;
  bz[0] = 0.0;
  bs[0] = 1.0;
  pc[0] = 0;
  gc[0] = 1;
  bitpix[0] = bp;
  etype[0] = EXT_IMAGE;
  
  unlink(fits_name);
  
  // Create the FITS file
  new_fits->create(fits_name, etype, bitpix, naxes, S_IRUSR|S_IWUSR, FITS_KEYS_PER_BLOCK, 
		   0, FITS_KEYS_PER_BLOCK);
  
  cout << "Fits file '" << fits_name << "' created ok." << endl;
  
  // Write standard keywords
  new_fits->write_std_keywords(etype, bs, bz, pc, gc);

  // write data
  new_fits->write_data(buf, bp, 0, 0, 0, width, height);
	
  new_fits->close();
}
