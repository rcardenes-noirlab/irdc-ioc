/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME config_table.cc
 * 
 * GENERAL DESCRIPTION
 *  Main program for testing configuration tables
 * 
 */

/* Include files */
#include <iostream>
#ifdef vxWorks
#include "configuration.h"
#endif
#include "config_table.h"
#include "exception.h"

/* defines */

/* typedefs */

/* class declarations */
/* global variables */

#ifdef vxWorks
  int configTableTest()
#endif
{
  try {
    int i;
    Instrument_Table table;
    Instrument_Table table2;
    Instrument_Table table3;
    Filenametype fname;
    string new_name;
    string ans;

    try{
#ifdef vxWorks
      strlcpy(fname,INSTRUMENT_TABLE,FILENAMELEN);
#endif
      table.read(fname);
      cout << "Instrument table read ok." << endl;
      cout << "Sizeof Instrument_desc=" << sizeof(Instrument_Desc) << endl;
      cout << "Sizeof Camera_desc=" << sizeof(Camera_Desc) << endl;
      cout << "Sizeof Controller_desc=" << sizeof(Controller_Desc) << endl;
      cout << "Sizeof Chip_desc=" << sizeof(Chip_Desc) << endl;

      table2 = table;
      cout << "Instrument table copied ok." << endl;

      new_name = fname;
      new_name += "-new";
      table2.write(new_name);
      cout << "New instrument table written ok." << endl;

      table3.read(new_name);
      cout << "New instrument table read ok." << endl;

      for (i=0; i<table3.n_items; i++) {
	cout << table3[i].name << endl;
      }
      
      cout << "All done...";
      cin >> ans;
    }
    catch (Error&e) {
      e.print_error(__FILE__,__LINE__);
    }
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return 0;
}
  
