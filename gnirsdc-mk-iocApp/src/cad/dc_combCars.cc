/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_combCars.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI DC PROJECT
 *
 * FILENAME dc_combCars.c
 *
 * GENERAL DESCRIPTION
 *
 * Combines up to 6 CAr val,err and message codes. Does this by selecting highest
 * VAL from each of the inputs to the gensub.
 *
 * Acknowledgment
 * This routine is modeled after similar routine used by GMOS team written by NOAO.
 *
 * $Log: dc_combCars.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.11  2005/09/10 19:46:55  pjy
 * Made strings constant defs
 *
 * Revision 1.10  2005/09/09 18:39:51  pjy
 * Fixed bug in comb health routine
 *
 * Revision 1.9  2005/08/18 03:30:18  pjy
 * Added dcHealthCombine
 *
 * Revision 1.8  2005/08/02 04:47:27  pjy
 * Commented unneeded msgs
 *
 * Revision 1.7  2005/07/28 09:38:52  pjy
 * Added debugging messages
 *
 * Revision 1.6  2005/07/18 00:36:21  pjy
 * Init vars
 *
 * Revision 1.5  2004/08/20 02:27:04  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.4  2004/03/23 02:11:03  pjy
 * Changed nifs to dc - so that code can be more generic
 *
 * Revision 1.3  2003/09/19 06:24:22  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.2  2003/03/25 06:26:22  pjy
 * Consistent naming using _
 *
 * Revision 1.1  2003/03/11 01:29:39  pjy
 * Import modules
 *
 *
 */
#include <cstdio>
#include <cstring>

#ifdef __cplusplus
extern "C" {
#endif
#include "genSubRecord.h"
#include "dbAccess.h"
#include "dbCommon.h"
#ifdef __cplusplus
}
#endif
#include "dc.h"
#include "logging.h"

#define HEALTH_GOOD    "GOOD"
#define HEALTH_BAD     "BAD"
#define HEALTH_WARNING "WARNING"

#define NULL_MSG       ""

#ifdef __cplusplus
extern "C" long dcCombCars(struct genSubRecord *pGenSub);
extern "C" long dcHealthCombine(struct genSubRecord *pGenSub);
#endif

/*
 *+
 * FUNCTION NAME:
 * dcCombCars
 *
 * INVOCATION:
 * struct genSubRecord *pGenSub;
 * long status;
 *
 * status = dcCombCars( *pGenSub )
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pGenSub   (struct genSubRecord *)   pointer to genSub data structure
 *
 * FUNCTION VALUE:
 * long  Status value, 0 for success
 *
 * PURPOSE:
 * User defined function for several genSub records used for hierarchically
 * combining CAR status values.
 *
 * DESCRIPTION:
 * This routine is called whenever the appropriate genSub records are processed.
 * It takes the CAR VAL, OERR and OMSS fields of three CAR records.  The highest
 * CAR VAL is determined.  The highest CAR value is placed on the genSub output
 * along with its error code (OERR) and error message (OMSS) values.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * The genSub record needing this routine must have its SNAM property set to
 * combCars.
 *
 * DEFICIENCIES:
 * None known
 *
 * HISTORY (optional):
 * 17-Apr-1997  Original version.				  J.E. Tvedt
 *
 * 13-Dec-2002  Modified for DC to use C++ and simplified.      P Young.
 *
 *-
 */


long dcCombCars(struct genSubRecord *pGenSub)
{

  const int MAX_INPUT_CARS = 6;
  int i;
  long ival=0;
  long tmp;
  long ierr=0;
  char imss[MAX_STRING_SIZE];
  long status = OK;
  void *genInputs[MAX_INPUT_CARS*3] = {pGenSub->a, pGenSub->b, pGenSub->c, pGenSub->d, pGenSub->e, pGenSub->f,
				       pGenSub->g, pGenSub->h, pGenSub->i, pGenSub->j, pGenSub->k, pGenSub->l,
				       pGenSub->m, pGenSub->n, pGenSub->o, pGenSub->p, pGenSub->q, pGenSub->r};

  try {

    //     logger::message(logger::Level::Full, "dcCombCars: genSub name=%s", pGenSub->name);

    // Loop over all possible Gensub inputs to find highest value
    // There could be up to MAX_INPUT_CARS of these
    for (i=0; i<MAX_INPUT_CARS; i++) {

      // If there is a another CAR attached to the genSub, compare
      // it to the current max.  If it is larger, make it the maximum.
      if (genInputs[i*3] != NULL) {

	// Make sure next two inputs have been setup, ie expect error code and message
	if ((genInputs[i*3+1] == NULL) || (genInputs[i*3+2] == NULL))
	  throw Error("NULL pointer found on gensub inputs", E_ERROR, -1, __FILE__, __LINE__);

	// Now check to see if highest error value (start from first value)
	tmp = *(long *) genInputs[i*3];
	if ((i == 0) || (tmp > ival)) {
	  ival = tmp;
	  ierr = *(long *) genInputs[i*3+1];
	  strncpy(imss, (char*) genInputs[i*3+2], MAX_STRING_SIZE-1);
	  logger::message(logger::Level::Full, "dcCombCars: input=%d, val=%d, err=%d", i,ival,ierr);
	  logger::message(logger::Level::Full, "dcCombCars: imss=%s", imss);
	}
      }
    }

    // Make sure output values have been setup for Val,Err amd Msg
    if ((pGenSub->vala == NULL) || (pGenSub->valb == NULL) || (pGenSub->valc == NULL))
      throw Error("NULL pointer found on gensub outputs", E_ERROR, -1, __FILE__, __LINE__);

    // Make sure data types have been specified correctly
    if ((pGenSub->ftva != DBF_LONG) || (pGenSub->ftvb != DBF_LONG) || (pGenSub->ftvc != DBF_STRING))
      throw Error("Wrong data type specified on gensub output", E_ERROR, -1, __FILE__, __LINE__);

    // Now put the maximum CAR status and its error message and error code
    // on the outputs of the genSub
    *(long*)pGenSub->vala = ival;
    *(long*)pGenSub->valb = ierr;
    strcpy((char*)pGenSub->valc, imss);
    logger::message(logger::Level::Full, "dcCombCars: output, val=%d, err=%d", ival,ierr);
    logger::message(logger::Level::Full, "dcCombCars: imss='%s'", (char*)pGenSub->valc);
  }
  catch (Error& err) {
    status = err.errnum;
    logger::message(logger::Level::Min, "dcCombCars: Error combining CARs, gensub '%s'", pGenSub->name);
    logger::message(logger::Level::Min, "dcCombCars: %s", err.record_error(__FILE__, __LINE__));
  }

  return status;
}

/*
 *+
 * FUNCTION NAME:
 * dcHealthCombine
 *
 * INVOCATION:
 * struct genSubRecord *pGenSub;
 * long status;
 *
 * status = dcHealthCombine( *pGenSub )
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pGenSub   (struct genSubRecord *)   pointer to genSub data structure
 *
 * FUNCTION VALUE:
 * long  Status value, 0 for success
 *
 * PURPOSE:
 * User defined function for combining input health pairs.
 *
 * DESCRIPTION:
 * This routine is called whenever the appropriate genSub records are processed.
 * It takes the Health SIR VAL and OMSS fields of up to five SIR records.  The highest
 * SIR health VAL is determined and then placed on the genSub output
 * along with its error message (OMSS) value.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * The genSub record needing this routine must have its SNAM property set to
 * dcCombHealth.
 *
 * DEFICIENCIES:
 * None known
 *
 * HISTORY (optional):
 * 13-Dec-2002  Modified for DC to use C++ and simplified from CICS.      P Young.
 *
 *-
 */
long dcHealthCombine(struct genSubRecord *pGenSub)
{
  const int MAX_INPUT_HEALTH = 5;
  int i, index;
  string str,str2,msg;
  long status = OK;
  void *genInputs[MAX_INPUT_HEALTH*2] = {pGenSub->a, pGenSub->b, pGenSub->c, pGenSub->d, pGenSub->e, pGenSub->f,
					 pGenSub->g, pGenSub->h, pGenSub->i, pGenSub->j};

  try {

    //logger::message(logger::Level::Full, "dcHealthCombine: genSub name=%s", pGenSub->name);

    // Loop over all possible Gensub inputs to find worst health value
    str = HEALTH_GOOD;
    msg = NULL_MSG;
    index = 1;

    for (i=0; i<MAX_INPUT_HEALTH; i++) {

      // If there is a another HEALTH SIR attached to the genSub, compare
      // it to the current max.  If it is larger, make it the maximum.
      if (genInputs[i*2] != NULL) {

	// Make sure next input has been setup, ie expect message
	if (genInputs[i*2+1] == NULL)
	  throw Error("NULL pointer found on gensub inputs", E_ERROR, -1, __FILE__, __LINE__);

	// Now check to see if highest health value (start from first value)
	str2 = (char*) genInputs[i*2];

	// If health BAD then set msg and exit loop
	if (str2 == HEALTH_BAD) {
	  str = str2;
	  index = i+1;
	  msg = (char*) genInputs[i*2+1];
	  logger::message(logger::Level::Full, "dcHealthCombine: BAD health (%d), msg='%s'", index, msg.c_str());
	  break;
	} else if (str2 == HEALTH_WARNING) {
	  str = str2;
	  index = i+1;
	  msg = (char*) genInputs[i*2+1];
	}
      } else
	logger::message(logger::Level::Full, "dcHealthCombine: genInput %d == NULL", i+1);
    }

    // Make sure output values have been setup for Val,Err amd Msg
    if ((pGenSub->vala == NULL) || (pGenSub->valb == NULL))
      throw Error("NULL pointer found on gensub outputs", E_ERROR, -1, __FILE__, __LINE__);

    // Make sure data types have been specified correctly
    if ((pGenSub->ftva != DBF_STRING) || (pGenSub->ftvb != DBF_STRING))
      throw Error("Wrong data type specified on gensub output", E_ERROR, -1, __FILE__, __LINE__);

    // Now put the maximum CAR status and its error message and error code
    // on the outputs of the genSub
    strcpy((char*)pGenSub->vala, (char*)str.c_str());
    strcpy((char*)pGenSub->valb, (char*)msg.c_str());
    *(long *)pGenSub->valc = index;
    logger::message(logger::Level::Full, "dcHealthCombine: output (%d), val=%s, imss='%s'", index, (char*)pGenSub->vala,(char*)pGenSub->valb);
  }
  catch (Error& err) {
    status = err.errnum;
    logger::message(logger::Level::Min, "dcHealthCombine: Error combining Health, gensub '%s'", pGenSub->name);
    logger::message(logger::Level::Min, "dcHealthCombine: %s", err.record_error(__FILE__, __LINE__));
  }

  return status;
}
