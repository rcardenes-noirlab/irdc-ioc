[schematic2]
uniq 169
[tools]
[detail]
w 1770 1755 100 0 n#161 hwout.hwout#157.outp 1840 1744 1760 1744 ebos.viewEnBo.OUT
w -158 1211 -100 0 CLID inhier.CLID.P -208 1296 -160 1296 -160 1200 -96 1200 eapply.seqApply.CLID
w 546 987 100 0 n#140 eapply.seqApply.OCLB 288 1040 464 1040 464 976 688 976 dcSeqObs.dcSeqObs#124.CLID
w 362 1115 100 0 n#139 eapply.seqApply.OCLA 288 1104 496 1104 496 1264 688 1264 dcSeqView.dcSeqView#123.CLID
w 314 1403 -100 0 VAL outhier.VAL.p 384 1392 304 1392 304 1232 288 1232 eapply.seqApply.VAL
w 346 1323 -100 0 MESS outhier.MESS.p 416 1312 336 1312 336 1200 288 1200 eapply.seqApply.MESS
w -190 1387 -100 0 DIR eapply.seqApply.DIR -96 1232 -128 1232 -128 1376 -192 1376 inhier.DIR.P
w 354 1595 100 0 n#132 dcSeqObs.dcSeqObs#124.MESS 848 976 1104 976 1104 1584 -336 1584 -336 1040 -96 1040 eapply.seqApply.INMB
w 354 1563 100 0 n#131 dcSeqObs.dcSeqObs#124.VAL 848 1008 1072 1008 1072 1552 -304 1552 -304 1072 -96 1072 eapply.seqApply.INPB
w 338 1531 100 0 n#130 dcSeqView.dcSeqView#123.MESS 848 1264 1008 1264 1008 1520 -272 1520 -272 1104 -96 1104 eapply.seqApply.INMA
w 338 1499 100 0 n#129 dcSeqView.dcSeqView#123.VAL 848 1296 976 1296 976 1488 -240 1488 -240 1136 -96 1136 eapply.seqApply.INPA
w 362 1083 100 0 n#127 eapply.seqApply.OUTB 288 1072 496 1072 496 1008 688 1008 dcSeqObs.dcSeqObs#124.DIR
w 546 1307 100 0 n#126 eapply.seqApply.OUTA 288 1136 464 1136 464 1296 688 1296 dcSeqView.dcSeqView#123.DIR
s -544 -16 200 0 Separate CADs are maintained for View and Obs modes
s -550 60 200 0 This is the top level schematic for the GNIRS sequencing parameters
s 1456 1584 100 0 This record sets the viewEnabled SIR in the SAD. This SIR is inspected
s 1456 1552 100 0 by the view loop enabling/disabling view mode operations.
s 1440 1184 100 0 The binary record, specInterval, flags whether the read interval is to be specified (1) 
s 1440 1136 100 0 or calculated (0). If it is specified, then the value of the analog record, interval, will
s 1440 1088 100 0 be used. In normal practice, the read interval is calculated. This is only provided for testing.
[cell use]
use eaos 1952 1392 100 0 interval
xform 0 2016 1312
use ebis 1536 1360 100 0 specInterval
xform 0 1616 1296
p 1264 1134 100 0 0 ONAM:enabled
p 1264 1166 100 0 0 ZNAM:disabled
use ebos 1568 1856 100 0 viewEnBo
xform 0 1632 1776
p 1568 1648 100 0 1 ONAM:View Disable
p 1568 1680 100 0 1 ZNAM:View Enable
p 1760 1744 75 768 -1 pproc(OUT):PP
use hwout 1840 1703 100 0 hwout#157
xform 0 1936 1744
p 1936 1735 100 0 -1 val(outp):$(sadtop)viewEnabled.VAL PP NMS
use inhier -224 1255 100 0 CLID
xform 0 -208 1296
use inhier -208 1335 100 0 DIR
xform 0 -192 1376
use outhier 352 1351 100 0 VAL
xform 0 368 1392
use outhier 384 1271 100 0 MESS
xform 0 400 1312
use dcSeqObs 688 855 100 0 dcSeqObs#124
xform 0 768 960
use dcSeqView 688 1143 100 0 dcSeqView#123
xform 0 768 1248
use eapply -20 1316 150 0 seqApply
xform 0 96 960
use eborderC -664 -405 100 0 eborderC#0
xform 0 1016 900
p 1912 -252 100 768 -1 author:Peter Young
p 1904 -132 40 0 -1 comment3:2001-02-02 Modified for GNIRS. Peter Young
p 1912 -284 100 768 -1 date:2001-02-02
p 2136 -204 200 768 -1 file:dcSeq
p 2408 -252 100 0 -1 page:1
p 2520 -252 100 0 -1 pages:1
p 2184 -252 100 0 -1 revision:0
p 2136 -140 150 768 -1 system:Gemini GNIRS DC
[comments]
