/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: gnirs_dc_setup.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GNIRS PROJECT
 *
 * FILENAME gnirs_dc_setup.h
 * 
 * PURPOSE
 * Include file with GNIRS DC setup definitions
 *
 * $Log: gnirs_dc_setup.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.1  2004/12/22 22:43:58  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.1  2001/10/19 00:24:47  pjy
 * Added for GNIRS DC
 *
 *
 */

#ifndef GNIRS_DC_SETUP
#define GNIRS_DC_SETUP
#include <dbDefs.h>
#include <cadRecord.h>
#include <dbCommon.h>
#include <recSup.h>
#include <cad.h>

#ifdef	__cplusplus
extern "C" {
#endif
  
  /* put definitions here */
  int dc_verify_pars(struct cadRecord *);
  int dc_copy_pars(struct cadRecord *);
  
#ifdef	__cplusplus
}
#endif
#endif /* GNIRS_DC_SETUP */
