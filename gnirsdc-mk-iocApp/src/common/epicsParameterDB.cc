/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * GEMINI NIFS PROJECT
 * 
 * FILENAME epicsParameterDB.cc
 * 
 * GENERAL DESCRIPTION
 * Implements methods for access to EPICS DB from
 * the EpicsParameterDB interface.
 *
 */

#include "utility.h"
#include <cstdlib>
#include "exception.h"
extern "C" {
#include <dbAccess.h>
#include <dbCommon.h>
}
#include "parameter.h"
#include "epicsParameterDB.h"

using namespace std;

/*
 *+ 
 * FUNCTION NAME: EpicsParameterDB::EpicsParameterDB
 * 
 * INVOCATION: epicsParameterDB = new EpicsParameterDB();
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: EpicsParameterDB constructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
EpicsParameterDB::EpicsParameterDB() 
{
}
/*
 *+ 
 * FUNCTION NAME: setAddr(char* dbName) 
 * 
 * INVOCATION: addr = setAddr(dbName) 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: address of named parameter
 * 
 * PURPOSE: takes a named EPICS parameter and returns its dbAddr
 * 
 * DESCRIPTION: Uses EPICS function dbNameToAddr to get pointer
 * to dbAddr structure for named EPICS variable
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void* EpicsParameterDB::setAddr(const char* dbName, const char* param, const char* prefix, const char* suffix) 
{
  void *addr = NULL;
  string fullName;

  if ((dbName != NULL) && (strlen(dbName) > 0)) {
    ostringstream ostr;
    long status;
    struct dbAddr *pAddr = NULL; 
    pAddr = (struct dbAddr*) malloc(sizeof(struct dbAddr));

    fullName = string(prefix) + string(dbName) + string(suffix);

    //cout << "About to call dbNameToAddr for " << fullName << endl; 

    // Call EPICS dbNameToAddr to get EPICS address
    if ((status = dbNameToAddr((char*)fullName.c_str(), pAddr)) != OK) {
      ostr << "Failed in dbNameToAddr for " << fullName << ends;
      throw Error(ostr, E_ERROR, status, __FILE__, __LINE__);
    }
    addr = (void*) pAddr;
  }
  
  return addr;
}
/*
 *+ 
 * FUNCTION NAME: freeAddr(void *addr) 
 * 
 * INVOCATION: freeAddr(addr) 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > addr - pointer to dbAddr structure to free
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: frees memory associated with dbAddr structure
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void EpicsParameterDB::freeAddr(const void *addr) 
{
  if (addr != NULL)
    free((void*)addr);
}
/*
 *+ 
 * FUNCTION NAME: put(const char* paramDBName, const char* dbName, const char* param, oid *addr, 
 *                    char*|long|double value, unsigned int offset, unsigned int n)  
 * 
 * INVOCATION: put(paramDBName, dbName, param, addr, value, offset, n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > paramDBName - name of parameter db that has called this put
 * > dbName - name of parameter in backend database
 * > param - common name of parameter - may be same as dbName
 * > addr - pointer to dbAddr structure 
 * > localAddr - pointer to local structure 
 * > value - value of parameter to set
 * > offset - offset added to addr to get desired starting address - default 0
 * > n - number of elements to put - default 1
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: sets EPICS parameter to value
 * 
 * DESCRIPTION: uses addr address to set an EPICS parameter to 
 * particular value. Function is overloaded to support string, long
 * and double variables
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, const string* value, unsigned int offset, unsigned int n)
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, const char* value, unsigned int offset, unsigned int n) 
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, const unsigned char *value, unsigned int offset, unsigned int n) 
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, const short *value, unsigned int offset, unsigned int n) 
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, const unsigned short *value, unsigned int offset, unsigned int n) 
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param,const void *addr, 
			   const void *localAddr, const int *value, unsigned int offset, unsigned int n) 
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param,const void *addr, 
			   const void *localAddr, const unsigned int *value, unsigned int offset, unsigned int n) 
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, const long *value, unsigned int offset, unsigned int n) 
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, const unsigned long *value, unsigned int offset, unsigned int n) 
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, const float* value, unsigned int offset, unsigned int n) 
{
  put_local_param(localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, const double *value, unsigned int offset, unsigned int n) 
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, const bool* value, unsigned int offset, unsigned int n) 
{
  put_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
/*
 *+ 
 * FUNCTION NAME: get(const char* paramDBName, const char* dbName, const char* param, void *addr, void *localAddr, 
 *                    char*|long*|double* &value, int offset, int n)  
 * 
 * INVOCATION: get(paramDBName, dbName, param, addr, localAddr, value, offset, n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > paramDBName - name of parameter db that has called this put
 * > dbName - name of parameter in backend database
 * > param - common name of parameter - may be same as dbName
 * > addr - pointer to DB addr structure 
 * > localAddr - pointer to local addr structure 
 * < value - value of parameter to get
 * > offset - offset added to addr to get desired starting address - default 0
 * > n - number of elements to put - default 1
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: gets TEST parameter  value
 * 
 * DESCRIPTION: uses addr address to get an TEST parameter value. 
 * Function is overloaded to support string, long and double variables
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, string* value, unsigned int offset, unsigned int n) 
{
  get_epics_param(dbName, param, addr, localAddr, value, offset, n);
}

void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, char* value, unsigned int offset, unsigned int n) 
{
  get_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, unsigned char* value, unsigned int offset, unsigned int n) 
{
  get_local_param(localAddr, value, offset, n);
}
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, short* value, unsigned int offset, unsigned int n) 
{
  get_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, unsigned short* value, unsigned int offset, unsigned int n) 
{
  get_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, int* value, unsigned int offset, unsigned int n) 
{
  get_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, unsigned int* value, unsigned int offset, unsigned int n) 
{
  get_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, long* value, unsigned int offset, unsigned int n) 
{
  get_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, unsigned long* value, unsigned int offset, unsigned int n) 
{
  get_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, float* value, unsigned int offset, unsigned int n) 
{
  get_local_param(localAddr, value, offset, n);
}
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, double* value, unsigned int offset, unsigned int n) 
{
  get_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
void EpicsParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, 
			   const void *localAddr, bool* value, unsigned int offset, unsigned int n) 
{
  get_epics_param(dbName, param, addr, localAddr, value, offset, n);
}
/*
 *+ 
 * FUNCTION NAME: publish(const void* addr) 
 * 
 * INVOCATION: index = publish(addr) 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: monitorability index of named parameter
 * 
 * PURPOSE: takes a named TEST parameter and returns -1
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: Does nothing
 *- 
 */

void EpicsParameterDB::publish(const void* addr)
{
}

void EpicsParameterDB::attach(const void* addr, const void* localAddr)
{
}

int EpicsParameterDB::add_callback(const void* addr, Parameter_Callback* callback)
{

  return -1;
}

void EpicsParameterDB::remove_callback(int cb_id)
{
}

void EpicsParameterDB::wait(const void* addr)
{
}

long EpicsParameterDB::dbNormalizedNameToAddr (std::string dbName, dbAddr *dba)
{
	size_t dot_pos = dbName.find('.');
	std::string normalized (dot_pos == std::string::npos ? dbName : dbName.substr(0, dot_pos));

	return dbNameToAddr(normalized.c_str(), dba);
}

/*
 *+ 
 * FUNCTION NAME: EpicsParameterDB::~EpicsParameterDB
 * 
 * INVOCATION: delete epicsParameterDB;
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: EpicsParameterDB destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
EpicsParameterDB::~EpicsParameterDB() 
{
}
//#ifdef CICADA_ONLY
/* Exclude this code now from the gemini port - it has been tested */
/*
 *+ 
 * FUNCTION NAME: epicsParameterTest
 * 
 * INVOCATION: epicsParameterTest()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Simple test harness for epics parameter DB
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
int epicsParameterTest()
{
  const char ELAPSED[] = "gnirs:dc:sad:elapsed";
  const char NPERIODS[] = "gnirs:dc:sad:obs:nperiods";
  const char FITSSERVER[] = "gnirs:dc:setDHSInfo.G";
  const char RDOUT[] = "gnirs:dc:sad:rdout";

  ParameterData pData;
  long nperiods = 0, lValid[2] = {0,100};
  long elapsed = 0;  
  double dValid[2] = {-5.0, 5.0};
  double read_time = 0.0; 
  bool rdout = false;
  char ans[2];
  string fitsServer = "maestro"; string sValid[2] = {"maestro", "mizar"};
  string fs2;

  try {
    ParameterDB *dcParDB = new EpicsParameterDB();
    Parameter *dcPar = new Parameter(dcParDB, "dcPar", NULL);
    ParameterDB *dcStatusDB = new EpicsParameterDB();
    Parameter *dcStatus = new Parameter(dcStatusDB, "dcStatus", NULL);   
    
    cout << "Adding nperiods (long), default value=0, range=0..100:" << endl;
    dcStatus->add("nperiods", NPERIODS, (void*)&nperiods, true, 1, &nperiods, VALIDITY_RANGE, 2, lValid, UNPUBLISHED);
    cout << "Done ...";

    cin.getline(ans,2);
    cout << "Adding fitsServer (char string), default value='maestro', len=40:" << endl;  
    dcPar->add("fitsServer", FITSSERVER, (void*)&fitsServer, true, 1, &fitsServer, VALIDITY_SET, 2, sValid, UNPUBLISHED);
    cout << "Done ...";
    cin.getline(ans,2);

    cout << "Adding elapsed (long), default value=0, range=0..MAXL:" << endl;  
    lValid[1] = MAXI;
    dcStatus->add("elapsed", ELAPSED, (void*)&elapsed, true, 1, &elapsed, VALIDITY_RANGE, 2, lValid, UNPUBLISHED);
    cout << "Done ...";
    cin.getline(ans,2);

    cout << "Adding read_time (double/not in EPICS DB), default value=0, range=0..MAXD:" << endl;  
    dValid[0] = 0;
    dValid[1] = MAXD;
    dcStatus->add("read_time", NULL, (void*)&read_time, true, 1, &read_time, VALIDITY_RANGE, 2, dValid, UNPUBLISHED);
    cout << "Done ...";
    cin.getline(ans,2);

    cout << "Adding rdout (long(bool)), default value=false:" << endl;  
    dcStatus->add("rdout", RDOUT, (void*)&rdout, true, 1, &rdout, VALIDITY_NONE, 0, (bool*)NULL, UNPUBLISHED);
    cout << "Done ...";
    cin.getline(ans,2);

    cout << "dcStatus parameter list:" << endl;
    dcStatus->list();
    cout << endl;

    cout << "dcPar parameter list:" << endl;
    dcPar->list();
    cout << endl;
    cin.getline(ans,2);
    
    cout << "Enter value to place in nperiods:";
    cin >> nperiods;
    
    dcStatus->put("nperiods", nperiods);
    cout << "Done ...";
    cin.getline(ans,2);
    
    cout << "Getting value of nperiods..." << endl;
    dcStatus->get("nperiods", nperiods);
    cout << "Done, nperiods=" << nperiods << endl;
    cin.getline(ans,2);
    
    cout << "Enter value to place in fitsServer:";
    cin >> fs2;
    
    dcPar->put("fitsServer", fs2);
    cout << "Done ...";
    cin.getline(ans,2);
    
    cout << "Getting value of fitsServer..." << endl;
    dcPar->get("fitsServer", fs2);
    cout << "Done, fitsServer=" << fs2 << endl;
    cin.getline(ans,2);
    
    cout << "Enter value to place in read_time:";
    cin >> read_time;
    
    dcStatus->put("read_time", read_time);
    cout << "Done ...";
    cin.getline(ans,2);
    
    cout << "Getting value of read_time..." << endl;
    dcStatus->get("read_time", read_time);
    cout << "Done, read_time=" << read_time << endl;
    cin.getline(ans,2);


    cout << "Enter value to place in rdout:";
    cin >> rdout;
    
    dcStatus->put("rdout", rdout);
    cout << "Done ...";
    cin.getline(ans,2);
    
    cout << "Getting value of rdout..." << endl;
    dcStatus->get("rdout", rdout);
    cout << "Done, rdout=" << rdout << endl;
    cin.getline(ans,2);

    cout << "All done." << endl;
    
    delete dcStatus;   
    delete dcStatusDB;
    delete dcPar;
    delete dcParDB;
  }
  catch (Error& de) {
    de.print_error(__FILE__,__LINE__);
  }
  return 0;
}
//#endif
