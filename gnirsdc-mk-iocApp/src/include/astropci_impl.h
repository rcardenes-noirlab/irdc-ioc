/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: astropci_impl.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GSAOI PROJECT
 * 
 * FILENAME astropci_impl.h
 * 
 * GENERAL DESCRIPTION 
 *
 * This is a VxWorks version of the device driver to communicate with
 * the SDSU PCI interface board (PMC format). 

 * This file contains private implementation details for VxWorks device driver for
 * the SDSU PCI Interface Card. This is based on the Solaris version of this file.

 * Original Author: Scott Streit (SDSU) - Solaris version 1.0 - 12 Nov 1999
 * Ported by: Peter Young (RSAA) - For the Gemini Telescope Project (GSAOI) - December 2003
 *
 * $Log: astropci_impl.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.5  2005/07/30 01:42:49  pjy
 * Simulate RSW
 *
 * Revision 1.4  2005/07/24 11:57:19  pjy
 * Added simulation addresses for amp and read inc
 *
 * Revision 1.3  2005/07/12 06:28:47  pjy
 * Added last_retries and last_disconnects
 *
 * Revision 1.2  2005/04/27 03:40:28  pjy
 * PCI DSP working
 *
 * Revision 1.1  2005/02/25 04:47:47  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.2  2004/03/04 03:26:18  pjy
 * Astro pci port work
 *
 * Revision 1.1  2004/02/16 22:10:08  pjy
 * Initial commit of vxWorks astropci device driver code.
 * This code successfully performs the basic SDSU operations exercised by the test code (ie reset,download(PCI,TIM),readout(RDC) etc).
 *
 *
 *  
 */
#ifndef __ASTROPCI_IMPL_H__
#define __ASTROPCI_IMPL_H__

/*
 * Define the Vendor and Device IDs string stored in the PROM
 */
static const int SDSU_PCI_VENDOR_ID = 0x1057; 
static const int SDSU_PCI_DEVICE_ID = 0x1801;

/* 
 * Setup and timeout constants 
 */
static const int SDSU_INTERRUPT_LEVEL = 7;          /* PCI bus interrupt level (set by card jumper)*/
static const int REPLY_MASK = 0x00ffffff;           /* only look at low 24 bits of reply words */
static const int SDSU_TEST_PATTERN_BIT = 0x2;
static const int SDSU_TEST_PATTERN_CONST_BIT = 0x4; /* SDSU test pattern constant or incrementing */ 
static const int SDSU_TEST_PATTERN_SOURCE_BIT = 0x8;
static const int SDSU_TEST_PATTERN_OS_PAR = 0xB;
static const int SDSU_TEST_PATTERN_INC_PAR = 0xC;   /* Y memory location for test pattern inc value */
static const int SDSU_TEST_PATTERN_SEED_PAR = 0xD;  /* Y memory location for test pattern seed value */
static const int SDSU_TEST_PATTERN_AMP_INC_PAR = 0xE;/* amp increment for test patterns */
static const int SDSU_TEST_PATTERN_READ_INC_PAR = 0xF;/* read inc for test patterns */

/* 
 * Make following definition #define so that it can be used in array
 * dimensioning 
 */
#define SDSU_REPLY_BUF_LEN 32                       /* length of reply ring buffer */

/*
 * Some Commands interpreted by the driver 
 */
static const int SDSU_PCI_ID = 0x1;          /* PCI board ID */
static const int SDSU_TIM_ID = 0x2;          /* Timing board ID */

/*
 * SDSU MEM space
 */
static const int P_MASK = 0x00100000;
static const int X_MASK = 0x00200000;
static const int Y_MASK = 0x00400000;
static const int R_MASK = 0x00800000;
static const int SDSU_MEM_P = 0x0;           /* P memory space */
static const int SDSU_MEM_X = 0x1;           /* X memory space */
static const int SDSU_MEM_Y = 0x2;           /* Y memory space */
#define N_BOARDS  4                          /* interface,timing,utility,coadder*/
#define N_MEMS 3                             /* P,X,Y */
#define MAX_MEM 0x3fff                       /* 16k words */

/*
 * PCI error memory locations - X memory
 */
static const int PCI_ERRS  = 0x00003A;      /* Number of PCI errors*/
static const int TO_ERRS   = 0x00003B;      /* Time Out error counter*/
static const int TR_ERRS   = 0x00003C;      /* Target Retry error counter */
static const int TD_ERRS   = 0x00003D;      /* Target Disconnect error counter*/
static const int TA_ERRS   = 0x00003E;      /* Target Abort error counter*/
static const int MA_ERRS   = 0x00003F;      /* Master Abort error counter*/
static const int DP_ERRS   = 0x000040;      /* Data Parity error counter*/
static const int AP_ERRS   = 0x000041;      /* Adsress Parity error counter*/

/*
 * SDSU SDN commands
 */
#define SDSU_ABR 0x414252        /* ABR - Abort */
#define SDSU_CAT 0x434154	 /* CAT - Test Coadder data link */ 
#define SDSU_CDS 0x434453	 /* CDS - Correlated Double Sampling */ 
#define SDSU_CLR 0x434C52        /* CLR - Clear detector */ 
#define SDSU_CON 0x434F4E	 /* CON - Power On (same as PON) */ 
#define SDSU_CRT 0x435254        /* CRT - Calibrate frame read time */ 
#define SDSU_DCA 0x444341        /* DCA - Load coadder from Timing SRAM */
#define SDSU_DFS 0x444653        /* DFS - Set digital filter samples */
#define SDSU_DON 0x444F4E        /* DON - Done */
#define SDSU_ERR 0x455252        /* ERR - system error */
#define SDSU_FBA 0x464241        /* FBA - Set frame buffer address*/
#define SDSU_GPE 0x475045        /* GPE - Get PCI error count */
#define SDSU_GP1 0x475031        /* GP1 - Get PCI timeout error count */
#define SDSU_GP2 0x475032        /* GP2 - Get PCI target retry error count */
#define SDSU_GP3 0x475033        /* GP3 - Get PCI target disconnect error count */
#define SDSU_GP4 0x475034        /* GP4 - Get PCI target abort error count */
#define SDSU_GP5 0x475035        /* GP5 - Get PCI master abort error count */
#define SDSU_GP6 0x475036        /* GP6 - Get PCI data parity error count */
#define SDSU_GP7 0x475037        /* GP7 - Get PCI address parity error count */
#define SDSU_ICA 0x494341        /* ICA - Initialise coadder */
#define SDSU_IDL 0x49444C        /* IDL - Start system idle mode */
#define SDSU_LCA 0x4C4341        /* LCA - Load coadder from Timing ROM */
#define SDSU_NCL 0x4E434C        /* NCL - Set number of image cols */
#define SDSU_NRW 0x4E5257        /* NRW - Set number of image rows */
#define SDSU_LDA 0x4C4441        /* LDA - Load Timing app */
#define SDSU_PCA 0x504341        /* PCA - Set pixel count address*/
#define SDSU_POF 0x504F46	 /* POF - Power Off	*/ 
#define SDSU_PON 0x504F4E	 /* PON - Power On	*/ 
#define SDSU_PRM 0x50524D        /* PRM - Set readout parameters */
#define SDSU_RCC 0x524343        /* RCC - read controller config */
#define SDSU_RDC 0x524443        /* RDC - run readout command */
#define SDSU_RET 0x524554	 /* RET - Get Exposure Time	*/ 
#define SDSU_RDM 0x52444D        /* RDM - read DSP memory location */
#define SDSU_ROP 0x524F50        /* ROP - Read options word */
#define SDSU_RRS 0x525253        /* RRS - Remote reset to timing board */
#define SDSU_RRT 0x525254        /* RRT - Get regions table size */
#define SDSU_RSW 0x525357        /* RSW - Read status word */
#define SDSU_SBN 0x53424E        /* SBN - set bias number */
#define SDSU_SBS 0x534253        /* SBS - set byte swapping*/
#define SDSU_SBV 0x534256        /* SBV - set bias supply voltages */
#define SDSU_SCA 0x534341        /* SCA - setup coadder */
#define SDSU_SET 0x534554	 /* SET - Set Exposure Time	*/ 
#define SDSU_SEX 0x534558        /* SEX - Start exposure */
#define SDSU_SFS 0x534653	 /* SFS - Send Fowler Samples	*/ 
#define SDSU_SMX 0x534D58        /* SMX - Set MUX */
#define SDSU_SNR 0x534E52        /* SNR - Set reset count */
#define SDSU_SOS 0x534F53        /* SOS - S */
#define SDSU_SRD 0x535244        /* SRD - Set reset delay */
#define SDSU_SRF 0x535246        /* SRF - Set number of reference samples */
#define SDSU_SRD 0x535244        /* SRD - Set reset delay */
#define SDSU_SRM 0x53524D        /* SRM - Select reset mode */
#define SDSU_STP 0x535450        /* STP - Stop idle mode */
#define SDSU_STX 0x535458        /* STX - Stop exposure */
#define SDSU_SUR 0x535552        /* SUR - Set Up The Ramp Mode */ 
#define SDSU_SYR 0x535952        /* SYR - system reset */
#define SDSU_TBS 0x544253        /* TBS - Test byte swap */
#define SDSU_TDL 0x54444C        /* TDL - run readout command */
#define SDSU_WOP 0x574F50        /* WOP - write options word */
#define SDSU_WRM 0x57524D        /* WRM - write DSP memory location */
#define SDSU_ZPE 0x595045        /* ZPE - Zero PCI error count */

#define SDSU_PON_BIT 0x400000;   /* Power on bit in status word */

/*
 * DSP Replies
 */
#define DON	 0x00444F4E
#define RDR	 0x00524452
#define ERR	 0x00455252
#define SYR	 0x00535952
#define TIMEOUT	 0x544F5554 /* TOUT */
#define READOUT	 0x524F5554 /* ROUT */


/*
 * DSP/PCI Control Registers
 */
typedef struct {
  volatile uint32_t reserved1;          /* DSP Reserved register */
  volatile uint32_t reserved2;	        /* DSP Reserved register */
  volatile uint32_t reserved3;	        /* DSP Reserved register */
  volatile uint32_t reserved4;	        /* DSP Reserved register */
  volatile uint32_t hctr;	        /* 0x10 Host interface control register on DSP */
  volatile uint32_t hstr;	        /* 0x14 Host interface status register on DSP */
  volatile uint32_t hcvr;	        /* 0x18 Host command vector register on DSP */
  volatile uint32_t reply_buffer;       /* 0x1C Command reply buffer */
  volatile uint32_t cmd_data;	        /* 0x20 DSP command data register */
  volatile uint32_t unused1;	        /* 0x24 Unused */
  volatile uint32_t unused2;	        /* 0x28 Unused */
  volatile uint32_t unused3;	        /* 0x2C Unused */
  volatile uint32_t unused4;	        /* 0x30 Unused */
  volatile uint32_t unused5;	        /* 0x34 Unused */
  volatile uint32_t unused6;	        /* 0x38 Unused */
  volatile uint32_t unused7;	        /* 0x3C Unused */
  volatile uint32_t unused8;  	        /* 0x40 Unused */
  volatile uint32_t unused9;	        /* 0x44 Unused */
} Astropci_Regs ;

enum Astropci_Register_Type {
  HCTR,
  HSTR,
  HCVR,
  REPLY,
  CMD
};
typedef enum Astropci_Register_Type Astropci_Register_Type;

/*
 * Astropci_State - Driver state structure.  All state variables related
 * to the state of the driver are kept in here.
 */
typedef struct {
  DEV_HDR header;                       /* VxWorks driver table structure */
  
  Astropci_Regs *control_registers;     /* DSP/PCI control registers */
  
  /* operation flags */
  int open;		                /* Flag set when driver opened              */
  int busy;		                /* Set when transfers in progress           */
  int intr_busy;		        /* Set when interrupt service in progress   */
  int ioctl_busy;                       /* set when an ioctl operation in progress */
  int read_busy;                        /* set when a read is running */
  int rdc_sent;                         /* set when and RDC command has been trapped */
  int stop;                             /* TRUE if STOP ioctl received */
  int abort;                            /* TRUE if ABORT ioctl received */
  int simulate;                         /* TRUE if in simulation mode */
  int sim_dsp_mem[N_BOARDS][N_MEMS][MAX_MEM];/* simulate DSP memory */
  int timeout_mult;                     /* standard timeout multiplier */
 
  /* Current DMA address */
  uint32_t image_address;               /* Current image DMA address */

  /* Readout setup information */
  uint32_t nreads;                      /* Number of reads to do in multiple IR read*/
  uint32_t nsamples;                    /* Number of samples to do in multiple IR read*/
  uint32_t namps;                       /* Number of readout amps */
  uint32_t readouts_done;               /* readouts done so far in multiple IR read */
  uint32_t read_running;                /* number of the current read running */
  uint32_t read_wait;                   /* length of time to wait for first read msecs*/
  uint32_t read_interval;               /* length of time between succesive IR readouts msecs*/
  uint32_t nresets;                     /* Number of resets to perform */
  uint32_t reset_delay;                 /* length of time to delay before expecting first data after a reset msec*/
  uint32_t buffer_size;                 /* size of cyclic output buffer in bytes - at least 2ximage_size*/
  uint32_t image_size;                  /* size of one image frame */
  uint32_t exptime;                     /* exposure time set with SET command */
  uint32_t read_time;                   /* Time expected to read one frame - msec */
  uint64_t readout_time;                /* Time actually required for a readout usec */
  uint32_t pixel_counter;               /* pixel counter */
  uint32_t last_pixel_counter;          /* last value of pixel counter */
  uint32_t last_retries;                /* last value of retry counter */
  uint32_t last_disconnects;            /* last value of disconnect counter */

  /* VxWorks device information */
  uint16_t vendor;
  int device;
  int devFn;
  uint8_t devno;   
  int bus;
  int instance;                         /* pci board instance number */
  int devAdd;                           /* set when iosDevAdd successful */

  /* interrupt control info*/
  int isr_tid;                          /* interrupt handling task */
  int intr_line;                        /* interrupt line       */
  int intr_count;                       /* coun of number of interrupts received */
  int sdsu_int_enable;                  /* interrupts enabled flag */
   
  /* Thread locking and synchronization */
  SEM_ID semAccess;                     /* Controls access to this structure */
  SEM_ID semIntr;                       /* Controls interrupt handling sync */
} Astropci_State;


/* Function prototype used for checking status register */
typedef int (*ASTROPCI_STATUS_FUNC)(uint32_t*);

/* Interrupt service routine type decl */
/*  typedef void (*Astropci_ISR)(int, int); */

#endif

