/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * CICADA PROJECT
 * 
 * FILENAME testParameterDB.cc
 * 
 * GENERAL DESCRIPTION
 * Implements methods for access to a test DB from
 * the TestParameterDB interface.
 *
 * REQUIREMENTS
 * Requires use of STL class "string"
 *
 */
#include "testParameterDB.h"
#include "utility.h"
#include "exception.h"

using namespace std;

/*
 *+ 
 * FUNCTION NAME: TestParameterDB::TestParameterDB
 * 
 * INVOCATION: testParameterDB = new TestParameterDB();
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > dbh - hostname of home host for database
 * > ba - base address of status
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: TestParameterDB constructor
 * 
 * DESCRIPTION: Constructor for a simple memory parameter database.
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
TestParameterDB::TestParameterDB() 
{
}

/*
 *+ 
 * FUNCTION NAME: publish(const void* addr) 
 * 
 * INVOCATION: index = publish(addr) 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: monitorability index of named parameter
 * 
 * PURPOSE: takes a named TEST parameter and returns -1
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: Does nothing
 *- 
 */

void TestParameterDB::publish(const void* addr)
{
}

void TestParameterDB::attach(const void* addr, const void* localAddr)
{
}

int TestParameterDB::add_callback(const void* addr, Parameter_Callback* callback)
{

  return -1;
}

void TestParameterDB::remove_callback(int cb_id)
{
}

void TestParameterDB::wait(const void* addr)
{
}

/*
 *+ 
 * FUNCTION NAME: setAddr(char* dbName) 
 * 
 * INVOCATION: addr = setAddr(dbName) 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: address of named parameter
 * 
 * PURPOSE: takes a named TEST parameter and returns its address
 * 
 * DESCRIPTION: Uses TEST function dbNameToAddr to get pointer
 * to addr structure for named TEST variable
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void* TestParameterDB::setAddr(const char* dbName, const char* param, const char* prefix, const char* suffix) 
{

  // TEST does not support a name to address function - yet
  return((void*)NULL);
}
/*
 *+ 
 * FUNCTION NAME: freeAddr(void *addr) 
 * 
 * INVOCATION: freeAddr(addr) 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > addr - pointer to addr structure to free
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: frees memory associated with addr structure
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void TestParameterDB::freeAddr(const void *addr) 
{
}
/*
 *+ 
 * FUNCTION NAME: put(const char* dbName, const char* param, oid *addr, char*|long|double value, unsigned int offset, 
 *                    unsigned int n)  
 * 
 * INVOCATION: put(dbName, param, addr, value, offset, n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > dbName - name of parameter db that has called this put
 * > param - name of parameter
 * > addr - pointer to addr structure 
 * > localAddr - pointer to local structure 
 * > value - value of parameter to set
 * > offset - offset added to addr to get desired starting address - default 0
 * > n - number of elements to put - default 1
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: sets TEST parameter to value
 * 
 * DESCRIPTION: uses addr address to set an TEST parameter to 
 * particular value. Function is overloaded to support string, long
 * and double variables
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr,
			  const string* value, unsigned int offset, unsigned int n)
{
  ostringstream ostr;
  unsigned int i;
  string* saddr = (string*) localAddr+offset;
  
  if (value == NULL) {
    ostr << "TestParameterPut: Cannot put NULL value for parameter '" << param << "' in database '" << paramDBName << "'!" << ends; 
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }

  // Copy directly in memory
  for (i=0; i<n; i++) {
    *saddr++ = value[i];
  }
}

void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param,const void *addr, const void* localAddr,
			  const char *value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (char*)value, offset, n);
}
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void* localAddr,
			  const unsigned char *value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (unsigned char*)value, offset, n);
}
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void* localAddr,
			  const short *value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (short*)value, offset, n);
}
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param,const void *addr, const void* localAddr,
			  const unsigned short *value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (unsigned short*)value, offset, n);
}
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param,const void *addr, const void* localAddr,
			  const int *value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (int*)value, offset, n);
}
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param,const void *addr, const void* localAddr,
			  const unsigned int *value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (unsigned int*)value, offset, n);
}
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void* localAddr, 
			  const long *value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (long*)value, offset, n);
}
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void* localAddr, 
			  const unsigned long *value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (unsigned long*)value, offset, n);
}
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void* localAddr, 
			  const float* value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (float*)value, offset, n);
}
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void* localAddr, 
			  const double* value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (double*)value, offset, n);
}
void TestParameterDB::put(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void* localAddr, 
			  const bool* value, unsigned int offset, unsigned int n) 
{
  put_test_param(paramDBName, param, addr, localAddr, (double*)value, offset, n);
}

/*
 *+ 
 * FUNCTION NAME: get(const char* dbName, const char* param, void *addr, void *localAddr, char*|long*|double* &value, 
 *                    int offset, int n)  
 * 
 * INVOCATION: get(paramDBName, param, addr, localAddr, value, offset, n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > dbName - name of parameter db that has called this get
 * > param - name of parameter
 * > addr - pointer to DB addr structure 
 * > localAddr - pointer to local addr structure 
 * < value - value of parameter to get
 * > offset - offset added to addr to get desired starting address - default 0
 * > n - number of elements to put - default 1
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: gets TEST parameter  value
 * 
 * DESCRIPTION: uses addr address to get an TEST parameter value. 
 * Function is overloaded to support string, long and double variables
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr, string* value, 
			  unsigned int offset, unsigned int n) 
{
  ostringstream ostr;
  unsigned int i;
  string* saddr = (string*) localAddr+offset;

  if (value == NULL) {
    ostr << "TestParameterGet: Must allocate enough memory to receive parameter '" << paramDBName << "." << param << "'!" << ends; 
    throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);    
  }
  // Now copy the string data from data memory
  for (i=0; i<n; i++) {
    value[i] = *saddr++;
  }
}

void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void* localAddr,
			  char* value, unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr,
			  unsigned char* value, unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr,
			  short* value, unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr,
			  unsigned short* value,unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr,
			  int* value, unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr,
			  unsigned int* value, unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr,
			  long* value, unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr,
			  unsigned long* value, unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr,
			  float* value, unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr,
			  double* value, unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}
void TestParameterDB::get(const char* paramDBName, const char* dbName, const char* param, const void *addr, const void *localAddr,
			  bool* value, unsigned int offset, unsigned int n) 
{
  get_test_param(paramDBName, param, addr, localAddr, value, offset, n);
}

/*
 *+ 
 * FUNCTION NAME: template <class T> put_test_param(const char* paramDBName, const char* param, const void *addr, void *localAddr,  
 *                                                  T* value, unsigned int offset, unsigned int n) 
 * 
 * INVOCATION: put_test_param(param, addr, localAddr, value, offset, n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > paramDBName - name of parameter db that has called this put
 * > param - name of parameter
 * > addr - pointer to addr structure 
 * > localAddr - pointer to local structure 
 * > value - value of parameter to set
 * > offset - offset added to addr to get desired starting address - default 0
 * > n - number of elements to put - default 1
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: sets test parameter to value
 * 
 * DESCRIPTION: uses addr address to set an Test parameter to 
 * particular value. This is the template function used by indivdual put methods
 * from TestparameterDB class
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
template <class T> void TestParameterDB::put_test_param(const char* paramDBName, const char* param, const void *addr, 
							const void* localAddr, T* value, unsigned int offset, unsigned int n)
{
  ostringstream ostr;
  T* iaddr = (T *) localAddr;
  
  if (value == NULL) {
    ostr << "TestParameterGet: Must allocate enough memory to receive parameter '" << paramDBName << "." << param << 
      "'!" << ends;
    throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);    
  }
  
  // Copy directly in memory
  memcpy(iaddr+offset, value, n * sizeof(T));
}

/*
 *+ 
 * FUNCTION NAME: template <class T> void get_test_param(const char* paramDBName, const char* param, void *addr, void *localAddr, 
 *                                                       T* value, int offset, int n)  
 * 
 * INVOCATION: get(const char* paramDBName, param, addr, localAddr, value, offset, n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > paramDBName - name of parameter db that has called this get
 * > param - name of parameter
 * > addr - pointer to addr structure 
 * > localAddr - pointer to local structure 
 * < value - value of parameter to get
 * > offset - offset added to addr to get desired starting address - default 0
 * > n - number of elements to put - default 1
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: gets TEST parameter  value
 * 
 * DESCRIPTION: uses addr address to get an TEST parameter value. 
 * This is a template function to support simple data types
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
template <class T> void TestParameterDB::get_test_param(const char* paramDBName, const char* param, const void *addr, 
							const void *localAddr,  T* value, unsigned int offset, unsigned int n)
{
  ostringstream ostr;
  T* iaddr = (T *) localAddr + offset;
  
  if (value == NULL) {
    ostr << "TestParameterGet: Must allocate enough memory to receive parameter '" << paramDBName << "." << param << 
      "'!" << ends;
    throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);    
  }
  memcpy(value, iaddr, n * sizeof(T));
}
/*
 *+
 * FUNCTION NAME: TestParameterDB::~TestParameterDB
 * 
 * INVOCATION: delete testParameterDB;
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: TestParameterDB destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
TestParameterDB::~TestParameterDB() 
{
}

