[schematic2]
uniq 14
[tools]
[detail]
w -302 1195 100 0 n#1 estringouts.DSPCmd.FLNK -304 1184 -240 1184 junction
w -302 1387 100 0 n#1 estringouts.DSPBootCmd.FLNK -304 1376 -240 1376 -240 640 -176 640 egenSub.DSPCmdAscii.SLNK
w -318 1163 100 0 n#2 estringouts.DSPCmd.OUT -304 1152 -272 1152 -272 1344 junction
w -270 1355 100 0 n#2 estringouts.DSPBootCmd.OUT -304 1344 -176 1344 egenSub.DSPCmdAscii.A
w 986 1259 100 0 n#3 ecad8.DSPDirectCmd.OUTF 896 1248 1136 1248 hwout.hwout#540.outp
w 378 1547 100 0 n#4 egenSub.DSPCmdAscii.OUTA 112 1312 240 1312 240 1536 576 1536 ecad8.DSPDirectCmd.B
w 986 1323 100 0 n#5 ecad8.DSPDirectCmd.OUTE 896 1312 1136 1312 hwout.hwout#515.outp
w 986 1387 100 0 n#6 ecad8.DSPDirectCmd.OUTD 896 1376 1136 1376 hwout.hwout#514.outp
w 986 1451 100 0 n#7 ecad8.DSPDirectCmd.OUTC 896 1440 1136 1440 hwout.hwout#513.outp
w 986 1515 100 0 n#8 ecad8.DSPDirectCmd.OUTB 896 1504 1136 1504 hwout.hwout#512.outp
w 986 1579 100 0 n#9 ecad8.DSPDirectCmd.OUTA 896 1568 1136 1568 hwout.hwout#510.outp
w 250 1643 100 0 n#10 embbos.DSPBoardIn.OUT 80 1632 480 1632 480 1600 576 1600 ecad8.DSPDirectCmd.A
w 250 347 100 0 n#11 embbos.DSPMemSpaceIn.OUT 80 336 480 336 480 1472 576 1472 ecad8.DSPDirectCmd.C
w 1026 1771 100 0 MESS ecad8.DSPDirectCmd.MESS 896 1760 1216 1760 outhier.MESS.p
w 1134 875 100 0 VAL cadPlus.DSPDirectCmdP.STAT 1200 864 1104 864 1104 1792 junction
w 1066 1803 100 0 VAL ecad8.DSPDirectCmd.VAL 896 1792 1296 1792 outhier.VAL.p
w 370 1771 100 0 CLID inhier.CLID.P 224 1760 576 1760 ecad8.DSPDirectCmd.ICID
w 314 1803 100 0 DIR inhier.DIR.P 112 1792 576 1792 ecad8.DSPDirectCmd.DIR
w 1018 907 100 0 n#12 ecad8.DSPDirectCmd.SPLK 896 896 1200 896 cadPlus.DSPDirectCmdP.SPIN
w 1018 939 100 0 n#13 cadPlus.DSPDirectCmdP.STIN 1200 928 896 928 ecad8.DSPDirectCmd.STLK
s 368 1280 100 0 arg2
s 368 1552 100 0 command
s 368 1472 100 0 memSpace
s 368 1408 100 0 address
s 368 1344 100 0 arg1
s 368 1600 100 0 board
[cell use]
use hwout 1136 1207 100 0 hwout#540
xform 0 1232 1248
p 1232 1239 100 0 -1 val(outp):$(sadtop)SDSUarg2.VAL PP MS
use hwout 1136 1271 100 0 hwout#515
xform 0 1232 1312
p 1232 1303 100 0 -1 val(outp):$(sadtop)SDSUarg1.VAL PP MS
use hwout 1136 1335 100 0 hwout#514
xform 0 1232 1376
p 1232 1367 100 0 -1 val(outp):$(sadtop)SDSUaddress.VAL PP MS
use hwout 1136 1399 100 0 hwout#513
xform 0 1232 1440
p 1232 1431 100 0 -1 val(outp):$(sadtop)SDSUmemSpace.VAL PP MS
use hwout 1136 1463 100 0 hwout#512
xform 0 1232 1504
p 1232 1495 100 0 -1 val(outp):$(sadtop)SDSUcommand.VAL PP MS
use hwout 1136 1527 100 0 hwout#510
xform 0 1232 1568
p 1232 1559 100 0 -1 val(outp):$(sadtop)SDSUboard.VAL PP MS
use estringouts -496 1424 100 0 DSPBootCmd
xform 0 -432 1360
p -533 1400 100 0 0 DESC:DSP Boot Command
p -624 1326 100 0 0 VAL:
use estringouts -496 1232 100 0 DSPCmd
xform 0 -432 1168
p -533 1208 100 0 0 DESC:Arbitrary DSP Command
p -624 1134 100 0 0 VAL:
use embbos -112 416 100 0 DSPMemSpaceIn
xform 0 -48 336
p -112 208 100 0 1 ONST:X
p -112 176 100 0 1 TWST:Y
p -112 240 100 0 1 ZRST:P
use embbos -96 1712 100 0 DSPBoardIn
xform 0 -48 1632
p -96 1440 100 0 0 FRST:
p -96 1536 100 0 1 ONST:PCI
p -96 1472 100 0 0 THST:
p -96 1504 100 0 1 TWST:TIMING
p -16 1726 100 0 0 ZRST:HOST
use egenSub -176 551 100 0 DSPCmdAscii
xform 0 -32 976
p -333 1291 100 0 0 DESC:Convert an SDSU DSP cmd., char to ascii
p -464 990 100 0 0 EFLG:ON CHANGE
p -80 1328 100 0 1 FTA:STRING
p -80 1296 100 0 1 FTVA:LONG
p -112 512 100 0 1 SNAM:dcDSPCmdAscii
p -592 1296 100 0 0 def(INPA):0.000000000000000e+00
p -379 -335 100 0 1 name:$(top)$(I)
use cadPlus 1200 976 100 0 DSPDirectCmdP
xform 0 1280 880
p 1216 752 100 0 1 set1:cad DSPDirectCmdP
use ecad8 672 1840 100 0 DSPDirectCmd
xform 0 736 1344
p 672 1696 100 0 0 DESC:DSP direct command CAD
p 672 1536 100 3072 1 FTVA:LONG
p 672 1504 100 3072 1 FTVB:LONG
p 672 1472 100 3072 1 FTVC:LONG
p 672 1440 100 3072 1 FTVD:LONG
p 672 1408 100 3072 1 FTVE:LONG
p 672 1360 100 0 1 FTVF:LONG
p 672 832 100 0 1 SNAM:dcCADDSPCommand
use inhier 208 1719 100 0 CLID
xform 0 224 1760
use inhier 96 1751 100 0 DIR
xform 0 112 1792
use outhier 1264 1751 100 0 VAL
xform 0 1280 1792
use outhier 1184 1719 100 0 MESS
xform 0 1200 1760
use eborderC -784 -313 100 0 eborderC#87
xform 0 896 992
p 1792 -160 100 768 -1 author:Mark Jarnyk
p 1792 -192 100 768 -1 date:2001-10-05
p 2016 -112 200 768 -1 file:dcSDSUDSPBoard
p 2288 -160 100 0 -1 page:1
p 2400 -160 100 0 -1 pages:1
p 2064 -160 100 0 -1 revision:0
p 2016 -48 150 768 -1 system:Gemini GNIRS DC
[comments]
