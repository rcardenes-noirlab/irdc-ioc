/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: gnirs_dc_status.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GNIRS PROJECT
 *
 * FILENAME 
 * 
 * PURPOSE
 *
 * $Log: gnirs_dc_status.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.1  2004/12/22 22:43:59  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.1  2001/10/19 00:24:48  pjy
 * Added for GNIRS DC
 *
 *
 */

#ifndef GNIRS_DC_PARAMETER
#define GNIRS_DC_PARAMETER

#include <vxworks.h>
#include <semLib.h>
#include <hash_map.h>
#include <dbAccess.h>
#include <string>

// put definitions here 
const int MAXSTRLEN = 2048;

typedef enum ValidityType ValidityType;
enum validityType {
  VALIDITY_RANGE = 0,
  VALIDITY_SET = 1
};

struct ParameterData {
  string name;
  short type;
  ValidityType vType;
  string epicsName;
  struct dbAddr epicsAddr;

  // Value - one of
  long lValue;
  double dValue;
  string sValue;

  // validity range or validity set
  int nValid; 
  long *lValid;
  double *dValid;
  string *sValid;
};

// DC parameters - these are set by DC processes
class DC_Parameter {
 private:
  SEM_ID semAccess;
  hash_map<const string, ParameterData> paramDict;

 public:

  // Constructor
  DC_Parameter();

  // Reset method - resets status pars to default
  reset();

  // Parameter add methods - overload to support 3 gemini types
  void add(const string param, const string epicsName, const string defValue, ValidityType vType,
	   int n, string *sValid);
  void add(const string param, const string epicsName, long defValue, ValidityType vType,
	   int n, long *lValid);
  void add(const string param, const string epicsName, double defValue, ValidityType vType,
	   int n, double *dValid);

  // Parameter set methods - overloaded to support 3 Gemini types
  void set(const string param, const string value);
  void set(const string param, long value);
  void set(const string param, double value);

  // Parameter get methods - overloaded to support 3 Gemini types
  string get(const string param);
  long get(const string param);
  double get(const string param);

  // Clear method - use to clear param flag once read
  void clear(const string param);

  // Destructor
  ~DC_Parameter();
};

#endif /* GNIRS_DC_PARAMETER */
