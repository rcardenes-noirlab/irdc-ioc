/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 * CICADA PROJECT
 *
 * FILENAME detector_regions.h
 * 
 * PURPOSE
 * Definition of RSAA's detector region class.
 * This class provides a mechanism for handling detector region specifications 
 * and their relationship with detector readout amplifiers on multi cip/amp
 * detectors. The arbitrary specification of multiple readout regions are 
 * supported though artificially limited by a maximum number of regions.
 *
 */
#ifndef _CICADA_REGIONS_H
#define _CICADA_REGIONS_H

// Include files 
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <vector>
#include <map>
#include "configuration.h"
#include "regions.h"
#include "utility.h"

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/
const unsigned long long FULL_MASK = ULONG_LONG_MAX;

/*--------------------------------------------------------------------------
 Typedefs 
 -------------------------------------------------------------------------*/

//+ output structure - a serial region, ie oriented along pixel readout axis
struct Ser_Regions {
  unsigned long xo;                     //+ absolute start of serial reg in amp coords
  unsigned long xo_b;                   //+ start of binned serial reg in amp coords
  unsigned long skip;                   //+ num pix since last ser_reg
  unsigned long width;                  //+ width of this ser_reg
  unsigned long width_b;                //+ binned width of this ser_reg
  unsigned long long amp_mask;          //+ amp which this ser_reg belongs to
  unsigned long long region_mask;       //+ user_region which this ser_reg belongs to
  unsigned long long *framelist;        //+ for each amp what frames are valid
  unsigned long long *subframelist;     //+ for each amp what subframes are valid
  unsigned long long *regionlist;       //+ for each amp what regions are valid
};

//+ output structure - a parallel region, ie oriented along row readout axis
struct Par_Regions {
  unsigned long yo;                     //+ absolute starting row
  unsigned long yo_b;                   //+ binned starting row
  unsigned long height;                 //+ height of par reg
  unsigned long height_b;               //+ binned height of par region
  unsigned long skip;                   //+ skip since last par reg.
  unsigned long width;                  //+ sum of length of ser region
  int nser_regions;                     //+ number of ser_regions in this par region
  unsigned long remain;                 //+ num pix remaining in serial register after last ser regions is read
  unsigned long long amp_mask;          //+ all the amps this par region has in it
  unsigned long long region_mask;       //+ all the regions this par region has in it
  Ser_Regions *ser_regions;             //+ array of serial lregions for this par region
};

//+ output structure - a subframe specification
struct Sub_Frame_Spec {
  unsigned long xo,yo;                  //+ origin of subframe
  int xdir,ydir;                        //+ direction subframe is readout
  Readout_Orientation orient;           //+ amp readout orientation
  unsigned long height,width;           //+ size of subframe
  unsigned long height_binned,width_binned;//+ binned size
  unsigned long data_height,data_width; //+ data size (less overscan)
  unsigned long data_height_binned,data_width_binned;//+ data binned size
  unsigned long trim_width;             //+ trimming width
  unsigned long prescan_binned;         //+ prescan size binned
  unsigned long postscan_binned;        //+ postscan size binned
  unsigned long camera_xo,camera_yo;    //+ origin in full camera coords
  unsigned long chip_xo,chip_yo;        //+ origin in full controller coords
  unsigned long amp_xo,amp_yo;          //+ origin in full chip coords
  unsigned long frame_xo,frame_yo;      //+ origin in full frame coords
  unsigned long overscan_rows_binned;   //+ overscan rows binned
  unsigned long overscan_cols_binned;   //+ overscan cols binned
  int rcf;                              //+ row binning factor
  int ccf;                              //+ column binning factor
  int amp;                              //+ amp this subframe belongs to
  int reg;                              //+ region this subframe belongs to
  int chip;                             //+ detector chip this subframe belongs to
  int subframe;                         //+ subframe index this subframe has been assigned in this frame
  int subframe_idx;                     //+ subframe index this subframe has been assigned in all subframes
  int frame;                            //+ frame index that this subframe is located in
};

//+ output structure - a frame specification
struct Frame_Spec {
  unsigned long height,width;           //+ full size of frame (inc overscan if included in data frames)
  unsigned long data_height,data_width; //+ full size of data section of frame
  unsigned long height_binned,width_binned;//+ binned size
  unsigned long data_height_binned,data_width_binned;//+ data binned size
  unsigned long camera_xo,camera_yo;    //+ origin in full camera coords
  Image_Data_Type data_type;            //+ Type of data in this frame
  vector<Sub_Frame_Spec> subframes;     //+ Subframe array
};


//+ map of frame specs indexed according to appropriate grouping
typedef map<int, Frame_Spec, less<int> > Regions_Frame_Map;

//+ map of amps to output frame
typedef map<int, int, less<int> > Amp_Frame_Map;

//+ Calculates a bitmask, given the bit position
// > bitpos - the bit position
inline unsigned long long mask_from_bitpos(int bitpos) {
  return static_cast<unsigned long long>(1)<<bitpos;
};

/*--------------------------------------------------------------------------
 Class declarations 
 -------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
CLASS
    Detector_Regions

DESCRIPTION  
    Detector_Regions class definition 
 
KEYWORDS
    Regions, ROI
----------------------------------------------------------------------------*/
class Detector_Regions: public Regions {

 public:

  //+ basic constructor - nothing setup
  // > cam - camera description - provides geometry context for this region spec
  // > ci - controller id
  // > cam_co - flag to indicate if following geometry info in camera coordinate space
  // > mm - mode of mosaicing output (none,amp,chip)
  // > append_os - set if overscan desired appended at end of data frame
  // > rowcf,colcf - row and column binning factors
  // > osrows,oscols - row and columns overscan sizes
  // > chip_mask - which chips in the controller description should be considered
  Detector_Regions(Camera_Desc& cam, int ci, bool cam_co=true, Mosaic_Mode mm=MOSAIC_NONE, bool append_os=true,
		   unsigned short rowcf=1, unsigned short colcf=1, unsigned long osrows=0, unsigned long oscols=0,
		   unsigned long long chip_mask=FULL_MASK);

  //+ Constructor with initial data
  // > cam - camera description - provides geometry context for this region spec
  // > ci - controller id
  // > cam_co - flag to indicate if following geometry info in camera coordinate space
  // > mm - mode of mosaicing output (none,amp,chip)
  // > append_os - set if overscan desired appended at end of data frame
  // > n - number of regions to handle
  // > xo,yo - arrays of region origins
  // > width,height - arrays of region sizes
  // > rowcf,colcf - row and column binning factors
  // > osrows,oscols - row and columns overscan sizes
  // > chip_mask - which chips in the controller description should be considered
  Detector_Regions(Camera_Desc& cam, int ci, bool cam_co, Mosaic_Mode mm, bool append_os, unsigned short n, 
		 unsigned long *xo, unsigned long *width, unsigned long *yo, unsigned long *height,
		 unsigned short rowcf=1, unsigned short colcf=1, unsigned long osrows=0, unsigned long oscols=0,
                 unsigned long long chip_mask=FULL_MASK);

  //+ Destructor
  ~Detector_Regions();

  // GROUP: Detector_Regions class Public Member Functions.

  //+ Given a region specification in chip coordinates, an amp readout 
  //+ specification is produced according to given amplifier geometry.
  void chip_to_amp();

  //+ pixels in amplifier coords are converted to chip coordinates.
  void amp_to_chip();

  //+ Prints on standard output a list of current amp readout specs
  void print_amp_specs();

  //+ check for a valid amplifier config
  void check_amp_config();

  //+ Checks if we have both row and column oriented readouts, and if
  //+ this is the case, sub-regions are not allowed.  
  void check_subregions_allowed();

  //+ Make sure region size and position a binning aligned
  void align_regions_with_binning();

  //+ Return total number of pixels that will be read out
  unsigned long total_readout_pixels();

  //+ return total number of binned pixels in the readout specification.
  unsigned long total_binned_readout_pixels();

  //+ Compute total size of (binned) image rows for
  //+ "Reading out xxxx/yyyy" progress display in gui.
  unsigned long get_progress_counter();

  //+ Return the frame/subframe/region mask that holds the region from the specified
  //+ amp,par_region,ser_region
  unsigned long long get_frame_mask(int amp_no, int pr, int sr);
  unsigned long long get_subframe_mask(int amp_no, int pr, int sr);
  unsigned long long get_region_mask(int amp_no, int pr, int sr);

  //+ returns linear address of amplifier number for the current global_amp_mask.
  int get_amp(int amp_no);
  
  //+ Read region descriptions from file - formatted using ximtool marker conventions
  void read_regs(const char * region_file,unsigned long xmax, unsigned long ymax);

  //+ return amplifier index of a given frame number
  // > f - frame index
  // > sf - subframe index
  int amp_index(int f, int sf);
  
  //+ return chip index of a given frame number
  // > f - frame index
  // > sf - subframe index
  int chip_index(int f, int sf);

  // GROUP: Detector_Region class Public member Variables.
  int debug;                            //+ debugging level
  int namps,nactive_amps;               //+ number of amps and active amps
  unsigned long overscan_rows;          //+ overscan rows
  unsigned long overscan_cols;          //+ overscan cols
  unsigned long overscan_rows_binned;   //+ overscan rows binned
  unsigned long overscan_cols_binned;   //+ overscan cols binned
  int rcf;                              //+ row binning factor
  int ccf;                              //+ column binning factor
  unsigned long *amp_xo, *amp_yo;       //+ origin of each amp
  unsigned long *amp_prescan, *amp_postscan;//+ pre and post scan for each amp
  unsigned long *amp_width, *amp_height;//+ amp sizes
  int *amp_xdir, *amp_ydir;             //+ amp readout direction
  int *amp_list;                        //+ list of amps
  Readout_Orientation *amp_orient;      //+ amp readout orientation
  int npar_regions;                     //+ number of parallel regions in readout spec
  Par_Regions *par_regions;             //+ Detector controller readout output structures and vars.
  int nsframes;                         //+ total number of sub frames in output
  vector<Sub_Frame_Spec> subframes;     //+ Subframe array (duplicated - not in hierarchy)
  int nframes;                          //+ total number of frames in output
  Regions_Frame_Map frames;             //+ full readout frame specifications in frame/subframe hierarchy
  Amp_Frame_Map amp_frame_map;          //+ map of amp to frame number
  int ndframes;                         //+ total number of data frames in output
  int ndsframes;                        //+ total number of data subframes in output
  
 private:

  // GROUP: Detector_Regions class Private member Functions.

  //+ set all working arrays to NULL
  void init_arrays();
  
  //+ cleanup space allocated to working arrays
  void cleanup_arrays();
  
  //+converts amplifier desc to internal variable names used in class 
  void setup_amps();

  //+ function to test whether point x,y falls inside region r (on amp a)
  // > r - region idx
  // > a - amp idx
  // > x - column
  // > y - row
  int point_in_region(int r, int a, long x, long y);

  //+ Updates framelist array with mask of frames in current framemask for amp
  // > fm - framemask
  // > this_amp - amp being processed
  // > inser - serial region being processed
  // > dt - data type
  void update_framelist(unsigned long long fm, int this_amp, int inser, Image_Data_Type dt);
  
  //+ Loops through each user specified region to determine whether the region
  //+ appears in the current amp being processed and sets coordinates of a new frame.
  // > ampi - index of amp being considered
  // > handling_data_section - set if we are looking for data regions - otherwise we are looking for overscan
  void do_region_loop(int ampi, bool handling_data_section);
  
  //+ Detects unique parallel clockout regions from spec
  void find_par_regions();

  //+ Adds serial region information to the parallel region array
  // > dt - data type in current parallel region
  void find_serial_regions(Image_Data_Type dt);

  //+ Sets up par_regions array element with serial overscan detail
  void handle_overscan_cols();

  //+ Fills a subframe structure with detail of this readout amp
  // > ampi - amp to place in subframe structure
  // > region - region number
  // > ampcoo_xo - amp coordinates x origin
  // > ampcoo_yo - amp coordinates y origin
  // > ampcoo_xsize - amp coordinates x size
  // > ampcoo_ysize - amp coordinates y size
  // > mm - mosaic mode to use while creating output frame
  void setup_output_subframe(int ampi, int region, long ampcoo_xo, long ampcoo_yo, long ampcoo_xsize, long ampcoo_ysize, 
			     Mosaic_Mode mm, Image_Data_Type dt);

  //+ Adds a subframe to the frames map
  // > subframe - subframe to add to frames vector
  // > mm - mosaicing mode
  void add_subframe(Sub_Frame_Spec& subframe, int ampi, Mosaic_Mode mm, Image_Data_Type dt);

  // GROUP: Detector_Regions class Private Variables.
  Camera_Desc camera;                   //+ Camera description
  Mosaic_Mode mosaic_mode;              //+ Method of mosaicing data on output (none,amp,chip)
  bool camera_coords;                   //+ True if regions specified in cam coords.
  bool append_overscan;                 //+ Set if overscan desired to be appended to data frame
  int cid;                              //+ Controller id
  int npar;                             //+ number of parallel regions
  int nser;                             //+ number of serial regions for current par region
  int *amp_chipnum;                     //+ chip number for each amp
  int *amp_ampnum;                      //+ amp index for each amp
  int amp;                              //+ amp currently being looked at
  unsigned long row;                    //+ current row being processed
  unsigned long long *xreg_bits;        //+ pixel arrays with region bit masks indicating pixels in each region
  unsigned long long *yreg_bits;        
  unsigned long long *xamp_bits;        //+ pixel arrays with amp bit masks indicating pixels from each amp
  unsigned long long *yamp_bits;
  unsigned long long *xframe_bits;      //+ pixel arrays with frame bit masks indicating pixels in each frame
  unsigned long long *yframe_bits;
  unsigned long long current_amp_mask;  //+ mask indicating current amp being looked at
  unsigned long long data_frames_mask;  //+ mask for all data subframes
  unsigned long long yregmask;          //+ current pixel masks for current row
  unsigned long long yampmask;
  unsigned long long yframemask;
  unsigned long long clockout_chip_mask;//+ Chip masks for handling clockout and saving of data
  unsigned long long save_chip_mask;    //+ Requested chip mask
  unsigned long long global_amp_mask;   //+ parallel amp masks ||ed.  
  unsigned long long full_controller_ampmask;//+ Mask with bits set for all amps wired on controller

};
#endif // _CICADA_REGIONS_H



