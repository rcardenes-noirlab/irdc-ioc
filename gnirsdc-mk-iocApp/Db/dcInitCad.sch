[schematic2]
uniq 8
[tools]
[detail]
w 5176 3291 100 0 n#1 elongins.initID.VAL 5152 3280 5248 3280 ecars.initC.ICID
w 3704 3723 -100 0 CLID inhier.CLID.P 3680 3712 3776 3712 3776 3632 3872 3632 ecad4.init.ICID
w 4268 3675 100 0 n#2 junction 4220 3664 4352 3664 4352 3088 4368 3088 cadPlus.initP.STAT
w 4216 3755 100 0 n#2 ecad4.init.VAL 4192 3664 4220 3664 4220 3856 4316 3856 outhier.VAL.p
w 4232 3035 100 0 n#3 ecad4.init.SPLK 4192 3024 4320 3024 4320 3120 4368 3120 cadPlus.initP.SPIN
w 4216 3067 100 0 n#4 ecad4.init.STLK 4192 3056 4288 3056 4288 3152 4368 3152 cadPlus.initP.STIN
w 4612 2859 100 2 n#5 hwin.hwin#18.in 4608 2864 4608 2864 ebos.initDone.DOL
w 4248 3707 100 0 n#6 ecad4.init.MESS 4192 3632 4252 3632 4252 3792 4316 3792 outhier.MESS.p
w 3800 3723 100 0 n#7 inhier.DIR.P 3732 3792 3804 3792 3804 3664 3872 3664 ecad4.init.DIR
s 2656 2256 200 0 The INIT CAD/CAR schematic. SNL code monitors the initP record
s 2656 2192 200 0 START and STOP links to perform required action.
[cell use]
use elongins 4896 3223 100 0 initID
xform 0 5024 3296
use inhier 3664 3671 100 0 CLID
xform 0 3680 3712
use inhier 3716 3751 100 0 DIR
xform 0 3732 3792
use cadPlus 4368 3200 150 0 initP
xform 0 4448 3104
p 4368 2992 100 0 1 set1:cad initP
use ecars 5312 3360 150 0 initC
xform 0 5408 3200
p 5200 2976 100 0 1 def(FLNK):$(top)combCars11.PROC
use hwin 4416 2823 100 0 hwin#18
xform 0 4512 2864
p 4419 2856 100 0 -1 val(in):1
use ebos 4680 2912 100 0 initDone
xform 0 4736 2832
p 4688 2720 100 0 1 ONAM:NOT_DONE
p 4288 2814 100 0 0 PINI:YES
p 4688 2688 100 0 1 ZNAM:DONE
use outhier 4284 3751 100 0 MESS
xform 0 4300 3792
use outhier 4284 3815 100 0 VAL
xform 0 4300 3856
use ecad4 3948 3708 150 0 init
xform 0 4032 3344
p 3980 3520 100 0 1 FTVA:STRING
p 3980 3488 100 0 1 FTVB:STRING
p 3476 2620 100 0 0 MFLG:THREE STATES
p 3964 2944 100 0 1 SNAM:CADinit
p 4192 3066 75 0 -1 pproc(STLK):PP
use eborderC 2464 1935 100 0 eborderC#0
xform 0 4144 3240
p 5040 2088 100 768 -1 author:Peter Young
p 5040 2200 40 0 -1 comment1:1997-10-01 GMOS version. Ken Ramey
p 5040 2180 40 0 -1 comment2:2000-12-11 Modified for GNIRS. Peter Young
p 5040 2056 100 768 -1 date:2000-12-11
p 5264 2136 200 768 -1 file:dcInitCad.sch
p 5536 2088 100 0 -1 page:1
p 5648 2088 100 0 -1 pages:1
p 5312 2088 100 0 -1 revision:0
p 5264 2200 150 768 -1 system:Gemini GNIRS DC
[comments]
