/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * FILENAME
 *   configuration.h
 *
 * PURPOSE
 *  This file contains all central configuration data structure definitions used
 *  in the RSAA Gemini DC code package. They are based on the RPCL definitons
 *  used in RSAA's Cicada package.
 *
 * RSAA GEMINI PROJECTS
 *
 * HISTORY
 *INDENT-ON
 *
 */
#ifndef _CONFIGURATION_H
#define _CONFIGURATION_H

#include <cstring>
#include <cstdio>

/* Include constant definitions */


/*==============================================================================
   Global constants
==============================================================================*/

/* String length constants */
const int HOSTLEN = 256;
const int FILENAMELEN = 256;
const int PREFIXLEN = 80;
const int SUFFIXLEN = 80;
const int COMMENTLEN = 70;
const int NAMELEN = 25;
const int IPCNAMELEN = 14;
const int FITSKEYWLEN = 9;
const int FITSVALUELEN = 71;
const int MESSAGEBUFLEN = 8192;
const int SMALLMSGLEN = 40;


/* Array size constants */
const int MAX_IMAGE_FRAMES = 64;        /* Maximum image frames supported */
const int MAX_AMPS = 32;                /* Maximum readout amps per controller */
const int MAX_CHIPS = 8;                /* Max chips per controller */
const int MAX_CAMERAS = 2;              /* Max cameras per instrument */
const int MAX_CONTROLLERS = 4;          /* Max controllers per camera */
const int MAX_ROI = 4;                  /* Max different region of interest specifications */
const int MAX_TEMPS = 12;               /* Maximum temp readouts on a dewar*/
const int MAX_TEMP_COEFFS = 12;         /* Maximum temperature coeefs */
const int MAX_SERVOS = 7;               /* Max Heater Servo parameters */
const int MAX_SPEEDS = 3;               /* readout speeds - slow,medium,fast */
const int FRAMES_MEM_SIZE = 16384;      /* Size of storage for frame data */
const int MAX_READOUT_BUFFERS = 32;     /* Maximum number of readout buffers allowed */



typedef char Hostnametype[HOSTLEN];
typedef char Filenametype[FILENAMELEN];
typedef char Prefixtype[PREFIXLEN];
typedef char Suffixtype[SUFFIXLEN];
typedef char Messagebuftype[MESSAGEBUFLEN];
typedef char Commenttype[COMMENTLEN];
typedef char Nametype[NAMELEN];
typedef char FitsKeyWType[FITSKEYWLEN];
typedef char FitsValueType[FITSVALUELEN];
typedef char IPCnametype[IPCNAMELEN];
typedef char SmallMsgType[SMALLMSGLEN];

/* Camera types */
enum Camera_Type {
  CAMERA_UNKNOWN,
  CCD_CAMERA,
  IR_CAMERA
};
// typedef enum Camera_Type Camera_Type;

/* Camera modes */
enum Camera_Mode {
  SCIENCE_CAMERA,
  GUIDER_CAMERA
};
// typedef enum Camera_Mode Camera_Mode;

/* Camera controller types */
enum Controller_Type {
  CONTROLLER_UNKNOWN,	
  DUMMY,
  SDSU2,
  SDSU3,
  SDSU3_5,
  APOGEE
};
// typedef enum Controller_Type Controller_Type;

/* Camera controller interface bus types */
enum Controller_Bus_Type {
  BUS_UNKNOWN,
  BUS_SBUS,
  BUS_PCI,
  BUS_VME,
  BUS_ISA,
  BUS_PCIe,
};
// typedef enum Controller_Bus_Type Controller_Bus_Type;


/* Chip Readmode type */
enum Chip_Readmode_Type {
  CHIP_READOUT_SLOW,
  CHIP_READOUT_MEDIUM,
  CHIP_READOUT_FAST,
  CHIP_READOUT_NULL
};
// typedef enum Chip_Readmode_Type Chip_Readmode_Type;

/* IR running mode */
enum IR_Mode {
  IR_NULL_MODE,
  IR_VIEW_MODE,
  IR_OBSERVE_MODE
};
// typedef enum IR_Mode IR_Mode;

/* IR readout modes */
enum IR_Readmode_Type {
  IR_READOUT_NULL = -1,
  IR_VERY_BRIGHT_READOUT = 0,
  IR_BRIGHT_READOUT,
  IR_FAINT_READOUT,
  IR_VERY_FAINT_READOUT,
};

enum IR_Quadrants_Type {
  IR_QUADRANT_NULL = -1,
  IR_FULL_FRAME = 0,
  IR_1_QUADRANT,
};

// typedef enum IR_Readmode_Type IR_Readmode_Type;
const int N_READMODES = 4;              /* Number of valid readmode types */
const int N_IRMODES = 2;                /* Number of valid IR operation modes (e.g. View and Observe) */



/* Detector reset parameters */
struct Detector_Reset {
  long nresets;                    /* number of times to reset */
  double reset_delay;              /* delay after reset before proceeding - seconds */
};
// typedef struct Detector_Reset Detector_Reset;


/* Hardware types supported */
enum Hardware_Type {
  CAMERA
};
// typedef enum Hardware_Type Hardware_Type;

/* Type of output mosaicing to be done on readout data */
enum Mosaic_Mode {
  MOSAIC_NONE,
  MOSAIC_AMP,
  MOSAIC_CHIP
};
// typedef enum Mosaic_Mode Mosaic_Mode;

/* Guider mode */
enum Guider_Mode {
  GUIDER_IDLE,
  GUIDER_CALIBRATE,
  GUIDER_ACQUIRE,
  GUIDER_GUIDE,
  GUIDER_CORRECT
};
// typedef enum Guider_Mode Guider_Mode;




/*==============================================================================
   Configuration description data structures
==============================================================================*/

/* which direction is readout oriented */
enum Readout_Orientation {
  ORIENT_ROW,
  ORIENT_COL
};
// typedef enum Readout_Orientation Readout_Orientation;

/* Chip Amplifier description */
struct Amp {
  Nametype name;                /* Name of amp being used */
  int xo;                       /* x offset of first pixel*/
  int yo;                       /* y offset of first pixel*/
  int width;                    /* number of columns (including prescan&postscan) */
  int height;                   /* number of rows */
  int prescan;                  /* number of prescan pixels */
  int postscan;                 /* number of postscan pixels */
  int xdir;                     /* direction of readout - columns */
  int ydir;                     /* direction of readout - rows */
  Readout_Orientation orient;   /* Orientation of readout, either column or row */
};
// typedef struct Amp Amp;

/* WCS transform parameters */
struct WCS_Info {
  Nametype ctype;                /* Coordinate type */
  float crval;                   /* Coordinate reference value */
  Nametype cunit;                /* Coordinate reference unit */
  float crpix;                   /* Coordinate reference pixel */
  float cd[2];                   /* Coordinate scale matrix */
};
// typedef struct WCS_Info WCS_Info;

/* Chip description */
struct Chip_Desc {
  Nametype name;                   /* Name of Chip being used */
  int cols;                        /* Number of columns */
  int rows;                        /* Number of rows */
  int amp_mask;                    /* Mask indicating active amps */
  int  has_wcs;                    /* TRUE if WCS info set for this Chip */
  WCS_Info wcs[2];                 /* WCS transform parameters for RA and DEC*/
  int  adu_increase;               /* True if ADUs increase with exposure time */
  int n_amps;                      /* Number of readout amplifiers */
  Amp amp[MAX_AMPS];               /* Amplifier description */
  Nametype manufacturer_serial;    /* manufacturer serial number*/
  Nametype manufacturer;           /* manufacturer name*/
  Commenttype comment;             /* Any comment about this desc */
};
// typedef struct Chip_Desc Chip_Desc;

/* Description of verification data */
struct Verify_Spec {
  int  do_test_pattern;            /* set if controller is to produce test pattern data */
  int  constant_val;               /* set if test pattern is a constant for each amplifier, otherwise incrementing */
  unsigned short seed;             /* starting value for first amp when increment or constant */
  unsigned short increment;        /* increment between pixels when increment mode */
  unsigned short amp_increment;    /* increment between amps */
  unsigned short readout_increment;/* increment between readouts */
  unsigned short overscan;         /* value to use for overscan in amp one, increased by amp_increment for other amps */
};
// typedef struct Verify_Spec Verify_Spec;

/* Camera controller description */
struct Controller_Desc {
  Nametype name;                   /* camera controller name*/
  Hostnametype host;               /* Host that owns controller */
  Controller_Type type;            /* camera controller type*/
  Controller_Bus_Type bus;         /* camera controller interface bus type*/
  Nametype hwv;                    /* controller hardware version*/
  Filenametype device;             /* Controller interface device name */
  int  mask_chips;                 /* TRUE if controller can mask out Chips */
  int address;                     /* Controller address on bus */
  int cols;                        /* Controller cols */
  int rows;                        /* Controller rows */
  int amp_mask;                    /* Mask indicating active amps - computed*/
  int n_amps;                      /* Total number of readout amps - computed */
  int n_chips;                     /* Number of Chips attached to controller */
  Chip_Desc chip[MAX_CHIPS];       /* Chip description */
  int chip_xo[MAX_CHIPS];          /* x offset of Chip on camera - active coordinates*/
  int chip_yo[MAX_CHIPS];          /* y offset of Chip on camera - active coordinates*/
  int chip_xgap[MAX_CHIPS];        /* x gap before Chip on camera - spatial coordinates*/
  int chip_ygap[MAX_CHIPS];        /* y gap before Chip on camera - spatial coordinates*/
  Verify_Spec verify_spec;         /* Used for generating data that can be used to verify output data */
  Commenttype comment;             /* Any comment about this desc */
};
// typedef struct Controller_Desc Controller_Desc;


/* Camera description */
struct Camera_Desc {
  Nametype name;                   /* name of this camera */
  Camera_Type type;                /* Type of this camera */
  Camera_Mode mode;                /* Mode of this camera */
  Mosaic_Mode mosaic_mode;         /* Set to mode that data from multiple amps is to be formed into a mosaic */
  int cols;                        /* Camera cols */
  int rows;                        /* Camera rows */
  int n_buffers;                   /* Number of image buffers (frames) to use for holding image */
  int n_controllers;               /* Number of controllers for this camera*/
  int phu1;                        /* single extension primary Fits header units */
  int phu2;                        /* multiple extension primary Fits header units */
  int ihu;                         /* Fits image header units */
  Controller_Desc controller[MAX_CONTROLLERS];/* Controller description */
  int cont_xo[MAX_CONTROLLERS];    /* x offset of controller on camera - active coordinates*/
  int cont_yo[MAX_CONTROLLERS];    /* y offset of controller on camera - active coordinates*/
  int cont_xgap[MAX_CHIPS];        /* x gap before controller on camera - spatial coordinates*/
  int cont_ygap[MAX_CHIPS];        /* y gap before controller on camera - spatial coordinates*/
  int cont_master[MAX_CONTROLLERS];/* TRUE if controller is master */
  int cont_shutter[MAX_CONTROLLERS];/* TRUE if controller controls shutter */
  int cont_temp[MAX_CONTROLLERS];  /* TRUE if controller controls temperature */
  Nametype cont_config[MAX_CONTROLLERS];/* name of contoller config to use */
  int  view_enable;                /* Flag for setting automatic view mode for IR cameras*/
};
// typedef struct Camera_Desc Camera_Desc;



/* Instrument description, made up of N cameras, M simples and a telescope*/
struct Instrument_Desc {
  Nametype name;                   /* name of this instrument */
  int n_cameras;                   /* number of cameras */
  Camera_Desc camera[MAX_CAMERAS]; /* each camera description */
};
// typedef struct Instrument_Desc Instrument_Desc;


/* Include specific hardware definition files here */

/* Include specific camera and controller definition files here */

/*==============================================================================
   Dummy controller specific data structures
==============================================================================*/

/* the maximum number of generated stars */
const int MAX_TEST_STARS = 4;

enum Dummy_Commands {
  DUMMY_INVALID,
  DUMMY_TEST
};
// typedef enum Dummy_Commands Dummy_Commands;


/* Configuration data */
struct Dummy_Config_Data {
  Filenametype sim_image;       /* File name of disk FITS simulation file */
  double sim_exptime;           /* Exposure time for the simulation image - seconds*/
  double pixel_read_time;       /* time in seconds to clock a pixel */
  double pixel_clear_time;      /* time in seconds to clear a pixel */
  double cosmic_rate;           /* Simulation rate of cosmic rays */
};
// typedef struct Dummy_Config_Data Dummy_Config_Data;

struct Dummy_Config {
  Dummy_Config_Data config_data;/* Configuration data*/
};
// typedef struct Dummy_Config Dummy_Config;

struct Dummy_Init {
  int dummy_steps;
  Dummy_Config_Data config_data;/* Configuration data*/
};
// typedef struct Dummy_Init Dummy_Init;

/* Dummy controller stuff */
struct Dummy_Controller_Request {
  Dummy_Commands type;
  union {
  int dummy;
  } Dummy_Controller_Request_u;
};
// typedef struct Dummy_Controller_Request Dummy_Controller_Request;

/* A readout structure for dummy hardware, can have two regions but with
   same y dimensions */
struct Dummy_Readout{
  int dummy;
};
// typedef struct Dummy_Readout Dummy_Readout;

/* Dummy request structure */
struct Dummy_Request {
  Dummy_Commands type;
  union {
   int dummy;
  } Dummy_Request_u;
};
// typedef struct Dummy_Request Dummy_Request;
/*
 * These are specific commands for the gains (Gen two only).  Please note that
 * the hex numbers are NOT the ASCII (character) values of the commands.  */
const int GN_NULL = 0;           /* Special value to indicate gain not set */
const int GN1 = 0x1;
const int GN2 = 0x2;             /* 2 gain */
const int GN4 = 0x5;	     /* 4.75 gain */
const int GN9 = 0xa;	     /* 9.5 gain */

/*
 * These are specific commands for the Anti-Blooming frequency (Gen two only).
 * Please note that the hex numbers are NOT the ASCII (character) values of the
 * commands.  */
const int AB12 = 0xc;           /* 12.5 Hz */
const int AB25 = 0x19;          /* 25 Hz */
const int AB50 = 0x32;          /* 50 Hz */
const int AB100 = 0x64;         /* 100 Hz */



/*==============================================================================
  SDSU controller specific data structures
==============================================================================*/

/* SDSU command types */
enum Sdsu_Commands {
  SDSU_INVALID,
  SDSU_DSP,
  SDSU_SPECIAL_DSP,
  SDSU_READ_TEMP,
  SDSU_SET_VOLTS
};
// typedef enum Sdsu_Commands Sdsu_Commands;

/* SDSU DSP memory space types */
enum Sdsu_Memory_Space {
  SDSU_MEM_P,
  SDSU_MEM_X,
  SDSU_MEM_Y,
  SDSU_MEM_R
};
// typedef enum Sdsu_Memory_Space Sdsu_Memory_Space;

/* SDSU DAC board types */
enum Sdsu_Dac_Board_Type {
  SDSU_VIDEO_BOARD,
  SDSU_CLOCK_BOARD
};
// typedef enum Sdsu_Dac_Board_Type Sdsu_Dac_Board_Type;

/* DSP program instruction stored in memory for ease of transport onece uploaded
   from file */
struct  DSP_instruction {
  Sdsu_Memory_Space mem;
  int address;
  int value;
};
// typedef struct DSP_instruction DSP_instruction;

const int SDSU_MAX_DSP_INSTRUCTIONS = 4096;         /* Maximum number of DSP program instructions */

/* DSP Download program data buffer */
struct  DSP_prog {
  int n_instructions;
  DSP_instruction instruction[SDSU_MAX_DSP_INSTRUCTIONS];
};
// typedef struct DSP_prog DSP_prog;

/* Voltages parameters */
const int SDSU_MAX_VOLTS = 50;
const int SDSU_MAX_VOLT_VALS = 2;

enum Sdsu_Voltage_Type {
  SDSU_CLOCK,
  SDSU_BIAS
};
// typedef enum Sdsu_Voltage_Type Sdsu_Voltage_Type;

enum Sdsu_Voltage_Index {
  V_LO,
  V_HI
};
// typedef enum Sdsu_Voltage_Index Sdsu_Voltage_Index;

/* Structure used to define a voltage.
 * CLOCK voltages have High and Low settings.
 * BIAS voltages only have one value.
 * However, BIAS voltages with a CLK board_type have two values (low & high).
 * Voltages must be within min_volt and max_volt.
 * ADUs calculated from the voltage value, must be within the physical
 * limits of the device min_adu_phys and max_adu_phys.
 * The "cal..." values are the calibration values used in the conversion
 * volts<->adus.
 * The set order is the order in which the volatges must be set on the
 * controller by the INIT code and by any APPLY done via SDSU config GUI.
 */
struct Volts_Desc {
  Nametype name;              			/* Name of Voltage being used */
  int  set_this_voltage	;			/* Whether or not to set this voltage on controller */
  Sdsu_Voltage_Type type;    			/* Whether CLOCK or BIAS */	
  int board_addr;    				/* address of board that will be setting the voltage */                	
  int dsp_addr;    				/* address in DSP Y: memory where voltage is stored */
  Sdsu_Dac_Board_Type board_type;		/* Whether clock or video */
  int precision;				/* num decimal places to display for this voltage */
  Commenttype comment;           		/* Any comment about this voltage */

  double value[SDSU_MAX_VOLT_VALS]; 	  	/* default value for voltage */
  int dac_addr[SDSU_MAX_VOLT_VALS];             /* DAC adress for voltage */
  int set_order[SDSU_MAX_VOLT_VALS];	        /* order in which the voltage has to be set */
  int set_delay[SDSU_MAX_VOLT_VALS];	        /* time to pause after setting voltage in msecs */
  double min_volt[SDSU_MAX_VOLT_VALS];		/* minimum voltage limit */
  double max_volt[SDSU_MAX_VOLT_VALS];		/* maximum voltage limit */
  double cal1_volt[SDSU_MAX_VOLT_VALS];		/* theoretical minimum for this voltage */
  double cal2_volt[SDSU_MAX_VOLT_VALS];		/* theoretical maximum for this voltage */
  int min_adu_phys[SDSU_MAX_VOLT_VALS];		/* physical minimum ADU val for this DAC */
  int max_adu_phys[SDSU_MAX_VOLT_VALS];		/* physical maximum ADU val for this DAC */
  int cal1_adu[SDSU_MAX_VOLT_VALS];		/* theoretical minimum for this voltage */
  int cal2_adu[SDSU_MAX_VOLT_VALS];		/* theoretical maximum for this voltage */
};
// typedef struct Volts_Desc Volts_Desc;

/* SDSU  config parameters */
struct Sdsu_Config_Data {
  /* flags to indicate what setup functions to perform */
  int  do_simulation;                           /* Set if driver is to simulate SDSU responses */
  int  do_comms_test_at_startup;                /* Set if required to run TDL short tests on driver attach */
  int  do_long_hardware_test;                   /* Set if required to run TDL long tests during init */
  int  do_short_hardware_test;                  /* Set if required to run TDL short tests during init */
  int  do_reset;                                /* Set if required to reset the SDSU during init */
  int  do_power_on;                             /* Set if required to turn SDSU power on during init */
  int  has_utility_board;                       /* Set if SDSU configured with a utility board */
  int  do_target_chip_temp;                     /* Set if SDSU to control temperature setpoint */
  int  do_heater_adu;                           /* Set if required to set the heater ADU value during init*/
  int  do_gain;                                 /* Set if required to set the controller gain during init*/
  int  do_tc;                                   /* Set if required to set the controller time constant during init*/
  int  do_ab;                                   /* Set if required to set the controller anti-blooming during init*/
  int  do_idle;                                 /* Set if required to set controller idle mode during init*/
  int  do_timing_download;                      /* Set if required to download timing DSP during init */
  int  do_utility_download;                     /* Set if required to download utility DSP during init */
  int  do_interface_download;                   /* Set if required to download interface DSP during init */
  int  chiptemp_set;                            /* Set if chip temperature is set during init (combined with do_target_chip_temp)*/
  int  idle_set;                                /* Set if turn idle on during init (combined with do_init) */
  int  enable_auto_rdc;                         /* Set if DSP generates RDC and end of exposure automatically */
  int  do_voltages;                             /* Set if required to set voltages */
  /* Flags used to initialise a coadder board */
  int  has_coadder_board;                       /* Set if SDSU configured with a coadder board */
  int  do_coadder_download;                     /* Set if required to download coadder DSP during init */
  int  do_coadder_init;                         /* Set if required to initialise coadder DSP during init */
  int  do_coadder_setup;                        /* Set if required to setup coadder DSP during init */
  int  test_pattern_by_coadder;                 /* Set if coadder generates test patterns */
  /* data values associated with setup functions */
  int pon_timeout;                              /* Timeout (msec) for PON */
  int clr_timeout;                              /* Timeout (msec) for CLR */
  int idle_timeout;                             /* Timeout (msec) for IDL */
  int timing_setting;                           /* Number of TDLs when testing timing board */
  int utility_setting;                          /* Number of TDLs when testing utility board */
  int coadder_setting;                          /* Number of TDLs when testing coadder board */
  int interface_setting;                        /* Number of TDLs when testing interface board */
  int heater_adu;                               /* Heater ADU setting */
  int gain_setting;                             /* Gain setting */
  int tc_setting;                               /* Time constant setting */
  int ab_setting;                               /* Anti blooming constant setting */
  /* Download information - data is read into download buffers from files */
  Filenametype timing_filename;                 /* name of timing DSP code file */
  Filenametype utility_filename;                /* name of utility DSP code file */
  Filenametype interface_filename;              /* name of interface DSP code file */
  Filenametype volts_filename;                  /* name of voltage setup file */
};
// typedef struct Sdsu_Config_Data Sdsu_Config_Data;

/* Full configuration information including DSP code */
struct Sdsu_Config {
  Sdsu_Config_Data config_data;                 /* SDSU configuration data */
  DSP_prog timing_prog;                         /* SDSU timing board DSP code */
  DSP_prog utility_prog;                        /* SDSU utility board DSP code */
  DSP_prog interface_prog;                      /* SDSU interface board DSP code */
  int n_volts;	                                /* Number of volts in table */
  Volts_Desc volts[SDSU_MAX_VOLTS];             /* voltage descriptions */
};
// typedef struct Sdsu_Config Sdsu_Config;

/* SDSU  Initialize structure */
struct Sdsu_Init {
  Sdsu_Config_Data config_data;                 /* Configuration data*/
};
// typedef struct Sdsu_Init Sdsu_Init;

/* boot commands */
const int LDA = 0x4C4441;        /* LDA - load address */
const int RDM = 0x52444D;        /* RDM - read memory location */
const int RST = 0x525354;        /* RST - timing card reset */
const int TDL = 0x54444C;        /* TDL - test data link */
const int WRM = 0x57524D;        /* WRM - write memory location */

/* Interface board commands */
const int CSR = 0x435352;        /* CSR - fetch command status register*/
const int ADR = 0x414452;        /* ADR - fetch address register*/
const int BCR = 0x424352;        /* BCR - fetch byte count register*/
const int BRR = 0x425252;        /* BRR - fetch byte remaining counter */
const int RDR = 0x524452;        /* RDR - fetch readouts done counter (IR) */
const int RDT = 0x524454;        /* RDT - fetch last readout time (msec) */
const int RDP = 0x524450;        /* RDP - fetch readout running flag */
const int RESET = 0x526573;      /* Res - SBUS reset*/
const int SBU = 0x534355;        /* SBU - Read current driver setup information */
const int SRS = 0x535253;        /* SRS - Fetch full readout status structure */
const int ABT = 0x414254;	     /* ABR - tell driver current readout has been aborted */
const int STO = 0x53544F;        /* STO - set driver stop flag */
const int FLU = 0x464C55;        /* FLU - set driver flush flag */

/* VME board commands */
const int SRA = 0x535241;        /* SRA - Set reply address */
const int PRM = 0x50524D;        /* PRM - Set readoout parameters */
const int PCA = 0x504341;        /* PCA - Pixel count address */
const int IIA = 0x494941;        /* IIA - Initialise frame read */

/* timing board commands */
const int CLR = 0x434C52;        /* CLR - clear detector */
const int HGN = 0x48474E;        /* HGN - set high gain*/
const int IDL = 0x49444C;        /* IDL - set idle */
const int LGN = 0x4C474E;        /* LGN - set low gain */
const int RAD = 0x524144;        /* RAD - */
const int RDC = 0x524443;        /* RDC - run readout command */
const int RRS = 0x525253;        /* RRS - remote reset */
const int SBV = 0x534256;        /* SBV - set DC bias supply voltages */
const int STP = 0x535450;        /* STP - stop readout */
const int SGN = 0x53474E;        /* SGN - */
const int TCK = 0x54434b;        /* TCK - */
const int TDC = 0x544443;        /* TDC - */
const int SYN = 0x53594E;        /* SYN - */

/* timing board commands for IR systems */
const int DFS = 0x444653;        /* DFS - set digital filter samples */
const int SRF = 0x535246;        /* SRF - set number of reference samples */
const int SFS = 0x534653;        /* SFS - set number of fowler samples */
const int SUR = 0x535552;        /* SUR - set number of ramp periods */
const int SRM = 0x53524D;        /* SRM - select reset mode */
const int SRD = 0x535244;        /* SRD - set reset delay */
const int SNR = 0x534E52;        /* SNR - set number of resets */
const int CDS = 0x434453;        /* CDS - select sampling mode*/
const int SETEX = 0x534554;      /* SET - set exposure time */
const int RET = 0x524554;        /* RET - read exposure time */
const int STX = 0x535457;        /* STX - stop current exposure  */
const int CRT = 0x435254;        /* CRT - calibrate frame read time */
const int SBN = 0x53424E;        /* SBN - set bias number */
const int SMX = 0x534D58;        /* SMX - set MUX  */
const int CON = 0x434F4E;        /* CON - turn on all camera biases and clocks (PON) */
const int NRW = 0x4E5257;        /* NRW - set number of rows */
const int NCL = 0x4E434C;        /* NCL - set number of cols */

/* timing board commands for IR coadder board systems */
const int LCA = 0x4C4341;        /* LCA - Load CoAdder from Timing ROM */
const int DCA = 0x444341;        /* DCA - Dowload CoAdder from Timing SRAM */
const int CAT = 0x434154;        /* CAT - Test CoAdder data link */
const int ICA = 0x494341;        /* ICA - Initialize CoAdder */
const int SCA = 0x534341;        /* SCA - Setup CoAdder */

/* utility board commands */
const int ABR = 0x414252;	     /* ABR - abort current readout */
const int AEX = 0x414558;        /* AEX - abort current exposure */
const int CSH = 0x435348;        /* CSH - close shutter */
const int OSH = 0x4f5348;        /* OSH - open shutter */
const int PEX = 0x504558;        /* PEX - pause exposure */
const int PON = 0x504F4E;        /* PON - turn on all camera biases and clocks (CON) */
const int POF = 0x504F46;        /* POF - turn +/- 15V power supplies off */
const int REX = 0x524558;        /* REX - resume exposure */
const int SEX = 0x534558;        /* SEX - start exposure */
const int SYR = 0x535952;        /* SYR - */
const int TAD = 0x544144;        /* TAD - */
const int TDA = 0x544441;        /* TDA - */
const int TDG = 0x544447;        /* TDG - */
const int DON = 0x444F4E;        /* DON - Done reply */
const int DSP_ERR = 0x455252;    /* ERR - error reply */
const int TOUT = 0x544F5554;     /* TOUT - timeout reply */

const int COK = 0x434F4B;        /* COK - */

/* Extra PCI mnemonics */
const int WSI = 0x575349;        /* Write Synthetic Image */
const int RDI = 0x524449;        /* Read Image */
const int SOS = 0x534F53;        /* Select Output Source */
const int MPP = 0x4D5050;        /* Multi-Pinned Phase Mode	*/
const int SNC = 0x534E43;        /* Set Number of Coadds */
const int SSS = 0x535353;        /* Set Subarray Sizes */
const int SSP = 0x535350;        /* Set Subarray Positions */
const int SPT = 0x535054;        /* Set Pass Through Mode */
const int RCC = 0x524343;        /* Read Controller Configuration */
const int TBS = 0x544253;        /* Test Byte Swapping */
const int SBS = 0x534253;        /* Set Byte Swapping */
const int FPB = 0x465042;        /* Set the Frames-Per-Buffer for Coadds */

/* SDSU III commands */
const int ROP = 0x524F50;        /* ROP - read options word */
const int RRT = 0x525254;        /* RRT - read regions table address and size */
const int RSW = 0x525357;        /* RSW - read status word */
const int WOP = 0x574F50;        /* WOP - write options word */

/* Board type mnemonics */
const int VID = 0x564944;        /* mnemonic that means video board	*/
const int CLK = 0x434C4B;        /* mnemonic that means clock board	*/

/* Commands as enumerated type */
enum Sdsu_Cmd {
  SDSU_NULL_CMD=0,
  SDSU_GET_PROGRESS=BRR,
  SDSU_READS_DONE=RDR,
  SDSU_READ_TIME=RDT,
  SDSU_READ_RUNNING=RDP,
  SDSU_INTERFACE_RESET=RESET,
  SDSU_GET_SETUP=SBU,
  SDSU_ABORT=ABT,
  SDSU_STOP=STO,
  SDSU_FLUSH=FLU,
  SDSU_READ_STATUS=SRS,
  /* VME board commands */
  SDSU_SRA=SRA,
  SDSU_PRM=PRM,
  SDSU_PCA=PCA,
  SDSU_IIA=IIA,
  /* Boot commands */
  SDSU_LDA=LDA,
  SDSU_RDM=RDM,
  SDSU_RST=RST,
  SDSU_TDL=TDL,
  SDSU_WRM=WRM,
  /* Timing Board commands */
  SDSU_CLR=CLR,
  SDSU_HGN=HGN,
  SDSU_IDL=IDL,
  SDSU_LGN=LGN,
  SDSU_RAD=RAD,
  SDSU_RDC=RDC,
  SDSU_RRS=RRS,
  SDSU_SBV=SBV,
  SDSU_STP=STP,
  SDSU_SGN=SGN,
  SDSU_TCK=TCK,
  SDSU_TDC=TDC,
  SDSU_SYN=SYN,
  SDSU_DFS=DFS,
  SDSU_SRF=SRF,
  SDSU_SFS=SFS,
  SDSU_SUR=SUR,
  SDSU_SRM=SRM,
  SDSU_SRD=SRD,
  SDSU_SNR=SNR,
  SDSU_CDS=CDS,
  SDSU_SET=SETEX,
  SDSU_RET=RET,
  SDSU_STX=STX,
  SDSU_CRT=CRT,
  SDSU_SBN=SBN,
  SDSU_SMX=SMX,
  SDSU_CON=CON,
  SDSU_LCA=LCA,
  SDSU_DCA=DCA,
  SDSU_CAT=CAT,
  SDSU_ICA=ICA,
  SDSU_SCA=SCA,
  SDSU_NRW=NRW,
  SDSU_NCL=NCL,
  /* Utility Board commands */
  SDSU_ABR=ABR,
  SDSU_AEX=AEX,
  SDSU_CSH=CSH,
  SDSU_OSH=OSH,
  SDSU_PEX=PEX,
  SDSU_PON=PON,
  SDSU_POF=POF,
  SDSU_REX=REX,
  SDSU_SEX=SEX,
  SDSU_SYR=SYR,
  SDSU_TAD=TAD,
  SDSU_TDA=TDA,
  SDSU_TDG=TDG,
  /* Some PCI Vector commands */
  SDSU_PCI_PC_RESET = 0x8077,
  SDSU_PCI_ABORT_READOUT = 0x8079,
  SDSU_PCI_BOOT_EEPROM = 0x807B,
  SDSU_PCI_READ_HEADER = 0x81,
  SDSU_PCI_READ_REPLY_VALUE = 0x83,
  SDSU_PCI_RESET_CONTROLLER = 0x87,
  /* Extra PCI commands */
  SDSU_WSI=WSI,
  SDSU_RDI=RDI,
  SDSU_SOS=SOS,
  SDSU_MPP=MPP,
  SDSU_SNC=SNC,
  SDSU_VID=VID,
  SDSU_CLK=CLK,
  SDSU_SSS=SSS,
  SDSU_SSP=SSP,
  SDSU_SPT=SPT,
  SDSU_RCC=RCC,
  SDSU_TBS=TBS,
  SDSU_SBS=SBS,
  SDSU_FPB=FPB,
  /* SDSU III commands */
  SDSU_ROP=ROP,
  SDSU_RRT=RRT,
  SDSU_RSW=RSW,
  SDSU_WOP=WOP,
  /* Replys */
  SDSU_DON=DON,
  SDSU_ERR=DSP_ERR,
  SDSU_COK=COK,
  SDSU_TOUT=TOUT
};
// typedef enum Sdsu_Cmd Sdsu_Cmd;

/* SDSU board identifiers */
const int INTERFACE_ID = 1;
const int SBUS_ID = 1;
const int VME_ID = 1;
const int PCI_ID = 1;
const int TIM_ID = 2;
const int UTIL_ID = 3;
const int MANUAL = 4;
const int COADD_ID = 4;

/* SDSU board identifiers as an enumerated type */
enum Sdsu_Board_Type {
  SDSU_BOARD_NULL = 0,
  SDSU_INTERFACE = INTERFACE_ID,
  SDSU_TIMING = TIM_ID,
  SDSU_UTILITY = UTIL_ID,
  SDSU_COADDER = COADD_ID
};
// typedef enum Sdsu_Board_Type Sdsu_Board_Type;

const int MAX_DSP_ARGS = 7;
const int MAX_DSP_DATA_ARGS = 4;

/* Data structures for specific commands - and associated parameters */
struct Sdsu_Data_Cmd {
  int ndata;
  int data[MAX_DSP_DATA_ARGS];
};
// typedef struct Sdsu_Data_Cmd Sdsu_Data_Cmd;

struct Sdsu_Mem_Cmd {
  Sdsu_Memory_Space memory_space;
  int address;
  int value;
};
// typedef struct Sdsu_Mem_Cmd Sdsu_Mem_Cmd;

struct Sdsu_Voltage_Cmd {
  int dac_board;
  int dac_number;
  Sdsu_Dac_Board_Type dac_board_type;
  int dac_voltage;
};
// typedef struct Sdsu_Voltage_Cmd Sdsu_Voltage_Cmd;

struct Sdsu_Mux_Cmd {
  int clk_board;
  int mux1;
  int mux2;
};
// typedef struct Sdsu_Mux_Cmd Sdsu_Mux_Cmd;

struct Sdsu_Direct {
  Sdsu_Cmd cmd;
  union {
   Sdsu_Mem_Cmd mem_cmd;
   Sdsu_Voltage_Cmd voltage_cmd;
   Sdsu_Mux_Cmd mux_cmd;
   Sdsu_Data_Cmd data_cmd;
  } Sdsu_Direct_u;
};
// typedef struct Sdsu_Direct Sdsu_Direct;

struct Sdsu_Dsp {
  int board;
  Sdsu_Direct dsp_data;
};
// typedef struct Sdsu_Dsp Sdsu_Dsp;

struct Sdsu_Special_Dsp {
  int header;
  int cmd;
  int timeout;
  int nargs;
  int arg[MAX_DSP_ARGS];
};
// typedef struct Sdsu_Special_Dsp Sdsu_Special_Dsp;

/* A readout structure for SDSU  hardware*/
struct Sdsu_Readout{
  Chip_Readmode_Type read_mode;  /* Readout mode */
};
// typedef struct Sdsu_Readout Sdsu_Readout;

/* SDSU  request structure */
struct Sdsu_Controller_Request {
  Sdsu_Commands type;
  union {
  Sdsu_Dsp dsp;
  Sdsu_Special_Dsp special_dsp;
  } Sdsu_Controller_Request_u;
};
// typedef struct Sdsu_Controller_Request Sdsu_Controller_Request;


/*==============================================================================
   CCD camera and controller specific data structures
==============================================================================*/




/* CCD Camera request types */
enum CCD_Request_Type {
  CCD_CAMERA_INIT,
  CCD_CAMERA_FLUSH,
  CCD_CAMERA_READOUT,
  CCD_CAMERA_EXPOSE,
  CCD_CAMERA_FOCUS,
  CCD_CAMERA_SHUTTER,
  CCD_CAMERA_DRAWRECT,
  CCD_CAMERA_IDLING,
  CCD_CAMERA_CONTROLLER,
  CCD_CAMERA_GUIDE,
  CCD_CAMERA_INVALID
};
// typedef enum CCD_Request_Type CCD_Request_Type;

/* Standard CCD camera activity values.  Each hardware class will use these and may
   also have some hardware specific activities.  When these are used, the
   Activity should be set to  */
enum CCD_Activity_Type {
  CCD_CAMERA_ACT_NULL,
  CCD_CAMERA_ACT_INIT,
  CCD_CAMERA_ACT_COUNTER,
  CCD_CAMERA_ACT_READOUT,
  CCD_CAMERA_ACT_FOCUS,
  CCD_CAMERA_ACT_CONTROLLER
};
// typedef enum CCD_Activity_Type CCD_Activity_Type;


/* CCD config structure */
struct CCD_Config {
  int filler;
};
// typedef struct CCD_Config CCD_Config;

/* CCD init structure */
struct CCD_Init {
  Chip_Readmode_Type read_mode;  /* Readout mode */
};
// typedef struct CCD_Init CCD_Init;

/* A shutter duration structure */
struct CCD_Shutter {
  double unit;                 /* Number of sec units.      */
  int duration;                /* Shutter time = duration*unit secs */
  int dark;                    /* Don't open shutter for Dark frames */
  int open;                    /* direct open shutter flag */
};
// typedef struct CCD_Shutter CCD_Shutter;

/* latest exposure results */
struct CCD_Exposure_Info {
  int  do_readout;              /* optionally do the readout */
  int  do_flush;                /* optionally do the flush */
  double exptime;               /* requested shutter duration - seconds */
};
// typedef struct CCD_Exposure_Info CCD_Exposure_Info;




/*
enum IR_Timing_Mode {
  IR_CALC_EXPTIME,
  IR_CALC_READPERIOD,
  IR_CALC_NPERIODS
};
*/
// typedef enum IR_Timing_Mode IR_Timing_Mode;


/* IR Camera request types */
enum IR_Request_Type {
  IR_CAMERA_INIT,
  IR_CAMERA_RESET,
  IR_CAMERA_EXPOSE,
  IR_CAMERA_VIEW, 	
  IR_CAMERA_CONTROLLER,
  IR_CAMERA_GUIDE,
  IR_CAMERA_INVALID
};
// typedef enum IR_Request_Type IR_Request_Type;

/* Standard IR camera activity values.  Each hardware class will use these and may
   also have some hardware specific activities.  When these are used, the
   Activity should be set to  */
enum IR_Activity_Type {
  IR_CAMERA_ACT_NULL,
  IR_CAMERA_ACT_INIT,
  IR_CAMERA_ACT_EXPOSING,
  IR_CAMERA_ACT_READOUT,
  IR_CAMERA_ACT_CONTROLLER
};
// typedef enum IR_Activity_Type IR_Activity_Type;

/* Infrared exposure info */
struct IR_Exposure_Info {
  long doReadout;                   /* optionally do the readout */
  long doReset;                     /* optionally do the reset */
//  long setIntvl;                    /* Set directly or alternatively calculate read interval from nfowler, read_time and period */
  IR_Readmode_Type readModeSet;     /* Type of IR exposure */
  IR_Quadrants_Type quadrantsSet;   /* Are we reading full-frame or some other mode? */
//  IR_Timing_Mode timeModeSet;       /* Way of determining exposure timers */
  long nfowler;                     /* Number of Fowler samples - number of reads in a set */
//  double readTime;                  /* frame read time (secs) - time to read one frame */
//  double readIntvl;                 /* read interval time (secs) between last read in set and first read in next set */
//  double period;                    /* read period (secs) - time between first frame read in set to first in next set */
//  long nperiods;                    /* Number of read periods */
  double exposedRQ;                 /* Requested total integration time (secs) - time from first frame in first set to first frame in last set */
  double exposed;                   /* Total integration time (secs) - time from first frame in first set to first frame in last set */
//  long ncoadds;                     /* number of co-adds per readout */
  long nresets;                     /* number of detector resets */
//  double resetDelay;                /* time to wait after reset for detector to settle - seconds*/
};
// typedef struct IR_Exposure_Info IR_Exposure_Info;

/* Infrared data processing info */
struct IR_Data_Info {
  int  doSubFile;                   /* true if required to subtract image from data */
  int  save;                        /* Save data */
  int  saveVar;                     /* Save variance data */
  int  saveQual;                    /* save quality data */
  int  saveNdrs;                    /* Save each non-destructive read set average (a set is the number of fowler samples)*/
  int  displayNdrs;                 /* true if required to display each ndr */
  int  saveCoadds;                  /* Save each non-destructive read set average (a set is the number of fowler samples)*/
  int  displayCoadds;               /* true if required to display each ndr */
  int  cosmRej;                     /* perform cosmic ray rejection */
  int  doCmpIm;                     /* compress image along spectral axis and combine slices */
  double waveMin;                   /* minimum wavelength in range for performing spectral compression */
  double waveMax;                   /* maximum wavelength in range for performing spectral compression */
  double cosmThrsh;                 /* Threshold change in pixel gradient for cosmic ray detection */
  double cosmMin;                   /* Minimum change in pixel gradient for cosmic ray detection */
  Filenametype subFile;             /* name of FITS calibration file to subtract */
};
// typedef struct IR_Data_Info IR_Data_Info;


/*==============================================================================
   IR camera and controller specific data structures
==============================================================================*/

/* Infrared config structure */
struct IR_Config {
  int filler;
};
// typedef struct IR_Config IR_Config;

/* Infrared init structure */
struct IR_Init {
  int filler;
  /*  IR_Readmode_Type read_mode;*/       /* Type of IR exposure */
};
// typedef struct IR_Init IR_Init;

/* ways to calculate exposure timing intervals, 3 variables numPeriods, readPeriod,
   and ExpTime, each can be calculated from two others */


/* Include specific camera and controller definition files here */



/*==============================================================================
   Generic camera and controller data structures
==============================================================================*/

/* A camera config structure */
struct Camera_Op_Config {
  Camera_Type type;
  union {
   CCD_Config ccd;
   IR_Config ir;
  } Camera_Op_Config_u;
};
// typedef struct Camera_Op_Config Camera_Op_Config;

/* A controller config structure */
struct Controller_Config {
  Controller_Type type;
  union {
   Dummy_Config dummy;
   Sdsu_Config sdsu;
  } Controller_Config_u;
};
// typedef struct Controller_Config Controller_Config;

/* An camera init structure */
struct Camera_Init_Data {
  Camera_Type type;
  union {
   CCD_Init ccd;
   IR_Init ir;
  } Camera_Init_Data_u;
};
// typedef struct Camera_Init_Data Camera_Init_Data;

/* A controller initialisation structure */
struct Controller_Init_Data {
  Controller_Type type;
  union {
   Dummy_Init dummy;
   Sdsu_Init sdsu;
  } Controller_Init_Data_u;
};
// typedef struct Controller_Init_Data Controller_Init_Data;

/* General Camera operations */
/* Camera state structures */
struct Camera_Op_State {
  int filler;
};
// typedef struct Camera_Op_State Camera_Op_State;

/* Controller state structures */
struct Controller_State {
  int get_temps;                   /* query temperatures */
  int get_volts;                   /* query voltages */
};
// typedef struct Controller_State Controller_State;

struct Camera_State {
  Camera_Op_State camera_op;
  Controller_State controller;
};
// typedef struct Camera_State Camera_State;

/* Full init structure */
struct Init {
  int use_config;               /* Flag indicating possible associated config data */
  Camera_Init_Data camera;      /* Specific camera initialisation data */
  Controller_Init_Data controller;/* Specific controller initialisation data */
};
// typedef struct Init Init;

/* Type of image data being handled */
enum Image_Data_Type {
  RAW_DATA,
  REF_DATA,	
  QUALITY_DATA,
  VARIANCE_DATA
};
// typedef enum Image_Data_Type Image_Data_Type;

/* region information for readout */
struct Region_Data {	
  int  camera_coords;           /* true if region dimensions are camera coords, otherwise CCD*/
  unsigned short nregions;      /* number of regions - max MAX_ROI */
  unsigned long xo[MAX_ROI];    /* bot. left pix of regions (x coord) */
  unsigned long yo[MAX_ROI];    /* bot. left pix of regions (y coord)*/
  unsigned long width[MAX_ROI]; /* widths of regions  */
  unsigned long height[MAX_ROI];/* height of regions */
  unsigned short rcf;           /* readout row compression factor */
  unsigned short ccf;           /* readout column compression factor */
  unsigned long overscan_cols;  /* number of overscan columns */
  unsigned long overscan_rows;  /* number of overscan rows */
  unsigned long long chip_mask; /* which chips on a mosaic to read out */
  FitsValueType window;         /* Name of region selection for WINDOW keyword */
};
// typedef struct Region_Data Region_Data;

/* Image details */
struct Detector_Image_Info {
  int  unsigned_data;           /* Set to true if data unsigned */
  int bitpix;                   /* Number of bits per pixel for this image */
  double bscale;                /* Scaling factor for this image */
  double bzero;                 /* Offset factor for this image*/
  Image_Data_Type data_type;    /* Type of data in this image */
};
// typedef struct Detector_Image_Info Detector_Image_Info;

/* Sub frame readout specification - part of main output frame */
struct Sub_Frame_Data {	
  unsigned long xo;                     /* x offset on amp */
  unsigned long yo;                     /* y offset on amp */
  unsigned long frame_xo;               /* x offset of amp on frame*/
  unsigned long frame_yo;               /* y offset of amp on frame*/
  unsigned long camera_xo;              /* x offset of amp on camera*/
  unsigned long camera_yo;              /* y offset of amp on camera*/
  unsigned long width;                  /* image subframe width */
  unsigned long height;                 /* image subframe height */
  unsigned long width_unbinned;         /* image subframe width before binning*/
  unsigned long height_unbinned;        /* image subframe height before binning */
  unsigned long data_width;             /* image subframe width */
  unsigned long data_height;            /* image subframe height */
  unsigned long data_width_unbinned;    /* image subframe width before binning*/
  unsigned long data_height_unbinned;   /* image subframe height before binning */
  unsigned long prescan;                /* image subframe amp prescan */
  unsigned long postscan;               /* image subframe amp postscan */
  unsigned long trim_width;             /* image subframe trim width */
  unsigned long overscan_cols;          /* image subframe overscan cols */
  unsigned long overscan_rows;          /* image subframe overscan rows */
  short xdir;                           /* readout x direction  */
  short ydir;                           /* readout y direction */
  unsigned short rcf;                   /* readout row compression factor */
  unsigned short ccf;                   /* readout column compression factor */
  Readout_Orientation orient;           /* Orientation of readout, either column or row*/
  double min;                           /* minimum data value */
  double max;                           /* maximum data value */
  int controller_idx;                   /* what controller does this subframe belong to*/
  int chip_idx;                         /* what chip does this subframe belong to */
  int amp_idx;                          /* what amp does this subframe belong to */
  int frame_idx;                        /* what main output frame does this subframe belong to */
  int subframe_idx;                     /* What is this subframes idx */
};
// typedef struct Sub_Frame_Data Sub_Frame_Data;

/*
 * Information on actual readout output frames/fits extensions.
 */
struct Frame_Data {
  unsigned long camera_xo;              /* x offset of amp on camera*/
  unsigned long camera_yo;              /* y offset of amp on camera*/
  unsigned long width;                  /* image frame width */
  unsigned long height;                 /* image frame height */
  unsigned long width_unbinned;         /* image frame width before binning */
  unsigned long height_unbinned;        /* image frame height before binning */
  unsigned long data_width;             /* image frame width */
  unsigned long data_height;            /* image frame height */
  unsigned long data_width_unbinned;    /* image frame width before binning */
  unsigned long data_height_unbinned;   /* image frame height before binning */
  Detector_Image_Info image_info;       /* details of image information */
  struct {
    unsigned int subframes_len;
    Sub_Frame_Data *subframes_val;           /* array of sub frames for this frame */
  } subframes;
};
// typedef struct Frame_Data Frame_Data;





/* Controller identity info */
struct Controller_Info {
  Nametype controller_swv;         /* software running on controller*/
};
// typedef struct Controller_Info Controller_Info;

/* A general readout structure for all cameras */
struct Detector_Readout_Data {
  Controller_Type type;
  union {
   Dummy_Readout dummy;
   Sdsu_Readout sdsu;
   int filler;
  } Detector_Readout_Data_u;
};
// typedef struct Detector_Readout_Data Detector_Readout_Data;

/* Full detector readout structure */
struct Detector_Readout {
  int  verify;                     /* Set if required to compare readout image against standard */
  Detector_Image_Info image_info;  /* details of image information */
  Region_Data regions;             /* region info for this readout */
  Verify_Spec verify_spec;         /* Used for generating data that can be used to verify output data */
  Detector_Readout_Data data;      /* Specific readout data */
};
// typedef struct Detector_Readout Detector_Readout;

/* A CCD exposure structure */
struct CCD_Expose {
  CCD_Exposure_Info info;       /* exposure information */
  Detector_Reset flush;         /* flush info */
  CCD_Shutter shutter;          /* shutter info */
  Detector_Readout readout;     /* readout info */
};
// typedef struct CCD_Expose CCD_Expose;

/* A CCD guide structure */
struct CCD_Guide {
  CCD_Expose expose;            /* exposure information */
  Guider_Mode guider_mode;      /* guider mode */
};
// typedef struct CCD_Guide CCD_Guide;



/* An IR exposure structure */
struct IR_Expose {
  IR_Mode irMode;               /* current operating mode */
  int  viewEnabled;             /* set if view mode is enabled */
  IR_Exposure_Info einfo;       /* exposure parameters */
  IR_Data_Info dinfo;           /* data processing parameters */
  Detector_Readout readout;     /* readout info */
};
// typedef struct IR_Expose IR_Expose;

/* A specific camera request structure */
struct Controller_Request {
  Controller_Type type;
  union {
   Dummy_Controller_Request dummy;
   Sdsu_Controller_Request sdsu;
  } Controller_Request_u;
};
// typedef struct Controller_Request Controller_Request;

/* General CCD camera request structure */
struct CCD_Request {
  CCD_Request_Type type;
  union {
   Detector_Reset flush;
   Detector_Readout readout;
   CCD_Expose expose;
   CCD_Shutter shutter;
   Controller_Request controller;
   CCD_Guide guide;
  } CCD_Request_u;
};
// typedef struct CCD_Request CCD_Request;

/* General IR camera request structure */
struct IR_Request {
  IR_Request_Type type;
  union {
   Detector_Reset reset;
   IR_Expose expose;
   Controller_Request controller;
  } IR_Request_u;
};
// typedef struct IR_Request IR_Request;




struct Camera_Specific_Request {
  Camera_Type type;
  union {
   CCD_Request ccd;
   IR_Request ir;
  } Camera_Specific_Request_u;
};
// typedef struct Camera_Specific_Request Camera_Specific_Request;

/* Frame index array */
typedef int Frame_Index[MAX_IMAGE_FRAMES];

/* The main image header structure */
struct Detector_Image_Main_Header {
  int id;                           /* identification of requesting controller*/
  int pid;                          /* part identification */
  unsigned short byte_order;        /* Byte ordering */
  int  saveVar;                     /* Save variance data */
  int  saveQual;                    /* save quality data */
  Mosaic_Mode mosaic_mode;          /* Set to other than MOSAIC_NONE if data from multiple frames is to be formed into a mosaic */
  unsigned long camera_width;       /* Full width of camera */
  unsigned long camera_height;      /* Full height of camera */
  unsigned short ncontrollers;      /* Number of controllers */
  unsigned short controller_mask;   /* Controllers used in current readout*/
  unsigned short cam_id;            /* ID of this camera */
  unsigned short cid;               /* ID of this controller */
  unsigned short extend;            /* Number of Fits extensions for this image*/
  unsigned short phu;               /* size of primary header (FITS  blocks)*/
  unsigned short ihu;               /* size of image header (FITS  blocks)*/
  unsigned int image_seq;           /* Image sequence number */
  int image_chunk_size;             /* size in bytes of each image chunk */
  float chiptemp;                   /* Chip temperature last read */
  int  verify;                      /* Set if required to compare readout image against standard */
  Region_Data regions;              /* region info for this image */
  Filenametype client_image_name;   /* Name of image file on client host */
  int nsframes;                     /* total number of sub frames in output */
  int ndframes;                     /* total number of raw data frames in output */
  int controller_nframes[MAX_CONTROLLERS]; /* number of frames for each controller */
  Frame_Index frame_idx[MAX_CONTROLLERS];/* frame indexed by subframe - which frame does this subframe belong to?*/
  struct {
    unsigned int frames_len;
    Frame_Data *frames_val;              /* frame data for this image  - storage actually in frames-mem*/
  } frames;
};
// typedef struct Detector_Image_Main_Header Detector_Image_Main_Header;

/* The part image header structure */
struct Detector_Image_Sub_Header {
  int id;                           /* identification of requesting controller*/
  int  done;                        /* TRUE if part image is last of whole*/
  unsigned short cam_id;            /* ID of this camera */
  unsigned short cid;               /* ID of this controller */
  unsigned int image_seq;           /* Image sequence number */
  unsigned short frame;             /* Frame this data belongs to */
  unsigned short subframe;          /* Subframe this data belongs to */
  unsigned short subframe_idx;      /* index of this subframe in whole camera set of subframes */
  unsigned long x;                  /* Starting x position of image part */
  unsigned long y;                  /* Starting y position of image part */
  unsigned long width;              /* width of part image in pixels */
  unsigned long height;             /* height of part image in pixels */
  short xdir;                       /* readout x direction  */
  short ydir;                       /* readout y direction */
  Readout_Orientation orient;       /* Orientation of readout, either column or row */
};
// typedef struct Detector_Image_Sub_Header Detector_Image_Sub_Header;


/* The close image header structure */
struct Detector_Image_Close_Header {
  int id;                           /* identification of requesting controller*/
  unsigned short cam_id;            /* ID of this camera */
  unsigned short cid;               /* Controller number */
  unsigned int image_seq;           /* Image sequence number */
};
// typedef struct Detector_Image_Close_Header Detector_Image_Close_Header;

enum Detector_Image_Header_Type {
  IMAGE_MAIN_HEADER,
  IMAGE_SUB_HEADER,
  IMAGE_CLOSE_HEADER
};
// typedef enum Detector_Image_Header_Type Detector_Image_Header_Type;

/* A union of the two image header types */
struct Detector_Image_Header {
  Detector_Image_Header_Type type;
  union {
  Detector_Image_Main_Header main_header;
  Detector_Image_Sub_Header sub_header;
  Detector_Image_Close_Header close_header;
  } Detector_Image_Header_u;
};
// typedef struct Detector_Image_Header Detector_Image_Header;

/* Image Pixel structure */
struct Detector_Image_Body_Data {
  Detector_Image_Sub_Header sub_header;/* Pixel data header information */
  struct {
    unsigned int pixel_data_len;
    char *pixel_data_val;               /* image pixels. Make this bytes and must */
  } pixel_data;
				     /* be last item in structure */
};
// typedef struct Detector_Image_Body_Data Detector_Image_Body_Data;

/* Image body structure */
enum Detector_Image_Body_Type {
  IMAGE_HEADER,
  IMAGE_BODY_DATA,
  IMAGE_CLOSE
};
// typedef enum Detector_Image_Body_Type Detector_Image_Body_Type;

struct Detector_Image_Body {
  Detector_Image_Body_Type type;
  union {
  Detector_Image_Main_Header main_header;
  Detector_Image_Body_Data data;
  } Detector_Image_Body_u;
};
// typedef struct Detector_Image_Body Detector_Image_Body;

/* An image structure */
struct Detector_Image {
  int  header_ok;                     /* set when header sent ok */
  char  frames_mem[FRAMES_MEM_SIZE]; /* storage for frame_data */
  char  raw_frames_mem[FRAMES_MEM_SIZE]; /* storage for raw frame_data */
  Detector_Image_Body body;           /* image data. headers + pixels */
};
// typedef struct Detector_Image Detector_Image;


/*
 * Copyright (c) 1994-2002 by RSAA Computing Section
 * $Id: configuration.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 *
 * CICADA PROJECT
 *
 * FILENAME instrument_status.x
 *
 * PURPOSE
 *  Structures to hold full instrument status information.
 *
 */




/* Status data from instrument processes - inst_ctl, slave and data */
struct Instrument_Ctl_Status {
  long progress;                        /* progress counter */
  long target;                          /* completion target */
  long iteration;                       /* iteration number in a sequence */
};
// typedef struct Instrument_Ctl_Status Instrument_Ctl_Status;


/* CCD status information */
struct CCD_Status {
  CCD_Request_Type req_type;            /* current request */
  CCD_Activity_Type activity;           /* current activity */
  Detector_Reset reset;                 /* reset info */
  Chip_Readmode_Type read_mode;         /* Readout mode */
};
// typedef struct CCD_Status CCD_Status;

/* Infrared status information */
struct IR_Status {
  IR_Request_Type req_type;             /* current request */
  IR_Activity_Type activity;            /* current activity */
  IR_Exposure_Info view;                /* parameters for view mode */
  IR_Exposure_Info obs;                 /* parameters for observe mode */
  IR_Data_Info d_view;                  /* parameters for view mode */
  IR_Data_Info d_obs;                   /* parameters for observe mode */
  int  prep;                            /* Boolean set if exposure in preparation*/
  int  acq;                             /* Boolean set if acquiring data (exposing)*/
  int  rdout;                           /* Boolean set if reading out detector*/
  long irProgress;                      /* progress during an exposure - units of rows read*/
  long irTarget;                        /* target number of rows to read for an exposure*/
  long nreads;                          /* total number of NDRs required for this exposure (nperiods+1)*/
  long nreadsDone;                      /* number of ndrs completed */
  long ncoaddsDone;                     /* number of coadds completed in this exposure sequence */
  long nfsDone;                         /* number of fowler samples completed */
  double timeLeft;                      /* time remaining in this exposure - seconds */
  double perTimeLeft;                   /* time remaining in this period - seconds */
  double expElapsed;                    /* time done in this exposure - seconds */
  IR_Mode irMode;                       /* current operational state */
  int  viewEnabled;                     /* set if view mode is enabled */
};
// typedef struct IR_Status IR_Status;


/* Dummy controller status information*/
struct Dummy_Status {
  long exposure;                        /* exposure counter in msecs*/
};
// typedef struct Dummy_Status Dummy_Status;

/* See also Activity in configuration header file */
enum Sdsu_Activity {
  SDSU_NULL,
  SDSU_TEMP
};
// typedef enum Sdsu_Activity Sdsu_Activity;

/* SDSU status information*/
struct Sdsu_Status {	
  long exposure;                        /* exposure counter in msecs*/
  int n_volts;                          /* Number of voltages set */
  Nametype voltage_name[SDSU_MAX_VOLTS];/* Voltage name */
  double voltage_value[SDSU_MAX_VOLTS]; /* Actual voltage value */
  int sdsu_command;                     /* Requested command to SDSU */
  int sdsu_command_done;                /* Actual last command to SDSU */
  int last_reply;                       /* Actual last reply from SDSU */
  int expected_reply;                   /* Expected reply from SDSU */
  int last_header;                      /* Actual last header from SDSU */
  int expected_header;                  /* Expected header from SDSU */
  int reply_status;                     /* Result code from last sdsu operation */
  int  controller_pon;                  /* set true if SDSU power on */
  Nametype controller_swv;              /* software running on controller*/
  SmallMsgType status_message;          /* last dsp command status (eg DON, ERR, TOUT etc) */
};
// typedef struct Sdsu_Status Sdsu_Status;




/* Camera controller status */
struct Controller_Status {
  Controller_Type type;
  union {
    Dummy_Status dummy;
    Sdsu_Status sdsu;
  } Controller_Status_u;
};
// typedef struct Controller_Status Controller_Status;


/* Camera operation status */
struct Camera_Op_Status {
  Camera_Type type;
  union {
    CCD_Status ccd;
    IR_Status ir;
  } Camera_Op_Status_u;
};
// typedef struct Camera_Op_Status Camera_Op_Status;



/* Guider mode status */
struct Guider_Status {
  Guider_Mode guider_mode;              /* Mode the guider is operating in */
  double offset_x;
  double offset_y;	
  long offset_counter;	
};
// typedef struct Guider_Status Guider_Status;


/* Camera mode status */
struct Camera_Mode_Status {
  Camera_Mode mode;
  union {
    Guider_Status guider;	
  } Camera_Mode_Status_u;
};
// typedef struct Camera_Mode_Status Camera_Mode_Status;

/* General camera status information*/
struct Camera_Status {
  Nametype name;                        /* Name of the camera configuration*/
  int cid;                              /* ID of camera owning this status */
  int  exposure_done;                   /* Set when exposure finishes */
  int  verified;                        /* Set when test pattern verification succeeds */
  double elapsed;                       /* Elapsed time between first open shutter and final close (seconds)*/
  double shutter_duration;              /* Set if new shutter duration requested seconds*/
  double exposedRQ;                     /* Set to indicate asked for duration (msec)*/
  double exposed;                       /* Set to indicate actual duration (msec)*/
  long shutter_open_time;               /* system time in seconds when shutter opened */
  long do_readout;                      /* is a readout required after exposure */
  long controller_simulation;           /* Flag that indicates controller simulation mode */
  double transfer_time;                 /* msecs to transfer image to shared mem */
  double total_operation_time;          /* total lapsed msecs for last operation */
  double readout_time[MAX_READOUT_BUFFERS];/* last readout time - msecs*/
  double readout_wait_time;             /* last readout wait time - msecs*/
  FitsValueType dateobs;                /* UT date when data acquisition started */
  FitsValueType utstart;                /* UT when data acquisition started */
  FitsValueType utend;                  /* UT when data acquisition ended */
  double tai;                           /* International atomic time TAI at utstart */
  Camera_Op_Status op_status;           /* specific camera operation status  */
  Camera_Mode_Status mode_status;       /* specific camera mode status */
  Controller_Status controller_status;  /* specific camera controller status  */
};
// typedef struct Camera_Status Camera_Status;

/* Specific hardware status structure */
struct Hardware_Status {
  Hardware_Type type;
  union {
    Camera_Status camera_status;
  } Hardware_Status_u;
};
// typedef struct Hardware_Status Hardware_Status;

/* Instrument status consists of instrument process state and instrument hardware state*/
struct Instrument_Status {
  int id;                               /* Identification of camera */
  int pid;                              /* Identification of instrument part */
  Instrument_Ctl_Status inst_ctl_status;/* Observer process status data */
  Hardware_Status hardware_status;      /* Specific hardware status structure*/
};
// typedef struct Instrument_Status Instrument_Status;




/* Configuration data for a camera */
struct Camera_Config {
  Camera_Desc desc;                    /* full description of camera */
  int cam_id;                          /* id of current camera */
  int cid;                             /* id of current controller */
  Camera_Op_Config camera;             /* configuration for camera type CCD|IR|TTT|Guider */
  Controller_Config controller;        /* configuration of current controller */
};
// typedef struct Camera_Config Camera_Config;

/* Configuration data */
struct Config_Data {
  Hardware_Type type;
  union {
  Camera_Config camera;              /* each camera description */
  } Config_Data_u;
};
// typedef struct Config_Data Config_Data;

/* Configuration data and description */
struct Config {
  Config_Data config_data;           /* Configuration data */
};
// typedef struct Config Config;




#endif

