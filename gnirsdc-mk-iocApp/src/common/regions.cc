/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  ipc.cc
 * 
 * GENERAL DESCRIPTION
 * Implementation of detector regions of interest (ROI) base  class.
 * This class provides a mechanism for handling detector region specifications 
 * and their relationship with detector readout amplifiers
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 1997
 *
 * HISTORY
 *   Sept 1997 PJY    Created for RSAA CICADA package
 *   Sept 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */

// Include files 
#include <errno.h>
#include <list>
#include <stdio.h>
#include "utility.h"
#include "exception.h"
#include "regions.h"

// defines and constants
// typedefs 

// class declarations

// global variable declarations

// global function definitions

// local function declarations

//class function definitions

/*
 *+ 
 * FUNCTION NAME: Regions::Regions
 * 
 * INVOCATION: rgn = new Regions()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Simple Regions constructor
 * 
 * DESCRIPTION: Does nothing
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Regions::Regions()
{
  debug=1;
  reg_xo = NULL;
  reg_yo = NULL;;
  reg_width = NULL;
  reg_height = NULL;
}
/*
 *+ 
 * FUNCTION NAME: Regions::Regions
 * 
 * INVOCATION: rgn = new Regions(unsigned short n, unsigned long *xo, unsigned long *width, 
 *	                         unsigned long *yo, unsigned long *height)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > n - number of regions in spec
 * > xo - array of x offsets
 * > yo - array of y offsets
 * > width - array of width specs
 * > height - array of height specs
 *
 * FUNCTION VALUE:  None
 * 
 * PURPOSE: Regions constructor
 * 
 * DESCRIPTION: Takes a set of ROI specs in form of x,y offsets and
 * width,height, and stores in internal structures
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- */
Regions::Regions(unsigned short n, unsigned long *xo, unsigned long *width, 
		 unsigned long *yo, unsigned long *height)
{
  int i;
  debug=1;

  nregions = (n>MAX_REGIONS)? MAX_REGIONS : n;
  reg_xo = new unsigned long[nregions];
  reg_yo = new unsigned long[nregions];
  reg_width = new unsigned long[nregions];
  reg_height = new unsigned long[nregions];

  for (i=0; i<nregions; i++) {
    reg_xo[i] = xo[i];
    reg_width[i] = width[i];
    reg_yo[i] = yo[i];
    reg_height[i] = height[i];
  }
}

/*
 *+ 
 * FUNCTION NAME: Regions::~Regions
 * 
 * INVOCATION: delete rgn
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Regions destructor
 * 
 * DESCRIPTION: Frees any allocated memory
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Regions::~Regions()
{
  if (reg_xo)
    delete [] reg_xo;
  if (reg_yo)
    delete [] reg_yo;
  if (reg_width)
    delete [] reg_width;
  if (reg_height)
    delete [] reg_height;
}

/*
 *+ 
 * FUNCTION NAME: Regions::read_regs
 * 
 * INVOCATION: rgn->read_regs(const char * region_file, unsigned long xmax, unsigned long ymax)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > region-file - name of file to read region spec from
 * > xmax - maximum allowed x coordinate
 * > ymax - maximum allowed y coordinate
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Reads regions spec from disk
 * 
 * DESCRIPTION: Read region descriptions from file - formatted using ximtool
 * marker conventions.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Regions::read_regs(const char * region_file, unsigned long xmax, unsigned long ymax)
{
  char line[MSGLEN];
  char *p; 
  char shape[20];
  int i,n;
  float xc,yc,xr,yr;
  ostringstream ostr;
  Reg_Spec *rs;
  list<Reg_Spec*> reg_list;
  
  nregions = 0;
  ifstream in(region_file);
  if (!in) {
    ostr << "Cannot open " << region_file << " for reading regions!" << ends;
    throw Error(ostr,E_WARNING,errno,__FILE__,__LINE__);
  }
  while (!in.eof()) {
    in.getline(line, MSGLEN);          // read in a line
    p = line;
    while (*p == ' ') p++;             // skip leading blanks
    if (strlen(p) && (p[0] != '#')) {  // skip null lines and # lines
       if ((n=sscanf(p,"%s %d %f %f %f %f",shape,&i,&xc,&yc,&xr,&yr)) == 6) {
	 // shape needs to be rectangle or box
	 if (strcmp(shape,"rectangle") &&
	     strcmp(shape,"box")) continue;

	 // Looks legal, add region
	 // if left/bottom edge of rect is <0, make it 0
	 rs = new Reg_Spec;
	 rs->rxo =  ((int)(xc - xr) < 0) ? 0 : (unsigned long)(xc - xr);
	 rs->ryo =  ((int)(yc - yr) < 0) ? 0 : (unsigned long)(yc - yr); 
	 rs->rw = ((xr*2)>xmax) ? xmax : (unsigned long)xr * 2; 
	 rs->rh = ((yr*2)>ymax) ? ymax : (unsigned long)yr * 2; 

	 // note that ximtool reports crap x/y coords when marked region is out
	 // of the wcs. So, any regions marked outside the wcs will be converted
	 // by the code below to random (but legal) regions on the
	 // image. Bummer.
	 rs->rw=(rs->rw<0)?0:rs->rw;
	 rs->rh=(rs->rh<0)?0:rs->rh;

	 // Add read region to list 
	 if ((rs->rw>0) && (rs->rh>0)) {
	   nregions++; 
	   reg_list.insert(reg_list.begin(),rs);
	 } else
	   delete rs;
	 if (nregions==MAX_REGIONS) 
	   break;
       }
    }
  }
  in.close();

  // Now move region specs from list to arrays.
  reg_xo = new unsigned long[nregions];
  reg_yo = new unsigned long[nregions];
  reg_width = new unsigned long[nregions];
  reg_height = new unsigned long[nregions];

  for (i=0; i<nregions; i++) {
    rs = reg_list.front();
    reg_xo[i] = rs->rxo;
    reg_width[i] = rs->rw;
    reg_yo[i] = rs->ryo;
    reg_height[i] = rs->rh;
    reg_list.pop_front();
    delete rs;
  }
  
}
/*
 *+ 
 * FUNCTION NAME: Regions::write
 * 
 * INVOCATION: rgn->write(const char * region_file)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > region-file - name of file to write region spec to
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Writes regions spec to disk
 * 
 * DESCRIPTION: write region descriptions to file - formatted using ximtool
 *  marker conventions.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- */
void Regions::write(char* region_file)
{
  float xc,yc,xr,yr;
  int i;
  ostringstream ostr;

  ofstream out(region_file);
  if (!out) {
    ostr << "Cannot open " << region_file << " for writing regions!" << 
      " errno = " << errno << ends;
    throw Error(ostr,E_WARNING,errno,__FILE__,__LINE__);
  } else {
    out << "# CICADA region descriptions" << endl;
    out << "# Format shape i xcentre ycentre half_height half_width rotation" << endl;
    out << "#" << endl;
    for (i=0; i<nregions; i++){
      xr = reg_width[i]/2;
      xc = reg_xo[i]+xr;
      yr = reg_height[i]/2;
      yc = reg_yo[i]+yr;
      out << "rectangle 1 " << xc << " " << yc << " " << xr << " " << yr 
	  << " " << 0.0 << endl;
    }
    out.close();
  }
}
