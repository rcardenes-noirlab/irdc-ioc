/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *   .cc
 * 
 * GENERAL DESCRIPTION
 *   Simple test program to test unravelling and reorienting column oriented 
 * pixel stream. Data used is small simple array of chars, so that it is easy
 * to view output at different stages of processing.
 *
 * ORIGINAL AUTHOR: 
 *  Mark Jarnyk & Peter Young 2002
 *
 * HISTORY
 */

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/

#include <iostream>
#include "camera.h"
#include "exception.h"
using namespace std;

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: print_array
 * 
 * INVOCATION: print_array(const char* title, unsigned char *dp, int nr, int nc)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > title - name of array printed
 * > dp - ptr to data array
 * > nr - number of rows
 * > mc - number of cols
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: writes a matrix of chars to stdout
 * 
 * DESCRIPTION: Simply prints out array - origin of output is top left
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: Keep arrays small !
 *
 *-------------------------------------------------------------------------*/
void print_array(const char* title, unsigned char *dp, int nr, int nc)
{
  int r,c;

  if ((nr>100)||(nc>100)) {
    cout << "Array too big for sensible output!" << endl;
    return;
  }

  cout << title << endl;
  for (r=0; r<nr; r++) {
    cout << " ";
    for (c=0; c<nc; c++) {
      cout << (char)dp[r*nc+c];
    }
    cout << endl;
  }
  cout << endl;
  
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: set_array
 * 
 * INVOCATION: set_array(unsigned char *dp, int nr, int nc, int xd, int yd)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * < dp - ptr to data array
 * > nr - number of rows
 * > mc - number of cols
 * > xd - x read direction
 * > yd - y read direction
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Sets up a column oriented array
 * 
 * DESCRIPTION: Forms an array as it would be read out in column orientation from 
 * amplifier in one of four corners specified by readout directions
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: Keep arrays small !
 *
 *-------------------------------------------------------------------------*/
void set_array(unsigned char* dp, int nr, int nc, int xd, int yd)
{
  int r,c,i,ir,ic;
  int xo,yo;
  xo = (xd<0)? nc-1:0;
  yo = (yd<0)? nr-1:0;
  i = 0;
  for (c=0; c<nc; c++) {
    ic = xo + xd * c;
    for (r=0; r<nr; r++) {
      ir = yo + yd * r;
      dp[ir*nc+ic] = 'A' + i++;
    }
  }
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: cmp_array
 * 
 * INVOCATION: cmp_array(unsigned char* inp, unsigned char* outp, int n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > inp - ptr to first data array
 * > outp - ptr to second data array
 * > n - number of pixels
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Compares two arrays for equality
 * 
 * DESCRIPTION: Writes Either 'Success' or 'Failed' if arrays are equal
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: Keep arrays small !
 *
 *-------------------------------------------------------------------------*/
void cmp_array(unsigned char* inp, unsigned char* outp, int n)
{    
  bool failed = false;
  int i;
  for (i=0; i<n; i++)
    if (inp[i] != outp[i]) {
      failed = true;
      break;
    }
  if (failed) 
    cout << "Failed!" << endl;
  else
    cout << "Success!" << endl;
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: main
 * 
 * INVOCATION: unravel
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: int
 * 
 * PURPOSE: Test unravelling/reorienting column oriented data
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: Not designed for multiplexed data from different amps
 *
 *-------------------------------------------------------------------------*/
#ifdef vxWorks
int unravelTest()
#else
int main () 
#endif
{
  try {
    unsigned char *inp, *bufp, *outp;
    int rows;
    int cols;
    int n;
    int r,i,slice,a;
    int xdir[4] = {1,1,-1,-1};
    int ydir[4] = {1,-1,-1,1}; 

    cout << "Enter #rows: ";
    cin >> rows;
    cout << "Enter #cols: ";
    cin >> cols;
    cout << "Enter # in slice of rows to process at a time: ";
    cin >> slice;
    if (slice>rows)
      slice = rows;
    
    n = rows*cols;

    inp = new unsigned char[n];
    bufp = new unsigned char[n];
    outp = new unsigned char[n];

    // Four readout amplifier cases:
    cout << "Origin is top, left - ie 90deg flip to usual images in these displays" << endl;
    cout << "Case 1, amp top, left (1, 1)" << endl;
    cout << "Case 2, amp bottom left (1, -1)" << endl;
    cout << "Case 3, amp bottom right (-1, -1)" << endl;
    cout << "Case 4, amp top right (-1, 1)" << endl;
    for (a=0; a<4; a++) {
      cout << "CASE " << a+1 << ": amp (" << xdir[a] << "," << ydir[a] << ")" << endl;
      set_array(inp, cols, rows, xdir[a], ydir[a]);
      print_array("Orig", inp, cols, rows);
      
      // reform array in order of clocking - ie a stream, this is always identical
      for (i=0; i<n; i++)
	inp[i] = 'A' + i;
      print_array("Clocked", inp, rows, cols);

      memset(bufp,0,n);
      memset(outp,0,n);
      
      // Now read out the array a slice at a time, reorienting pixels in the output buffer
      for (r=0; r<rows; r+=slice) {
	unravel_pix(inp+r*cols, bufp, 1, cols, slice, cols, xdir[a], ydir[a]);    
	print_array("Buf", bufp, slice, cols);
	reorient_pix(bufp, outp, r, rows, slice, cols, xdir[a], ydir[a]);
      }
      print_array("Final", outp, cols, rows);

      // Now compare original and final arrays
      set_array(inp, cols, rows, xdir[a], ydir[a]);
      cmp_array(inp,outp,n);
      cout << endl;
    }

    delete inp;
    delete outp;
    delete bufp;
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return 0;
}



