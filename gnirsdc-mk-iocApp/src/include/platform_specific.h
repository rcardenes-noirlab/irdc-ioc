/* Copyright (c) 2003 by RSAA Computing Section 
 *
 * CICADA PROJECT
 *
 * Platform specific data structures and classes
 *
 */

#ifndef _PLATFORM_SPECIFIC_H
#define _PLATFORM_SPECIFIC_H

// Include files 

// Constants 

//+ The prefixes of the database objects relative to hardware_status
#include "dc_epics_db.h"
//+ Check for EPICS database prefix definitions - if not defined
//+ The prefixes of the XDR database objects relative to hardware_status
static const char HARDWARE_PREFIX[] = DCSADTOP;
static const char CAMERA_PREFIX[] = DCSADTOP;
static const char GUIDER_PREFIX[] = DCSADTOP;
static const char IR_PREFIX[] = DCSADTOP;

//+ The prefixes of XDR objects relative to controller_status
static const char CONTROLLER_PREFIX[] = DCSADTOP;
static const char SDSU_PREFIX[] = DCSADTOP;
static const char DUMMY_PREFIX[] = DCSADTOP;

//+ For all SAD 
static const char DB_PREFIX[] = DCSADTOP;

//+ All suffixes are Null string for now
static const char DB_SUFFIX[] = ".VAL";

// typedefs 

// function declarations

// class declarations

/*
 * CLASS
 *   Platform_Specific
 *
 * DESCRIPTION
 *   Base class for platform specific data that must be passed through to
 *   all hardware constructors
 *
 * KEYWORDS
 *
 */
class Platform_Specific
{
};


#endif //_PLATFORM_SPECIFIC_H 
