[schematic2]
uniq 17
[tools]
[detail]
w 1128 1075 100 0 n#1 ecad20.seqView.OUTK 1072 1072 1184 1072 hwout.hwout#449.outp
w 1128 1139 100 0 n#2 ecad20.seqView.OUTJ 1072 1136 1184 1136 hwout.hwout#475.outp
w 1128 1203 100 0 n#3 ecad20.seqView.OUTI 1072 1200 1184 1200 hwout.hwout#472.outp
w 1128 1267 100 0 n#4 ecad20.seqView.OUTH 1072 1264 1184 1264 hwout.hwout#448.outp
w -24 1923 100 0 n#5 embbos.view_readMode.OUT -336 1920 288 1920 288 1824 656 1824 656 1744 752 1744 ecad20.seqView.A
w 1128 1331 100 0 n#6 ecad20.seqView.OUTG 1072 1328 1184 1328 hwout.hwout#442.outp
w 1128 1395 100 0 n#7 ecad20.seqView.OUTF 1072 1392 1184 1392 hwout.hwout#441.outp
w 1128 1459 100 0 n#8 ecad20.seqView.OUTE 1072 1456 1184 1456 hwout.hwout#440.outp
w 1128 1587 100 0 n#9 ecad20.seqView.OUTC 1072 1584 1184 1584 hwout.hwout#438.outp
w 1128 1715 100 0 n#10 ecad20.seqView.OUTA 1072 1712 1184 1712 hwout.hwout#436.outp
w 1960 547 100 0 n#11 elongins.seqViewID.VAL 1920 544 2000 544 ecars.seqViewC.ICID
w 1096 1939 100 0 VAL ecad20.seqView.VAL 1072 1936 1120 1936 1120 2048 1328 2048 outhier.VAL.p
w 1248 243 100 0 VAL cadPlus.seqViewP.STAT 1296 240 1200 240 1200 1936 1120 1936 junction
w 1184 307 100 0 n#12 ecad20.seqView.STLK 1072 304 1296 304 cadPlus.seqViewP.STIN
w 1184 275 100 0 n#13 ecad20.seqView.SPLK 1072 272 1296 272 cadPlus.seqViewP.SPIN
w 1256 1907 100 0 MESS ecad20.seqView.MESS 1072 1904 1440 1904 outhier.MESS.p
w 576 1907 100 0 CLID inhier.CLID.P 400 1904 752 1904 ecad20.seqView.ICID
w 464 2051 100 0 DIR inhier.DIR.P 304 2048 624 2048 624 1936 752 1936 ecad20.seqView.DIR
w 276 1747 100 0 n#14 estringouts.view_exposed.OUT -56 1744 608 1744 608 1680 752 1680 ecad20.seqView.B
w 300 1283 100 0 n#15 embbos.view_quadrants.OUT 176 1280 424 1280 424 1040 752 1040 ecad20.seqView.L
w 1082 1018 -100 0 n#16 ecad20.seqView.OUTL 1072 1008 1184 1008 hwout.hwout#479.outp
s 1808 1424 100 0 sequence which copies the values to the SAD.
s 1808 1456 100 0 is issued. This START directive triggers the view_ss
s 1808 1488 100 0 once all have been verified and a START directive
s 1808 1520 100 0 The new parameters are only copied to the SAD
s -624 -96 200 0 They are stored in the gnirs_dc_par class object.
s -624 -32 200 0 Up to 20 observation parameters can be set here
s -624 32 200 0 View mode sequence parameters Apply/CAD record
[cell use]
use estringouts -312 1687 100 0 view_exposed
xform 0 -184 1760
use hwout 1184 1095 100 0 hwout#475
xform 0 1280 1136
p 1280 1127 100 0 -1 val(outp):$(sadtop)view_readIntvl PP NMS
use hwout 1184 1159 100 0 hwout#472
xform 0 1280 1200
p 1280 1191 100 0 -1 val(outp):$(sadtop)view_readTime PP NMS
use hwout 1184 1671 100 0 hwout#436
xform 0 1280 1712
p 1280 1703 100 0 -1 val(outp):$(sadtop)view_readModeSet PP NMS
use hwout 1184 1543 100 0 hwout#438
xform 0 1280 1584
p 1280 1575 100 0 -1 val(outp):$(sadtop)view_resetDelay PP NMS
use hwout 1184 1415 100 0 hwout#440
xform 0 1280 1456
p 1280 1447 100 0 -1 val(outp):$(sadtop)view_period PP NMS
use hwout 1184 1351 100 0 hwout#441
xform 0 1280 1392
p 1280 1383 100 0 -1 val(outp):$(sadtop)view_nperiods PP NMS
use hwout 1184 1287 100 0 hwout#442
xform 0 1280 1328
p 1280 1319 100 0 -1 val(outp):$(sadtop)view_ncoadds PP NMS
use hwout 1184 1223 100 0 hwout#448
xform 0 1280 1264
p 1280 1255 100 0 -1 val(outp):$(sadtop)view_exposedRQ PP NMS
use hwout 1184 1031 100 0 hwout#449
xform 0 1280 1072
p 1280 1063 100 0 -1 val(outp):$(sadtop)view_timeModeSet PP NMS
use embbos -560 2000 100 0 view_readMode
xform 0 -464 1920
p -608 1760 100 0 1 ONST:BRIGHT
p -608 1696 100 0 1 THST:VERY_FAINT
p -608 1728 100 0 1 TWST:FAINT
p -608 1792 100 0 1 ZRST:VERY_BRIGHT
use elongins 1728 640 100 0 seqViewID
xform 0 1792 560
use eborderC -784 -313 100 0 eborderC#87
xform 0 896 992
p 1792 -160 100 768 -1 author:Peter Young
p 1792 -20 40 0 -1 comment1:2001-02-02 Modified for GNIRS. Peter Young
p 1792 -192 100 768 -1 date:2000-11-02
p 2016 -112 200 768 -1 file:dcSeqView.sch
p 2288 -160 100 0 -1 page:1
p 2400 -160 100 0 -1 pages:1
p 2064 -160 100 0 -1 revision:0
p 2016 -48 150 768 -1 system:Gemini GNIRS DC
use cadPlus 1328 352 100 0 seqViewP
xform 0 1376 256
p 1296 144 100 0 1 set1:cad seqViewP
use inhier 384 1863 100 0 CLID
xform 0 400 1904
use inhier 288 2007 100 0 DIR
xform 0 304 2048
use ecars 2080 640 150 0 seqViewC
xform 0 2160 464
p 2000 272 100 0 1 def(FLNK):$(top)combCars12.PROC
use ecad20 832 1984 150 0 seqView
xform 0 912 1104
p 848 1872 100 0 0 CTYP:12
p 848 1680 100 0 1 FTVA:LONG
p 848 1648 100 0 1 FTVB:DOUBLE
p 848 1616 100 0 1 FTVC:DOUBLE
p 848 1584 100 0 1 FTVD:LONG
p 848 1552 100 0 1 FTVE:DOUBLE
p 848 1520 100 0 1 FTVF:LONG
p 848 1488 100 0 1 FTVG:LONG
p 848 1456 100 0 1 FTVH:DOUBLE
p 848 1424 100 0 1 FTVI:DOUBLE
p 848 1392 100 0 1 FTVJ:DOUBLE
p 848 1360 100 0 1 FTVK:LONG
p 848 1328 100 0 1 FTVL:LONG
p 848 1296 100 0 0 FTVM:LONG
p 848 1264 100 0 0 FTVN:LONG
p 848 1040 100 0 0 INAM:dcCADseqInit
p 848 204 100 0 1 SNAM:dcCADseq
p 592 112 100 0 0 def(INPA):0
p 592 80 100 0 0 def(INPB):0
p 592 48 100 0 0 def(INPC):0.0
p 592 16 100 0 0 def(INPD):1
p 592 -16 100 0 0 def(INPE):5
p 592 -48 100 0 0 def(INPF):1
p 592 -80 100 0 0 def(INPG):1
p 592 -112 100 0 0 def(INPH):0
p 592 -144 100 0 0 def(INPI):1
p 592 -176 100 0 0 def(INPJ):5
p 592 -208 100 0 0 def(INPK):5
p 592 -240 100 0 0 def(INPL):0
p 592 -272 100 0 0 def(INPM):0
use outhier 1296 2007 100 0 VAL
xform 0 1312 2048
use outhier 1408 1863 100 0 MESS
xform 0 1424 1904
use embbos -59 1188 100 0 view_quadrants
xform 0 48 1280
p -283 1170 100 0 1 ONST:1_QUADRANT
p -284 1196 100 0 1 ZRST:FULL_FRAME
use hwout 1184 992 100 0 hwout#479
xform 0 1280 1008
p 1280 999 100 0 -1 val(outp):$(sadtop)view_quadrantsSet PP NMS
[comments]
