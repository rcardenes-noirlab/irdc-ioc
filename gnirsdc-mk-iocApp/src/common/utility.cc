/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME
 *  utility.cc
 *
 * GENERAL DESCRIPTION
 *  Library of C++ utility routines
 *
 * ORIGINAL AUTHOR:
 *  Peter Young - RSAA 1993
 *
 * HISTORY
 *
 */

/* Include files */
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>
#include <errno.h>
#include <signal.h>
#include <errno.h>
#include <algorithm>
#include "utility.h"
#include "exception.h"
#include <epicsThread.h>

using namespace std;

/* defines */
#define MAX_LINE 500

/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/* class function definitions */


// Utility functions

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: dbyte_swap
 *
 * INVOCATION: dbyte_swap(double d)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > d - input value
 *
 * FUNCTION VALUE: double
 *
 * PURPOSE: swap double routine from little-endian to big-endian
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES: None.
 *
 * PRIOR REQUIREMENTS: None.
 *
 * DEFICIENCIES:
 *
 *-------------------------------------------------------------------------*/
double dbyte_swap(double d)
{ int tmp;
  union {
    double yd;
    int yi[2];
  } y;
  y.yd=d;
  tmp = y.yi[0];
  y.yi[0] = lbyte_swap(y.yi[1]);
  y.yi[1] = lbyte_swap(tmp);
  return(y.yd);
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: fbyte_swap
 *
 * INVOCATION: fbyte_swap(float f)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > f - input value
 *
 * FUNCTION VALUE: float
 *
 * PURPOSE: swap float routine from little-endian to big-endian
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES: None.
 *
 * PRIOR REQUIREMENTS: None.
 *
 * DEFICIENCIES:
 *
 *-------------------------------------------------------------------------*/
double fbyte_swap(float f)
{ int tmp;
  union {
    float yf;
    unsigned short yi[2];
  } y;
  y.yf=f;
  tmp = y.yi[0];
  y.yi[0] = sbyte_swap(y.yi[1]);
  y.yi[1] = sbyte_swap(tmp);
  return(y.yf);
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: byte_order
 *
 * INVOCATION: byte_order()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: Byte_Order_Type
 *
 * PURPOSE: Function to return the byte ordering of the current machine
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES: None.
 *
 * PRIOR REQUIREMENTS: None.
 *
 * DEFICIENCIES:
 *
 *-------------------------------------------------------------------------*/
Byte_Order_Type byte_order()
{
  union {
    char c[2];
    unsigned short i;
  } two_bytes;
  two_bytes.i = 1;
  if (two_bytes.c[0]) return (WORD_LITTLE_ENDIAN);
  else return (WORD_BIG_ENDIAN);
}


/*
 *+
 * FUNCTION NAME: rest
 *
 * INVOCATION: rest(int interval)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > interval - time to rest in msec
 *
 * FUNCTION VALUE: time actually slept
 *
 * PURPOSE: Routine that rests for n msecs
 *
 * DESCRIPTION: Uses nanosleep to do small rest but will round down to nearest
 * clock resolution - returns time attempted to rest based on resolution
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
int rest(int interval)
{
	// TODO: In theory, rest returns the "actual" rested interval
	//       depending on the granularity of the system, but I haven't
	//       seen it used anywhere.
	epicsThreadSleep(double(interval) / 1000);

	return interval;
}

/*
 *+
 * FUNCTION NAME: rest_micro
 *
 * INVOCATION: rest_micro(int interval)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > interval - time to rest in µsec
 *
 * FUNCTION VALUE: time actually slept
 *
 * PURPOSE: Routine that rests for n µsecs
 *
 * DESCRIPTION: Uses nanosleep to do small rest but will round down to nearest
 * clock resolution - returns time attempted to rest based on resolution
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
int rest_micro(int interval)
{
	// TODO: In theory, rest returns the "actual" rested interval
	//       depending on the granularity of the system, but I haven't
	//       seen it used anywhere.
	epicsThreadSleep(double(interval) / 1000000);

	return interval;
}

//+
/*
 *+
 * FUNCTION NAME: get_current_ut()
 *
 * INVOCATION: ut = get_current_ut(double& rawt, double& tai, char* buffer, int len);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * < rawt - raw UT time measured as seconds since jan 1 1970
 * < tai - raw time onverted into a Modified Julian Date using TAI (international atomic time)
 * ! buffer - buffer optionally provided to store result
 * > len - length of buffer
 *
 * FUNCTION VALUE: returns string of form HH:MM:SS.s
 *
 * PURPOSE: Get the current UT (uses UTC time system),
 *
 * DESCRIPTION:
 *
 * Gets the current time and converts it into a UTC time formatted as a string
 * with HH:MM:SS.s format. Uses the current time to get an MJD value using TAI
 * system.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: For vxWorks uses Gemini timeLib
 *
 * DEFICIENCIES:
 *
 *-
 */
char* get_current_ut(double& rawt, double& tai, char* buffer, int len)
{
  static char ut_buffer[MSGLEN];
  char* ut = (buffer != NULL)? buffer: ut_buffer;
  int l = (buffer != NULL)? len: MSGLEN;

  /* Does nothing for now */
  strncpy(ut,"00:00:00.0",l);
  rawt = tai = 0.0;

  return ut;
}

static
std::string trim_decimals(string s) {
	auto pos = s.find_last_not_of("0");
	return pos <= s.length() ? s.substr(0, pos + 1) : "0";
}

std::string left_justify(string s, int size, char fillchar) {
	int nfill = size - s.length();
	return nfill > 0 ? string(nfill, fillchar) + s : s;
}

std::string to_ut_date(epicsTime timestamp)
{
	gm_tm_nano_sec ut_nanos(timestamp);
	char target[11];

	strftime(target, 11, "%Y-%m-%d", &ut_nanos.ansi_tm);

	return std::string(target);
}

std::string to_ut_time(epicsTime timestamp)
{
	gm_tm_nano_sec ut_nanos(timestamp);
	char target[11];
	unsigned long uSec = static_cast<unsigned long>(round(ut_nanos.nSec / 1000.));
	if (uSec > 999999)
		uSec = 999999;

	strftime(target, 9, "%H:%M:%S", &ut_nanos.ansi_tm);

	return std::string(target) + "." + trim_decimals(left_justify(to_string(uSec), 6, '0'));
}

/*
 *+
 * FUNCTION NAME: timestamp_res
 *
 * INVOCATION: timestamp_res()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: long
 *
 * PURPOSE: returns the time stamp resolution in parts per second
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
static constexpr auto NANOSECOND = 1000000000;
long timestamp_res()
{
  // epicsTimeStamp type resolution is in nanoseconds
  return NANOSECOND;
}

static constexpr epicsTimeStamp NullEpicsTime({0, 0});

bool operator!(const epicsTime& stamp) {
//  return stamp == NullEpicsTime;
  return stamp == NullEpicsTime;
}

double operator/(const epicsTime& stamp, double divisor) {
	epicsTimeStamp eStamp = stamp;
	return (eStamp.secPastEpoch + (eStamp.nsec / NANOSECOND)) / divisor;
}

void reset_timestamp(epicsTime& stamp) {
  stamp = NullEpicsTime;
}

// Some utility string functions

/*
 *+
 * FUNCTION NAME: stringtonum
 *
 * INVOCATION: stringtonum(char *str, numeric& val)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > str - input string
 * ! val - converted value
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Converts a string to a number of appropriate type
 *
 * DESCRIPTION:
 * Overloaded to deal with all numeric types. Handles octal, decimal and
 * hexidecimal strings
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void stringtonum(const char* str, short& val)
{
  val = (short) strtol(str, (char **)NULL, 0);
}
void stringtonum(const char* str, unsigned short& val)
{
  val = (unsigned short) strtol(str, (char **)NULL, 0);
}
void stringtonum(const char* str, int& val)
{
  val = (int) strtol(str, (char **)NULL, 0);
}
void stringtonum(const char* str, unsigned int& val)
{
  val = (unsigned int) strtoul(str, (char **)NULL, 0);
}
void stringtonum(const char* str, long& val)
{
  val = strtol(str, (char **)NULL, 0);
}
void stringtonum(const char* str, unsigned long& val)
{
  val = strtoul(str, (char **)NULL, 0);
}
void stringtonum(const char* str, long long& val)
{
  int i;
  //val = strtoll(str, (char **)NULL, 0);
  i=sscanf(str,"%lld", &val);
}
void stringtonum(const char* str, unsigned long long& val)
{
  int i;
  //val = strtoull(str, (char **)NULL, 0);
  i=sscanf(str,"%llu", &val);
}
void stringtonum(const char* str, float& val)
{
  val = atof(str);
}
void stringtonum(const char* str, double& val)
{
  val = atof(str);
}
/*
 *+
 * FUNCTION NAME: parse_list
 *
 * INVOCATION: len = parse_list(RWCString list, int* ilist, int n)
 * len = parse_list(RWCString list, double* ilist, int n)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * list - string input list
 * ilist - ptr to integer array to contain result
 * n - length of ilist
 *
 * FUNCTION VALUE: lenght of parsed list of integers
 *
 * PURPOSE: Parses comma and dash separated list of integers
 *
 * DESCRIPTION:
 * Takes a list of comma separated or dash ranged integers
 * eg 1,4,6-12,15-20,24
 * and places included values in array
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
template<typename T> int parse_list(string list, T* vlist, int n)
{
  Tokenizer tlist(list);
  string t,t2;
  int c=0,j;
  T v;
  int dash=FALSE;

  while ((!(t = tlist(" \t\n,")).empty()) && (c<n)) {
    Tokenizer range(t);
    dash=FALSE;
    while ((!(t2 = range("-")).empty()) && (c<n)) {
      stringtonum(t2.c_str(), v);
      if ((dash) && (c>0)) {
	for (j=(int)vlist[c-1]; j<v; j++)
	  vlist[c++] = j+1;
      } else
	vlist[c++] = v;
      dash = TRUE;
    }
  }
  return (c);
}
template int parse_list<int>(string list, int* vlist, int n);
template int parse_list<double>(string list, double* vlist, int n);
/*
 *+
 * FUNCTION NAME: Tokenizer::operator()
 *
 * INVOCATION: Tokenizer(const string str, size_t l)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: string
 *
 * PURPOSE: returns tokenised string
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
string Tokenizer::operator()(const string str, size_t l)
{
  size_t i,j,tmp;
  bool hasws = false;
  if (l==0)
    l = str.length();
  //Skip leading delimeters
  while (actual<string2tokenize.size()) {
    for (i=0; i<l; i++) {
      if (string2tokenize[actual]==str[i])
	hasws = true;
    }
    if (hasws) {
      actual++;
      hasws = false;
    } else
      break;
  }

  for (j=actual; j<string2tokenize.size(); j++) {
    for (i=0; i<l; i++) {
      if (string2tokenize[j]==str[i])
	break;
    }
    if (i<l)
      break;
  }
  if (j!=string2tokenize.size()) {
    tmp=actual;
    actual=j+1;
    return string2tokenize.substr(tmp,j-tmp);
  } else {
    tmp=actual;
    actual=j;
    return string2tokenize.substr(tmp,j-tmp);
  }
}
/*
 *+
 * FUNCTION NAME: strip_str
 *
 * INVOCATION: strip_str(string& str, Strip_Type stripType, char c)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Strip white-space from string
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void strip_str(string& str, Strip_Type stripType, char c)
{
  unsigned int i,j;
  if (str.length()==0)
    return;
  string retVal = str;
  switch (stripType) {
  case STRIP_LEADING: {
    for (i=0; i<str.length(); i++)
      if (str[i] != c) break;
    retVal = str.substr(i,str.length()-i);
  } break;
  case STRIP_TRAILING:{
    for (j=str.length()-1; j>=0; j--)
      if (str[j] != c) break;
    retVal = str.substr(0,j+1);
  } break;
  case STRIP_BOTH: {
    for (i=0; i<str.length(); i++)
      if (str[i] != c) break;
    string tmp(str.substr(i,str.length()-i));
    for (j=tmp.length()-1; j>=0; j--)
      if (tmp[j] != c) break;
    retVal = tmp.substr(0,j+1);
  }
  }
  str = retVal;
}

template <class UnaryOperator>
string str_transform(string str, UnaryOperator fn) {
  string out(str.length(), ' ');
  transform(str.begin(), str.end(), out.begin(), fn);
  return out;
}

/*
 *+
 * FUNCTION NAME: str_tolower
 *
 * INVOCATION: str_tolower(string str)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: A string with lowercase "str".
 *
 * PURPOSE: Change string to lower case
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
string str_tolower(string str) {
  return str_transform<>(str, [](unsigned char c){ return tolower(c); });
}

/*
 *+
 * FUNCTION NAME: str_toupper
 *
 * INVOCATION: str_toupper(string& str)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: A string with uppercase "str".
 *
 * PURPOSE: Change string to upper case
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
string str_toupper(string str) {
  return str_transform(str, [](unsigned char c){ return toupper(c); });
}

/*
 *+
 * FUNCTION NAME: regx_match
 *
 * INVOCATION: regx_match( string str, string pat, int& len)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > str - input string to match
 * > pat - regular expression
 * < len - length of match
 *
 * FUNCTION VALUE: index of first pattern matched
 *
 * PURPOSE: To provide regular expression searches to StandardK    *
*           STL string objects.
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int regx_match(const string &str, const string &pat, int& len)
{
  regex_t regexp;
  int status = 0;
  char *err_msg = NULL;
  regmatch_t match;
  int pos=0;

  len = 0;

  // Compile the regular expression pattern
  if ((status = regcomp( &regexp, pat.c_str(), REG_EXTENDED)) != 0 ){
    if (status == REG_ESPACE )
      throw Error("Out of memory when calling regcomp",E_ERROR,status,__FILE__,__LINE__);
    else {
      // Get the size of the message.
      int err_msg_sz = regerror( status, &regexp, NULL, (size_t) 0 );

      // Allocate the memory, print the message,
      // and then free the memory.
      if ((err_msg = (char *) malloc( err_msg_sz )) != NULL ) {
	regerror( status, &regexp, err_msg, err_msg_sz );
	string error = err_msg;
	free( err_msg );
	err_msg = NULL;
	error += "\nRegular expression = \"";
	error += pat;
	error += "\"";
	throw Error(error.c_str(),E_ERROR,status,__FILE__,__LINE__);
      } else {
	string invalid = "Invalid regular expression:  ";
	invalid += pat;
	throw Error(invalid.c_str(),E_ERROR,status,__FILE__,__LINE__);
      }
    }
  }

  // Search for the regular expression in the string
  status = regexec( &regexp, str.c_str(), (size_t)1, &match, 0 );

  if (!status) {
    // Matched
    pos = (int) match.rm_so;
    len = (int) (match.rm_eo - match.rm_so);
  }

  // Free the memory allocated by the regex.h functions.
  regfree( &regexp );

  // Now handle error conditions
  if (status == REG_NOMATCH)
    return -1;
  else if (status != 0 ) {
    if (status == REG_ESPACE)
      throw Error("Out of memory when calling regcomp",E_ERROR,status,__FILE__,__LINE__);
    else{
      // Get the size of the message.
      int err_msg_sz = regerror( status, &regexp, NULL, (size_t) 0 );

      // Allocate the memory, print the message, and then free the memory.
      if (( err_msg = (char *) malloc( err_msg_sz ) ) != NULL ){
	regerror( status, &regexp, err_msg, err_msg_sz );
	string error = err_msg;
	free( err_msg );
	err_msg = NULL;
	error += "\nRegular expression = \"";
	error += pat;
	error += "\"";
	throw Error(error.c_str(),E_ERROR,status,__FILE__,__LINE__);
      } else {
	string invalid = "Invalid regular expression:  ";
	invalid += pat;
	throw Error(invalid.c_str(),E_ERROR,status,__FILE__,__LINE__);
      }
    }
  }

  return pos;
}

//+ vxWorks does not have nint function - Supply it here
int nint(double d)
{
  return (int) floor(d+0.5);
}
int nint(float f)
{
  return (int) floor((double)f+0.5);
}


/*
 *+
 * FUNCTION NAME: strlcpy()
 *
 * INVOCATION: strlcpy(char* dst, const char* src, size_t dstsize)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > dst - the destination buffer
 *  > src - the source buffer
 *  > dstsize - the size of the destination buffer
 *
 * FUNCTION VALUE: strlen(src)
 *
 * PURPOSE:
 *
 * DESCRIPTION: An implementation of strlcpy for those platforms where
 *  it is not natively available.  It performs a similar task to strncpy
 *  but ensures that the destination buffer is always null terminated.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 */
size_t strlcpy(char* dst, const char* src, size_t dstsize)
{
  size_t len = strlen(src);
  size_t ret = len;
  if (dstsize > 0) {
    if (len >= dstsize) {
      len = dstsize-1;
    }
    memcpy(dst, src, len);
    dst[len] = 0;
  }
  return ret;
}

/*
 *+
 * FUNCTION NAME: strlcpy()
 *
 * INVOCATION: strlcpy(char* dst, const string& src, size_t dstsize)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > dst - the destination buffer
 *  > src - the source buffer
 *  > dstsize - the size of the destination buffer
 *
 * FUNCTION VALUE: strlen(src)
 *
 * PURPOSE:
 *
 * DESCRIPTION: An overloaded version of strlcpy that allows copying
 *  data from a regular C++ string.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 */
size_t strlcpy(char *dst, const string &src, size_t dstsize) {
  return strlcpy(dst, src.c_str(), dstsize);
}

/*
 *+
 * FUNCTION NAME: strlcat()
 *
 * INVOCATION: strlcat(char* dst, const char* src, size_t dstsize)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > dst - the destination buffer
 *  > src - the source buffer
 *  > dstsize - the size of the destination buffer
 *
 * FUNCTION VALUE: strlen(dst)+strlen(src)
 *
 * PURPOSE:
 *
 * DESCRIPTION: An implementation of strlcat for those platforms where
 *  it is not natively available.  It performs a similar task to strncat
 *  but ensures that the destination buffer is always null terminated.
 *  Also, the third parameter is the size of the destination rather than
 *  the number of characters to copy.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 */
size_t strlcat(char *dst, const char *src, size_t dstsize)
{
  size_t len1 = strlen(dst);
  size_t len2 = strlen(src);
  size_t ret = len1 + len2;

  if (len1 < dstsize - 1) {
    if (len2 >= dstsize - len1) {
      len2 = dstsize - len1 - 1;
    }
    memcpy(dst+len1, src, len2);
    dst[len1+len2] = 0;
  }
  return ret;
}

size_t strlcat(char *dst, const string &src, size_t dstsize) {
  return strlcat(dst, src.c_str(), dstsize);
}
