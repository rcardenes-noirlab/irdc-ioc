/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *   common.cc
 * 
 * GENERAL DESCRIPTION
 * General purpose routines commonly used in most modules.
 *   
 *
 * ORIGINAL AUTHOR: 
 * Peter Young      1995  
 *
 * HISTORY
 *
 */

/* Include files */
#include "configuration.h"
#include "common.h"
#include "utility.h"
#include "exception.h"
#include "epicsTime.h"

/* defines */
/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/* global function definitions */


/*
 *+ 
 * FUNCTION NAME: heartbeat
 * 
 * INVOCATION: heartbeat(int& heart)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * ! heart - value of heartbeat - updated on exit
 * 
 * FUNCTION VALUE: int
 * 
 * PURPOSE: Heartbeat function increment 
 * 
 * DESCRIPTION: Updates heartbeat if elapsed time has changed by 0.5s. Heart
 * is in 0.5s units. Wraps when heart reaches 1 million.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
// 
int heartbeat(int& heart)
{
  int ref;
  static epicsTime ts0;                    // starting point for this process
  epicsTime ts = epicsTime::getCurrent();  // in nano seconds
  if (!ts0)
    ts0 = ts;
  ref = (int) (ts/100000000)%1000000; // in .1s units modulo 1 million 
  // heart beat in 0.5s units, increment if another 0.5 s elapsed
  if ((ref > heart*5 + 5) || (ref < heart*5)) {
    heart = (ref/5 > 0)? ref/5:1;
  }
  return heart;
}

/*
 *+ 
 * FUNCTION NAME: imstat
 * 
 * INVOCATION: imstat(void *ptr, int np, short bitpix, double& sum, double& min, 
 *	              double& max)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > ptr - image data pointer
 * > np - number of pixels in image
 * > bitpix - number of bits per pixel (FITS convention)
 * < sum - pixel sum
 * < min - min pixel value
 * < max - max pixel value
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Get stats for image 
 * 
 * DESCRIPTION: Interprets bitpix parameter to determine image data type and
 * calls appropriate template method.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void imstat(void *ptr, int np, short bitpix, double& sum, double& min, double& max)
{
  /* following used for debugging */
  unsigned char bmin,bmax;
  short smin,smax;
  int imin,imax;
  float fmin,fmax;
  double dmin,dmax;

  switch (bitpix) {
  case BYTE_BITPIX:
    sum_minmax((unsigned char*) ptr, sum, bmin, bmax, np);
    min = bmin;
    max = bmax;
    break;
  case SHORT_BITPIX:
    sum_minmax((short *) ptr, sum, smin, smax, np);
    min = smin;
    max = smax;
    break;
  case INT_BITPIX:
    sum_minmax((int *) ptr, sum, imin, imax, np);
    min = imin;
    max = imax;
    break;
  case FLOAT_BITPIX:
    sum_minmax((float *) ptr, sum, fmin, fmax, np);
    min = fmin;
    max = fmax;
    break;
  case DOUBLE_BITPIX:
    sum_minmax((double *) ptr, sum, dmin, dmax, np);
    min = dmin;
    max = dmax;
    break;
  default:
    break;
  }
}


/*
 *+ 
 * FUNCTION NAME: get_version_string
 * 
 * INVOCATION: get_version_string(const char* version_str)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > version_str - version string in cvs $Name:  $ format
 * 
 * FUNCTION VALUE: stripped version string
 * 
 * PURPOSE: Strips a CVS Name tag string of CVS related info
 * 
 * DESCRIPTION: Takes a CVS $Name:  $ expanded string and strips out the
 * CVS bits to produce a tagged version number. It also converts '_' to
 * '.' characters to get around the CVS string limitations.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
char* get_version_string(const char* version_str)
{
  const char CVS_NAME[] = "$Name: ";
  static string str;
  int offset=strlen(CVS_NAME)-1;

  str = version_str;

  // Strip the CVS keyword
  str = str.substr(offset, strlen(version_str)-2-offset);
  // Swap _ for .
  while ((offset=str.find("_",0)) != -1)
    str.replace(offset, 1, ".");
  // Remove the C from the string eg CX.Y.Z
  while ((offset=str.find("C",0)) != -1)
    str.erase(offset, 1);
  return (char*)str.c_str();
}
