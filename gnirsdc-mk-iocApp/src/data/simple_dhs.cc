/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 * $Id
 * GNIRS PROJECT
 * 
 * FILENAME 
 *   simple_dhs.cc
 * 
 * GENERAL DESCRIPTION
 *   Program to provide a simple test of the DHS system with timings.
 *   Sends sample data of selectable size to DHS, eg 4 1kx1k frames of
 *   intensity pixels. Based on ICD 3.
 *
 * ORIGINAL AUTHOR: 
 *   Peter Young
 *
 * HISTORY

 *INDENT-ON
 *
 * $Log: simple_dhs.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Gnirs; first gemini-modified release
 *
 * Revision 1.1  2004/03/19 01:51:50  pjy
 * Added
 *
 *
 -------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#ifdef vxWorks
#include <vxWorks.h>
#endif

#include <time.h>
#include <stdio.h>
#include "dhs.h"

/*---------------------------------------------------------------------------
 External functions
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 External variables
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Static variables
 -------------------------------------------------------------------------*/

static DHS_CONNECT dhsConnection;  // DHS connection pointer
static char *instrument;

/*--------------------------------------------------------------------------
 Global variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Class definitions
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Non-member functions
 -------------------------------------------------------------------------*/

static void checkDhs(const char* label, int dhsResult, const char* fname, int lineno);
static void dhsErrorCallback(DHS_CONNECT,DHS_STATUS,DHS_ERR_LEVEL,char *,DHS_TAG,void *);
void simple_dhs_init(const char *cname, const char *hostip, const char *server,
		     const char* inst);
void simple_transfer_dhs(unsigned long rows, unsigned long cols, int frames);


/*+--------------------------------------------------------------------------
 * FUNCTION NAME: testDhsInit()
 * 
 * INVOCATION: testDhsInit
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Initialises a connection to the DHS
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
#ifndef vxWorks
int main() 
{
  simple_dhs_init("gnirs_data", "150.203.89.131", "dataServerNS", "GNIRS");
  simple_transfer_dhs(1024,1024,1);
  return 0;
}
#endif
void simple_dhs_init(const char *cname, const char *hostip, const char *server,
		     const char* inst)
{
  DHS_STATUS dhsResult;              // DHS library status value
  DHS_THREAD dhsThreadId;            // DHS thread ID. 
  int eventLoop = 0;                 // set TRUE if eventLoop is started
  instrument = (char*)inst;

  try {
    printf("In dhsInitTest...\n");  

    dhsResult = DHS_S_SUCCESS;

    // Now initialise the DHS library with space for 1 connection
    printf("calling dhsInit...\n");  
    dhsInit(cname, 10, &dhsResult);
    checkDhs("dhsInit", dhsResult, __FILE__, __LINE__);
    
    // Set up callback functions for errors and puts
    printf("calling dhsCallbackSet...\n");  
    dhsCallbackSet(DHS_CBT_ERROR, (DHS_CB_FN_PTR)&dhsErrorCallback, &dhsResult);
    checkDhs("dhsCallbackSet:ErrorCallback", dhsResult, __FILE__, __LINE__);
    
    // Start the eventloop
    printf("calling dhsEventLoop...\n");  
    dhsEventLoop(DHS_ELT_THREADED, &dhsThreadId, &dhsResult);
    checkDhs("dhsEventLoop", dhsResult, __FILE__, __LINE__);
    eventLoop = 1;
    
    // Make a connection to the DHS data server (this is the service name  - not
    // the DHS server hostname!)   
    printf("calling dhsConnect with IP=%s, name=%s\n",hostip, server);  
    dhsConnection = dhsConnect(hostip, server, NULL, &dhsResult);
    checkDhs("dhsConnect", dhsResult, __FILE__, __LINE__);
    printf("connection made = %d\n",dhsConnection);
  }
  catch (int& dce) {
    printf("DhsInit caught error at line %d...\n",dce);  
    if (eventLoop) 
      dhsEventLoopEnd(&dhsResult);
    dhsExit(&dhsResult);
  }
  
  printf("dhsInitTest done...\n");  

}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: simple_transfer_dhs()
 * 
 * INVOCATION: simple_transfer_dhs
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Transfers a data set to the DHS
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void simple_transfer_dhs(unsigned long rows, unsigned long cols, int frames)
{
  DHS_STATUS dhsResult;              // DHS library status value
  char *contrib[1];                  // List of contributors to the data
  char* dataLabel;                   // Unique name returned by DHS
  char *qlStreams[2];                // List of streams to send data to
  char label[50];                    // Dataset name
  DHS_BD_DATASET dataSet;            // Id of the dataset
  DHS_BD_FRAME intensity[4];         // intensity frames for each quadrant
  unsigned long axisSize[2];         // Size of a frame
  unsigned long dims[1];             // Array to contain data dimensions
  char *axisLabel[2];                // Labels for each axis
  float *intensityData[4];
  long origin[2];                    // Origin of frame

  DHS_TAG putTag;                    // tag returned by the put function
  DHS_CMD_STATUS sendStatus;         // Final status of the put
  char *msg;                         // failure message from server
  unsigned int i,j,k;
  struct timespec tps,tpf;           // Timestamps
  double ts,tf;                      // times in seconds
  int status,q;
  unsigned long xOrigin[4] = {1, cols, 1, cols}; // Simple linear 
  unsigned long yOrigin[4] = {1, 1, rows, rows};// arrangement of frames
  bool do_ql = true;

  try {
    printf("In transferDhs...\n"); 

    if (frames>4) {
      printf("Cannot handle %d frames - max is 4\n", frames); 
      return;
    }
    
    // Get timestamp
    status = clock_gettime(CLOCK_REALTIME,&tps);

    dhsResult = DHS_S_SUCCESS;
    
    // Get a unique name for this dataset from the DHS
    dataLabel = dhsBdName(dhsConnection, &dhsResult);
    checkDhs("dhsBdName", dhsResult, __FILE__, __LINE__);
    
    printf("got dataSet label: %s\n", dataLabel);  

    // Set up the control information for the dataset
    contrib[0] = instrument;
    qlStreams[0] = instrument;
    dhsBdCtl(dhsConnection, DHS_BD_CTL_LIFETIME, dataLabel, DHS_BD_LT_PERMANENT);
    dhsBdCtl(dhsConnection, DHS_BD_CTL_CONTRIB, dataLabel, 1, contrib);
    if (do_ql)
      dhsBdCtl(dhsConnection, DHS_BD_CTL_QLSTREAM, dataLabel, 1, qlStreams);
    
    printf("Set up the control information...\n");  

    // Fill in the other components of the dataset name
    sprintf(label, "%s.0.0", dataLabel);
    
    // Create a new dataset
    dataSet = dhsBdDsNew(&dhsResult);
    checkDhs("dhsBdDsNew", dhsResult, __FILE__, __LINE__);
    
    printf("Created a new dataset...\n");  

    // Fill in the dataset attributes
    dhsBdAttribAdd(dataSet, "instrument", DHS_DT_STRING, 0, NULL, instrument, &dhsResult);
    checkDhs("dhsBdAttribAdd:instrument", dhsResult, __FILE__, __LINE__);
    
    // Create frames for each of the detector quadrants
    for (q=0; q<frames; q++) {
      // Create the intensity frame
      axisSize[0] = cols;
      axisSize[1] = rows;
      intensity[q] = dhsBdFrameNew(dataSet, "Intensity", q, DHS_DT_FLOAT, 2, axisSize,
				(const void **) &intensityData[q], &dhsResult);
      checkDhs("dhsBdFrameNew:intensity", dhsResult, __FILE__, __LINE__);
      
      // Fill in the intensity attributes
      origin[0] = xOrigin[q];  
      origin[1] = yOrigin[q];
      dims[0] = 2;
      dhsBdAttribAdd(intensity[q], "dataType", DHS_DT_STRING, 0, NULL, "Intensity", &dhsResult);
      checkDhs("dhsBdAttribAdd:dataType", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(intensity[q], "units", DHS_DT_STRING, 0, NULL, "Photons", &dhsResult);
      checkDhs("dhsBdAttribAdd:units", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(intensity[q], "origin", DHS_DT_INT32, 1, dims, origin, &dhsResult);
      checkDhs("dhsBdAttribAdd:origin", dhsResult, __FILE__, __LINE__);
      dhsBdAttribAdd(intensity[q], "axisSize", DHS_DT_UINT32, 1, dims, axisSize, &dhsResult);
      checkDhs("dhsBdAttribAdd:axisSize", dhsResult, __FILE__, __LINE__);
      printf("Created intensity frame %d\n", q);  
       
      // Put the data into the data arrays
      for (i=0; i<axisSize[0]; i++)
	for (j=0; j<axisSize[1]; j++) {
	  k = (i*axisSize[1])+j;
	  intensityData[q][k] = (float) k;
	}
    }
    
    // Send the data to the DHS server. This is the last data for the dataset, so the last
    // parameter is set to TRUE.
    putTag = dhsBdPut(dhsConnection, label, DHS_BD_PT_DS, DHS_TRUE, dataSet, NULL, &dhsResult);
    checkDhs("dhsBdPut", dhsResult, __FILE__, __LINE__);
    
    printf("Sent data to the DHS server\n");  
    
    // Wait for the data to be received
    dhsWait(1, &putTag, &dhsResult);    
    checkDhs("dhsWait", dhsResult, __FILE__, __LINE__);
    
    sendStatus = dhsStatus( putTag, &msg, &dhsResult);    
    checkDhs("dhsStatus", dhsResult, __FILE__, __LINE__);
    
    if (sendStatus != DHS_CS_DONE) {
      printf (" DHS put failed: connection=%d label=%s\n",
		(int) dhsConnection, label);
      if (msg != NULL)
	printf (" DHS put reason: %s\n", msg);
    } else
      printf("Data sent OK\n");        

    // Free tag and data
    dhsTagFree(putTag, &dhsResult);    
    checkDhs("dhsTagFree", dhsResult, __FILE__, __LINE__);

    dhsBdDsFree(dataSet, &dhsResult);    
    checkDhs("dhsTagFree", dhsResult, __FILE__, __LINE__);

    // Now report time taken for full operation
    status = clock_gettime(CLOCK_REALTIME,&tpf);
    tf = (double)tpf.tv_sec + tpf.tv_nsec/1000000000.0;
    ts = (double)tps.tv_sec + tps.tv_nsec/1000000000.0;
    printf("Time for DHS transfer= %e seconds, tpf=%d,tps=%d\n", tf-ts, tpf.tv_sec, tps.tv_sec);
  }
  catch (int& dce) {
    printf ("TransferDhs failure at line %d\n", dce);
    dhsTagFree(putTag, &dhsResult);    
    dhsBdDsFree(dataSet, &dhsResult);    
  }
  printf("Done transfer to DHS\n");        
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: checkDhs
 * 
 * INVOCATION: checkDhs(char* label, int dhsResult, char* fname, int lineno)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: checks DHS status value and reports if error
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
static void checkDhs(const char* label, int dhsResult, const char* fname, int lineno)
{
  if (dhsResult != DHS_S_SUCCESS) {
    printf("Error in DHS call %s, status=%d\n", label, dhsResult);
    throw lineno;
  }
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: dhsErrorCallback
 * 
 * INVOCATION: dhsErrorCallback(DHS_CONNECT     connect,
			  DHS_STATUS      errorNum,
			  DHS_ERR_LEVEL   errorLev,
			  char *          msg,     
			  DHS_TAG         tag,     
			  void *          userData)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Callback for DHS to use on error
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
static void dhsErrorCallback(DHS_CONNECT     connect,
			     DHS_STATUS      errorNum,
			     DHS_ERR_LEVEL   errorLev,
			     char *          msg,     
			     DHS_TAG         tag,     
			     void *          userData)
{
   printf ("Data:  DHS error callback: connection=%d errNum=%d level=%d \"%s\"\n",
             (int) connect, (int) errorNum, (int) errorLev, msg);
}
