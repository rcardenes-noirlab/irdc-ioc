
/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2005 by RSAA Computing Section 
 * CICADA PROJECT
 * 
 * FILENAME 
 *   osmic_ray_test.cc
 * 
 * GENERAL DESCRIPTION
 *   Simple test program to test cosmic ray removal
 *
 * ORIGINAL AUTHOR: 
 *  Peter Young 2005
 *
 * HISTORY
 */

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/

#include <iostream>
#include <math.h>
using namespace std;

static double f;
static double fsy = 0;
static double fsiy = 0;
static double fsy2 = 0;
static double v;
static double fs = 0;
static int badPix = 0;
static int cPix=0;
static const double T = 1.1;
static const double Tmin = 5.0;

static void cm(int pn, unsigned short pix, bool last) 
{
  double n,ns,k,k0,k1,k2,yavg,y,dy,diff,sy,ys,m,mdivmpn,ndivmpn,vs;
  static unsigned short z;
  int i;
  bool skipPix;

  // For the first NDR - just record the frame - this is our zero point
  if (pn==1) {
    
    z = pix;      
    
  } else {
    // calculate number of samples in sy - we want average delta y to last sample
    // remember the first sample is at T0 and is zero (or just noise - or perhaps a cosmic ray?)
    n = pn-cPix-1; // Exclude first ndr and also current ndr
    ns = (n-1)/2*n; // ns is the actual number of summed pixel accumulations before this NDR
    
    for (i=0; i<1; i++) { // do loop once in this test code
      
      if (badPix) 
	continue;

      skipPix = false;

      // Subtract zero point - it may have been a cosmic ray hit?
      y = pix - z;
      
      if (n>1) {
	// Handle cosmic rays?
	// Do this by checking that the new photon rate hasn't changed
	// significantly more than the previous average rate
	yavg = fsy/ns; 
	
	// calculate new delta y, remember that each new data point is cumulated charge
	dy = y - yavg*(n-1);
	
	// difference
	diff = fabs(dy-yavg);
	
	// Use min of two to check threshold
	ys = (dy<yavg)? dy:yavg;
	
	// Threshold is given as factor of ys - we want to check slope change is less than 
	// the threshold and it is as least as much as a minimum
	if ((diff>ys*T) && (diff>Tmin)) {
	  // In case of 3rd NDR has reduced signifying that a cosmic ray may have been
	  // captured during the 2nd NDR
	  if ((n==2) && (dy<yavg)) {
	    if (cPix == 0) {
	      z = pix;
	      fsy = fsy2 = fsiy = 0.0;
	      cPix = (int)n;
	    } else 
	      badPix = pn-1;
	  } else {
	    if (cPix == 0) {
	      z = pix;
	      vs = v;
	      fs = f;
	      fsy = fsy2 = fsiy = v = 0.0;
	      cPix = (int)n;
	    } else {
	      badPix = pn;
	    }
	  }
	  // Continue to next loop iteration - found cosmic ray
	  skipPix = true;
	}
      }
      
      if (!skipPix) {
	// Pixel ok - do cumulative sums and calculate slope & variance
	// Calculate average pixel value
	// Now time calculations using simplified formulae
	k0 = (n + 1)/2.0;
	k1 = (n / 12.0) * (n * n - 1);
	
	// photon rate is in units ADU/s - set this here (period is in seconds)
	k2 = 10 * k1; // period = 10
	
	// Make sure values are sensible - ie ensure no div by zero
	if (n>1 && (k1==0.0 || k2==0.0)) {
	  cout << "IR_Camera: Linear fitting constants (k1&k2) must be greater than zero!" << endl; 
	  return;
	}
	
	fsy += y;
	fsy2 += y*y;
	fsiy += n*y;
	
	if (n>1) {
	  k = fsiy-k0*fsy; 
	  f = k / k2;
	  sy = fsy;
	  v = fsy2 - (sy*sy / n) - (k*k / k1);
	}      
      }

      // If last NDR compute values if cosmic ray hit
      if (last && cPix>0) {
	m = cPix;
	mdivmpn = m/(m+n);
	ndivmpn = n/(m+n);
	f = mdivmpn*fs + ndivmpn*f;
	v = mdivmpn*mdivmpn*vs + ndivmpn*ndivmpn*v;
      }

    }

  }

}

#ifdef vxWorks
int cosmicRayTest()
#else
int main()
#endif
{
  int n,cn,i,cn2;
  unsigned short pix,cpix;
  const unsigned short cosmic = 1000;
  cout << "Enter n:"; cin >> n;
  cout << "Enter pixel value:"; cin >> pix;
  cout << "Enter cosmic ray NDR:"; cin >> cn;
  cout << "Enter 2nd cosmic ray NDR:"; cin >> cn2;
  
  cpix = 0;
  for (i=1; i<n+2; i++) {
    if (cn==i)
      cpix+=cosmic;
    else if (cn2==i)
      cpix+=cosmic;
    cm(i,cpix,i==n+2);
    cpix+=pix;
  }
  
  cout << "1st cosmic ray detected on NDR=" << cPix+1 << " 2nd on " << badPix << endl;
  cout << "Slope fit=" << f << " Var=" << v << endl;
  return 0;
}


    
