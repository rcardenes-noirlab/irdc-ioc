/*
 * Copyright (c) 1994-2002 by MSSSO Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME sdsu_setup.cc
 * 
 * GENERAL DESCRIPTION
 * Implements an SDSU preferences class using base Preferences class
 *
 */

/* Include files */
#include "common.h"
#include "sdsu_setup.h"

/*
 *+ 
 * FUNCTION NAME: Sdsu_Setup::Sdsu_Setup()
 * 
 * INVOCATION: Sdsu_Setup setup;
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: A constructor for sdsu setup file
 * 
 * DESCRIPTION: Initialises setup values to sensible defaults
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Sdsu_Setup::Sdsu_Setup():
  Preferences("sdsu.setup", 1, true, "")
{
  int i;

  Sdsu_Config_Data tsdsu_config = {
    DEFAULT_DO_SIMULATION,
    DEFAULT_DO_COMMS_TEST,
    DEFAULT_DO_LONG_HARDWARE_TEST,
    DEFAULT_DO_SHORT_HARDWARE_TEST,
    DEFAULT_DO_SDSU_RESET, 
    DEFAULT_DO_POWER_ON, 
    DEFAULT_HAS_UTILITY_BOARD, 
    DEFAULT_DO_TARGET_CHIP_TEMP, 
    DEFAULT_DO_HEATER_ADU, 
    DEFAULT_DO_GAIN, 
    DEFAULT_DO_TC, 
    DEFAULT_DO_AB, 
    DEFAULT_DO_IDLE,
    DEFAULT_DO_TIMING_DOWNLOAD, 
    DEFAULT_DO_UTILITY_DOWNLOAD, 
    DEFAULT_DO_INTERFACE_DOWNLOAD, 
    DEFAULT_CHIP_TEMP_SET, 
    DEFAULT_IDLE_SET, 
    DEFAULT_ENABLE_AUTO_RDC, 
    DEFAULT_HAS_COADDER, 
    DEFAULT_DO_DOWNLOAD_COADDER, 
    DEFAULT_DO_INIT_COADDER, 
    DEFAULT_DO_SETUP_COADDER, 
    DEFAULT_TEST_PATTERN_BY_COADDER,
    DEFAULT_PON_TIMEOUT, 
    DEFAULT_CLR_TIMEOUT, 
    DEFAULT_IDLE_TIMEOUT, 
    DEFAULT_TIMING_SETTING, 
    DEFAULT_UTILITY_SETTING, 
    DEFAULT_COADDER_SETTING, 
    DEFAULT_INTERFACE_SETTING, 
    DEFAULT_HEATER_ADU, 
    DEFAULT_GAIN_SETTING, 
    DEFAULT_TC_SETTING, 
    DEFAULT_AB_SETTING, 
    DEFAULT_DO_VOLTAGES, 
    DEFAULT_TIMING_FILENAME, 
    DEFAULT_UTILITY_FILENAME, 
    DEFAULT_INTERFACE_FILENAME, 
    DEFAULT_VOLTAGES_FILENAME
  };
 
  config = tsdsu_config;

  for (i=0; i<SDSU_MAX_TIMING_FILES; i++) {
    gain_list[i] = GN1;
    do_gain_list[i] = DEFAULT_GAIN_SETTING;
    tc_list[i] = DEFAULT_TC_SETTING;
    do_tc_list[i] = DEFAULT_DO_TC;
    ab_list[i] = AB12;
    do_ab_list[i] = DEFAULT_AB_SETTING;
  }

  timing_filename_is_special = FALSE;
  last_special_timg_file[0] = '\0';
  last_readout_mode = CHIP_READOUT_FAST;
  last_gain = GN_NULL;
}
/*
 *+ 
 * FUNCTION NAME: void Sdsu_Setup::put(char* data, int pos)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > data - item pointer - optionally put pars from this pointer arg
 * > pos - position in list to place item
 * Both above pars are ignored for sdsu setup files - only one internal item
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Puts a new setup structure into internal table 
 * 
 * DESCRIPTION: Uses table put methods to place each setup par
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Sdsu_Setup::put(void* data, int pos)
{
  Table_Item* item;
  ostringstream ostr;

  if ((item = find("NONAME")) != NULL) {
    ostr << "Preferences already exist in table!" << ends;
    throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);     
  }

  item = new Table_Item("NONAME");
  item->put("doSimulation",config.do_simulation) ;
  item->put("doCommsTestAtStartup",config.do_comms_test_at_startup) ;
  item->put("doLongHardwareTest",config.do_long_hardware_test) ;
  item->put("doShortHardwareTest",config.do_short_hardware_test) ;
  item->put("doReset",config.do_reset) ;
  item->put("doPowerOn",config.do_power_on) ;
  item->put("hasUtilityBoard",config.has_utility_board) ;
  item->put("doTargetChipTemp",config.do_target_chip_temp) ;
  item->put("doHeaterADU",config.do_heater_adu);
  item->put("doGain",config.do_gain) ;
  item->put("doTc",config.do_tc) ;
  item->put("doAB",config.do_ab) ;
  item->put("doIdle",config.do_idle);

  item->put("hasCoAdderBoard",config.has_coadder_board) ;
  item->put("coadderDownload",config.do_coadder_download) ;
  item->put("doCoadderInit",config.do_coadder_init) ;
  item->put("doCoadderSetup",config.do_coadder_setup) ;
  item->put("testPatternByCoadder",config.test_pattern_by_coadder);

  item->put("setChipTemp",config.chiptemp_set) ;
  item->put("idle",config.idle_set) ;
  item->put("EnableAutoRDC",config.enable_auto_rdc) ;
  item->put("gainSetting",config.gain_setting);
  item->put("tcSetting",config.tc_setting);
  item->put("abSetting",config.ab_setting);
  item->put("abSetting",config.ab_setting);
  item->put("timingDownload",config.do_timing_download) ;
  item->put("utilityDownload",config.do_utility_download) ;
  item->put("interfaceDownload",config.do_interface_download) ;
  item->put("PONTimeout",config.pon_timeout);
  item->put("CLRTimeout",config.clr_timeout);
  item->put("timingFilename",config.timing_filename);
  item->put("utilityFilename",config.utility_filename);
  item->put("interfaceFilename",config.interface_filename);
  item->put("heaterADU",config.heater_adu);
  item->put("timingSetting",config.timing_setting);
  item->put("utilitySetting",config.utility_setting);
  item->put("interfaceSetting",config.interface_setting);
  item->put("coadderSetting",config.coadder_setting);

  item->put("GainList",gain_list,SDSU_MAX_TIMING_FILES);
  item->put("DoGainList",do_gain_list,SDSU_MAX_TIMING_FILES);
  item->put("TCList",tc_list,SDSU_MAX_TIMING_FILES);
  item->put("DoTCList",do_tc_list,SDSU_MAX_TIMING_FILES);
  item->put("ABList",ab_list,SDSU_MAX_TIMING_FILES);
  item->put("DoABList",do_ab_list,SDSU_MAX_TIMING_FILES);

  item->put("TimingFilenameIsSpecial",timing_filename_is_special);
  item->put("LastSpecialTimgFile",last_special_timg_file);
  item->put("LastReadoutMode", last_readout_mode);
  item->put("LastGain", last_gain);

  item->put("DoVoltsSettings", config.do_voltages);
  item->put("VoltsFilename", config.volts_filename);

  insert(item,pos);
}	  
/*
 *+ 
 * FUNCTION NAME: void Sdsu_Setup::get(Table_Item *item)
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > item - get sdsu pars from this table item
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Gets a setup structure from internal table item
 * 
 * DESCRIPTION: Uses table find methods to fetch each setup par
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: parameter names passwd to find must be
 *               all lower case.
 *- 
 */
void Sdsu_Setup::get(Table_Item *item)
{
  // Now for each possible setup parameter try to find in table
  // General preferences

  // ** DON'T USE MIXED CASE IN PARAM NAMES HERE

  int tmp_last_readout_mode;
  item->find("dosimulation",config.do_simulation) ;
  item->find("docommstestatstartup",config.do_comms_test_at_startup) ;
  item->find("dolonghardwaretest",config.do_long_hardware_test);
  item->find("doshorthardwaretest",config.do_short_hardware_test);
  item->find("doreset",config.do_reset);
  item->find("dopoweron",config.do_power_on);
  item->find("hasutilityboard",config.has_utility_board);
  item->find("dotargetchiptemp",config.do_target_chip_temp);
  item->find("doheateradu",config.do_heater_adu);
  item->find("dogain",config.do_gain);
  item->find("dotc",config.do_tc);
  item->find("doab",config.do_ab);
  item->find("doidle",config.do_idle);

  item->find("hascoadderboard",config.has_coadder_board);
  item->find("coadderdownload",config.do_coadder_download);
  item->find("docoadderinit",config.do_coadder_init);
  item->find("docoaddersetup",config.do_coadder_setup);
  item->find("testpatternbycoadder",config.test_pattern_by_coadder);

  item->find("setchiptemp",config.chiptemp_set);
  item->find("idle",config.idle_set);
  item->find("enableautordc",config.enable_auto_rdc);
  item->find("gainsetting",config.gain_setting);
  item->find("tcsetting",config.tc_setting);
  item->find("absetting",config.ab_setting);
  item->find("absetting",config.ab_setting);
  item->find("timingdownload", config.do_timing_download);
  item->find("utilitydownload",config.do_utility_download);
  item->find("interfacedownload",config.do_interface_download);
  item->find("pontimeout",config.pon_timeout);
  item->find("clrtimeout",config.clr_timeout);
  item->find("timingsetting",config.timing_setting);
  item->find("utilitysetting",config.utility_setting);
  item->find("interfacesetting",config.interface_setting);
  item->find("coaddersetting",config.coadder_setting);
  item->find("heateradu",config.heater_adu);
  item->find("utilityfilename",config.utility_filename,FILENAMELEN);
  item->find("timingfilename",config.timing_filename,FILENAMELEN);
  item->find("interfacefilename",config.interface_filename,FILENAMELEN);

  item->find("gainlist",gain_list,SDSU_MAX_TIMING_FILES);
  item->find("dogainlist",do_gain_list,SDSU_MAX_TIMING_FILES);
  item->find("tclist",tc_list,SDSU_MAX_TIMING_FILES);
  item->find("dotclist",do_tc_list,SDSU_MAX_TIMING_FILES);
  item->find("ablist",ab_list,SDSU_MAX_TIMING_FILES);
  item->find("doablist",do_ab_list,SDSU_MAX_TIMING_FILES);

  item->find("timingfilenameisspecial",timing_filename_is_special);
  item->find("lastspecialtimgfile",last_special_timg_file,FILENAMELEN);
  item->find("lastreadoutmode", tmp_last_readout_mode);
  last_readout_mode = (Chip_Readmode_Type) tmp_last_readout_mode;
  item->find("lastgain", last_gain);

  item->find("dovoltssettings", config.do_voltages);
  item->find("voltsfilename", config.volts_filename, FILENAMELEN);
}
/*
 *+ 
 * FUNCTION NAME: Sdsu_Setup::~Sdsu_Setup()
 * 
 * INVOCATION: delete sdsu
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Destructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Sdsu_Setup::~Sdsu_Setup()
{
}


