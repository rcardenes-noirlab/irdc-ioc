/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * CICADA PROJECT
 *
 * FILENAME preferences.h
 * 
 * PURPOSE
 * General config preferences reader/writer. Makes use of base
 * table class.
 *
 * REQUIREMENTS
 *
 * Requires use of STL classes "map", "set" and "string"
 * 
 */

#ifndef PREFERENCES_H
#define PREFERENCES_H

#include "table.h"

/*
CLASS
    Preferences
    Configuration table class comprised of simple par/val pairs
DESCRIPTION  
    Configuration preferences class. Simple par/val configurations are
    handled by this class including reading/writing files.

KEYWORDS
    Preferences Table
*/
class Preferences: public Table {
public:
  //+ Constructor - named preferences table
  // > tname - name of this table
  // > il - number of items to format on single output line
  // > ns - Whether or not items are sorted by name
  // > ire - regular expression string that defines item name
  // > is - item name start delimiter
  // > ie - item name end delimiter
  // > cc - comment delimiter
  // > sep - parameter value separator string
  // > eop - parameter value ending strring/delimeter
  // > dp - pointer to item cache
  // > dps - pointer to item cache save
  // > sz - size of data cache
  Preferences(const string tname="", const int il=1, const bool ns=true, 
	      const string ire="(\\[[A-Za-z0-9].*\\])", 
	      const string is="[", const string ie="]", const string cc="#", 
	      const string sep=":", const string eop="\n", 
	      const void* dp=NULL, const void* dps=NULL, const int sz=0); 

  //+ destructor
  virtual ~Preferences();  
 
  // GROUP:   Public member functions
  
  //+ Writes a preferences table in standard format using delimiters
  // > tname - name of table file to write
  virtual void write(const string tname);

  //+ Reads a Table into internal data structure
  // > tname - name of table file to read
  virtual void read(const string tname);

  //+ This function does nothing so derived class is expected to provide functionality
  // > itemp - pointer to data item to put into table
  // > pos - index to place table item, defaults to append
  virtual void put(void* itemp, int pos=999);
  
  //+ This function does nothing so derived class is expected to provide functionality
  // < item - item retrieved - only one in a preferences table
  virtual void get(Table_Item *item);          

  //+ Gets element by index
  // > idx - the nth element to get
  virtual void get_element(int idx);  
  
  //+ Gets element by name
  // > item_name - the name of the parameter to get
  virtual void get_element(const string item_name);
};

#endif /* PREFERENCES_H */
