/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI PROJECT
 * 
 * FILENAME dc.cc
 * 
 * GENERAL DESCRIPTION
 * Implements methods for the DC common classes - inc Error. 
 * Also declares common global variables that multiple modules
 * acces in the DC design
 *
 * $Log: dc.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.24  2005/09/27 19:56:39  pjy
 * Removed redundant status_msg vars
 *
 * Revision 1.23  2005/09/24 00:45:22  pjy
 * Added support for extra debugging states
 *
 * Revision 1.22  2005/09/11 22:53:53  pjy
 * Added overloaded operators for enum types to format as strings
 *
 * Revision 1.21  2005/09/08 23:19:47  pjy
 * Only 39 chars for status msgs -  reserve 1 for null terminator
 *
 * Revision 1.20  2005/09/07 02:25:25  pjy
 * Off by one error in status message function
 *
 * Revision 1.19  2005/09/07 00:46:16  pjy
 * Moved message_write to dc.cc
 *
 * Revision 1.18  2005/09/06 22:33:58  pjy
 * Added update_status_msg function
 *
 * Revision 1.17  2005/08/18 02:29:19  pjy
 * New vars for health
 *
 * Revision 1.16  2005/08/02 04:48:08  pjy
 * DHS connection stuff
 *
 * Revision 1.15  2005/07/29 23:48:07  pjy
 * Change printer_error to message_write
 *
 * Revision 1.14  2005/07/28 09:39:44  pjy
 * Added sdsu_ntests
 *
 * Revision 1.13  2005/07/26 06:14:46  pjy
 * Added nframes
 *
 * Revision 1.12  2005/07/24 11:58:54  pjy
 * Added reconnecting
 *
 * Revision 1.11  2005/07/20 03:30:59  pjy
 * Added bunit
 *
 * Revision 1.10  2005/07/14 06:47:17  pjy
 * Added "saving" param
 *
 * Revision 1.9  2005/07/12 06:51:16  pjy
 * Removed reference to CC DB
 * Added grating name
 * Added verify
 * Removed data and obs destination strings
 *
 * Revision 1.8  2005/05/18 02:32:41  pjy
 * Removed literal string for debug_mode parameter
 *
 * Revision 1.7  2005/04/27 03:43:15  pjy
 * PCI DSP working
 *
 * Revision 1.6  2005/02/25 04:49:09  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.5  2004/12/22 06:10:35  pjy
 * DC camera src files ready for first Gemini release
 *
 * Revision 1.4  2004/08/20 02:28:51  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.3  2004/04/25 02:15:08  pjy
 * Pre T2.2 version
 *
 * Revision 1.2  2003/09/19 06:24:24  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.1  2003/05/22 03:12:00  pjy
 * GNIRS camera files as at May 03
 *
 * Revision 1.2  2003/03/25 07:11:49  pjy
 * State at start of testing Data task
 *
 * Revision 1.1  2003/03/11 01:28:59  pjy
 * Import modules
 *
 * Revision 1.1  2001/10/19 00:29:52  pjy
 * Added for GNIRS DC
 *
 *
 */
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>

#include "logging.h"
#include "dc.h"
#include "parameter.h"
#include "epicsParameterDB.h"
#include "configuration.h"
#include "config_table.h"
#include "ir_camera.h"
#include "ir_setup.h"
#include "epicsEvent.h"

// Global Variables 

// NB ***********************************
// Do not declare any global vars that are static C++ class objects
// VxWorks needs these to be preprocessed before loading and this is 
// a bit too complicated for my liking! This includes the 'string' type!
// *************************************

// Parameter objects
ParameterDB *dc_status_db = NULL;                 //+ EPICS interface to the DC SAD
Parameter *dc_status = NULL;                      //+ Internal DC status parameters
ParameterDB *dc_par_db = NULL;                    //+ EPICS interface to the CAD pars
Parameter *dc_par = NULL;                         //+ Internal control parameters

//+ The camera and its configuration
IR_Camera *ir_camera = NULL;                      //+ The infra red camera object

//+ Parameters required for image spectral compression and stacking
double gratingMin;                                //+ Grating min wavelength (microns)
double gratingMax;                                //+ Grating max wavelength (microns)
Nametype gratingName;                             //+ Name of current grating

//+ Data arrays
Detector_Image *shared_image_buffer = NULL;       //+ image transfer buffer
float* dc_variance[4];                            //+ variance data array
unsigned char* dc_quality[4];                     //+ quality data array
float* dc_ref_data[4];                            //+ reference pixel array

//+ Configuration, status, control and reporting structures
Nametype configName;                              //+ Name of camera configuration
Instrument_Status *inst_status = NULL;            //+ Full instrument status - local copy
Config *shared_config = NULL;                     //+ Full instrument configuration - read from disk
/* TODO: Fix later
Report *dc_report = NULL;                         //+ Reporting object
*/
IR_Setup* ir_setup = NULL;                        //+ Used for retrieving and storing setup information from disk
Verify_Spec verify_spec;                          //+ Readout verification/test pattern spec
bool do_verify;                                   //+ Do readout verification

//+ Data destination
DC_Dest data_destination;                         //+ Destination of data - either dhs, fits or none

//+ Data handler connection
bool doConnect;                                   //+ Connect/disconnect from data handler

/*
//+ DHS stuff
bool dhs_available;                               //+ flag set if DHS is available
bool dhs_connected;                               //+ flag set when the dhs is connected
bool handling_dhs_connection;                     //+ flag set when the dhs connection is being handled
Hostnametype dhs_server_ip;                       //+ DHS server IP address
Hostnametype dhs_server_name;                     //+ DHS server host name
Nametype obs_ql_stream;                           //+ DHS Quick Look stream in use for obs mode
Nametype view_cmp_ql_stream;                      //+ DHS Quick Look stream in use for view mode compressed data
Nametype view_ql_stream;                          //+ DHS Quick Look stream in use for view mode
char* dc_qls_ptr[MAX_QLS];                        //+ ptr to DHS Quick Look streams in use
int n_qlstreams;                                  //+ Number of QL streams in use
char data_label[DHS_DATA_LABEL_LEN];              //+ Datalable provided with OBSERVE or unique name returned by DHS
Nametype bunit;                                   //+ Data units
long nframes;                                     //+ Number of frames in data set
*/

//+ FITS server stuff
bool fits_available;                              //+ flag set if FITS server is available
bool fits_connected;                              //+ flag set when fits server connected
long fits_server_port;                            //+ FITS server socket port number
Hostnametype fits_server_name;                    //+ FITS server host name

//+ System health records
Nametype dc_health;                               //+ DC health state
EPICS_String dc_health_msg;                       //+ DC health state message
Nametype dhs_health;                              //+ DHS health state
EPICS_String dhs_health_msg;                      //+ DHS health state message
bool dhs_problem;                                 //+ DHS problem flag
EPICS_String dhs_problem_msg;                     //+ DHS problem message
Nametype fits_health;                             //+ FITS server health state
EPICS_String fits_health_msg;                     //+ FITS server health state message
bool fits_problem;                                //+ FITS problem flag
EPICS_String fits_problem_msg;                    //+ FITS problem message
Nametype data_health;                             //+ Data task health state
EPICS_String data_health_msg;                     //+ Data task health state message
Nametype slave_health;                            //+ Slave task health state
EPICS_String slave_health_msg;                    //+ Slave task health state message
Nametype camera_health;                           //+ Camera health state
EPICS_String camera_health_msg;                   //+ Camera health state message
bool camera_op;                                   //+ Camera operation flag
EPICS_String camera_op_msg;                       //+ Camera operation message

//+ Detector infor SIRS
Nametype detector_type;                           //+ Detector type
Nametype detector_id;                             //+ detector id
Nametype timing_dsp;                              //+ SDSU timing DSP name
Nametype interface_dsp;                           //+ SDSU interface DSP name

//+ SDSU DSP command SIRS
long sdsu_board;                                  //+ SDSU direct command board id
long sdsu_cmd;                                    //+ SDSU direct command 
long sdsu_mem_space;                              //+ SDSU direct command memory space
long sdsu_mem_addr;                               //+ SDSU direct command memory address
long sdsu_arg1;                                   //+ SDSU direct command argument 1
long sdsu_arg2;                                   //+ SDSU direct command argument 2
int sdsu_debug;                                   //+ SDSU debug command argument
int sdsu_ntests;                                  //+ SDSU test command argument
bool sdsu_comms;                                  //+ SDSU comms flag

//+ States and flags
IR_Mode ir_mode;                                  //+ Mode of current observation OBSERVE or VIEW
DC_State dc_state;                                //+ DC global state
DC_State dc_slave_state;                          //+ state of slave task
DC_State dc_data_state;                           //+ current Data task run state
Debug_Level debug_mode;                           //+ DC debugging mode 
Nametype debug_mode_str;                          //+ debug mode as seen in EPICS DB
Nametype sim_mode_str;                            //+ simulation mode as seen in EPICS DB
Nametype state_str;                               //+ state as seen in EPICS DB
bool aborting;                                    //+ flag set if OBSERVE is aborted
bool sdsu_canceling;                               //+ flag set if SDSU ops canceled
bool stopping;                                    //+ flag set if OBSERVE is stopped
bool connection;                                  //+ flag set if connecting/disconnecting to data handler
bool saving;                                      //+ flag set when saving OBSERVE data
bool sdsu_init_done;                              //+ flag set if sdsu init complete
bool sdsu_cmd_running;                            //+ flag set if sdsu command active
bool camera_setup_changed;                        //+ Flag set when any readout setting change

/*
 *+ 
 * FUNCTION NAME: string to_string(DC_State state)
 * 
 * INVOCATION: string to_string(DC_State state)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * ! str - output string
 * > state - enumeration to convert
 * 
 * FUNCTION VALUE: string
 * 
 * PURPOSE: string casting operator for the DC_State enumeration
 * 
 * DESCRIPTION: Simply returns a string to reperesent DC_State 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
string to_string(DC_State state)
{
  switch (state) {
  case DC_State::DC_STOPPED:
    return "STOPPED";
  case DC_State::DC_INITIALIZING:
    return "INITIALIZING";
  case DC_State::DC_RUNNING:
    return "RUNNING";
  case DC_State::DC_ABORTING:
    return "ABORTING";
  case DC_State::DC_CONTROL_WAITING:
    return "CONTROL_WAITING";
  case DC_State::DC_CONTROL_READY:
    return "CONTROL_READY";
  case DC_State::DC_SLAVE_WAITING:
    return "SLAVE_WAITING";
  case DC_State::DC_SLAVE_READY:
    return "SLAVE_READY";
  case DC_State::DC_SLAVE_MONITOR:
    return "SLAVE_MONITOR";
  case DC_State::DC_SLAVE_BUSY:
    return "SLAVE_BUSY";
  case DC_State::DC_SLAVE_CONFIGURE:
    return "SLAVE_CONFIGURE";
  case DC_State::DC_DATA_READY:
    return "DATA_READY";
  case DC_State::DC_DATA_TRANSFERRING:
    return "DATA_TRANSFERRING";
  default:
    return "Impossible";
  }
};

/*
 *+ 
 * FUNCTION NAME: string to_string(DC_Cmd cmd)
 * 
 * INVOCATION: string to_string(DC_Cmd cmd)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * ! str - output string
 * > cmd - enumeration to convert
 * 
 * FUNCTION VALUE: string
 * 
 * PURPOSE: string casting operator for the DC_Cmd enumeration
 * 
 * DESCRIPTION: Simply returns a string to represent DC_Cmd 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
string to_string(DC_Cmd cmd)
{
  switch (cmd) {
  case DC_Cmd::NOCMD:
    return "NOCMD";
  case DC_Cmd::TEST:
    return "TEST";
  case DC_Cmd::REBOOT:
    return "REBOOT";
  case DC_Cmd::INIT:
    return "INIT";
  case DC_Cmd::DATUM:
    return "DATUM";
  case DC_Cmd::PARK:
    return "PARK";
  case DC_Cmd::APPLY:
    return "APPLY";
  case DC_Cmd::VERIFY:
    return "VERIFY";
  case DC_Cmd::ENDVERIFY:
    return "ENDVERIFY";
  case DC_Cmd::GUIDE:
    return "GUIDE";
  case DC_Cmd::ENDGUIDE:
    return "ENDGUIDE";
  case DC_Cmd::OBSERVE:
    return "OBSERVE";
  case DC_Cmd::ENDOBSERVE:
    return "ENDOBSERVE";
  case DC_Cmd::PAUSE:
    return "PAUSE";
  case DC_Cmd::CONTINUE:
    return "CONTINUE";
  case DC_Cmd::STOP:
    return "STOP";
  case DC_Cmd::ABORT:
    return "ABORT";
  case DC_Cmd::DEBUG:
    return "DEBUG";
  case DC_Cmd::DO_CONNECTION:
    return "DO_CONNECTION";
  case DC_Cmd::CONTROLLER_INIT:
    return "CONTROLLER_INIT";
  case DC_Cmd::CONTROLLER_CMD:
    return "CONTROLLER_CMD";
  case DC_Cmd::CONTROLLER_SHUTDOWN:
    return "CONTROLLER_SHUTDOWN";
  case DC_Cmd::CONTROLLER_RESET:
    return "CONTROLLER_RESET";
  case DC_Cmd::CONTROLLER_TEST:
    return "CONTROLLER_TEST";
  case DC_Cmd::CONTROLLER_DEBUG:
    return "CONTROLLER_DEBUG";
  case DC_Cmd::CONTROLLER_SIMULATE:
    return "CONTROLLER_SIMULATE";
  case DC_Cmd::CONTROLLER_INFO:
    return "CONTROLLER_INFO";
  case DC_Cmd::CONTROLLER_CANCEL:
    return "CONTROLLER_CANCEL";
  default:
    return "Impossible";
  }
}

/*
 *+ 
 * FUNCTION NAME: string& operator<<(string& str, DC_Dest dest)
 * 
 * INVOCATION: string& operator<<(string& str, DC_Dest dest)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * ! str - output string
 * > dest - enumeration to convert
 * 
 * FUNCTION VALUE: string
 * 
 * PURPOSE: string formatting operator for the DC_Dest enumeration
 * 
 * DESCRIPTION: Simply returns a string to reperesent DC_Dest 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
/*
string& operator<<(string& str, DC_Dest dest)
{
  switch (dest) {
  case DC_DEST_DHS:
    str = "DHS";
    break;
  case DC_DEST_FITS:
    str = "FITS";
    break;
  case DC_DEST_NONE:
    str = "NONE";
    break;
  default:
    str = "Impossible";
    break;
  }
  return str;
}
*/

/*
 *+ 
 * FUNCTION NAME: dc_set_debug_level()
 * 
 * INVOCATION: dc_set_debug_level()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Gets debug_mode string parameter and converts to enum
 * 
 * DESCRIPTION: Matches Gemini debug mode string to internal enum
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void dc_set_debug_level()
{
  string debug_str;
  logger::Level debug;

  debug = logger::getLevel();
  if (dc_status) {
    try {
      dc_status->get(DEBUG_MODE, debug_mode_str, 0, NAMELEN);
      debug_str = debug_mode_str;
      if (debug_str == "NONE") {
	debug_mode = DEBUG_NONE;
	debug = logger::Level::None;
	logger::message(logger::Level::NoLog,"Debug mode set to DEBUG_NONE");
      } else if (debug_str == "NOLOG") {
	debug_mode = NO_LOG;
	debug = logger::Level::NoLog;
	logger::message(logger::Level::NoLog,"Debug mode set to NO_LOG");
      } else if (debug_str == "ALWAYS_LOG") {
	debug_mode = ALWAYS_LOG;
	debug = logger::Level::Full;
	logger::message(logger::Level::NoLog,"Debug mode set to ALWAYS_LOG");
      } else if (debug_str == "MIN") {
	debug_mode = DEBUG_MIN;
	debug = logger::Level::Min;
	logger::message(logger::Level::NoLog,"Debug mode set to DEBUG_MIN");
	/* 
	 * Following modes correspond to more detailed specific debugging
	 * in lower level C++ code, but equate to CICS_MIN in higher level code
	 */
      } else if (debug_str == "SDSU") {
	debug_mode = DEBUG_SDSU;
	debug = logger::Level::Min;
	logger::message(logger::Level::NoLog,"Debug mode set to DEBUG_SDSU");
      } else if (debug_str == "SPECIAL") {
	debug_mode = DEBUG_SPECIAL_G;
	debug = logger::Level::Min;
	logger::message(logger::Level::NoLog,"Debug mode set to DEBUG_SPECIAL");
       } else if (debug_str == "CALCS") {
	debug_mode = DEBUG_CALCS_G;
	debug = logger::Level::Min;
	logger::message(logger::Level::NoLog,"Debug mode set to DEBUG_CALCS");
      } else if (debug_str == "STATE") {
	debug_mode = DEBUG_STATE_G;
	debug = logger::Level::Min;
	logger::message(logger::Level::NoLog,"Debug mode set to DEBUG_STATE");
      } else if (debug_str == "FULL") {
	debug_mode = DEBUG_FULL;
	debug = logger::Level::Full;
	logger::message(logger::Level::NoLog,"Debug mode set to DEBUG_FULL");
      } else {
	logger::message(logger::Level::NoLog,"Bad debug mode str '%s', debug mode defaults to DEBUG_NONE");
	debug_mode = DEBUG_NONE;
	debug = logger::Level::None;
      }
    }
    catch (Error& dce) {
      logger::message(logger::Level::NoLog,"%s", dce.record_error(__FILE__, __LINE__));
    }
  }
  logger::setLevel(debug);
}

/*
 *+ 
 * FUNCTION NAME: dc_set_health
 * 
 * INVOCATION: dc_set_health(const char* health_valp, Error_Type health_val, const char* health_msgp, char* health_msgv, 
 *                           char* health_msg)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > health_valp - health record VAL field
 * > health_val - value to set
 * > health_msgp - health record OMSS field
 * > health_msgv - message address pointer - message gets copied here
 * > health_msg - msg value to set
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Changes the health SIR VAL and OMSS fields
 * 
 * DESCRIPTION: uses the parameter database to change the requested EPICS health 
 * record.
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: 
 *

 *- 
 */
void dc_set_health(const char* health_valp, Error_Type health_val, const char* health_msgp, char* health_msgv, const char* health_msg)
{
  string status_str = "GOOD";
  switch (health_val) {
  case E_ERROR:
  case E_FATAL:
    logger::message(logger::Level::NoLog,"'%s' BAD, reason='%s'", health_valp, health_msg);
    status_str = "BAD";
    break;
  case E_INFO:
  case E_WARNING:
    logger::message(logger::Level::Full,"'%s' WARNING, reason='%s'", health_valp, health_msg);
    status_str = "WARNING";
    break;
  case E_OK:
  default:
    status_str = "GOOD";
  }

  try {
    if (dc_status != NULL) {
      // NB always write IMSS first, and VAL field second. EPICS processes SIR after VAL updated
      // therefore IMSS will be copied to OMSS.
      strncpy(health_msgv, health_msg, MAX_STRING_SIZE-1);
      dc_status->put(health_msgp, health_msgv, 0, strlen(health_msgv)+1);       
      dc_status->put(health_valp, status_str.c_str(), 0, status_str.length());
    }
  }
  catch (Error & e) {
    logger::message(logger::Level::NoLog,"'%s'", e.record_error(__FILE__, __LINE__));
  }
}

/*
 *+ 
 * FUNCTION NAME: update_status_msg()
 * 
 * INVOCATION: update_status_msg(char* str)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > str - string to be used for update
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Writes the str to EPICS status message record(s)
 * 
 * DESCRIPTION: Breaks string into a number of 40 char messages and writes
 * to EPICS DB variables.
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void update_status_msg(const char* str)
{
  if (dc_status != NULL) {
    Nametype status_var;
    string status_str{str};
    string substr;
    int s = 0;
    int l;
		
    // Update each status string with 40 char substrs
    while ((status_str.length()>0) && (s < N_STATUS_STRS)) {
      snprintf(status_var,NAMELEN,"%s%d", STATUS_STR,s+1);
      l = MIN(MAX_STRING_SIZE-1, status_str.length());
      substr = status_str.substr(0,l);
      dc_status->put(status_var, substr);
      status_str = status_str.substr(l,status_str.length());
      s++;
    }
		
    // Clear any not updated
    while (s < N_STATUS_STRS) {
      snprintf(status_var,NAMELEN,"%s%d", STATUS_STR,s+1);
      dc_status->put(status_var, (string)" ");
      s++;
    }
  }
}

/*
 *+ 
 * FUNCTION NAME: message_write(dl, format, ...)
 * 
 * INVOCATION: message_write(int dl, char* format, ap)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > dl - debugging level
 * > format - format string for message
 * > ap - variable argument list
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Writes a timestamped message to stdout
 * 
 * DESCRIPTION: Uses the c variable argument list processing to
 * write out a message to stdout. No error checking is done for
 * buffer overflow
 * 
 * EXTERNAL VARIABLES: Uses 'message_debug_level' file variable for checking if message
 * should be printed. 'message_debug_level' is set on first call or if format is NULL.
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: Potential buffer overflow - throws if chars written
 * greater than buffer length
 *
 *- 
 */

/*
void message_write(Debug_Level dl, const char* format ...)
{
  const int TIMELEN = 20;                         // lenght of formatted time buffer
  char msgbuf[MSGLEN];                            // Temporary message buffer - large
  char msgbuf2[MSGLEN+TIMELEN];                   // Second temporary message buffer - larger
  va_list ap;                                     // variable argument list
  time_t current_time;                            // current time of day
  char timebuf[TIMELEN];                          // formatted time string
  char fmt[17]="%b %d %H:%M:%S: ";                 // Format of timestamp
  
  static Debug_Level message_debug_level = DEBUG_NOT_SET; // Setting of debug level - changed in message_write
  // If this is the first call - set the the message debugging level
  // Also reset if format is NULL
  if ((message_debug_level == DEBUG_NOT_SET) || (format == NULL))
    message_debug_level = dl;

  // No message to report - just return
  if (format == NULL) 
    return;

  va_start(ap,format);
  vsnprintf(msgbuf, MSGLEN-1, format, ap);
  va_end(ap);
  current_time = time(0);
  strftime(timebuf,20,fmt,localtime(&current_time));
  snprintf(msgbuf2,MSGLEN+TIMELEN-1,"%s%s",timebuf, msgbuf);

  logger::Level debug_level;
  switch (dl) {
	  case ALWAYS_LOG:
		  update_status_msg(msgbuf);
		  debug_level = logger::Level::NoLog;
		  break;
	  case NO_LOG:
		  debug_level = logger::Level::NoLog;
		  break;
	  case DEBUG_NONE:
		  debug_level = logger::Level::None;
		  break;
	  case DEBUG_MIN:
		  debug_level = logger::Level::Min;
		  break;
	  case DEBUG_FULL:
		  debug_level = logger::Level::Full;
		  break;
	  default:
		  debug_level = logger::getDefaultLevel();
  }

  logger::message(debug_level, msgbuf2);
}
*/
