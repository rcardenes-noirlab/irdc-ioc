[schematic2]
uniq 127
[tools]
[detail]
w 354 -277 100 0 n#124 esirs.verified.FLNK 288 -224 336 -224 336 -288 432 -288 embbis.verifiedO.SLNK
w 330 -245 100 0 n#123 esirs.verified.VAL 288 -256 432 -256 embbis.verifiedO.INP
w 402 139 100 0 n#120 esirs.testPatVerify.FLNK 336 192 384 192 384 128 480 128 embbis.testPatVerifyO.SLNK
w 378 171 100 0 n#119 esirs.testPatVerify.VAL 336 160 480 160 embbis.testPatVerifyO.INP
s 912 -1040 500 512 GNIRS DC SDSU Test pattern SAD Database
[cell use]
use embbis 480 55 100 0 testPatVerifyO
xform 0 608 128
p 544 -16 100 0 1 ONST:No
p 544 -48 100 0 1 PV:$(sadtop)
p 544 16 100 0 1 ZRST:Yes
use embbis 432 -361 100 0 verifiedO
xform 0 560 -288
p 496 -432 100 0 1 ONST:No
p 496 -464 100 0 1 PV:$(sadtop)
p 496 -400 100 0 1 ZRST:Yes
use esirs -80 -57 100 0 testPatVerify
xform 0 128 96
p 16 -96 100 0 1 DESC:Test pattern verification
p 16 -160 100 0 0 EGU:number
p -144 -320 100 0 0 FDSC:Test pattern verification
p 16 -128 100 0 1 FTVL:LONG
p 16 -192 100 0 0 PV:$(sadtop)
p 16 -192 100 0 0 SNAM:
use esirs -80 391 100 0 testPatOS
xform 0 128 544
p 16 352 100 0 1 DESC:Test pattern overscan
p 16 288 100 0 0 EGU:number
p -144 128 100 0 0 FDSC:Test pattern overscan
p 16 320 100 0 1 FTVL:LONG
p 16 256 100 0 0 PV:$(sadtop)
p 16 256 100 0 0 SNAM:
use esirs -688 391 100 0 testPatReadInc
xform 0 -480 544
p -592 352 100 0 1 DESC:Test pattern read increment
p -592 288 100 0 0 EGU:number
p -752 128 100 0 0 FDSC:Test pattern read increment
p -592 320 100 0 1 FTVL:LONG
p -592 256 100 0 0 PV:$(sadtop)
p -592 256 100 0 0 SNAM:
use esirs -1264 391 100 0 testPatAmpInc
xform 0 -1056 544
p -1168 352 100 0 1 DESC:Test pattern amp increment
p -1168 288 100 0 0 EGU:number
p -1328 128 100 0 0 FDSC:Test pattern amp increment
p -1168 320 100 0 1 FTVL:LONG
p -1168 256 100 0 0 PV:$(sadtop)
p -1168 256 100 0 0 SNAM:
use esirs -688 -57 100 0 testPatInc
xform 0 -480 96
p -592 -96 100 0 1 DESC:Test pattern increment
p -592 -160 100 0 0 EGU:number
p -752 -320 100 0 0 FDSC:Test pattern increment
p -592 -128 100 0 1 FTVL:LONG
p -592 -192 100 0 0 PV:$(sadtop)
p -592 -192 100 0 0 SNAM:
use esirs -1264 -57 100 0 testPatSeed
xform 0 -1056 96
p -1168 -96 100 0 1 DESC:Test pattern seed
p -1168 -160 100 0 0 EGU:number
p -1168 -96 100 0 0 FDSC:Test pattern seed
p -1168 -128 100 0 1 FTVL:LONG
p -1168 -192 100 0 0 PV:$(sadtop)
p -1168 -192 100 0 0 SNAM:
use esirs -128 -473 100 0 verified
xform 0 80 -320
p -32 -512 100 0 1 DESC:Test pattern verified ok
p -32 -576 100 0 0 EGU:number
p -192 -736 100 0 0 FDSC:Test pattern verified ok
p -32 -544 100 0 1 FTVL:LONG
p -32 -608 100 0 0 PV:$(sadtop)
p -32 -608 100 0 0 SNAM:
use bc200tr -1504 -1192 -100 0 frame
xform 0 176 112
p 1072 -1024 100 0 1 author:P. Young
p 1296 -1040 100 0 -1 border:C
p 1072 -1056 100 0 1 checked:Peter Young
p 1328 -1040 100 0 -1 date:2004-02-18
p 1312 -912 100 0 -1 project:Gemini GNIRS DC
p -1504 -1144 100 0 0 revision:1.0
p 1312 -976 150 0 -1 title:dcSDSUTestSad.sch
[comments]
