/*
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME controller.cc
 *
 * GENERAL DESCRIPTION
 *  CICADA_CONTROLLER class implementation and functions.
 * This class is used to interface to a generic astronomy detector controller.
 * Classes derived off this are used to interface directly to specific types of detector
 * controller and therefore contain all specific knowledge of that particular controller.
 * This class will be used by a Camera object to interface generically to a Controller
 * to control detector setup and operation. It has members for buffering detector data
 * after readout before that data is transferred out of the system.
 *
 */

/* Include files */
#include "common.h"
#include "config_table.h"
#include "controller.h"
#include "camera.h"
#include "epicsParameterDB.h"
#include "math.h"
#include <cstdarg>

using std::shared_ptr;

/* typedefs */

/* class declarations */

/* global variables */
extern Config *shared_config;                     //+ Shared memory for configuration

const std::string Controller::PARAMETER_NAME = "CtrlPar";

/* local function declarations */

/* local function definitions */

/* class function definitions */

/*============================================================================
 *  Cicada_Controller Methods
 *===========================================================================*/
/*
 *+
 * FUNCTION NAME: Cicada_Controller::Cicada_Controller()
 *
 * INVOCATION: controller = new Cicada_Controller(Camera_Desc cam, int camid, int c)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam - description of camera that is parent of this controller. Also holds
 *         controller description
 * > camid - identification umber of parent camera
 * > c - identification number of this controller within the camera.
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 * Simple Constructor used for initialising configuration variables
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generic controller
 * object.  It is never used directly but is called by a derived version of this
 * class that handles a specific controller type. A specific controller object
 * is created by a parent camera object.
 *
 * This version of the controller is used just for holding a controllers
 * configuration information. It is not used to interface with real hardware. It
 * is, therefore, only useful in modules that need to know about a configuration
 * but do not actually interface with hardware.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Controller::Controller(Camera_Desc cam, int camid, int c, Platform_Specific* ps)
{
  ostringstream ostr;

  controller_status = nullptr;
  controller_pDB = nullptr;
  controller_statusp = nullptr;
  controller_param_map = nullptr;
  camera = cam;
  cid = c;
  if (cid > MAX_CONTROLLERS-1) {
    ostr << "Controller: controller id " << cid << "> maximum allowed (" << MAX_CONTROLLERS-1 << ")!" << ends;
    throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);
  }
  controller = cam.controller[cid];

  // Set the platform specific data
  specific = ps;
  controller_param_map = nullptr;

  // Initialise some member vars
  Controller::init_vars();
}

/*
 *+
 * FUNCTION NAME: Cicada_Controller::Cicada_Controller()
 *
 * INVOCATION: controller = new Cicada_Controller(Camera_Desc cam, int camid, int c, const char* param_host,
 *                          Instrument_Status *st, paramMapType *pm)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam - description of camera that is parent of this controller. Also holds
 *         controller description
 * > camid - identification umber of parent camera
 * > c - identification number of this controller within the camera.
 * > param_host - name of host where parameter database resides.
 * > st - pointer to instrument status data structure for all instrument status
 * > pm - pointer to parameter map type
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 * Constructor used for initialising configuration variables and setting up the
 * parameter database
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generic controller
 * object.  It is never used directly but is called by a derived version of this
 * class that handles a specific controller type. A specific controller object
 * is created by a parent camera object.
 *
 * This version of the controller is used just for holding a controllers
 * configuration information and also its status parameter database. It is not
 * used to interface with real hardware. It is, therefore, only useful in
 * modules that need to know about a configuration and be able to read/write
 * status parameters but do not actually interface with hardware.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Controller::Controller(Camera_Desc cam, int camid, int c, const char* param_host, Instrument_Status *st, Platform_Specific* ps)
{
  ostringstream ostr;

  // Base address for instrument status
  status = st;
  // Address for depositing ctl status info
  ctl_status = &st->inst_ctl_status;

  // Set the platform specific data
  specific = ps;
  controller_param_map = nullptr;

  // Setup a pointer to the camera & controller status structures in shared mem
  camera_status = &st->hardware_status.Hardware_Status_u.camera_status;
  controller_status = &camera_status->controller_status;

  // Add controller generic status parameters
  add_status_parameters(param_host, false);

  camera = cam;
  cid = c;
  if (cid > MAX_CONTROLLERS-1) {
    ostr << "Controller: controller id " << cid << "> maximum allowed (" << MAX_CONTROLLERS-1 << ")!" << ends;
    throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);
  }
  controller = cam.controller[cid];

  // Initialise some member vars
  Controller::init_vars();
}

/*
 *+
 * FUNCTION NAME: Cicada_Controller::Cicada_Controller()
 *
 * INVOCATION: controller = new Cicada_Controller(Instrument_Status *st, Config *cf,
 *                                                Parameter* ip, Parameter* cp, Parameter* op, int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > st - pointer to instrument status data structure for all instrument status
 * > ip - pointer to the parameter database for the instrument
 * > cp - pointer to the parameter database for the camera
 * > op - pointer to the operation parameter database
 * > deb - debug flag
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Constructor used for interfacing to real controller hardware
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generic controller
 * object.  It is never used directly but is called by a derived version of this
 * class that handles a specific controller type. A specific controller object
 * is created by a parent camera object.
 *
 * This version of the controller is used for interfacing with real hardware. It
 * is, therefore, required for components that are attached to the hardware. Its
 * derived classes know how to communicate with specific controllers, while this
 * parent class can be used in a generic way to deal with all controller types.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Controller::Controller(Instrument_Status *st, Parameter* ip, Parameter* cp, Parameter* op,
		       int deb, Platform_Specific* ps)
{
  ostringstream ostr;

  // Base address for instrument status
  status = st;

  // Address for depositing ctl status info
  ctl_status = &st->inst_ctl_status;

  // Setup a pointer to the camera & controller status structures in status memory
  camera_status = &st->hardware_status.Hardware_Status_u.camera_status;
  controller_status = &camera_status->controller_status;

  // Set the platform specific data
  specific = ps;
  controller_param_map = nullptr;

  add_status_parameters("", true);

  inst_statusp = ip;
  camera_statusp = cp;
  op_statusp = op;

  current_config = shared_config;

  // Copy the camera and controller description out of the configuration memory
  camera = current_config->config_data.Config_Data_u.camera.desc;
  cid = current_config->config_data.Config_Data_u.camera.cid;
  if (cid > MAX_CONTROLLERS-1) {
    ostr << "Controller: controller id " << cid << "> maximum allowed (" << MAX_CONTROLLERS-1 << ")!" << ends;
    throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);
  }
  controller = current_config->config_data.Config_Data_u.camera.desc.controller[cid];

  // Initialise some member vars
  Controller::init_vars();


  debug = deb;          // debugging level

}

/*
 *+
 * FUNCTION NAME: Controller::init_vars()
 *
 * INVOCATION: init_vars()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Initialise some member vars for constructors
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::init_vars()
{
  string cname;

  controller_abort = FALSE;
  image_buffer = nullptr;	
  verify_buf = nullptr;
  debug = 0;
  last_size = 0;
  last_verify_size = 0;
  readout_no = 0;
  readouts_done = 0;
  read_running = 0;
  doing_image_transfer = false;
  finished_image_transfer = true;
  image_transfer_thr = nullptr;
  ir_timer_thr = nullptr;
  n_sync = 1;
  msg_severity = E_INFO;


  // Setup default config file - check if special config
  cname = camera.cont_config[cid];
  if (cname != "") {
    cname = controller.name;
    cname += "/";
    cname += camera.cont_config[cid];
  } else
    cname = controller.name;
  strlcpy(controller_dir, cname.c_str(), FILENAMELEN);

  // Store verify spec from the controller description
  verify_spec =  controller.verify_spec;   // verify_spec stores latest spec to use - could be from command line or config

}

/*
 *+
 * FUNCTION NAME: Controller::add_status_parameters
 *
 * INVOCATION: add_status_parameters(const char* param_host, bool init)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > param_host - name of host that hold home version of parameters
 * > init - set if required to initialise variables
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Adds all generic controller status parameters to the parameter database
 *
 * DESCRIPTION:
 * Uses the Parameter class to handle any controller status parameters. This
 * class does immediate write-thru operations to the status memory on the
 * database computer located any where on the net.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::add_status_parameters(const char* param_host, bool init)
{
  char pname[IPC_NAMELEN];
//  char basename[IPC_NAMELEN];

  // name the status parameter DB
//  ipc_name(basename, "Inst", "Status", status->id, status->pid);

  controller_pDB = new EpicsParameterDB();
  controller_statusp = new Parameter(controller_pDB, PARAMETER_NAME, controller_param_map, CONTROLLER_PREFIX, DB_SUFFIX);

  // Just add the controller type
  controller_statusp->add(CONTROLLER_TYPE, CONTROLLER_TYPE_DB, (void*)&controller_status->type, init, 1,
			  (int*)&controller_status->type, VALIDITY_NONE, 0, (int*)NULL, UNPUBLISHED);
}


/*
 *+
 * FUNCTION NAME: Camera::init
 *
 * INVOCATION: init(Init init_rec)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > init_rec - initialisation record
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Do hardware initialisation - must be derived
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::init(Init init_rec)
{
  Init tinit = init_rec;

  initialised = TRUE;

  // Setup warmup and verification settings from appropriate place
  if (init_rec.use_config) {
  } else {
  }
}

/*
 *+
 * FUNCTION NAME: Controller::handle_reset
 *
 * INVOCATION: handle_reset()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *   Handle a reset condition.  Can, and probably should, be overriden.
 *
 * DESCRIPTION:
 *   Sets the controller_abort flag to TRUE.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::handle_reset()
{
  controller_abort = TRUE;	
  if (image_transfer_thr!=NULL)
    image_transfer_thr->signal();
  message_write("Controller abort flag set!", status->pid);
}

/*
 *+
 * FUNCTION NAME: Controller::clear_reset
 *
 * INVOCATION: clear_reset()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Clears the reset condition.
 *
 * DESCRIPTION: Sets controller_abort to FALSE
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::clear_reset()
{
  controller_abort = FALSE;
}


/*
 *+
 * FUNCTION NAME: Controller::create_image_buffer
 *
 * INVOCATION: imbuf = ctrlr->create_image_buffer(unsigned long size, int align)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > size - size in bytes of the buffer
 * > align - alignment constraint - if zero ignored
 *
 * FUNCTION VALUE: char*
 *
 * PURPOSE:
 *  Create an image readout buffer and returns pointer to buffer
 *
 * DESCRIPTION:
 *
 * Uses virtual memory to create an image buffer which can be locked from being
 * paged - if process is running as setuid root.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
char* Controller::create_image_buffer(unsigned long size, int align)
{
  ostringstream ostr;

  // Allocate memory for the CCD image, lock in core
  image_buffer_size = size;

  if ((image_buffer_size>0) && (image_buffer_size>last_size)) {
    if (debug&DEBUG_CALCS) {
      message_write("Creating readout buffer of %d bytes", size);
    }

    // remove the previously allocated image buffer - not of right size
    if (image_buffer != NULL) {
      free((void*)image_buffer);
    }
    image_buffer = nullptr;
    image_buffer_ptr = nullptr;

    // Now allocate the memory
    /* TODO: Test this...
    if (align > 0)
      image_buffer = (char *) memalign(align, image_buffer_size);
    else
    */
      image_buffer = (char *) malloc(image_buffer_size);
	
    if (image_buffer == NULL) {
      ostr << "Controller: Unable to allocate space for readout buffer (" << image_buffer_size << " bytes)!" << ends;
      throw Error(ostr, E_ERROR, ENOMEM, __FILE__, __LINE__);
    }


    // set number of image buffers in use
    if (!actual_image_size) {
      throw Error("Controller: Image size must be greater than zero!",
		  E_ERROR, -1, __FILE__, __LINE__);
    }
    nbuffers = image_buffer_size/actual_image_size;

    // Update the preserved buffer size
    last_size = size;
  }

  if (image_buffer != NULL) {
    image_buffer_ptr = image_buffer;

    // Clear the buffer
    memset(image_buffer_ptr, 0x0, image_buffer_size);

    return image_buffer_ptr;
  } else
    return NULL;
}
/*
 *+
 * FUNCTION NAME:  Controller::setup_verify_data
 *
 * INVOCATION: setup_verify_data(int rn)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > rn - readout number
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Generates data for readout verification
 *
 * DESCRIPTION: Prepare the verification buffer using specified formula
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES: None
 *
 *-
 */
void Controller::setup_verify_data(int rn)
{
  int sr,swidth,swidth_b,nr,pr,n,yo,r,namps;// counters and indices
  int height, height_b;
  char *bvp,bval;                           // pointers for different data types
  short *svp,sval;
  unsigned short *uvp,uval;
  int *ivp,ival;
  unsigned int *uivp,uival;
  float *fvp,fval;
  double *dvp,dval;
  unsigned short tval,tinc,inc,ainc,rinc;   // pixel values and increments
  bool in_overscan;                         // set when in overscan section

  if ((image_buffer_size>0) && (image_buffer_size>last_verify_size)) {
    // Create the buffer - remove old one first
    if (verify_buf != NULL) {
      delete [] verify_buf;
    }
    verify_buf = nullptr;

    // Now allocate the memory
    verify_buf = new char[image_buffer_size];
    if (!verify_buf) {
      throw Error("Controller: Unable to allocate space for verification image!",
		  E_ERROR, -1, __FILE__, __LINE__);
    }

    last_verify_size = image_buffer_size;
  }

  // Setup up pointers to buffer in each data type
  bvp = verify_buf;
  svp = (short*) verify_buf;
  uvp = (unsigned short*) verify_buf;
  ivp = (int*) verify_buf;
  uivp = (unsigned int*) verify_buf;
  fvp = (float*) verify_buf;
  dvp = (double*) verify_buf;

  // Now generate the verification data
  n = 0;
  tval = (verify_spec.constant_val)? verify_spec.seed:0;
  tinc = (verify_spec.constant_val)? 0:verify_spec.increment;
  ainc = verify_spec.amp_increment;
  namps = rgn->nactive_amps;
  in_overscan = true;

  // Loop over the parallel regions
  for (pr=0; pr<rgn->npar_regions; pr++) {
    yo = rgn->par_regions[pr].yo;
    height = rgn->par_regions[pr].height;
    height_b = rgn->par_regions[pr].height_b;

    // Now loop over each row in this parallel region
    r = yo;
    for (nr=0; nr<height_b; nr++) {

      // Loop over each serial region
      for (sr=0; sr<rgn->par_regions[pr].nser_regions; sr++) {
	swidth = rgn->par_regions[pr].ser_regions[sr].width;
	swidth_b = rgn->par_regions[pr].ser_regions[sr].width_b;

	// Check to see if this is an overscan region?
	if (((rgn->overscan_rows) && (pr == rgn->npar_regions - 1)) ||
	    ((rgn->overscan_cols) && (sr == rgn->par_regions[pr].nser_regions - 1))) {	
	  // Generate overscan pixels - use constant value
	  bval = uval = sval = uival = ival = verify_spec.overscan;
	  fval = dval = (float) verify_spec.overscan;
	  inc = 0;
	  rinc = 0;
	  in_overscan = true;
	} else if (in_overscan) {
	  // Normal pixels - could be incrementing or constant
	  bval = uval = sval = uival = ival = tval;
	  fval = dval = (float) tval;
	  inc = tinc;
	  rinc = rn*verify_spec.readout_increment;
	  in_overscan = false;
	}

	// Handle each data type - call template routine to generate 1 row for sr
	switch (image_info.bitpix) {
	case 8:
	  generate_pixels(bvp,bval,MAXC,n,inc,ainc,rinc,namps,swidth,swidth_b,rgn->rcf);
	  break;
	case 16:
	  if (image_info.unsigned_data)
	    generate_pixels(uvp,uval,MAXU,n,inc,ainc,rinc,namps,swidth,swidth_b,rgn->rcf);
	  else
	    generate_pixels(svp,sval,MAXS,n,inc,ainc,rinc,namps,swidth,swidth_b,rgn->rcf);
	  break;
	case 32:
	  if (image_info.unsigned_data)
	    generate_pixels(uivp,uival,MAXUI,n,inc,ainc,rinc,namps,swidth,swidth_b,rgn->rcf);
	  else
	    generate_pixels(ivp,ival,MAXI,n,inc,ainc,rinc,namps,swidth,swidth_b,rgn->rcf);
	  break;
	case -32:
	  generate_pixels(fvp,fval,MAXF,n,inc,ainc,rinc,namps,swidth,swidth_b,rgn->rcf);
	  break;
	case -64:
	  generate_pixels(dvp,dval,MAXD,n,inc,ainc,rinc,namps,swidth,swidth_b,rgn->rcf);
	  break;
	default:
	  break;
	}	
      } // end of serial region loop
      r+=rgn->ccf;
    } // end of row loop
  } // end of row region loop
}

/*
 *+
 * FUNCTION NAME: Controller::setup_readout
 *
 * INVOCATION: setup_readout(int ns, Detector_Readout readout_rec, Detector_Regions* trgn, unsigned int& isize, double exptime)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ns - number of controllers to sync
 * > readout_rec - readout request structure
 * > trgn - readout regions object
 * ! isize - total bytes in readout - may be modified by controller specific req
 * > exptime - last exposure time
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: setup the readout
 *
 * DESCRIPTION: Generic setup stuff for all controllers
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::setup_readout(int ns, Detector_Readout* readout_rec, Detector_Regions* trgn,
			       IR_Exposure_Info& ir_einfo, unsigned long& isize, double exptime)
{
  actual_image_size = isize;
  n_sync = (ns>0)? ns:1;
  regions = readout_rec->regions;
  rgn = trgn;
  last_exptime = exptime;
  rows_transfered = 0;
  transfer_buffer = 0;
  buffer_number = 0;

  verify_spec = readout_rec->verify_spec;
}

/*
 *+
 * FUNCTION NAME: Controller::init_readout
 *
 * INVOCATION: init_readout(int s, int r)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > s - sample number
 * > r - read number
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: initialise readout parameters
 *
 * DESCRIPTION:
 * Generic readout init stuff for all controllers.
 * A full readout can consist of a set on n individual readouts each
 * of which can have s samples.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::init_readout(int s, int r)
{
  if ((r==0) && (s==0)) {
    rows_transfered = 0;
    transfer_buffer = 0;
  }
  buffer_number = transfer_buffer%camera.n_buffers;
  doing_image_transfer = true;
  finished_image_transfer = false;
}

/*
 *+
 * FUNCTION NAME: Controller::finalise_readout
 *
 * INVOCATION: finalise_readout()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: finalise readout parameters
 *
 * DESCRIPTION: Generic finalise stuff for all controllers
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::finalise_readout()
{
  doing_image_transfer = false;
  finished_image_transfer = true;
}

/*
 *+
 * FUNCTION NAME: Controller::cleanup_exposure()
 *
 * INVOCATION: controller>cleanup_exposure()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Perform necessary steps cleaning up after an IR exposure
 *
 * DESCRIPTION:
 * Joins the IR timer and image transfer threads. Should be overridden if other
 * specific exposure things are required for each controller
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::cleanup_exposure()
{

  // Wait to join image transfer thread - thread timeout prevents permanent block
  if (image_transfer_thr != NULL) {

    // Signal readout thread that we are finishing up
    image_transfer_thr->lock();
    exposure_done = true;
    image_transfer_thr->signal();
    image_transfer_thr->unlock();

    delete image_transfer_thr;
    image_transfer_thr = nullptr;
  }

}


/*
 *+
 * FUNCTION NAME: Controller::change_debug
 *
 * INVOCATION: hard->change_debug(int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > deb - debugging flag
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sets reporting object and debug level
 *
 * DESCRIPTION: Sets the controller debugging level and associated report object.
 * Expects calling program to deal with allocation/deallocation of object
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::change_debug(int deb)
{
  debug = deb;                   // debugging level
}

/*
 *+
 * FUNCTION NAME: Controller::change_simulation_mode
 *
 * INVOCATION: hard->change_simulation_mode(int sim)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > sim - simulation level
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sets simulation_mode level
 *
 * DESCRIPTION: Sets the controller simulation level.
 * Simple changes the level in the status structure.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Controller::change_simulation_mode(int sim)
{
  camera_statusp->put(CONTROLLER_SIMULATION, (long)sim);
}

/*
 *+
 * FUNCTION NAME: Controller::~Controller
 *
 * INVOCATION: delete controller
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
*
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Destructor
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Controller::~Controller()
{
  if (controller_statusp != NULL)
    delete controller_statusp;
  if (controller_pDB != NULL)
    delete controller_pDB;
  if (image_buffer != NULL) {
    free((void*)image_buffer);
  }
  if (verify_buf != NULL)
    delete [] verify_buf;
}
