/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  utility_test.cc
 * 
 * GENERAL DESCRIPTION
 *  Main program to test utility C++ classes 
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 1993
 *
 * HISTORY
 *
 */

/* Include files */

#include "utility.h"
#include "exception.h"

/* defines */

/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/* class function definitions */

/* Main program */
#ifdef vxWorks
int utilityTest()
#else
int main()
#endif
{
  try {
    ostringstream ostr;



    size_t start = 3;
    int match_pos, len;

    string str("hh mm   ss.s");
    string re("([ \t]*[ \t,][ \t]*)");

    match_pos = regx_match(str.substr(start,str.length()), re, len);
    cout << "String is " << str << "\n";
    cout << "RE is " << re << "\n";
    cout << "start position is " << start << "\n";
    cout << "match position (relative to start) is " << match_pos << "\n";
    cout << "length of matched re is " << len << "\n";

  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return(0);
}


