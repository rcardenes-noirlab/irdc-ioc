/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * CICADA PROJECT
 *
 * FILENAME parameter.h
 * 
 * PURPOSE
 * Definitions for anonymous parameter dictionary class
 *
 * REQUIREMENTS
 * Requires use of STL classes "hash_map" and "string"
 *
 */

#ifndef PARAMETER_H
#define PARAMETER_H

#include <map>
#include <list>
#include <string>
#include <typeinfo>
#include "utility.h"
#include "exception.h"
#include "parameterDB.h"
#include <cstdio>
#include <algorithm>

using namespace std;

// put definitions here

//+ Parameter constants
const int MAXSTRLEN = 2048;
const bool PUBLISHED = true;
const bool UNPUBLISHED = false;

//+ Set of parameter data types
enum DType {
  PARAM_NONE,
  PARAM_STRING, 
  PARAM_CHAR, 
  PARAM_UCHAR, 
  PARAM_SHORT, 
  PARAM_USHORT, 
  PARAM_INT, 
  PARAM_UINT, 
  PARAM_LONG, 
  PARAM_ULONG, 
  PARAM_FLOAT, 
  PARAM_DOUBLE, 
  PARAM_BOOL
};

//+ Set of validity checks
enum ValidityType {
  VALIDITY_NONE, 
  VALIDITY_RANGE, 
  VALIDITY_SET
};

//+ Structure of information about each parameter that is added to the map
struct ParameterData {
  string name;              //+ Common name of parameter
  DType dType;              //+ Data type of parameter
  ValidityType vType;       //+ Type of validity checking
  string dbName;            //+ Name of parameter in DB
  void *addr;               //+ Address of parameter in DB
  void *localAddr;          //+ Address of parameter in local structure
  unsigned int n;           //+ Number of elements for this parameter
  unsigned int nValid;      //+ number of elements in validity range or set 
  void* valid;              //+ values in validity checks
  bool is_published;        //+ Is this parameter published?

  //+ Default constructor
  ParameterData() {
    name = "";
    dType = PARAM_NONE;
    vType = VALIDITY_NONE;
    dbName = "";
    addr = NULL;
    localAddr = NULL;
    n = 0;
    nValid = 0;
    valid = NULL;
    is_published = false;
  };

  //+ Copy constructor
  ParameterData(const ParameterData& p) {
    name = p.name;
    dType = p.dType;
    vType = p.vType;
    dbName = p.dbName;
    addr = p.addr;
    localAddr = p.localAddr;
    n = p.n;
    nValid = p.nValid;
    valid = p.valid;
    is_published = p.is_published;
  };

  //+ Assignment operator
  ParameterData& operator=(const ParameterData& p) {
    name = p.name;
    dType = p.dType;
    vType = p.vType;
    dbName = p.dbName;
    addr = p.addr;
    localAddr = p.localAddr;
    n = p.n;
    nValid = p.nValid;
    valid = p.valid;
    is_published = p.is_published;
    return *this;
  }
};

//+ Maps used to hold information about parameters
class Parameter;
typedef map<string, Parameter*, less<string> > ParamMapType;
typedef map<string, ParameterData, less<string> > ParameterData_Map;

/*
CLASS
    ParameterDB
    Base parameter class
DESCRIPTION  
    Base parameter class - this is basis for parameter database where
    mapping between a parameter name and actual storage is maintained.
    Uses a parameterDB for actual storage management. This class maintains
    all information about the parameter to acheive this.

KEYWORDS
    ParameterDB
*/
class Parameter {
 private:

  //+ deletes arbitrary data type allocated with new
  void delete_data(DType dType, void* data);

  // GROUP:   Member variables
  ParameterData_Map paramDict;   //+ Dictionary of added parameters
  ParameterDB *paramDB;          //+ Parameter DB to use for maintaining values
  string paramDBName;            //+ Name of this parameter object
  string prefix;                 //+ parameter prefix
  string suffix;                 //+ parameter suffix

 public:
  
  //+ Constructor - empty
  Parameter();

  //+ Constructor - full
  // > db - pointer to ParameterDB object for managing DB parameters
  // > pname - name of Parameter Database
  // > pm - type of parameter map
  // > pre - parameter prefix
  // > suf - parameter suffix
  Parameter(ParameterDB *db, string pname, ParamMapType *pm = NULL, string pre = "", string suf = "");

  //+ Destructor
  ~Parameter();

  // GROUP:   Member functions
  //+ Reset method - resets status pars to default
  void reset();


  //+ A template for other simple types
  // > param - name of parameter
  // > dbName - name of parameter in DB - may be NULL if this parameter is only local
  // > addr - pointer to local addr - used for memcpy
  // > init - initialise this parameter to supplied defValue 
  // > n - number of elements this parameter has
  // > defValue - use if init is set
  // > vType - type of validity checking to use
  // > nv - number of elements in validity value array
  // > valid - array of validity values
  // > pub_state - set if this parameter is to be published
  template <typename T> void add(const char* param, const char* dbName, const void* addr, bool init, unsigned int n, 
				 const T* defValue, ValidityType vType, unsigned int nv, const T* valid, const bool pub_state);
  template <typename T> void add(const string param, const char* dbName, const void* addr, bool init, unsigned int n, 
				 const T* defValue, ValidityType vType, unsigned int nv, const T* valid, const bool pub_state);
  template <typename T> void add(const string param, const string dbName, const void* addr, bool init, unsigned int n, 
				 const T* defValue, ValidityType vType, unsigned int nv, const T* valid, const bool pub_state);

  //+ A template put for other simple types
  // > param - name of parameter
  // > value - value of parameter to set
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to put - default 1
  template <typename T> void put(const char* param, const T* value, unsigned int offset, unsigned int n);
  template <typename T> void put(const string param, const T* value, unsigned int offset, unsigned int n);

  //+ Simplified puts for single values
  // > param - name of parameter
  // > value - value of parameter to set
  template <typename T> void put(const char* param, const T value);
  template <typename T> void put(const string param, const T value);

  //+ Handle char* separately
  // > param - name of parameter
  // > value - value of parameter to set
  void put(const char* param, char* value);
  void put(const string param, char* value);

  //+ A template check for other simple types
  // > param - name of parameter
  // > value - value of parameter to set
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to check - default 1
  template <typename T> void check(const char* param, const T* value, unsigned int offset, unsigned int n);
  template <typename T> void check(const string param, const T* value, unsigned int offset, unsigned int n);

  //+ Simplified checks for single values
  // > param - name of parameter
  // > value - value of parameter to set
  template <typename T> void check(const char* param, const T value);
  template <typename T> void check(const string param, const T value);

  //+ Handle char* separately
  // > param - name of parameter
  // > value - value of parameter to set
  void check(const char* param, char* value);
  void check(const string param, char* value);

  //+ A template get for other simple types
  // > param - name of parameter
  // < value - returned value of parameter
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to get - default 1
  template <typename T> void get(const char* param, T* value, unsigned int offset, unsigned int n);
  template <typename T> void get(const string param, T* value, unsigned int offset, unsigned int n);

  //+ Simplified get for single values
  // > param - name of parameter
  // < value - returned value of parameter
  template <typename T> void get(const char* param, T& value);
  template <typename T> void get(const string param, T& value);

  //+ Clear methods - use to clear param flag once read
  // > param - name of parameter
  void clear(const char* param);

  //+ Clear methods - use to clear param flag once read
  // > pData - full parameter structure
  void clear(ParameterData& pData);

  //+ param_to_str method - returns a string representation of a parameter
  string param_to_str(const char* param, unsigned int offset, unsigned int n);

  //+ List method - lists all parameters to standard out
  void list();

  //+ Query data type method - use to find out data type of parameter
  // > param - name of parameter
  DType dType(const char* param);
  DType dType(const string param);

  //+ Find out whether the given parameter is published
  // > param - name of parameter
  bool is_published(const char* param);

  //+ Add a callback
  int add_callback(const char* param, Parameter_Callback *callback);

  //+ Remove a callback
  void remove_callback(int cb_id);

  //+ Wait for a parameter to change
  // > param - name of parameter
  void wait(const char* param);
};

//+ template method definition - so that instantiation can occur whenever needed
//+ template for other types of add
template <typename T>
void Parameter::add(const char* param, const char* dbName, const void* addr, bool init, unsigned int n, const T* defValue, 
		    ValidityType vType, unsigned int nv, const T* valid, const bool pub_state)
{
  DType dtype;
  ostringstream ostr;
  ParameterData pData;
  unsigned int i;
  T* val;

  // This looks bad, but the compiler should be able to unravel it
  // - the if conditions are all constant once a particular
  // function is instantiated.  It's not possible to switch on
  // a typeid by the way.
  if (typeid(T) == typeid(string)) {
    dtype = PARAM_STRING;
  } else if (typeid(T) == typeid(char)) {
    dtype = PARAM_CHAR;
  } else if (typeid(T) == typeid(unsigned char)) {
    dtype = PARAM_UCHAR;
  } else if (typeid(T) == typeid(short)) {
    dtype = PARAM_SHORT;
  } else if (typeid(T) == typeid(unsigned short)) {
    dtype = PARAM_USHORT;
  } else if (typeid(T) == typeid(int)) {
    dtype = PARAM_INT;
  } else if (typeid(T) == typeid(unsigned int)) {
    dtype = PARAM_UINT;
  } else if (typeid(T) == typeid(long)) {
    dtype = PARAM_LONG;
  } else if (typeid(T) == typeid(unsigned long)) {
    dtype = PARAM_ULONG;
  } else if (typeid(T) == typeid(float)) {
    dtype = PARAM_FLOAT;
  } else if (typeid(T) == typeid(double)) {
    dtype = PARAM_DOUBLE;
  } else if (typeid(T) == typeid(bool)) {
    dtype = PARAM_BOOL;
  } else {
    ostr << "Unknown parameter type '" << typeid(T).name() << "'!" << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }

  if (strlen(param) > 0) {
    // Add to dictionary - first construct a short param data structure
    pData.name = param;
    pData.dType = dtype;
    pData.vType = vType;

    // Look up the parameter's address in the database
    pData.addr = paramDB->setAddr(dbName, param, prefix.c_str(), suffix.c_str());
    pData.localAddr = (void*) addr;

    // If no DB address set to localAddr so that local DB is also DB
    // Note that this only works if the underlying parameterDB class treats
    // local and DB addresses in the same way.
    if (!pData.addr) {
      pData.addr = pData.localAddr;
    }

    if (dbName && strcmp(dbName,"")) {
      pData.dbName = prefix + string(dbName) + suffix;
    } else {
      pData.dbName = "";
    }

    pData.n = n;
    pData.nValid = 0;
    pData.is_published = pub_state;
    
    // Allocate space for validity data, but only for RANGE and SET
    // not for NONE
    switch (vType) {
      case VALIDITY_RANGE:
      case VALIDITY_SET:
	if ((nv > 0) && (valid != NULL)) {
	  //cout << "Allocating space for " << nv << " validity values" << endl;
	  val = new T[nv];
	  pData.valid = (void*) val;
	  for (i=0; i<nv; i++) {
	    val[i] = valid[i];
	  }
	  pData.nValid = nv;
	}
	break;
      default:
	break;
    }

    if (pub_state == PUBLISHED) {
      paramDB->publish(pData.addr);
    }
    // Attach the database and local addresses
    paramDB->attach(pData.addr, pData.localAddr);

    paramDict.insert(make_pair(string(param),pData));
    
    // Write default value to DB
    if (init) {
      put(param, defValue, 0, n);
    }

  } else {
    throw Error("Null parameter name - cannot add to database!", E_WARNING, -1, __FILE__, __LINE__);    
  }
}
// Alternative add prototype - convenience for caller
template <typename T>
void Parameter::add(const string param, const char* dbName, const void* addr, bool init, unsigned int n, const T* defValue, 
		    ValidityType vType, unsigned int nv, const T* valid, const bool pub_state)
{
  add(param.c_str(),dbName,addr,init,n,defValue,vType,nv,valid,pub_state);
}
// 2nd alternative add prototype - convenience for caller
template <typename T>
void Parameter::add(const string param, const string dbName, const void* addr, bool init, unsigned int n, const T* defValue, 
		    ValidityType vType, unsigned int nv, const T* valid, const bool pub_state)
{
  add(param.c_str(),dbName.c_str(),addr,init,n,defValue,vType,nv,valid,pub_state);
}

// template for other types of check
template <typename T>
void Parameter::check(const char* param, const T* value, unsigned int offset, unsigned int n)
{
  ParameterData pData;
  ParameterData_Map::iterator iter;
  ostringstream ostr;
  bool valid = true;
  bool v;
  unsigned int i,j;
  T* val;
  DType temp_dtype;

  if ((iter = paramDict.find(param)) == paramDict.end()) {
    ostr << "Attempt to check parameter '" << param << "' that doesn't belong in database?" << ends; 
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }
  pData = (*iter).second;

  // Check validity of value

  // This looks bad, but the compiler should be able to unravel it
  // - the if conditions are all constant once a particular
  // function is instantiated.  It's not possible to switch on
  // a typeid by the way.
  if (typeid(T) == typeid(string)) {
    temp_dtype = PARAM_STRING;
  } else if (typeid(T) == typeid(char)) {
    temp_dtype = PARAM_CHAR;
  } else if (typeid(T) == typeid(unsigned char)) {
    temp_dtype = PARAM_UCHAR;
  } else if (typeid(T) == typeid(short)) {
    temp_dtype = PARAM_SHORT;
  } else if (typeid(T) == typeid(unsigned short)) {
    temp_dtype = PARAM_USHORT;
  } else if (typeid(T) == typeid(int)) {
    temp_dtype = PARAM_INT;
  } else if (typeid(T) == typeid(unsigned int)) {
    temp_dtype = PARAM_UINT;
  } else if (typeid(T) == typeid(long)) {
    temp_dtype = PARAM_LONG;
  } else if (typeid(T) == typeid(unsigned long)) {
    temp_dtype = PARAM_ULONG;
  } else if (typeid(T) == typeid(float)) {
    temp_dtype = PARAM_FLOAT;
  } else if (typeid(T) == typeid(double)) {
    temp_dtype = PARAM_DOUBLE;
  } else if (typeid(T) == typeid(bool)) {
    temp_dtype = PARAM_BOOL;
  } else {
    ostr << "Unknown parameter type '" << typeid(T).name() << "'!" << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }

  if (pData.dType != temp_dtype) {
    ostr << "Attempt to check data of type " << temp_dtype << " into param '" << param << "' of type " << pData.dType << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }

  if (pData.name != param) {
    ostr << "Parameter '" << param << "' isn't in database?" << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }

  if (n+offset>pData.n) {
    ostr << "Data out of bounds, param='" << param<< "', offset=" << 
      offset << ", n=" << n << ", max n=" << pData.n << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }

  //cout << "Current dictionary value " << pData.lValue << endl;
  if (pData.nValid > 0) {
    val = (T*) pData.valid;
    switch (pData.vType) {
    case VALIDITY_RANGE: 
      for (i=0; i<n; i++) {
	if ((value[i] < val[0]) || (value[i] > val[1])) {
	  ostr << "'" << value[i] << "' out of range '" << val[0] << "'-'" << val[1] << 
	    "' for '" << param << "'" << ends;
	  throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
	}
      }
      break;
    case VALIDITY_SET:
      for (i=0; (valid && (i<n)); i++) {
	j = 0;
	v = false;
	while (!v && (j<pData.nValid)) {
	  if (val[j] != value[i]) j++;
	  else v = true;
	}
	valid = v;
      }
      if (!valid) {
	ostr << "'" << value[i-1] << "' not in valid set for '" << param << "'" << ends;
	throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
      }
      break;
    default:
	break;
    }
  }
}
// Alternative check prototypes - convenience
template <typename T>
void Parameter::check(const string param, const T* value, unsigned int offset, unsigned int n)
{
  check(param.c_str(),value,offset,n);
}
template <typename T> void Parameter::check(const char* param, const T value)
{
  check(param,&value,0,1);
}
template <typename T> void Parameter::check(const string param, const T value)
{
  check(param.c_str(),&value,0,1);
}
  
// template for other types of put
template <typename T>
void Parameter::put(const char* param, const T* value, unsigned int offset, unsigned int n)
{
  ParameterData pData;
  ParameterData_Map::iterator iter;
  ostringstream ostr;

  if ((iter = paramDict.find(param)) == paramDict.end()) {
    ostr << "Attempt to check parameter '" << param << "' that doesn't belong in database?" << ends; 
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }
  pData = (*iter).second;

  // Check validity of value - will throw an exception if not valid
  check(param, value, offset, n);

  // Write value to DB variable
  paramDB->put(paramDBName.c_str(), pData.dbName.c_str(), pData.name.c_str(), pData.addr, pData.localAddr, value, offset, n);
  (*iter).second = pData;
}
// Alternative put prototypes - convenience
template <typename T>
void Parameter::put(const string param, const T* value, unsigned int offset, unsigned int n)
{
  put(param.c_str(),value,offset,n);
}
template <typename T> void Parameter::put(const char* param, const T value)
{
  put(param,&value,0,1);
}
template <typename T> void Parameter::put(const string param, const T value)
{
  put(param.c_str(),&value,0,1);
}
// template for other types of get

template <typename T>
void Parameter::get(const char* param, T* value, unsigned int offset, unsigned int n)
{
  ParameterData pData;
  ParameterData_Map::iterator iter;
  ostringstream ostr;
  unsigned int i,j,i1;
  bool valid = true;
  bool v;
  T* val;

  if ((iter = paramDict.find(param)) == paramDict.end()) {
    ostr << "Attempt to get parameter '" << param << "' that doesn't belong in database?" << ends; 
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }
  pData = (*iter).second;

  if (n+offset>pData.n) {
    ostr << "Attempt to get data out of bounds, param='" << param << "', offset=" << 
      offset << ", n=" << n << ", max n=" << pData.n << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }

  // Read value from DB variable
  paramDB->get(paramDBName.c_str(), pData.dbName.c_str(), pData.name.c_str(), pData.addr, pData.localAddr, value, offset, n);

  // Check that value is valid - could have been put there by CA
  if (pData.nValid > 0) {
    val = (T*) pData.valid;
    switch (pData.vType) {
    case VALIDITY_RANGE: 
      for (i=0; i<n; i++) {
	if ((value[i] < val[0]) || (value[i] > val[1])) {
	  ostr << "'" << value[i] << "' out of range '" << val[0] << "'-'" << val[1] << 
	    "' for '" << param << "'" << ends;
	  throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);
	}
      }
      break;
    case VALIDITY_SET:
      for (i=0; (valid && (i<n)); i++) {
	j = 0;
	v = false;
	i1 = i;
	while (!v && (j<pData.nValid)) {
	  if (val[j] != value[i]) j++;
	  else v = true;
	}
	valid = v;
      }
      if (!valid) {
	ostr << "'" << value[i1] << "' not in valid set for '" << param << "'" << ends;
	throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
      }
      break;
    default:
	break;
    }
  }

  (*iter).second = pData;
}
template <typename T>
void Parameter::get(const string param, T* value, unsigned int offset, unsigned int n)
{
  get(param.c_str(),value,offset,n);
}

// Special case single pass by ref get for single value
template <typename T> void Parameter::get(const char* param, T& value)
{
  get(param,&value,0,1);
}
template <typename T> void Parameter::get(const string param, T& value)
{
  get(param.c_str(),&value,0,1);
}

#endif /* PARAMETER_H */
