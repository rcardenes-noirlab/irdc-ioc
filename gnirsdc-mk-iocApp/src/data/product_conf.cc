#include "product_conf.h"
#include <cstdio>

void
HeaderConf::add_card(const std::string& name, const std::string& value)
{
	cards[name] = value;
}

void HeaderConf::save_to(json_object* target) const
{
	for (const auto& kv: cards) {
		json_object_object_add(target, kv.first.c_str(),
					json_object_new_string(kv.second.c_str()));
	}
}


void
ProductConf::add_unit(const std::string name)
{
	// This forces an insertion if the name didn't exist yet,
	// but does nothing otherwise.
	units[name];
}

HeaderConf&
ProductConf::operator[](const std::string name)
{
	return units[name];
}

void ProductConf::save_to(std::ofstream& output) const
{
	json_object *json_output = json_object_new_object();

	for (const auto& kv: units) {
		json_object *section = json_object_new_object();

		json_object_object_add(json_output, kv.first.c_str(), section);
		kv.second.save_to(section);
	}

	output << json_object_to_json_string_ext(json_output, JSON_C_TO_STRING_PRETTY);
	output.flush();

	// Destroy the JSON object
	json_object_put(json_output);
}
