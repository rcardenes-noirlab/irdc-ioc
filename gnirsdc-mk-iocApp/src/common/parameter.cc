/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * CICADA PROJECT
 * 
 * FILENAME parameter.cc
 * 
 * GENERAL DESCRIPTION
 * Implements methods for the anonymous parameter class. 
 * This class encapsulates all the parameters used in the 
 * system that are handled by the chosen ParameterDB interface
 *
 * REQUIREMENTS
 * Requires use of STL classes "map" and "string"
 *
 */

/* Include files */
#include <stdio.h>
#include <map>
#include <string>
#include "parameter.h"
#include "parameterDB.h"
#include "utility.h"
#include "exception.h"

using namespace std;

/*
 *+ 
 * FUNCTION NAME: Parameter::Parameter
 * 
 * INVOCATION: dcParameter = new Parameter();
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Parameter constructor - simple
 * 
 * DESCRIPTION: sets up defaults
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
Parameter::Parameter() 
{
  paramDB = NULL;
  paramDict.clear();
  paramDBName = "Null";
}
/*
 *+ 
 * FUNCTION NAME: Parameter::Parameter
 * 
 * INVOCATION: dcParameter = new Parameter(ParameterDB *db);
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > db - pointer to external parameter DB instance 
 * > pname - Name of parameter map
 * > pm - pointer to parameter name to parameter object mapping, used to ensure
 *        single named instance of a parameter object
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Parameter constructor
 * 
 * DESCRIPTION: Resets all Parameter parameters by calling reset method
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
Parameter::Parameter(ParameterDB *db, string pname, ParamMapType *pm, string pre, string suf) 
{
  ParamMapType::iterator iter;
  ostringstream ostr;

  paramDB = db;
  reset();
  paramDBName = pname;
  prefix = pre;
  suffix = suf;

  if (pm != NULL) {
    // Check that there isn't already a parameter object with this name
    if ((iter = pm->find(paramDBName)) != pm->end()) {
      ostr << "Attempt to add parameter object '" << paramDBName << "' that already exists" << ends;
      throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);
    }
    (*pm)[paramDBName] = this;
  }
}

/*
 *+ 
 * FUNCTION NAME: void Parameter::reset()
 * 
 * INVOCATION: dcParameter->reset();
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Reset method - resets pars to default
 * 
 * DESCRIPTION: Resets all Parameter parameters 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void Parameter::reset()
{
  ParameterData pData;
  ParameterData_Map::iterator iter;

  if (!paramDict.empty()) {
    // clear out any parameter values.
    iter = paramDict.begin();
    while (iter != paramDict.end()) {
      pData = (*iter).second; 
      if (pData.nValid > 0)
	delete_data(pData.dType, pData.valid);

      if ((pData.dbName != "" ) && pData.addr && (pData.addr != pData.localAddr)) {
	paramDB->freeAddr(pData.addr);
      }
      iter++;
    }
    paramDict.clear();
  }
}


/*
 *+ 
 * FUNCTION NAME: void Parameter::put(const char* param, char* value)
 * 
 * INVOCATION: dcParameter->set(param, val);
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > param - name of parameter to set
 * > val - value to set parameter to
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Put method - short form for char string params
 * 
 * DESCRIPTION: Sets a named parameter.
 * Uses map to store parameter/value pairs and also sets in external DB.
 * Calls long form with defaults
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void Parameter::put(const char* param, char* value)
{
  put(param,(const char*)value,0,strlen(value)+1);
}
void Parameter::put(const string param, char* value)
{
  put(param.c_str(),(const char*)value,0,strlen(value)+1);
}
/*
 *+ 
 * FUNCTION NAME: short Parameter::dType(const char* param)
 * 
 * INVOCATION: dtype = dcParameter->dType(param);
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > param - name of parameter to query
 * 
 * FUNCTION VALUE: data type of parameter value
 * 
 * PURPOSE: Determine data type of a parameter
 * 
 * DESCRIPTION: Finds parameter in dictionary and returns data type.
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
DType Parameter::dType(const char* param)
{
  ParameterData pData;
  ParameterData_Map::iterator iter;
  ostringstream ostr;

  if ((iter = paramDict.find(param)) == paramDict.end()) {
    ostr << "Attempt to get dType of parameter '" << param << "' that doesn't belong in database?" << ends; 
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }
  pData = (*iter).second;

  return(pData.dType);
}
DType Parameter::dType(const string param)
{
  return dType(param.c_str());
}

/*
 *+ 
 * FUNCTION NAME: void Parameter::clear(const char* param)
 * 
 * INVOCATION: dcParameter->clear(param);
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > param - name of parameter to clear
 * or 
 * > pData - parameter data structure
 * 
 * FUNCTION VALUE: parameter value
 * 
 * PURPOSE: Clear method - clears set flag for parameter
 * 
 * DESCRIPTION: Clears a named parameter.
 * Alternatively uses map to retrieve parameter pairs or
 * uses ParameterData as input argument.
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void Parameter::clear(const char* param)
{
  ParameterData pData;
  ParameterData_Map::iterator iter;
  ostringstream ostr;

  if (strlen(param)>0) {
    if ((iter = paramDict.find(param)) == paramDict.end()) {
      ostr << "Attempt to clear parameter '" << param << "' that doesn't belong in database?" << ends; 
      throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
    }
    pData = (*iter).second;
    if (pData.name == param) {
      if (pData.nValid > 0) 
	delete_data(pData.dType, pData.valid);

      if ((pData.dbName != "" ) && pData.addr && (pData.addr != pData.localAddr)) 
	paramDB->freeAddr(pData.addr);
      paramDict.erase(iter);
    }
  }
}
void Parameter::clear(ParameterData& pData)
{
  if (pData.nValid > 0) 
    delete_data(pData.dType, pData.valid);
  
  if ((pData.dbName != "" ) && pData.addr && (pData.addr != pData.localAddr)) {
    paramDB->freeAddr(pData.addr);
  }
  paramDict.erase(pData.name);
}

/*
 *+ 
 * FUNCTION NAME: void Parameter::string(const char* param)
 * 
 * INVOCATION: dcParameter->string(param);
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *  > param - parameter name
 * 
 * FUNCTION VALUE: A string representation of the given parameter
 * 
 * PURPOSE: See the value of a parameter as a string
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
string Parameter::param_to_str(const char* param, unsigned int offset, unsigned int n)
{
  ParameterData_Map::iterator iter;
  ParameterData pData;
  unsigned int i;
  string *str = nullptr;
  char *cval = nullptr;
  unsigned char *ucval = nullptr;
  short *sval = nullptr;
  unsigned short *usval = nullptr;
  int *ival = nullptr;
  unsigned int *uival = nullptr;
  long *lval = nullptr;
  unsigned long *ulval = nullptr;
  float *fval = nullptr;
  double *dval = nullptr;
  bool *bval = nullptr;
  void *data = nullptr;
  ostringstream ostr;
  string ret;

  if ((iter = paramDict.find(param)) == paramDict.end()) {
    ostr << "Attempt to get parameter '" << param << "' that doesn't belong in database?" << ends; 
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }
  pData = (*iter).second;
  
  switch (pData.dType) {
  case PARAM_STRING:
    str = new string[n];
    data = (void*) str;
    get(pData.name.c_str(), str, offset, n);
    break;
  case PARAM_CHAR:
    cval = new char[n+1];
    data = (void*) cval;
    get(pData.name.c_str(), cval, offset, n);
    cval[n] = 0;
    break;
  case PARAM_UCHAR:
    ucval = new unsigned char[n+1];
    data = (void*) ucval;
    get(pData.name.c_str(), ucval, offset, n);
    ucval[n] = 0;
    break;
  case PARAM_SHORT:
    sval = new short[n];
    data = (void*) sval;
    get(pData.name.c_str(), sval, offset, n);
    break;
  case PARAM_USHORT:
    usval = new unsigned short[n];
    data = (void*) usval;
    get(pData.name.c_str(), usval, offset, n);
    break;
  case PARAM_INT:
    ival = new int[n];
    data = (void*) ival;
    get(pData.name.c_str(), ival, offset, n);
    break;
  case PARAM_UINT:
    uival = new unsigned int[n];
    data = (void*) uival;
    get(pData.name.c_str(), uival, offset, n);
    break;
  case PARAM_LONG:
    lval = new long[n];
    data = (void*) lval;
    get(pData.name.c_str(), lval, offset, n);
    break;
  case PARAM_ULONG:
    ulval = new unsigned long[n];
    data = (void*) ulval;
    get(pData.name.c_str(), ulval, offset, n);
    break;
  case PARAM_FLOAT:
    fval = new float[n];
    data = (void*) fval;
    get(pData.name.c_str(), fval, offset, n);
    break;
  case PARAM_DOUBLE:
    dval = new double[n];
    data = (void*) dval;
    get(pData.name.c_str(), dval, offset, n);
    break;
  case PARAM_BOOL:
    bval = new bool[n];
    data = (void*) bval;
    get(pData.name.c_str(), bval, offset, n);
    break;
  default:
    break;
  }
  switch (pData.dType) {
  case PARAM_CHAR:
    ostr << cval;
    break;
  case PARAM_UCHAR:
    ostr << ucval;
    break;
  default:
    for (i=0; i<pData.n; i++) {
      if (i>0) ostr << ",";
      switch (pData.dType) {
      case PARAM_STRING:
	ostr << str[i]; 
	break;
      case PARAM_SHORT:
	ostr << sval[i]; 
	break;
      case PARAM_USHORT:
	ostr << usval[i]; 
	break;
      case PARAM_INT:
	ostr << ival[i]; 
	break;
      case PARAM_UINT:
	ostr << uival[i]; 
	break;
      case PARAM_LONG:
	ostr << lval[i]; 
	break;
      case PARAM_ULONG:
	ostr << ulval[i]; 
	break;
  #ifdef CICADA_ONLY
      case PARAM_LONGLONG:
	ostr << llval[i]; 
	break;
  #endif
      case PARAM_FLOAT:
	ostr << fval[i]; 
	break;
      case PARAM_DOUBLE:
	ostr << dval[i]; 
	break;
      case PARAM_BOOL:
	ostr << bval[i]; 
	break;
      default:
	break;
      }
    }
  }

  // Free the temp allocation
  delete_data(pData.dType, data);
    
  ostr << ends;
    
  ret = ostr.str();
    
  // Unfreeze stream to avoid memory leaks
  // ostr.rdbuf()->freeze(0);
    
  return ret;
}

/*
 *+ 
 * FUNCTION NAME: void Parameter::list()
 * 
 * INVOCATION: dcParameter->list();
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: List method 
 * 
 * DESCRIPTION: lists all parameters in dictionary to stdout
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void Parameter::list()
{
  ParameterData_Map::iterator iter;
  ParameterData pData;
  
  for (iter=paramDict.begin(); (iter != paramDict.end()); iter++) {
    pData = (*iter).second;
    // Check that this parameter really exists
    if ((*iter).first == pData.name) {
      cout << (*iter).first << ": '" <<  pData.dbName << "' = ";
      cout << param_to_str(pData.name.c_str(), 0, pData.n) << endl;
    }
  }
  
}
 
/*
 *+ 
 * FUNCTION NAME: bool Parameter::is_published(const char* param)
 * 
 * INVOCATION: dcParameter->is_published(param);
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *  > param - parameter name to query
 * 
 * FUNCTION VALUE: true if the given param is published
 * 
 * PURPOSE: Determine publishing status of given param
 * 
 * DESCRIPTION: Finds given param and retrieves its is_published flag
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
bool Parameter::is_published(const char* param)
{
  ParameterData pData;
  ParameterData_Map::iterator iter;
  ostringstream ostr;

  if ((iter = paramDict.find(param)) == paramDict.end()) {
    ostr << "Attempt to call 'is_published' on parameter '" << param << "' that doesn't belong in database?" << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }
  pData = (*iter).second;

  return(pData.is_published);
}

/*
 *+ 
 * FUNCTION NAME: int Parameter::add_callback(const char* param, void* (*callback)(void*), void* arg)
 * 
 * INVOCATION: dcParameter->add_callback(param, callback, arg);
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *  > param - the parameter to associate the callback with
 *  > callback - a callback function
 *  > arg - a void* that is passed to the callback each time it is called
 *          make sure that what it points to is always available...
 * 
 * FUNCTION VALUE: the callback id (an int)
 * 
 * PURPOSE: Associates the given callback function with the give parameter
 * 
 * DESCRIPTION: Creates a thread which blocks on the cv associated with the
 *  given parameter then executes the given callback function, all in a loop.
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
int Parameter::add_callback(const char* param, Parameter_Callback* callback)
{
  ostringstream ostr;
  ParameterData pData;
  ParameterData_Map::iterator iter;

  if ((iter = paramDict.find(param)) == paramDict.end()) {
    ostr << "Attempt to add callback for parameter '" << param << "' that doesn't belong in database?" << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }
  pData = (*iter).second;

  if (!pData.is_published) {
    ostr << "Attempt to add callback for a private parameter '" << param << "'" << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }
  return paramDB->add_callback(pData.addr, callback);
}

/*
 *+ 
 * FUNCTION NAME: int Parameter::remove_callback(int cb_id)
 * 
 * INVOCATION: dcParameter->remove_callback(cb_id);
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *  > cb_id - the id of the callback to remove
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Removes the given callback
 * 
 * DESCRIPTION: Calls the ParameterDB::remove_callback method
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void Parameter::remove_callback(int cb_id)
{
  paramDB->remove_callback(cb_id);
}

/*
 *+ 
 * FUNCTION NAME: int Parameter::wait(const char* param)
 * 
 * INVOCATION: dcParameter->wait(const char* param)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *  > param - param to wait for
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Wait until the parameter value changes
 * 
 * DESCRIPTION: Calls the parameter database method to wait until a value change
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void Parameter::wait(const char* param)
{
  ostringstream ostr;
  ParameterData pData;
  ParameterData_Map::iterator iter;

  if ((iter = paramDict.find(param)) == paramDict.end()) {
    ostr << "Attempt to wait on parameter '" << param << "' that doesn't belong in database?" << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }
  pData = (*iter).second;

  if (!pData.is_published) {
    ostr << "Attempt to wait on a private parameter '" << param << "'" << ends;
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
  }
  paramDB->wait(pData.addr);
}

/*
 *+ 
 * FUNCTION NAME: int Parameter::delete_data(DType dType, void* data)
 * 
 * INVOCATION: dcParameter->delete_data(DType dType, void* data)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *  > dtype - data type of value to be deleted
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: deletes arbitrary data type allocated with new
 * 
 * DESCRIPTION: typescasts to correct data type and uses delete to remove
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
void Parameter::delete_data(DType dType, void* data)
{
  switch (dType) {
  case PARAM_STRING:
    delete [] (string*)data;
    break;
  case PARAM_CHAR:
    delete [] (char*)data;
    break;
  case PARAM_UCHAR:
    delete [] (unsigned char*)data;
    break;
  case PARAM_SHORT:
    delete [] (short*)data;
    break;
  case PARAM_USHORT:
    delete [] (unsigned short*)data;
    break;
  case PARAM_INT:
    delete [] (int*)data;
    break;
  case PARAM_UINT:
    delete [] (unsigned int*)data;
    break;
  case PARAM_LONG:
    delete [] (long*)data;
    break;
  case PARAM_ULONG:
    delete [] (unsigned long*)data;
    break;
  case PARAM_FLOAT:
    delete [] (float*)data;
    break;
  case PARAM_DOUBLE:
    delete [] (double*)data;
    break;
  case PARAM_BOOL:
    delete [] (bool*)data;
    break;
  default:
    break;
  }
}
/*
 *+ 
 * FUNCTION NAME: Parameter::~Parameter
 * 
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Parameter destructor
 * 
 * DESCRIPTION:
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
Parameter::~Parameter() 
{
  // Reset contents
  reset();  
}
