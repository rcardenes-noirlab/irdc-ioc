/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME detector_regions.cc
 * 
 * GENERAL DESCRIPTION
 * Implementation of RSAA's detector region class.
 * This class provides a mechanism for handling detector region specifications 
 * and their relationship with detector readout amplifiers on multi cip/amp
 * detectors. The arbitrary specification of multiple readout regions are 
 * supported though artificially limited by a maximum number of regions.
 *
 */

// Include files 
#include <errno.h>
#include <math.h>
#include "utility.h"
#include "exception.h"
#include "detector_regions.h"

/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/* class function definitions */

/*
 *+ 
 * FUNCTION NAME: 
 *  Detector_Regions::Detector_Regions()
 * INVOCATION: 
 *  Detector_Regions::Detector_Regions(Camera_Desc& cam, int ci, bool cam_co, Mosaic_Mode mm, bool append_os,
 *				       unsigned short rowcf, unsigned short colcf, 
 *				       unsigned long osrows, unsigned long oscols,
 *				       unsigned long long chip_mask)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > cam - camera description - provides geometry context for this region spec
 * > ci - controller id for this region spec
 * > cam_co - flag to indicate if following geometry info in camera coordinate space
 * > mm - mode of mosaicing output (none,amp,chip)
 * > append_os - set if overscan desired appended at end of data frame
 * > rowcf,colcf - row and column binning factors
 * > osrows,oscols - row and columns overscan sizes
 * > chip_mask - which chips in the controller description should be considered
 * 
 * FUNCTION VALUE: void
 *  
 * PURPOSE: 
 *  simple Detector_Regions constructor, using no region description
 *
 * DESCRIPTION: 
 *  setup of some variables, calls setup_amps and check_amp_config
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Detector_Regions::Detector_Regions(Camera_Desc& cam, int ci, bool cam_co, Mosaic_Mode mm, bool append_os,
				   unsigned short rowcf, unsigned short colcf, unsigned long osrows, unsigned long oscols,
				   unsigned long long chip_mask)
{
  debug = 0;
  rcf = rowcf;
  ccf = colcf;
  if (!rcf || !ccf) {
    throw Error("Detector_Regions: Row and column compression factors must be greater than zero!",
		E_ERROR, -1 ,__FILE__, __LINE__); 
  }
  overscan_rows = osrows;
  overscan_cols = oscols;
  overscan_rows_binned = (unsigned long)ceil(((float)overscan_rows)/ccf);
  overscan_cols_binned = (unsigned long)ceil(((float)overscan_cols)/rcf);
  camera = cam;
  cid = ci;
  camera_coords = cam_co;
  mosaic_mode = mm;

  append_overscan = (append_os && (nregions == 1) && (mosaic_mode == MOSAIC_NONE));

  init_arrays();

  // Always save what the user asks for
  save_chip_mask = chip_mask;  

  // But set clockout mask to what the controller can handle
  if (camera.controller[cid].mask_chips) 
    clockout_chip_mask = chip_mask;
  else 
    clockout_chip_mask = FULL_MASK;      

  try {
    setup_amps();
    check_amp_config();
  }
  catch (Error& e) {
    // Make sure space is cleaned up
    cleanup_arrays();
    e.rethrow();
  }
}

/*
 *+ 
 * FUNCTION NAME:  Detector_Regions::Detector_Regions()
 *
 * INVOCATION: rgn = new Detector_Regions(Camera_Desc& cam, int ci, bool cam_co, Mosaic_mode mm, bool append_os,
 *                                        unsigned short n, unsigned long *xo, unsigned long *width, 
 *				          unsigned long *yo, unsigned long *height,
 *				          unsigned short rowcf, unsigned short colcf,
 *				          unsigned long osrows, unsigned long oscols,
 *				          unsigned long long chip_mask)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > cam - camera description - provides geometry context for this region spec
 * > ci - controller id
 * > cam_co - flag to indicate if following geometry info in camera coordinate space
 * > mm - mode of mosaicing output (none,amp,chip)
 * > append_os - set if overscan desired appended at end of data frame
 * > n - number of regions to handle
 * > xo,yo - arrays of region origins
 * > width,height - arrays of region sizes
 * > rowcf,colcf - row and column binning factors
 * > osrows,oscols - row and columns overscan sizes
 * > chip_mask - which chips in the controller description should be considered
 * 
 * FUNCTION VALUE:  void
 *
 * PURPOSE: constructor
 *
 * DESCRIPTION: 
 *   complex constructor - passes regions descriptions in too
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Detector_Regions::Detector_Regions(Camera_Desc& cam, int ci, bool cam_co, Mosaic_Mode mm, bool append_os, unsigned short n, 
				   unsigned long *xo, unsigned long *width, unsigned long *yo, unsigned long *height,
				   unsigned short rowcf, unsigned short colcf, unsigned long osrows, unsigned long oscols,
				   unsigned long long chip_mask)
  :Regions(n,xo,width,yo,height)
{
  int i,j;
  
  debug = 0;
  rcf = rowcf;
  ccf = colcf; 
  overscan_rows = osrows;
  overscan_cols = oscols;
  overscan_rows_binned = (unsigned long)ceil(((float)overscan_rows)/ccf);
  overscan_cols_binned = (unsigned long)ceil(((float)overscan_cols)/rcf);
  camera = cam;
  cid = ci;
  camera_coords = cam_co;
  mosaic_mode = mm;

  append_overscan = (append_os && (nregions == 1) && (mosaic_mode == MOSAIC_NONE));

  init_arrays();

  // Always save what the user asks for
  save_chip_mask = chip_mask;  

  // But set clockout mask to what the controller can handle
  if (camera.controller[cid].mask_chips) 
    clockout_chip_mask = chip_mask;
  else 
    clockout_chip_mask = FULL_MASK;      

  try {
    setup_amps();
    check_amp_config();
    check_subregions_allowed();
    align_regions_with_binning();
    chip_to_amp();
    
    if (debug) {
      // Print out result
      printf("Par    yo   hei  skip    fw   rem amask rmask\n");
      for (i=0; i<npar_regions; i++) {
	printf("%3d%6ld%6ld%6ld%6ld%6ld%6llu%6llu\n",i,par_regions[i].yo,par_regions[i].height,
	       par_regions[i].skip,par_regions[i].width,par_regions[i].remain,
	       par_regions[i].amp_mask,par_regions[i].region_mask);
	printf("   Ser    xo  skip   wid amask rmask\n");
	for (j=0; j<par_regions[i].nser_regions; j++) {
	  printf("%6d%6ld%6ld%6ld%6llu%6llu\n",j,par_regions[i].ser_regions[j].xo,
		 par_regions[i].ser_regions[j].skip,par_regions[i].ser_regions[j].width,
		 par_regions[i].ser_regions[j].amp_mask,par_regions[i].ser_regions[j].region_mask);
	}
      }
    }
  }
  catch (Error& e) {
    // Make sure space is cleaned up
    cleanup_arrays();
    e.rethrow();
  }
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::init_arrays
 *
 * INVOCATION: init_arrays()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: void
 * 
 * PURPOSE: Sets working arrays to NULL
 *  
 * DESCRIPTION: 
 *   
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Detector_Regions::init_arrays()
{
  amp_xo = NULL;
  amp_yo = NULL;
  amp_prescan = NULL;
  amp_postscan = NULL;
  amp_width = NULL;
  amp_height = NULL;
  amp_xdir = NULL;
  amp_ydir = NULL;
  amp_orient = NULL;
  amp_chipnum = NULL;
  amp_ampnum = NULL;
  amp_list = NULL;
  par_regions = NULL;
}
/*
 *+ 
 * FUNCTION NAME: Detector_Regions::cleanup_arrays
 *
 * INVOCATION: cleanup_arrays()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: void
 * 
 * PURPOSE: Frees space allocated for working arrays
 *  
 * DESCRIPTION: 
 *   
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Detector_Regions::cleanup_arrays()
{
  int i,j;
  if (amp_xo != NULL)
    delete [] amp_xo;
  if (amp_yo != NULL)
    delete [] amp_yo;
  if (amp_prescan != NULL)
    delete [] amp_prescan;
  if (amp_postscan != NULL)
    delete [] amp_postscan;
  if (amp_width != NULL)
    delete [] amp_width;
  if (amp_height != NULL)
    delete [] amp_height;
  if (amp_xdir != NULL)
    delete [] amp_xdir;
  if (amp_ydir != NULL)
    delete [] amp_ydir;
  if (amp_orient != NULL)
    delete [] amp_orient;
  if (amp_chipnum != NULL)
    delete [] amp_chipnum;
  if (amp_ampnum != NULL)
    delete [] amp_ampnum;
  if (amp_list != NULL)
    delete [] amp_list;
  if (par_regions != NULL) {
    for (i=0; i<npar_regions; i++) {
      if (par_regions[i].ser_regions != NULL) {
	for (j=0; j<par_regions[i].nser_regions; j++) {
	  delete [] par_regions[i].ser_regions[j].framelist;
	  delete [] par_regions[i].ser_regions[j].subframelist;
	  delete [] par_regions[i].ser_regions[j].regionlist;
	}
	delete [] par_regions[i].ser_regions;
      }
    }
    delete [] par_regions;
  }

  // Clear the frame containers
  subframes.clear();
  for (i=0; i<(int)frames.size(); i++)
    frames[i].subframes.clear();
  frames.clear();

  amp_frame_map.clear();

  init_arrays();
}
/*
 *+ 
 * FUNCTION NAME: Detector_Regions::setup_amps()
 *
 * INVOCATION: setup_amps()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: void
 *
 * PURPOSE:  converts amplifier desc to internal variable names used in class 
 *
 * DESCRIPTION: Allocates internal working space depending on the number of
 * amps in the controller description that are being considered. Ensures limits
 * aren't exceeded.
 *   
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Detector_Regions::setup_amps() 
{
  int i,j, na;
  ostringstream ostr;

  // first - setup total number of amps in all chips
  full_controller_ampmask = namps = 0; 

  for (i=0; i<camera.controller[cid].n_chips; i++) {
    for (j=0; j<camera.controller[cid].chip[i].n_amps; j++) { 

      // Only count this amp if configured in hardware mask
      if (camera.controller[cid].chip[i].amp_mask&(j+1)) {
	full_controller_ampmask |= mask_from_bitpos(namps);
	namps++;
	if (namps>MAX_AMPS) {
	  ostr << "Error: Too many amps specified for controller, only " << MAX_AMPS << "supported!" << ends;
	  throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);
	}
      }
    }
  }

  // Now allocate space for storing amp specifications
  amp_xo = new unsigned long[namps];
  amp_yo = new unsigned long[namps];
  amp_prescan = new unsigned long[namps];
  amp_postscan = new unsigned long[namps];
  amp_width = new unsigned long[namps];
  amp_height = new unsigned long[namps];
  amp_xdir = new int[namps];
  amp_ydir = new int[namps];
  amp_orient = new Readout_Orientation[namps];
  amp_chipnum = new int[namps];
  amp_ampnum = new int[namps];
  amp_list = new int[namps];

  na = 0;
  // Now loop through again and assign values from configuration
  for (i=0;i<camera.controller[cid].n_chips;i++) {
    for (j=0;j<camera.controller[cid].chip[i].n_amps;j++) { 

      // Only use this amp if configured in hardware mask
      if (camera.controller[cid].chip[i].amp_mask&(j+1)) {

	// the things in the config files are in CHIP coords,
	// we want them to be in controller coords.
	amp_xo[na] = camera.controller[cid].chip[i].amp[j].xo+camera.controller[cid].chip_xo[i];         
	amp_yo[na] = camera.controller[cid].chip[i].amp[j].yo+camera.controller[cid].chip_yo[i];
	amp_prescan[na] = camera.controller[cid].chip[i].amp[j].prescan;
	amp_postscan[na] = camera.controller[cid].chip[i].amp[j].postscan;
	amp_width[na] = camera.controller[cid].chip[i].amp[j].width;
	amp_height[na] = camera.controller[cid].chip[i].amp[j].height;
	amp_xdir[na] = camera.controller[cid].chip[i].amp[j].xdir;
	amp_ydir[na] = camera.controller[cid].chip[i].amp[j].ydir;
	amp_orient[na] = camera.controller[cid].chip[i].amp[j].orient;
	amp_chipnum[na] = i;
	amp_ampnum[na] = j;
	na++;
      }
    }
  }

  // if >1 amp, check that all amps are the same size
  if (namps>1) {
    for (i=1; i<namps; i++) {
      if ((amp_width[i] != amp_width[i-1]) ||
	  (amp_height[i] != amp_height[i-1])) {
	ostr << "Error: Invalid amplifier config! Sizes for amplifiers " << i << " and " << i-1 << " do not match!" << ends;	
	throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
      }
    } 
  }
}

/*
 *+ 
 * FUNCTION NAME:  Detector_Regions::~Detector_Regions()
 *
 * INVOCATION: delete rgn
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: void
 *
 * PURPOSE: Simple destructor - does nothing
 *
 * DESCRIPTION: cleans up any allocarted working space
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Detector_Regions::~Detector_Regions() 
{
  cleanup_arrays();
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::print_amp_specs()
 *
 * INVOCATION: print_amp_specs()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *  
 * FUNCTION VALUE: void
 *
 * PURPOSE: Prints on standard output a list of current amp readout specs
 *
 * DESCRIPTION: 
 *   loop through amplifiers, printing out geometry - used for debugging
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Detector_Regions::print_amp_specs() 
{
  int i;
  cout <<"Amplifier config:\nN\t x\t y\t w\t h\t xdir\t ydir\n";
  for (i=0;i<namps;i++) {
    cout << i <<"\t"<< amp_xo[i] <<"\t"
	 << amp_yo[i]<<"\t"<< amp_width[i]<<"\t"<<amp_height[i]<<"\t"
	 <<amp_xdir[i]<<"\t"<<amp_ydir[i]<<endl ;
  };
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::check_amp_config()
 *
 * INVOCATION: 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: void
 *
 * PURPOSE: check for a valid amplifier config
 *
 * DESCRIPTION: 
 *   compare each amplifier geometry to every other amplifier geometry,
 *   and check they dont overlap. Throw an exception if an overlap is found.
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Detector_Regions::check_amp_config()
{
  int i,j,ok=1;
  int xmin,xmax,ymin,ymax;
  ostringstream ostr;
  bool have_col_orientation = false;
  
  if (debug) { 
    cout <<"checking amplifier geom:";
  }

  for (i=0; i<namps; i++) {
    for (j=0; j<namps; j++) {
      if (i != j) {
	if (amp_xo[i]>amp_xo[j]) {
	  xmin = MIN(amp_xo[i]+amp_xdir[i]*(amp_width[i]-1),amp_xo[i]);
	  xmax = MAX(amp_xo[j]+amp_xdir[j]*(amp_width[j]-1),amp_xo[j]);
	} else {
	  xmin = MIN(amp_xo[j]+amp_xdir[j]*(amp_width[j]-1),amp_xo[j]);
	  xmax = MAX(amp_xo[i]+amp_xdir[i]*(amp_width[i]-1),amp_xo[i]);
	}
	if (amp_yo[i]>amp_yo[j]) {
	  ymin = MIN(amp_yo[i]+amp_ydir[i]*(amp_height[i]-1),amp_yo[i]);
	  ymax = MAX(amp_yo[j]+amp_ydir[j]*(amp_height[j]-1),amp_yo[j]);
	} else {
	  ymin = MIN(amp_yo[j]+amp_ydir[j]*(amp_height[j]-1),amp_yo[j]);
	  ymax = MAX(amp_yo[i]+amp_ydir[i]*(amp_height[i]-1),amp_yo[i]);
	}
	
	// overlap test: overlap if both x and y overlap
	ok = ((xmin>xmax) || (ymin>ymax));
	if (!ok) {
	  ostr << "Invalid amplifier configuration! Offending configuration was:\nController: " << camera.controller[cid].name << 
	    "\nCHIP: " << camera.controller[cid].chip[amp_chipnum[i]].name << "\nAmp: " << i << " and " << j << " overlap" << ends;
	  throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
	}
	
	// Remember if we have column oriented amps
	if (amp_orient[i] == ORIENT_COL)
	  have_col_orientation = true;
	
	// could also test that min-max==multiple of amp size
	// should be done in the future. maybe. if we're bored.
      }
    }
  }

  // If we have any column oriented amps check that the amps are square.
  // This is a current constraint of the regions class wrt column orientation
  if (have_col_orientation)
    for (i=0; i<namps; i++)
      if ((amp_height[i] != amp_width[i]) || 
	  ((i>0) && (amp_height[i] != amp_height[i-1]))) {
	ok = 0;
	ostr << "Invalid amplifier configuration! If you have any column oriented amps\n" << 
	        "they must be square and all the same. Offending configuration is:\nController: " << 
	  camera.controller[cid].name << "\nCHIP: " << camera.controller[cid].chip[amp_chipnum[i]].name << "\nAmp: " << i << ends;
	throw Error(ostr, E_ERROR,-1,__FILE__,__LINE__);
      }
  
  if (ok && debug) { 
    cout << " OK." << endl;
  }
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::check_subregions_allowed()
 *
 * INVOCATION: check_subregions_allowed()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *  none
 *
 * FUNCTION VALUE: 
 * void
 *
 * PURPOSE: 
 *   Checks if we have both row and column oriented readouts, and if
 *   this is the case, sub-regions are not allowed.
 *
 * DESCRIPTION: 
 *   First check for mixed readouts, and if this is the case, make
 *   sure that there are no subregions. If we have a subregion, throw
 *   an error.
 *
 * EXTERNAL VARIABLES: 
 *   
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Detector_Regions::check_subregions_allowed() 
{

  bool have_mixed_orientations=false;
  int i,j;
  ostringstream ostr;

  for (i=0; i<namps; i++)
    for (j=i+1; j<namps; j++)
      if (amp_orient[i] != amp_orient[j])
	have_mixed_orientations = true;
	  
  if (have_mixed_orientations) {
    if ((nregions != 1) || (reg_xo[0] != 0L) || (reg_yo[0] != 0L) || 
	(reg_width[0] != (unsigned long) camera.controller[cid].cols) || (reg_height[0] != (unsigned long) camera.controller[cid].rows) ) {
      ostr << "Subregions are not supported when mixing row\n and column-oriented chips\n" << ends;
      throw Error(ostr, E_ERROR,-1,__FILE__,__LINE__);		
    }
  }
}
/*
 *+ 
 * FUNCTION NAME: Detector_Regions::align_regions_with_binning()
 *
 * INVOCATION: align_regions_with_binning()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *  none
 *
 * FUNCTION VALUE: 
 * void
 *
 * PURPOSE: 
 *   Make sure region size and position are binning aligned
 *
 * DESCRIPTION: 
 *  To help prevent rounding problems this routine forces requested regions to 
 * be aligned with chosen binning factors. If this isn't done, then the readout
 * sequnce size calculations could contain rounding errors causing missing
 * data effects in the final output frames.
 *
 * EXTERNAL VARIABLES: 
 *   
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Detector_Regions::align_regions_with_binning() 
{

  int i;

  if (rcf>1) { 
    for (i=0; i<nregions; i++) {
      reg_xo[i] = reg_xo[i]/rcf*rcf;
      reg_width[i] = (reg_width[i]%rcf)? (reg_width[i]/rcf+1)*rcf:reg_width[i];
    }
  }

  if (ccf>1) {
    for (i=0; i<nregions; i++) {
      reg_yo[i] = reg_yo[i]/ccf*ccf;
      reg_height[i] = (reg_height[i]%ccf)? (reg_height[i]/ccf+1)*ccf:reg_height[i];
    }
  }
}
/*
 *+ 
 * FUNCTION NAME: Detector_Regions::point_in_region
 *
 * INVOCATION: point_in_region(int r, int a, int x, int y)  
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > r - region index
 * > a - amp index
 * > x - x position of pixel
 * > y - y position of pixel
 *
 * FUNCTION VALUE: int
 *
 * PURPOSE: 
 *  function to test whether point x,y falls inside region r (on amp a)
 * 
 * DESCRIPTION: 
 *   
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *- 
 */
int Detector_Regions::point_in_region(int r, int a, long x, long y)
{
  if (camera_coords) {
    // Translate point to camera coordinates - it is in amp coords.
    x = (amp_xdir[a]>0)? x + amp_xo[a] : (long)amp_xo[a] - x;
    x += camera.cont_xo[cid];
    y = (amp_ydir[a]>0)? y + amp_yo[a] : (long)amp_yo[a] - y;
    y += camera.cont_yo[cid];
  }
  return (((unsigned long)x<=reg_xo[r]+reg_width[r]-1) && ((unsigned long)x>=reg_xo[r]) &&
	  ((unsigned long)y<=reg_yo[r]+reg_height[r]-1) && ((unsigned long)y>=reg_yo[r]));
}
/*+ 
 * FUNCTION NAME: Detector_Regions::do_region_loop
 *
 * INVOCATION: do_region_loop(int ampi, bool handling_data_section)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > ampi - index of amp being considered
 * > handling_data_section - set if we are looking for data regions - otherwise we are looking for overscan
 *
 * FUNCTION VALUE: 
 * void
 *
 * PURPOSE: 
 *   Loops through each user specified region to determine whether the region
 *  appears in the current amp being processed and sets coordinates of a new
 *  frame.
 * 
 * DESCRIPTION: 
 *   . translates camera coordinates to controller coordinates
 *   . checks bounds of region to see if intersects amp bounds and if so
 *     . increments frame count
 *     . determines the x and y coords of the amp pixels that are to be clocked
 *     . sets a bit in an x and y amp mask for each pixel needed corresponding to this amp
 *     . sets a bit in an x and y region mask for each pixel needed corresponding to this region
 *     . sets frame dimension public member variables that can be used externally
 *   
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *- 
 */
void Detector_Regions::do_region_loop(int ampi, bool handling_data_section)
{
  int region,i;
  long left,bottom,right,top;
  ostringstream ostr;
  long *ampcoo_xo, *ampcoo_yo;          //+ amp coordinates origins
  long *ampcoo_xsize, *ampcoo_ysize;    //+ amp coordinates sizes
  unsigned long amp_minx;               //+ min bound of current amp
  unsigned long amp_miny;
  unsigned long amp_maxx;               //+ max bound of current amp
  unsigned long amp_maxy;

  try {

    ampcoo_xo = new long[namps];
    ampcoo_yo = new long[namps];
    ampcoo_xsize = new long[namps];
    ampcoo_ysize = new long[namps];

    // now loop thru each region
    for (region=0; region<nregions; region++) {
      
      // Region coordinates are in camera units - translate to controller units
      // These are converted to pixel offset units in controcller coordinates
      left = reg_xo[region];
      bottom = reg_yo[region]; 
      if (camera_coords) {
	left -= camera.cont_xo[cid];
	bottom -= camera.cont_yo[cid];
      } else {
	left += camera.controller[cid].chip_xo[amp_chipnum[ampi]];
	bottom += camera.controller[cid].chip_yo[amp_chipnum[ampi]];
      }
      right = left+reg_width[region];
      top = bottom+reg_height[region];
      
      if (debug) {
	cout << "reg "<<region<< " minx " <<left<< " maxx " <<right-1
	     << " miny " <<bottom<< " maxy "<<top-1<<endl;
      }
      
      // Setup min and max coordinates for this amp
      // These are all unsigned - better hope that the region is configured
      // correctly or the subtraction will wrap.
      amp_maxx =  (amp_xdir[ampi]>0) ? amp_xo[ampi]+amp_width[ampi]-1 : amp_xo[ampi] ;
      amp_minx =  (amp_xdir[ampi]>0) ? amp_xo[ampi] : (long)amp_xo[ampi]-(long)amp_width[ampi]+1 ;
      amp_maxy =  (amp_ydir[ampi]>0) ? amp_yo[ampi]+amp_height[ampi]-1 : amp_yo[ampi] ;
      amp_miny =  (amp_ydir[ampi]>0) ? amp_yo[ampi] : (long)amp_yo[ampi]-(long)amp_height[ampi]+1 ;
      if (debug) {
	cout << "amp "<<ampi<< ": minx " <<amp_minx<< " maxx " <<amp_maxx
	     << " miny " <<amp_miny<< " maxy "<<amp_maxy<<endl;
      };
      
      // is this region in this amp?
      if ((right-1>=(long)amp_minx) && (left<=(long)amp_maxx) &&
	  (top-1>=(long)amp_miny) && (bottom<=(long)amp_maxy) ) {
	if (debug) {
	  // we have overlap!
	  cout << "overlap with region "<< region << " and amplifier "<< amp <<endl;
	}
	
	// set amp offsets and sizes
	ampcoo_xo[ampi] = (amp_xdir[ampi]<0) ? (long)amp_xo[ampi]-right+1 : left-(long)amp_xo[ampi];
	ampcoo_yo[ampi] = (amp_ydir[ampi]<0) ? (long)amp_yo[ampi]-top+1 : bottom-(long)amp_yo[ampi];
	ampcoo_xsize[ampi] = right-left;
	ampcoo_ysize[ampi] = top-bottom;
	
	if (debug) {
	  cout << "amp coords before bounds check:"<<endl;
	  cout << "ampcoo_xo[ampi]: " <<ampcoo_xo[ampi] <<" ampcoo_yo[ampi]: "
	       <<ampcoo_yo[ampi]<<" amp_coord_xsize: "<<ampcoo_xsize[ampi]
	       <<" amp_coord_ysize: "<<ampcoo_ysize[ampi]<<endl;
	}
	
	// do a bounds check
	if (ampcoo_xo[ampi]<0) {
	  ampcoo_xsize[ampi] = ampcoo_xo[ampi]+ampcoo_xsize[ampi];
	  ampcoo_xo[ampi] = 0;
	}
	if (ampcoo_yo[ampi]<0) {
	  ampcoo_ysize[ampi] = ampcoo_yo[ampi]+ampcoo_ysize[ampi];
	  ampcoo_yo[ampi] = 0;
	}
	
	if ((unsigned long)(ampcoo_xo[ampi]+ampcoo_xsize[ampi])>amp_width[ampi]) {
	  ampcoo_xsize[ampi] = (long)amp_width[ampi]-ampcoo_xo[ampi];
	}
	if ((unsigned long)(ampcoo_yo[ampi]+ampcoo_ysize[ampi])>amp_height[ampi]) {
	  ampcoo_ysize[ampi] = (long)amp_height[ampi]-ampcoo_yo[ampi];
	}
	
	// sanity check on sizes
	if ((ampcoo_xsize[ampi]<0) || (ampcoo_ysize[ampi]<0)) {
	  ostr << " Error! amp coord sizes are negative! " << reg_xo[ampi] << "," << reg_yo[ampi] << "," << 
	    reg_width[ampi] << "," << reg_height[ampi] << ends;
	  throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
	}
	if (debug) {	
	  cout << "amp coords after bounds check:" << endl;
	  cout << "amp: "<< amp <<" region: " << region << " xo:"<< ampcoo_xo[ampi]
	       << " yo:"<< ampcoo_yo[ampi] << " xsize:" << ampcoo_xsize[ampi]
	       << " ysize:" << ampcoo_ysize[ampi] << endl;
	}
	

	if (handling_data_section) {
	  // for x and y, add this subregion to the amplifier 
	  // serial/parallel sections.
	  for (i=ampcoo_xo[ampi]; i<(ampcoo_xo[ampi]+ampcoo_xsize[ampi]); i++) {
	    xamp_bits[i] |= current_amp_mask;
	    if (mask_from_bitpos(amp_chipnum[ampi]) & save_chip_mask) {
	      xframe_bits[i] |= mask_from_bitpos(nsframes);
	      xreg_bits[i] |= mask_from_bitpos(region);
	    }
	  }
	  for (i=ampcoo_yo[ampi]; i<ampcoo_yo[ampi]+ampcoo_ysize[ampi]; i++) {
	    yamp_bits[i] |= current_amp_mask;
	    if (mask_from_bitpos(amp_chipnum[ampi]) & save_chip_mask) {
	      yframe_bits[i] |= mask_from_bitpos(nsframes);
	      yreg_bits[i] |= mask_from_bitpos(region);
	    }
	  }
	  
	  // If this chip is included in the readout - setup the output file frame
	  // for the current amp.
	  if (mask_from_bitpos(amp_chipnum[ampi]) & save_chip_mask) {
	    
	    setup_output_subframe(ampi, region, ampcoo_xo[ampi], ampcoo_yo[ampi], ampcoo_xsize[ampi], ampcoo_ysize[ampi], 
				  mosaic_mode, RAW_DATA);
	    data_frames_mask |= mask_from_bitpos(nsframes);
	    nsframes++;
	    
	  }

	} else { // handling overscan

	  // Add new frames for overscan rows - if not appending to existing frames
	  if (overscan_rows && !append_overscan) {
	    if (mask_from_bitpos(amp_chipnum[ampi]) & save_chip_mask) {
 	      for (i=ampcoo_xo[ampi]; i<(ampcoo_xo[ampi]+ampcoo_xsize[ampi]); i++)
 		xframe_bits[i] |= mask_from_bitpos(nsframes);
	      for (i=(int)amp_height[ampi]; i<(int)(amp_height[ampi]+overscan_rows); i++)
		yframe_bits[i] |= mask_from_bitpos(nsframes);
	      
	      setup_output_subframe(ampi, region, ampcoo_xo[ampi], 0, ampcoo_xsize[ampi], overscan_rows, MOSAIC_NONE, REF_DATA);
	      nsframes++;
	    }
	  }
	  
	  // Add new frames for overscan cols - if not appending to existing frames
	  if (overscan_cols && !append_overscan) {
	    if (mask_from_bitpos(amp_chipnum[ampi]) & save_chip_mask) {
 	      for (i=ampcoo_yo[ampi]; i<(ampcoo_yo[ampi]+ampcoo_ysize[ampi]); i++)
		yframe_bits[i] |= mask_from_bitpos(nsframes);
	      for (i=(int)amp_width[ampi]; i<(int)(amp_width[ampi]+overscan_cols); i++)
		xframe_bits[i] |= mask_from_bitpos(nsframes);
	      
	      setup_output_subframe(ampi, region, 0, ampcoo_yo[ampi], overscan_cols, ampcoo_ysize[ampi], MOSAIC_NONE, REF_DATA);
	      nsframes++;
	    }
	  }
	}
      }         
    }
  }
  catch (Error& e) {
    // Delete temp vars
    delete [] ampcoo_xo;
    delete [] ampcoo_yo;
    delete [] ampcoo_xsize;
    delete [] ampcoo_ysize;
    
    e.rethrow();
  }
  // Delete temp vars
  delete [] ampcoo_xo;
  delete [] ampcoo_yo;
  delete [] ampcoo_xsize;
  delete [] ampcoo_ysize;
 
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::setup_output_subframe
 *
 * INVOCATION: setup_output_subframe(int ampi, int region, long ampcoo_xo, long ampcoo_yo, long ampcoo_xsize, 
 *				     long ampcoo_ysize, Mosaic_Mode mm, Image_Data_Type dt)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ampi - amp to place in subframe structure
 * > region - region number
 * > ampcoo_xo - amp coordinates x origin
 * > ampcoo_yo - amp coordinates y origin
 * > ampcoo_xsize - amp coordinates x size
 * > ampcoo_ysize - amp coordinates y size
 * > mm - mosaic mode to use while creating output frame
 * > dt - type of data in this frame
 *
 * FUNCTION VALUE: void
 *
 * PURPOSE: 
 *  Fills a subframe structure with detail of this readout amp. Places 
 * subframe in appropriates output frame structure
 * 
 * DESCRIPTION: 
 *
 * The regions class needs to provide detail of how data from each amp will be
 * placed in the output file. This is accomplished using the hierarchical
 * frame/subframe data structures. Each main frame represents one output file
 * extension, which, in turn , is made up of a number of subframes within that
 * extension. Depending on how data is required to be saved - either at the
 * individual amp, chip or mosaic level there will be a different composition of
 * each main frame. In the case of:
 * 1) 1 amp per extension - no mosaicing of data on output is desired. This is
 * most primitive and most natural level but means as many extensions as there
 * are output amps.
 * 2) 1 chip per extension - mosaicing is desired at the amp level. All amps are
 * mosaiced into a single extension. This may be desirable in the case where
 * there is little need to preserve amp level detail and therefore reduce the
 * number of ouput extensions.
 * 3) 1 mosaic per extension - mosaicing is required at the chip level. All
 * chips are mosaiced into a single extension. This is usually not done as
 * information about each chip will be lost on output
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *- */
void Detector_Regions::setup_output_subframe(int ampi, int region, long ampcoo_xo, long ampcoo_yo, long ampcoo_xsize, 
					     long ampcoo_ysize, Mosaic_Mode mm, Image_Data_Type dt)
{
  Sub_Frame_Spec subframe;
  unsigned long prescan, postscan;
  unsigned long osr, osc, osr_binned, osc_binned;

  // Is overscan to be appended to the data frame?
  osr = osc = osr_binned = osc_binned = 0;
  if (append_overscan) {
    osr = overscan_rows;
    osc = overscan_cols;
    osr_binned = (unsigned long)ceil(((float)overscan_rows)/ccf);
    osc_binned = (unsigned long)ceil(((float)overscan_cols)/rcf);
  }

  // set public access member variables
  // frame origin in amplifier coords
  subframe.xo = ampcoo_xo;
  subframe.yo = ampcoo_yo;
  
  // compute frame width sizes with/without binning and overscan
  subframe.data_width = ampcoo_xsize;
  subframe.width = ampcoo_xsize + osc;
  subframe.data_width_binned = (unsigned long)ceil(((float)subframe.data_width)/rcf);
  subframe.width_binned = subframe.data_width_binned + osc_binned;
  
  // Trim width is width of region less exposed pre and post scan pixels - binned.
  prescan = MAX(0,(long)amp_prescan[ampi]-ampcoo_xo);
  postscan = MAX(0,(ampcoo_xo+ampcoo_xsize)-((long)amp_width-(long)amp_postscan[ampi]));
  subframe.prescan_binned = (unsigned long)ceil(((float)prescan)/rcf);
  subframe.postscan_binned = (unsigned long)ceil(((float)postscan)/rcf);
  subframe.trim_width = (unsigned long)floor(((float)subframe.data_width - prescan - postscan)/rcf);
  
  // compute frame height sizes with/without binning and overscan
  subframe.data_height = ampcoo_ysize;
  subframe.height = ampcoo_ysize + osr;
  subframe.data_height_binned = (unsigned long)ceil(((float)subframe.data_height)/ccf);
  subframe.height_binned = subframe.data_height_binned + osr_binned;
  
  // Frame origin in amp coords- what is the offset to this frame on the chip
  subframe.amp_xo = camera.controller[cid].chip[amp_chipnum[ampi]].amp[amp_ampnum[ampi]].xo;
  subframe.amp_yo = camera.controller[cid].chip[amp_chipnum[ampi]].amp[amp_ampnum[ampi]].yo;
  subframe.amp_xo += (amp_xdir[ampi]==1)? subframe.xo: (1-subframe.xo-subframe.data_width);
  subframe.amp_yo += (amp_ydir[ampi]==1)? subframe.yo: (1-subframe.yo-subframe.data_height);

  // Frame origin in chip coords - what is the offset to this frame on the controller
  subframe.chip_xo = camera.controller[cid].chip_xo[amp_chipnum[ampi]] + subframe.amp_xo;
  subframe.chip_yo = camera.controller[cid].chip_yo[amp_chipnum[ampi]] + subframe.amp_yo;
  
  // frame origin in camera coords - what is the offset of this frame on the camera
  subframe.camera_xo = camera.cont_xo[cid] + subframe.chip_xo;
  subframe.camera_yo = camera.cont_yo[cid] + subframe.chip_yo;
  
  // frame readout direction and orientation
  subframe.xdir = amp_xdir[ampi];
  subframe.ydir = amp_ydir[ampi];
  subframe.orient = amp_orient[ampi];

  // Record compression factors
  subframe.rcf = rcf;
  subframe.ccf = ccf;

  // record overscan binned
  subframe.overscan_rows_binned = osr_binned;   //+ overscan rows binned
  subframe.overscan_cols_binned = osc_binned;   //+ overscan cols binned
	  
  // amplifier,region and chipnum this frame is in
  subframe.amp = amp;
  subframe.reg = region;
  subframe.chip = amp_chipnum[ampi];
  subframe.subframe_idx = nsframes;
  
  if (debug) {
    cout<<"frame xo yo width height amp# reg#:"<<subframe.xo<<" "<<
      subframe.yo<<" "<<subframe.width<<" "<< subframe.height<<" "<<subframe.amp<<" "<<
      subframe.reg<<endl;
  }

  // Add this subframe to the correct main frame
  add_subframe(subframe, ampi, mm, dt);

  // Also add to subframe vector
  subframes.push_back(subframe);
}

 /*
 *+ 
 * FUNCTION NAME: Detector_Regions::add_subframe
 *
 * INVOCATION: add_subframe(Sub_Frame_Spec& subframe, Mosaic_Mode mm, Image_Data_Type dt)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > subframe - subframe to add to frames vector
 * > mm - mosaicing mode
 *
 * FUNCTION VALUE: void
 *
 * PURPOSE: 
 *  Find main frame that this subframe belongs to and adds to vector
 * 
 * DESCRIPTION: 
 * Uses chosen mosaicing mode to select an output frame for this subframe.
 *
 * The regions class needs to provide detail of how data from each amp will be
 * placed in the output file. This is accomplished using the hierarchical
 * frame/subframe data structures. Each main frame represents one output file
 * extension, which, in turn , is made up of a number of subframes within that
 * extension. Depending on how data is required to be saved - either at the
 * individual amp, chip or mosaic level there will be a different composition of
 * each main frame. In the case of:
 * 1) 1 amp per extension - no mosaicing of data on output is desired. This is
 * most primitive and most natural level but means as many extensions as there
 * are output amps.
 * 2) 1 chip per extension - mosaicing is desired at the amp level. All amps are
 * mosaiced into a single extension. This may be desirable in the case where
 * there is little need to preserve amp level detail and therefore reduce the
 * number of ouput extensions.
 * 3) 1 mosaic per extension - mosaicing is required at the chip level. All
 * chips are mosaiced into a single extension. This is usually not done as
 * information about each chip will be lost on output
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *- 
 */
void Detector_Regions::add_subframe(Sub_Frame_Spec& subframe, int ampi, Mosaic_Mode mm, Image_Data_Type dt)
{
  int fi;
  Amp_Frame_Map::iterator fit;

  switch (mm) {
  case MOSAIC_NONE: 
    // Each frame has just one subframe - frame count is the frame index, increment
    fi = nframes;
    nframes++;
    frames[fi].width = subframe.width;
    frames[fi].height = subframe.height;
    frames[fi].width_binned = subframe.width_binned;
    frames[fi].height_binned = subframe.height_binned;
    frames[fi].data_width = subframe.data_width;
    frames[fi].data_height = subframe.data_height;
    frames[fi].data_width_binned = subframe.data_width_binned;
    frames[fi].data_height_binned = subframe.data_height_binned;
    frames[fi].camera_xo = subframe.camera_xo;
    frames[fi].camera_yo = subframe.camera_yo;
    subframe.frame_xo = 0;
    subframe.frame_yo = 0;
    break;
  case MOSAIC_AMP: 
    // The frame index is mapped to the chip index - 
    // make sure we haven't encountered this chip before
    if ((fit = amp_frame_map.find(subframe.chip)) != amp_frame_map.end()) {
      // Yes - get the frame index from the map
      fi = (*fit).second;
    } else {
      // No - so add a new frame to the map
      fi = nframes;
      amp_frame_map[subframe.chip] = nframes;
      nframes++;
    }
    frames[fi].width = camera.controller[cid].chip[amp_chipnum[ampi]].cols;
    frames[fi].height = camera.controller[cid].chip[amp_chipnum[ampi]].rows;
    frames[fi].width_binned = (unsigned long)ceil(((float)camera.controller[cid].chip[amp_chipnum[ampi]].cols)/rcf);
    frames[fi].height_binned = (unsigned long)ceil(((float)camera.controller[cid].chip[amp_chipnum[ampi]].rows)/ccf);
    frames[fi].data_width = frames[fi].width;
    frames[fi].data_height = frames[fi].height;
    frames[fi].data_width_binned = frames[fi].width_binned;
    frames[fi].data_height_binned = frames[fi].height_binned;
    frames[fi].camera_xo = camera.cont_xo[cid] + camera.controller[cid].chip_xo[amp_chipnum[ampi]];
    frames[fi].camera_yo = camera.cont_yo[cid] + camera.controller[cid].chip_yo[amp_chipnum[ampi]];
    subframe.frame_xo = subframe.amp_xo;
    subframe.frame_yo = subframe.amp_yo;
    break;  
  case MOSAIC_CHIP: 
    // There is only one data frame - it is always the first
    fi = 0;
    nframes=1;
    frames[fi].width = camera.cols;
    frames[fi].height = camera.rows;
    frames[fi].width_binned = (unsigned long)ceil(((float)camera.cols)/rcf);
    frames[fi].height_binned = (unsigned long)ceil(((float)camera.rows)/rcf);
    frames[fi].data_width = frames[fi].width;
    frames[fi].data_height = frames[fi].height;
    frames[fi].data_width_binned = frames[fi].width_binned;
    frames[fi].data_height_binned = frames[fi].height_binned;
    frames[fi].camera_xo = 0;
    frames[fi].camera_yo = 0;
    subframe.frame_xo = subframe.camera_xo;
    subframe.frame_yo = subframe.camera_yo;
    break;
  default:
    throw Error("Unhandled Mosaic mode!", E_ERROR, EINVAL, __FILE__, __LINE__);
  }

  // Use appropriate index to deposit values in map
  subframe.frame = fi;
  subframe.subframe = frames[fi].subframes.size();
  frames[fi].data_type = dt;
  frames[fi].subframes.push_back(subframe);       
}
/*
 *+ 
 * FUNCTION NAME: void Detector_Regions::update_framelist
 *
 * INVOCATION: update_framelist(unsigned long long fm, int this_amp, int this_ser, Image_Data_Type dt)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > fm - current framemask
 * > this_amp - current amp
 * > this_ser - current serial region
 * > dt - data type being processed
 *
 * FUNCTION VALUE: void
 *
 * PURPOSE: 
 *  Updates framelist & subframelist array with mask of frames in current framemask for amp
 * 
 * DESCRIPTION: 
 * . Loops over current framemask
 * . if frame is on this amp bitor to subframelist 
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *- 
 */
void Detector_Regions::update_framelist(unsigned long long fm, int this_amp, int this_ser, Image_Data_Type dt)
{
  int sf;

  for (sf=0; ((fm>0)&&(sf<nsframes)); sf++) {		  

    // test for regular data pixels, ie point inside frame or in overscan and 
    // amp active for this frame
    if ((fm & 1) && (subframes[sf].amp==this_amp) && 
	((frames[subframes[sf].frame].data_type == dt) || ((dt==REF_DATA) && append_overscan))) {
      if (debug) {
	cout << "found frame " << sf+1 << " for amp,par,ser=" <<
	  this_amp << npar << this_ser << endl;
      }

      // The subframelist array is a mask that shows which frame this
      // amp/par region/ser region appear in
      if (debug)
        cout << "subframelist was " << par_regions[npar].ser_regions[this_ser].subframelist[this_amp] << endl;
      par_regions[npar].ser_regions[this_ser].subframelist[this_amp] |= mask_from_bitpos(sf); // frame numbers start from 1
      if (debug)
        cout << "subframelist is now " << par_regions[npar].ser_regions[this_ser].subframelist[this_amp] << endl;
      par_regions[npar].ser_regions[this_ser].framelist[this_amp] |= mask_from_bitpos(subframes[sf].frame); // frame numbers start from 1
      if (debug)
	cout << "assigning subframelist...." <<endl;
    }
    fm >>= 1;
  } // end for f loop
}
/*
 *+ 
 * FUNCTION NAME: Detector_Regions::find_serial_regions
 *
 * INVOCATION: find_serial_regions(Image_Data_Type dt)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > dt- type of data in this parallel region
 *
 * FUNCTION VALUE: 
 * void
 *
 * PURPOSE: 
 *  Adds serial region information to the parallel region array
 * 
 * DESCRIPTION: 
 * . Adds a serial regions to the parrallel region
 * . Updates subframelist array 
 * . increments serial region counter
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *- 
 */
void Detector_Regions::find_serial_regions(Image_Data_Type dt)
{
  unsigned long col;
  unsigned long long xregmask,xampmask,amptemp_mask,framemask,old_framemask;
  unsigned long inser,ser_start,sum_width,sum_width_b;
  int i,k,r,amps;
  ostringstream ostr;

  // find serial regions by comparing regions bits of par regions.
  // to those in ser regions.	NB all amps MUST be same size
  old_framemask = 0;
  nser = inser = ser_start = sum_width = sum_width_b = 0;
  for (col=0; col<amp_width[0]; col++) {
    xampmask = xamp_bits[col];
    xregmask = xreg_bits[col];
    framemask = xframe_bits[col]&yframemask;

    // Check to see if we change bitmasks at this col. Indicating a 
    // serial region change
    if (framemask != old_framemask) {

      // Were we in a serial region in previous col? If so finish it up
      if (old_framemask > 0) {
	if (debug) {
	  cout <<"\tserial  ends  at "<< col-1 << endl;
	}
	par_regions[npar].ser_regions[inser].width = col-ser_start;  
	sum_width += par_regions[npar].ser_regions[inser].width;

	// Compute binned width for this ser region
	if (framemask>0) {// internal to frame - round down
	  par_regions[npar].ser_regions[inser].width_b = (col-ser_start)/rcf;
	  sum_width_b += par_regions[npar].ser_regions[inser].width_b;
	} else {
	  // end of frame - set width so that sum of ser widths = full frame width
	  par_regions[npar].ser_regions[inser].width_b = (unsigned long)ceil(((float)sum_width)/rcf) - sum_width_b;
	  sum_width = sum_width_b = 0;
	}
      }

      // Now check to see if we are entering a new serial region, if so set it up
      if (framemask > 0) {	     
	if (debug) {
	  cout <<"\tserial starts at "<< col <<endl;
	}
	
	if (nser+1>MAX_REGIONS) {
	  ostr << "Error: Region geometry is too complicated. Max number of serial regions exceeded!" << ends;
	  throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
	}	    
	
	// Mark this serial region as the one we are in now - for when detecting
	// end of serial region
	inser = nser;
	ser_start = col;
	
	// Allocate new space for another serial region, copy old data to new space
	// A bit inefficient but heck a lot simpler than handling with a list or something
	// for ser_regions because of original design (ie was a fixed sized array...)
	Ser_Regions *tser = new Ser_Regions[nser + 1];
	if (nser > 0) {
	  for (i=0; i<nser; i++)
	    tser[i] = par_regions[npar].ser_regions[i];
	  delete [] par_regions[npar].ser_regions;
	}
	par_regions[npar].ser_regions = tser;	
	par_regions[npar].ser_regions[nser].framelist = new unsigned long long[namps];
	par_regions[npar].ser_regions[nser].subframelist = new unsigned long long[namps];
	par_regions[npar].ser_regions[nser].regionlist = new unsigned long long[namps];
	memset((void*)par_regions[npar].ser_regions[nser].framelist, 0, sizeof(unsigned long long)*namps);
	memset((void*)par_regions[npar].ser_regions[nser].subframelist, 0, sizeof(unsigned long long)*namps);
	memset((void*)par_regions[npar].ser_regions[nser].regionlist, 0, sizeof(unsigned long long)*namps);
	
	// Update public par_regions array
	par_regions[npar].ser_regions[nser].xo = col;
	par_regions[npar].ser_regions[nser].xo_b = col/rcf;
	par_regions[npar].ser_regions[nser].amp_mask = xampmask&yampmask;
	par_regions[npar].ser_regions[nser].region_mask = xregmask&yregmask;
	
	// first ser_reg: skip==xo
	if (nser==0)
	  par_regions[npar].ser_regions[nser].skip = col;
	else //2nd or later ser_reg:skip=xo-(last_xo+last_width)
	  par_regions[npar].ser_regions[nser].skip = col-(par_regions[npar].ser_regions[nser-1].xo+
							  par_regions[npar].ser_regions[nser-1].width);  
	
	// loop over all frames looking for match with this ser/par region
	amptemp_mask = xampmask&yampmask;
	for (amps=0; amptemp_mask!=0; amps++) { // loop up till the highest amp num
	  amptemp_mask >>= 1;
	}
	
	// Now for each amp that is in this ser/par region update the subframelist mask
	for (k=0; k<amps; k++) {

	  //only look for amps in current selection masks
	  if ((mask_from_bitpos(amp_chipnum[k]) & save_chip_mask) && (xampmask & yampmask & mask_from_bitpos(k))) {

	    update_framelist(framemask, k, inser, dt);

	    // Set the region list array with masks for this pixel (serial region)
	    for (r=0; r<nregions; r++) {		  
	      if (point_in_region(r,k,col,row))
		par_regions[npar].ser_regions[inser].regionlist[k] |= mask_from_bitpos(r);
	    }
	  } // end if in ampmask condition
	} // end amp check loop
	
	// Increment serial region counter
	nser++;      
      } // if start serial region condition 

      // Update old framemask
      old_framemask = framemask;
    }
      
  } // end column loop
  
  // Now check for the end of a serial region, ie framemask non-zero
  if (old_framemask>0) {
    if (debug) {
      cout <<"\tserial  ends  at "<< col-1 << endl;
    }
    par_regions[npar].ser_regions[inser].width = col-ser_start;    
    sum_width += par_regions[npar].ser_regions[inser].width;
    par_regions[npar].ser_regions[inser].width_b = (unsigned long)ceil(((float)sum_width)/rcf) - sum_width_b;
  } // end if end of serial region condition
}
/*
 *+ 
 * FUNCTION NAME: Detector_Regions::handle_overscan_cols
 *
 * INVOCATION: handle_overscan_cols()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * none
 *
 * FUNCTION VALUE: void
 *
 * PURPOSE: 
 *  Sets up par_regions array element with serial overscan detail
 * 
 * DESCRIPTION: 
 * . Adds a serial overscan region to the parrallel region
 * . Updates subframelist array 
 * . increments serial region counter
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *   Way too complicated to describe.
 *- 
 */
void Detector_Regions::handle_overscan_cols()
{ 
  int i,k,r;

  if (debug) {
    cout << "\toverscan starts at " << amp_width[0] <<endl;
    cout << "\toverscan ends  at " << amp_width[0]+overscan_cols-1 <<endl;
  }
  if (debug) {
    cout<<"\tnserial="<<nser+1<<endl; 
  }

  // Allocate new space for another serial region, copy old data to new space
  // A bit inefficient but heck a lot simpler than handling with a list or something
  // for ser_regions because of original design (ie was a fixed sized array...)
  Ser_Regions *tser = new Ser_Regions[nser + 1];
  if (nser > 0) {
    for (i=0; i<nser; i++)
      tser[i] = par_regions[npar].ser_regions[i];
    delete [] par_regions[npar].ser_regions;
  }
  par_regions[npar].ser_regions = tser;	
  par_regions[npar].ser_regions[nser].framelist = new unsigned long long[namps];
  par_regions[npar].ser_regions[nser].subframelist = new unsigned long long[namps];
  par_regions[npar].ser_regions[nser].regionlist = new unsigned long long[namps];
  memset((void*)par_regions[npar].ser_regions[nser].framelist, 0, sizeof(unsigned long long)*namps);
  memset((void*)par_regions[npar].ser_regions[nser].subframelist, 0, sizeof(unsigned long long)*namps);
  memset((void*)par_regions[npar].ser_regions[nser].regionlist, 0, sizeof(unsigned long long)*namps);
	  
  par_regions[npar].ser_regions[nser].xo=amp_width[0];
  par_regions[npar].ser_regions[nser].skip =
    (long)amp_width[0] - ((long)par_regions[npar].ser_regions[nser-1].xo+
		    (long)par_regions[npar].ser_regions[nser-1].width);
  par_regions[npar].ser_regions[nser].width= overscan_cols;
  par_regions[npar].ser_regions[nser].width_b = (unsigned long)ceil(((float)overscan_cols)/rcf);

  // masks for the overscan region - all serial regions have same overscan
  par_regions[npar].ser_regions[nser].amp_mask = yampmask;
  par_regions[npar].ser_regions[nser].region_mask = yregmask;

  for (k=0; k<namps; k++) {
    if ((mask_from_bitpos(amp_chipnum[k]) & save_chip_mask) && (yampmask & mask_from_bitpos(k))) {

      update_framelist(yframemask, k, nser, REF_DATA);

      for (r=0; r<nregions; r++) {		  
	if (yregmask&mask_from_bitpos(r)) 
	  par_regions[npar].ser_regions[nser].regionlist[k] |= mask_from_bitpos(r);
      }
    }
  }
  nser++;
}
/*
 *+ 
 * FUNCTION NAME: Detector_Regions::find_par_regions
 *
 * INVOCATION: find_par_regions()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * none
 *
 * FUNCTION VALUE: void
 *
 * PURPOSE: 
 *  Detects unique parallel regions from spec
 * 
 * DESCRIPTION: 
 * . loops through all rows in amp
 * . detects start of parallel region from change in yamp pixel mask
 * . finds all serial regions for current par region once detected
 * . adds serial overscan if needed
 * . continues through rows until end of par region detected
 * . looks for next par region
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *- 
 */
void Detector_Regions::find_par_regions()
{
  unsigned int i;
  int j;
  unsigned long par_start;
  int inpar;
  unsigned long long old_yframemask;
  unsigned long sum_height,sum_height_b;
  Image_Data_Type data_type, old_data_type;
  ostringstream ostr;

  // Some debugging information
  if (debug>=2) { // this is very verbose for large chips
    cout <<endl;
    for (i=0;i<amp_width[0];i++) {
      cout << "xreg_bits["<<i<<"]= "<<xreg_bits[i]<<
	", xamp_bits["<<i<<"]= "<< xamp_bits[i]<<endl;
    }
    cout <<endl;
    for (i=0;i<amp_height[0]+overscan_rows;i++) {
      cout << "yreg_bits["<<i<<"]= "<<yreg_bits[i]<<
	", yamp_bits["<<i<<"]= "<< yamp_bits[i]<<endl;
    }
  }

  // Loop through each row of the amp. NB All amps are REQUIRED to be the same size
  // and overscan is the same for all amps
  par_start = npar = inpar = sum_height = sum_height_b = 0;
  old_yframemask = 0;
  old_data_type = RAW_DATA;

  for (row=0; row<amp_height[0]+overscan_rows; row++) {

    // check for start of par region by comparing with the ampmask of the prev par reg)
    yregmask = yreg_bits[row];    
    yampmask = yamp_bits[row]; 
    yframemask = yframe_bits[row];
    data_type = (row<amp_height[0])? RAW_DATA:REF_DATA;

    if (debug>2) {
      cout<<"loop thru par regs: row="<< row
	  <<" yregmask="<< yregmask<<" yampmask="<<yampmask<<" yframemask="<<yframemask<<endl;
    }

    // Check to see if we change bitmasks at this row. Indicating a frame change
    // Also change par regions if moving into overscan rows
    if ((yframemask != old_yframemask) || (old_data_type != data_type)) {

      // Were we in a par region in previous row? If so finish it up
      if (old_yframemask > 0) {
	if (debug) {
	  cout <<"   par ends at "<<row-1<<" , with ";
	  cout <<"yregmask="<<yregmask<<", yampmask="<< yampmask <<endl;
	}
	par_regions[inpar].height = row-par_start;
	sum_height += par_regions[inpar].height;

	// Calculate binned height
	if (yframemask > 0) {	
	  par_regions[inpar].height_b = (row-par_start)/ccf;
	  sum_height_b += par_regions[inpar].height_b;
	} else {

	  // end of frame - set height so that sum of par heights = full frame height
	  par_regions[inpar].height_b = (unsigned long)ceil(((float)sum_height)/ccf) - sum_height_b;
	  sum_height = sum_height_b = 0;
 	}

     }
      
      // Now check to see if we are entering a new par region, if so set it up
      if (yframemask > 0) {	
	if (debug) {cout <<"another par starts at "<<row<<" , with ";}
	if (npar+1>MAX_REGIONS) {
	  ostr << " Error: Region geometry is too complicated. Max number of parallel regions exceeded!" << ends;
	  throw Error(ostr,E_ERROR,-1,__FILE__,__LINE__);
	}
	// Mark start of parallel region
	inpar = npar;
	par_start=row;

	// Allocate new space for another parallel region, copy old data to new space
	// A bit inefficient but heck a lot simpler than handling with a list or something
	// for par_regions because of original design (ie was a fixed sized array...)
	Par_Regions *tpar = new Par_Regions[npar + 1];
	if (npar > 0) {
	  for (j=0; j<npar; j++)
	    tpar[j] = par_regions[j];
	  delete [] par_regions;
	}
	par_regions = tpar;	
	  
	// Update public member var par_regions with new info
	par_regions[npar].yo = row;
	par_regions[npar].yo_b = row/ccf;
	if (npar==0) {
	  par_regions[npar].skip = row;
	} else {
	  par_regions[npar].skip=row-(par_regions[npar-1].yo+par_regions[npar-1].height);
	}
	par_regions[npar].amp_mask = yampmask;
	par_regions[npar].region_mask = yregmask;

	// find serial regs for this par reg.
	find_serial_regions(data_type);
	
	// Now handle overscan columns
	if ((overscan_cols>0) && ((data_type==RAW_DATA) || append_overscan))
	  handle_overscan_cols();
	
	// Update par region with just found serial region info
	par_regions[npar].nser_regions = nser;
	par_regions[npar].remain = (overscan_cols==0)? 
	  (long)amp_width[0]-((long)par_regions[npar].ser_regions[nser-1].xo+(long)par_regions[npar].ser_regions[nser-1].width)
	  : 0;
	
	// Increment the par region count 
	npar++;
      } // if test for new par region

      // update old framemask
      old_yframemask = yframemask;
      old_data_type = data_type;
    } // if test for par region change
  } // for row=
  
  // Were we in a par region in previous row? If so finish it up
  if (old_yframemask > 0) {
    if (debug) {
      cout <<"   par ends at "<<row-1<<" , with ";
      cout <<"yregmask="<<yregmask<<", yampmask="<< yampmask <<endl;
    }
    par_regions[inpar].height = row-par_start;
    sum_height += par_regions[inpar].height;

    // Calculate binned height
    // end of frame - set height so that sum of par heights = full frame height
    par_regions[inpar].height_b = (unsigned long)ceil(((float)sum_height)/ccf) - sum_height_b;
  }
}
/*
 *+ 
 * FUNCTION NAME: Detector_Regions::chip_to_amp
 *
 * INVOCATION: chip_to_amp()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * none
 *
 * FUNCTION VALUE: void
 *
 * PURPOSE: 
 *   Given a region specification in controller coordinates, an amp readout 
 *   specification is produced according to given amplifier geometry.
 * 
 * DESCRIPTION: 
 *   Way too complicated to describe. Almost all the work is done here.
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *   Way too complicated to describe.
 *- 
 */
void Detector_Regions::chip_to_amp() 
{
  unsigned int ui;
  int i,j,f,np;
  unsigned long long temp_mask;  

  try {

    // xreg_bits is only width wide since oscols are added manually.
    // yreg_bits must extend out to the height including osrows so
    // it can be treated as any other parallel region.
    xreg_bits = new unsigned long long[amp_width[0]]; // always have >=1 amp,all are same size
    xamp_bits = new unsigned long long[amp_width[0]]; // always have >=1 amp,all are same size
    xframe_bits = new unsigned long long[amp_width[0]+overscan_cols]; // always have >=1 amp,all are same size
    yreg_bits = new unsigned long long[amp_height[0]+overscan_rows];
    yamp_bits = new unsigned long long[amp_height[0]+overscan_rows];
    yframe_bits = new unsigned long long[amp_height[0]+overscan_rows];

    memset((void *)xreg_bits,0,sizeof(unsigned long long)*amp_width[0]);
    memset((void *)xamp_bits,0,sizeof(unsigned long long)*amp_width[0]);
    memset((void *)xframe_bits,0,sizeof(unsigned long long)*(amp_width[0]+overscan_cols));
    memset((void *)yreg_bits,0,sizeof(unsigned long long)*(amp_height[0]+overscan_rows));
    memset((void *)yamp_bits,0,sizeof(unsigned long long)*(amp_height[0]+overscan_rows));
    memset((void *)yframe_bits,0,sizeof(unsigned long long)*(amp_height[0]+overscan_rows));

    
    if (debug) {
      cout << "in chip_to_amp\n"<<endl;
      cout <<"Amplifier config:\nN\t x\t y\t w\t h\t xdir\t ydir"<<endl;
      for (i=0;i<namps;i++) {
	cout << i <<"\t"<< amp_xo[i] <<"\t"
	     << amp_yo[i]<<"\t"<< amp_width[i]<<"\t"<<amp_height[i]<<"\t"
	     <<amp_xdir[i]<<"\t"<<amp_ydir[i]<<endl ;
      };
      cout <<"\nRegions:\nN\t x\t y\t w\t h"<<endl;
      for (i=0;i<nregions;i++) {
	cout << i<< "\t" <<reg_xo[i]<< "\t"
	     <<reg_yo[i]<< "\t" <<reg_width[i]<< "\t" <<reg_height[i]<<"\n"<<endl;
      };
    }

    nframes = nsframes = 0;
    current_amp_mask = 1;
    data_frames_mask = 0;

    // loop thru each amplifier, determine whether any pixels need to be read
    for (amp=0; amp<namps; amp++) {      

      // Is this amp in the clockout mask?
      if (current_amp_mask & clockout_chip_mask) {
       	
	// Process each user specified region, setting required pixel arrays
	// and frame public access variables
	do_region_loop(amp, true);
	
      }

      // Shift the mask to next position for next amp
      current_amp_mask = current_amp_mask << 1;    
    }

    // We have added all the data frames so set the data frame counters
    ndframes = nframes;
    ndsframes = nsframes;

    // setup parallel amplifier and region masks for the osrows stuff.
    // these are set to 0xffff, since they'll be anded (&) with the serial 
    // masks, and we want them to appear anywhere there is a serial region.
    for (ui=amp_height[0]; ui<(amp_height[0]+overscan_rows); ui++) {
      yreg_bits[ui] = FULL_MASK;
      yamp_bits[ui] = FULL_MASK;
      //if (append_overscan)
	yframe_bits[ui] = data_frames_mask;
    }

    // If not appending overscan - repeat the region loop looking for overscan
    if (!append_overscan && (overscan_cols || overscan_rows)) {
      current_amp_mask = 1;      

      // loop thru each amplifier, determine whether any pixels need to be read
      for (amp=0; amp<namps; amp++) {      
	
	// Is this amp in the clockout mask?
	if (current_amp_mask & clockout_chip_mask) {
	  
	  // Process each user specified region, setting required pixel arrays
	  // and frame public access variables
	  do_region_loop(amp, false);
	  
	}

	// Shift the mask to next position for next amp
	current_amp_mask = current_amp_mask << 1;    
      }
    }

    // now run through par and then ser regs looking for starts
    // and ends, and filling up the output structs
    find_par_regions();

    // calc total pix and serial width
    for (i=0;i<npar;i++) {
      par_regions[i].width = 0;
      for (j=0;j<par_regions[i].nser_regions;j++) {
	par_regions[i].width += par_regions[i].ser_regions[j].width;
      }
      if (debug) {
	cout<<"par_region_widths["<<i<<"]="<<par_regions[i].width<<endl;
      }
    }

    if (debug) {
      cout << "so, npar_regions=" << npar << endl;
      cout << "overscan cols = " << overscan_cols << endl;
      cout << "overscan rows = " << overscan_rows << endl;
    }	  
    if (debug) {
      for (f=0; f<(int)frames.size(); f++) {
	cout << "for frame " << f << " nsframes is " << frames[f].subframes.size() << endl;
	cout << "xo yo wid height amp reg" << endl;
	for (i=0; i<(int)frames[f].subframes.size(); i++) {
	  cout <<
	    frames[f].subframes[i].xo <<"  "<<  
	    frames[f].subframes[i].yo <<"  "<<
	    frames[f].subframes[i].width <<"  "<<
	    frames[f].subframes[i].height <<"    "<<
	    frames[f].subframes[i].amp <<"    "<<
	    frames[f].subframes[i].reg << endl;
	}
      }
    }

    // compute global amp mask
    global_amp_mask = 0;

    if (camera.controller[cid].mask_chips) {
      // Dont count overscan rows par region
      np = (overscan_rows>0)? npar-1:npar;
      for (i=0;i<np;i++) {
	global_amp_mask |= par_regions[i].amp_mask;
      }
    } else {
      global_amp_mask = full_controller_ampmask;
    }

    // set public member var
    npar_regions = npar;

    // map active amp mask to linear amp number
    // and calculate active amps
    temp_mask = global_amp_mask;
    nactive_amps = 0;
    for (i=0; temp_mask!=0; i++) { // loop up till the highest amp num
      if (temp_mask&1) {
	amp_list[nactive_amps] = i;
	nactive_amps++;
      }
      temp_mask >>= 1;
    } 
    if (debug) {
      for (i=0; i<nactive_amps; i++) {
	cout << "Amplist= " << amp_list[i] << " ";
      }
      cout << endl;
    }
  }
  catch(Error &e) {
    delete [] xreg_bits;
    delete [] yreg_bits;
    delete [] xamp_bits;
    delete [] yamp_bits;
    delete [] xframe_bits;
    delete [] yframe_bits;
    
    e.rethrow();
  }
  delete [] xreg_bits;
  delete [] yreg_bits;
  delete [] xamp_bits;
  delete [] yamp_bits;
  delete [] xframe_bits;
  delete [] yframe_bits;
  
};

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::get_frame_mask
 *
 * INVOCATION: get_frame_mask()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: unsigned long long
 *
 * PURPOSE: 
 *   Return the frame mask for a the region from the specified
 *   amp,par_region,ser_region.
 * 
 * DESCRIPTION: 
 *   Loop through framelist array, returning masks of elements that are 
 *   not equal to 0. chip_to_amp() will set the used framelist elements to
 *   the frame mask corresponding to the amp/ser/par indices.
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
unsigned long long Detector_Regions::get_frame_mask(int amp_no, int pr, int sr)
{
  // given amp number, serial # and par #, return frame # starting from 1.
  // Frames are numbered from 1, not zero!
  char msgbuf[MSGLEN];

  if (amp_no<100) {
    if ((amp_no>MAX_AMPS)||(pr>MAX_REGIONS)||(sr>MAX_REGIONS)) {
      snprintf(msgbuf, MSGLEN,"Error: Regions::get_frame_mask() out of bounds, amp=%d par_region=%d ser_region=%d", amp_no, pr, sr);
      throw Error(msgbuf,E_ERROR,-1,__FILE__,__LINE__);   
    }
    return par_regions[pr].ser_regions[sr].framelist[amp_no]; // frame numbers start from 1
  } else { // amp_no>100, debugging
    return par_regions[pr].ser_regions[sr].framelist[amp_no-100]; // frame numbers start from 1
  }
}
/*
 *+ 
 * FUNCTION NAME: Detector_Regions::get_subframe_mask
 *
 * INVOCATION: get_subframe_mask()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: unsigned long long
 *
 * PURPOSE: 
 *   Return the frame mask for a the region from the specified
 *   amp,par_region,ser_region.
 * 
 * DESCRIPTION: 
 *   Loop through subframelist array, returning masks of elements that are 
 *   not equal to 0. chip_to_amp() will set the used subframelist elements to
 *   the frame mask corresponding to the amp/ser/par indices.
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
unsigned long long Detector_Regions::get_subframe_mask(int amp_no, int pr, int sr)
{
  // given amp number, serial # and par #, return frame # starting from 1.
  // Frames are numbered from 1, not zero!
  char msgbuf[MSGLEN];

  if (amp_no<100) {
    if ((amp_no>MAX_AMPS)||(pr>MAX_REGIONS)||(sr>MAX_REGIONS)) {
      snprintf(msgbuf, MSGLEN,"Error: Regions::get_frame_mask() out of bounds, amp=%d par_region=%d ser_region=%d", amp_no, pr, sr);
      throw Error(msgbuf,E_ERROR,-1,__FILE__,__LINE__);   
    }
    return par_regions[pr].ser_regions[sr].subframelist[amp_no]; // frame numbers start from 1
  } else { // amp_no>100, debugging
    return par_regions[pr].ser_regions[sr].subframelist[amp_no-100]; // frame numbers start from 1
  }
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::get_region_mask
 *
 * INVOCATION: get_region_mask()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: unsigned long long - region mask
 *
 * PURPOSE: 
 *   Return the region mask for a pixel from the specified
 *   amp,par_region,ser_region.
 * 
 * DESCRIPTION: 
 *   Loop through regionlist array, returning masks of elements that are 
 *   not equal to 0. chip_to_amp() will set the used regionlist elements to
 *   the region numbers corresponding to the amp/ser/par indices.
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
unsigned long long Detector_Regions::get_region_mask(int amp_no, int pr, int sr)
{
  // given amp number, serial # and par #, return region mask starting from 1.
  // Regions are numbered from 1, not zero!
  char msgbuf[MSGLEN];
  
  if (amp_no<100) {
    if ((amp_no>MAX_AMPS)||(pr>MAX_REGIONS)||(sr>MAX_REGIONS)) {
      snprintf(msgbuf, MSGLEN,"Error: Regions::get_region_mask() out of bounds, amp=%d  par_region=%d ser_region=%d", amp_no, pr, sr);
      throw Error(msgbuf,E_ERROR,-1,__FILE__,__LINE__);   
    }
    return par_regions[pr].ser_regions[sr].regionlist[amp_no];
  } else { // amp_no>100, debugging
    return par_regions[pr].ser_regions[sr].regionlist[amp_no-100];
  }
}
/*
 *+ 
 * FUNCTION NAME: Detector_Regions::get_amp
 *
 * INVOCATION: get_amp(int amp_no)
 *   
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > amp_no - index of amp that address is required for
 *
 * FUNCTION VALUE: int - amp address 
 *
 * PURPOSE: 
 *   returns linear address of amplifier number for the current
 *   global_amp_mask.
 *
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
int Detector_Regions::get_amp(int amp_no) 
{  
  char msgbuf[MSGLEN];
  
  if (amp_no<MAX_AMPS) {
    return amp_list[amp_no];
  } else {
    snprintf(msgbuf, MSGLEN, "Regions::get_amp() called with absurd amplfier number:%d",amp_no);
    throw Error(msgbuf,E_ERROR,-1,__FILE__,__LINE__);
  }
}


/*
 *+ 
 * FUNCTION NAME: Detector_Regions::amp_to_chip
 *
 * INVOCATION: amp_to_chip()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: void
 *
 * PURPOSE: 
 *  return controller coords of a given amplifier region.
 *
 * DESCRIPTION: pixels in amplifier coords are converted to chip coordinates.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *   does nothing yet. Not required??
 *- 
 */
void Detector_Regions::amp_to_chip() 
{

};

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::read_regs
 *
 * INVOCATION: read_regs()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: void
 *
 * PURPOSE: 
 *  Read region descriptions from file - formatted using ximtool marker
 *  conventions
 * 
 * DESCRIPTION: 
 *   calls base class read_regs method.
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Detector_Regions::read_regs(const char * region_file,unsigned long xmax, unsigned long ymax)
{
  Regions::read_regs(region_file,xmax,ymax);
  chip_to_amp();
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::total_readout_pixels
 *
 * INVOCATION: total_readout_pixels()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * none
 *
 * FUNCTION VALUE: int
 * 
 * PURPOSE: Return total number of pixels that will be read out
 *
 * DESCRIPTION: 
 *  Return total number of pixels that will be read out
 *  returns sum of (unbinned) width*height of all serial regions in all
 *  parallel regions.
 *
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *   
 *- 
 */
unsigned long Detector_Regions::total_readout_pixels()
{
  // # unbinned pix in reference amplifier.
  // the means pixels with multiple ampmasks are counted only once,
  // but this is taken care of in unravel_pix() using the ampmasks.
  int i,j;
  unsigned long totalpix;
  totalpix = 0;
  for (i=0; (i<npar_regions); i++) 
    for (j=0; j<par_regions[i].nser_regions; j++) {
      totalpix+= par_regions[i].height * par_regions[i].ser_regions[j].width;
    }
  return totalpix;
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::total_binned_readout_pixels
 *
 * INVOCATION: total_binned_readout_pixels()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * none
 *
 * FUNCTION VALUE: unsigned long
 *  
 * PURPOSE: 
 *  return total number of binned pixels in the readout specification.
 *
 * DESCRIPTION: 
 *  returns sum of (binned) width*height of all serial regions in all
 *  parallel regions. width/height are rounded up to nearest unsigned long.
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
unsigned long Detector_Regions::total_binned_readout_pixels()
{
  int i,j;
  unsigned long totalbinnedpix;
  // compute # of binned pixels in "reference" amplifier.
  // the means pixels with multiple ampmasks are counted only once,
  // but this is taken care of in unravel_pix() using the ampmasks.
    totalbinnedpix = 0;
    for (i=0; (i<npar_regions); i++) 
      for (j=0; j<par_regions[i].nser_regions; j++) {
	totalbinnedpix+=par_regions[i].height_b * par_regions[i].ser_regions[j].width_b;
      }
    return totalbinnedpix;
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::get_progress_counter
 *
 * INVOCATION: get_progress_counter()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * none
 *
 * FUNCTION VALUE: int - total rows for all regions
 *
 * PURPOSE: 
 *  Compute total size of (binned) image rows for
 *  "Reading out xxxx/yyyy" progress display in gui.
 * 
 * DESCRIPTION: 
 *   return sum of binned height of all par regions.
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
unsigned long Detector_Regions::get_progress_counter()
{
  int i;
  unsigned long target = 0;

  for (i=0; i<npar_regions; i++) 
    target += (unsigned long)(ceil(((float)par_regions[i].height)/ccf));
  
  return target;
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::amp_index
 *
 * INVOCATION: amp_index(int f, int sf)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > f - frame index
 * > sf - subframe index
 * 
 * FUNCTION VALUE: int 
 *
 * PURPOSE: 
 *  return amplifier index of a given frame and subframe number
 *
 * DESCRIPTION: 
 *  return amp_ampnum[g(f,sf)], which is filled in chip_to_amp()
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
int Detector_Regions::amp_index(int f, int sf)
{
  return amp_ampnum[frames[f].subframes[sf].amp];
}

/*
 *+ 
 * FUNCTION NAME: Detector_Regions::chip_index
 *
 * INVOCATION: chip_index(int f, int sf)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > f - frame index
 * > sf - subframe index
 *
 * FUNCTION VALUE: int
 *
 * PURPOSE: 
 *   return chip index of a given frame number
 *
 * DESCRIPTION: 
 *   return chip index, which is filled in chip_to_amp()
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
int Detector_Regions::chip_index(int f, int sf)
{
  return frames[f].subframes[sf].chip;
}
