/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *   regions.h
 * 
 * PURPOSE
 *   Simple regions of interest base class
 *
 * HISTORY
 *   July 1997 Kim    Created for RSAA CICADA package
 *   June 2002 PJY    Ported to VxWorks/GNIRS from RSAA CICADA package
 */

#ifndef _REGIONS_H
#define _REGIONS_H

const int MAX_REGIONS = 16;

/*
CLASS
    Regions
    Used for detector regions of interest (ROI) handling.  
DESCRIPTION  
    Used for detector regions of interest handling. This base class sets up data
    structures that spcify chosen regions of interest.
KEYWORDS
    Regions
*/
struct Reg_Spec {
  unsigned long rxo,ryo,rw,rh;
};
class Regions {
 private:
  int debug;                            //+ Debugging level
  
 public:

  //+ Default constructor - initialises member vars
  Regions();
  //+ Constructor with initial data
  // > n - number of regions in spec
  // > xo - array of x offsets
  // > yo - array of y offsets
  // > width - array of width specs
  // > height - array of height specs
  Regions(unsigned short n, unsigned long *xo, unsigned long *width, 
	  unsigned long *yo, unsigned long *height);
  //+ Destructor - frees allocated memory
  virtual ~Regions();

  // GROUP:   Public Member variables
  int nregions;                         //+ number of regions in spec
  unsigned long *reg_xo;                //+ region x offsets
  unsigned long *reg_yo;                //+ region y offsets
  unsigned long *reg_width;             //+ region widths
  unsigned long *reg_height;            //+ region heights

  // GROUP:   Public Member functions
  //+Read region descriptions from file - formatted using ximtool marker conventions
  // > region-file - name of file to read region spec from
  // > xmax - maximum allowed x coordinate
  // > ymax - maximum allowed y coordinate
  virtual void read_regs(const char * region_file, unsigned long xmax, unsigned long ymax);

  //+ write region descriptions to file - formatted using ximtool marker
  //+ conventions
  // > region-file - name of file to read region spec from
  void write(char* region_file);

};
#endif // _REGIONS_H
