/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_cadData.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI NIFS PROJECT
 * 
 * FILENAME dc_cadData.c
 * 
 * GENERAL DESCRIPTION
 *
 * Handle CADS from commands that are ignored by the NIFS DC.  These are DATUM,
 * GUIDE, ENDGUIDE, VERIFY, ENDVERIFY and ENDOBSERVE.  Their equivalent CAR
 * records will be set to busy and then back to idle.
 *
 *
 * $Log: dc_cadData.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.9  2005/09/07 00:48:07  pjy
 * Moved message_write to dc.cc
 *
 * Revision 1.8  2005/02/25 04:48:36  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.7  2004/12/22 06:09:53  pjy
 * DC cad src files ready for first Gemini release
 *
 * Revision 1.6  2004/08/20 02:27:04  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.5  2004/04/27 07:25:53  pjy
 * T2.2
 *
 * Revision 1.1  2004/03/23 01:59:48  pjy
 * Gem 8.6 port
 *
 * Revision 1.2  2003/09/19 06:24:21  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.1  2003/03/11 01:29:26  pjy
 * Import modules
 *
 * 
 */
/* VxWorks include files */
#include <cstdio>
#include <cstring>

#ifdef __cplusplus
extern "C" {
#endif
#include <cadRecord.h>
#include <cad.h>
#include <dbAccess.h>
#include <dbCommon.h>
#ifdef __cplusplus
}
#endif

#include "parameter.h"
#include "configuration.h"
#include "ir_camera.h"
#include "utility.h"
#include "logging.h"

// Globals

//+ The camera
extern IR_Camera *ir_camera;            //+ The infra red camera object - created by dcSlave task
extern bool camera_setup_changed;       //+ Flag set when any readout setting change

#ifdef __cplusplus
extern "C" long dcCADdata(struct cadRecord *pCad);
extern "C" long dcCADdataInit(struct cadRecord *pCad);
extern "C" long dcCADsetDHSInfo(struct cadRecord *pCad);
extern "C" void set_and_verify_output_cad(struct cadRecord *pCad);
extern "C" void load_input_cad(struct cadRecord *pCad);
#endif

/*
 *+
 * FUNCTION NAME:
 * dcCADdata
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADdata( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for data CAD records
 *
 * DESCRIPTION:
 * This routine is called whenever a datauence CAD record is processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADdata(struct cadRecord *pCad) 
{
  long status;                     /* return status */
  
  status = CAD_ACCEPT;
  
  logger::message(logger::Level::Full,"dcCADdata: %d directive.", pCad->dir);
   
  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:                    
    break;

  case menuDirectivePRESET:		
    /* Reject the command if the arguments don't validate */
     try {
      // Verify input parameters and set outputs
      set_and_verify_output_cad(pCad);
      camera_setup_changed = true;
      logger::message(logger::Level::Full,"dcCADdata: Data Cad verified ok!");
    } 
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;

  case menuDirectiveSTART:		
    
    break;
    
  case menuDirectiveSTOP:			/* Really can't	*/
    strncpy(pCad->mess, "dcCADdata: Cannot stop!", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
    
  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADdata: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  } 
  return status;
}

/*
 *+
 * FUNCTION NAME:
 * dcCADdataInit
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADdataInit( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined inam function for seq CAD records
 *
 * DESCRIPTION:
 * This routine is called whenever an sequence CAD record is loaded.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 * None
 *
 *-`
 */

long dcCADdataInit(struct cadRecord *pCad) 
{
  long status;                     /* return status */
  
  status = CAD_ACCEPT;
  
  // Call the routine to load the input fields
  try {
    load_input_cad(pCad);
  }
  catch (Error& err) {
    status = CAD_REJECT;
    err.print_error(__FILE__, __LINE__);
    strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
  }

  return status;
}

/*
 *+
 * FUNCTION NAME:
 * dcCADsetDHSInfo
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADsetDHSInfo( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for seq CAD records
 *
 * DESCRIPTION:
 * This routine is called whenever a sequence CAD record is processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADsetDHSInfo(struct cadRecord *pCad) 
{
  long status;                     /* return status */
  
  status = CAD_ACCEPT;
  
  logger::message(logger::Level::Full,"dcCADsetDHSInfo: %d directive.", pCad->dir);
  
  /* Create name of record to process car from name of this record*/
  
  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:                    
    break;

  case menuDirectivePRESET:		
    // Simply copy input parameters to outputs
     try {
      // Verify input parameters and set outputs
      set_and_verify_output_cad(pCad);

      logger::message(logger::Level::Full,"dcCADsetDHSInfo: DHS Cad verified ok!");
    } 
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;

  case menuDirectiveSTART:		
    
    break;
    
  case menuDirectiveSTOP:			/* Really can't	*/
    strncpy(pCad->mess, "dcCADsetDHSInfo: Cannot stop!", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
    
  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADsetDHSInfo: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  } 
  return status;
}


