/*
 * Copyright (c) 2000-2003 RSAA
 * $Id: fits_server.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 *
 * GEMINI GNIRS PROJECT
 * 
 * FILENAME fits_server.cc
 * 
 * GENERAL DESCRIPTION
 *
 * This program listens on a socket connection for messages to save
 * data into a FITS file. Each Fits file will be transferred as a 
 * series of messages using following protocol:
 *
 *  Main_header - describes image that follows
 *  N x pairs of Sub_header + Image data - sub header describes the image packet to follow
 *  Close_Header - indicates image complete
 *
 * To run on
 * Solaris:      $ ./fits_server server_port [notify [outdir]]
 * where
 * server_port - port number for listening on
 * notify - boolean to indicate whether to post a notification message
 * outdir - output directory name (default = current dir)
 * 
 * $Log
 *
 *
 */
/*include files*/
#ifdef vxWorks
#error This code only runs under Solaris
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include "ipc.h"
#include "fits.h"
#include "image.h"
#include "common.h"
#include "camera.h"
#include "configuration.h"
#include "utility.h"

using namespace std;

// Global variables
static Detector_Image_Main_Header main_header;
static char frames_mem[FRAMES_MEM_SIZE];
static Nametype key_name;

Detector_Image *shared_image_buffer = NULL;
Config *shared_config = NULL;

/* function definitions */
static int get_message(TCPSocket_Server* fits_server, char* buf, int len);
static void create_new_fits(const char* dir, Fits*& new_fits);
static void notify_new_image(Mailbox* image_mox, Image_Descriptor& image);
static char* get_key_name(const char* base_key_name, int idx);

/* Main program - handles input from serial or socket interface */
int main(int argc, char *argv[])
{
  // Parameters
  int server_port;
  Filenametype dir;                
  Hostnametype hostname;
  bool notify = false;
  char *frames_buf=NULL;                   // Temporary frames buffer

  // data structures 
  TCPSocket_Server* fits_server = NULL;
  Detector_Image_Header header;
  Detector_Image_Sub_Header *sub_header = &header.Detector_Image_Header_u.sub_header;  
  Detector_Image_Close_Header *close_header = &header.Detector_Image_Header_u.close_header; 
  Frame_Data *this_frame;
  Sub_Frame_Data* this_subframe;
  Fits *new_fits = NULL;
  Image_Descriptor image = {   // Setup some suitable values for the image descriptor
    TRUE,  // new_image
    "",    // image name - set when known later
    TRUE,  // adaptive_scaling
    TRUE,  // use_median
    0.0,   // min scaling value - don't use
    0.0,   // max scaling value - don't use
    1000,  // sample_pix      
    0.5,   // contrast
    0,     // disp_xo
    0,     // disp_yo
    1,     // disp_xbin
    1      // disp_ybin
  };
  Mailbox* image_mbox = NULL;                     // Mailbox for latest image bulletins
  char qname[IPCNAMELEN];


  // Work variables
  int ack, nr, n, fi, j;
  char *buf = NULL;
  int buf_size = 0;

  // Expect server port as argument
  if (argc<2) {
    printf("Usage: fits_server server_port [notify [output_directory]]!\n");
    return 1;
  } else {
    server_port = atoi(argv[1]);
    if (argc>2)
      notify = true;
    if (argc>3) 
      strncpy(dir,argv[3],FILENAMELEN);
    else
      getcwd(dir, FILENAMELEN);
 }

  // Trap any exceptions
  try {   
    cout << "Fits_Server: Starting, port = " << server_port << endl;
    
    gethostname(hostname, HOSTLEN);
    
    // Allocate shared memory areas
    shared_config = new Config;
    
    shared_image_buffer = (Detector_Image*) malloc(sizeof(Detector_Image) + IMAGE_BUF_SIZE);  

    // Create network socket object
    fits_server = new TCPSocket_Server(hostname, server_port);

    
    // Make sure the receiving frames buffer is aligned appropriately
    frames_buf = (char*) memalign(ALIGN_SIZE,FRAMES_MEM_SIZE);

    // Set up image message queue if required
    if (notify) {
      ipc_name(qname, "Image", ".", geteuid(), 0);
      cout << "Fits_Server: Establishing an image message queue, name=" << qname << endl;
      image_mbox = new Mailbox(qname, ESTABLISH, RWALL);    
    }
    
    // Start accepting connections - does not exit
    while (true) {
      cout << "Fits_Server: Listening for connections ... " << endl;
      fits_server->accept_connections();
      
      cout << "Fits_Server: Connected ..." << endl;

      try {
	
	// Get message 
	cout << "Fits_Server: Waiting for first image header..." << endl;
	
	while (nr = get_message(fits_server, (char*)&header, sizeof(header)) > 0) {
		
	  cout << "Fits_Server: Read " << nr << " bytes from socket" << endl;
	  
	  try {
	    if (header.type != IMAGE_MAIN_HEADER) 
	      throw 0;
	    else {
	      ack = sizeof(header);
	      
	      // Header received ok - so acknowledge with expected value
	      fits_server->send_message((void*)&ack, sizeof(ack));

	      // Collect the frames data
	      nr = get_message(fits_server, frames_buf, FRAMES_MEM_SIZE);
	      cout << "Fits_Server: Read " << nr << " frames data bytes" << endl;
	      
	      // Make a copy of the main header as it is used during full image transfer
	      copy_frame_references(header.Detector_Image_Header_u.main_header, main_header, frames_buf,
				    frames_mem, true);
	      ack = nr;
	    }
	    
	    cout << "Fits_Server: Creating FITS file '" << main_header.client_image_name << "' in directory '" << 
	      dir << "'" << endl;
	    create_new_fits(dir, new_fits);
	    
	    // File created ok - so acknowledge with expected value
	    fits_server->send_message((void*)&ack, sizeof(ack));
	    
	    // Now collect the image data from the socket
	    nr = get_message(fits_server, (char*)&header, sizeof(header));
	    cout << "Fits_Server: Read " << nr << " header bytes" << endl;
	      
	    while (header.type == IMAGE_SUB_HEADER) {
	      // Got a sub_header message
	      ack = nr;
	      fits_server->send_message((void*)&ack, sizeof(ack));
	      
	      this_frame = &main_header.frames.frames_val[sub_header->frame];
	      this_subframe = &this_frame->subframes.subframes_val[sub_header->subframe];

	      // Now get the image pixels - first allocate the right size buffer
	      n = sub_header->height * sub_header->width * BYTE_PIX(this_frame->image_info.bitpix);
	      if (n != buf_size) {
		if (buf != NULL)
		  delete [] buf;
		buf = new char[n];
		buf_size = n;
	      }
	      
	      // Get the pixels - this might require a series of socket reads to fill the buffer
	      ack = get_message(fits_server, buf, n);
	      cout << "Fits_Server: Read " << ack << " pixel bytes" << endl;
	      
	      // Send acknowledgement
	      fits_server->send_message((void*)&ack, sizeof(ack));
	      
	      // Add the pixels to the Fits file
	      fi = sub_header->frame;
	      
	      // Only try writing the data if we have a valid frame index
	      if ((fi >= 0) && (fi <= new_fits->nextend)) {
		j = fi + new_fits->extend;
		new_fits->write_data(buf, new_fits->xt[j].bitpix, j, sub_header->x, sub_header->y,
				     sub_header->width, sub_header->height);
	      } else
		cout << "Fits_Server: - Bad frame number " << sub_header->frame << ", data not written!" << endl;
	      
	      // Now collect the next header record
	      nr = get_message(fits_server, (char*)&header, sizeof(header));	
	      cout << "Fits_Server: Read " << nr << " header bytes" << endl;
	    }
	    
	    // Expect to have a close-header if here -= send appropriate acknowledgement
	    if (header.type != IMAGE_CLOSE_HEADER) 
	      throw 0;
	    else 
	      ack = sizeof(header);
	    
	    fits_server->send_message((void*)&ack, sizeof(ack));
	  }
	  catch (int& ie) {
	    //Must have received and out of sync message
	    cout << "Fits_Server: Out of sync message - abandoning interpretation." << endl;
	    ack = 0;
	    fits_server->send_message((void*)&ack, sizeof(ack));	
	  }
	  
	  // Close the fits file
	  if (new_fits != NULL) {
	    strncpy(image.image_name, new_fits->fitsname, FITSNAMELEN);
	    new_fits->close();
	    delete new_fits;
	    new_fits = NULL;
	    cout << "Fits_Server: Closed " << image.image_name << " OK." << endl;
	    
	    if (notify) 
	      notify_new_image(image_mbox, image);
	  }
	  
	  // Message to indicate we are waiting for next image header
	  cout << "Fits_Server: Waiting for next image header..." << endl;
	}
      }
      catch (Error& e) {
	// Send an ack with 0 message to indicate failure
	ack = 0;
	fits_server->send_message((void*)&ack, sizeof(ack));	
	e.print_error(__FILE__, __LINE__);
	cout << "Fits_Server: End of connection..." << endl;
      }
    }
    
    // End of loop - go back to listen for next connection
  }
  catch (Error & e) {
    e.print_error(__FILE__, __LINE__);
  }
  
  // No longer need the receiving buffer now that frames block has been copied
  if (frames_buf != NULL) 
    free(frames_buf);

  /*
   * Since this program has an infinite loop, the socket "sock" is never
   * explicitly closed unless there is an abnormal termination. However, all
   * sockets will be closed automatically when a process is killed or terminates
   * normally.  
   */
  if (fits_server != NULL)
    delete fits_server;
  if (image_mbox != NULL)
    delete image_mbox;

  if (shared_config != NULL)
    delete shared_config;
  if (shared_image_buffer != NULL)
    delete shared_image_buffer;

}
	


/*
 *+ 
 * FUNCTION NAME: get_message
 * 
 * INVOCATION: get_message(TCPSocket_Server* fits_server, char* buf, int len) 
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > fits_server - TCP socket to use
 * < buf - buffer to fill
 * > len - number of bytes expected
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: reads len bytes from socket
 * 
 * DESCRIPTION: 
 * Keeps reading bytes from socket until buffer filled with len bytes
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *  No timeouts - could stall waiting forever...
 *- 
 */
static int get_message(TCPSocket_Server* fits_server, char* buf, int len) 
{
  int ack, n;
  char* addr = buf;
  
  if (fits_server == NULL)
    return 0;
  else {
    ack = 0;
    while (ack < len) {	  
      if ((n = fits_server->get_message(addr+ack, len-ack)) == 0)
	// Return a failure on zero length messages - client has disconnected...
	return 0;
      else
	ack += n;
    }
  }
  return ack;
}

/*
 *+ 
 * FUNCTION NAME: create_new_fits
 * 
 * INVOCATION: create_new_fits(const char* dir, Fits*& new_fits)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > dir - outpur directory
 * < fits - created fits file 
 * > main_header - image header record to use for creating Fits file
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Create a new empty Fits file with standard headers
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
static void create_new_fits(const char* dir, Fits*& new_fits)
{
  int naxes[2*FITS_MAX_EXTEND+2];
  Ext_Type etype[FITS_MAX_EXTEND+1];
  int bp[FITS_MAX_EXTEND+1];
  int pc[FITS_MAX_EXTEND+1];
  int gc[FITS_MAX_EXTEND+1];
  double bz[FITS_MAX_EXTEND+1];
  double bs[FITS_MAX_EXTEND+1];
  Frame_Data *this_frame;
  Sub_Frame_Data* this_subframe;
  int j,extend,xo,yo,f,sf;
  string fits_name;
  Fitsline keybuf;

  // Create the fits file based on the main header description
  if (new_fits != NULL)
    delete new_fits;
  new_fits = new Fits();
  
  // Set up Fits data structures from main_header
  j = (main_header.frames.frames_len>1)? 1:0;
  extend = (main_header.frames.frames_len>1)? main_header.frames.frames_len:0;

  naxes[0] = naxes[1] = 0;
  for (f=0; f<main_header.frames.frames_len; f++) {
    this_frame = &main_header.frames.frames_val[f];
    naxes[2*j] = this_frame->width;
    naxes[2*j+1] = this_frame->height;
    bp[j] = this_frame->image_info.bitpix;
    bz[j] = this_frame->image_info.bzero;
    bs[j] = this_frame->image_info.bscale;
    pc[j] = 0;
    gc[j] = 1;
    etype[j] = EXT_IMAGE;
    j++;
  }
  
  // Construct output file name
  if (strlen(dir) > 0) {
    fits_name = dir;
    if (dir[strlen(dir)-1] != '/')
      fits_name += "/";
  } else
    fits_name = "./";
  fits_name += basename(main_header.client_image_name);
  
  unlink(fits_name.c_str());

  // Make sure main header phu and ihu sensible
  main_header.phu = (!main_header.phu)? 1:main_header.phu;
  main_header.ihu = (!main_header.ihu)? 1:main_header.ihu;
  
  // Create the FITS file
  new_fits->create((char*)fits_name.c_str(), etype, bp, naxes, S_IRUSR|S_IWUSR, main_header.phu*FITS_KEYS_PER_BLOCK, 
		   extend, main_header.ihu*FITS_KEYS_PER_BLOCK);
  
  cout << "Fits_Server: Fits file '" << fits_name << "' created ok." << endl;
  
  // Write standard keywords
  new_fits->write_std_keywords(etype, bs, bz, pc, gc);

  // Write some geometry keywords. Here we make assumptions about some
  // information because the header structure cannot tell us more.
  sprintf(keybuf,"[1:%d,1:%d]",main_header.camera_width,main_header.camera_height);
  new_fits->write_skeyword("DETSIZE", keybuf, "Detector size (pixels)");
  new_fits->write_ikeyword("NCCDS", 1, "Number of CCDs");
  new_fits->write_ikeyword("NAMPS", main_header.frames.frames_len, "Number of amplifiers");

  j = (main_header.frames.frames_len>1)? 1:0;
  for (f=0; f<main_header.frames.frames_len; f++) {
    sprintf(keybuf,"[1:%d,1:%d]",main_header.frames.frames_val[f].width,main_header.frames.frames_val[f].height);   
    new_fits->write_skeyword("CCDSIZE",keybuf,"CCD size",j);
    new_fits->write_ikeyword("CCDNAMPS", main_header.frames.frames_val[f].subframes.subframes_len, 
			     "Number of amplifiers used to readout CCD",j);
    sprintf(keybuf,"%d %d",main_header.regions.rcf,main_header.regions.ccf);   
    new_fits->write_skeyword("CCDSUM",keybuf,"CCD pixel summing",j);

    for (sf=0; sf<main_header.frames.frames_val[f].subframes.subframes_len; sf++) {
      this_subframe = &main_header.frames.frames_val[f].subframes.subframes_val[sf];
      xo = this_subframe->frame_xo;
      yo = this_subframe->frame_yo;
      sprintf(keybuf,"%d",j);
      new_fits->write_skeyword(get_key_name("AMPNAME",sf), keybuf, "Amplifier identification",j);
      
      sprintf(keybuf,"[%d:%d,%d:%d]",xo+1,xo+this_subframe->width, yo+1, yo+this_subframe->height);
      new_fits->write_skeyword(get_key_name("AMPSIZE",sf),keybuf,"Amplifier Size",j);
      new_fits->write_skeyword(get_key_name("AMPSEC",sf),keybuf,"Amplifier Section",j);
      new_fits->write_skeyword(get_key_name("DETSEC",sf),keybuf,"Detector section",j);
      
      sprintf(keybuf,"[%d:%d,%d:%d]",1,this_subframe->width,1,this_subframe->height);
      new_fits->write_skeyword(get_key_name("CCDSEC",sf),keybuf,"Region of CCD read",j);
      new_fits->write_skeyword(get_key_name("DATASEC",sf),keybuf,"Data section",j);
      new_fits->write_skeyword(get_key_name("TRIMSEC",sf),keybuf,"Trim section",j);
    }
    j++;
  }
}
static char* get_key_name(const char* base_key_name, int idx)
{
  if (main_header.mosaic_mode != MOSAIC_NONE) 
    snprintf(key_name, NAMELEN, "%s%d", base_key_name, idx);
  else
    strncpy(key_name, base_key_name, NAMELEN);
  return key_name;		
}
/*
 *+ 
 * FUNCTION NAME: notify_new_image
 * 
 * INVOCATION: notify_new_image(Mailbox* image_mox, Image_Descriptor& image)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > image_mbox - message queue for depositing image notifications
 * > image - image description
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Write pixels to correct location if Fits file
 * 
 * DESCRIPTION: 
 * Pixels are written row by row as they could be at an arbitrary
 * rectangular location in the image an any image extension.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
static void notify_new_image(Mailbox* image_mbox, Image_Descriptor& image)
{
  int msgs_waiting;
  Image_Descriptor tmp_image;

  // Update image mailbox with message about new image available for reading from disk     
  // First clear any messages more than IPC_MAXMSG
  msgs_waiting = image_mbox->messages_waiting();
  while (msgs_waiting >= IPC_MAXMSG) {
    image_mbox->get_message((char *)&tmp_image, sizeof(Image_Descriptor), IPC_NOWAIT);
    msgs_waiting = msgs_waiting - 1;
  }
  
  // Set up latest_image descriptor
  // Work out the name of image being written
  image_mbox->send_message((char *)&image, sizeof(Image_Descriptor));
}
	
