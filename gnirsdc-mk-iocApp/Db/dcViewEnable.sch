[schematic2]
uniq 90
[tools]
[detail]
w 2376 2315 100 0 n#89 ebis.viewEnBi.FLNK 2240 2304 2560 2304 hwout.hwout#88.outp
s 2288 2256 100 0 Enable/Disable
[cell use]
use hwout 2560 2263 100 0 hwout#88
xform 0 2656 2304
p 2656 2295 100 0 -1 val(outp):#C0 B0
use ebis 2048 2352 100 0 viewEnBi
xform 0 2112 2288
p 2048 2144 100 0 1 ONAM:disabled
p 2048 2112 100 0 1 ZNAM:enabled
use eborderC 608 799 100 0 eborderC#0
xform 0 2288 2104
p 3184 952 100 768 -1 author:M Jarnyk
p 3184 920 100 768 -1 date:19 Oct 2001
p 3408 1000 200 768 -1 file:dcViewEnable
p 3680 952 100 0 -1 page:1
p 3792 952 100 0 -1 pages:1
p 3456 952 100 0 -1 revision:0
p 3408 1064 150 768 -1 system:Gemini GNIRS DC
[comments]
