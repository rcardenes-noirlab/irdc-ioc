/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME sdsu_volts.h
 * 
 * GENERAL DESCRIPTION
 *  CICADA SDSU voltages table class header.
 * 
 */
#ifndef _SDSU_VOLTS_H
#define _SDSU_VOLTS_H

#include "table.h"
#include "preferences.h"
#ifdef vxWorks
#include "configuration.h"
#endif
#include "config_table.h"
#include <string>
#include <list>

using namespace std;

const char TEMP_VOLTS_FILE[] = "/tmp/sdsu.volts.tmp";
const char VOLTS_ERROR_FILE[] = "/tmp/sdsu.volts.errors";

// class declarations 

/*-------------------------------------------------------------------------
CLASS
    Sdsu_Volts
    Volts table class
DESCRIPTION  
    Used to hold SDSU voltage settings
 
KEYWORDS
    Sdsu_Volts Config_Table Table
-------------------------------------------------------------------------*/
class Sdsu_Volts:public Config_Table  {
private:
  // GROUP:   Private member variables.
  int error;                            //+ set true if errors found during table read
  list<string> *em;                     //+ pointer to list of error messages

public:
  
  //+ Constructor 
  Sdsu_Volts();

  //+ Destructor
  ~Sdsu_Volts();  

  // GROUP:   Public member variables.
  Volts_Desc volt_desc;                 //+ buffer holding recently accessed desc
  Volts_Desc volt_desc_save;            //+ buffer holding recently accessed desc saved

  // GROUP:   Public member Functions.

  //+ read & check voltages file 
  // > tname - name of table
  // < emsgs - list of error message strings
  int read(const string tname, list<string>* emsgs);

  //+ check value is within bounds
  // > v - value to check
  void check_value(Sdsu_Voltage_Index v);                

  //+ print out error
  // > msgbuf - error message
  void report_error(char* msgbuf);      

  //+ add a volt_desc, itemp is pointer to desc
  // > itemp - pointer to data to insert into table
  // > pos - position in table for insertion
  void put(void* itemp,int pos=999);    

  //+ gets a volt desc
  // > item - description of item to get
  void get(Table_Item *item);           

  //+ dump table to an array
  // > vd - voltage desc to dump
  int dump(Volts_Desc *vd);              

  //+ returns desc of idx can be an lvalue
  // > idx - index of item in table
  // > item_name - name of item in table
  Volts_Desc& operator[](int idx);      
  Volts_Desc& operator[](const char* item_name);
};

#endif //_SDSU_VOLTS_H 
