/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 * $Id
 * GNIRS PROJECT
 * 
 * FILENAME 
 *   dhs_client_test.cc
 * 
 * GENERAL DESCRIPTION
 *   Simple test program for dhs_client class
 *
 * ORIGINAL AUTHOR: 
 *   Peter Young
 *
 * HISTORY
 *
 *  April 2003 - PJY Created.
 *
 *INDENT-ON
 *
 * $Log: dhs_client_test.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Gnirs; first gemini-modified release
 *
 * Revision 1.5  2005/08/18 03:57:32  pjy
 * Added routine to run test in spawned task
 * Use mid constructor for IR_Camera (simple one not working???)
 *
 * Revision 1.4  2005/07/18 00:42:23  pjy
 * Removed shared_image_buffer
 * Added frames_mem as param
 *
 * Revision 1.3  2004/12/22 06:13:46  pjy
 * DC data src files ready for first Gemini release
 *
 * Revision 1.2  2004/08/20 02:31:48  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.1  2004/03/19 01:51:44  pjy
 * Added
 *
 *
 -------------------------------------------------------------------------*/
#ifdef vxWorks
#include <vxWorks.h>
#include <sysLib.h>
#include <stdioLib.h>
#include <sysLib.h>
#include <timers.h>
#include <cicsLib.h>
#include <memLib.h>
#include <hostLib.h>
#endif
#include <errno.h>

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/

#include <time.h>
#include "dhs.h"
#include "dhs_client.h"
#include "ir_camera.h"
#include "config_table.h"
#include "platform_specific.h"
#include "utility.h"
#include "dc.h"

/*---------------------------------------------------------------------------
 External functions
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 External variables
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Static variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Global variables
 -------------------------------------------------------------------------*/
#ifdef SOLARIS
Detector_Image *shared_image_buffer=NULL;         //+ image transfer buffer
Config *shared_config = NULL;                     //+ Full instrument configuration - read from disk
#else
extern Detector_Image *shared_image_buffer;       //+ image transfer buffer
#endif

/*--------------------------------------------------------------------------
 Class definitions
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Non-member functions
 -------------------------------------------------------------------------*/

// dhs_client_test "GNIRS","dataServerNS","150.203.89.131","cnf/gnirs_mdata16.fits"
#ifdef vxWorks
  static int dhs_client_test_task(char* cname, char* sname, char* ip, char* fits_name);
  int dhs_client_test(char* cname, char* sname, char* ip, char* fits_name)
{
  int tid;

  // Start this in a spawned task with an appropriately sized stack
  if ((tid = taskSpawn("dhs_client_test", 100, VX_FP_TASK, 0x1000000, (FUNCPTR) dhs_client_test_task, 
		      (int)cname, (int)sname, (int)ip, (int)fits_name,0,0,0,0,0,0)) == ERROR) {
    printErr("taskSpawn of dhs_client_test_task failed, errno=%d!\n", errno,0,0,0,0,0);
  }

  return 0;
}
static int dhs_client_test_task(char* cname, char* sname, char* ip, char* fits_name)
{
#else
int main(int argc, char *argv[])
{
  Nametype cname;
  Hostnametype ip;
  Nametype sname;
  Fitsname fits_name;
#endif
  DHS_Client *dhs_client = NULL;
  Fits *fits = NULL;
  IR_Camera *ir_camera = NULL;
  Frame_Data *frames = NULL;
  Sub_Frame_Data* this_subframe = NULL;
  Instrument_Status *inst_status;            //+ Full instrument status - local copy
  Hostnametype host;                  //+ Name of this host

  Camera_Desc camera;
  Filenametype fname;
  Region_Data regions;
  unsigned int nframes;
  Detector_Image_Info image_info;
  Detector_Image_Sub_Header sub_header;
  Platform_Specific specific;   
  const int NSLICES = 10;
  int f,s,scols,srows,j,r,sf,x;
  char *qls[2];                // List of streams to send data to
  Camera_Table ctable;
  
#ifndef vxWorks
  // Typical values for parameters
  //    cname = "GNIRS";
  //    sname = "dataServerNS";
  //    ip = "150.203.89.131";

  if (argc < 6) {
    printf("Usage: dhs_client_test connection_name server_name ip fits_name\n");
    return -1;
  }
  strncpy(cname, argv[1], NAMELEN);
  strncpy(sname, argv[2], NAMELEN);
  strncpy(ip, argv[3], IPC_HOSTLEN);
  strncpy(fits_name, argv[4], FITSNAMELEN);
#else
  cicsSetDebug(DEBUG_FULL);
#endif

  try {
    // read in the camera configuration table
    strncpy(fname, CAMERA_TABLE, FILENAMELEN-1);
    cout << "Reading camera configuration table " << fname << endl;
    ctable.read(fname);
    
    cout << "Done" << endl;

    camera = ctable[cname];

    // Allocate space for the shared image
    shared_image_buffer = new Detector_Image;

    cout << "Instantiating DHS client" << endl;
    dhs_client = new DHS_Client(cname, sname, ip, &camera);   
    cout << "Done" << endl;

    // Open and read Fits file to get some data for this camera - assumes user has matched Fits file with camera
    cout << "Opening input FITS file " << fits_name << endl;
    fits = new Fits(fits_name, O_RDONLY, TRUE);
    cout << "Done" << endl;
    
    if (gethostname(host, HOSTLEN) == -1) {
      throw Error("Failed to get hostname of this IOC!", E_ERROR, errno, __FILE__, __LINE__);
    }

    inst_status = new Instrument_Status;
    
    inst_status->id = 0;
    inst_status->pid = 0;

    cout << "Instantiating an IR_Camera object" << endl;
    ir_camera = new IR_Camera(camera, 0, 0, host, inst_status, NULL, &specific);
 
    //    ir_camera = new IR_Camera(camera,0,0,&specific);
    cout << "Done" << endl;

    // Describe a single full detector region
    regions.camera_coords = true;
    regions.nregions = 1;
    regions.xo[0] = regions.yo[0] = 0;
    regions.width[0] = camera.cols;
    regions.height[0] = camera.rows;
    regions.rcf = regions.ccf = 1;
    regions.overscan_cols = regions.overscan_rows = 0;
    regions.chip_mask = 1;
 
    image_info.unsigned_data= false;
    image_info.bitpix = fits->xt[0].bitpix;
    image_info.bscale = 1.0;
    image_info.bzero = 0.0;

    // Call the camera methods that can setup the data structures describing the image
    ir_camera->setup_regions(regions, image_info.bitpix);
    setup_image_header(0, 0, 0, false, &camera, ir_camera->main_header, ir_camera->rgn, regions, 
		       (char*)&shared_image_buffer->frames_mem, image_info, false, false);
    nframes = ir_camera->main_header.frames.frames_len;
    frames = ir_camera->main_header.frames.frames_val;

    qls[0] = cname;

    // Setup the DHS to handle a new dataset for this image
    cout << "Setting up the header dataset" << endl;
    dhs_client->setup_hdr_dataset(cname, DHS_BD_LT_PERMANENT, cname, "", 1, qls, ir_camera->main_header,
				  (char*)&shared_image_buffer->frames_mem);
    cout << "Done" << endl;

    // Send the main header stuff
    cout << "Sending the header dataset to the DHS" << endl;
    dhs_client->send_dataset(DHS_FALSE);
    cout << "Done" << endl;

    // Send image as a set of chunks 
    for (s=0; s<NSLICES; s++) {

      // First setup the data sub header - send row oriented frames first
      // Assume each frame has same size
      x = 0;
      for (f=0; f<(int)nframes; f++) {	
	for (sf=0; sf<(int)frames[f].subframes.subframes_len; sf++) {
	  this_subframe = &frames[f].subframes.subframes_val[sf];
	  srows = this_subframe->height/NSLICES;
	  scols = this_subframe->width/NSLICES;
	  sub_header.x = (this_subframe->orient == ORIENT_ROW)? 0:s*scols;
	  sub_header.y = (this_subframe->orient == ORIENT_ROW)? s*srows:0;
	  sub_header.frame = f;
	  sub_header.subframe = sf;
	  sub_header.subframe_idx = this_subframe->subframe_idx;
	  sub_header.width = (this_subframe->orient == ORIENT_ROW)? this_subframe->width:
	    ((s+1)*scols > (long)this_subframe->width)? (this_subframe->width-s*scols) : scols;
	  sub_header.height = (this_subframe->orient == ORIENT_COL)? this_subframe->height:
	    ((s+1)*srows > (long)this_subframe->height)? (this_subframe->height-s*srows) : srows; 
	  
	  // Now get the DHS ready to accept these chunks
	  cout << "Setting up dataset for slice " << s+1 << endl;
	  dhs_client->setup_data_chunk(&sub_header);
	  cout << "Done" << endl;
	  
	  // Fill the DHS data arrays with pixel data
	  cout << "Filling the data buffer with FITS pixels" << endl;
	  j = (fits->extend)? 1:0;
	  if (this_subframe->orient == ORIENT_ROW) {
	      memcpy(dhs_client->pixel_data[f], fits->xt[j+x].data+
		     (sub_header.y*sub_header.width*BYTE_PIX(image_info.bitpix)), 
		     sub_header.height*sub_header.width*BYTE_PIX(image_info.bitpix));
	  } else {
	    for (r=0; r<(long)sub_header.height; r++) {
	      memcpy((char*)dhs_client->pixel_data[f]+(r*sub_header.width*BYTE_PIX(image_info.bitpix)), 
		     fits->xt[j+x].data+((r*this_subframe->width+sub_header.x)*BYTE_PIX(image_info.bitpix)), 
		     sub_header.width*BYTE_PIX(image_info.bitpix));
	    }
	  }
	  cout << "Done" << endl;
	    
	  // Now send the data to the DHS
	  cout << "Sending the slice dataset to the DHS" << endl;
	  
	  dhs_client->send_dataset(DHS_FALSE);
	  cout << "Done" << endl;

	  // increment the fits xtension
	  x++;
	}
      }
    }

    // Setup the DHS to handle a new dataset for this image
    cout << "Setting up end dataset" << endl;
    dhs_client->setup_end_dataset();
    cout << "Done" << endl;

    // Send the main header stuff
    cout << "Sending the close dataset to the DHS" << endl;
    dhs_client->send_dataset(DHS_TRUE);
    cout << "Done" << endl;


    // Send the main header stuff
    cout << "Retrieve the dataset from the DHS" << endl;
    dhs_client->retrieve_dataset(dhs_client->data_label);
    cout << "Done" << endl;
  } 
  catch (Error& e) {
    cout << "Check dhs.h for meaning of DHS error codes..." << endl;
    e.print_error(__FILE__,__LINE__);
  }

  if (shared_image_buffer != NULL) 
    delete shared_image_buffer;

  if (fits != NULL) 
    delete fits;
  if (ir_camera != NULL) 
    delete ir_camera;    
  if (dhs_client != NULL) 
    delete dhs_client;
  if (inst_status != NULL) 
    delete inst_status;
  return 0;
}
#ifdef vxWorks
// Routine to use to run test in spawned task
void spawn_dhs_client_test()
{
  int dhs_tid;
  
  cicsSetDebug(DEBUG_FULL);

  if ((dhs_tid = taskSpawn("dhs_client_test", 100, VX_FP_TASK, 0x400000, (FUNCPTR) dhs_client_test, 
			   (int)"GNIRS",(int)"dataServerNS",(int)"150.203.89.131",(int)"cnf/gnirs_mef.fits",(int)"GNIRS",
			   0,0,0,0,0)) == ERROR) {
    message_write(ALWAYS_LOG,"spawn_dhs_client_testt: taskSpawn of dhs_client_test failed!");
  } else
    message_write(ALWAYS_LOG,"spawn_dhs_client_testt: taskSpawn of dhs_client_test succeeded!");
}
#endif
