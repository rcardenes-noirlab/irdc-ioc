/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 *
 * FILENAME hardware.h
 * 
 * PURPOSE
 * This header file defines the base hardware class in RSAA's model
 * for astronomical instrumentation. See the system documentation
 * for a full description of this model.
 *
 */
#ifndef _HARDWARE_H
#define _HARDWARE_H

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#include "parameter.h"
#include "platform_specific.h"
#include "configuration.h"
#include "errlog.h"
#include "logging.h"

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/
static const char STATUS_INFO_TYPE[] = "status_info_type";
static const char STATUS_INFO_TYPE_DB[] = "";

/*-------------------------------------------------------------------------
CLASS
    Hardware

DESCRIPTION  
    Definition of the base hardware class in CICADA's model for astronomical
	instrumentation.  Actual hardware definitions are derived from this.
 
KEYWORDS
    Hardware
-------------------------------------------------------------------------*/
class Hardware {

public:

  static const std::string PARAMETER_NAME;

  //+ Default Constructor
  // > ps - structure for specific platforms
  Hardware(Platform_Specific* ps);

  //+ Constructor used when access to parameters only required
  // > param_host - hostname where parameters reside
  // > st - status structure with mappings for each parameter
  // > pm - type of parameter map
  Hardware(const char* param_host, Instrument_Status *st, Platform_Specific* ps);

  //+ Constructor used when full access to hardware required
  // > st - status structure with mappings for each parameter
  // > msg - ring buffer for messages
  // > deb - debugging mask, when set to 0, no debugging output
  // > ps - structure for specific platforms
  Hardware(Instrument_Status *st, int deb, Platform_Specific* ps);

  //+ Hardware class Destructor
  virtual ~Hardware();

  // GROUP:   Public Hardware class member Functions.

  //+ Received a signal to pause. If shutter open close it then wait for
  //  semaphore to continue - onces set reopen shutter if closed.
  virtual void handle_interrupt() = 0;

  //+ Interrupt handle for the RESET signal
  virtual void handle_reset();

  //+ Clear the reset signal
  virtual void clear_reset();

  //+ Interrupt handle for the STOP signal
  virtual void handle_stop();

  //+ Clear the stop signal
  virtual void clear_stop();

  //+ Sets reporting object and debug level
  // > deb - debugging flag
  virtual void change_debug(int deb);

  //+ Received a state change signal, handle it
  virtual void handle_change_state() = 0;

  //+ Common request finish up
  virtual void request_wrapup();

  //+ Write a varargs message to the message buffer and report file
  // > format - message format
  // > ... - varargs parameter list
  template<typename ... Args>
  void message_write(const string& format, Args const & ... args) {
	  logger::message(logger::Level::Min, format, args...);
  }

  // GROUP: Public Hardware class member Variables.
  Instrument_Status *status;        //+ Status information
  Instrument_Ctl_Status *ctl_status;//+ Instrument process status info
  Hardware_Status *hardware_status; //+ Specific hardware status information
  ParameterDB *hardware_pDB;        //+ parameter DB object
  Parameter *hardware_statusp;      //+ parameter status database
  int debug;              //+ debug level
  bool hardware_abort;              //+ abort flag
  bool hardware_stop;               //+ stop flag

protected:
  // GROUP:   Protected Hardware class member Variables.
  Platform_Specific* specific;
  Config *current_config;           // Current configuration data - shared memory
  ParamMapType *hardware_param_map; //+ Parameter name to pointer mapping

  // GROUP:   Private Hardware class member Functions.
  //+ initialise class member variables - called from constructors
  virtual void init_vars();

  //+ Add status parameters to the status database - called from constructors
  // > param_host - hostname where parameters reside
  // > init - flag set if required to initialise added parameters
  virtual void add_status_parameters(const char* param_host, bool init);

private:

};
/*-------------------------------------------------------------------------
CLASS
    Hardware_Test

DESCRIPTION
    Simple test class for the base Hardware class. Does not do anything other
    than to declare empty stub methods for pure virtual Hardware methods.
 
KEYWORDS
    Hardware Hardware_Test
-------------------------------------------------------------------------*/
class Hardware_Test: public Hardware {

public:

  //+ Default Constructor
  Hardware_Test(Platform_Specific* ps):Hardware(ps) {};

  //+ Constructor used when access to parameters only required
  // > param_host - hostname where parameters reside
  // > st - status structure with mappings for each parameter
  // > pm - type of parameter map
  Hardware_Test(const char* param_host, Instrument_Status *st, Platform_Specific* ps):
    Hardware(param_host, st, ps) {};

  //+ Constructor used when full access to hardware required
  // > st - status structure with mappings for each parameter
  // > deb - debugging mask, when set to 0, no debugging output
  Hardware_Test(Instrument_Status *st, int deb, Platform_Specific* ps):
    Hardware(st, deb, ps) {};

  //+ Hardware class Destructor
  virtual ~Hardware_Test() {};

  // GROUP:   Public Hardware class member Functions.

  //+ Received a signal to pause. If shutter open close it then wait for
  //  semaphore to continue - onces set reopen shutter if closed.
  virtual void handle_interrupt() {};

  //+ Received a state change signal, handle it
  virtual void handle_change_state() {};

protected:

private:

};


#endif //_HARDWARE_H 
