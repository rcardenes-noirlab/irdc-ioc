[schematic2]
uniq 4
[tools]
[detail]
w 4456 2971 100 0 n#1 elongins.rebootID.VAL 4400 2960 4560 2960 ecars.rebootC.ICID
w 3104 3915 -100 0 CLID inhier.CLID.P 3088 3904 3168 3904 3168 3856 3232 3856 ecad4.reboot.ICID
w 3620 3963 -100 0 c ecad4.reboot.VAL 3552 3888 3616 3888 3616 4048 3744 4048 outhier.VAL.p
w 3608 3259 100 0 n#2 ecad4.reboot.SPLK 3552 3248 3712 3248 3712 3328 3792 3328 cadPlus.rebootP.SPIN
w 3592 3291 100 0 n#3 ecad4.reboot.STLK 3552 3280 3680 3280 3680 3360 3792 3360 cadPlus.rebootP.STIN
w 3644 3915 -100 0 c ecad4.reboot.MESS 3552 3856 3648 3856 3648 3984 3744 3984 outhier.MESS.p
w 3204 3931 -100 0 c inhier.DIR.P 3136 3984 3200 3984 3200 3888 3232 3888 ecad4.reboot.DIR
s 2624 2496 200 0 The REBOOT CAD schematic. SNL code monitors the rebootP
s 2624 2416 200 0 START input link to ready the system for a reboot.
[cell use]
use elongins 4144 2903 100 0 rebootID
xform 0 4272 2976
use ecars 4560 2711 100 0 rebootC
xform 0 4720 2880
use inhier 3072 3863 100 0 CLID
xform 0 3088 3904
use inhier 3120 3943 100 0 DIR
xform 0 3136 3984
use cadPlus 3792 3408 150 0 rebootP
xform 0 3872 3312
p 3792 3200 100 0 1 set1:cad rebootP
use outhier 3712 4007 100 0 VAL
xform 0 3728 4048
use outhier 3712 3943 100 0 MESS
xform 0 3728 3984
use ecad4 3312 3932 150 0 reboot
xform 0 3392 3568
p 3476 2620 100 0 0 MFLG:THREE STATES
p 3312 3168 100 0 1 SNAM:CADreboot
use eborderC 2408 2027 100 0 eborderC#0
xform 0 4088 3332
p 4984 2180 100 768 -1 author:Peter Young
p 4984 2310 40 0 -1 comment1:1999-01-11 GMOS version. Ken Ramey
p 4984 2290 40 0 -1 comment2:2000-11-02 Modified for GNIRS. Peter Young
p 4984 2148 100 768 -1 date:2000-11-03
p 5208 2228 200 768 -1 file:dcRebootCad.sch
p 5480 2180 100 0 -1 page:1
p 5592 2180 100 0 -1 pages:1
p 5256 2180 100 0 -1 revision:0
p 5208 2292 150 768 -1 system:Gemini GNIRS DC
[comments]
