/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME
 *   utility.h
 *
 * PURPOSE
 * Utility data structures and interface
 *
 * HISTORY
 *
 */
#ifndef _UTILITY_H
#define _UTILITY_H

// Include files
#include <regex.h>
#include <sys/stat.h>
#include <float.h>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <string.h>
#include <epicsTime.h>


using namespace std;

//+ LONG LONG definitions - not yet in ANSI standard
#ifndef __LONG_LONG_MAX__
#define __LONG_LONG_MAX__ 9223372036854775807LL
#endif
#ifndef LONG_LONG_MIN
#define LONG_LONG_MIN (-__LONG_LONG_MAX__-1)
#endif
#ifndef LONG_LONG_MAX
#define LONG_LONG_MAX __LONG_LONG_MAX__
#endif

/* Maximum value an `unsigned long long int' can hold.  (Minimum is 0).  */
#ifndef ULONG_LONG_MAX
#define ULONG_LONG_MAX (LONG_LONG_MAX * 2ULL + 1)
#endif


//+ defines and constants
const unsigned FNAMELEN=255;  // Size of filename string
const unsigned MSGLEN=255;    // Size of message string
const unsigned UNAMELEN=25;   // Size of username string

//+ Useful MACROS
//+ mask for bytes 1 | 3
#define m13 0xff00ff00
//+ mask for bytes 2 | 4
#define m24 0xff00ff
//+ mask for byte 2,3 for 4 byte integers
#define m2 0xff0000
#define m3 0xff00
//+ mask for bytes 2,3,4,5,6,7 for long long integers
#define ml2 0xff000000000000
#define ml3 0xff0000000000
#define ml4 0xff00000000
#define ml5 0xff000000
#define ml6 0xff0000
#define ml7 0xff00

//+ definitions to swap bytes in short, long ints and long long ints
//+ NB Make sure bit operations are type cast to unsigned values
#define sbyte_swap(i) ((unsigned short)(i)<<8 | (unsigned short)(i)>>8)
#define lbyte_swap(i) (((unsigned int)(i)<<24)|((unsigned int)(i)<<8&m2)|((unsigned int)(i)>>8&m3)|((unsigned int)(i)>>24))
#define left_half(i) ((unsigned long long int)(i)<<56)|((unsigned long long int)(i)<<40&ml2)|((unsigned long long int)(i)<<24&ml3)|((unsigned long long int)(i)<<8&ml4)
#define right_half(i) ((unsigned long long int)(i)>>56)|((unsigned long long int)(i)>>40&ml7)|((unsigned long long int)(i)>>24&ml6)|((unsigned long long int)(i)>>8&ml5)
#define llbyte_swap(i) (left_half(i)|right_half(i))

//+ TRUE and FALSE get defined so many times,
//+ lets not get in the way of other definitions.
#if !defined(TRUE) || ((TRUE) != 1)
#undef TRUE
#define TRUE    (1)
#endif
#if !defined(FALSE) || ((FALSE) != 0)
#undef FALSE
#define FALSE   (0)
#endif
#if     !defined(ERR) || ((ERR) != -1)
#undef ERR
#define ERR     (-1)
#endif
#if     !defined(OK) || ((OK) != 0)
#undef OK
#define OK      (0)
#endif

#define MIN(m1,m2) ((m1<m2)? (m1):(m2))
#define MAX(m1,m2) ((m1>m2)? (m1):(m2))

//+ Ownership permissions
const mode_t RWOWN = S_IRUSR|S_IWUSR;
const mode_t RWALL = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH;

//+ Data ranges
const char MINC = -128;
const char MAXC = 0x7f;
const unsigned char MINUC = 0x0;
const unsigned char MAXUC = 0xff;
const short MINS = -32768;
const short MAXS = 32767;
const unsigned short MINU = 0;
const unsigned short MAXU = 65535;
const int MINI = 0x80000000;
const int MAXI = 0x7fffffff;
const unsigned int MINUI = 0;
const unsigned int MAXUI = 0xfffffffe;
const float MAXF = FLT_MAX;
const float MINF = FLT_MIN;
const double MAXD = DBL_MAX;
const double MIND = DBL_MIN;
const int BYTE_BITPIX = 8;        //+ default output type when converting to a byte type
const int SHORT_BITPIX = 16;      //+ default output type when converting to a short type
const int INT_BITPIX = 32;        //+ default output type when converting to a int type
const int FLOAT_BITPIX = -32;     //+ default output type when converting to a real type
const int DOUBLE_BITPIX = -64;    //+ default output type when converting to a double type

//+ Some commonly used rest times = msec
const int TEN_MSEC = 10;
const int TWENTY_MSEC = 20;
const int FIFTY_MSEC = 50;
const int TENTH_SECOND = 100;
const int QUARTER_SECOND = 250;
const int HALF_SECOND = 500;
const int SECOND = 1000;

// typedefs

//+ Set of byte orderings
enum Byte_Order_Type {
  WORD_BIG_ENDIAN,
  WORD_LITTLE_ENDIAN
};

//+ Gemini debug levels
enum Debug_Level {
  DEBUG_NOT_SET,
  NO_LOG,
  ALWAYS_LOG,
  DEBUG_NONE,
  DEBUG_MIN,
  DEBUG_STATE_G,
  DEBUG_CALCS_G,
  DEBUG_SPECIAL_G,
  DEBUG_SDSU,
  DEBUG_FULL
};

typedef char Fnametype[FNAMELEN];
typedef char Msgtype[MSGLEN];
typedef char Unametype[UNAMELEN];

// Class definitions

//+ Swap byte function for doubles
// > d - value to byte swap
double dbyte_swap(double d);

//+ Swap byte function for floats
// > f - value to byte swap
double fbyte_swap(float f);

//+ Function to return the byte ordering of the current machine
Byte_Order_Type byte_order();


/*
CLASS
    Tokenizer
    String tokenizer.
DESCRIPTION
  String tokenizer.
  It derives from the implementation of the Rogue Wave
  RWTokenizer. It intrinsically uses STL string. (From GEANT software library)
KEYWORDS
    Log
*/
class Tokenizer
{
private:
  string string2tokenize;  //+ actual string to tokenise
  size_t actual;           //+ current position
public:
  //+ Constructor
  Tokenizer(const string& s):string2tokenize(s),actual(0){}

  //+ Operator to return the next token
  // > str - delimeter string
  // > l - length of delimeter to consider - default 0 means all
  string operator()(const string str=" \t\n", size_t l=0);

};

// global function definitions

//+ template quicksort function for multiple data types
// > n - number of items to sort
// ! data - array to sort
template<typename T> void qsort(int n, T *data);

//+ template quicksort function for multiple data types with associated data
// > n - number of items to sort
// ! data - array to sort
// ! data2 - array to keep aligned with data
template<typename T> void qsort2(int n, T *data, T *data2);

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: template<typename T> void qqs
 *
 * INVOCATION: qqs(int first, int last, T *data)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > first - first index into array
 * > last - last index into array
 * ! data - data array of T type
 *
 * FUNCTION VALUE: Byte_Order_Type
 *
 * PURPOSE: Qsort - recursive sorting routine
 *
 * DESCRIPTION: Template function that does work
 *
 * EXTERNAL VARIABLES: None.
 *
 * PRIOR REQUIREMENTS: None.
 *
 * DEFICIENCIES:
 *
 *-------------------------------------------------------------------------*/
template<typename T> void qqs(int first, int last, T *data)
{
  T temp;

  int i = first;
  int j = last;
  T vmid = data[(i + j)/2];
  do {
    while (data[i] < vmid) i++;
    while (vmid < data[j]) j--;
    if (i <= j) {
      temp = data[i];
      data[i] = data[j];
      data[j] = temp;
      i++;
      j--;
    }
  } while (i <= j);

  if (first < j) qqs(first, j, data);
  if (i < last) qqs(i, last, data);
}


/*+--------------------------------------------------------------------------
 * FUNCTION NAME: template<typename T> void qsort
 *
 * INVOCATION: qsort(int n, T *data)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > n - number of ele3ments in array
 * ! data - data array of T type
 *
 * FUNCTION VALUE: Byte_Order_Type
 *
 * PURPOSE: Qsort - recursive sorting routine
 *
 * DESCRIPTION: Template function calls qqs to do actual sorting
 *
 * EXTERNAL VARIABLES: None.
 *
 * PRIOR REQUIREMENTS: None.
 *
 * DEFICIENCIES:
 *
 *-------------------------------------------------------------------------*/
template<typename T> void qsort(int n, T *data)
{
  qqs(0, n-1, data);
}
/*  template void qsort<int>(int n, int* data); */
/*  template void qsort<float>(int n, float* data); */

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: template<typename T> void qqs
 *
 * INVOCATION: qqs(int first, int last, T *data)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > first - first index into array
 * > last - last index into array
 * ! data - data array of T type
 *
 * FUNCTION VALUE: Byte_Order_Type
 *
 * PURPOSE: Qsorts an associate array in line with data array
 *
 * DESCRIPTION: Template function that does work
 *
 * EXTERNAL VARIABLES: None.
 *
 * PRIOR REQUIREMENTS: None.
 *
 * DEFICIENCIES:
 *
 *-------------------------------------------------------------------------*/
template<typename T> void qqs2(int first, int last, T *data, T *data2)
{
  T temp;

  if (data2==NULL) return;
  int i = first;
  int j = last;
  T vmid = data[(i + j)/2];
  do {
    while (data[i] < vmid) i++;
    while (vmid < data[j]) j--;
    if (i <= j) {
      temp = data[i];
      data[i] = data[j];
      data[j] = temp;
      temp = data2[i];
      data2[i] = data2[j];
      data2[j] = temp;
      i++;
      j--;
    }
  } while (i <= j);

  if (first < j) qqs2(first, j, data, data2);
  if (i < last) qqs2(i, last, data, data2);
}


/*+--------------------------------------------------------------------------
 * FUNCTION NAME: template<typename T> void qsort2
 *
 * INVOCATION: qsort2(int n, T *data, T *data2)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > n - number of ele3ments in array
 * ! data - data array of T type
 * ! data2 - associate data array of T type
 *
 * FUNCTION VALUE: Byte_Order_Type
 *
 * PURPOSE: Qsort2 - recursive sorting routine that sorts two arrays
 *
 * DESCRIPTION: Template function calls qqs to do actual sorting
 *
 * EXTERNAL VARIABLES: None.
 *
 * PRIOR REQUIREMENTS: None.
 *
 * DEFICIENCIES:
 *
 *-------------------------------------------------------------------------*/
template<typename T> void qsort2(int n, T *data, T *data2)
{
  qqs2(0, n-1, data, data2);
}
/*  template void qsort2<int>(int n, int *data, int *data2); */
/*  template void qsort2<float>(int n, float *data, float *data2); */

//+ function to sleep for interval
// > interval - sleep time in msecs
int rest(int interval);
//+ function to sleep for interval
// > interval - sleep time in µsecs
int rest_micro(int interval);

//+ functions to provide a high resolution timestamps
long timestamp_res();

//+ Implement ! for epicsTime
bool operator!(const epicsTime&);
//+ Implement / for epicsTime
double operator/(const epicsTime&, double);

void reset_timestamp(epicsTime&);

//+ Get the current UT (uses UTC time system), returns string of form HH:MM:SS.s
char* get_current_ut(double& rawt, double& tai, char* buffer = NULL, int len = 0);
std::string to_ut_date(epicsTime timestamp);
std::string to_ut_time(epicsTime timestamp);

std::string left_justify(string s, int size, char fillchar);

//+ Stringtonum functions for different data types
// > str - string to convert
// < val - value found
void stringtonum(const char* str, short& val);
void stringtonum(const char* str, unsigned short& val);
void stringtonum(const char* str, int& val);
void stringtonum(const char* str, unsigned int& val);
void stringtonum(const char* str, long& val);
void stringtonum(const char* str, unsigned long& val);
void stringtonum(const char* str, unsigned long long& val);
void stringtonum(const char* str, long long& val);
void stringtonum(const char* str, float& val);
void stringtonum(const char* str, double& val);

//+ list parsing routine
// > list - list to parse
// < vlist - values parsed from list
// > n - number to look for
template<typename T> int parse_list(string list, T* vlist, int n);

//+ Strip leading and trailing whitespace from string
// ! str - input/output string
// > stripType - where to strip from
// > whitespace character - default = ' '
enum Strip_Type {STRIP_LEADING, STRIP_TRAILING, STRIP_BOTH};
void strip_str(string& str, Strip_Type stripType = STRIP_LEADING, char c = ' ');

string str_tolower(string);
string str_toupper(string);

//+ Converts any value to a string
template <typename T> string to_string(const T& value);

//+ To provide regular expression searches to STL string objects.
// From code by Richard Kernan  http://userpages.umbc.edu/~rkerna1/Courses/stl_regex.html
// > str - input string to match
// > pat - regular expressin pattern
int regx_match(const string &str, const string &pat, int& len);

int nint(double d);
int nint(float f);

/*
 *+
 * FUNCTION NAME: to_string
 *
 * INVOCATION: to_string(T& value)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > value - the value to return as a string
 *
 * FUNCTION VALUE: value as a string
 *
 * PURPOSE: Return the given value as a string
 *
 * DESCRIPTION: Uses an ostringstream to translate the given value
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 * The value passed must be of a type that supports operator<<
 *
 * DEFICIENCIES:
 *
 *-
 */
template <typename T> string to_string(const T& value)
{
  string buf;
  ostringstream ostr;
  ostr << value << ends;
  buf = ostr.str();
  // Unfreeze the stream so that it can be automatically freed
  // ostr.rdbuf()->freeze(0);
  return buf;
}

size_t strlcpy(char*, const char*, size_t);
size_t strlcpy(char*, const string&, size_t);
size_t strlcat(char*, const char*, size_t);
size_t strlcat(char*, const string&, size_t);

#endif //_UTILITY_H
