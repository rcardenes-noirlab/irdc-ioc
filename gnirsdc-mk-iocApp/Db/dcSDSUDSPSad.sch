[schematic2]
uniq 124
[tools]
[detail]
w 984 1019 100 0 n#121 esirs.SDSUcmdDone.FLNK 880 1168 912 1168 912 1008 1104 1008 esirs.SDSUcmdAsciiO.SLNK
w 912 1147 100 0 n#120 esirs.SDSUcmdDone.VAL 880 1136 992 1136 992 1168 1104 1168 esirs.SDSUcmdAsciiO.INP
w -864 -597 100 0 n#106 esirs.lastReply.VAL -896 -608 -784 -608 -784 -576 -672 -576 esirs.lastReplyHex.INP
w -792 -725 100 0 n#105 esirs.lastReply.FLNK -896 -576 -864 -576 -864 -736 -672 -736 esirs.lastReplyHex.SLNK
w -792 -309 100 0 n#104 esirs.lastHeader.FLNK -896 -160 -864 -160 -864 -320 -672 -320 esirs.lastHeaderHex.SLNK
w -864 -181 100 0 n#103 esirs.lastHeader.VAL -896 -192 -784 -192 -784 -160 -672 -160 esirs.lastHeaderHex.INP
w -864 267 100 0 n#100 esirs.exptReply.VAL -896 256 -784 256 -784 288 -672 288 esirs.exptReplyHex.INP
w -792 139 100 0 n#99 esirs.exptReply.FLNK -896 288 -864 288 -864 128 -672 128 esirs.exptReplyHex.SLNK
w -792 587 100 0 n#98 esirs.exptHeader.FLNK -896 736 -864 736 -864 576 -672 576 esirs.exptHeaderHex.SLNK
w -864 715 100 0 n#97 esirs.exptHeader.VAL -896 704 -784 704 -784 736 -672 736 esirs.exptHeaderHex.INP
w -792 1035 100 0 n#95 esirs.SDSUcommand.FLNK -896 1184 -864 1184 -864 1024 -672 1024 esirs.SDSUcmdAscii.SLNK
w -864 1163 100 0 n#94 esirs.SDSUcommand.VAL -896 1152 -784 1152 -784 1184 -672 1184 esirs.SDSUcmdAscii.INP
s 928 -1008 500 512 GNIRS DC SDSU Status/Alarm Database
[cell use]
use esirs 464 919 100 0 SDSUcmdDone
xform 0 672 1072
p 544 880 100 0 1 DESC:SDSU DSP command executed
p 544 848 100 0 1 FTVL:LONG
p 544 816 100 0 1 PV:$(sadtop)
use esirs 1104 919 100 0 SDSUcmdAsciiO
xform 0 1312 1072
p 1104 864 100 0 1 DESC:ASCII string repr. of SDSU command
p 1104 832 100 0 1 FTVL:STRING
p 1104 800 100 0 1 PV:$(sadtop)
p 1200 960 100 0 1 SNAM:dcDSPAsciiCmd
use esirs -64 935 100 0 VBIASGATE
xform 0 144 1088
p 16 896 100 0 1 DESC:SDSU DSP command
p 16 864 100 0 1 FTVL:DOUBLE
p 16 832 100 0 1 PV:$(sadtop)
use esirs -1312 935 100 0 SDSUcommand
xform 0 -1104 1088
p -1232 896 100 0 1 DESC:SDSU DSP command
p -1232 864 100 0 1 FTVL:LONG
p -1232 832 100 0 1 PV:$(sadtop)
use esirs -1312 -409 100 0 lastHeader
xform 0 -1104 -256
p -1232 -448 100 0 1 DESC:SDSU last header
p -1232 -480 100 0 1 FTVL:LONG
p -1232 -512 100 0 1 PV:$(sadtop)
use esirs -1312 39 100 0 exptReply
xform 0 -1104 192
p -1232 0 100 0 1 DESC:SDSU expected reply
p -1232 -32 100 0 1 FTVL:LONG
p -1232 -64 100 0 1 PV:$(sadtop)
use esirs -1312 487 100 0 exptHeader
xform 0 -1104 640
p -1232 448 100 0 1 DESC:SDSU expected header
p -1232 416 100 0 1 FTVL:LONG
p -1232 384 100 0 1 PV:$(sadtop)
use esirs -1312 -825 100 0 lastReply
xform 0 -1104 -672
p -1232 -864 100 0 1 DESC:SDSU last reply
p -1232 -896 100 0 1 FTVL:LONG
p -1232 -928 100 0 1 PV:$(sadtop)
use esirs -672 935 100 0 SDSUcmdAscii
xform 0 -464 1088
p -672 880 100 0 1 DESC:ASCII string repr. of SDSU command
p -672 848 100 0 1 FTVL:STRING
p -672 816 100 0 1 PV:$(sadtop)
p -576 976 100 0 1 SNAM:dcDSPAsciiCmd
use esirs -672 487 100 0 exptHeaderHex
xform 0 -464 640
p -672 432 100 0 1 DESC:HEX string representation of SDSU word
p -672 400 100 0 1 FTVL:STRING
p -672 368 100 0 1 PV:$(sadtop)
p -576 528 100 0 1 SNAM:dcDSPHeaderHex
use esirs -672 39 100 0 exptReplyHex
xform 0 -464 192
p -672 -16 100 0 1 DESC:HEX string representation of SDSU word
p -672 -48 100 0 1 FTVL:STRING
p -672 -80 100 0 1 PV:$(sadtop)
p -576 80 100 0 1 SNAM:dcDSPReplyHex
use esirs -672 -409 100 0 lastHeaderHex
xform 0 -464 -256
p -672 -464 100 0 1 DESC:HEX string representation of SDSU word
p -672 -496 100 0 1 FTVL:STRING
p -672 -528 100 0 1 PV:$(sadtop)
p -576 -368 100 0 1 SNAM:dcDSPHeaderHex
use esirs -672 -825 100 0 lastReplyHex
xform 0 -464 -672
p -672 -880 100 0 1 DESC:HEX string representation of SDSU word
p -672 -912 100 0 1 FTVL:STRING
p -672 -944 100 0 1 PV:$(sadtop)
p -576 -784 100 0 1 SNAM:dcDSPReplyHex
use esirs -64 487 100 0 DRAIN
xform 0 144 640
p 16 448 100 0 1 DESC:SDSU DSP command
p 16 416 100 0 1 FTVL:DOUBLE
p 16 384 100 0 1 PV:$(sadtop)
use esirs -64 39 100 0 VDDA
xform 0 144 192
p 16 0 100 0 1 DESC:SDSU DSP command
p 16 -32 100 0 1 FTVL:DOUBLE
p 16 -64 100 0 1 PV:$(sadtop)
use esirs 1056 -409 100 0 CELLDRAIN
xform 0 1264 -256
p 1136 -448 100 0 1 DESC:SDSU DSP command
p 1136 -480 100 0 1 FTVL:DOUBLE
p 1136 -512 100 0 1 PV:$(sadtop)
use esirs 480 487 100 0 VRESET
xform 0 688 640
p 560 448 100 0 1 DESC:SDSU DSP command
p 560 416 100 0 1 FTVL:DOUBLE
p 560 384 100 0 1 PV:$(sadtop)
use esirs 480 39 100 0 SUB
xform 0 688 192
p 560 0 100 0 1 DESC:SDSU DSP command
p 560 -32 100 0 1 FTVL:DOUBLE
p 560 -64 100 0 1 PV:$(sadtop)
use esirs -64 -409 100 0 DSUB
xform 0 144 -256
p 16 -448 100 0 1 DESC:SDSU DSP command
p 16 -480 100 0 1 FTVL:DOUBLE
p 16 -512 100 0 1 PV:$(sadtop)
use esirs -64 -825 100 0 VBIASPWR
xform 0 144 -672
p 16 -864 100 0 1 DESC:SDSU DSP command
p 16 -896 100 0 1 FTVL:DOUBLE
p 16 -928 100 0 1 PV:$(sadtop)
use esirs 480 -409 100 0 OFFSET7
xform 0 688 -256
p 560 -448 100 0 1 DESC:SDSU DSP command
p 560 -480 100 0 1 FTVL:DOUBLE
p 560 -512 100 0 1 PV:$(sadtop)
use esirs 480 -825 100 0 OFFSET15
xform 0 688 -672
p 560 -864 100 0 1 DESC:SDSU DSP command
p 560 -896 100 0 1 FTVL:DOUBLE
p 560 -928 100 0 1 PV:$(sadtop)
use esirs 1056 487 100 0 OFFSET23
xform 0 1264 640
p 1136 448 100 0 1 DESC:SDSU DSP command
p 1136 416 100 0 1 FTVL:DOUBLE
p 1136 384 100 0 1 PV:$(sadtop)
use esirs 1056 39 100 0 OFFSET31
xform 0 1264 192
p 1136 0 100 0 1 DESC:SDSU DSP command
p 1136 -32 100 0 1 FTVL:DOUBLE
p 1136 -64 100 0 1 PV:$(sadtop)
use bc200tr -1504 -1192 -100 0 frame
xform 0 176 112
p 1072 -1024 100 0 1 author:Mark Jarnyk
p 1296 -1040 100 0 -1 border:C
p 1072 -1056 100 0 1 checked:Peter Young
p 1328 -1040 100 0 -1 date:2001-10-12
p 1312 -912 100 0 -1 project:Gemini GNIRS DC
p -1504 -1144 100 0 0 revision:1.1
p 1312 -976 150 0 -1 title:dcSDSUDSPSad
[comments]
