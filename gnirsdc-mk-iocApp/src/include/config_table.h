/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME config_table.h
 * 
 * GENERAL DESCRIPTION
 *  Configuration classes definition - uses generic table class as base.
 * RSAA's model of astronomical instrumentation is highly configurable. This
 * configurability is achieved through the use of configuration tables with sets
 * of parameter value pairs. This set of classes handles each of the configuration
 * tables for each component of an instrument.
 * 
 */

#ifndef _CONFIG_TABLE_H
#define _CONFIG_TABLE_H

#include "table.h"
#include "configuration.h"

// Constants and typedefs
#include "config_table_consts.h"

//+ class declarations 
/*-------------------------------------------------------------------------
CLASS
    Config_Table

DESCRIPTION  
    Base configuration table - derived from ordinary table. Provides put and get methods
 
KEYWORDS
    Config_Table Table
-------------------------------------------------------------------------*/
class Config_Table:public Table {
public:
  
  //+ Constructor 
  // > name - name of configuration table file
  // > dp - pointer to item cache
  // > dps - pointer to item cache save
  // > sz - size of data cache
  Config_Table(const string name, const void* dp=NULL, const void* dps=NULL, const int sz=0);
  //+ Constructor 
  // > name - name of configuration table file
  // > il - items per line to be written out
  // > ns - sort entries by name (or not)
  // > dp - pointer to item cache
  // > dps - pointer to item cache save
  // > sz - size of data cache
  Config_Table(const string name, const int il, const bool ns, const void* dp=NULL, const void* dps=NULL, const int sz=0);

  //+ Destructor
  virtual ~Config_Table();  

  // GROUP:   Public member Functions.

  //+ read table from file
  // > tname - name of table
  virtual void read(const string tname);

  //+ Write table to file
  // > tname - name of table
  // > do_owned_tables - flag set if tables contained in this one are to be written
  virtual void write(const string tname, bool do_owned_tables = true);

  //+ add a setup_desc, data is pointer to desc
  // > data_item - pointer to data to insert into table
  // > pos - position in table for insertion
  virtual void put(void* data_item, int pos=999) = 0;    

  //+ Get an item from the table and make current
  // > item - description of item to get
  virtual void get(Table_Item *item) = 0;   

  //+ Remove an item from table with name
  // > name - name of item to remove
  virtual int remove(const string name);

  //+ get a table element by index into table
  // > idx - item index
  virtual void get_element(int idx);

  //+ get a table element by name
  // > item_name - name of item to get
  virtual void get_element(const char *item_name);

};


/*-------------------------------------------------------------------------
CLASS
    Chip_Table

DESCRIPTION  
    Detector chip configuration table - derived from config table. Provides put
    and get methods for chip descriptions.
 
KEYWORDS
    Chip_Table Config_Table Table
-------------------------------------------------------------------------*/
class Chip_Table:public Config_Table  {
private:
  // GROUP:   Private member vars
  Chip_Desc chip;                       //+ buffer holding recently accessed desc
  Chip_Desc chip_save;                  //+ saved buffer holding recently accessed desc

public:

  //+ Constructor
  Chip_Table();

  //+ Destructor
  ~Chip_Table();  

  // GROUP:   Public member functions.

  //+ add a Chip_desc to table
  // > data - pointer to data to insert into table
  // > pos - position in table for insertion
  void put(void* data_item,int pos=999);    

  //+ Get a CHIP description from the table and make current
  // > item - description of item to get
  void get(Table_Item *item);           

  //+ returns desc of idx can be an lvalue
  // > idx - index into table
  // > item_name - name of item to fetch
  Chip_Desc& operator[](int idx);       
  Chip_Desc& operator[](const char* item_name);
};


/*-------------------------------------------------------------------------
CLASS
    Controller_Table

DESCRIPTION  
    Detector Controller configuration table - derived from config
    table. Provides put and get methods for stdcontroller descriptions.
 
KEYWORDS
    Controller_Table Config_Table Table
-------------------------------------------------------------------------*/
class Controller_Table:public Config_Table {
private:
  // GROUP:   Private member vars
  Controller_Desc controller;        //+ Controller description
  Controller_Desc controller_save;   //+ Saved controller description

  // GROUP:   Private member functions

  //+ Calculate the size of detector and the chip mask
  // > tcontroller - controller description to use for calculation
  void calc_size_and_mask(Controller_Desc& tcontroller);

public:
  //+ Constructor
  Controller_Table();

  //+ Destructor
  ~Controller_Table();  

  // GROUP:   Public member variables.
  Chip_Table chip_table;                    //+ available CHIPs

  // GROUP:   Public member functions.

  //+ Specific table read
  // > tname - name od table
  void read(const string tname);         

  //+ Specific table write
  // > tname - name of table
  // > do_owned_tables - flag set if required to write owned tables
  void write(const string tname, bool do_owned_tables = true);            

  //+ add a desc, data is pointer to desc
  // > data_item - pointer to data to insert into table
  // > pos - position in table for insertion
  void put(void* data_item, int pos=999);

  //+ gets a desc
  // > item - description of item to get
  void get(Table_Item *item); 

  //+ returns desc of idx can be an lvalue       
  // > idx - index into table
  // > item_name - name of item to fetch
  Controller_Desc& operator[](int idx);
  Controller_Desc& operator[](const char* item_name);
};



/*-------------------------------------------------------------------------
CLASS
    Camera_Table

DESCRIPTION  
    Camera configuration table - derived from config table. Provides
    put and get methods for stdcamera descriptions.
 
KEYWORDS
    Camera_Table Config_Table Table
-------------------------------------------------------------------------*/
class Camera_Table:public Config_Table {
private:
  // GROUP:   Private member vars
  Camera_Desc camera;                   //+ Camera description
  Camera_Desc camera_save;              //+ Camera description

  // GROUP:   Private member functions

  //+ computes total size of camera
  // > tcamera - camera description to use
  void calc_rows_cols(Camera_Desc& tcamera); 
public:
  //+ Constructor
  Camera_Table();

  //+ Destructor
  ~Camera_Table();  

  // GROUP:   Public member variables.
  Controller_Table controller_table;    //+ available controllers

  // GROUP:   Public member functions.

  //+ Specific table read
  // > tname - name of table
  void read(const string tname);

  //+ Specific table write
  // > tname - name of table
  // > do_owned_tables - flag set if required to write owned tables
  void write(const string tname, bool do_owned_tables = true);

  //+ add a desc, data is pointer to desc
  // > data_item - pointer to data to insert into table
  // > pos - position in table for insertion
  void put(void* data_item, int pos=999);

  //+ gets a desc
  // > item - description of item to get
  void get(Table_Item *item);

  //+ returns desc of idx can be an lvalue       
  // > idx - index into table
  // > item_name - name of item to fetch
  Camera_Desc& operator[](int idx);
  Camera_Desc& operator[](const char* item_name);
};

/*-------------------------------------------------------------------------
CLASS
    Instrument_Table

DESCRIPTION  
    Instrument configuration table - derived from config table. Provides
    put and get methods for stdinstrument descriptions.
 
KEYWORDS
    Instrument_Table Config_Table Table
-------------------------------------------------------------------------*/
class Instrument_Table:public Config_Table {
private:
  // GROUP:   Private member vars
  Instrument_Desc instrument;           //+Instrument data
  Instrument_Desc instrument_save;      //+Instrument data

public:
  //+ Constructor
  Instrument_Table();

  //+ Destructor
  ~Instrument_Table();  

  // GROUP:   Public member variables.
  Camera_Table camera_table;                //+ available cameras

  // GROUP:   Public member functions.

  //+ Specific table read
  // > tname - name of table
  void read(const string tname);

  //+ Specific table write
  // > tname - name of table
  // > do_owned_tables - flag set if required to write owned tables
  void write(const string tname, bool do_owned_tables = true);

  //+ add a desc, data is pointer to desc
  // > data_item - pointer to data to insert into table
  // > pos - position in table for insertion
  void put(void* data_item, int pos=999);

  //+ gets a desc
  // > item - description of item to get
  void get(Table_Item *item);

  //+ returns desc of idx can be an lvalue       
  // > idx - index into table
  // > item_name - name of item to fetch
  Instrument_Desc& operator[](int idx);
  Instrument_Desc& operator[](const char* item_name);
};



// Other function definitions

#endif //_CONFIG_TABLE_H 
