#ifndef __LOGGING_H__
#define __LOGGING_H__

#include <errlog.h>

#if defined(__cplusplus)

#include <string>

#define LEVEL_VALUES_LIST \
	X(NoLog, "NOLOG") \
	X(None,  "NONE")  \
	X(Min,   "MIN") \
	X(Full,  "FULL")

namespace logger {

#define X(name, text) name,
	enum class Level {
		LEVEL_VALUES_LIST
	};
#undef X

	Level getLevel() noexcept;
	void setLevel(Level debug);
	Level getDefaultLevel() noexcept;
	const char* to_string(Level debug) noexcept;

	void message(Level log_level, const std::string msg);
	void message(const std::string msg);

	template<typename ... Args>
	void message(Level log_level, const std::string format, Args const & ... args) noexcept
	{
		if (log_level > getLevel())
			return;

		errlogPrintf((format + "\n").c_str(), args...);
	}

}

#else  // !defined(__cplusplus)

void logger_message

#endif // __cplusplus

#endif // __LOGGING_H__
