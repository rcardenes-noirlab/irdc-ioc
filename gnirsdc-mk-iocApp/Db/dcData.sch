[schematic2]
uniq 9
[tools]
[detail]
w 18 1259 -100 0 CLID inhier.CLID.P -32 1344 16 1344 16 1248 80 1248 eapply.dataApply.CLID
w 538 1099 100 0 n#1 eapply.dataApply.OCLB 464 1088 672 1088 672 1024 864 1024 dcDataObs.dcDataObs#124.CLID
w 538 1163 100 0 n#2 eapply.dataApply.OCLA 464 1152 672 1152 672 1312 864 1312 dcDataView.dcDataView#123.CLID
w 490 1451 -100 0 VAL outhier.VAL.p 560 1440 480 1440 480 1280 464 1280 eapply.dataApply.VAL
w 522 1371 -100 0 MESS outhier.MESS.p 592 1360 512 1360 512 1248 464 1248 eapply.dataApply.MESS
w -14 1435 -100 0 DIR eapply.dataApply.DIR 80 1280 48 1280 48 1424 -16 1424 inhier.DIR.P
w 530 1643 100 0 n#3 dcDataObs.dcDataObs#124.MESS 1024 1024 1280 1024 1280 1632 -160 1632 -160 1088 80 1088 eapply.dataApply.INMB
w 530 1611 100 0 n#4 dcDataObs.dcDataObs#124.VAL 1024 1056 1248 1056 1248 1600 -128 1600 -128 1120 80 1120 eapply.dataApply.INPB
w 514 1579 100 0 n#5 dcDataView.dcDataView#123.MESS 1024 1312 1184 1312 1184 1568 -96 1568 -96 1152 80 1152 eapply.dataApply.INMA
w 514 1547 100 0 n#6 dcDataView.dcDataView#123.VAL 1024 1344 1152 1344 1152 1536 -64 1536 -64 1184 80 1184 eapply.dataApply.INPA
w 554 1131 100 0 n#7 eapply.dataApply.OUTB 464 1120 704 1120 704 1056 864 1056 dcDataObs.dcDataObs#124.DIR
w 722 1355 100 0 n#8 eapply.dataApply.OUTA 464 1184 640 1184 640 1344 864 1344 dcDataView.dcDataView#123.DIR
s -544 -16 200 0 Separate CADs are maintained for View and Obs modes
s -550 60 200 0 This is the top level schematic for the GNIRS data parameters
[cell use]
use inhier -48 1303 100 0 CLID
xform 0 -32 1344
use inhier -32 1383 100 0 DIR
xform 0 -16 1424
use outhier 528 1399 100 0 VAL
xform 0 544 1440
use outhier 560 1319 100 0 MESS
xform 0 576 1360
use dcDataObs 864 903 100 0 dcDataObs#124
xform 0 944 1008
use dcDataView 864 1191 100 0 dcDataView#123
xform 0 944 1296
use eapply 156 1364 150 0 dataApply
xform 0 272 1008
use eborderC -664 -405 100 0 eborderC#0
xform 0 1016 900
p 1912 -252 100 768 -1 author:Peter Young
p 1904 -132 40 0 -1 comment3:2001-02-02 Modified for GNIRS. Peter Young
p 1912 -284 100 768 -1 date:2001-02-02
p 2136 -204 200 768 -1 file:dcData.sch
p 2408 -252 100 0 -1 page:1
p 2520 -252 100 0 -1 pages:1
p 2184 -252 100 0 -1 revision:0
p 2136 -140 150 768 -1 system:Gemini GNIRS DC
[comments]
