/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: astrovme_io.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GNIRS PROJECT
 * 
 * FILENAME astrovme_impl.h
 * 
 * GENERAL DESCRIPTION 
 *
 * This is a VxWorks version of the device driver to communicate with
 * the SDSU VME interface board). 
 *
 * This file contains I/O control commands for the VxWorks device driver for the
 * SDSU VME Interface Card. These commands are for use with ioctl().
 * 
 * Author: peter Young (RSAA) - For the Gemini Telescope Project (GSAOI) - December 2003
 *
 * $Log: astrovme_io.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.2  2005/02/25 04:45:56  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.1  2004/12/22 22:43:18  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.2  2004/08/20 02:18:25  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.1  2004/04/25 02:03:31  pjy
 * Pre T2.2 version
 *
 * 
 */
#ifndef __ASTROVME_IO_H__
#define __ASTROVME_IO_H__

#ifndef _ASTRO_COMMON_IOCTL
#define _ASTRO_COMMON_IOCTL
/*
 * Information retrieval commands
*/
#define ASTRO_READ_STATUS            0xA01
#define ASTRO_GET_PROGRESS	     0xA02
#define ASTRO_READS_DONE	     0xA03
#define ASTRO_READ_RUNNING	     0xA04
#define ASTRO_GET_SETUP 	     0xA05
#define ASTRO_READ_TIME              0xA06
#define ASTRO_GET_RDC 	             0xA07
#define ASTRO_PRINT_REPLY_BUF        0xA08

/*
 * Action commands
 */
#define ASTRO_COMMAND		     0xA015
#define ASTRO_STOP     	             0xA016
#define ASTRO_RESET     	     0xA017
#define ASTRO_ABORT    	             0xA018

/*
 * Setup commands
 */
#define ASTRO_SET_TIMEOUT     	     0xA107
#define ASTRO_SETUP	 	     0xA110
#define ASTRO_SIMULATE		     0xA111
#define ASTRO_DEBUG		     0xA112

/*
 * Generic Constants
 */
#define ASTRO_CMD_MAX		     6
#endif 

/*
 * DSP Fifo Check Constants
 */
#define ASTRO_BUSY_WAIT_DELAY	     1		/* ticks */
#define ASTRO_READ_WAIT_TIMEOUT      10.0       /* 10 s */
#define ASTRO_READ_WAIT_DELAY        1          /* ticks */

#ifndef _ASTRO_SETUP
#define _ASTRO_SETUP
/* 
 * some setup parameters 
 */
typedef struct {
  uint32_t buffer_size;                 /* Buffer size in bytes */
  uint32_t image_size;                  /* image size in bytes */
  uint32_t nreads;                      /* Number of read sets in this exposure */
  uint32_t nsamples;                    /* Number of samples in a read set */
  uint32_t read_interval;               /* Time interval between read sets - msec */
  uint32_t read_time;                   /* Time to read one frame - msec */
  uint32_t nresets;                     /* Number of resets to perform */
  uint32_t reset_delay;                 /* Time interval after reset - msec */
  uint32_t namps;                       /* Number of amps in readout */
} Astro_Setup;
#endif

#ifndef _ASTRO_READOUT_STATUS
#define _ASTRO_READOUT_STATUS
/*
 * Device driver readout status structure
 */
typedef struct {
  uint64_t bytes_remaining;             /* bytes remaining in full read */
  uint32_t readouts_done;               /* readouts done so far in multiple IR read */
  int32_t read_running;                 /* number of read running */
  uint64_t last_readout_time;           /* time required for last readout (seconds)*/
} Astro_Readout_Status;
#endif


#endif

