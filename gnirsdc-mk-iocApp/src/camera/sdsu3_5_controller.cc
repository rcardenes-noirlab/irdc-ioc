/*
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME sdsu_controller.cc
 *
 * GENERAL DESCRIPTION

 *  SDSU_CONTROLLER class implementation and functions.  This class is used to
 *  interface to a generic SDSU detector controller.  Classes derived off this
 *  are used to interface directly to a specific generation of SDSU detector
 *  controller and therefore contain all specific knowledge of that particular
 *  controller.

 *  This class provides functions for sending commands to the SDSU controller,
 *  reading back the responses and data, defining various detector control
 *  parameters, reading and writing DSP memory, downloading and uploading
 *  configuration files. Further details of the SDSU controller hardware,
 *  firmware and DSP interface are contained in the "SDSU CCD Controller User's
 *  Manual",
 *
 * Acknowledgement:
 *  This class has code developed from the original SDSU sbustool code from Bob
 *  Leach for the SBUS interface. It also has some code developed from the
 *  Gemini SdsuLib for the VME interface device driver. These code segments have
 *  been reworked to fit into the RSAA camera/controller C++ class framework.
 *
 */

/*---------------------------------------------------------------------------
  Include files
  --------------------------------------------------------------------------*/

#include <sys/types.h>
#include <math.h>
#include "camera.h"
#include "common.h"
#include "sdsu3_5_controller.h"
#include "sdsu_setup.h"
#include "sdsu_volts.h"
#include "detector_regions.h"
#include "ir_setup.h"
#include "ir_camera.h"
#include "epicsTime.h"
#include <chrono>
#include "logging.h"
#include <list>
#include <CArcPCI.h>
#include <CArcPCIe.h>
#include <unordered_map>
#include <map>
#include <epicsExport.h>
#include "dc.h"

using namespace std;
using namespace arc::device;

const auto REST_PERIOD = 200; // In microseconds
const auto TIME_SAMPLING = "TIME_SAMPLES";
const auto RAW_COLS = "RAW_COLS";
const auto RAW_ROWS = "RAW_ROWS";
const auto BYTES_PER_PIXEL = 2;

extern "C" {
static int sdsu_simulation = 0;
}

extern IR_Camera* ir_camera;

struct ModeConfig {
	std::string label;
	std::string lod_file;
	bool full_frame;
	int nrows;
	int ncols;
	long lrns;   // * 2, because the same amount of Fowler Samples are taken before and after exposure.
	long ndavgs;
	float exposure;

	uint32_t rows_per_sample() const {
		return 512;
	}

	uint32_t cols_per_sample() const {
		return (full_frame ? 2048 : 512) * ndavgs;
	}
};

static map<ModeIdx, ModeConfig> setups{
	{{IR_VERY_BRIGHT_READOUT, IR_1_QUADRANT}, {"Very Bright Object", "VeryBrightObject.lod", false, 2048, 512, 1, 1}},
	{{IR_BRIGHT_READOUT,  IR_1_QUADRANT}, {"Bright Object", "BrightObject.lod", false, 1024, 3072, 1, 6}},
	{{IR_FAINT_READOUT,  IR_1_QUADRANT}, {"Faint Object", "FaintObject.lod", false, 16384, 3072, 16, 6}},
	{{IR_VERY_FAINT_READOUT, IR_1_QUADRANT}, {"Very Faint Object", "VeryFaintObject.lod", false, 32768, 3072, 32, 6}},
	{{IR_VERY_FAINT_READOUT, IR_FULL_FRAME}, {"Very Faint Object - Full frame", "VeryFaintFullFrame.lod", true, 32768, 2048 * 6, 32, 6}},
	{{IR_FAINT_READOUT, IR_FULL_FRAME}, {"Faint Object - Full frame", "FaintFullFrame.lod", true, 16384, 2048 * 6, 16, 6}},
	{{IR_BRIGHT_READOUT, IR_FULL_FRAME}, {"Bright Object - Full frame", "BrightFullFrame.lod", true, 1024, 2048 * 6, 1, 6}},
	{{IR_VERY_BRIGHT_READOUT, IR_FULL_FRAME}, {"Very Bright Object - Full frame", "VeryBrightObjectFullFrame.lod", true, 1024, 2048, 1, 1}}
};

/* class declarations */

/*
 *+
 * CLASS
 *   LockGuard
 *   Class to ensure lock consistency
 * DESCRIPTION
 *   RAII-based guard that ensures that a lock is acquired when declared
 *-
 */
class LockGuard {
	public:
		LockGuard(epicsMutex& lock, bool startLocked=false)
			: _lock(lock), locked(false)
		{
			if (startLocked)
				this->lock();
		}

		void lock() {
			if (!locked) {
				locked = true;
				_lock.lock();
			}
		}

		void unlock() {
			if (locked) {
				locked = false;
				_lock.unlock();
			}
		}


		~LockGuard() {
			unlock();
		}
	private:
		epicsMutex& _lock;
		bool locked;
};

/*
 *+
 * CLASS
 *   SdsuImageTransferThread
 *   Thread subclass to perform sdsu readout
 * DESCRIPTION
 *   This readout simulation takes as input a chosen FITS image and then
 *   models a chip readout that clocks out the serial register a pixel at
 *   a time, interleaving pixels from multiple amplifiers (multiple FITS
 *   extensions). Support for binning and arbitrary region selection is
 *   also provided. Overscan pixels (row and col) are also supported and a
 *   constant (arbitrary) bias value is added artificially to the FITS data.
 *   Image transfer routine
 *
 *-
 */

class SdsuImageTransferThread : public Thread {
	public:
		SdsuImageTransferThread(Sdsu3_5_Controller* sc, const string &name,
				bool dolock,
				unsigned int priority=epicsThreadPriorityLow);
		virtual void run();
	private:
		Sdsu3_5_Controller *sdsu;
};

/*---------------------------------------------------------------------------
  Constants
  --------------------------------------------------------------------------*/
const int SDSU_EXTRA_BYTES = 0; // Use when SBUS interface set with DSP code
const std::string Sdsu3_5_Controller::PARAMETER_NAME = "SDSUPar";

/*---------------------------------------------------------------------------
  Functions
  --------------------------------------------------------------------------*/

/*============================================================================
 *  Sdsu3_5_Controller
 *===========================================================================*/
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::Sdsu3_5_Controller
 *
 * INVOCATION: sdsu = new Sdsu3_5_Controller(Camera_Desc& cam, int camid, int c, Platform_Specific* ps)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam - Camera_Desc - description of the camera
 * > camid - camera id
 * > c - controller id
 * > ps - pointer to platform specific data
 *
 * FUNCTION VALUE: Void
 *
 * PURPOSE: Basic constructor for the sdsu Controller
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
	Sdsu3_5_Controller::Sdsu3_5_Controller(Camera_Desc& cam, int camid, int c, Platform_Specific* ps)
:Controller(cam, camid, c, ps),
	pDevice(nullptr),
	current_mode(IR_READOUT_NULL, IR_QUADRANT_NULL)
{
	// Initialise member variables
	Sdsu3_5_Controller::init_vars();
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::Sdsu3_5_Controller()
 *
 * INVOCATION: controller = new Sdsu3_5_Controller(Camera_Desc cam, int camid, int c, const char* param_host,
 *                          Instrument_Status *st, paramMapType *pm)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam - description of camera that is parent of this controller. Also holds
 *         controller description
 * > camid - identification umber of parent camera
 * > c - identification number of this controller within the camera.
 * > param_host - name of host where parameter database resides.
 * > st - pointer to instrument status data structure for all instrument status
 * > pm - pointer to parameter map type
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 * Constructor used for initialising configuration variables and setting up the
 * parameter database
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generic SDSU controller
 * object.  It is never used directly but is called by a derived version of this
 * class that handles a SDSU controller type. A specific controller object
 * is created by a parent camera object.
 *
 * This version of the controller is used just for holding a controllers
 * configuration information and also its status parameter database. It is not
 * used to interface with real hardware. It is, therefore, only useful in
 * modules that need to know about a configuration and be able to read/write
 * status parameters but do not actually interface with hardware.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu3_5_Controller::Sdsu3_5_Controller(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st,
		Platform_Specific* ps)
:Controller(cam, camid, c, param_host, st, ps),
	pDevice(nullptr),
	current_mode(IR_READOUT_NULL, IR_QUADRANT_NULL)
{
	sdsu_status = &controller_status->Controller_Status_u.sdsu;

	// Initialise member variables and add status parameters
	Sdsu3_5_Controller::init_vars();
	add_status_parameters(false);

	// From SDSU3
	controller_statusp->put(CONTROLLER_TYPE, (int) SDSU3_5);
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::Sdsu3_5_Controller()
 *
 * INVOCATION: controller = new Sdsu3_5_Controller(Instrument_Status *st, Parameter* ip, Parameter* cp,
 Parameter* op, int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > st - pointer to instrument status data structure for all instrument status
 * > ip - pointer to the parameter database for the instrument
 * > cp - pointer to the parameter database for the camera
 * > op - pointer to the operation parameter database
 * > deb - debug flag
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Constructor used for interfacing to real controller hardware
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generic SDSU controller
 * object.  It is never used directly but is called by a derived version of this
 * class that handles a specific SDSU controller type. A specific controller object
 * is created by a parent camera object.
 *
 * This version of the controller is used for interfacing with real hardware. It
 * is, therefore, required for components that are attached to the hardware. Its
 * derived classes know how to communicate with specific controllers, while this
 * parent class can be used in a generic way to deal with all controller types.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu3_5_Controller::Sdsu3_5_Controller(Instrument_Status *st, Parameter* ip, Parameter* cp, Parameter* op,
		int deb, Platform_Specific* ps)
:Controller(st,ip,cp,op,deb,ps),
	pDevice(nullptr),
	current_mode(IR_READOUT_NULL, IR_QUADRANT_NULL)
{
	sdsu_status = &controller_status->Controller_Status_u.sdsu;
	sdsu_configp = &current_config->config_data.Config_Data_u.camera.controller.Controller_Config_u.sdsu;
	controller_statusp->put(CONTROLLER_TYPE, (int) current_config->config_data.Config_Data_u.camera.controller.type);

	// Initialise member variables and add status parameters
	Sdsu3_5_Controller::init_vars();

	// Set default sdsu_config to hardware configuration
	sdsu_config = *sdsu_configp;

	// TODO: Figure this one out... memalign is vxWorks stuff, btw
	/*
	// Allocate space for command/reply buffer
	reply_buf
	reply_buf = (unsigned long *) memalign (SDSU_ALIGN, SDSU_MAX_REPLY_ARGS * sizeof (unsigned long));
	if (reply_buf == NULL) {
	throw Error("Unable to allocate aligned memory for SDSU replies", E_ERROR,
	errno, __FILE__, __LINE__);
	}
	*/

	// Set index into reply buffer for circular buffer operations
	last_reply_len = 0;

	do_set_boards();

	// Add in the parameters to the parameter object
	add_status_parameters(true);

	// Initialize objects
	default_request = ASTRO_COMMAND;
	sdsu_request = default_request;
	sdsu_last_header = 0;
	sdsu_last_reply = 0;

	// At startup assume timing code not yet loaded
	timing_loaded = false;

	// Data is unsigned 16 bit
	unsigned_data = TRUE;

	controller_statusp->put(CONTROLLER_TYPE, (int) SDSU3_5);

	// From SDSU2
	open_and_check_interface();

	// From SDSU3
	// TIMING board is used for shutter control
	shutter_control_board = TIM_ID;
}

/*
 *+
 * FUNCTION NAME: do_gain
 *
 * INVOCATION:
 *  do_gain();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Sets controller gain according to configured setting
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::do_gain()
{
	// TODO: Review if this is of any utility to us
	/*
	   int ret_value;
	   message_write("Setting gain...");
	   switch (sdsu_config.config_data.gain_setting) {
	   case GN1:
	   case GN2:
	   case GN4:
	   case GN9:
	   ret_value = sdsu_command(TIM_ID, SDSU_SGN, sdsu_config.config_data.gain_setting,
	   sdsu_config.config_data.tc_setting);
	   if(ret_value != SDSU_OK)
	   message_write("Gain can not be set to %d nor speed to %d!", sdsu_config.config_data.gain_setting,
	   sdsu_config.config_data.tc_setting);
	   else
	   message_write("Gain set to %d, speed set to %d!", sdsu_config.config_data.gain_setting,
	   sdsu_config.config_data.tc_setting);
	   break;
	   default:
	   message_write("Illegal gain setting for this controller - not set to %d",
	   sdsu_config.config_data.gain_setting);
	   }
	   */
	message_write("******* ERROR: Trying to set Gain for this controller - not implemented");
}

/*
 *+
 * FUNCTION NAME: do_tc
 *
 * INVOCATION:
 *  do_tc();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Sets controllers time constant algorithm according to configured setting
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::do_tc()
{
	// TODO: Review if this is of any utility to us
	/*
	   int ret_value;
	   message_write("Setting time constant...");
	   switch (sdsu_config.config_data.tc_setting) {
	   case 0:
	   case 1:
	   ret_value = sdsu_command(TIM_ID, SDSU_SGN, sdsu_config.config_data.gain_setting,
	   sdsu_config.config_data.tc_setting);
	   if(ret_value != SDSU_OK)
	   message_write("Gain can not be set to %d  nor time constant to %d!", sdsu_config.config_data.gain_setting,
	   sdsu_config.config_data.tc_setting);
	   else
	   message_write("Gain set to %d, time constant set to %d!", sdsu_config.config_data.gain_setting,
	   sdsu_config.config_data.tc_setting);
	   break;
	   default:
	   message_write("illegal time constant setting for this controller - not set to %d",
	   sdsu_config.config_data.tc_setting);
	   break
	   }
	   */
	message_write("****** ERROR: Trying to set Time Constant for this controller - not implemented");
}
/*
 *+
 * FUNCTION NAME: do_test_pattern
 *
 * INVOCATION:
 *  do_test_pattern();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Sets controllers test pattern mode
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::do_test_pattern()
{
	message_write("****** ERROR: do_test_pattern is not implemented");
	/*
	   int ret_value = SDSU_OK;
	   int options_word = 0;

	// Read the current SDSU_Y_OPTIONS_WORD_PAR
	options_word = sdsu_command(TIM_ID, SDSU_ROP);

	if (verify_spec.do_test_pattern) {
	message_write("Enabling test pattern...");

	// Set enable bit
	options_word |= SDSU3_TEST_PATTERN_BIT;

	// Set/clear constant type bit
	if (verify_spec.constant_val) {
	options_word |= SDSU3_TEST_PATTERN_CONST_BIT;
	} else if (options_word & SDSU3_TEST_PATTERN_CONST_BIT) {
	options_word = options_word ^ SDSU3_TEST_PATTERN_CONST_BIT;
	}

	// Set/clear test pattern source bit
	//      if (sdsu_config.config_data.has_coadder_board && sdsu_config.config_data.test_pattern_by_coadder) {
	//        options_word |= SDSU_TEST_PATTERN_SOURCE_BIT;
	//      } else if (options_word & SDSU_TEST_PATTERN_SOURCE_BIT) {
	//        options_word = options_word ^ SDSU_TEST_PATTERN_SOURCE_BIT;
	//      }

	// Now write the new options word
	ret_value = sdsu_command(TIM_ID, SDSU_WOP, options_word);
	if (ret_value != SDSU_OK)
	message_write("Failed to enable test pattern options bits, err=%d", ret_value);

	// Now set the test pattern constant, seed and overscan values
	ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.seed, SDSU_MEM_Y, SDSU_TEST_PATTERN_SEED_PAR);
	if (ret_value != SDSU_OK)
	message_write("Failed to set test pattern constant, err=%d", ret_value);

	ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.increment, SDSU_MEM_Y, SDSU_TEST_PATTERN_INC_PAR);
	if (ret_value != SDSU_OK)
	message_write("Failed to set test pattern seed, err=%d", ret_value);

	ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.amp_increment, SDSU_MEM_Y, SDSU_TEST_PATTERN_AMP_INC_PAR);
	if (ret_value != SDSU_OK)
	message_write("Failed to set test pattern amp increment, err=%d", ret_value);

	ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.readout_increment, SDSU_MEM_Y, SDSU_TEST_PATTERN_READ_INC_PAR);
	if (ret_value != SDSU_OK)
	message_write("Failed to set test pattern readout increment, err=%d", ret_value);

	ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.overscan, SDSU_MEM_Y, SDSU_TEST_PATTERN_OS_PAR);
	if (ret_value != SDSU_OK)
	message_write("Failed to set test pattern seed, err=%d", ret_value);
	} else if (options_word & SDSU_TEST_PATTERN_BIT) {
	message_write("Disabling test pattern...");
	options_word = options_word ^ SDSU3_TEST_PATTERN_BIT;
	ret_value = sdsu_command(TIM_ID, SDSU_WOP, options_word);
	if (ret_value != SDSU_OK)
	message_write("Failed to disable test pattern bit, err=%d", ret_value);
	}
	*/
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::init_vars
 *
 * INVOCATION: sdsu->init_vars()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Initialise any sdsu vars
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::init_vars()
{
	Sdsu_Setup tsetup;

	sdsu_config_data = tsetup.config;
	memset((void*)&sdsu_dsp, 0, sizeof(Sdsu_Dsp));
	memset((void*)&sdsu_special_dsp, 0, sizeof(Sdsu_Dsp));
	idle_turned_off = true;
	default_timeout = 1;
	interface_fd = 0;
	use_interface_fd = false;
	sdsu_bad_header_count = 0;
	byte_swapping = false;
	update_status_vars = true;


	// Status parameters init to NULL
	sdsu_statusp = NULL;

	// controller reply buf must be allocated
	reply_buf = NULL;

	// Setup default config file
	strlcpy(sdsu_config_file,sdsu_path(SDSU_SETUP,controller_dir), FILENAMELEN);
	test_mode = FALSE;

	// Setup SDSU image info
	image_info.unsigned_data = TRUE;
	image_info.bitpix = DEFAULT_SDSU_BITPIX;
	image_info.bscale = DEFAULT_SDSU_BSCALE;
	image_info.bzero = DEFAULT_SDSU_BZERO;

	astro_state.devAdd = TRUE;
	astro_state.instance = 0;
	astro_state.open = 0;
	astro_state.rdc_sent = FALSE;
	astro_state.intr_count = 0;
	astro_state.control_registers = NULL;
	astro_state.ioctl_busy = FALSE;
	astro_state.read_busy = FALSE;
	astro_state.buffer_size = 0;
	astro_state.image_size = 0;
	astro_state.read_wait = 0;
	astro_state.read_time = 0;
	astro_state.read_interval = 0;
	astro_state.nresets = 0;
	astro_state.reset_delay = 0;
	astro_state.nreads = 0;
	astro_state.nsamples = 0;
	astro_state.stop = FALSE;
	astro_state.abort = FALSE;
	astro_state.simulate = FALSE;
	// TODO: Do we need this one?
	// memset((void*)astro_state.sim_dsp_mem, 0, sizeof(asp->sim_dsp_mem));
	astro_state.exptime = 0;
	astro_state.readout_time = 0;
	astro_state.readouts_done = 0;
	astro_state.read_running = 0;
	astro_state.timeout_mult = 1;

	astro_state.last_pixel_counter = 0;
	astro_state.pixel_counter = 0;
	astro_state.image_address = 0;
}
/*
 *+
 * FUNCTION NAME: Sdsu_CCD::add_status_parameters
 *
 * INVOCATION: add_status_parameters(bool init)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > init - set if required to initialise variables
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:
 *  Adds all sdsu status parameters to the parameter database
 *
 * DESCRIPTION: Uses the Parameter class to handle any Cicada status
 * parameters. This class does immediate write-thru operations to the
 * Cicada status shared memory on the observer computer any where on the net.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::add_status_parameters(bool init)
{

	// TODO: Review to make sure that all are applicable
	long lValid[2];
	double dValid[2];

	// Create the status parameter DB
	sdsu_statusp = new Parameter(controller_pDB, PARAMETER_NAME, controller_param_map, SDSU_PREFIX, DB_SUFFIX);

	// Now add all the Cicada Status parameters
	sdsu_status->exposure = 0;
	lValid[0] = 0;
	lValid[1] = MAXI;
	sdsu_statusp->add(EXPOSURE, NULL, (void*)&sdsu_status->exposure, init, 1, &sdsu_status->exposure,
			VALIDITY_RANGE, 2, lValid, PUBLISHED);


	sdsu_status->reply_status = 0;
	sdsu_statusp->add(REPLY_STATUS, REPLY_STATUS_DB, (void*)&sdsu_status->reply_status, init, 1,
			&sdsu_status->reply_status, VALIDITY_NONE, 0, (int*)NULL, UNPUBLISHED);

	sdsu_status->sdsu_command = 0;
	sdsu_statusp->add(SDSU_COMMAND, SDSU_COMMAND_DB, (void*)&sdsu_status->sdsu_command, init, 1,
			&sdsu_status->sdsu_command, VALIDITY_NONE, 0, (int*)NULL, UNPUBLISHED);

	sdsu_status->sdsu_command_done = 0;
	sdsu_statusp->add(SDSU_COMMAND_DONE, SDSU_COMMAND_DONE_DB, (void*)&sdsu_status->sdsu_command_done, init, 1,
			&sdsu_status->sdsu_command_done, VALIDITY_NONE, 0, (int*)NULL, UNPUBLISHED);

	sdsu_status->last_reply = 0;
	sdsu_statusp->add(LAST_REPLY, LAST_REPLY_DB, (void*)&sdsu_status->last_reply, init, 1,
			&sdsu_status->last_reply, VALIDITY_NONE, 0, (int*)NULL, UNPUBLISHED);

	sdsu_status->last_header = 0;
	sdsu_statusp->add(LAST_HEADER, LAST_HEADER_DB, (void*)&sdsu_status->last_header, init, 1,
			&sdsu_status->last_header, VALIDITY_NONE, 0, (int*)NULL, UNPUBLISHED);

	sdsu_status->expected_reply = 0;
	sdsu_statusp->add(EXPECTED_REPLY, EXPECTED_REPLY_DB, (void*)&sdsu_status->expected_reply, init, 1,
			&sdsu_status->expected_reply, VALIDITY_NONE, 0, (int*)NULL, UNPUBLISHED);

	sdsu_status->expected_header = 0;
	sdsu_statusp->add(EXPECTED_HEADER, EXPECTED_HEADER_DB, (void*)&sdsu_status->expected_header, init, 1,
			&sdsu_status->expected_header, VALIDITY_NONE, 0, (int*)NULL, UNPUBLISHED);

	sdsu_status->controller_swv[0] = '\0';
	sdsu_statusp->add(CONTROLLER_SWV, CONTROLLER_SWV_DB, (void*)sdsu_status->controller_swv, init, NAMELEN,
			sdsu_status->controller_swv, VALIDITY_NONE, 0, (char*)NULL, UNPUBLISHED);

	sdsu_statusp->add(CONTROLLER_PON, CONTROLLER_PON_DB, (void*)&sdsu_status->controller_pon, false, 1,
			(bool*)&sdsu_status->controller_pon, VALIDITY_NONE, 0, (bool*) NULL, UNPUBLISHED);

	sdsu_status->status_message[0] = '\0';
	sdsu_statusp->add(STATUS_MESSAGE, STATUS_MESSAGE_DB, (void*)sdsu_status->status_message, init, SMALLMSGLEN,
			sdsu_status->status_message, VALIDITY_NONE, 0, (char*)NULL, UNPUBLISHED);

	if (init) {
		// Add the voltage parameters - only for hardware constructor
		for (int v=0; v<sdsu_config.n_volts; v++ ) {
			sdsu_status->voltage_value[v] = sdsu_config.volts[v].value[0];
			dValid[0] = sdsu_config.volts[v].min_volt[0];
			dValid[1] = sdsu_config.volts[v].max_volt[0];
			sdsu_statusp->add(sdsu_config.volts[v].name, sdsu_config.volts[v].name, (void*)&sdsu_status->voltage_value[v], init, 1,
					&sdsu_status->voltage_value[v], VALIDITY_RANGE, 2, dValid, PUBLISHED);
		}
	}
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::open_and_check_interface
 *
 * INVOCATION: Sdsu3_5_Controller::open_and_check_interface()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Open the interface and test comms
 *
 * DESCRIPTION: This should only be done on instrument machine - ie called from slave
 * It should not be called from the constructor as it calls a pure-virtual function
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::open_and_check_interface()
{
	try {

		prepare_interface(true);

		// Try to communicate by sending TDLs to the timing and utility boards.
		if (sdsu_config.config_data.do_comms_test_at_startup) {
			do_hardware_test(TRUE, DISPLAY_MIN_MESGS);

			// Also set correct default timeout based on idle mode
			default_timeout = (check_idle())? sdsu_config.config_data.idle_timeout:1;
		}

		// Set our command timeout multiplier
		// TODO: Can we do this?
		// send_timeout(n_sync * default_timeout);

	}
	catch (Error& e) {
		msg_severity = e.type;
		message_write(e.record_error(__FILE__,__LINE__));
		e.rethrow();
	}
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::open_interface
 *
 * INVOCATION: open_interface(bool force)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Opens the SDSU controller interface
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

int Sdsu3_5_Controller::open_interface() {
	return open_interface(false);
}

// UPDATED TO 3.5
int Sdsu3_5_Controller::open_interface(bool force)
{
	sdsu_errno = OPEN_INTERFACE_ERROR;

	// Check if already open - if so close
	if (pDevice != nullptr)
		close_interface();

	const std::string* device_list;
	int dev_count = 0;

	{
		LockGuard lg(deviceLock, true);

		switch (controller.bus) {
			case BUS_PCI:
				CArcPCI::FindDevices();
				pDevice = new CArcPCI();
				device_list = CArcPCI::GetDeviceStringList();
				dev_count = CArcPCI::DeviceCount();
				break;
			case BUS_PCIe:
				CArcPCIe::FindDevices();
				pDevice = new CArcPCIe();
				device_list = CArcPCIe::GetDeviceStringList();
				dev_count = CArcPCIe::DeviceCount();
				break;
			default:
				message_write("open_interface: device type not supported");
				return SDSU_ERROR;
		}
	}


	if (dev_count < 1) {
		message_write("open_interface: no devices found");
		return SDSU_ERROR;
	}
	else if (force) {
		try {
			pDevice->Open(0);
		}
		catch (std::runtime_error e) {
			message_write("open_interface: %s", e.what());
			return SDSU_ERROR;
		}
		sdsu_errno = 0;
		return SDSU_OK;
	}
	else {
		const std::string comparing{controller.device};
		for (int i = 0; i < dev_count; i++) {
			const auto split = device_list[i].find_last_of(' ');
			const auto tested = device_list[i].substr(split + 2);
			if (comparing == tested.substr(0, tested.length() - 1)) {
				try {
					LockGuard lg(deviceLock, true);

					pDevice->Open(i);
				}
				catch (std::runtime_error e) {
					message_write("open_interface: %s", e.what());
					return SDSU_ERROR;
				}
				if (!pDevice->IsOpen()) {
					message_write("open_interface: error when opening the device");
					return SDSU_ERROR;
				}
				sdsu_errno = 0;
				return SDSU_OK;
			}
		}

		message_write("open_interface: device file %s not found", controller.device);
	}

	return SDSU_ERROR;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::close_interface
 *
 * INVOCATION: close_interface()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Closes the SDSU controller interface
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::close_interface()
{
	sdsu_errno = 0;

	if (pDevice != nullptr) {
		LockGuard lg(deviceLock, true);
		pDevice->Close();
		if (pDevice->IsOpen()) {
			sdsu_errno = CLOSE_INTERFACE_ERROR;
			return SDSU_ERROR;
		}
	}

	return SDSU_OK;
}


/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::prepare_interface
 *
 * INVOCATION: prepare_interface(bool do_interface_reset)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > do_interface_reset - reset SDSU interface if set
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:  SDSU interface card setup routine
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::prepare_interface(bool do_interface_reset)
{
	char msgbuf[MSGLEN];   // Temporary message buffer

	if (open_interface() == SDSU_ERROR) {
		close_interface();
		snprintf(msgbuf, MSGLEN, "Unable to open the SDSU interface %s!", controller.device);
		throw Error(msgbuf, E_ERROR, errno, __FILE__, __LINE__);
	}

	// Set the device driver debug mode to off
	set_debug_mode(0);

	// reset interface board on startup
	if (do_interface_reset)
		send_interface_reset();
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::ready_interface
 *
 * INVOCATION: ready_interface()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Readies the interface for accepting an image from controller
 *
 * DESCRIPTION: Calls the appropriate driver setup routine
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::ready_interface()
{
	switch (controller.bus) {
		case BUS_PCI:
		case BUS_PCIe:
			astro_state.nreads = nreads;
			astro_state.nsamples = nsamples;
			astro_state.read_interval = read_interval;
			astro_state.read_time = read_time;
			astro_state.nresets = nresets;
			astro_state.reset_delay = reset_delay;
			astro_state.buffer_size = buffer_size;
			astro_state.image_size = image_size;
			astro_state.namps = rgn->nactive_amps;

			send_pci_setup();
			break;
		default:
			break;
	}
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::get_controller_info()
 *
 * INVOCATION: get_controller_info()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Queries the controller for its current info
 *
 * DESCRIPTION:
 *
 * Reads DSP version number
 * Reads status word and checks for power on
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::get_controller_info()
{
	// int options_word = 0;
	// bool pon_bit = false;

	// Get the DSP version number
	sdsu_statusp->put(CONTROLLER_SWV, get_dsp_version(util_id), 0, NAMELEN);

	// Check options word

	// Read the current SDSU_X:STATUS
	// TODO: Cannot derive the PON bit from this!
	/*
	if (pDevice != nullptr) {
		try {
			options_word = pDevice->GetStatus();
			if (options_word != (SDSU_TOUT&REPLY_MASK)) {
				printf("**** SETTING pon_bit\n");
				pon_bit = ((options_word & SDSU3_PON_BIT) == SDSU3_PON_BIT);
			}
		}
		catch (std::runtime_error e) {
			printf("***** Getting the Status Word: %s", e.what());
			// Do nothing...
		}
	}

	printf("**** SETTING CONTROLLER_PON TO: %d\n", int(pon_bit));
	sdsu_statusp->put(CONTROLLER_PON,  pon_bit);
	*/
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::change_simulation_mode()
 *
 * INVOCATION: change_simulation_mode(int sim)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > sim - simulation level
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Sets driver up to sumulate interaction with controller
 *
 * DESCRIPTION:
 * If desired, a driver can simulate interaction with an SDSU controller.
 * All commands to the driver in simulation mode will return expected
 * replies. Image readout commands will return test pattern data as
 * currently setup.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::change_simulation_mode(int sim)
{
	message_write("Setting controller simulation to %d ...", sim);

	// Call parent method first so that any generic stuff is handled
	Controller::change_simulation_mode(sim);

	astro_state.simulate = sim > 1;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::set_debug_mode()
 *
 * INVOCATION: set_debug_mode(int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > deb - debug flag setting - usually a mask to be interpreted by specific driver
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Sets driver debug flag
 *
 * DESCRIPTION:
 * Driver debugging levels can be turned on or off through this call
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// PENDING UPDATE
void Sdsu3_5_Controller::set_debug_mode(int deb)
{
	message_write("****** set_debug_mode NOT IMPLEMENTED");
	/* TODO: Rewrite this
	   message_write("Setting device driver debug mode to %d ...", deb);
	   sdsu_request = ASTRO_DEBUG;
	   send_command(deb, SDSU_END);
	   */
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::send_interface_command
 *
 * INVOCATION: send_interface_command()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: int - returns SDSU_ERROR if fails
 *
 * PURPOSE: Sends a command to the SDSU interface card
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS: Both sdsu_request and cmd_buf members must
 * be setup before thsi call
 *
 * DEFICIENCIES:
 *
 *-
 */
// PENDING UNTIL DEPENDENCIES ARE SOLVED
int Sdsu3_5_Controller::send_interface_command()
{
	int iostat, i;
	iostat = 0;

	if (update_status_vars) {
		sdsu_statusp->put(REPLY_STATUS, 0);
		sdsu_statusp->put(STATUS_MESSAGE, "\0", 0, 1);
		if (sdsu_request == ASTRO_COMMAND)
			sdsu_statusp->put(SDSU_COMMAND_DONE, (int)cmd_buf[1]);
		else
			sdsu_statusp->put(SDSU_COMMAND_DONE, 0x43544C); // CTL (control) command
	}

	// Write command to the SDSU hardware
	switch (controller.bus) {
		case BUS_PCI:
		case BUS_PCIe:
			// ioctl puts its reply into the same buffer it took its commands from
			// so copy the commands into the reply buffer and pass that to ioctl.
			if (ncmd_words <= SDSU_MAX_REPLY_ARGS) {
				memcpy(reply_buf, cmd_buf, ncmd_words*sizeof(unsigned long));
			} else {
				throw Error("ncmd_words too large in send_interface_command()", E_ERROR, SDSU_ERROR, __FILE__, __LINE__);
			}
			if (debug&SDSU_SHOW_COMMANDS) {
				message_write("Sending IOCTL, request=0x%x, #cmd words=%d", sdsu_request, ncmd_words);
				for (i=0; i<ncmd_words; i++)
					message_write("   %2d: 0x%x", i, cmd_buf[i]);
			}

			/* TODO: Figure this ioctl out
			 *
			 if ((iostat = ioctl(interface_fd, sdsu_request, reply_buf)) != OK) {
			 if (debug&SDSU_SHOW_COMMANDS)
			 message_write("IOCTL error errno=%d, reply0=0x%x, reply1=0x%x", errno, reply_buf[0], reply_buf[1]);
			 return SDSU_ERROR;
			 }
			 */
			if (debug&SDSU_SHOW_COMMANDS)
				message_write("IOCTL iostat=%d, reply0=0x%x, reply1=0x%x", iostat, reply_buf[0], reply_buf[1]);
			return iostat;
		default:
			return SDSU_ERROR;
	}
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::read_interface_reply
 *
 * INVOCATION: read_interface_reply(int nbytes, int timeout)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > nbytes - max number of bytes to read
 * > timeout - timeout in seconds
 *
 * FUNCTION VALUE: int - returns SDSU_ERROR if fails
 *
 * PURPOSE: reads a reply from the SDSU interface card
 *
 * DESCRIPTION: A reply is read into the SDSU reply buffer. A
 * maximum of SDSU_REPLY_SIZE words can be handled.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Sdsu3_5_Controller::read_interface_reply(int nbytes, int timeout)
{

	switch (controller.bus) {
		case BUS_PCI:
		case BUS_PCIe: {
				       // Read the reply header
				       // Not yet implemented in PCI DSP
				       //      if (send_pci_hcvr(SDSU_PCI_READ_HEADER, UNDEFINED, SDSU_END) != SDSU_OK) {
				       //        message_write("Failed to read PCI reply header = (0x%lx)!", reply_buf[0]);
				       //        return SDSU_ERROR;
				       //      }

				       // Now the reply word
				       if (send_pci_hcvr((Sdsu_Cmd) SDSU_PCI_READ_REPLY_VALUE, UNDEFINED, SDSU_END) != SDSU_OK) {
					       message_write("Failed to read PCI reply value = (0x%lx)!", reply_buf[0]);
					       return SDSU_ERROR;
				       }
				       return SDSU_OK;
			       }
		default:
			       return SDSU_ERROR;
	}
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::write_configuration
 *
 * INVOCATION: Sdsu3_5_Controller::write_configuration(const char* fname, const Sdsu_Config_Data this_config)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > fname - file name to use
 * > this_config - configuration to save
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Writes a configuration to config file
 *
 * DESCRIPTION: Uses the sdsu setup class to perform write
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::write_configuration(const char* fname, const Sdsu_Config_Data this_config)
{
	Sdsu_Setup setup;
	setup.config = this_config;
	message_write("Saving CCD setup as: %s", fname);
	setup.write(fname);
}


/******************************************************************************
  D S P   Functions
 *******************************************************************************/

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_command
 *
 * INVOCATION: sdsu_command(Sdsu_Cmd command)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > command - actual command to run
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Execute an SDSU interface DSP command
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::sdsu_command(Sdsu_Cmd command)
{
	// int status;

	// Interface commands
	switch (command) {
		case SDSU_INTERFACE_RESET:
			send_interface_reset();
			return SDSU_OK;
			// TODO: The three are done the same way
			//       in this mode, apparently. Sort out
			//       the differences.
		case SDSU_ABR:
		case SDSU_ABORT:
		case SDSU_STOP:
			try {
				pDevice->StopExposure();
				return SDSU_OK;
			}
			catch (std::runtime_error e) {
				message_write("sdsu_command: %s", e.what());
			}

		default:
			message_write("Unknown SDSU command %s", current_command_str(command));
			return SDSU_ERROR;
	}
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_command
 *
 * INVOCATION: sdsu_command(int board_id, Sdsu_Cmd command)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - SDSU board identifier
 * > command - SDSU command
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Send a DSP command to a particular board id
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::sdsu_command(int board_id, Sdsu_Cmd command)
{
	int ret_val = 0;
	int expected = 0;
	LockGuard lg(deviceLock, true);

	if (sdsu_simulation != 0)
		return SDSU_OK;

	if ((pDevice == nullptr) || (!pDevice->IsOpen())) {
		if (pDevice == nullptr)
			message_write("sdsu_command: Unitialized device");
		else
			message_write("sdsu_command: Closed device");
		return SDSU_ERROR;
	}

	// Reset interface for any commands that expect a reply
	switch (command) {
		/*  boot commands */
		case SDSU_RST:
			ret_val = pDevice->Command(board_id, command);
			expected = RST;
			break;
			/* timing board commands */
		case SDSU_HGN:
		case SDSU_LGN:
		case SDSU_RAD:
		case SDSU_SBV:
		case SDSU_TCK:
		case SDSU_TDC:
		case SDSU_TBS:
			ret_val = pDevice->Command(board_id, command);
			expected = DON;
			break;
		case SDSU_CLR:
			// TODO: Do we need this? Figure it out
			// send_timeout(n_sync*sdsu_config.config_data.clr_timeout);

			try {
				// clr_timeout is in seconds - multiplier is used to increase min timeout
				// (1 sec) by amount required for CLR
				ret_val = pDevice->Command(board_id, command);
				expected = DON;
			}
			catch (Error& ce) {
				// Reset the timeout and just rethrow
				// TODO: Do we need this? Figure it out
				// send_timeout(n_sync*default_timeout);
				ce.rethrow();
			}
			// Restore timeout
			// TODO: Do we need this? Figure it out
			// send_timeout(n_sync*default_timeout);
			break;
		case SDSU_RRS:
			// Use same timeout for reset as used for CLR
			// send_timeout(n_sync*sdsu_config.config_data.clr_timeout);

			try {
				// timeout is in seconds - multiplier is used to increase min timeout
				// (1 sec) by amount required for RRS
				ret_val = pDevice->Command(board_id, command);
				expected = SYR;
			}
			catch (Error& ce) {
				// Reset the timeout and just rethrow
				// send_timeout(n_sync*default_timeout);
				ce.rethrow();
			}
			// Restore timeout
			// send_timeout(n_sync*default_timeout);
			break;
		case SDSU_IDL:
			ret_val = pDevice->Command(board_id, command);
			expected = DON;
			/* TODO: Figure this one out...
			   if ((reply = get_sdn_reply(board_id, DON, DEFAULT_TIMEOUT)) == SDSU_OK) {
			   default_timeout = sdsu_config.config_data.idle_timeout;
			   send_timeout(n_sync * default_timeout);
			   }
			   */
			break;
		case SDSU_STP:
			ret_val = pDevice->Command(board_id, command);
			expected = DON;
			/* TODO: Figure this one out...
			   if ((reply = get_sdn_reply(board_id, DON, DEFAULT_TIMEOUT)) == SDSU_OK) {
			   default_timeout = 1;
			   send_timeout(n_sync * default_timeout);
			   }
			   */
			break;
		case SDSU_SYN:
		case SDSU_RDC:
			pDevice->Command(board_id, command);
			return 1;
			/* utility board commands */
		case SDSU_AEX:
			/*
			   send_cmd(AEX, board_id);
			// AEX commands will generate two replies the first an ABR acknowledgment
			// followed by the normal DON - NB This is only while the DSP code
			// thinks we are running a VME interface! - REMOVE WHEN MARK UPDATES
			// DSP CODE
			// Had to add an extra parameter to get_sdn_reply to handle the unusual
			// expected reply after an AEX command. This is added to the expected
			// reply. - PJY - removed on 4/12/98
			// get_sdn_reply(board_id, ABR, DEFAULT_TIMEOUT, 0x000100);
			return get_sdn_reply(board_id, DON, DEFAULT_TIMEOUT);
			*/
			return SDSU_OK;
		case SDSU_CSH:
		case SDSU_OSH:
		case SDSU_PEX:
		case SDSU_REX:
		case SDSU_SYR:
		case SDSU_TAD:
		case SDSU_TDA:
		case SDSU_TDG:
		case SDSU_SEX:
		case SDSU_LCA:
		case SDSU_DCA:
		case SDSU_ICA:
		case SDSU_SCA:
			ret_val = pDevice->Command(board_id, command);
			expected = DON;
			break;
		case SDSU_PON:
		case SDSU_POF:
			// First update timeout to handle PON/POF
			// send_timeout(n_sync*sdsu_config.config_data.pon_timeout);

			try {
				ret_val = pDevice->Command(board_id, command);
				lg.unlock();

				ret_val = evaluate_sdn_reply(ret_val, DON);
				sdsu_statusp->put(CONTROLLER_PON, bool(command==SDSU_PON));
			}
			catch (Error& ce) {
				// Reset the timeout and just rethrow
				// send_timeout(n_sync*default_timeout);
				ce.rethrow();
			}

			// Now restore timeout
			// send_timeout(n_sync*default_timeout);

			return ret_val;
		case SDSU_RET:
		case SDSU_ROP:
		case SDSU_RRT:
		case SDSU_RSW:
			ret_val = pDevice->Command(board_id, command);
			expected = SDSU_ACTUAL_VALUE;
			break;
			// PCI board commands
		default:
			message_write("Unknown simple board DSP command %s", current_command_str(command));
			return SDSU_ERROR;
	}

	lg.unlock();

	return evaluate_sdn_reply(ret_val, expected);
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_command
 *
 * INVOCATION: sdsu_command(int board_id, Sdsu_Cmd command, int data, Sdsu_Memory_Space mem_space, int address)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - SDSU board identifier
 * > command - SDSU command
 * > data - data to send
 * > mem_space - SDSU memory type
 * > address - memory address
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Send a DSP command which reads or writes a mem address
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::sdsu_command(int board_id, Sdsu_Cmd command, int data, Sdsu_Memory_Space mem_space, int address)
{
	sdsu_request = ASTRO_COMMAND;
	LockGuard lg(deviceLock, true);
	int ret_val;
	int expected;

	if ((pDevice == nullptr) || (!pDevice->IsOpen())) {
		if (pDevice == nullptr)
			message_write("sdsu_command: Unitialized device");
		else
			message_write("sdsu_command: Closed device");
		return SDSU_ERROR;
	}

	try {
		switch (command) {
			/*  boot commands */
			case SDSU_RDM:
				ret_val = pDevice->Command(board_id, command, mem_space, address);
				expected = SDSU_ACTUAL_VALUE;
				break;
			case SDSU_WRM:
				ret_val = pDevice->Command(board_id, command, mem_space, address, data);
				expected = DON;
				break;
			default:
				message_write("sdsu_command: Unknown DSP memory command %s", current_command_str(command));
				return SDSU_ERROR;
		}

		lg.unlock();

		return evaluate_sdn_reply(ret_val, expected);
	}
	catch (std::runtime_error e) {
		message_write("sdsu_command: %s", e.what());
		return SDSU_ERROR;
	}
	lg.unlock();

	return evaluate_sdn_reply(ret_val, expected);
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_command
 *
 * INVOCATION: sdsu_command(int board_id, Sdsu_Cmd command, int data)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - SDSU board identifier
 * > command - SDSU command
 * > data - data to send
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Send a DSP command with associated data
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::sdsu_command(int board_id, Sdsu_Cmd command, int data)
{
	int ret_val;
	int expected = DON;
	LockGuard lg(deviceLock, true);

	switch (command) {
		case SDSU_TDL:
		case SDSU_CAT:
			ret_val = pDevice->Command(board_id, command, data);
			expected = SDSU_ACTUAL_VALUE;
			break;
		case SDSU_LDA:
		case SDSU_SRF:
		case SDSU_SFS:
		case SDSU_NRW:
		case SDSU_NCL:
		case SDSU_SUR:
		case SDSU_SRM:
		case SDSU_SRD:
		case SDSU_SNR:
		case SDSU_SET:
		case SDSU_SBS:
		case SDSU_WOP:
			ret_val = pDevice->Command(board_id, command, data);
			break;
		case SDSU_DFS:
			ret_val = pDevice->Command(board_id, command, data, TRUE);
			break;
		default:
			message_write("Unknown DSP data command %s", current_command_str(command));
			return SDSU_ERROR;
	}
	lg.unlock();

	return evaluate_sdn_reply(ret_val, expected);
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_command
 *
 * INVOCATION: sdsu_command(int board_id, Sdsu_Cmd command, int data1, int data2)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - SDSU board identifier
 * > command - SDSU command
 * > data1 - first data to send
 * > data2 - second data to send
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Send a DSP command with two associated data items
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::sdsu_command(int board_id, Sdsu_Cmd command, int data1, int data2)
{
	int ret_val;
	LockGuard lg(deviceLock, true);

	switch (command) {
		case SDSU_DFS:
		case SDSU_SGN:
			ret_val = pDevice->Command(board_id, command, data1, data2);
			break;
		default:
			message_write("Unknown DSP double data command %s", current_command_str(command));
			return SDSU_ERROR;
	}
	lg.unlock();

	return evaluate_sdn_reply(ret_val, DON);
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_command
 *
 * INVOCATION: sdsu_command(int board_id, Sdsu_Cmd command, Sdsu_Voltage_Cmd voltage_cmd)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - SDSU board identifier
 * > command - SDSU command
 * > voltage_cmd - structure containing data for a voltage command
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Send a DSP command which reads or writes a mem address
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::sdsu_command(int board_id, Sdsu_Cmd command, Sdsu_Voltage_Cmd voltage_cmd)
{
	int ret_val;
	int dac_board_mnemonic;
	LockGuard lg(deviceLock, true);

	switch (command) {
		//  voltage commands
		case SDSU_SBN:
			switch (voltage_cmd.dac_board_type) {
				case SDSU_VIDEO_BOARD: dac_board_mnemonic = SDSU_VID;
						       break;
				case SDSU_CLOCK_BOARD: dac_board_mnemonic = SDSU_CLK;
						       break;
				default:
						       throw Error("Illegal DAC board type setting?", E_ERROR, EINVAL, __FILE__, __LINE__);
			}
			ret_val = pDevice->Command(board_id, SBN, voltage_cmd.dac_board, voltage_cmd.dac_number, dac_board_mnemonic, voltage_cmd.dac_voltage);
			break;
		default:
			message_write("Unknown DSP voltage command %s", current_command_str(command));
			return SDSU_ERROR;
	}
	lg.unlock();

	return evaluate_sdn_reply(ret_val, DON);
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_command
 *
 * INVOCATION: sdsu_command(int board_id, Sdsu_Cmd command, Sdsu_Mux_Cmd mux_cmd)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - SDSU board identifier
 * > command - SDSU command
 * > mux_cmd - structure containing data for a mux command
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Send a DSP command which reads or writes a mem address
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::sdsu_command(int board_id, Sdsu_Cmd command, Sdsu_Mux_Cmd mux_cmd)
{
	int ret_val;
	LockGuard lg(deviceLock, true);

	switch (command) {
		//  mux commands
		case SDSU_SMX:
			ret_val = pDevice->Command(board_id, SMX, mux_cmd.clk_board, mux_cmd.mux1, mux_cmd.mux2);
			break;
		default:
			message_write("Unknown DSP mux command %s", current_command_str(command));
			return SDSU_ERROR;
	}
	lg.unlock();

	return evaluate_sdn_reply(ret_val, DON);
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::send_pci_hcvr
 *
 * INVOCATION: send_pci_hcvr(Sdsu_Cmd command, int expected_reply, int data1, int data2)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > command - SDSU command
 * > expected_reply - SDSU reply expected from controller
 * > data1 - first data to send
 * > data2 - second data to send
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Send a command to the PCI DSP via the vector register.
 *
 * DESCRIPTION:
 * The reply returned from the PCI DSP is checked against the specified expected
 * reply value.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Sdsu3_5_Controller::send_pci_hcvr(Sdsu_Cmd command, int expected_reply, int data1, ...)
{
	va_list ap;
	int data;

	va_start(ap, data1);
	sdsu_request = CArcPCI::ASTROPCI_HCVR_DATA;
	ncmd_words = 1;
	data = data1;

	// If there's data, send it, multiple data arguments will come after the first data1
	// they will be terminated by SDSU_END
	while (data != SDSU_END) {
		if (data != UNDEFINED) {
			cmd_buf[0] = data;
			if (send_interface_command() == SDSU_ERROR) {
				message_write("Send_interface command returned error = %d during CArcPCI::ASTROPCI_HCVR_DATA", errno);
				sdsu_request = default_request;
				return SDSU_ERROR;
			}
		}
		data = va_arg(ap, int);
	}
	va_end(ap);

	// Now Send the command
	cmd_buf[0] = (int) command;
	sdsu_request = CArcPCI::ASTROPCI_SET_HCVR;
	if (send_interface_command() == SDSU_ERROR) {
		message_write("Send_interface command returned error = %d during CArcPCI::ASTROPCI_SET_HCVR", errno);
		sdsu_request = default_request;
		return SDSU_ERROR;
	}

	// Set request back to default
	sdsu_request = default_request;

	// If a reply is expected, check it
	if (expected_reply != UNDEFINED)
		return check_expected_reply(reply_buf[0], expected_reply);
	else
		return SDSU_OK;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::send_pci_cmd
 *
 * INVOCATION: send_pci_cmd(int command, int arg)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > command - SDSU command
 * > arg - single argument to pass
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Send a direct command to the PCI DSP via the vector register.
 *
 * DESCRIPTION:
 * The reply returned is used directrly by the caller by accessing reply_buf
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::send_pci_cmd(int command, int arg)
{
	sdsu_request = command;
	ncmd_words = 1;
	cmd_buf[0] = arg;
	if (send_interface_command() == SDSU_ERROR) {
		message_write("Send_interface command returned error = %d during CArcPCI::ASTROPCI_SET_HCVR", errno);
		sdsu_request = default_request;
		throw Error("Error during send_pci_cmd!", E_ERROR, EIO, __FILE__, __LINE__);
	}
	sdsu_request = default_request;
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::ready_reply_mem
 *
 * INVOCATION: ready_reply_mem(int size)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > size - size of reply mem to clear
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: block of memory is cleared
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::ready_reply_mem(int size)
{
	switch (controller.bus) {
		case BUS_PCI:
		case BUS_PCIe:
			// Initialise reply buffer by filling it with zeros
			memset((void*)reply_buf, 0, size*sizeof(unsigned long));
			break;
		default:
			break;
	}
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::request_type_str
 *
 * INVOCATION: request_type_str()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: returns a string that describes the dsp request
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
char* Sdsu3_5_Controller::request_type_str()
{
	switch (sdsu_request) {
		case ASTRO_COMMAND: return (char*) "Standard DSP command";
		case ASTRO_GET_PROGRESS: return (char*) "Read SDSU progress";
		case ASTRO_READS_DONE: return (char*) "Read SDSU reads done";
		case ASTRO_READ_TIME: return (char*) "Read SDSU last readout time";
		case ASTRO_READ_RUNNING: return (char*) "Read SDSU read running";
		case ASTRO_READ_STATUS: return (char*) "Get readout status";
		case ASTRO_RESET: return (char*) "SDSU reset";
		case ASTRO_ABORT: return (char*) "SDSU ABORT";
		case ASTRO_STOP: return (char*) "SDSU stop";
		case ASTRO_SET_TIMEOUT: return (char*) "Set timeout multiplier";
		case ASTRO_SETUP: return (char*) "Write SDSU setup";
		case CArcPCI::ASTROPCI_COMMAND: return (char*) "Standard PCI DSP command";
		case ASTRO_SIMULATE: return (char*) "SDSU simulation";
		default: return (char*) "Unknown request type?";
	}
}
/*
 *+
 * FUNCTION NAME: SSdsu3_5_Controller::send_command
 *
 * INVOCATION: send_command(int arg1, ...)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > arg1 - first argument in variable list
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Interpret, format and send command to interface
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::send_command(int arg1, ...)
{
	va_list ap;
	const int ARGLEN = 20;
	char arg[ARGLEN];
	int argument;
	int i,j;
	char msgbuf[MSGLEN];   // Temporary message buffer

	va_start(ap, arg1);

	ready_reply_mem(SDSU_MAX_REPLY_ARGS);
	ncmd_words = 0;

	argument = arg1;

	j = 0;
	if ((argument != SDSU_END) && (debug & SDSU_SHOW_COMMANDS)) {
		snprintf(msgbuf, MSGLEN,"Command (%s):",request_type_str());
		j=strlen(msgbuf);
	} else
		msgbuf[0] = 0;

	// Lopp over arguments
	while ((argument != SDSU_END) && (ncmd_words < SDSU_CMD_BUF_SIZE)) {
		if (ncmd_words==1) {
			// This the command to be sent - setup the current command string
			current_command_str(argument);
			if (debug & SDSU_SHOW_COMMANDS) {
				/* This one shows the actual values of the commands instead of the
				   ASCII values if the "show commands" is chosen from the properties
				   for debugging. */
				for (i=0; i<3; i++){
					msgbuf[j++] = current_command_buf[i];
				}
				msgbuf[j++] = ' ';
				msgbuf[j] = 0;
				// Also print the header
				snprintf(arg, ARGLEN,"(header:%x)  ", arg1);
				strlcat(msgbuf,arg,MSGLEN);
			}
		} else if (debug & SDSU_SHOW_COMMANDS) {
			snprintf(arg, ARGLEN,"%x  ", argument);
			strlcat(msgbuf,arg,MSGLEN);
		}
		if (!(debug & SDSU_DO_NOTHING)) {
			cmd_buf[ncmd_words] = argument;
		}
		ncmd_words++;
		argument = va_arg(ap, int);
	}
	if ((debug & SDSU_SHOW_COMMANDS) && (strlen(msgbuf)>0))
		message_write(msgbuf);

	if (!(debug & SDSU_DO_NOTHING)){
		if (send_interface_command() == SDSU_ERROR) {
			if (sdsu_request == ASTRO_COMMAND)
				message_write("send_interface command returned error = %d, request=0x%x, cmd=0x%x", errno, sdsu_request,(int)cmd_buf[1]);
			else
				message_write("send_interface command returned error = %d, request=0x%x, cmd=0x43544C", errno, sdsu_request);
			reply_buf[0] = SDSU_ERROR;
		}
		sdsu_request = default_request;
	}
	va_end(ap);
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::send_interface_reset
 *
 * INVOCATION: send_interface_reset()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: reset interface card
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::send_interface_reset()
{
	try {
		LockGuard lg(deviceLock);

		if (pDevice != nullptr)
			pDevice->Reset();
		return SDSU_OK;
	}
	catch (std::runtime_error e) {
		message_write("send_interface_reset: %s", e.what());
		return SDSU_ERROR;
	}
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::send_controller_reset
 *
 * INVOCATION: send_controller_reset()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: reset controller
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::send_controller_reset()
{
	try {
		LockGuard lg(deviceLock);

		if (pDevice != nullptr)
			pDevice->ResetController();
		return SDSU_OK;
	}
	catch (std::runtime_error e) {
		message_write("send_controller_reset: %s", e.what());
		return SDSU_ERROR;
	}
}


/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::send_timeout
 *
 * INVOCATION: send_timeout(int timeout_multiplier)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > timeout_multiplier - used to set timeout to be a multiple of this
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Sets the timeout multiplier
 *
 * DESCRIPTION:
 * The astro device driver maintains a timeout for handling responses from the
 * controller - this routine modifies the timeout multiplier that is used for
 * calculating the absolute timeout value.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// PENDING UPDATE
void Sdsu3_5_Controller::send_timeout(int timeout_multiplier)
{
	if (timeout_multiplier == 0)
		timeout_multiplier = 1;
	sdsu_request = ASTRO_SET_TIMEOUT;
	send_command(timeout_multiplier, SDSU_END);
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::send_pci_setup
 *
 * INVOCATION: send_pci_setup()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Sends the ASTRO driver setup
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// PENDING GETTING THE ACTUAL CONFIG
void Sdsu3_5_Controller::send_pci_setup()
{
	// This command used to set a number of values (nreads, nsamples,
	// and so on), but those are mostly useless for our new DSP firmware.

	// TODO: fixed data size. We should replace this with a setting, but it doesn't change anyway...
	// TODO: All this depends on loading the proper config
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::get_sdn_reply
 *
 * INVOCATION: get_sdn_reply(int board_id, int expected_reply, int timeout,
 *			     int added_reply)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - SDSU board identifier
 * > expected_reply - expeted reply from previous command
 * > timout - timeout in msec to wait for reply
 * > added_reply - extra info in reply
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Get a reply from a previously sent command.
 *
 * DESCRIPTION: This method reads a Source/Destination/Nargs style
 * reply from the SDSU interface.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// PENDING UPDATE: MAYBE REMOVE
int Sdsu3_5_Controller::get_sdn_reply(int board_id, int expected_reply, int timeout, int added_reply)
{
	int expected_header;
	int local_errno = 0;
	char msgbuf[MSGLEN];   // Temporary message buffer

	expected_header = (board_id << 16) | 2 | added_reply;
	sdsu_last_header = sdsu_last_reply = 0;

	if (update_status_vars) {
		sdsu_statusp->put(EXPECTED_HEADER, expected_header);
		sdsu_statusp->put(EXPECTED_REPLY, expected_reply);
	}

	if ((controller.bus == BUS_PCI) || (controller.bus == BUS_PCIe)) {
		// Check the first reply word to see if an SDN reply is available
		// timeout not used because is already in the driver cmd handling code
		sdsu_last_reply = (reply_buf[0] & REPLY_MASK);
		sdsu_last_header = expected_header;
	} else {

		// Make sure reply is what we expected - parse parts of reply
		sdsu_last_header = (reply_buf[0] & REPLY_MASK);
		sdsu_last_reply = (reply_buf[1] & REPLY_MASK);
	}

	if (update_status_vars) {
		sdsu_statusp->put(LAST_HEADER, sdsu_last_header);
		sdsu_statusp->put(LAST_REPLY, sdsu_last_reply);
	}

	// Is the received header expected
	if ((sdsu_last_header != expected_header) && !controller_abort) {

		// Report if the error count is less than maximum for reporting (ie prevent flood)
		if (sdsu_bad_header_count < MAX_SDSU_BAD_REPLY_HEADERS) {
			snprintf(msgbuf, SMALLMSGLEN-1, "BAD REPLY,h:%08x,r:%08x(%s)",sdsu_last_header, sdsu_last_reply,
					current_command_str(sdsu_last_reply));
			message_write("BAD REPLY HEADER, expected header:%08x, actual header:%08x, actual reply:%08x (%s)",
					expected_header, sdsu_last_header, sdsu_last_reply, current_command_str(sdsu_last_reply));
		}

		sdsu_errno = SDSU_BAD_REPLY_HEADER;
		throw Error(report_errno(local_errno,msgbuf), E_ERROR, local_errno, __FILE__,__LINE__);
	}

	// For replies where we expect a value - just return
	if (expected_reply == SDSU_ACTUAL_VALUE) {

		if (update_status_vars) {
			snprintf(msgbuf, SMALLMSGLEN-1, "SDSU returned actual value (0x%x)", sdsu_last_reply);
			sdsu_statusp->put(STATUS_MESSAGE, msgbuf, 0, strlen(msgbuf));
		}

		if (debug & SDSU_SHOW_COMMANDS)
			message_write("	returned value: 0x%x", sdsu_last_reply);

		last_reply_len = 2;

		return sdsu_last_reply;
	}

	// Otherwise - was the last reply an error?
	if (sdsu_last_reply == SDSU_ERR) {

		// Yes - throw an exception, ie no point continuing
		if (debug & SDSU_SHOW_ERRORS)
			message_write("***ERROR");

		sdsu_errno = SDSU_REPLY_ERROR;
		snprintf(msgbuf, SMALLMSGLEN-1, "ERR REPLY,h:%08x,r:%08x(%s)",sdsu_last_header, sdsu_last_reply,
				current_command_str(sdsu_last_reply));

		throw Error(report_errno(SDSU_ERR,msgbuf), E_ERROR, SDSU_ERROR, __FILE__, __LINE__);

	} else if (sdsu_last_reply == expected_reply) {
		// No - we have the reply we expected, return successfully

		if (update_status_vars) {
			snprintf(msgbuf, SMALLMSGLEN-1, "SDSU returned expected reply (%s)", current_command_str(sdsu_last_reply));
			sdsu_statusp->put(STATUS_MESSAGE, msgbuf, 0, strlen(msgbuf));
		}

		if (debug & SDSU_SHOW_COMMANDS)
			message_write("     returned value: %s", current_command_str(sdsu_last_reply));

		last_reply_len = sdsu_last_header & 0xff;
		last_reply_len = (last_reply_len > SDSU_MAX_REPLY_ARGS)? SDSU_MAX_REPLY_ARGS:last_reply_len;

		return SDSU_OK;

	} else if (!controller_abort) {
		// Or - we got an unexpected reply, throw an exception here as well...
		snprintf(msgbuf, SMALLMSGLEN-1, "BAD REPLY:%08x, wanted:%08x", sdsu_last_reply, expected_reply);
		message_write("BAD REPLY:%08x, wanted:%08x", sdsu_last_reply, expected_reply);
		throw Error(report_errno(local_errno,msgbuf), E_ERROR, local_errno, __FILE__,__LINE__);
	}

	return SDSU_OK;
}

// UPDATED TO 3.5: NEWLY CREATED
int Sdsu3_5_Controller::evaluate_sdn_reply(int actual_reply, int expected_reply, int added_reply)
{
	int local_errno = 0;
	char msgbuf[MSGLEN];   // Temporary message buffer

	if (update_status_vars) {
		sdsu_statusp->put(EXPECTED_REPLY, expected_reply);
	}

	// For replies where we expect a value - just return
	if (expected_reply == SDSU_ACTUAL_VALUE) {

		if (update_status_vars) {
			snprintf(msgbuf, SMALLMSGLEN-1, "SDSU returned actual value (0x%x)", actual_reply);
			sdsu_statusp->put(STATUS_MESSAGE, msgbuf, 0, strlen(msgbuf));
		}

		if (debug & SDSU_SHOW_COMMANDS)
			message_write("	returned value: 0x%x", actual_reply);

		// TODO: Apparently useless
		//		last_reply_len = 2;

		return actual_reply;
	}

	// Otherwise - was the last reply an error?
	if (actual_reply == SDSU_ERR) {

		// Yes - throw an exception, ie no point continuing
		if (debug & SDSU_SHOW_ERRORS)
			message_write("***ERROR");

		sdsu_errno = SDSU_REPLY_ERROR;
		snprintf(msgbuf, SMALLMSGLEN-1, "ERR REPLY,,r:%08x(%s)",
				sdsu_errno, current_command_str(actual_reply));

		throw Error(report_errno(SDSU_ERR,msgbuf), E_ERROR, SDSU_ERROR, __FILE__, __LINE__);

	} else if (actual_reply == expected_reply) {
		// No - we have the reply we expected, return successfully

		if (update_status_vars) {
			snprintf(msgbuf, SMALLMSGLEN-1, "SDSU returned expected reply (%s)", current_command_str(actual_reply));
			sdsu_statusp->put(STATUS_MESSAGE, msgbuf, 0, strlen(msgbuf));
		}

		if (debug & SDSU_SHOW_COMMANDS)
			message_write("     returned value: %s", current_command_str(actual_reply));

		// TODO: Apparently useless
		/*
		   last_reply_len = sdsu_last_header & 0xff;
		   last_reply_len = (last_reply_len > SDSU_MAX_REPLY_ARGS)? SDSU_MAX_REPLY_ARGS:last_reply_len;
		   */

		return SDSU_OK;

	} else if (!controller_abort) {
		// Or - we got an unexpected reply, throw an exception here as well...
		snprintf(msgbuf, SMALLMSGLEN-1, "BAD REPLY:%08x, wanted:%08x", actual_reply, expected_reply);
		message_write("BAD REPLY:%08x, wanted:%08x", actual_reply, expected_reply);
		throw Error(report_errno(local_errno,msgbuf), E_ERROR, local_errno, __FILE__,__LINE__);
	}

	return SDSU_OK;
}

/*
 *+
 * FUNCTION NAME: check_expected_reply
 *
 * INVOCATION: check_expected_reply(int reply, int expected_reply)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > reply - received reply
 * > expected_reply - expeted reply from previous command
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Check the reply returned from the interface board against the specified
 *	    expected reply value.
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Sdsu3_5_Controller::check_expected_reply(int reply, int expected_reply)
{
	sdsu_last_reply = reply;
	if (reply != expected_reply) {
		message_write("UNKNOWN reply, expected reply:%08x,actual reply:%08x", expected_reply, sdsu_last_reply);
		sdsu_errno = SDSU_REPLY_ERROR;
		return SDSU_ERROR;
	} else
		return SDSU_OK;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_poll
 *
 * INVOCATION: sdsu_poll(int board_id, int expected_reply)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - SDSU board identifier
 * > expected_reply - expeted reply from previous command
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Check to see if a reply has been received
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Sdsu3_5_Controller::sdsu_poll(int board_id, int expected_reply)
{
	int	expected_header;
	int	reply, which_board;

	expected_header = (board_id << 16) | 2;

	which_board = reply_buf[0] & REPLY_MASK;
	reply = reply_buf[1] & REPLY_MASK;

	if (reply == expected_reply) {
		if (which_board == expected_header)
			return SDSU_OK;
		else {
			sdsu_errno = SDSU_BAD_REPLY_HEADER;
			return SDSU_ERROR;
		}
	}
	else if (reply == SDSU_ERR) {
		sdsu_errno = SDSU_REPLY_ERROR;
		return SDSU_ERROR;
	}
	else
		return SDSU_WAITING;
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::read_sdsu_download
 *
 * INVOCATION: read_sdsu_download(int board, char *filename)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - SDSU board identifier
 * > filename - file to get DSP code from
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Read download files into configuration buffers
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5 <- Irrelevant in the new model
int Sdsu3_5_Controller::read_sdsu_download(int board, std::string& filename)
{
	/*

	   FILE	*download_fp;
	   int	finished, board_id, addr;
	   char	buf[255], addr_type;
	   Sdsu_Memory_Space mem_space;

	   if ((download_fp = fopen(filename, "r")) == NULL) {
	   sdsu_errno = SDSU_DOWNLOAD_OPEN_ERROR;
	   return SDSU_ERROR;
	   }

	   if ((board_id = sdsu_get_download_type(download_fp)) == SDSU_ERROR) {
	   sdsu_errno = SDSU_DOWNLOAD_FILE_ERROR;
	   return SDSU_ERROR;
	   }

	   if (board != board_id) {
	   sdsu_errno = SDSU_DOWNLOAD_TYPE_ERROR;
	   return SDSU_ERROR;
	   }

	   finished = FALSE;
	   while (!finished) {
	   sdsu_read_line(download_fp, buf);
	   if (strncmp(buf, "_END", 4) == 0) {
	   finished = TRUE;
	   } else if (sscanf(buf, "_DATA %c %x", &addr_type, &addr) == 2) {
	   if (addr < SDSU_DOWNLOAD_ADDR_MAX) {
	   if (addr_type=='P')
	   mem_space=SDSU_MEM_P;
	   else if (addr_type=='X')
	   mem_space=SDSU_MEM_X;
	   else if (addr_type=='Y')
	   mem_space=SDSU_MEM_Y;
	   else if (addr_type=='R')
	   mem_space=SDSU_MEM_R;

	   switch (board_id) {
	   case TIM_ID:
	   sdsu_process_data(download_fp, mem_space, addr, &sdsu_config.timing_prog);
	   break;
	   case UTIL_ID:
	   sdsu_process_data(download_fp, mem_space, addr, &sdsu_config.utility_prog);
	   break;
	   case INTERFACE_ID:
	   if (controller.bus == BUS_VME) {
	   sdsu_process_data(download_fp, mem_space, addr, &sdsu_config.interface_prog);
	   } else if (controller.bus == BUS_PCI) {
	   sdsu_process_data(download_fp, mem_space, addr, &sdsu_config.interface_prog);

	// For PCI files the first instruction is the count of the number of
	// words in this program, The second is the starting address (should
	// be 0) Stop getting instructions when this count is reached
	finished = (sdsu_config.interface_prog.n_instructions >= (sdsu_config.interface_prog.instruction[0].value + 2));
	}
	break;
	default:
	break;
	}
	}
	}
	}
	fclose(download_fp);

	// Print a confirmation message that DSP has been loaded
	switch (board_id) {
	case TIM_ID:
	message_write(" loaded timing dsp %s, #ins=%d...", filename, sdsu_config.timing_prog.n_instructions);
	break;
	case UTIL_ID:
	message_write(" loaded utility dsp %s, #ins=%d...", filename, sdsu_config.utility_prog.n_instructions);
	break;
	case INTERFACE_ID:
	message_write(" loaded interface dsp %s, #ins=%d...", filename, sdsu_config.interface_prog.n_instructions);
	break;
	default:
	message_write(" board id %d for dsp code %s not understood???", board_id, filename);
}
*/
return SDSU_OK;
}


/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_download
 *
 * INVOCATION: sdsu_download(int board, dsp_prog* dsp_prog)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - SDSU board identifier
 * > dsp_prog - actual DSP program stored im memory
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Download DSP code to SDSU controller
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
int Sdsu3_5_Controller::sdsu_download(int board, std::string& filename)
{
	if (sdsu_simulation != 0)
		return SDSU_OK;

	// bool destroy = false;

	if ((pDevice == nullptr) || !pDevice->IsOpen()) {
		return SDSU_ERROR;
		/*
		   destroy = true;
		   if (open_interface(true) == SDSU_ERROR) {
		   message_write("No available interface");
		   return SDSU_ERROR;
		   }
		   */
	}
	if (board != TIM_ID) {
		message_write("Only supporting timing dsp download");
		return SDSU_ERROR;
	}
	else {
		try {
			LockGuard lg(deviceLock);
			message_write("About to load timing dsp %s", filename.c_str());

			pDevice->LoadControllerFile(filename, true);
			message_write("Loaded timing dsp %s", filename.c_str());
		}
		catch (std::runtime_error e) {
			message_write("read_sdsu_download: Error trying to download %s: %s", filename.c_str(), e.what());
		}
	}

	/*
	   if (destroy)
	   close_interface();
	   */

	timing_loaded = true;

	return SDSU_OK;
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_read_line
 *
 * INVOCATION: sdsu_read_line(FILE *fp, char *buf)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > fp - file pointer
 * > buf - input buffer
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Moves a line of DSP code from file to buffer
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::sdsu_read_line(FILE *fp, char *buf)
{
	if (fgets(buf, DSP_PROG_LINE_LEN, fp) == NULL) {
		throw Error("Error on file input",E_ERROR,errno,__FILE__,__LINE__);
	}
}


/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_get_download_type
 *
 * INVOCATION: sdsu_get_download_type(FILE *fp)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > fp - file pointer
 *
 * FUNCTION VALUE: int
 *
 * PURPOSE: Determine download file type
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Sdsu3_5_Controller::sdsu_get_download_type(FILE *fp)
{
	char *cp, buf[DSP_PROG_LINE_LEN];
	int id;

	sdsu_read_line(fp, buf);
	if (strncmp(buf, "_START", 6) != 0) {
		return SDSU_ERROR;
	}

	cp = buf+7;
	switch (*cp){
		case 'T':
			id = TIM_ID;
			break;
		case 'U':
			id = UTIL_ID;
			break;
		case 'P':
			id = PCI_ID;
			break;
		case 'V':
			id = VME_ID;
			break;
		default:
			return SDSU_ERROR;
	}

	return id;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::sdsu_process_data
 *
 * INVOCATION: sdsu_process_data(FILE *download_fp, Sdsu_Memory_Space mem_space,
 *				int addr, DSP_prog* dsp_prog)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > download_fp - file pointer
 * > mem_space - type of SDSU memory to download to
 * > addr - address to download program into
 * > dsp_prog - memory version of program to load in
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Process a DSP file into an internal memory representation
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::sdsu_process_data(FILE *download_fp, Sdsu_Memory_Space mem_space,
		int addr, DSP_prog* dsp_prog)
{
	const int LF = 10;
	const int CR = 13;
	const int SPACE = 32;
	bool finished;
	int value;
	char c;

	finished = false;
	while (!finished) {
		while ((c = getc(download_fp)) == SPACE);
		if (c == '_')  {
			ungetc(c, download_fp);
			finished = true;
		} else if ((c != LF) && (c != CR)){
			ungetc(c, download_fp);
			fscanf(download_fp, "%x", &value);

			if (dsp_prog->n_instructions>SDSU_MAX_DSP_INSTRUCTIONS)
				throw Error("Too many program instructions",E_ERROR, ENOMEM, __FILE__,__LINE__);
			dsp_prog->instruction[dsp_prog->n_instructions].mem = mem_space;
			dsp_prog->instruction[dsp_prog->n_instructions].address = addr;
			dsp_prog->instruction[dsp_prog->n_instructions].value = value;
			dsp_prog->n_instructions++;
			addr++;
		}
	}
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::report_errno
 *
 * INVOCATION: report_errno(int local_errno, char* msg)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > local_errno - local SDSU erro code
 * > msg - error message
 *
 * FUNCTION VALUE: char*
 *
 * PURPOSE: report an SDSU error by compiling an error string
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
char* Sdsu3_5_Controller::report_errno(int local_errno, char* msg)
{
	char errmsg[MSGLEN];

	sdsu_statusp->put(REPLY_STATUS, local_errno);
	switch (local_errno) {
		case ENOMEM:
			strlcpy(errmsg,"Allocation of buffers failed",MSGLEN);
			break;
		case ECHILD:
			strlcpy(errmsg,"All the image buffers are busy",MSGLEN);
			break;
		case ENOTEMPTY:
			strlcpy(errmsg,"Bad interrupt encountered",MSGLEN);
			break;
		case ETIME:
			strlcpy(errmsg,"Data transfer time out",MSGLEN);
			break;
		case EDEADLK:
			strlcpy(errmsg,"Image buffer not busy",MSGLEN);
			break;
		case ENOLCK:
			strlcpy(errmsg,"Data transfer error",MSGLEN);
			break;
		case EPIPE:
			strlcpy(errmsg,"Copy error to user space",MSGLEN);
			break;
		case EPERM:
			strlcpy(errmsg,"Image buffer not done",MSGLEN);
			break;
		case ENXIO:
			strlcpy(errmsg,"There is no DMA device",MSGLEN);
			break;
		case ENOTBLK:
			strlcpy(errmsg,"DMA not busy",MSGLEN);
			break;
		case SDSU_ERR:
			strlcpy(errmsg,"DSP returned ERR message",MSGLEN);
			break;
		default:
			sdsu_statusp->put(REPLY_STATUS, sdsu_errno);
			switch (sdsu_errno) {
				case OPEN_INTERFACE_ERROR:
					strlcpy(errmsg,"Error opening SDSU interface", MSGLEN);
					break;
				case CLOSE_INTERFACE_ERROR:
					strlcpy(errmsg,"Error closing SDSU interface", MSGLEN);
					break;
				case SDSU_BAD_REPLY_HEADER:
					strlcpy(errmsg,"Bad reply header from SDSU controller", MSGLEN);
					break;
				case SDSU_BAD_REPLY:
					strlcpy(errmsg,"Bad reply from SDSU controller", MSGLEN);
					break;
				case SDSU_REPLY_ERROR :
					strlcpy(errmsg,"ERR reply from SDSU controller", MSGLEN);
					break;
				case SDSU_DOWNLOAD_OPEN_ERROR:
					strlcpy(errmsg,"Error opening download DSP file", MSGLEN);
					break;
				case SDSU_DOWNLOAD_FILE_ERROR:
					strlcpy(errmsg,"Format error in download DSP file", MSGLEN);
					break;
				case SDSU_DOWNLOAD_TYPE_ERROR:
					strlcpy(errmsg,"Wrong download type in DSP file", MSGLEN);
					break;
				default:
					// Only handle the above local_errno - for others just continue.
					strlcpy(errmsg,"SDSU system error - unknown type?",MSGLEN);
			}
			break;
	}
	snprintf(msg,MSGLEN-1,"%s (%s)",errmsg, current_command_buf);

	sdsu_statusp->put(STATUS_MESSAGE, msg, 0, min(strlen(msg), (long unsigned)SMALLMSGLEN));
	return msg;
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::current_command_str
 *
 * INVOCATION: current_command_str(int cmd)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cmd - cmd to convert to ascii
 *
 * FUNCTION VALUE: char*
 *
 * PURPOSE: translate current command into string name
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
char* Sdsu3_5_Controller::current_command_str(int cmd)
{
	int i;

	strlcpy(current_command_buf,"???", 4);
	for (i=0; i<3; i++)
		current_command_buf[i] = cmd >> (16-i*8);
	return current_command_buf;
}

/******************************************************************************
  End  of  D S P  routines
 ******************************************************************************/
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::execute_long_test
 *
 * INVOCATION: execute_long_test(int value, int  board_id, Sdsu_Cmd command)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > value - value to write in test
 * > board_id - board identifier
 * > command - command to use for test
 *
 * FUNCTION VALUE: int
 *
 * PURPOSE: run the long hardware test
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Sdsu3_5_Controller::execute_long_test(int value, int board_id, Sdsu_Cmd command)
{
	int errorno, number, i, sendnum, addnum, feedback, expected_header;
	bool save_update = update_status_vars;

	errorno = 0;
	sendnum = 0;
	sdsu_bad_header_count = 0;

	// generate number to test all 24 bits
	if (value<1000)
		value = 1000;
	addnum = (16777000/value);

	// For this series of TDL commands do not update status variables every
	// time - too slow! So clear flag for updating status flags and set flags to
	// expected values.  If there is a failure then these will be set
	// appropriately during the download
	expected_header = (board_id<<16)|2;
	sdsu_statusp->put(EXPECTED_HEADER, expected_header);
	sdsu_statusp->put(EXPECTED_REPLY, value);
	sdsu_statusp->put(LAST_HEADER, expected_header);
	sdsu_statusp->put(LAST_REPLY, value);
	sdsu_statusp->put(REPLY_STATUS, 0);
	sdsu_statusp->put(STATUS_MESSAGE, "\0", 0, 1);
	sdsu_statusp->put(SDSU_COMMAND, TDL);
	update_status_vars = false;

	// Takes ~2 secs for each 1000 tests, lets make sure we
	// report at least every minute
	feedback = (value/10>30000)? 30000:value/10;

	for (i=0; ((i<value) && (!controller_abort)); i++) {
		sendnum = (sendnum + addnum);

		// Test progress
		if ((i%feedback)==0)
			message_write("   test # %d, value sent:     %08x", i,sendnum);

		try {
			number = sdsu_command(board_id, command, sendnum);
		}
		catch (Error &e) {
			number = -1;
			if (sdsu_errno == SDSU_BAD_REPLY_HEADER) {
				sdsu_bad_header_count++;
			} else {
				sdsu_bad_header_count = 0;
				e.rethrow();
			}
		}
		if (number != sendnum) {
			errorno++;
			if (errorno<=10){
				message_write("            value sent:     %08x", sendnum);
				message_write("            value received: %08x", number);
			}
		}
	}
	update_status_vars = save_update;
	sdsu_bad_header_count = 0;
	return errorno;
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_hardware_test
 *
 * INVOCATION: do_hardware_test(int short_test, int disp_msg)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > short_test - set if only short version wanted
 * > disp_msg - set if required to display progress messages
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Do a hardware test
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5: PENDING UNNEEDED
void Sdsu3_5_Controller::do_hardware_test(int short_test, int disp_msg, int ntests)
{
	message_write("******* ERROR: Hardware Test not implemented (not in the config)");
	/*
	   int i, value, errorno, toterr, total;
	   const int NTESTS = 10;

	   if (ntests==0)
	   ntests = NTESTS;

	   if (disp_msg > DISPLAY_MIN_MESGS)
	   message_write("Doing hardware test...");

	   if (short_test) {
	   toterr = errorno = total = 0;

	   if (disp_msg >= DISPLAY_MIN_MESGS)
	   message_write("     Testing interface board...");
	   total += ntests;
	   errorno = 0;
	   for (i=1; i<=ntests; i++) {
	   value = sdsu_command(INTERFACE_ID, SDSU_TDL, i);
	   if ((value != i) && (value != SDSU_ABORTED)) {
	   errorno++;
	   if (disp_msg > DISPLAY_MIN_MESGS) {
	   message_write("            value sent:     %08x", i);
	   message_write("            value received: %08x", value);
	   }
	   }
	   }
	   toterr = toterr + errorno;
	   if ((errorno != 0) || (disp_msg >= DISPLAY_MIN_MESGS))
	   message_write("     TDL errors for interface board:  %d of %d", errorno, ntests);

	   if (disp_msg >= DISPLAY_MIN_MESGS)
	   message_write("     Testing Timing Board...");
	   total += ntests;
	   errorno = 0;
	   for (i=1; i<=ntests; i++) {
	   value = sdsu_command(TIM_ID, SDSU_TDL, i);
	   if ((value != i) && (value != SDSU_ABORTED)) {
	   errorno++;
	   if (disp_msg > DISPLAY_MIN_MESGS) {
	   message_write("            value sent:     %08x", i);
	   message_write("            value received: %08x", value);
	   }
	   }
	   }
	   toterr = toterr + errorno;
	   if ((errorno != 0) || (disp_msg >= DISPLAY_MIN_MESGS))
	   message_write("     TDL errors for Timing board:  %d of %d", errorno, ntests);

	   if (sdsu_config.config_data.has_utility_board) {
	   if (disp_msg >= DISPLAY_MIN_MESGS)
	   message_write("     Testing Utility board...");
	   total += ntests;
	   errorno = 0;
	   for (i=1; i<=ntests; i++) {
	   value = sdsu_command(UTIL_ID, SDSU_TDL, i);
	   if ((value != i) && (value != SDSU_ABORTED)) {
	   errorno++;
	   if (disp_msg > DISPLAY_MIN_MESGS) {
	   message_write("            value sent:     %08x", i);
	   message_write("            value received: %08x", value);
	   }
	   }
	   }
	   toterr = toterr + errorno;
	   if ((errorno != 0) || (disp_msg >= DISPLAY_MIN_MESGS))
	   message_write("     TDL errors for Utility board:  %d of %d", errorno, ntests);
	   }

	// Now test Coadder if DSP to timing board has been loaded
	if ((sdsu_config.config_data.has_coadder_board) && (timing_loaded)) {
	if (disp_msg >= DISPLAY_MIN_MESGS)
		message_write("     Testing CoAdder board...");
	total += ntests;
	errorno = 0;
	for (i=1; i<=ntests; i++) {
		value = sdsu_command(TIM_ID, SDSU_CAT, i);
		if ((value != i) && (value != SDSU_ABORTED)) {
			errorno++;
			if (disp_msg > DISPLAY_MIN_MESGS) {
				message_write("            value sent:     %08x", i);
				message_write("            value received: %08x", value);
			}
		}
	}
	toterr = toterr + errorno;
	if ((errorno != 0) || (disp_msg >= DISPLAY_MIN_MESGS))
		message_write("     TDL errors for CoAdder board:  %d of %d", errorno, ntests);
}

if (disp_msg > DISPLAY_MIN_MESGS)
	message_write("TDL errors for all boards:  %d of %d", toterr, total);
	} else { // Long test
		// Test coomms to the interface card DSP if present
		if (sdsu_config.config_data.interface_setting) {
			message_write("     Testing interface board (%d)...",sdsu_config.config_data.interface_setting);
			message_write("     Number of errors for interface board:  %d of %d",
					execute_long_test(sdsu_config.config_data.interface_setting, INTERFACE_ID, SDSU_TDL),
					sdsu_config.config_data.interface_setting);
		}

		if (sdsu_config.config_data.timing_setting) {
			message_write("     Testing Timing board (%d)...",sdsu_config.config_data.timing_setting);
			message_write("     Number of errors for Timing board:  %d of %d",
					execute_long_test(sdsu_config.config_data.timing_setting,TIM_ID,SDSU_TDL),
					sdsu_config.config_data.timing_setting);
		}

		if (sdsu_config.config_data.utility_setting && sdsu_config.config_data.has_utility_board) {
			message_write("     Testing Utility board (%d)...",sdsu_config.config_data.utility_setting);
			message_write("     Number of errors for Utility board:  %d of %d",
					execute_long_test(sdsu_config.config_data.utility_setting,UTIL_ID,SDSU_TDL),
					sdsu_config.config_data.utility_setting);
		}

		if (sdsu_config.config_data.coadder_setting && sdsu_config.config_data.has_coadder_board) {
			message_write("     Testing CoAdder board (%d)...",sdsu_config.config_data.coadder_setting);
			message_write("     Number of errors for CoAdder board:  %d of %d",
					execute_long_test(sdsu_config.config_data.coadder_setting,TIM_ID,SDSU_CAT),
					sdsu_config.config_data.coadder_setting);
		}
	}
if (disp_msg > DISPLAY_MIN_MESGS)
	message_write("Hardware test complete.");
	*/
	}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_power_on()
 *
 * INVOCATION: do_power_on()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: do power on
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::do_power_on()
{
	message_write("Turning SDSU controller power on...");
	if (sdsu_command(TIM_ID, SDSU_PON) != SDSU_OK)
		message_write("Power on can not be completed.");
	else
		message_write("     power on complete.");
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::check_idle()
 *
 * INVOCATION: check_idle()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE:  Check to see if IDL mode is on
 *
 * DESCRIPTION:
 * Reads the X mem idle state bit for IDL mode
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *- */
// UPDATED TO 3.5
bool Sdsu3_5_Controller::check_idle()
{
	int value;
	value = sdsu_command(TIM_ID, SDSU_RDM, 0, SDSU_MEM_X, SDSU_0_PAR);
	return (value & 0x01);
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_timing_board
 *
 * INVOCATION:do_timing_board()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE:  Do timing board
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::do_timing_board()
{
	epicsTime stime;
	std::string fname;

	fname = sdsu_path(sdsu_config.config_data.timing_filename,controller_dir);

	message_write("Configuring Timing board...");

	sdsu_download(TIM_ID, fname);
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_utility_board
 *
 * INVOCATION: do_utility_board()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE:  Do utility board
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED to 3.5 <- We do not have a utility board
void Sdsu3_5_Controller::do_utility_board()
{
	/*
	   int	ret_value;
	   epicsTime stime;
	   double dtime;

	   message_write("Configuring Utility board...");

	   stime = epicsTime::getCurrent();
	// Download filename to utility board
	message_write("     downloading %s, #ins=%d...", sdsu_config.config_data.utility_filename, sdsu_config.utility_prog.n_instructions);
	ret_value = sdsu_download(UTIL_ID, &sdsu_config.utility_prog);

	dtime = epicsTime::getCurrent() - stime;
	if (ret_value == SDSU_ERROR)
	message_write("Utility board can not be configured.");
	else {
	message_write("Utility board configured, time=%f secs",dtime/(double)timestamp_res());

	}
	*/
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_pci_board
 *
 * INVOCATION:do_pci_board()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE:  Do PCI board DSP code download
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::do_pci_board()
{
	epicsTime stime;
	double dtime;
	int reply = 0;
	int i, n, addr, expected_header;
	char msgbuf[MSGLEN];
	int save_debug = debug;
	bool save_update = update_status_vars;

	message_write("Configuring PCI board...");

	stime = epicsTime::getCurrent();

	// Download filename to pci board
	message_write("     Setting up the PCI board for a download...");


	// Before we start, do some basic checking - because mistakes in loaded PCI
	// code here could render the system unusable. The first two instructions
	// should be the number of words in the DSP program followed by the address -
	// for the PCI addr should be 0.
	if (sdsu_config.interface_prog.n_instructions < 2) {
		snprintf(msgbuf, MSGLEN, "Sdsu_Controller: PCI DSP program looks empty?, #instructions=%d!",
				sdsu_config.interface_prog.n_instructions);
		throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	}

	n = sdsu_config.interface_prog.instruction[0].value;
	addr = sdsu_config.interface_prog.instruction[1].value;

	if (sdsu_config.interface_prog.n_instructions != n+2) {
		snprintf(msgbuf, MSGLEN, "Sdsu_Controller: Incorrect instruction count in PCI DSP program?, #=%d, expected=%d!",
				sdsu_config.interface_prog.n_instructions, n+2);
		throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	}

	if (addr != 0) {
		snprintf(msgbuf, MSGLEN, "Sdsu_Controller: Incorrect PCI DSP program starting addr?, addr=0x%x, expected=0x0!", addr);
		throw Error(msgbuf, E_ERROR, EINVAL, __FILE__, __LINE__);
	}

	// OK that is all we can do now - so here goes...

	// Set the PCI board to slave mode.
	send_pci_cmd(CArcPCI::ASTROPCI_GET_HCTR, 0);

	// Clear the HTF bits and bit 3.
	reply = reply_buf[0] & SDSU_PCI_HTF_CLEAR_MASK & SDSU_PCI_BIT3_CLEAR_MASK;

	// Set the HTF bits.
	reply = reply | SDSU_PCI_HTF_MASK;

	send_pci_cmd(CArcPCI::ASTROPCI_SET_HCTR, reply);

	// Inform the DSP that new pci boot code will be downloaded.
	send_pci_cmd(CArcPCI::ASTROPCI_PCI_DOWNLOAD, 0);

	// Set the magic value that says this is a PCI download.
	send_pci_cmd(CArcPCI::ASTROPCI_HCVR_DATA, SDSU_PCI_DOWNLOAD);

	message_write("     downloading %s, #ins=%d...", sdsu_config.config_data.interface_filename, sdsu_config.interface_prog.n_instructions);

	// Make sure we really want to see debugging from a download
	// This will be hefty - turn off temporarily if not
	if (debug & SDSU_SHOW_COMMANDS) {
		if (!(debug & SDSU_SHOW_DOWNLOAD))
			debug = debug ^ SDSU_SHOW_COMMANDS;
	}

	// For this series of WRM/RDM commands do not update status variables every
	// time - too slow! So clear flag for updating status flags and set flags to
	// expected values.  If there is a failure then these will be set
	// appropriately during the download
	expected_header = (PCI_ID<<16)|2;
	sdsu_statusp->put(EXPECTED_HEADER, expected_header);
	sdsu_statusp->put(EXPECTED_REPLY, DON);
	sdsu_statusp->put(LAST_HEADER, expected_header);
	sdsu_statusp->put(LAST_REPLY, DON);
	sdsu_statusp->put(REPLY_STATUS, 0);
	sdsu_statusp->put(STATUS_MESSAGE, "\0", 0, 1);
	sdsu_statusp->put(SDSU_COMMAND, WRM);
	update_status_vars = false;

	for (i=0; i<sdsu_config.interface_prog.n_instructions; i++) {
		send_pci_cmd(CArcPCI::ASTROPCI_HCVR_DATA, sdsu_config.interface_prog.instruction[i].value);
	}

	// Restore debug  and update_status_vars to entry values
	debug = save_debug;
	update_status_vars = save_update;

	// Now set the PCI board data transfer format (Set HTF bits to 00).
	send_pci_cmd(CArcPCI::ASTROPCI_GET_HCTR, 0);
	reply = (reply_buf[0] & SDSU_PCI_HTF_CLEAR_MASK) | 0x900;
	send_pci_cmd(CArcPCI::ASTROPCI_SET_HCTR, reply);

	// Wait for the PCI DSP to finish initialization.
	send_pci_cmd(CArcPCI::ASTROPCI_PCI_DOWNLOAD_WAIT, 0);

	dtime = epicsTime::getCurrent() - stime;

	if ((int)reply_buf[0] != DON) {
		message_write("ERROR: Bad reply, expected: DON (0x444F4E), got: 0x%X\n", reply_buf[0]);
		message_write("PCI Board mis-configured - system may need rebooting/power cycling!");
	} else
		message_write("PCI Board configured, time=%f secs", dtime/(double)timestamp_res());

}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::set_pci_hardware_byte_swapping
 *
 * INVOCATION: status = set_pci_hardware_byte_swapping()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: Either SDSU_OK or SDSU_ERROR
 *
 * PURPOSE:  Set hardware byte swapping from PCI board if available
 *
 * DESCRIPTION:

 * Checks if hardware byte swapping of image data is available and turns it
 * ON if it is.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Sdsu3_5_Controller::set_pci_hardware_byte_swapping()
{
	message_write("Turning on hardware byte swapping ...");

	byte_swapping = false;

	// Check if byte swapping is available
	if (sdsu_command(PCI_ID, SDSU_TBS) == SDSU_ERROR)
		return SDSU_ERROR;

	// Turn hardware byte swapping ON
	if (sdsu_command(PCI_ID, SDSU_SBS, 1) == SDSU_ERROR)
		return SDSU_ERROR;

	byte_swapping = true;

	return SDSU_OK;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_idle
 *
 * INVOCATION: do_idle(bool enable)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > enable - set if idle mode required
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:  Sets idle mode
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::do_idle(bool enable)
{
	int ret_value;
	idle_turned_off = true;

	if (enable) {
		message_write("Setting idle...");
		ret_value = sdsu_command(TIM_ID, SDSU_IDL);
		if (ret_value != SDSU_OK)
			message_write("Idle can not be set to 'Yes'.");
		else {
			message_write("Idle set to 'Yes'.");
			idle_turned_off = false;
		}
	} else {
		ret_value = sdsu_command(TIM_ID, SDSU_STP);

		if (ret_value != SDSU_OK)
			message_write("Idle can not be set to 'No'.");
		else {
			message_write("Idle set to 'No'.");
		}
	}
}
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_voltages
 *
 * INVOCATION: do_voltages()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:  Set required voltages on controller
 *
 * DESCRIPTION: Set the voltages which have the "set this voltage"
 *              flag TRUE.  Need to set them in order too.
 * The voltages are passed as an array of Volts_Desc structs.
 * Each Volts_Desc can contain one or two actual voltages
 * depending on the board and voltage type (two voltages unless
 * we have a BIAS voltage on the VID board).
 * The algorithm in use here is to build an array of individual
 * voltages (i.e. split up the pairs) and then to sort that
 * into ascending order of "set_order".
 * We then pass through the new array and set any voltages which
 * have the set_this_voltage flag TRUE.
 *
 * EXTERNAL VARIABLES: uses the voltages array in config struct
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::do_voltages()
{
	char lowhigh[MSGLEN];
	char boardtype[MSGLEN];
	Volts_Desc vd;
	Sdsu_Voltage_Cmd voltage_cmd;
	long delay=0;

	Sdsu_Volt v[SDSU_MAX_VOLTS*2];
	Sdsu_Volt temp_v;

	int i,j,max, n_volts;

	message_write("Setting %d voltages ...", sdsu_config.n_volts);

	memset((void*)v, 0, sizeof(Sdsu_Volt)*SDSU_MAX_VOLTS*2);

	// Build array of individual voltages from array of Volts_Desc's
	for (i=0, j=0; (i<sdsu_config.n_volts) && (i<SDSU_MAX_VOLTS); i++) {
		vd = sdsu_config.volts[i];
		if ((vd.min_volt[V_LO] == 0.0) &&
				(vd.max_volt[V_LO] == 0.0) &&
				(vd.value[V_LO] == 0.0)) {
			// end of array
			break;
		}
		strlcpy(v[j].name,vd.name,NAMELEN);
		v[j].low = TRUE;
		v[j].set_order = vd.set_order[V_LO];
		v[j].set_delay = vd.set_delay[V_LO];
		v[j].set_this_voltage = vd.set_this_voltage;
		v[j].type = vd.type;
		v[j].board_type = vd.board_type;
		v[j].board_addr = vd.board_addr;
		v[j].dsp_addr = vd.dsp_addr;
		v[j].dac_addr = vd.dac_addr[V_LO];
		v[j].value = vd.value[V_LO];

		// calculate the number of ADUs to set to
		v[j].dacval = calc_adu_val(vd,V_LO,vd.value[V_LO]);
		j++;
		if ((vd.type != SDSU_BIAS) ||
				(vd.board_type != SDSU_VIDEO_BOARD)) {
			strlcpy(v[j].name,vd.name,NAMELEN);
			v[j].low = FALSE;
			v[j].set_order = vd.set_order[V_HI];
			v[j].set_delay = vd.set_delay[V_HI];
			v[j].set_this_voltage = vd.set_this_voltage;
			v[j].type = vd.type;
			v[j].board_type = vd.board_type;
			v[j].board_addr = vd.board_addr;
			v[j].dsp_addr = vd.dsp_addr;
			v[j].dac_addr = vd.dac_addr[V_HI];
			v[j].value = vd.value[V_HI];

			// calculate the number of ADUs to set to
			v[j].dacval = calc_adu_val(vd,V_HI,vd.value[V_HI]);
			j++;
		}
	}

	// Store number of volts in array
	n_volts = j;

	// Now need to sort this array by set_order
	for (i = n_volts-1; i > 0; i--) {
		max = 0;
		for (j = 0; j <= i; j++)
			if (v[max].set_order < v[j].set_order)
				max = j;
		temp_v = v[i];
		v[i] = v[max];
		v[max] = temp_v;
	}

	// Now run through the array and set the voltages
	for (i=0, j=0; i<n_volts; i++) {
		if (v[i].set_this_voltage) {
			j++;

			// If we have previously set a voltage, may need to wait a while for it
			// (and others) to settle before setting this one.
			if (delay > 0) {
				message_write("Waiting for %d ms",delay);
				rest(delay);
				delay = 0;
			}

			if ((v[i].type != SDSU_BIAS) ||
					(v[i].board_type != SDSU_VIDEO_BOARD)) {
				if (v[i].low)
					strlcpy(lowhigh,"low", MSGLEN);
				else
					strlcpy(lowhigh,"high", MSGLEN);
			} else {
				lowhigh[0] = '\0';
			}

			if(v[i]. board_type == SDSU_VIDEO_BOARD)
				strlcpy(boardtype, "VID", MSGLEN);
			else
				strlcpy(boardtype, "CLK", MSGLEN);

			message_write("Setting %s %s (board %d, DAC %d, type %s) to %f volts %d adus",
					v[i].name,lowhigh,v[i].board_addr,v[i].dac_addr,boardtype,v[i].value,v[i].dacval);
			voltage_cmd.dac_board = v[i].board_addr;
			voltage_cmd.dac_number = v[i].dac_addr;
			voltage_cmd.dac_board_type = v[i].board_type;
			voltage_cmd.dac_voltage = v[i].dacval;

			// Now set the voltage
			if (sdsu_command(TIM_ID,SDSU_SBN,voltage_cmd) != SDSU_OK) {
				message_write("Failed to set voltage");
			}

			// increment the amount to delay before setting the next voltage
			delay += v[i].set_delay;

		} else { // if set_this_voltage
			// Need to accumulate delays of unset voltages, but only after we have set
			// the first one (i.e. no delays until after the first voltsge is set).
			if (j > 0)
				delay += v[i].set_delay;
		}
	}
	message_write("... finished setting %d voltages",j);

}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_DSP_code_setup
 *
 * INVOCATION: do_DSP_code_setup()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:  Download DSP code to boards as required
 *
 * DESCRIPTION:
 * Uses the hardware setup toggles to optionally download DSP code
 * to each of the SDSU controller boards. re-=opens the interface
 * first.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::do_DSP_code_setup()
{
	int ret_value;
	bool bus_pci = controller.bus == BUS_PCI || controller.bus == BUS_PCIe;

	// Close and Reopen interface
	close_interface();

	// Only reset if we are not downloading the interface code
	// because a download will force a reset anyway
	prepare_interface(!sdsu_config.config_data.do_interface_download);

	// Download pci file? - do this before doing a reset
	if (bus_pci && (sdsu_config.config_data.do_interface_download) && !controller_abort){
		do_pci_board();
	}

	// Reset the controller
	if ((sdsu_config.config_data.do_reset) && !controller_abort){
		message_write("Resetting controller...");
		do_reset();
	}

	// Perform hardware tests?
	if (((sdsu_config.config_data.do_long_hardware_test)||
				(sdsu_config.config_data.do_short_hardware_test))
			&& !controller_abort){
		do_hardware_test(sdsu_config.config_data.do_short_hardware_test, DISPLAY_ALL_MESGS);
	}

	// Download timing file?
	if ((sdsu_config.config_data.do_timing_download) && !controller_abort){
		do_timing_board();
	}

	// Download utility file?
	if (sdsu_config.config_data.has_utility_board) {
		if ((sdsu_config.config_data.do_utility_download) && !controller_abort){
			do_utility_board();
		}
	}

	// Has this controller got a coadder card?
	if (sdsu_config.config_data.has_coadder_board) {
		// Ok - download DSP to it and setup
		if (sdsu_config.config_data.do_coadder_download) {
			message_write("Downloading IR CoAdder board...");
			ret_value = sdsu_command(TIM_ID, SDSU_DCA);
			if (ret_value != SDSU_OK) {
				message_write("Failed to download CoAdder from timing SRAM, err=%d", ret_value);
			}
		}
		if (sdsu_config.config_data.do_coadder_init) {
			message_write("Initialising IR CoAdder board...");
			ret_value = sdsu_command(TIM_ID, SDSU_ICA);
			if (ret_value != SDSU_OK) {
				message_write("Failed to initialise CoAdder, err=%d", ret_value);
			}
		}
		if (sdsu_config.config_data.do_coadder_setup) {
			message_write("Setup IR CoAdder board...");
			ret_value = sdsu_command(TIM_ID, SDSU_SCA);
			if (ret_value != SDSU_OK) {
				message_write("Failed to setup CoAdder, err=%d", ret_value);
			}
		}
	}

	// Finally get the DSP version number just downloaded
	sdsu_statusp->put(CONTROLLER_SWV, get_dsp_version(util_id), 0, NAMELEN);

}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_set_boards()
 *
 * INVOCATION: do_set_boards()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: set board ids for various control functions
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::do_set_boards()
{
	// Some hardware configurations don't have a utility card - utility cmds
	// are handled by the timing card - set this up here
	if (sdsu_config.config_data.has_utility_board)
		util_id = shutter_control_board = UTIL_ID;
	else {
		util_id = shutter_control_board = TIM_ID;
	}

	// SDSU3 always does shutter control
	shutter_control_board = TIM_ID;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_setup
 *
 * INVOCATION: do_setup()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE:  Perform a complete hardware setup
 *
 * DESCRIPTION:
 * Uses the hardware setup toggles to optionally perform steps in an SDSU setup
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::do_setup()
{
	try {
		message_write("Starting setup...");

		// Download DSP code
		do_DSP_code_setup();

		// Turn power on?
		if (sdsu_config.config_data.do_power_on &&
				!controller_abort){
			rest(HALF_SECOND);
			do_power_on();
		}


		// Set gain or TC?
		if (sdsu_config.config_data.do_gain &&
				!controller_abort) {
			do_gain();
		} else if (sdsu_config.config_data.do_tc &&
				!controller_abort) {
			do_tc();
		}

		// Set idle mode?
		if (sdsu_config.config_data.do_idle &&
				!controller_abort) {
			do_idle(sdsu_config.config_data.idle_set);
		}

		// Set voltages?
		if (sdsu_config.config_data.do_voltages &&
				!controller_abort) {
			do_voltages();
		}

		message_write("Setup complete.");
	}
	catch (Error& ce) {
		msg_severity = ce.type;
		message_write(ce.record_error(__FILE__,__LINE__));
		ce.rethrow();
	}

}

// Direct D S P methods
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::send_direct_sdsu_command
 *
 * INVOCATION: send_direct_sdsu_command(Sdsu_Dsp direct_dsp)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: int
 *
 * PURPOSE:  Send a command directly to DSP code
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// PENDING A FEW THINGS
int Sdsu3_5_Controller::send_direct_sdsu_command(Sdsu_Dsp direct_dsp)
{
	int return_value=0;
	int dma_value=0;
	Sdsu_Cmd cmd = direct_dsp.dsp_data.cmd;
	// Sdsu_Data_Cmd data_cmd = direct_dsp.dsp_data.Sdsu_Direct_u.data_cmd;
	// Sdsu_Mem_Cmd mem_cmd = direct_dsp.dsp_data.Sdsu_Direct_u.mem_cmd;
	// Sdsu_Voltage_Cmd voltage_cmd = direct_dsp.dsp_data.Sdsu_Direct_u.voltage_cmd;
	// Sdsu_Mux_Cmd mux_cmd = direct_dsp.dsp_data.Sdsu_Direct_u.mux_cmd;

	send_interface_reset();

	switch (cmd) {
		case SDSU_GET_PROGRESS:
			update_total_readout_bytes();
			bytes_remaining = total_readout_bytes - (astro_state.last_pixel_counter * 2);
			dma_value = 0;
			break;
		case SDSU_READ_RUNNING:
			update_readout_running();
			dma_value = 0;
			break;
		case SDSU_INTERFACE_RESET:
			return_value = sdsu_command(cmd);
			break;
		case SDSU_RST:
		case SDSU_RET:
			return_value = sdsu_command(direct_dsp.board, cmd);
			break;
			/*
			   case SDSU_LDA:
			   case SDSU_TDL:
			   case SDSU_CAT:
			   case SDSU_SRF:
			   case SDSU_SFS:
			   case SDSU_NRW:
			   case SDSU_NCL:
			   case SDSU_SUR:
			   case SDSU_SRM:
			   case SDSU_SRD:
			   case SDSU_SNR:
			   case SDSU_CDS:
			   case SDSU_SET:
			   case SDSU_WOP:
			   if (data_cmd.data[0] == UNDEFINED){
			   throw Error("You must specify a data value.", E_ERROR,0,__FILE__,__LINE__);
			   }
			   return_value = sdsu_command(direct_dsp.board, cmd, data_cmd.data[0]);
			   break;
			   case SDSU_DFS:
			   if ((data_cmd.data[0] == UNDEFINED) || (data_cmd.data[1] == UNDEFINED)) {
			   throw Error("You must specify two data values.", E_ERROR,0,__FILE__,__LINE__);
			   }
			   return_value = sdsu_command(direct_dsp.board, cmd, data_cmd.data[0], data_cmd.data[1]);
			   break;
			   case SDSU_RDM:
			   if (mem_cmd.address == UNDEFINED) {
			   throw Error("You must specify an address value.", E_ERROR, 0, __FILE__,__LINE__);
			   }
			   return_value = sdsu_command(direct_dsp.board, cmd, 0, mem_cmd.memory_space, mem_cmd.address);
			   break;
			   case SDSU_WRM:
			   if (mem_cmd.address == UNDEFINED) {
			   throw Error("You must specify an address value.", E_ERROR, 0, __FILE__,__LINE__);
			   }
			   if (mem_cmd.value == UNDEFINED){
			   throw Error("You must specify a data value.", E_ERROR,0,__FILE__,__LINE__);
			   }
			   return_value = sdsu_command(direct_dsp.board, cmd, mem_cmd.value, mem_cmd.memory_space, mem_cmd.address);
			   break;
			   case SDSU_SBN:
			   if (voltage_cmd.dac_board == UNDEFINED) {
			   throw Error("You must specify a DAC board address value.", E_ERROR, 0, __FILE__,__LINE__);
			   }
			   if (voltage_cmd.dac_number == UNDEFINED) {
			   throw Error("You must specify a DAC number.", E_ERROR, 0, __FILE__,__LINE__);
			   }
			   if (voltage_cmd.dac_board_type == UNDEFINED) {
			   throw Error("You must specify a DAC board type.", E_ERROR, 0, __FILE__,__LINE__);
			   }
			   if (voltage_cmd.dac_voltage == UNDEFINED) {
			   throw Error("You must specify a DAC voltage.", E_ERROR, 0, __FILE__,__LINE__);
			   }
			   return_value = sdsu_command(direct_dsp.board, cmd, voltage_cmd);
			   break;
			   case SDSU_SMX:
			   if (mux_cmd.clk_board == UNDEFINED) {
			   throw Error("You must specify a CLK board value.", E_ERROR, 0, __FILE__,__LINE__);
			   }
			   if (mux_cmd.mux1 == UNDEFINED) {
			   throw Error("You must specify MUX1.", E_ERROR, 0, __FILE__,__LINE__);
			   }
			   if (mux_cmd.mux2 == UNDEFINED) {
			   throw Error("You must specify MUX2.", E_ERROR, 0, __FILE__,__LINE__);
			   }
			   return_value = sdsu_command(direct_dsp.board, cmd, mux_cmd);
			   break;
			   */
		default:
			return_value = sdsu_command(direct_dsp.board, cmd);
			break;
	}

	switch (return_value) {
		case SDSU_ERROR:
			message_write("Reply: ERR");
			break;
		case SDSU_OK:
			if (cmd == SDSU_POF)
				message_write("Reply: Power Off DON Ok.");
			else
				message_write("Reply: DON");
			break;
		case SDSU_NO_EXPECTED_REPLY:
			message_write("Reply: None Expected");
			break;
		default:
			if (dma_value) {
				message_write("Returned value:  %08x",return_value);
				dma_value = 0;
			} else {
				message_write("Returned value:  %06x",return_value);
			}
	}

	return return_value;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::send_special_sdsu_command
 *
 * INVOCATION: send_special_sdsu_command(Sdsu_Special_Dsp special_dsp)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > special_dsp - special DSP command structure
 *
 * FUNCTION VALUE: void
 *
 * PURPOSE:  Send a user specified DSP command
 *
 * DESCRIPTION: Special DSP commands allow the user to send an arbitrary DSP
 * command to the controller. The user has to specify the command header, the 3
 * letter ascii command and up to MAX_SDSU_ARGS integer arguments. This command then
 * attempts to interpret the reply by looking to see if a readable ascii reply
 * is encoded in the 2nd reply word. Otherwise all reply words are echoed to
 * the output channel.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::send_special_sdsu_command(Sdsu_Special_Dsp special_dsp)
{
	int i;
	char msgbuf[MSGLEN];   // Temporary message buffer
	string msg;
	int nargs, ri;
	int local_errno = 0;

	send_interface_reset();

	// special_timeout is in seconds.
	send_timeout(n_sync*special_dsp.timeout);

	ready_reply_mem(SDSU_MAX_REPLY_ARGS);
	ncmd_words = 0;

	msg = "";
	current_command_str(special_dsp.cmd);

	// Place header and cmd in DSP command buffer
	if (!(debug & SDSU_DO_NOTHING)) {
		cmd_buf[ncmd_words++] = special_dsp.header;
		cmd_buf[ncmd_words++] = special_dsp.cmd;
	}

	// Now if required build a command string to display
	if (debug & SDSU_SHOW_COMMANDS) {
		snprintf(msgbuf, MSGLEN,"Command sent (%s):",request_type_str());
		msg = msgbuf;
		snprintf(msgbuf, MSGLEN," %x", special_dsp.header);
		msg += msgbuf;
		snprintf(msgbuf, MSGLEN," %s", current_command_buf);
		msg += msgbuf;
	}

	// Add the arguments required and append to display string
	for (i=0; i<special_dsp.nargs; i++) {
		if (!(debug & SDSU_DO_NOTHING)) {
			cmd_buf[ncmd_words++] = special_dsp.arg[i];
		}
		if (debug&SDSU_SHOW_COMMANDS) {
			snprintf(msgbuf, MSGLEN," %x", special_dsp.arg[i]);
			msg += msgbuf;
		}
	}

	// Now show the constructed string
	if ((debug & SDSU_SHOW_COMMANDS) && (msg.length()>0))
		message_write(msg.c_str());

	// Send the command to the controller
	if (!(debug & SDSU_DO_NOTHING)){
		if (send_interface_command() == SDSU_ERROR) {
			snprintf(msgbuf, MSGLEN,"send_interface_command returned error = %d", errno);
			local_errno = errno;
			throw Error(report_errno(local_errno,msgbuf),E_ERROR,local_errno,__FILE__,__LINE__);
		}
		sdsu_request = default_request;
	}


	ri = 0;
	if (controller.bus == BUS_PCI || controller.bus == BUS_PCIe) {
		nargs = 1;
		sdsu_last_header = 0;

		// First reply word in this lot might be an ASCII reply
		sdsu_last_reply = (reply_buf[0] & REPLY_MASK);

		//      if ((special_dsp.header & SDSU_DEST_MASK) != PCI_ID) {
		// Must get header after the reply because the HCVR command to get it will stomp on
		// last actual reply.
		// Not yet implemented in PCI DSP
		//        if (send_pci_hcvr(SDSU_PCI_READ_HEADER, UNDEFINED, SDSU_END) != SDSU_OK) {
		//  	snprintf(msgbuf, MSGLEN, "Failed to read PCI reply header = (0x%lx)!", reply_buf[0]);
		//  	throw Error(msgbuf, E_ERROR, local_errno, __FILE__,__LINE__);
		//        }
		//        sdsu_last_header = (reply_buf[0] & REPLY_MASK);

		//        if (debug & SDSU_SHOW_COMMANDS)
		//  	message_write("Reply Header received: %x", sdsu_last_header);
		//      }
	} else {
		// First reply word is a header
		sdsu_last_header = (reply_buf[ri++] & REPLY_MASK);

		if (debug & SDSU_SHOW_COMMANDS)
			message_write("Reply Header received: %x", sdsu_last_header);

		// Get number of words
		nargs = sdsu_last_header & 0xff;
		nargs = (nargs > SDSU_MAX_REPLY_ARGS)? SDSU_MAX_REPLY_ARGS:nargs;


		if (debug & SDSU_SHOW_COMMANDS)
			message_write("Reply nargs: %d", nargs);

		switch (controller.bus) {
			default:
				break;
		}

		// First reply word in this lot might be an ASCII reply
		sdsu_last_reply = (reply_buf[ri++] & REPLY_MASK);
	}

	if (debug & SDSU_SHOW_COMMANDS)
		message_write("Reply received: %x", sdsu_last_reply);

	// write the lot out - first handle reply (check to see if it is expected ascii set (ERR,DON,RST)
	msg = "reply: ";
	if ((sdsu_last_reply == SDSU_ERR) || (sdsu_last_reply == DON) || (sdsu_last_reply == RST)) {
		current_command_str(sdsu_last_reply);
		msg += current_command_buf;
	}
	snprintf(msgbuf, MSGLEN," 0x%06x(%d)", sdsu_last_reply, sdsu_last_reply);
	msg += msgbuf;

	// Now print out rest of reply words in both hex and decimal
	for (i=ri; i<nargs; i++) {
		snprintf(msgbuf, MSGLEN," 0x%06lx(%ld)", reply_buf[i]&REPLY_MASK, reply_buf[i]&REPLY_MASK);
		msg += msgbuf;
	}
	message_write("SPECIAL CMD REPLY, header:0x%06x, %s", sdsu_last_header, msg.c_str());

	// Restore timeout
	send_timeout(n_sync*default_timeout);
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::getconfig
 *
 * INVOCATION: getconfig(int read_configuration)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > read_configuration - set if required to read config from disk
 *
 * FUNCTION VALUE: Config_Data*
 *
 * PURPOSE:  Get SDSU controller configuration
 *
 * DESCRIPTION: Eithere reads new configuration from disk or returns
 * in memory config.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Controller_Config* Sdsu3_5_Controller::getconfig(int read_configuration)
{
	int ret_value, nerrs;
	epicsTime stime;
	double dtime;
	std::string fname;
	char msgbuf[MSGLEN];   // Temporary message buffer
	list<string> emsgs; // list for error messages from table read
	std::string sdsu_volts_file;

	config.Config_Data_u.camera.controller.type = controller_status->type;

	if (read_configuration) {
		Sdsu_Setup setup;
		Sdsu_Volts voltages;

		setup.read(sdsu_config_file);

		sdsu_volts_file = sdsu_path(setup.config.volts_filename,controller_dir);
		if ((nerrs = voltages.read(sdsu_volts_file, &emsgs)) > 0) {

			// read has found errors in file
			ofstream ef(VOLTS_ERROR_FILE);

			if (!ef) {
				snprintf(msgbuf, MSGLEN,"Failed to open file for writing: %s",VOLTS_ERROR_FILE);
				throw Error(msgbuf,E_ERROR,errno,__FILE__,__LINE__);
			}


			while (emsgs.size() > 0) {
				// Get the value of the "front" list item.
				message_write(emsgs.front().c_str());
				ef << emsgs.front().c_str() << endl;

				// Remove the item from the front of the list
				emsgs.pop_front();
			}
			ef.close();

			// Now tell user to look in volts error file
			snprintf(msgbuf, MSGLEN, "\n\n**** There were errors found in %s.\n**** See %s for further details.\n\n",
					setup.config.volts_filename,VOLTS_ERROR_FILE);
			throw Error(msgbuf,E_ERROR,SDSU_ERROR,__FILE__,__LINE__);
		}

		// Save in configuration memory
		sdsu_config.config_data = setup.config;

		// Update config command memory with new config
		sdsu_config_data = setup.config;

		// Now read timing and utility programs into config memory
		sdsu_config.timing_prog.n_instructions = 0;
		sdsu_config.utility_prog.n_instructions = 0;
		sdsu_config.interface_prog.n_instructions = 0;

		// Using simple filename - add the full sdsu_path
		fname = sdsu_path(sdsu_config.config_data.timing_filename,controller_dir);

		message_write("Reading timing download file %s", fname.c_str());

		stime = epicsTime::getCurrent();
		if ((ret_value = read_sdsu_download(TIM_ID, fname)) == SDSU_ERROR) {
			snprintf(msgbuf, MSGLEN,"Failed to read timing download file %s", fname.c_str());
			throw Error(msgbuf, E_ERROR, sdsu_errno, __FILE__,__LINE__);
		}
		if (debug>=DEBUG_CALCS) {
			dtime = epicsTime::getCurrent() - stime;
			message_write("Done, #ins= %d, time=%f secs", sdsu_config.timing_prog.n_instructions,dtime/(double)timestamp_res());
		}

		if (sdsu_config.config_data.has_utility_board) {
			// Check if a simple filename is being used - expand to full pathname
			// Using simple filename - add the full sdsu_path
			fname = sdsu_path(sdsu_config.config_data.utility_filename,controller_dir);

			message_write("Reading utility download file %s", fname.c_str());

			stime = epicsTime::getCurrent();
			if ((ret_value = read_sdsu_download(UTIL_ID, fname)) == SDSU_ERROR) {
				snprintf(msgbuf, MSGLEN,"Failed to read utility download file %s", fname.c_str());
				throw Error(msgbuf, E_ERROR, sdsu_errno, __FILE__,__LINE__);
			}
			if (debug>=DEBUG_CALCS) {
				dtime = epicsTime::getCurrent() - stime;
				message_write("Done, #ins= %d, time=%f secs", sdsu_config.utility_prog.n_instructions,dtime/(double)timestamp_res());
			}
		}

		// Now read the INTERFACE board config file into config memory
		if (sdsu_config.config_data.do_interface_download && (controller.bus != BUS_SBUS)) {
			switch (controller.bus) {
				case BUS_PCI:
				case BUS_PCIe:
					// Using simple filename - add the full sdsu_path
					fname = sdsu_path(sdsu_config.config_data.interface_filename,controller_dir);
					break;

				default:
					break;
			}

			message_write("Reading interface DSP download file %s", fname.c_str());

			stime = epicsTime::getCurrent();
			if ((ret_value = read_sdsu_download(INTERFACE_ID, fname)) == SDSU_ERROR) {
				snprintf(msgbuf, MSGLEN,"Failed to read interface download file %s", fname.c_str());
				throw Error(msgbuf, E_ERROR, sdsu_errno, __FILE__,__LINE__);
			}

			if (debug>=DEBUG_CALCS) {
				dtime = epicsTime::getCurrent() - stime;
				message_write("Number of interface DSP instructions %d", sdsu_config.interface_prog.n_instructions);
				message_write("Done, #ins= %d, time=%f secs", sdsu_config.interface_prog.n_instructions,dtime/(double)timestamp_res());
			}
		}

		// Extract voltage information from table and place into an array
		sdsu_config.n_volts = voltages.dump(sdsu_config.volts); // set up volts array
		message_write("Built voltages array - %d volts found!", sdsu_config.n_volts);

	}

	config.Config_Data_u.camera.controller.Controller_Config_u.sdsu = sdsu_config;
	return &config.Config_Data_u.camera.controller;
}

//+ Generic hardware class required methods
/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::init
 *
 * INVOCATION: sdsu->init(Init init_rec)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > init - initialisation record
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Do hardware initialisation
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::init(Init init_rec)
{
	Sdsu_Init* sdsu = &init_rec.controller.Controller_Init_Data_u.sdsu;

	// Call base class init for setting up common data
	Controller::init(init_rec);

	// Store init settings in local structure, either from request or from currently set configuration
	if (init_rec.use_config) {
		sdsu_config = *sdsu_configp;
	}

	// Override on-disk config data settings with settings from command line
	sdsu_config.config_data = sdsu->config_data;

	// Run the setup routine
	do_setup();

	get_controller_info();
}


/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::controller_reset
 *
 * INVOCATION: sdsu->controller_reset
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Do a controller reset
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::controller_reset()
{
	message_write("Running Sdsu Controller Reset");
	do_reset();
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::controller_test
 *
 * INVOCATION: sdsu->controller_test(int ntests)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > ntests - number of tests to run
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Do a controller test
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::controller_test(int ntests)
{
	message_write("Running Sdsu Controller Test");
	do_hardware_test(TRUE, DISPLAY_MIN_MESGS, ntests);
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::controller_shutdown
 *
 * INVOCATION: sdsu->controller_shutdown
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Do a controller shutdown
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::controller_shutdown()
{
	message_write("Turning SDSU controller power off...");
	if (sdsu_command(TIM_ID, SDSU_POF) != SDSU_OK)
		message_write("Power off can not be completed.");
	else
		message_write("     power off complete.");
}
/*
 *+
 * FUNCTION NAME: get_regions_memory_address()
 *
 * INVOCATION:
 *  addr = get_regions_memory_address()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *  Regions start address
 *
 * PURPOSE:
 *  returns addres of DSP regions setup memory
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Sdsu3_5_Controller::get_regions_memory_address()
{
	const int SDSU_MIN_REGIONS_TABLE_SIZE = 0x10;
	int addr_size;
	char msgbuf[MSGLEN];   // Temporary message buffer

	addr_size = sdsu_command(TIM_ID, SDSU_RRT);

	// Top 12 bits are size of regions table available
	// while bottom 12 bits are actual Y mem address
	// For now do not use table size - we expect it to be ample
	// Just throw if way too small
	if (((addr_size&0xFFF000) >> 12) < SDSU_MIN_REGIONS_TABLE_SIZE) {
		sprintf(msgbuf,"Sdsu3_Controller: Regions memory area too small, is %d bytes, should be at least %d!",
				(addr_size&0xFFF000) >> 12, SDSU_MIN_REGIONS_TABLE_SIZE);
		throw Error(msgbuf, E_ERROR, ENOMEM,__FILE__, __LINE__);
	}

	return addr_size&0xFFF;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::reset_detector
 *
 * INVOCATION: sdsu->reset_detector(Detector_Reset reset, bool exp)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > reset - reset parameters
 * > exp - reset in an exposure sequence
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Reset detector hardware
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::reset_detector(Detector_Reset reset, bool exp)
{
	int i;
	int reset_time = nint(reset.reset_delay * THOUSAND);  // reset_delay in seconds - convert to msec for SDSU
	int delay;
	int count = reset.nresets;

	if (camera.type == IR_CAMERA) {
		// Set reset delay, one parameter (delay in msec)
		sdsu_command(TIM_ID, SDSU_SRD, reset_time);

		// Set reset count, one parameter (nresets)
		sdsu_command(TIM_ID, SDSU_SNR, reset.nresets);

		// For IR cameras the reset count is handled by the SNR parameter
		// so just run CLR once here unless we are about to run an exposure
		// which is handles the CLR internally
		count = (exp)? 0:1;
	}

	// Perform the reset a number of times
	for (i=0; i<count; i++) {
		if (controller_abort) {
			if (debug&DEBUG_STATE) {
				message_write("Aborting out of flush loop, %s, %d",__FILE__,__LINE__);
			}
			break;
		}
		if (debug>=DEBUG_CALCS) {
			message_write("Resetting Chip, %d of %d",i+1,reset.nresets);
		}

		// Now send the CLR command to reset
		sdsu_command(TIM_ID, SDSU_CLR);
	}


	if (camera.type != IR_CAMERA) {
		// Now wait for required delay before proceeding
		while (reset_time > 0) {
			delay = (reset_time > HALF_SECOND)? HALF_SECOND:reset_time;
			rest(delay);
			reset_time = reset_time - delay;
		}
	}

}


/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::setup_readout
 *
 * INVOCATION: setup_readout(int ns, Detector_Readout* readout_rec, Detector_Regions* trgn,
 *                           int& isize, double exptime)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ns - number of controllers to sync
 * > readout_rec - readout request structure
 * > trgn - readout regions object
 * ! isize - total bytes in readout - may be modified by controller specific req
 * > exptime - last exposure time
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: setup the readout of a CCD
 *
 * DESCRIPTION: Load the DSP program with info about this readout
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::setup_readout(int ns, Detector_Readout* readout_rec, Detector_Regions* trgn,
				       IR_Exposure_Info& ir_einfo, unsigned long& isize, double exptime)
{
	if (sdsu_simulation != 0)
		return;

	// int serial_regions, bin_height, bin_width;// ROI settings
	// unsigned int max_height;              // maximum row of all regions
	// unsigned int total_rows;              // total rows to be read out
	unsigned long is;
	bool changed_lod = false;
	ModeConfig mc;

	if ((current_mode.first != ir_einfo.readModeSet) || (current_mode.second != ir_einfo.quadrantsSet)) {
		ModeIdx new_mode{ir_einfo.readModeSet, ir_einfo.quadrantsSet};
		std::string fname;

		if (current_mode.first == -1 || current_mode.second == -1) {
			logger::message(logger::Level::Min, "setup_readout: the current SDSU mode is not valid or unknown");
		} else {
			logger::message(logger::Level::Min, "setup_readout: the current SDSU mode is %s", setups[current_mode].label.c_str());
		}

		try{
			mc = setups.at(new_mode);
		}
		catch (std::out_of_range e) {
			throw std::runtime_error("Trying to set an illegal SDSU mode");
		}

		fname = sdsu_path(mc.lod_file.c_str(), controller_dir);
		sdsu_download(TIM_ID, fname);

		logger::message(logger::Level::Min, "setup_readout: new SDSU mode: %s.", setups[new_mode].label.c_str());

		changed_lod = true;
		current_mode = new_mode;
	}
	else {
		mc = setups.at(current_mode);
	}

	ir_camera->update_sensor_parameter(NDAVGS, mc.ndavgs);
	ir_camera->update_sensor_parameter(LRNS, mc.lrns);
	ir_camera->add_raw_parameter("PDU", RAW_ROWS, to_string(mc.nrows));
	ir_camera->add_raw_parameter("PDU", RAW_COLS, to_string(mc.ncols));
	ir_camera->add_config_unit(TIME_SAMPLING);


	nrows = mc.nrows;
	ncols = mc.ncols;
	is = nrows * ncols * 2;

	// Call generic setup stuff
	Controller::setup_readout(ns, readout_rec, trgn, ir_einfo, is, exptime);

	// Need to send and RDC with this call to readout
	auto_rdc = false;
	send_rdc = true;

	// != 0 only makes sense for CCD
	skip_bytes = 0;

	send_interface_reset();

	long buf_size = nrows * ncols * 2;
	if ((pDevice != nullptr) && (pDevice->IsOpen()) && changed_lod) {
		pDevice->UnMapCommonBuffer();
		pDevice->SetImageSize(nrows, ncols);
		pDevice->MapCommonBuffer(buf_size);
		if (pDevice->CommonBufferSize() != buf_size)
			throw std::runtime_error("The common buffer hasn't mapped properly");
	}

	pDevice->SetOpenShutter(true);
	set_exposure_time(int(exptime * 1000));

	sdsu_statusp->put(CONTROLLER_SWV, get_dsp_version(util_id), 0, NAMELEN);
}

/*
 *+
 * FUNCTION NAME: do_reset
 *
 * INVOCATION:
 *  do_reset();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Resets timing board
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::do_reset()
{

	// int board_header, nargs, ios;

	// Board header is src|dest|nargs is 3 low order bytes (src is host computer = 0)
	// we need 3 arguments for this command, ie header, cmd, data
	// nargs = 3;
	// board_header = (TIM_ID << 8) | 3;

	message_write("Resetting Timing Board...");

	if (sdsu_simulation == 0) {
		// First reset the interface board
		send_interface_reset();

		// Increase timeout for reset to be that of a clear
		// TODO: This doesn't work at the time
		// send_timeout(n_sync*sdsu_config.config_data.clr_timeout);
		if (send_controller_reset() != SDSU_OK) {
			message_write("Timing reset can not be completed");
			throw Error("Error trying to reset the controller");
		}

		// Restore timeout
		// TODO: Not doing this at the time
		// send_timeout(n_sync*default_timeout);
	}
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::get_dsp_version
 *
 * INVOCATION: ver = get_dsp_version(int board_id)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > board_id - DSP board to send command to
 *
 * FUNCTION VALUE: char*
 *
 * PURPOSE:
 *  Returns a version string of form Va.b
 *
 * DESCRIPTION:
 *
 * Retrieves the DSP version number located in a memory location in DSP memory.
 * This is a 24 bit number with the LS 12 bits the low order version number (b)
 * and the top 12 bits the high order
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
char* Sdsu3_5_Controller::get_dsp_version(int board_id)
{
	/*
	   int swv,swv1,swv2,swv3;                 // DSP software version numbers

	   printf("***** Getting DSP version\n");
	   if (debug&DEBUG_INDICATORS)
	   message_write("Getting DSP version number from board %d...", board_id);

	// TODO: Review the following comment. It doesn't match reality
	// Get software version number from controller address
	// This is a 24 bit number with the lower 12 bits is the controller
	// revision number and the upper 12 bits is the basic timing file revision number
	strcpy(sdsu_status->controller_swv,"");
	swv = sdsu_command(board_id, SDSU_RDM, 0, SDSU_MEM_Y, SDSU_SW_VERSION_PAR);
	printf("sdsu_command(...) = %d ==? %d\n", swv, SDSU_ERROR);
	if (swv != SDSU_ERR) {
	printf("Decoding DSP version\n");
	swv1 = swv >> 16;
	swv2 = (swv&0xFF00) >> 8;
	swv3 = swv&0xFF;
	snprintf(sdsu_status->controller_swv, NAMELEN, "V%d.%d.%d", swv1, swv2, swv3);
	if (debug&DEBUG_INDICATORS)
	message_write("DSP version (0x%x) V%d.%d.%d", swv, swv1, swv2, swv3);
	} else {
	message_write("DSP version unknown - check??");
	}

	return sdsu_status->controller_swv;
	*/


	// TODO: The code above gets nothing for us. We should check the underlying
	//       DSP code.
	strcpy(sdsu_status->controller_swv, "UNKNOWN");
	return sdsu_status->controller_swv;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::create_image_buffer
 *
 * INVOCATION: imbuf = ctrlr->create_image_buffer(unsigned long size, int align)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > size - size in bytes of the buffer
 * > align - alignment constraint - if zero ignored
 *
 * FUNCTION VALUE: char*
 *
 * PURPOSE:
 *  Create an image readout buffer and returns pointer to buffer
 *
 * DESCRIPTION: Uses virtual memory to create an image buffer which
 * can be locked from being paged - if process is running as setuid root.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// PENDING REVIEW OF THE CAMERA CODE
char* Sdsu3_5_Controller::create_image_buffer(unsigned long size, int align)
{

	char *buf_ptr;

	// Make sure buffer size is a multiple of astro device driver dma limit
	switch (controller.bus) {
		case BUS_PCI:
		case BUS_PCIe:
			buffer_size = size;

			// Call generic create routine
			break;
		default:
			break;
	}

	// Now call the generic create routine
	buf_ptr = Controller::create_image_buffer(buffer_size, SDSU_ALIGN);

	// Setup the image readout parameters so that they can be sednt to the interface
	nreads = 1;
	nsamples = 1;
	image_size = actual_image_size;

	// Now adjust starting address for SDSU junk bytes
	buf_ptr += skip_bytes;
	image_size -= skip_bytes;

	// read 4 bytes for extraneous utility->vme RDC reply
	//    buf_ptr += (send_rdc || (controller.bus != BUS_SBUS))? 0:SDSU_EXTRA_BYTES;
	//    image_size -= (send_rdc || (controller.bus != BUS_SBUS))? 0:SDSU_EXTRA_BYTES;

	return buf_ptr;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::start_ir_image_transfer_thread()
 *
 * INVOCATION: sdsu->start_ir_image_transfer_thread(IR_Expose& expose, Thread *ir_tt)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > expose - exposure request
 * > ir_tt - pointer to thread monitoring IR exposure timing
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Starts the image transfer thread
 *
 * DESCRIPTION: Take copy of exposure info and call generic routine to start thread
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::start_ir_image_transfer_thread(IR_Expose& expose, Thread *ir_tt)
{

	ModeConfig mc(setups.at(current_mode));

	// record current exposure info
	ir_einfo = expose.einfo;

	// Set ir_timer thread member var for use by image transfer thread
	ir_timer_thr = ir_tt;

	// Tell the interface what to expect
	nreads = mc.ndavgs;
	nsamples = mc.lrns;
	nresets = 0;
	read_running = 0;

	// Now set all required controller IR parameters

	if (debug&DEBUG_INDICATORS) {
		message_write("SDSU: Finished setting up IR parameters, about to start read thread");
	}

	// Start the image transfer
	start_image_transfer_thread();

}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::start_image_transfer_thread()
 *
 * INVOCATION: sdsu->start_image_transfer_thread()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Starts image transfer thread
 *
 * DESCRIPTION: Start thread if any readout region specified
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::start_image_transfer_thread()
{
	image_readout_thread_abort = false;

	// Start the image transfer thread

	if (actual_image_size>0) {
		stop_readout = false;
		numbytes = actual_image_size - skip_bytes;

		// Let the interface know what to expect
		// ready_interface();

		// OK - start the thread
		image_transfer_thr = new SdsuImageTransferThread(this, "sdsu_image_transfer", false);
		image_transfer_thr->start();


		// send RDC (start readout) command to Timing if 0 length exposure
		// otherwise RDC sent from Utility Board
		// TODO: Remove completely?
		// TODO: Figure out if we need to unlock manually
		/* Irrelevant with the new firmware
		   if (!auto_rdc) {
		   if (send_rdc) {
		// TODO: Timeout not being used...
		// send_timeout(n_sync*2*default_timeout);

		sdsu_command(TIM_ID, SDSU_RDC);
		}

		// Unlock the image mutex so that the read thread can get going
		image_transfer_thr->unlock();
		}
		*/
	}

	if (debug&DEBUG_INDICATORS) {
		message_write("SDSU: Read thread started");
	}
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::start_row_region
 *
 * INVOCATION: sdsu->start_row_region(int rr)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > rr - row number of region
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Actions necessary before reading a row region
 *
 * DESCRIPTION: Nothing required for Sdsu
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::start_row_region(int rr)
{
	// Nothing to do. In the old code this would add some delay
	// for CCDs, but we deal with IR detectors
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::wait_segment_readout
 *
 * INVOCATION: sdsu->wait_segment_readout(int rows_processed, int row, int rowbytes, int& rt)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > rows_processed - total number of rows expected so far
 * > row - starting row number of chunk being clocked out
 * > rowbytes - number of bytes per row
 * ! rt - current count of total rows transferred in readout
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Wait on thread to complete a segment readout
 *
 * DESCRIPTION: Looks at status of image transfer by monitoring
 * . number of rows processed
 * . if image trasnfer is still going
 * . not aborted
 *
 * EXTERNAL VARIABLES:
 * Member variables of class Controller
 *  bool doing_image_transfer;      // flag indicating image tranfer started
 *  bool finished_image_transfer;   // flag indicating image tranfer finished
 *  pthread_mutex_t image_mutex;    // Mutex lock for image
 *  int rows_transferred;           // Number of rows transferred in readout
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::wait_segment_readout(int rows_processed, int row, int rowbytes, int& rt)
{
	/*
	char *buffer_adr;
	int buffer,npix,i,frame_rows;
	unsigned short *pix;

	if (!rowbytes) {
		throw Error("Sdsu_Controller: Number of bytes in a row (rowbytes) must be greater than zero!",
				E_ERROR,SDSU_ERROR,__FILE__, __LINE__);
	}
	if (!numbytes) {
		throw Error("Sdsu_Controller: Number of bytes in a readout (numbytes) must be greater than zero!",
				E_ERROR,SDSU_ERROR,__FILE__, __LINE__);
	}

	// Wait for mutex before setting vars
	image_transfer_thr->lock();
	get_readout_status(rowbytes);

	rt = rows_transfered;

	// Try unblocking waiting readout thread
	image_transfer_thr->signal();

	// unlock mutex so that read thread can access vars
	image_transfer_thr->unlock();

	// Wait until rows_processed rows have been done in this image readout
	while ((rt<rows_processed) && !controller_abort) {
		rest(IMAGE_WAIT);

		// Wait for mutex before checking vars
		image_transfer_thr->lock();
		get_readout_status(rowbytes);
		rt = rows_transfered;

		// Try unblocking waiting readout thread
		image_transfer_thr->signal();

		// unlock mutex so that read thread can access vars
		image_transfer_thr->unlock();


	if (image_readout_thread_abort) {
		// Write a message about how many bytes still to transfer as well as thread_msg
		message_write("Readout failure - %llu bytes expected, %llu still required!", total_readout_bytes, bytes_remaining);
	} else if (!controller_abort) {
		// bool pci_controller = controller.bus == BUS_PCI || controller.bus == BUS_PCIe;

		 if (pci_controller && (!byte_swapping)) {
		// Do we need to swap pixels in software - ie hardware byte swapping not on
		// work out address of starting pixel
		buffer = (rows_processed*rowbytes/image_size)%nsamples;
		buffer_adr = image_buffer_ptr + buffer%nbuffers*image_size;
		pix = (unsigned short*) (buffer_adr + row * rowbytes);
		frame_rows = rows_processed%(image_size/rowbytes);
		if (frame_rows == 0)
		frame_rows = image_size/rowbytes;
		npix = ((frame_rows-row)*rowbytes)/BYTE_PIX(image_info.bitpix);

		if (debug & DEBUG_CALCS)
		message_write("Swap bytes, rp=%d, r=%d, npix=%d, pix=%p frame_rows=%d!", rows_processed, row, npix, pix, frame_rows);

		// Now swap bytes for chunk of pixels just looked at
		for (i=0; i<npix; i++)
		pix[i] = sbyte_swap(pix[i]);
		}
	}
	}
	*/
}

/*
 *+
 * FUNCTION NAME: get_readouts_done
 *
 * INVOCATION: rd = get_readouts_done()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: int
 *
 * PURPOSE:
 *  Returns the number of readouts completed
 *
 * DESCRIPTION: Calls the driver ASTRO_FRAMES_READ to get readouts done
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// PENDING 3.5
long Sdsu3_5_Controller::get_readouts_done()
{
	return readouts_done;
}
/*
 *+
 * FUNCTION NAME: get_read_running
 *
 * INVOCATION: rd = get_read_running()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: int
 *
 * PURPOSE:
 *  Returns the number of readouts completed
 *
 * DESCRIPTION: Calls the driver ASTRO_READ_RUNNING to get readouts done
 *
 * EXTERNAL VARIABLES:
 v *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
long Sdsu3_5_Controller::get_read_running()
{
	return read_running;
}

/*
 *+
 * FUNCTION NAME: get_readout_status
 *
 * INVOCATION: get_readout_status(int rowbytes)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > rowbytes - number of bytes per row
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Returns the number of readouts completed
 *
 * DESCRIPTION: Calls the driver GET_READ_STATUS to get readout status items
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::get_readout_status(int rowbytes)
{
	double rt;

	if (!controller_abort) {
		/*
		 * TODO: The driver is not separate any longer, so this is not
		 *       relevant. But ensure that the info is updated elsewhere

		 readout_status = *(Astro_Readout_Status*) reply_buf;
		 read_running = readout_status.read_running;
		 readouts_done = readout_status.readouts_done;
		 bytes_remaining = readout_status.bytes_remaining;
		 */


		// Driver returns readout time in usec - convert to seconds
		// Ensure this is a positive value first though.
		if (readout_status.last_readout_time > 0) {
			rt = readout_status.last_readout_time/DMILLION;
			camera_statusp->put(READOUT_TIME, &rt, buffer_number, 1);
		}

		if (rowbytes > 0) {
			rows_transfered = (int)(((uint64_t)nreads*total_readout_bytes - bytes_remaining)/rowbytes);
			transfer_buffer = rows_transfered*rowbytes/numbytes;
			buffer_number = transfer_buffer%camera.n_buffers;
		}

	}
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::do_stop_readout
 *
 * INVOCATION: do_stop_readout()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE:
 *  stop the readouts
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::do_stop_readout()
{
	message_write("Sending command to SDSU controller to stop exposure...");
	stop_readout = true;
	sdsu_command(SDSU_STOP);
}

/*
 *+
 * FUNCTION NAME: Controller::finalise_readout
 *
 * INVOCATION: finalise_readout()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: finalise readout parameters
 *
 * DESCRIPTION: SDSU finalise stuff - calls BRR to get bytes left to be read
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::finalise_readout()
{
	Controller::finalise_readout();

	update_total_readout_bytes();
	bytes_remaining = total_readout_bytes - (astro_state.last_pixel_counter * 2);
}

/*
 *+
 * FUNCTION NAME: Controller::flush_readout
 *
 * INVOCATION: flush_readout()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: flush any stray data coming from controller
 *
 * DESCRIPTION: Send an SDSU_FLUSH to the driver telling it to expect to timeout
 * while reading any stray data.
 * And then waits until a read times out indicating all data flow is finished.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::flush_readout()
{
	// SDSU_FLUSH is not part of the current firmware caps
	// And no one uses it...
}

/*
 *+
 * FUNCTION NAME: finish_readout
 *
 * INVOCATION:
 *  finish_readout();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Cleans up after a readout either finishes or is aborted
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu3_5_Controller::finish_readout()
{


}

/*
 *+
 * FUNCTION NAME:  Sdsu3_5_Controller::prepare_exposure()
 *
 * INVOCATION: prepare_exposure(int ns)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Prepare for an SDSU Exposure
 *
 * DESCRIPTION: Sets DSP flags for exposure and checks for
 * any sync requirements
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES: None
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::prepare_exposure(int ns)
{

	// Read back voltage settings
	read_current_voltage_settings();

	// Set multiple controllers to sync
	n_sync = (ns>0)? ns:1;


	// Optionally call readout without sending an RDC because exposure will have
	// already done so except for bias frames
	auto_rdc = sdsu_config.config_data.enable_auto_rdc;
	send_rdc = !auto_rdc;
}

/*
 *+
 * FUNCTION NAME:  Sdsu3_5_Controller::set_exposure_time
 *
 * INVOCATION: Sdsu3_5_Controller::set_exposure_time(int msec)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > msec - exposure time in msec to set
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Sets the exposure time
 *
 * DESCRIPTION: Uses the WRM command to write directly into controllers exposure
 * time memory location
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES: None
 *
 *- */
// UPDATED TO 3.5
void Sdsu3_5_Controller::set_exposure_time(int msec)
{
	sdsu_command(TIM_ID, SDSU_SET, msec);
}

/*
 *+
 * FUNCTION NAME:  Sdsu3_5_Controller::read_current_voltage_settings
 *
 * INVOCATION: msec = Sdsu3_5_Controller::read_current_voltage_settings()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Read back voltage settings
 *
 * DESCRIPTION: Uses the RDM command to fetch the controllers voltage settings.
 * These voltages are described by the sdsu_volts configuration and include the
 * DSP address of the current setting. Each setting is converted from ADUs to
 * floating point voltage value using V = Vmax * ADU/ADU_Max.
 * The status value is then updated with converted voltage.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */

// PENDING
void Sdsu3_5_Controller::read_current_voltage_settings()
{
	// TODO: Write something sensible here!!
	//       I don't see any voltage stuff in GNIRS, though?
	/*
	   int v;
	   int adu;

	   for (v=0; v<sdsu_config.n_volts; v++ ) {
	// If in full simulation write - expected value first
	if (camera_status->controller_simulation == 2)
	sdsu_command(TIM_ID, SDSU_WRM,  calc_adu_val(sdsu_config.volts[v], V_LO, sdsu_config.volts[v].value[0]), SDSU_MEM_Y,
	sdsu_config.volts[v].dsp_addr);
	if ((adu = sdsu_command(TIM_ID, SDSU_RDM, 0, SDSU_MEM_Y, sdsu_config.volts[v].dsp_addr)) != SDSU_ERR) {
	sdsu_status->voltage_value[v] = calc_volt_val(sdsu_config.volts[v], V_LO, adu);
	if (debug & DEBUG_INDICATORS)
	message_write("Read voltage '%s', adu=%d, value=%f", sdsu_config.volts[v].name, adu, sdsu_status->voltage_value[v]);
	sdsu_statusp->put(sdsu_config.volts[v].name, sdsu_status->voltage_value[v]);
	} else
	message_write("Unable to get voltage '%s'", sdsu_config.volts[v].name);
	}
	*/

}
/*
 *+
 * FUNCTION NAME:  Sdsu3_5_Controller::get_exposure_time
 *
 * INVOCATION: msec = Sdsu3_5_Controller::get_exposure_time()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: exposure time in msec
 *
 * PURPOSE: Gets the exposure time counter value
 *
 * DESCRIPTION: Uses the RDM command to read directly from the controllers exposure
 * time counter memory location
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES: None
 *
 *- */
// UPDATED TO 3.5
int Sdsu3_5_Controller::get_exposure_time()
{
	int dspmsecs;
	if ((dspmsecs = sdsu_command(shutter_control_board, SDSU_RET)) == SDSU_ERR)
		throw Error("Error getting exposure time from SDSU3", E_ERROR, EFAULT,__FILE__, __LINE__);
	else
		return dspmsecs;
}

#include <sys/time.h>

//+ Sends the "start exposure" command
// NEW IN 3.5
epicsTime Sdsu3_5_Controller::start_exposure()
{
        /* Pre-fill the buffer */
	ModeConfig mc(setups.at(get_current_mode_index()));
	uint64_t bytes_per_sample = mc.rows_per_sample() * mc.cols_per_sample() * BYTES_PER_PIXEL;
	uint64_t total_readout_bytes = mc.lrns * bytes_per_sample * 2;
	memset(pDevice->CommonBufferVA(), 0x27, total_readout_bytes);
	/* End pre-fill the buffer */
	astro_state.last_pixel_counter = 0;

	// Start exposing now
	if (pDevice->Command( TIM_ID, SEX ) != DON) {
		throw std::runtime_error("Error when starting the exposure!\n");
	}

	auto exposure_start = exposure_clock.set_period_start();
	auto exp_started_string = exposure_clock.time_point_to_string(exposure_start);

	logger::message(logger::Level::Full, "Exposure started at: %s.", exp_started_string.c_str());

	return exposure_clock.to_epicsTime(exposure_start);
}
//
//+ Sends the "stop exposure" command
void Sdsu3_5_Controller::stop_exposure()
{
	exposure_clock.set_period_end();
	pDevice->StopExposure();
}

// NEW IN 3.5
double Sdsu3_5_Controller::exposure_lapsed()
{
	return double(epicsTime::getCurrent() - exposure_clock.to_epicsTime(exposure_clock.get_period_start()));
}

size_t Sdsu3_5_Controller::dump_to(FILE *dest)
{
	if (fwrite(pDevice->CommonBufferVA(), total_readout_bytes, 1, dest) != 1) {
		throw std::runtime_error(strerror(errno));
	}

	return total_readout_bytes;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::handle_reset
 *
 * INVOCATION: handle_reset()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:  none
 *
 * PURPOSE:
 * Interrupt handler for the RESET signal - called from camera
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
// UPDATED TO 3.5
void Sdsu3_5_Controller::handle_reset()
{

	Controller::handle_reset();

	if (doing_image_transfer) {
		// Abort the readout and cancel the read thread
		message_write("Sending command to SDSU controller to abort readout...");

		try { // catch any errors that might occur while sending this


			// Tell the driver we are aborting
			if (sdsu_command(SDSU_ABORT) != SDSU_ERROR) {

				// Now wait for the last transfer to finish
				message_write("Waiting on SDSU controller to abort readout...");
				while (doing_image_transfer) {
					rest(HALF_SECOND);
				}
				message_write("SDSU controller completed abort.");

			} else
				message_write("SDSU failed to complete an abort?");

		}
		catch (Error & e) {
			msg_severity = e.type;
			message_write(e.record_error(__FILE__,__LINE__));
			e.rethrow();
		}
	}
}

void Sdsu3_5_Controller::update_total_readout_bytes()
{
	total_readout_bytes = nrows * ncols * 2;
}

void Sdsu3_5_Controller::update_readout_running()
{
	throw(std::runtime_error("update_total_readout_bytes: not implemented"));
}

void Sdsu3_5_Controller::update_pixel_count()
{
	astro_state.last_pixel_counter = pDevice->GetPixelCount();
}

ModeIdx Sdsu3_5_Controller::get_current_mode_index() const
{
	return current_mode;
}

/*
 *+
 * FUNCTION NAME: Sdsu3_5_Controller::~Sdsu_Controller
 *
 * INVOCATION: delete sdsu
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Destructor
 *
 * DESCRIPTION: Clear dsp hash tables
 *
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu3_5_Controller::~Sdsu3_5_Controller()
{

	// Close the interface
	close_interface();

	// delete the sdsu status object
	if (sdsu_statusp)
		delete sdsu_statusp;


}

SdsuImageTransferThread::SdsuImageTransferThread(Sdsu3_5_Controller* sc, const string &name,
		bool dolock, unsigned int priority)
: Thread(name.c_str(), priority, epicsThreadGetStackSize(epicsThreadStackSmall), dolock),
	sdsu(sc)
{
}

static const std::string reset_prefix = "RESET_";
static const std::string signal_prefix = "SIGNAL_";

void
SdsuImageTransferThread::run()
{
	uint64_t byte_count;                   // Count of bytes read in full readout set
	bool mlock = false;

	try {
		sdsu->image_readout_thread_abort = false;
		sdsu->image_readout_errno = 0;


		// Wait for mutex before proceeding to initialise clockout
		lock();
		mlock = true;

		if (sdsu->camera.type == IR_CAMERA) {
			// Now wait until the caller is ready to do this read
			while ((sdsu->readout_no != 0) && !sdsu->controller_abort && !sdsu->stop_readout) {
				// NB this is not a timed wait - we must ensure that header block is handled
				sdsu->image_transfer_thr->wait();
			}
		}

		// Initialise controller readout parameters
		sdsu->init_readout(0, 0);

		// Unblock waiting calling thread
		sdsu->image_transfer_thr->signal();

		// unlock mutex so that main thread can access vars
		sdsu->image_transfer_thr->unlock();
		mlock = false;

		// Work out length of buffer taking into account number of samples and max
		// buffer size, assume input iov_len is the size of one frame. Make sure we
		// do not go beyond buffer_size - this will cause the driver to CRASH the
		// system.  Actual readout size will be adjusted in strategy routine.

		ModeConfig mc(setups.at(sdsu->get_current_mode_index()));

		uint64_t bytes_per_sample = mc.rows_per_sample() * mc.cols_per_sample() * BYTES_PER_PIXEL;
		uint64_t half_readout;

		// No need to multiply by NDAVGS: it's included in the bytes_per_sample
		sdsu->total_readout_bytes = mc.lrns * bytes_per_sample * 2;
		half_readout = sdsu->total_readout_bytes / 2;

		// x2 because nsamples counts only the number of FS before OR after the exposure
		sdsu->message_write("SDSU_read_thread: Readout for (%ld x 2) x %ld bytes per FS (buffer size used=%ld)",
				mc.lrns, bytes_per_sample, sdsu->total_readout_bytes);

		// Initialize bytes_remaining
		sdsu->bytes_remaining = sdsu->total_readout_bytes;

		if (sdsu->debug & SDSU_DO_NOTHING) {
			byte_count = sdsu->total_readout_bytes;
		} else {
			int phase = 0;
			uint64_t next_timing_capture = bytes_per_sample;
			unsigned timing_idx = 1;
			epicsTime start = sdsu->get_start_exposure();
			std::string current_prefix = reset_prefix;

			byte_count = 0;

			// TODO: Implement abort/stop
			while (byte_count < sdsu->total_readout_bytes) {
				if (byte_count >= next_timing_capture) {
					ir_camera->add_raw_parameter(TIME_SAMPLING, current_prefix + left_justify(to_string(timing_idx++), 2, '0'),
							to_string(epicsTime::getCurrent() - start));
					next_timing_capture += bytes_per_sample;
				}
				if ((phase == 0) && (byte_count >= half_readout)) {
					// Reset sampling done. We're now exposing.
					phase = 1;
					timing_idx = 0;
				}
				if ((phase == 1) && (byte_count > half_readout)) {
					// Exposing done. We're not signal sampling.
					ir_camera->add_raw_parameter(TIME_SAMPLING, signal_prefix + left_justify(to_string(timing_idx++), 2, '0'),
							to_string(epicsTime::getCurrent() - start));
					phase = 2;
					current_prefix = signal_prefix;
				}
				sdsu->update_pixel_count();
				byte_count = sdsu->astro_state.last_pixel_counter * 2;
				rest_micro(REST_PERIOD);
			}
		}

		sdsu->stop_exposure();

		// Wait for mutex before proceeding to set flag
		sdsu->image_transfer_thr->lock();
		mlock = true;

		sdsu->read_running = 1;
		sdsu->readouts_done = 1;

		// Unset flag so that slave can proceed
		sdsu->finalise_readout();
		sdsu->image_transfer_thr->signal();

		// unlock mutex now that read done
		sdsu->image_transfer_thr->unlock();
		mlock = false;

		if (sdsu->debug & SDSU_DO_NOTHING) {
			sdsu->message_write("SDSU_read_thread: do nothing... ");
		} else {
			sdsu->message_write("SDSU_read_thread: Total bytes read for %ld reads = %llu", sdsu->nreads, byte_count);
		}
	}
	catch (Error& ce) {
		sdsu->message_write("Exception caught in image readout thread: %s", ce.record_error(__FILE__,__LINE__));

		if (!mlock)
			sdsu->image_transfer_thr->lock();

		// Unset flag so that slave can proceed
		sdsu->doing_image_transfer = false;
		sdsu->finished_image_transfer = true;
		sdsu->handle_reset();
		sdsu->image_readout_thread_abort = true;

		// unlock mutex now that read done and signal cv
		sdsu->image_transfer_thr->signal();
		sdsu->image_transfer_thr->unlock();
		strlcpy(sdsu->image_readout_thread_msg,ce.record_error(__FILE__,__LINE__),MSGLEN);
		sdsu->message_write(sdsu->image_readout_thread_msg);
	}
}

extern "C" {
epicsExportAddress(int, sdsu_simulation);
}
