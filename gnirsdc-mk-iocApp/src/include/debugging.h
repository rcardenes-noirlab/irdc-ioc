#ifndef __DEBUGGING_H__
#define __DEBUGGING_H__

#include <string>

void stamped_puts(const char *msg);
void stamped_puts(const std::string& msg);

#endif // __DEBUGGING_H__
