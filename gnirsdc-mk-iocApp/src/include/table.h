/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * CICADA PROJECT
 *
 * FILENAME table.h
 * 
 * PURPOSE
 * General config table reader/writer. Makes use of STL map and set collection
 * classes Each table is a collection consisting of an item name plus a map
 * of key value pairs
 *
 * REQUIREMENTS
 *
 * Requires use of STL classes "map", "set" and "string"
 * 
 */

#ifndef TABLE_H
#define TABLE_H

#include <utility.h>
#include <map>
#include <string>
#include <set>
#include <typeinfo>

using namespace std;

//+ Table class constants
const int LINELEN = 1024;
const int FORMAT_STR_LEN = 25;

//+ typedefs 
typedef char Linetype[LINELEN];

//+ Forward declaration for the Table_Item class
struct Table_Item_Less;
class Table_Item;

//+ A Table_Item_Set is a set of items containing parameter value pairs
typedef set<Table_Item*, Table_Item_Less> Table_Item_Set;

//+ A Table_Parameter_Map is a parameter value collection of strings
typedef map<string, string, less<string> > Table_Parameter_Map;

/*
CLASS
    Table_Item
    Items in a Table, each have a name and list of par/val pairs
DESCRIPTION  
    This class provides methods for operating on a single item out of a
    table. Each table can have an arbitrary number of items. An item is a
    collection of parameter value pairs that are stored as strings in an STL
    map.

KEYWORDS
    Table_Item 
*/
class Table_Item {
private:
  // GROUP:   Private member variables
  string separator;                     //+ Char string that separates a par from val
  string end_of_par;                    //+ Char string indicates end of par

  // GROUP:   Private methods
  //+ Parse a list of values from a string and place in array
  // > val_list - string containing comma separated list of values
  // < val - array of values extracted
  // > n - number of values to extract
  template <typename T> void get_list(const string val_list, T* val, int n);

  //+ Construct a list of values from an array and place in a string 
  // < val - array of values to use
  // > n - number of values to form into list
  template <typename T> string put_list(const T* val, int n);

 public:
  //+ Constructor - unnamed item
  // > sep - string that separates parameters and values (whitespace ignored)
  // > eop - string that signals end of parameter/value pair (whitespace ignored)
  // > ns - Whether or not items are sorted by name
  // > sk - Sort key if NOT sorted by name (the STL set class needs a sort key)
  Table_Item(string sep="=", string eop=";", bool ns=true, int sk=0);


  //+ Constructor - named item
  // > item_name - name of this item
  // > sep - string that separates parameters and values (whitespace ignored)
  // > eop - string that signals end of parameter/value pair (whitespace ignored)
  // > ns - Whether or not items are sorted by name
  // > sk - Sort key if NOT sorted by name (the STL set class needs a sort key)
  Table_Item(const char* item_name, string sep="=", string eop=";", bool ns=true, int sk=0);

  //+ destructor
  ~Table_Item();

  // GROUP:   Public member variables
  string name;                          //+ name of this item
  bool namesort;                        //+ Whether or not items are sorted by name
  int sortkey;                          //+ Sort key if NOT sorted by name (the STL set class needs a sort key)
  Table_Parameter_Map pars;             //+ map of parameter value pairs

  // GROUP:   Public member functions
  //+ Parses a line of text searching for param/value pairs to add to hash table
  // > line - line of text to parse
  void read(string line);

  //+ currently unimplemented
  void write();

  //+ Gets value associated with parameter name 
  // > par - parameter to lookup
  // < val - value retrieved
  template <typename T> bool find(const string par, T& val);
  bool find(const string par, string& val);

  //+ Gets array of values associated with parameter name
  // > par - parameter to lookup
  // < val - value retrieved
  // > n - max number of values that will fit in array
  //+ Special case for char* strings, ie not a list
  template <typename T> bool find(const string par, T* val, int n);

  //+ Puts a value associated with parameter name in par val map
  // > par - parameter to reference
  // > val - value to put
  template <typename T> void put(const string par, const T val);

  //+ Puts a list of values associated with parameter name in par val map
  // > par - parameter to reference
  // > val - array of values to put
  // > n - number of values in list
  //+ Special case for char* strings, ie not a list
  template <typename T> void put(const string par, const T* val, int n);

  //+ Lists to stdout the contents of the table_items par map
  void print();
  
  //+ Define an equality test operator - required for linked list
  int operator==(const Table_Item& item) const {
    return (name.compare(item.name) == 0);}
  int operator==(const Table_Item* item) const {
    return (name.compare(item->name) == 0);}
 
  //+ define a < operator for set insertion
  int operator<(const Table_Item* item) const {
    return (name.compare(item->name) < 0);}
};

//+ Used for determining whether one item is less than another
struct Table_Item_Less:
  public binary_function<Table_Item*, Table_Item*, bool> {
  bool operator()(const Table_Item* lhs, const Table_Item* rhs) const
  { 
    if (lhs->namesort) 
      return (lhs->name.compare(rhs->name) < 0);
    else
      return (lhs->sortkey < rhs->sortkey);
  }
};

// Section that implements template functions in Tablie_Item class
/*
 *+ 
 * FUNCTION NAME: void Table_Item::get_list(char* list, int* val, int n)
 * 
 * INVOCATION: table_Item.get_list(list,val,n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Get items from comma separated list and put into val
 * 
 * DESCRIPTION: Uses the STL istringstream class to format the output value
 * from an arbitrary input data type.
 * 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template <typename T> void Table_Item::get_list(const string list, T* val, int n)
{
  istringstream def_istr("0");
  int i;
  Tokenizer tokens(list);
  for (i=0; i<n; i++) {
    string str = tokens(",");
    if (!str.empty()) {
      istringstream istr(str.c_str());
      istr >> val[i];
    } else 
      def_istr >> val[i];
  }
}
/*
 *+ 
 * FUNCTION NAME: string Table_Item::put_list(const T* val, int n)
 * 
 * INVOCATION: str = table_Item.put_list(val,n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > val - array of values to form into a list
 * > n - number of elements
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE:  Puts items in a comma separated list from val
 * 
 * DESCRIPTION: Uses the STL ostringstream class to form the list from
 * an arbitrary input data type with default formatting.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template <typename T> string Table_Item::put_list(const T* val, int n)
{
  int i;
  ostringstream ostr;

  for (i=0; i<n; i++) {
    if (i>0) 
      ostr << ",";
    ostr << val[i];
  }
  ostr << ends;
  string str = ostr.str();
  // Unfreeze the stream so that it can be automatically freed
  // ostr.rdbuf()->freeze(0);
  return str;
}
/*
 *+ 
 * FUNCTION NAME: void Table_Item::find(const string par, T& val [int n])
 *
 * INVOCATION: item->find(par,val)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > par - char string, name of parameter to find in hash table
 * < val - ref to output value
 * [> n - number of values allowed in array]
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Gets value[s] associated with parameter name
 * 
 * DESCRIPTION: Uses the STL istringstream class to format the output value from an
 * arbitrary input data type. Leaves val unmodified if parameter not found.
 * For the array case uses the get_list method to fill output array.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- */
template <typename T> bool Table_Item::find(const string par, T& val)
{
  Table_Parameter_Map::iterator iter;
  // We store all parms as lower case, ie ignore case
  string lpar = str_tolower(par);

  if ((iter = pars.find(lpar)) != pars.end()) {
    string str = (*iter).second;
    // Special case for string - ie just copy directly to output value
    if (typeid(T) == typeid(string)) {
      string *s = (string*)&val;
      *s = str;
    } else {
      istringstream istr(str.c_str());
      istr >> val;
    }
    return true;
  } else
    return false;
}
template <typename T> bool Table_Item::find(const string par, T* val, int n)
{
  Table_Parameter_Map::iterator iter;
  string vallist;
  // We store all parms as lower case, ie ignore case
  string lpar = str_tolower(par);

  if ((iter = pars.find(lpar)) != pars.end()) {
    vallist = (*iter).second;
    // Special case for char* - ie an array of chars not comma separated
    if (typeid(T) == typeid(char)) {
      strncpy((char*)val,vallist.c_str(),n); 
    } else
      get_list(vallist, val, n);
    return true;
  } else
    return false;
}
/*
 *+ 
 * FUNCTION NAME: void Table_Item::put(const string par, const T val, [int n])
 *                
 * INVOCATION: item->put(par,val,[n])
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > par - char string, name of parameter
 * > val - char string|int|float, value of parameter
 * [> n - number of items to put from array ]
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Adds param/value pair to set
 * 
 * DESCRIPTION: Places values associated with parameter in par/val set.
 * Uses STL ostringstream to format the parameter value as a string
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
template <typename T> void Table_Item::put(const string par, const T val)
{
  ostringstream ostr;
  // We store all parms as lower case, ie ignore case
  string lpar = str_tolower(par);

  ostr << val << ends;
  string str = ostr.str();
  // Unfreeze the stream so that it can be automatically freed
  // ostr.rdbuf()->freeze(0);
  pars[lpar] = str;
}   
template <typename T> void Table_Item::put(const string par, const T* val, int n)
{
  // We store all parms as lower case, ie ignore case
  string lpar = str_tolower(par);

  if (typeid(T) == typeid(char)) 
    pars[lpar] = (char*) val;
  else 
    pars[lpar] = put_list(val,n);
}   

/*
CLASS
    Table
    A Table is a list of items
DESCRIPTION  
    This class provides methods for operating a set of items that form a
    table. Each table can have an arbitrary number of items. An item is a
    collection of parameter value pairs that are stored as strings in an STL map
    (see Table_Item class)

KEYWORDS
    Table, Table_Item */
class Table {
private:
  // GROUP:   Private member variables
  string item_regexp;                   //+ Reg exp char string that describes an item name
  string item_start;                    //+ char string that indicates start of an item
  string item_end;                      //+ char string that indicates end of an item
  string comment_char;                  //+ char string that indicates start of a comment
  string separator;                     //+ Char string that separates a par from val
  string end_of_par;                    //+ Char string indicates end of par
  int items_per_line;                   //+ Number of par/val pairs to write per line of file
  bool namesort;                        //+ Whether or not items are sorted by name

public:

  //+ Constructor
  // > tname - name of table
  // > il - number of items to format on single output line
  // > ns - Whether or not items are sorted by name
  // > ire - regular expression string that defines item name
  // > is - item name start delimiter
  // > ie - item name end delimiter
  // > cc - comment delimiter
  // > sep - parameter value separator string
  // > eop - parameter value ending strring/delimeter
  // > dp - pointer to item cache
  // > dps - pointer to item cache save
  // > sz - size of data cache
  Table(const string tname="", const int il=5, const bool ns=true,
	const string ire="(\\[[A-Za-z0-9].*\\])", 
	const string is="[", const string ie="]", const string cc="#", 
	const string sep="=", const string eop=";",
	const void* dp=NULL, const void* dps=NULL, const int sz=0); 
   
  //+ Destructor
  virtual ~Table();

  // GROUP:   Public member variables
  int n_items;                          //+ items in list
  string table_name;                    //+ name of this table
  Table_Item_Set items;                 //+ define the set of entries in table 
  void* data;                           //+ pointer to current table item data
  void* data_save;                      //+ pointer to saved current table item data
  int size;                             //+ size of data item in bytes
  string last_name;                     //+ Name of last item accessed in table
  int last_pos;                         //+ Index of last item

  // GROUP:   Public member functions
  //+ assignment operator - copy constructor
  // > t - input Table
  virtual Table& operator=(const Table& t);

  //+ Set data cache pointers and size
  // > dp - pointer to item cache
  // > dps - pointer to item cache save
  // > sz - size of data cache
  virtual void set_data_cache(const void* dp, const void* dps, const int sz); 

  //+ Reads a Table into internal data structure
  // > fname - name of table file to read
  virtual void read(const string fname);

  //+ Writes a table in standard format using delimiters
  // > fname - name of table file to write
  // > mode - Unix file permissions to set for new file
  virtual void write(const string fname, const int mode=S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH);

  //+ Find item in table by name
  // > name - name of item required
  virtual Table_Item* find(const string name) const;


  //+ Find item in table by index
  // > idx - index of item required
  virtual Table_Item* find(int idx) const;

  //+ return index in table of item with name
  // > name - name of item required
  virtual int index(const string name);

  //+ This function does nothing so derived class is expected to provide functionality
  // > itemp - pointer to data item to put into table
  // > pos - index to place table item, defaults to append
  virtual void put(void* itemp, int pos=999);

  //+ Places an item in table
  // > item - table item to insert in table
  // pos - index to place table item, defaults to append
  virtual void insert(Table_Item* item, int pos);

  //+ Removes table item with name
  // > name - name of item to remove
  virtual int destroy(const string name);

  //+ Lists to stdout the contents of the table
  virtual void print();

  //+ Updates the internal representation of the table with the cached data pointed to by data
  virtual void update(); 

  //+ Check to see if an update of the cached data is needed.
  virtual bool need_update();   
  
  //+ Clears table of all entries and frees memory
  virtual void clear();
};

#endif /* TABLE_H */
