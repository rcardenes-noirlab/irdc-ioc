[schematic2]
uniq 7
[tools]
[detail]
w 4736 3259 100 0 n#1 elongins.parkID.VAL 4672 3248 4848 3248 ecars.parkC.ICID
w 2744 3499 -100 0 CLID inhier.CLID.P 2720 3488 2816 3488 2816 3408 2912 3408 ecad4.park.ICID
w 3326 3451 100 0 n#2 junction 3296 3440 3392 3440 3392 2864 3440 2864 cadPlus.parkP.STAT
w 3300 3531 100 0 n#2 ecad4.park.VAL 3232 3440 3296 3440 3296 3632 3392 3632 outhier.VAL.p
w 3272 2811 100 0 n#3 ecad4.park.SPLK 3232 2800 3360 2800 3360 2896 3440 2896 cadPlus.parkP.SPIN
w 3360 2939 100 0 n#4 ecad4.park.STLK 3232 2832 3328 2832 3328 2928 3440 2928 cadPlus.parkP.STIN
w 3332 3483 100 0 n#5 ecad4.park.MESS 3232 3408 3328 3408 3328 3568 3392 3568 outhier.MESS.p
w 2856 3499 100 0 n#6 inhier.DIR.P 2784 3568 2852 3568 2852 3440 2912 3440 ecad4.park.DIR
s 2480 1920 200 0 START and STOP links to perform the required action.
s 2480 1984 200 0 The PARK CAD/CAR schematic. SNL code monitors the parkP record
[cell use]
use elongins 4416 3191 100 0 parkID
xform 0 4544 3264
use inhier 2704 3447 100 0 CLID
xform 0 2720 3488
use inhier 2768 3527 100 0 DIR
xform 0 2784 3568
use cadPlus 3440 2976 150 0 parkP
xform 0 3520 2880
p 3552 2768 100 0 1 set1:cad parkP
use ecars 4912 3328 150 0 parkC
xform 0 5008 3168
p 4816 2976 100 0 1 def(FLNK):$(top)combCars3.PROC
use outhier 3360 3527 100 0 MESS
xform 0 3376 3568
use outhier 3360 3591 100 0 VAL
xform 0 3376 3632
use ecad4 3004 3492 150 0 park
xform 0 3072 3120
p 3476 2620 100 0 0 MFLG:THREE STATES
p 2992 2736 100 0 1 SNAM:CADpark
use eborderC 2280 1659 100 0 eborderC#0
xform 0 3960 2964
p 4856 1812 100 768 -1 author:Peter Young
p 4856 1924 40 0 -1 comment1:1997-10-01 GMOS version. Ken Ramey
p 4856 1904 40 0 -1 comment2:2000-12-11 Modified for GNIRS. Peter Young
p 4856 1780 100 768 -1 date:2000-12-11
p 5080 1860 200 768 -1 file:dcParkCad.sch
p 5352 1812 100 0 -1 page:1
p 5464 1812 100 0 -1 pages:1
p 5128 1812 100 0 -1 revision:0
p 5080 1924 150 768 -1 system:Gemini GNIRS DC
[comments]
