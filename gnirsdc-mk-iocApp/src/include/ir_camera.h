/*
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME ir_camera.h
 *
 * PURPOSE
 * Defines generic operations for an IR camera.
 * This class is used to interface to a generic astronomy IR camera.
 * Classes derived off this are used to interface directly to specific types of IR
 * camera and therefore contain all specific knowledge of that particular camera.
 * This class understands how an IR camera operates, it handles IR specific readout
 * operations and data processing.
 *
 */
#ifndef _IR_CAMERA_H
#define _IR_CAMERA_H

/*---------------------------------------------------------------------------
 Include files
 --------------------------------------------------------------------------*/
#include "configuration.h"
#include "dummy_controller.h"
#include "sdsu3_5_controller.h"
#include "camera.h"
#include "ipc.h"

/*---------------------------------------------------------------------------
 Constants
 --------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Typedefs
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Global variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Class declarations
 -------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
CLASS
    IR_Camera
    Infrared Camera class definition.

DESCRIPTION
    Definition of the Infrared Camera class in CICADA's model for astronomical
    instrumentation

KEYWORDS
    Hardware, Camera, IR, Infrared
----------------------------------------------------------------------------*/
class IR_Camera: public Camera {

private:
  // GROUP: private member variables
  bool xfer_image;                      //+ set if required to transfer the resultant image


  // GROUP: IR_Camera Member methods

  //+ initialise class member variables - called from constructors
  void init_vars();

  //+ Add status parameters to the status database - called from main constructor
  // > init - flag set if required to initialise added parameters
  void add_status_parameters(bool init);

  //+ Add ir readout mode status parameters to the status database - called from add_status_parameters
  // > ir_mode - readout mode (either observe or view)
  // > init - flag set if required to initialise added parameters
  void add_ir_parameters(IR_Mode ir_mode, bool init);

  //+ Called periodically to upate IR status varaibles during an exposure
  // > ir_mode - current exposure mode
  void update_ir_status_info(IR_Mode ir_mode);

  //+ routine to initailise work space
  void initialise_fitpix();

 //+ Cleans up after an IR exposure
  void cleanup_exposure();

  // Controllers
  Dummy_Controller *dummy_controller;


protected:

  // GROUP:  IR_Camera protected member variables

  IR_Expose expose_set;                 //+ Current settings for expose
  IR_Expose view_expose;                //+ Current settings for expose
  IR_Exposure_Info view_einfo;          //+ View mode exposure parameters
  IR_Exposure_Info observe_einfo;       //+ observe mode exposure parameters
  IR_Data_Info view_dinfo;              //+ View mode data parameters
  IR_Data_Info observe_dinfo;           //+ Observe mode data parameters
  IR_Exposure_Info *ir_einfo;           //+ Pointer to current ir exposure info structure
  IR_Data_Info *ir_dinfo;               //+ Pointer to current ir data info structure
  unsigned long byte_counter;           //+ counter of number of bytes read
  float* fpixSum;                       //+ Float summed pixel buffer
  float* fitPix;                        //+ Fitted pixel buffer
  float* coaddPix;                      //+ Coadded pixel buffer
  float* fitPixSave;                    //+ Saved fitted pixel buffer
  unsigned short* zeroPixSave;          //+ Saved first NDR
  unsigned long* pixSum;                //+ Summed pixel buffer
  double* fit_sy;                       //+ Linear fitting work array
  double* fit_siy;                      //+ Linear fitting work array
  double* fit_sy2;                      //+ Linear fitting work array
  float* variance;                      //+ Linear fitting variance array
  float* variance1;                     //+ Linear fitting variance array saved after first cosmic ray detection
  int dx,fit_x,fit_sx,fit_sxx;          //+ Linear fitting work variables
  unsigned char* badPix;                //+ Bad pixel array
  unsigned char* cosmicPix;             //+ Record of NDR where first cosmic ray encountered
  int period_no;                        //+ Read counter
  int nview;                            //+ view exposure counter
//  Filenametype raw_fname;               //+ Filename for holding raw data
//  int raw_fd;                           //+ raw file descriptor

  // GROUP:
  //+ Initialise pixSum and other calculation buffers to zero
  virtual void initialise_readout_buffers();

  //+ Does readout processing particular to the readout mode chosen
  // > iptr - pointer to input pixels
  // > npix - number of pixels to process
  // > fs - fowler sample number
  // > fi - starting index into fitPix array
  virtual void process_ir_data(char* iptr, int npix, int fs, int fi);


  //+ Opens file ready for receiving raw IR data
  // > im_info - image info
  // > finfo - fits info
  // > ndr - NDR number
  // > coadd - coadd number
//  virtual void open_raw_ir_file(Detector_Image_Info im_info, int ndr, int coadd);

  //+ Saves raw IR data into local disk file
  // > iptr - pointer to input pixels
  // > npix - number of pixels to process
//  virtual void save_raw_ir_data(char* iptr, int np);

public:
  Sdsu3_5_Controller *sdsu_controller;
  static const std::string PARAMETER_NAME;

  // GROUP: IR_Camera public Member variables.
  IR_Status *ir_status;                 //+ Current IR camera status ins shared mem
  Parameter *ir_statusp;                //+ Cicada parameter status database for dummy

  epicsTime exp_timestamp;              //+ Timestamp when first IR NDR started
  epicsTime last_timestamp;             //+ Timestamp when last IR NDR started
  Thread *ir_timer_thr;                 //+ thread used for IR timing

  //+ Simple constructor - used for setting up configuration
  // > cam   - Camera_Desc - description of the camera
  // > camid - camera id
  // > c     - controller id
  IR_Camera(Camera_Desc& cam, int camid, int c, Platform_Specific* ps);

  //+ Constructor for parameters?
  // > cam   - Camera_Desc - description of the camera
  // > camid - camera id
  // > c 	  - controller id
  // > param_host - host on which the parameter database resides
  // > st	- status memory for this instrument
  // > pm	- parameter map
  IR_Camera(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st, Platform_Specific* ps);

  //+ Full constructor.
  // > st  - status memory for this instrument
  // > ip  - instrument status parameter object
  // > deb - debug level
  IR_Camera(Instrument_Status *st, Parameter* ip, int deb, Platform_Specific* ps);

  //+ Destructors
  virtual ~IR_Camera();

  // other IR_Camera public Member functions.


  //+ Initialise IR camera operations
  // > init_rec - initialisation record.
  virtual void init(Init init_rec);

  //+ Readout methods - must be derived
  virtual void adjust_image_header(Detector_Image_Main_Header& main_hdr) {};

  // Do Readout and associated pure virtual Camera methods
  //+ prepares system for a readout.
  // > n_sync - number of controllers to sync.
  // > expose - Exposure request.
  virtual void prepare_readout(int n_sync, IR_Expose& expose);

  //+ Read out the contents of the detector.
  // > readout_rec - Cicada Readout record
  // > sfy - starting y position in possible multi frame readout.
  virtual void readout(Detector_Readout readout_rec, int rn = 0);

  //+ Updates the exposure-time-left and period-time-left counters.
  virtual void update_ir_times();

  //+ Cleans up after readout finished.  wow, how meaningful!
  virtual void cleanup_readout(bool finish);

  //+ Do Exposure
  // > expose - IR exposure request info
  virtual void exposure(IR_Expose expose);

  //+ Calculate exposure time
  virtual void calc_exposure_time(IR_Exposure_Info *ir_einfo);


  // Handle CCD_Camera Signals
  //+ Received a signal to pause. If shutter open, close it then wait for
  // semaphore to continue - once set, reopen shutter if closed.
  virtual void handle_interrupt();

  //+ Handle change state signal
  virtual void handle_change_state();

  std::string get_sensor_parameter_name(const std::string& param) const;
  template <typename T> void update_sensor_parameter(const std::string& param, const T& value) {
	ir_statusp->put(get_sensor_parameter_name(param), value);
  }
  template <typename T> void get_sensor_parameter(const std::string& param, T& value) {
	ir_statusp->get(get_sensor_parameter_name(param), value);
  }

  /*
   * The following deal with parameters that are not exposed in the database,
   * only needed for the raw header
   */
  void add_raw_parameter(const std::string& unit, const std::string& param, const std::string value);
  void add_config_unit(const std::string& unit_name);
};

// GROUP: IR_Camera function declarations
/*
 *+
 * FUNCTION NAME: sum_ir_data
 *
 * INVOCATION: sum_ir_data(T* rptr, U* dptr, int np)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * < rptr - pointer to result pixels
 * > dptr - pointer to input pixels
 * > np - number of pixels
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Simple template function used for summing image data types
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
template<class T, class U> void sum_ir_data(T* rptr, U* dptr, int np)
{
  int i;
  for (i=0; i<np; i++) {
    rptr[i] += dptr[i];
  }
}
/*
 *+
 * FUNCTION NAME: copy_ir_data
 *
 * INVOCATION: copy_ir_data(T* rptr, U* dptr, int np)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * < rptr - pointer to result pixels
 * > dptr - pointer to input pixels
 * > np - number of pixels
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Simple template function used for copying image data types to floating
 * point array
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
template<class T, class U> void copy_ir_data(T* rptr, U* dptr, int np)
{
  int i;
  for (i=0; i<np; i++) {
    rptr[i] = (T)dptr[i];
  }
}
#endif //_IR_CAMERA_H
