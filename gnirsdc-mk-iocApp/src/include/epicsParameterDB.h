/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * GEMINI GNIRS PROJECT
 *
 * FILENAME epicsParameterDB.h
 * 
 * PURPOSE
 * Definitions for access to EPICS database from simple class interface
 * This is derived from the base class ParameterDB
 *
 */

#ifndef EPICS_PARAMETER_DB_H
#define EPICS_PARAMETER_DB_H

#include <string>
#include <typeinfo>
#include "parameterDB.h"
#include <dbAccess.h>

using namespace std;

// put definitions here 
const long EPICS_TRUE = 0;
const long EPICS_FALSE = 1;

/*
CLASS
    EpicsParameterDB
    DC parameters - these are set by DC processes
DESCRIPTION  
    DC EPICS parameter class. This class handles the mapping from
    local parameter map to the EPICS DC parameter database. It allows for
    simple get and put operations on EPICS parameters - providing a 
    simple interface to the EPICS channel access library.

KEYWORDS
    EpicsParameterDB ParameterDB
*/
class EpicsParameterDB: public ParameterDB {
 public:

  //+ Constructor
  EpicsParameterDB();

  //+ Destructor
  ~EpicsParameterDB();

  // GROUP:   Member functions
  //+ set parameter address from name
  // > dbName - name of database variable
  // > param - common name of parameter
  // > prefix - database prefix
  // > suffix - database suffix
  void* setAddr(const char* dbName, const char* param, const char* prefix, const char* suffix);

  //+ ParameterDB put methods - overloaded to support simple data types
  // > paramDBName - name of parameter db that has called this put
  // > dbName - name of parameter in backend database
  // > param - common name of parameter - may be same as dbName
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local addr - used for memcpy
  // > value - value of parameter to set
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to put - default 1
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const string* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const char* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr,
	   const unsigned char* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const short* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const unsigned short* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const int* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const unsigned int* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const long* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const unsigned long* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const float* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const double* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const bool* value, unsigned int offset, unsigned int n);
  
  // ParameterDB get methods, overloaded to support multiple data types
  // > paramDBName - name of parameter db that has called this put
  // > dbName - name of parameter in backend database
  // > param - common name of parameter - may be same as dbName
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local addr - used for memcpy
  // > value - value of parameter to set
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to put - default 1
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   string* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   char* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   unsigned char* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   short* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   unsigned short* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   int* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   unsigned int* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   long* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   unsigned long* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   float* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   double* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   bool* value, unsigned int offset, unsigned int n);

  //+ Free address in use
  // > addr - pointer to DB addr structure to free
  void freeAddr(const void* addr);

  //+ publish the named parameter
  // These methods are used to support parameter monitoring
  // > addr - pointer to DB addr structure 
  void publish(const void* addr);

  //+ attach the local and database addresses
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local address
  void attach(const void* addr, const void* localAddr);

  //+ add a callback for when a monitored parameter changes
  // > dbName - name of parameter db that has called this get
  // > addr - pointer to DB addr structure 
  // > callback - pointer ro callback function
  int add_callback(const void* addr, Parameter_Callback* callback);

  //+ remove a callback for a monitored parameter
  // > cb_id - callback identifier
  void remove_callback(int cb_id);

  //+ wait for a parameter to change
  // > addr - pointer to DB addr structure 
  void wait(const void* addr);

 private:
  //+ These methods do the real work of putting and getting
  // > dbName - name of parameter db that has called this get
  // > param - name of parameter
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local addr - used for memcpy
  // < value - value of parameter to get
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to put - default 1
  template <class T> void put_local_param(const void *localAddr, T* value, unsigned int offset, unsigned int n);
  template <class T> void get_local_param(const void *localAddr, T* value, unsigned int offset, unsigned int n);
  template <class T> void put_epics_param(const char* dbName, const char* param, const void *addr, const void* localAddr, 
					  T* value, unsigned int offset, unsigned int n);
  template <class T> void get_epics_param(const char* dbName, const char* param, const void *addr, const void* localAddr, 
					  T* value, unsigned int offset, unsigned int n);


  long dbNormalizedNameToAddr (std::string dbName, dbAddr *dba);
};
/*
 *+ 
 * FUNCTION NAME: template <class T> put_local_param(const void *localAddr, T* value, unsigned int offset, unsigned int n)
 * 
 * INVOCATION: put_local_param(const void *localAddr, T* value, unsigned int offset, unsigned int n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > localAddr - pointer to local structure 
 * > value - value of parameter to set
 * > offset - offset added to addr to get desired starting address - default 0
 * > n - number of elements to put - default 1
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: sets local parameter to value
 * 
 * DESCRIPTION: uses localAddr address to set a local non-Epics parameter to 
 * particular value. This is the template function used by indivdual put methods
 * from EpicsparameterDB class
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
template <class T> void EpicsParameterDB::put_local_param(const void *localAddr, T* value, unsigned int offset, unsigned int n)
{
  // Copy directly in memory to localAddr
  if (localAddr != NULL) {
    T* iaddr = (T*) localAddr;
    memcpy((void*)(iaddr+offset), (const void*) value, n * sizeof(T));
  }

}

/*
 *+ 
 * FUNCTION NAME: template <class T> get_local_param(const void *localAddr, T* value, unsigned int offset, unsigned int n)
 * 
 * INVOCATION: get_local_param(const void *localAddr, T* value, unsigned int offset, unsigned int n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > localAddr - pointer to local structure 
 * > value - value of parameter to set
 * > offset - offset added to addr to get desired starting address - default 0
 * > n - number of elements to put - default 1
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: gets local parameter value
 * 
 * DESCRIPTION: uses localAddr address to get a local non-Epics parameters value. 
 * This is the template function used by indivdual put methods from EpicsparameterDB class
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
template <class T> void EpicsParameterDB::get_local_param(const void *localAddr, T* value, unsigned int offset, unsigned int n)
{
  // Copy directly in memory from localAddr
  if (localAddr != NULL) {
    T* iaddr = (T *) localAddr + offset;
    memcpy(value, iaddr, n * sizeof(T)); 
  }
}

/*
 *+ 
 * FUNCTION NAME: template <class T> put_epics_param(const char* dbName, const char* param, const void *addr, void *localAddr,  
 *                                                  T* value, unsigned int offset, unsigned int n) 
 * 
 * INVOCATION: put_epics_param(param, addr, localAddr, value, offset, n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > dbName - name of parameter db that has called this put
 * > param - name of parameter
 * > addr - pointer to addr structure 
 * > localAddr - pointer to local structure 
 * > value - value of parameter to set
 * > offset - offset added to addr to get desired starting address - default 0
 * > n - number of elements to put - default 1
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: sets epics parameter to value
 * 
 * DESCRIPTION: uses addr address to set an Epics parameter to 
 * particular value. This is the template function used by indivdual put methods
 * from EpicsparameterDB class
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
template <class T> void EpicsParameterDB::put_epics_param(const char* dbName, const char* param, const void *addr, 
							  const void* localAddr, T* value, unsigned int offset, unsigned int n)
{
  ostringstream ostr;

  if (value == NULL) {
    ostr << "EpicsParameterPut: Cannot put NULL value for parameter '" << param << "(" << dbName << ")'!" << ends; 
    throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);
  }

  if (strlen(dbName) > 0) {
    if (addr != NULL) {
      //cout << "About to call dbPutField for " << param << " of type " << typeid(T).name() << endl; 
      long status;
      char pbuffer[MAX_STRING_SIZE];
      dbAddr* dba = (dbAddr *)static_cast<const struct dbAddr*>(addr);

      if (typeid(T) == typeid(string)) {
	strncpy(pbuffer,(char*)((string*)value)->c_str(), MAX_STRING_SIZE-1);
	status = dbPutField(dba, DBR_STRING, (void*)pbuffer, 1L);
      } else if (typeid(T) == typeid(char)) {
	strncpy(pbuffer, (char*)value, MAX_STRING_SIZE-1);
	status = dbPutField(dba, DBR_STRING, (void*)pbuffer, 1L);
      } else if (typeid(T) == typeid(long)) {
	status = dbPutField(dba, DBR_LONG, (void*)value, n);
      } else if ((typeid(T) == typeid(int)) || (typeid(T) == typeid(unsigned int)) ||
		 (typeid(T) == typeid(short)) || (typeid(T) == typeid(unsigned short)) ||
		 (typeid(T) == typeid(unsigned long))) {
	unsigned int i;
	long *tvalue = new long[n];
	int *tb = (int*)value;
	for (i=0; i<n; i++) {
	  tvalue[i] = tb[i];
	}	
	status = dbPutField(dba, DBR_LONG, (void*)tvalue, n);
	delete [] tvalue;
      } else if (typeid(T) == typeid(double)) {
	status = dbPutField(dba, DBR_DOUBLE, (void*)value, n);
      } else if (typeid(T) == typeid(bool)) {
	unsigned int i;
	long *tvalue = new long[n];
	bool *tb = (bool*)value;
	for (i=0; i<n; i++) {
	  if (tb[i]) tvalue[i] = EPICS_TRUE;
	  else tvalue[i] = EPICS_FALSE;
	}
	status = dbPutField(dba, DBR_LONG, (void*)tvalue, n);
	delete [] tvalue;
      } else
	status = ERR;
      if (status != OK) {
	ostr << "EpicsParameterPut: Failed in dbPutField for '" << param << "(" << dbName << ")' status=" << status << 
	  "!" << ends;
	throw Error(ostr, E_ERROR, status, __FILE__, __LINE__);
      }
    } else {
      ostr << "EpicsParameterPut: Cannot put value to NULL address!" << ends; 
      throw Error(ostr, E_WARNING, -1, __FILE__, __LINE__);    
    }
  }

  // Copy directly in memory to localAddr
  if (localAddr != NULL) {
    T* iaddr = (T*) localAddr;
    memcpy((void*)(iaddr+offset), (const void*) value, n * sizeof(T));
  }
}
/*
 *+ 
 * FUNCTION NAME: template <class T> void get_epics_param(const char* dbName, const char* param, void *addr, void *localAddr, 
 *                                                       T* value, int offset, int n)  
 * 
 * INVOCATION: get(const char* dbName, param, addr, localAddr, value, offset, n)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > dbName - name of parameter db that has called this get
 * > param - name of parameter
 * > addr - pointer to addr structure 
 * > localAddr - pointer to local structure 
 * < value - value of parameter to get
 * > offset - offset added to addr to get desired starting address - default 0
 * > n - number of elements to put - default 1
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: gets EPICS parameter  value
 * 
 * DESCRIPTION: uses addr address to get an EPICS parameter value. 
 * This is a template function to support simple data types
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: None
 * 
 * DEFICIENCIES: None
 *
 *- 
 */
template <class T> void EpicsParameterDB::get_epics_param(const char* dbName, const char* param, const void *addr, 
							  const void *localAddr,  T* value, unsigned int offset, unsigned int n)
{
  ostringstream ostr;

  if (value == NULL) {
    ostr << "EpicsParameterGet: Must allocate enough memory to receive parameter '" << param <<
      "(" << dbName << ")'!" << ends;	    
    throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);
  }

  if ((strlen(dbName) > 0) && (addr != NULL)) {
    long status;
    long nv = n;
    long options = 0;
    char pbuffer[MAX_STRING_SIZE];
    dbAddr* dba = (dbAddr *)static_cast<const dbAddr*>(addr);
    
    //cout << "About to call dbGetField for " << param << " of type " << typeid(T).name() << endl; 
    
    if (typeid(T) == typeid(string)) {
      string val;
      status = dbGetField(dba, DBR_STRING, (void*)pbuffer, &options, &nv, NULL);
      // For now just support handling one string (need to find out how multiple strings are handled in pbuffer!)
      val = pbuffer;
      *(string*) value = val;
    } else if (typeid(T) == typeid(char)) {
      status = dbGetField(dba, DBR_STRING, (void*)pbuffer, &options, &nv, NULL);
      strncpy((char*)value, pbuffer, n);
    } else if ((typeid(T) == typeid(int)) || (typeid(T) == typeid(unsigned int)) ||
	       (typeid(T) == typeid(short)) || (typeid(T) == typeid(unsigned short)) ||
	       (typeid(T) == typeid(unsigned long))) {
      int i;
      long *tvalue = new long[nv];
      status = dbGetField(dba, DBR_LONG, (void*)tvalue, &options, &nv, NULL);
      for (i=0; i<nv; i++)
	value[i] = tvalue[i];
      delete [] tvalue;
    } else if (typeid(T) == typeid(long)) {
      status = dbGetField(dba, DBR_LONG, (void*)value, &options, &nv, NULL);
      /*
       * DBR_LONG types are 32 bit. In 64 bit platforms this may cause dbGetField
       * to leave garbage on the most significant half...
       */
      *((long *)(value)) &= 0x0000FFFFL;
    } else if (typeid(T) == typeid(double)) {
      status = dbGetField(dba, DBR_DOUBLE, (void*)value, &options, &nv, NULL);
    } else if (typeid(T) == typeid(bool)) {
      int i;
      long *tvalue = new long[nv];
      status = dbGetField(dba, DBR_LONG, (void*)tvalue, &options, &nv, NULL);
      for (i=0; i<nv; i++)
	value[i] = (tvalue[i] == EPICS_TRUE);
      delete [] tvalue;
    } else
      status = ERR;
    
    if (status != OK) {
      ostr << "EpicsParameterGet: Failed in dbGetField for '" << param << "(" << dbName << ")'!" << ends;
      throw Error(ostr, E_ERROR, status, __FILE__, __LINE__);
    }
    
  } else if (localAddr != NULL) {
    T* iaddr = (T *) localAddr + offset;
    memcpy(value, iaddr, n * sizeof(T));   
  } else {
    ostr << "EpicsParameterGet: Cannot get value from NULL address!" << ends;	    
    throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);
  }

}

#endif /* EPICS_PARAMETER_DB_H */
