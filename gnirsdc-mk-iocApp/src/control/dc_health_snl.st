/*
 *   FILENAME
 *   --------
 *   dc_health_snl.stpp
 *
 *   PURPOSE
 *   -------
 *   This file contains the EPICS state transition language program for
 *   managing the GNIRS detector controller health.
 *
 *
 *   FUNCTION NAME(S)
 *   ----------------
 *   camera_ss  - Manage the SDSU camera health status.
 *
 *   PROGRAM
 *   --------
 *   dcHealth
 *
 *   INVOCATION
 *   -----------
 *   Include the following line in the VxWorks startup script:
 *
 *   seq &dcHealth, "top=gnirs:dc:, sadtop=gnirs:sad:dc:"
 *
 *   PARAMETERS (all are input)
 *   --------------------------
 *      top      string    String to be substituted for the {top} macro
 *                         used to for the top level name of the DC
 *                         EPICS database. The default value is "gnirs:dc:"
 *
 *      sadtop   string    String to be substituted for the {sad} macro
 *                         used for the name of the DC Status/Alarm
 *                         Database. The default value is "gnirs:sad:dc:"
 *
 *
 *   RETURN VALUE
 *   ------------
 *   N/A
 *
 *   GLOBALS
 *   -------
 *      EXTERNAL FUNCTIONS
 *
 *      This program makes use of the library functions included with the 
 *      EPICS state notation language (efTest, efClear, pvGet, pvPut, delay)
 *
 *      EXTERNAL VARIABLES
 *
 *      Several records in the DC CAD, CAR and Status/Alarm database
 *      are used as external variables. See the declarations in the
 *      code for details
 *
 *   REQUIREMENTS
 *   ------------
 *   This program assumes that the EPICS database "dcTop", has been 
 *   loaded, and that this databases contain records whose names match 
 *   those in the "assign" declarations below. If any changes are made to 
 *   the names of these records, the changes must also be reflected in 
 *   this program. Failure to do this will result in one or more channels 
 *   not connecting when the program is executed
 *
 *   The ordering of the arguments within each CAD record (A, B, C...) is
 *   defined in the description of the interface between the CAD database
 *   and its clients. Changes to that interface should be reflected in this
 *   file.
 *
 *   AUTHORS
 *   ------
 *   Peter Young
 *
 *   HISTORY
 *   -------
 *INDENT-OFF*
 *
 * $Log: dc_health_snl.st,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.3  2005/09/27 03:08:01  pjy
 * Used strncpy rather than strcpy for safety
 *
 * Revision 1.2  2005/09/23 01:39:09  pjy
 * Made NULL string a single blank space
 *
 * Revision 1.1  2005/08/18 03:54:47  pjy
 * Extracted health management and put in new health SNL code
 *
 *
 *INDENT-ON*
 *
 */

program dcHealth ("top=gnirs:dc:, sadtop=gnirs:sad:dc:")

/*
 * The following option is needed to make the code reentrant. This
 * allows for multiple instances of this sequence code, although this 
 * is not a current requirement of the GNIRS detector controller.
 */

option +r;

%%#include <stdlib.h>
%%#include <stdio.h>
%%#include <string.h>
%%#include <ctype.h>
%%#include <time.h>

%%#include <cadRecord.h>
%%#include <carRecord.h>

%%#include <cicsConst.h>
%%#include <timeLib.h>

%%#include "dc_state_c.h"


/*
 * Constants
 */

#define CHECK_HEALTH_STATUS_TIME 30
#define SIM_FULL       "FULL"
#define SIM_FAST       "FAST"
#define SIM_NONE       "NONE"
#define HEALTH_GOOD    "GOOD"
#define HEALTH_BAD     "BAD"
#define HEALTH_WARNING "WARNING"

#define NULL_MSG       " "
#define BAD_VERSION    "SDSU DSP version unknown - beware!"
#define NO_POWER       "SDSU power off - camera inoperable!"
#define NO_COMMS       "Cannot communicate with SDSU?"
#define NOT_INIT       "SDSU not initialised - may not perform?"


#define debug(s) if (strcmp(debugLevel,"NONE")) printf(s)

     /* SUBSYSTEM COMMAND DECLARATIONS
 *
 * (1) "long" variables are assigned to counters which are incremented
 *     in the database whenever a successful START directive is detected.
 *     The STOP directive is not monitored because it is rejected by the
 *     "init" CAD record.
 *
 * (2) An event flag is synchronised with each "long" variable, so the
 *     flag is triggered whenever the variable changes. The net result
 *     is that an event flag is triggered whenever a command is started
 *     or stopped in the outside world.
 */

string debugLevel;        /* Debug level supplied with debug command */
assign debugLevel to "{top}debug.VALA";

int32_t initSDSUDone;         /* DONE flag set when sdsu initialised */
assign initSDSUDone to "{sadtop}initSDSUDone.VAL";
monitor initSDSUDone;

string cameraHealth;         /* Camera (SDSU) health SIR record - val */
assign cameraHealth to "{sadtop}cameraHealth.VAL";
string cameraHealthMsg;         /* Camera (SDSU) health SIR record - msg */
assign cameraHealthMsg to "{sadtop}cameraHealth.IMSS";

string DSPVer;         /* SDSU DSP version number Va.b.c */
assign DSPVer to "{sadtop}DSPVer.VAL";
monitor DSPVer;

int32_t SDSUpon;         /* SDSU power on flag */
assign SDSUpon to "{sadtop}SDSUpon.VAL";
monitor SDSUpon;

int32_t SDSUcomms;        /* SDSU communications flag */
assign SDSUcomms to "{sadtop}SDSUcomms.VAL";
monitor SDSUcomms;

int32_t cameraOp;         /* SDSU problem flag */
assign cameraOp to "{sadtop}cameraOp.VAL";
monitor cameraOp;
string cameraOpMsg;        /* SDSU problem message */
assign cameraOpMsg to "{sadtop}cameraOpMsg.VAL";

/* FITS health variables */
string fitsHealth;         /* FITS health SIR record - val */
assign fitsHealth to "{sadtop}fitsHealth.VAL";
string fitsHealthMsg;      /* FITS health SIR record - msg */
assign fitsHealthMsg to "{sadtop}fitsHealth.IMSS";

int32_t fitsAvailable;        /* FITS available flag */
assign fitsAvailable to "{sadtop}fitsAvailable.VAL";
monitor fitsAvailable;

int32_t fitsConnected;        /* FITS connection flag */
assign fitsConnected to "{sadtop}fitsConnected.VAL";
monitor fitsConnected;

int32_t fitsProblem;          /* FITS problem flag */
assign fitsProblem to "{sadtop}fitsProblem.VAL";
string fitsProblemMsg;     /* FITS problem message */
assign fitsProblemMsg to "{sadtop}fitsProbMsg.VAL";
monitor fitsProblem;

/* Simulation variables */
string simMode;            /* Simulation ? */
assign simMode to "{sadtop}simMode.VAL";
monitor simMode;

/* ===================================================================== */
/*+
 *   Function name:
 *   State set "camera_ss"
 *
 *   Purpose:
 *   This state set monitors the health of the SDSU camera.
 *
 *   Invocation:
 *   Invoked automatically when sequence program "dcHealth" started
 *
 *   Parameters: (">" input, "!" modified, "<" output)
 *   None
 *
 *   Return value:
 *   N/A
 *
 *   External functions:
 *   EPICS sequencer functions used are
 *   pvGet      - Get a process variable
 *   pvPut      - Put a process variable
 *   delay      - Wait for a specified length of time
 *
 *   External variables:
 *   See definitions above
 *
 *   Requirements:
 *   Same as prior requirements for program "dcHealth"
 *
 *   Limitations:
 *   Same as deficiencies for program "dcHealth"
 *
 *-
 */

ss camera_ss {

  /*******************************************************************
   * state startup/unknown - This is the state the camera_ss state set begins in
   * when it first starts running. 
   *******************************************************************/
 
  state startup {

    entry {
      debug("camera_ss: entering startup\n");
    }

    when (pvConnectCount() == pvChannelCount()) {
      /* 
       * Initialise - get vars of interest 
       */
      pvGet(SDSUpon);
      pvGet(SDSUcomms);
      pvGet(initSDSUDone);
      pvGet(DSPVer);
      pvGet(cameraOp);
      pvGet(simMode);

      debug("camera_ss: startup -> unknown\n");
    } state unknown

  }

  state unknown {

    /* GOOD health if in simulation or initialised, has comms and has PON */
    when (((initSDSUDone == EPICS_TRUE) || (strcmp(simMode,SIM_FULL)==0)) && 
	  ((SDSUcomms == EPICS_TRUE) || (strcmp(simMode,SIM_FULL)==0)) && 
	  ((SDSUpon == EPICS_TRUE) || (strcmp(simMode,SIM_NONE)!=0)) && 
	  ((strlen(DSPVer)>0) || (strcmp(simMode,SIM_NONE)!=0))) {
      strncpy(cameraHealthMsg,NULL_MSG, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_GOOD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: unknown -> good (in simulation/initialized, has comms, PON, DSP version)\n");
    } state good

    /* GOOD health if in simulation or has comms, has PON and dsp version string */
    when (((SDSUcomms == EPICS_TRUE) || (strcmp(simMode,SIM_FULL)==0)) && 
	  ((SDSUpon == EPICS_TRUE) || (strcmp(simMode,SIM_NONE)!=0)) && 
	  ((strlen(DSPVer)>0) || (strcmp(simMode,SIM_NONE)!=0))) {
      strncpy(cameraHealthMsg,NULL_MSG, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_GOOD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: unknown -> good (in simulation OR has comms, PON, DSP version)\n");
    } state good

    when ((SDSUcomms == EPICS_TRUE) && (SDSUpon == EPICS_TRUE) && (strlen(DSPVer)==0)) {
      strncpy(cameraHealthMsg,BAD_VERSION, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_WARNING, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: unknown -> warning\n");
    } state warning

    /* Comms but no power and not in simulation mode - BAD */
    when ((SDSUcomms == EPICS_TRUE) && (SDSUpon == EPICS_FALSE) && (strcmp(simMode,SIM_NONE)==0)) {
      strncpy(cameraHealthMsg, NO_POWER, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_BAD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);
      
      debug("camera_ss: unknown -> bad\n");
    } state bad

    /* No comms and not in FULL simulation mode - BAD */
    when ((SDSUcomms == EPICS_FALSE) && (strcmp(simMode,SIM_FULL)!=0)) {
      strncpy(cameraHealthMsg, NO_COMMS, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_BAD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);
      
      debug("camera_ss: unknown -> bad\n");
    } state bad
  }

  /*******************************************************************
   * state unknown - This is the state the camera_ss state set begins in
   * when it first starts running. 
   *******************************************************************/
 
  state good {

    when ((SDSUcomms == EPICS_FALSE) && (strcmp(simMode,SIM_FULL)!=0)) {
      strncpy(cameraHealthMsg, NO_COMMS, MAX_STRING_SIZE-1);
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_BAD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: good -> bad (!SDSUcomms && simMode != SIM_FULL)\n");
    } state bad

    when ((SDSUpon == EPICS_FALSE) && (strcmp(simMode,SIM_NONE)==0)) {
      strncpy(cameraHealthMsg,NO_POWER, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_BAD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: good -> bad (!SDSUpon && simMode == SIM_NONE)\n");
    } state bad

    when ((cameraOp == EPICS_FALSE) && (SDSUpon == EPICS_TRUE)) {
      pvGet(cameraOpMsg);
      strncpy(cameraHealthMsg, cameraOpMsg, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_WARNING, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);
 
      debug("camera_ss: good -> warning (!cameraOp && SDSUpon)\n");
    } state warning

    when ((initSDSUDone == EPICS_FALSE) && (SDSUpon == EPICS_TRUE) && (strcmp(simMode,SIM_NONE)==0)) { 
      strncpy(cameraHealthMsg, NOT_INIT, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_WARNING, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);
      
      debug("camera_ss: good -> warning (!initSDSUDone && SDSUpon && simMode == SIM_NONE)\n");
    } state warning

    when ((strlen(DSPVer)==0) && (SDSUpon == EPICS_TRUE) && (strcmp(simMode,SIM_NONE)==0)) {
      strncpy(cameraHealthMsg, BAD_VERSION, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_WARNING, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: good -> warning (DSPVer == '' && SDSUpon && simMode == SIM_NONE\n)");
    } state warning
  }
	 

  /*******************************************************************
   * state bad - This is the state the camera_ss state set moves to
   * when the camera health is in a bad condition.
   * It responds to the following signals while in this state:
   *******************************************************************/
 
  state bad {

    /* GOOD health if in simulation or initialised, has comms and has PON */
    when (((initSDSUDone == EPICS_TRUE) || (strcmp(simMode,SIM_FULL)==0)) && 
	  ((SDSUcomms == EPICS_TRUE) || (strcmp(simMode,SIM_FULL)==0)) && 
	  ((SDSUpon == EPICS_TRUE) || (strcmp(simMode,SIM_NONE)!=0)) && 
	  ((strlen(DSPVer)>0) || (strcmp(simMode,SIM_NONE)!=0))) {
      strncpy(cameraHealthMsg,NULL_MSG, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_GOOD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: bad -> good\n");
    } state good

    /* GOOD health if in simulation or has comms and has PON and DSP version ok */
    when (((SDSUpon == EPICS_TRUE) || (strcmp(simMode,SIM_NONE)!=0)) && 
	  ((SDSUcomms == EPICS_TRUE) || (strcmp(simMode,SIM_FULL)==0)) && 
	  ((strlen(DSPVer)>0) || (strcmp(simMode,SIM_NONE)!=0))) {
      strncpy(cameraHealthMsg,NULL_MSG, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_GOOD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: bad -> good\n");
    } state good

    /* Could transition to good comms bad no power */
    when ((SDSUpon == EPICS_FALSE) && (SDSUcomms == EPICS_TRUE) && (strcmp(simMode,SIM_FULL)!=0) && 
	  (strcmp(cameraHealthMsg,NO_POWER) != 0)) {
      strncpy(cameraHealthMsg,NO_POWER, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_BAD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: bad -> bad\n");
    } state bad

    /* Could transition from good comms to bad comms*/
    when ((SDSUcomms == EPICS_FALSE) && (strcmp(simMode,SIM_FULL)!=0) && 
	  (strcmp(cameraHealthMsg,NO_COMMS) != 0)) {
      strncpy(cameraHealthMsg,NO_COMMS, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_BAD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: bad -> bad\n");
    } state bad
  }

  /*******************************************************************
   * state warning - This is the state the camera_ss state set moves to
   * when the camera health is in a warning condition.
   * It responds to the following signals while in this state:
   *******************************************************************/
 
  state warning {

   when ((SDSUcomms == EPICS_FALSE) && (strcmp(simMode,SIM_FULL)!=0)) {
      strncpy(cameraHealthMsg, NO_COMMS, MAX_STRING_SIZE-1);
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_BAD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);
//      efClear(cameraOpFlag);

      debug("camera_ss: warning -> bad (!SDSUcomms && !SIM_FULL)\n");
    } state bad

    when ((SDSUpon == EPICS_FALSE) && (strcmp(simMode,SIM_NONE)==0)) {
      strncpy(cameraHealthMsg, NO_POWER, MAX_STRING_SIZE-1);
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_BAD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: warning -> bad (!SDSUpon && SIM_NONE)\n");
    } state bad

    when ((cameraOp == EPICS_TRUE) && 
	  ((SDSUcomms == EPICS_TRUE) || (strcmp(simMode,SIM_FULL)==0)) &&
	  ((initSDSUDone == EPICS_TRUE) || (strcmp(simMode,SIM_NONE)!=0)) && 
	  ((SDSUpon == EPICS_TRUE) || (strcmp(simMode,SIM_NONE)!=0)) && 
	  ((strlen(DSPVer)>0) || (strcmp(simMode,SIM_NONE)!=0))) {
      strncpy(cameraHealthMsg, NULL_MSG, MAX_STRING_SIZE-1);	  
      pvPut(cameraHealthMsg);
      strncpy(cameraHealth,HEALTH_GOOD, MAX_STRING_SIZE-1);
      pvPut(cameraHealth);

      debug("camera_ss: warning -> good\n");
    } state good

  }
}
