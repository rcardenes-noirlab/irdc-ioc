/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  socket_client_test.cc
 * 
 * GENERAL DESCRIPTION
 *  Main program to test socket client C++ class.
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 1993
 *
 * HISTORY
 *   Sept 2003 PJY    Created
 *
 */
/* Include files */

#ifdef vxWorks
#include <vxWorks.h>
#include <sysLib.h>
#endif
#include <time.h>
#include <string>
#include <string.h>
#include "ipc.h"
#include "utility.h"
#include "exception.h"

/* defines */

const string usage_statement = "Usage: socket_client_test TCP/UDP server port <nmsgs>";

/* typedefs */
typedef enum {UNKNOWN_SOCKET_TYPE, TCP, UDP} Socket_Type;

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */
Socket_Type id_socket_type(char *s)
{
  if (strcmp(s,"TCP") == 0)
    return TCP;
  if (strcmp(s,"UDP") == 0)
    return UDP;
  
  cerr << "Unrecognised protocol: " << s << endl;
  cerr << usage_statement << endl;
  return UNKNOWN_SOCKET_TYPE;
}

/* class function definitions */

/* Main program */
#ifdef vxWorks
int socketClient(char* prot, char* sname, int sp, int n)
#else
int main(int argc, char *argv[])
#endif
{
  try {
  //  TCPSocket_Client* client = NULL;
    Socket * client = 0;
    char protocol[IPC_HOSTLEN];
    char server_name[IPC_HOSTLEN];
    int port;
    int nmsgs;
    int nr;
    int ack;
    char* buf;
    char b;
    Socket_Type socket_type;

#ifdef vxWorks
    strlcpy(protocol, prot, IPC_HOSTLEN);
    strlcpy(server_name, sname, IPC_HOSTLEN);
    port = sp;
    nmsgs = n;
#else
    if (argc<4) {
      cerr << usage_statement << endl;
      return -1;
    } else {
      strlcpy(protocol, argv[1], IPC_HOSTLEN);
      strlcpy(server_name, argv[2], IPC_HOSTLEN);
      port = atoi(argv[3]);
      if (argc > 4)
	nmsgs = atoi(argv[4]);
      else 
	nmsgs = 5;
    }
#endif

    try {
      socket_type = id_socket_type(protocol);
      cout << "Client: starting, server = " << server_name << " port = " << port << endl;

      // Create a client socket to connect to server
      switch (socket_type) {
      case TCP: client = new TCPSocket_Client(server_name, port); break;
      case UDP: client = new UDPSocket_Client(server_name, port); break;
      default: return -2;
     }
      
      cout << "ok: 1" << endl;
      // Simple test message protocol
      // Send a message of varying size and then send ctrl-D
      while (nmsgs>0) {
	buf = new char[nmsgs];
	
	client->send_message(buf, nmsgs, 0, SECOND);
	cout << "Client: sent " << nmsgs << " bytes to socket" << endl;
	nr = client->get_message((void*)&ack, sizeof(ack), 0, SECOND);
	cout << "Client: ack=" << ack << endl;
	delete [] buf;
	nmsgs--;
      }

      // Now send ctrl-D
      b = 4;
      client->send_message((char*)&b, 1, 0, SECOND);
      cout << "Client: sent ctrl-D" << endl;
      nr = client->get_message((void*)&ack, sizeof(ack), 0, SECOND);
      cout << "Client: ack=" << ack << endl;
      
      delete client;

      cout << "Client: exiting" << endl;
    }

    catch (IPC_Error& e) {
      cout << "Client Error: " << endl;
      e.print_error(__FILE__, __LINE__);
      if (client != NULL)
	delete client;     
    }
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return 0;
}

