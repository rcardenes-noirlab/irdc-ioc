[schematic2]
uniq 145
[tools]
[detail]
w 1018 43 100 0 n#141 egenSub.combHealth2.FLNK 992 32 1104 32 1104 608 1152 608 esirs.health.SLNK
w 1002 683 100 0 n#140 egenSub.combHealth2.OUTB 992 672 1072 672 1072 736 1152 736 esirs.health.IMSS
w 1042 779 100 0 n#139 egenSub.combHealth2.VALA 992 768 1152 768 esirs.health.INP
w 626 555 100 0 n#138 hwin.hwin#136.in 608 544 704 544 egenSub.combHealth2.INPD
w 626 619 100 0 n#137 hwin.hwin#135.in 608 608 704 608 egenSub.combHealth2.INPC
w 234 715 100 0 n#134 egenSub.combHealth.VALB 80 704 448 704 448 672 704 672 egenSub.combHealth2.INPB
w 234 779 100 0 n#133 egenSub.combHealth.VALA 80 768 448 768 448 736 704 736 egenSub.combHealth2.INPA
w -310 171 100 0 n#110 hwin.hwin#109.in -352 160 -208 160 egenSub.combHealth.INPJ
w -310 235 100 0 n#107 hwin.hwin#108.in -352 224 -208 224 egenSub.combHealth.INPI
w -310 299 100 0 n#106 hwin.hwin#105.in -352 288 -208 288 egenSub.combHealth.INPH
w -310 363 100 0 n#103 hwin.hwin#104.in -352 352 -208 352 egenSub.combHealth.INPG
w -310 427 100 0 n#102 hwin.hwin#101.in -352 416 -208 416 egenSub.combHealth.INPF
w -304 491 100 0 n#99 hwin.hwin#100.in -352 480 -208 480 egenSub.combHealth.INPE
w -304 555 100 0 n#98 hwin.hwin#97.in -352 544 -208 544 egenSub.combHealth.INPD
w -304 619 100 0 n#95 hwin.hwin#96.in -352 608 -208 608 egenSub.combHealth.INPC
w -304 683 100 0 n#94 hwin.hwin#93.in -352 672 -208 672 egenSub.combHealth.INPB
w -304 747 100 0 n#92 hwin.hwin#91.in -352 736 -208 736 egenSub.combHealth.INPA
w 250 43 100 0 n#132 egenSub.combHealth.FLNK 80 32 480 32 480 64 704 64 egenSub.combHealth2.SLNK
s 928 -1008 500 512 GNIRS DC Health Status/Alarm Database
[cell use]
use esirs -1296 -649 100 0 controlHealth
xform 0 -1088 -496
p -1200 -672 100 0 1 DESC:Control Health
p -1360 -848 100 0 0 EGU:0/1
p -1345 -768 100 0 0 FDSC:Control Health
p -1200 -704 100 0 1 FTVL:STRING
p -1200 -768 100 0 1 PV:$(sadtop)
p -1200 -736 100 0 1 SNAM:
p -1200 -800 100 0 1 def(FLNK):$(sadtop)combHealth2.INPC
use esirs -1296 -121 100 0 slaveTaskHealth
xform 0 -1088 32
p -1200 -144 100 0 1 DESC:Slave Task Health
p -1360 -320 100 0 0 EGU:0/1
p -1345 -240 100 0 0 FDSC:Slave Task Health
p -1200 -176 100 0 1 FTVL:STRING
p -1200 -240 100 0 1 PV:$(sadtop)
p -1200 -208 100 0 1 SNAM:
p -1200 -272 100 0 1 def(FLNK):$(sadtop)combHealth.INPC
use esirs -1296 359 100 0 dataTaskHealth
xform 0 -1088 512
p -1200 336 100 0 1 DESC:Data Task Health
p -1360 160 100 0 0 EGU:0/1
p -1345 240 100 0 0 FDSC:Data Task Health
p -1200 304 100 0 1 FTVL:STRING
p -1200 240 100 0 1 PV:$(sadtop)
p -1200 272 100 0 1 SNAM:
p -1200 208 100 0 1 def(FLNK):$(sadtop)combHealth.INPA
use esirs 1152 519 100 0 health
xform 0 1360 672
p 1248 496 100 0 1 DESC:Detector Controller health
p 1103 96 100 0 0 EGU: 
p 1103 400 100 0 0 FDSC:Detector Controller health
p 1248 464 100 0 1 FTVL:STRING
p 1248 400 100 0 1 PV:$(sadtop)
p 1248 432 100 0 1 SNAM:
use esirs -704 -649 100 0 cameraHealth
xform 0 -496 -496
p -608 -672 100 0 1 DESC:Camera Health
p -768 -848 100 0 0 EGU:0/1
p -753 -768 100 0 0 FDSC:Camera Health
p -608 -704 100 0 1 FTVL:STRING
p -608 -768 100 0 1 PV:$(sadtop)
p -608 -736 100 0 1 SNAM:
p -608 -800 100 0 1 def(FLNK):$(sadtop)combHealth.INPI
use esirs -112 -649 100 0 dhsHealth
xform 0 96 -496
p -16 -672 100 0 1 DESC:DHS Health
p -176 -848 100 0 0 EGU:0/1
p -161 -768 100 0 0 FDSC:DHS Health
p -16 -704 100 0 1 FTVL:STRING
p -16 -768 100 0 1 PV:$(sadtop)
p -16 -736 100 0 1 SNAM:
p -16 -800 100 0 1 def(FLNK):$(sadtop)combHealth.INPG
use esirs 496 -649 100 0 fitsHealth
xform 0 704 -496
p 592 -672 100 0 1 DESC:Fits Health
p 432 -848 100 0 0 EGU:0/1
p 447 -768 100 0 0 FDSC:Fits Health
p 592 -704 100 0 1 FTVL:STRING
p 592 -768 100 0 1 PV:$(sadtop)
p 592 -736 100 0 1 SNAM:
p 592 -800 100 0 1 def(FLNK):$(sadtop)combHealth.INPE
use hwin -544 695 100 0 hwin#91
xform 0 -448 736
p -720 720 100 0 -1 val(in):$(sadtop)dataTaskHealth.VAL
use hwin -544 631 100 0 hwin#93
xform 0 -448 672
p -720 656 100 0 -1 val(in):$(sadtop)dataTaskHealth.OMSS
use hwin -544 567 100 0 hwin#96
xform 0 -448 608
p -720 592 100 0 -1 val(in):$(sadtop)slaveTaskHealth.VAL
use hwin -544 503 100 0 hwin#97
xform 0 -448 544
p -720 528 100 0 -1 val(in):$(sadtop)slaveTaskHealth.OMSS
use hwin -544 439 100 0 hwin#100
xform 0 -448 480
p -720 464 100 0 -1 val(in):$(sadtop)fitsHealth.VAL
use hwin -544 375 100 0 hwin#101
xform 0 -448 416
p -720 400 100 0 -1 val(in):$(sadtop)fitsHealth.OMSS
use hwin -544 311 100 0 hwin#104
xform 0 -448 352
p -720 336 100 0 -1 val(in):$(sadtop)dhsHealth.VAL
use hwin -544 247 100 0 hwin#105
xform 0 -448 288
p -720 272 100 0 -1 val(in):$(sadtop)dhsHealth.OMSS
use hwin -544 183 100 0 hwin#108
xform 0 -448 224
p -720 208 100 0 -1 val(in):$(sadtop)cameraHealth.VAL
use hwin -544 119 100 0 hwin#109
xform 0 -448 160
p -720 144 100 0 -1 val(in):$(sadtop)cameraHealth.OMSS
use hwin 416 567 100 0 hwin#135
xform 0 512 608
p 240 592 100 0 -1 val(in):$(sadtop)controlHealth.VAL
use hwin 416 503 100 0 hwin#136
xform 0 512 544
p 240 528 100 0 -1 val(in):$(sadtop)controlHealth.OMSS
use egenSub -128 816 100 0 combHealth
xform 0 -64 400
p -365 715 100 0 0 DESC:Combines Health inputs
p -128 768 100 0 1 FTA:STRING
p -128 736 100 0 1 FTB:STRING
p -128 704 100 0 1 FTC:STRING
p -128 672 100 0 1 FTD:STRING
p -128 640 100 0 1 FTE:STRING
p -128 608 100 0 1 FTF:STRING
p -128 576 100 0 1 FTG:STRING
p -128 544 100 0 1 FTH:STRING
p -128 512 100 0 1 FTI:STRING
p -128 480 100 0 1 FTJ:STRING
p -128 304 100 0 1 FTVA:STRING
p -128 272 100 0 1 FTVB:STRING
p -144 -80 100 0 1 PV:$(sadtop)
p -144 -48 100 0 1 SNAM:dcHealthCombine
use egenSub 784 816 100 0 combHealth2
xform 0 848 400
p 547 715 100 0 0 DESC:Combines Health inputs
p 784 768 100 0 1 FTA:STRING
p 784 736 100 0 1 FTB:STRING
p 784 704 100 0 1 FTC:STRING
p 784 672 100 0 1 FTD:STRING
p 784 640 100 0 0 FTE:STRING
p 784 608 100 0 0 FTF:STRING
p 784 576 100 0 0 FTG:STRING
p 784 544 100 0 0 FTH:STRING
p 784 512 100 0 0 FTI:STRING
p 784 480 100 0 0 FTJ:STRING
p 784 304 100 0 1 FTVA:STRING
p 784 272 100 0 1 FTVB:STRING
p 768 -80 100 0 1 PV:$(sadtop)
p 768 -48 100 0 1 SNAM:dcHealthCombine
use estringins -192 1095 100 0 statusMsg3
xform 0 -64 1168
p -160 1040 100 0 1 PV:$(sadtop)
use estringins -544 1095 100 0 statusMsg2
xform 0 -416 1168
p -512 1040 100 0 1 PV:$(sadtop)
use estringins -896 1095 100 0 statusMsg1
xform 0 -768 1168
p -864 1040 100 0 1 PV:$(sadtop)
use bc200tr -1504 -1192 -100 0 frame
xform 0 176 112
p 1072 -1024 100 0 1 author:P.Young
p 1296 -1040 100 0 -1 border:C
p 1072 -1056 100 0 1 checked:
p 1328 -1040 100 0 -1 date:2001-02-21
p 1312 -912 100 0 -1 project:Gemini GNIRS DC
p -1504 -1144 100 0 0 revision:1.0
p 1312 -976 150 0 -1 title:dcSysSad.sch
[comments]
