[schematic2]
uniq 43
[tools]
[detail]
s 928 -1008 500 512 GNIRS DC Status/Alarm Database
n 1248 -832 1728 -480 100
This schematic contains the 
systemwideStatus/Alarm Database for 
the GNIRS DC.It includes the following 
schematics:
dcSysSad  - System SAD records
dcSDSUSad  - Detector SAD records
dcSeqSad  - Exposure SAD records
dcDataSad  - Data Handling SAD records
_
[cell use]
use dcDataSad -232 -537 100 0 dcDataSad#42
xform 0 -120 -432
use dcSeqSad -200 -185 100 0 dcSeqSad#41
xform 0 -112 -80
use dcSDSUSad -200 199 100 0 dcSDSUSad#40
xform 0 -112 304
use dcSysSad -200 599 100 0 dcSysSad#39
xform 0 -112 704
use bc200tr -1504 -1192 -100 0 frame
xform 0 176 112
p 1072 -1024 100 0 1 author:R. Cardenes
p 1296 -1040 100 0 -1 border:C
p 1072 -896 40 0 -1 comment1:2020-05-01 Modified for GNIRS. Ricardo Cardenes
p 1072 -1056 100 0 1 checked:
p 1328 -1040 100 0 -1 date:2020-05-01
p 1312 -912 100 0 -1 project:Gemini GNIRS DC
p -1504 -1144 100 0 0 revision:1.0
p 1312 -976 150 0 -1 title:dcSad.sch
[comments]
