/*
 * Copyright (c) 1994 - 2002 by MSSSO Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME sdsu_controller.h
 *
 * PURPOSE
 * SDSU hardware data structures, and class definitions
 * This is a C++ class definition that conforms to the Cicada
 * metod of building a new controller class derived from the Generic
 * Controller class. It provides base support for SDSU controllers.
 * For specific SDSU Gen I and II controllers sub-classes are
 * derived from here.
 *
 */
#ifndef _SDSU3_5_CONTROLLER_H
#define _SDSU3_5_CONTROLLER_H

/*---------------------------------------------------------------------------
 Include files
 --------------------------------------------------------------------------*/
#include <string>
#include <utility>
#include <sys/types.h>
#include "astropci_io.h"

#include "controller.h"
#include "sdsu_utils.h"
#include "CArcDevice.h"

#include "epicsMutex.h"

using ModeIdx = std::pair<IR_Readmode_Type, IR_Quadrants_Type>;

/*---------------------------------------------------------------------------
 Constants
 --------------------------------------------------------------------------*/

const int UNDEFINED = -123456;          //+ Default SDSU command data calue

//+ DSP interface constants
const int SDSU_DOWNLOAD_ADDR_MAX = 0x4000;//+ maximum size of DSP program memory

const int DEFAULT_TIMEOUT = 1;          //+ default timeout in secods for getting reply from controller

//+ Error codes
const int SDSU_ERROR = -1;
const int SDSU_ABORTED = -2;
const int SDSU_OK = -3;
const int SDSU_WAITING = -4;
const int SDSU_NO_EXPECTED_REPLY = -5;
const int SDSU_END = -123456;
const int OPEN_INTERFACE_ERROR = 1;
const int CLOSE_INTERFACE_ERROR = 3;
const int SDSU_BAD_REPLY_HEADER = 4;
const int SDSU_BAD_REPLY = 5;
const int SDSU_REPLY_ERROR = 6;
const int SDSU_DOWNLOAD_OPEN_ERROR = 8;
const int SDSU_DOWNLOAD_TYPE_ERROR = 9;
const int SDSU_DOWNLOAD_FILE_ERROR = 10 ;

const int SDSU_ACTUAL_VALUE = -1;

//+ Debugging mask levels
const int SDSU_SHOW_NOTHING = 0;
const int SDSU_SHOW_ERRORS = 256;
const int SDSU_SHOW_COMMANDS = 512;
const int SDSU_SHOW_REPLYS = 1024;
const int SDSU_SHOW_DOWNLOAD = 2048;
const int SDSU_DO_NOTHING = 4096;

const int MAX_SDSU_BAD_REPLY_HEADERS =  10;//+ maximum bad reply headers to report

//+ Device numbers
const int SDSU_DEVICE_ZERO = 1;
const int SDSU_DEVICE_ONE = 2;
const int SDSU_ALIGN = 0x400;           //+ 1024 byte alignment for SDSU DMA buffers

//+ Command Reply buffer constants
const int SDSU_BUFFER_SIZE = 64;
const int SDSU_REPLY_SIZE = 8;

//+ DSP program constants
const int DSP_PROG_LINE_LEN = 80;

//+ SBUS interface constants
const int SBUS_DMA_FIFO_RESET = 0x90000180;

//+ Different memory bank masks
const int P_MASK = 0x00100000;
const int X_MASK = 0x00200000;
const int Y_MASK = 0x00400000;
const int R_MASK = 0x00800000;

//+ Bits to consider in the reply word - ie 24 bits only
const int REPLY_MASK = 0x00FFFFFF;

//+ Bits containing the SDSU board (source and destination)
const int SDSU_SRC_MASK  = 0xFF0000;
const int SDSU_DEST_MASK = 0x00FF00;

//+ Delay (msec) before readout starts
const int SDSU_READOUT_DELAY = 3000;    //+ Wait this long before expecting to see any data

//+ Data transfer mode between electronics rack and interface card
const int SDSU_COMMAND_MODE = 0;
const int SDSU_IMAGE_MODE = 1;
const int SDSU_RESET_MODE = 7;

//+ Heater adu/voltage conversion factors
const float SDSU_MAX_HEATER_VOLTS = 10.0;
const float SDSU_MAX_HEATER_ADUS = 4096.0;

//+ DSP parameter numbers
//+ X memory
const int SDSU_0_PAR = 0x0;
const int SDSU_X_OPTIONS_WORD_PAR = 0x1;

//+ Options word bit positions
const int SDSU_AUTOSHUTTER_BIT = 0x1;
const int SDSU_AUTOREAD_BIT = 0x2;
const int SDSU_AUTOCLEAR_BIT = 0x4;

//+ Y memory - timing board
const int SDSU_NSERIAL_PAR = 0x1;
const int SDSU_NPARALLEL_PAR = 0x2;
const int SDSU_NSERIAL_CLEAR_PAR = 0x3;
const int SDSU_NPARALLEL_CLEAR_PAR = 0x4;
const int SDSU_RCF_PAR = 0x5;
const int SDSU_CCF_PAR = 0x6;
const int SDSU_NAMPS_PAR = 0x7;
const int SDSU_AMP_MASK_PAR = 0x8;

//+ Y memory - utility board
const int SDSU_HEATER_PAR = 0x2;
const int SDSU_EXPOSED_PAR = 0x17;
const int SDSU_EXPSET_PAR = 0x18;
const int SDSU_TEMPSET_PAR = 0x1C;
const int SDSU_TEMP_WORD_PAR = 0x38;
const int SDSU_TEMP_CONTROL_PAR = 0x39;
const int SDSU_SW_VERSION_PAR = 0x10;

//+ Temp word bit positions
const int SDSU_TUPROPT_BIT = 0x1;
const int SDSU_TC_BIT = 0x2;

//+ Maximum command size
const int SDSU_CMD_BUF_SIZE = 32;

//+ Maximum number of reply words in response to a command
const int SDSU_MAX_REPLY_ARGS = 32;

//+ Maximum number of replys stacked up
const int SDSU_MAX_REPLY_MSGS = 10;

//+ minimum time to wait before checking exposure timer progress
const int SDSU_MIN_WAIT = 500;

// From SDSU2
//+ Y memory - timing board
const int SDSU_Y_OPTIONS_WORD_PAR = 0x0;
const int SDSU_AB_SETTING_PAR = 0x8;
const int SDSU_TEST_PATTERN_OS_PAR = 0xB;
const int SDSU_TEST_PATTERN_INC_PAR = 0xC;
const int SDSU_TEST_PATTERN_SEED_PAR = 0xD;
const int SDSU_TEST_PATTERN_AMP_INC_PAR = 0xE;
const int SDSU_TEST_PATTERN_READ_INC_PAR = 0xF;
const int SDSU2_RGNS_START_PAR = 0x3F00;

//+ Anti-blooming bit
const int SDSU_AB_BIT = 0x0;

// Test pattern bits
const int SDSU_TEST_PATTERN_BIT = 0x2;
const int SDSU_TEST_PATTERN_CONST_BIT = 0x4;
const int SDSU_TEST_PATTERN_SOURCE_BIT = 0x8;

// From SDSU3
//+ DSP parameter numbers
//+ Y memory options word
const int SDSU3_TEST_PATTERN_BIT = 0x2;
const int SDSU3_TEST_PATTERN_CONST_BIT = 0x4;
//const int SDSU_TEST_PATTERN_SOURCE_BIT = 0x8;
const int SDSU3_AUTOSHUTTER_BIT = 0x20;
const int SDSU3_AUTOREAD_BIT = 0x40;
const int SDSU3_AUTOCLEAR_BIT = 0x10;
const int SDSU3_PON_BIT = 0x400000;


//+ For the SBUS interface card the followint CSR bits are interpreted so:
/* LSI DMAC+ (L64853A) control/status register (csr) bits
   (in all cases: true = 1, false = 0)

0:	   ro	interrupt pending
1:	   ro	error pending
2:      ro	draining
3:      ro	draining
4:	   rw	interrupt enable
5:      wo	flush buffer
6:      rw   slave error
7:      rw   reset dma
8:      rw   memory read/write
9:      rw   enable dma
10--12:  ro	unused == 0
13:	   rw	enable counter
14:	   ro	terminal count
15:	   rw	reserved (always write 0 to this bit)
16--19:  ro   unused == 0
20:      rw	address latch enable/address strobe
21:      ro	lance error
22:      rw	faster
23:	   rw	tc interrupt disable
24:	   rw	enable next
25:	   ro	dma on
26:	   ro	address loaded
27:	   ro	next address loaded
28--31:  ro   device id == 1001

(ro = read only, rw = read or write, wo = write only)
*/

//+ SBUS CSR constants
const unsigned long DMAC_INT_PEND  = 0x00000001;
const unsigned long DMAC_ERR_PEND  = 0x00000002;
const unsigned long DMAC_DRAINING  = 0x0000000c;
const unsigned long DMAC_INT_EN    = 0x00000010;
const unsigned long DMAC_FLUSH     = 0x00000020;
const unsigned long DMAC_SLAVE_ERR = 0x00000040;
const unsigned long DMAC_RESET     = 0x00000080;
const unsigned long DMAC_WRITE     = 0x00000100;
const unsigned long DMAC_EN_DMA    = 0x00000200;
const unsigned long DMAC_EN_CNT    = 0x00002000;
const unsigned long DMAC_TC        = 0x00004000;
const unsigned long DMAC_ALE_AS    = 0x00100000;
const unsigned long DMAC_FASTER    = 0x00400000;
const unsigned long DMAC_TCI_DIS   = 0x00800000;
const unsigned long DMAC_EN_NEXT   = 0x01000000;
const unsigned long DMAC_DMA_ON    = 0x02000000;
const unsigned long DMAC_A_LOADED  = 0x04000000;
const unsigned long DMAC_NA_LOADED = 0x08000000;

/******************************************************************************
 *  Masks to set the PCI Host Control Register HCTR.
 *
 *       Only three bits of this register are used. Two are control bits to set
 *  the mode of the PCI board (bits 8 and 9)  and  the  other (bit 3) is a flag
 *  indicating the progress of image data transfer to the user's application.
 *
 *       Bit 3   = 1     Image buffer busy transferring to user space.
 *               = 0     Image buffer not  transferring to user space.
 *
 *       Bit 8= 0 & Bit 9= 1   PCI board set to slave mode for PCI file download.
 *       Bit 8= 0 & Bit 9= 0   PCI board set to normal processing.
 *
 *       Note that the HTF_MASK, sets the HTF bits 8 and 9 to transfer mode.
 *
 ******************************************************************************/
const int SDSU_PCI_HTF_MASK = 0x200;
const int SDSU_PCI_HTF_CLEAR_MASK = 0xFFFFFCFF;
const int SDSU_PCI_BIT3_CLEAR_MASK = 0xFFFFFFF7;
const int SDSU_PCI_BIT3_SET_MASK = 0x00000008;
const int SDSU_PCI_HTF_BITS = 0x00000038;

//+ Magic code to let PCI interface that a download is coming
const int SDSU_PCI_DOWNLOAD = 0x555AAA;

//+ FITS info - used for interpreting image data
const int DEFAULT_SDSU_BITPIX = 16;
const double DEFAULT_SDSU_BSCALE = 1.0;
const double DEFAULT_SDSU_BZERO = 32768.0;

//+ Message display constants
const int DISPLAY_MIN_MESGS = 1;
const int DISPLAY_ALL_MESGS = 2;

//+ Parameter database name constants
static const char HEATER_VOLTAGE[] = "heater_voltage";
static const char SDSU_ACTIVITY[] = "activity";
static const char REPLY_STATUS[] = "reply_status";
static const char SDSU_COMMAND[] = "sdsu_command";
static const char SDSU_COMMAND_DONE[] = "sdsu_command_done";
static const char LAST_REPLY[] = "last_reply";
static const char LAST_HEADER[] = "last_header";
static const char EXPECTED_REPLY[] = "expected_reply";
static const char EXPECTED_HEADER[] = "expected_header";
static const char CONTROLLER_SWV[] = "controller_swv";
static const char CONTROLLER_PON[] = "controller_pon";
static const char STATUS_MESSAGE[] = "status_message";
static const char REPLY_STATUS_DB[] = "replyStatus";
static const char SDSU_COMMAND_DB[] = "SDSUcommand";
static const char SDSU_COMMAND_DONE_DB[] = "SDSUcmdDone";
static const char LAST_REPLY_DB[] = "lastReply";
static const char LAST_HEADER_DB[] = "lastHeader";
static const char EXPECTED_REPLY_DB[] = "exptReply";
static const char EXPECTED_HEADER_DB[] = "exptHeader";
static const char CONTROLLER_SWV_DB[] = "DSPVer";
static const char CONTROLLER_PON_DB[] = "SDSUpon";
static const char STATUS_MESSAGE_DB[] = "statusMsg";

/*--------------------------------------------------------------------------
  Typedefs
  -------------------------------------------------------------------------*/


//+ SDSU voltage setup structure
typedef struct {
	Nametype name;
	int low;                      //+ used to tell whether low or high when setting
	int set_order;                //+ order in which this voltage is applied
	int set_delay;                //+ delay before apply
	int set_this_voltage;         //+ true if this voltage is to be set
	Sdsu_Voltage_Type type;       //+ Type - either BIAS or CLOCK
	Sdsu_Dac_Board_Type board_type;//+ Type of board - either VIDEO or CLOCK
	int board_addr;               //+ board address
	int dac_addr;                 //+ DAC address
	int dsp_addr;                 //+ DSP memory address
	int dacval;                   //+ DAC value
	double value;                 //+ voltage value to set
} Sdsu_Volt;

/*--------------------------------------------------------------------------
  Class declarations
  -------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
  CLASS
  Sdsu3_5_Controller

  DESCRIPTION
  SDSU base hardware class definition

  KEYWORDS
  Sdsu3_5_Controller, Controller

  -------------------------------------------------------------------------*/
class Sdsu3_5_Controller: public Controller {
public:
	static const std::string PARAMETER_NAME;

	//+ Basic constructor - used for setting up configuration.
	// > cam   - Camera_Desc - description of the camera
	// > camid - camera id
	// > c     - controller id
	Sdsu3_5_Controller(Camera_Desc& cam, int camid, int c, Platform_Specific* ps);

	//+ Midlevel constructor - used for accessing parameter information.
	// > cam   - Camera_Desc - description of the camera
	// > camid - camera id
	// > c 	  - controller id
	// > param_host - host on which the parameter database resides
	// > st	- status memory for this instrument
	// > pm	- parameter map
	Sdsu3_5_Controller(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st,
			Platform_Specific* ps);

	//+ Full constructor, used on the instrument machine
	// > st  - status memory for this instrument
	// > ip  - instrument status parameter object
	// > cp  - Camera parameter object
	// > op  - operations parameter object
	// > deb - debug level
	Sdsu3_5_Controller(Instrument_Status *st, Parameter* ip, Parameter* cp, Parameter* op, int deb, Platform_Specific* ps);

	//+ Destructor
	virtual ~Sdsu3_5_Controller();

	// GROUP: public member functions

	//+ Return configuration data
	// > read_configuration -
	virtual Controller_Config* getconfig(int read_configuration);


	//+ Interrupt handle for the RESET signal
	virtual void handle_reset();

	//+ Do hardware initialisation
	// > init_rec - initialisation record.
	virtual void init(Init init_rec);

	//+ Do a controller reset
	virtual void controller_reset();

	//+ Do a controller test
	virtual void controller_test(int ntests=10);

	//+ Do a controller shutdown
	virtual void controller_shutdown();

	//+ Resets detector hardware (hardware flush).
	// > reset - reset parameters.
	// > exp - reset in an exposure sequence.
	virtual void reset_detector(Detector_Reset reset, bool exp = false);

	//+ Prepare for an Exposure
	// > ns - number of controllers to sync
	virtual void prepare_exposure(int ns = 1);

	//+ Sets simulation mode
	// > sim - simulation level, 0=none
	virtual void change_simulation_mode(int sim);


	//+ Sets the exposure time
	// > msec - exposure time in msec to set
	virtual void set_exposure_time(int msec);

	//+ Gets the exposure time counter value - msec
	virtual int get_exposure_time();

	//+ Sends the "start exposure" command
	virtual epicsTime start_exposure();

	//+ Sends the "stop exposure" command
	virtual void stop_exposure();

	//+ Gets the exposure time lapsed - sec
	virtual double exposure_lapsed();

	//+ Checks to see if controller is in idle mode
	virtual bool check_idle();

	//+ Set or clear the controllers idle state
	// > enable - set or clear idle flag
	virtual void do_idle(bool enable);

	//+ Set controller voltages
	virtual void do_voltages();

	//+ query the controller for the full readout status
	// > rowbytes - used to calculate the number of rows read
	virtual void get_readout_status(int rowbytes);

	//+ finalise readout operation
	virtual void finalise_readout();

	//+ Write current configuration values to a new configuration file.
	// > fname - configuration file name.
	// > this_config - current configuration values.
	virtual void write_configuration(const char* fname, const Sdsu_Config_Data this_config);

	//+ Method to send a direct DSP command
	virtual int send_direct_sdsu_command(Sdsu_Dsp direct_dsp);

	//+ Method to send a special DSP command
	virtual void send_special_sdsu_command(Sdsu_Special_Dsp special_dsp);

	//+ Set the device driver debug mode
	// > deb - debug mode
	virtual void set_debug_mode(int deb);

	//+ Get controller information
	virtual void get_controller_info();

	//+ Dump the common buffer here
	virtual size_t dump_to(FILE *dest);

	virtual void update_total_readout_bytes();
	virtual void update_readout_running();
	virtual void update_pixel_count();
	ModeIdx get_current_mode_index() const;

	// GROUP: SDSU_Controller public data members variables.
	Sdsu_Readout sdsu_readout;               //+ Preserved readout settings
	Sdsu_Status *sdsu_status;                //+ Specific Astromed 3200 status
	Parameter *sdsu_statusp;                 //+ Cicada parameter status database for dummy
	Sdsu_Config *sdsu_configp;               //+ Pointer to SDSU configuration in shared memory
	Sdsu_Config sdsu_config;                 //+ Currently used settings and configuration
	Hostnametype client_host;                //+ Name of host starting camera
	Filenametype sdsu_config_file;           //+ Name of config file
	int skip_bytes;                          //+ Number of bytes to skip at start of readout
	unsigned long numbytes;                  //+ Number of bytes in readout
	uint64_t total_readout_bytes;            //+ total number of bytes to readout in each set
	uint64_t bytes_remaining;                //+ Total number of bytes still to read
	Astro_Readout_Status readout_status;     //+ Astro driver readout status
//	Astro_Setup astro_setup;          //+ Astro PCI driver setup info
	Astropci_State astro_state;              //+ Astro PCI driver information
	int auto_rdc;                            //+ TRUE if RDC is automatic from utility card
	bool idle_turned_off;                    //+ Set if not idling

protected:
	// GROUP: Protected member functions
	//+ SDSU interface methods

	//+ Open and check communications with the controller interface
	virtual void open_and_check_interface();

	//+ Open the controller interface
	virtual int open_interface();
	virtual int open_interface(bool force);

	//+ Close the controller interface
	virtual int close_interface();

	//+ Open the controller interface and get driver setup after a reset
	virtual void prepare_interface(bool do_reset);

	//+ Ready teh controller interface for receiving an image
	virtual void ready_interface();

	//+ Send a command to the SDSU DSP to get the DSP code version currently loaded
	// > board_id - board to send command to
	virtual char* get_dsp_version(int board_id);

	//+ Send a command to the SDSU interface card
	virtual int send_interface_command();

	//+ Read a reply from the SDSU interface card
	// > nbytes - max bytes to read
	// > timeout - timeout in seconds
	virtual int read_interface_reply(int nbytes, int timeout);

	//+ Execute an SDSU interface DSP command
	// > command - actual command to run
	virtual int sdsu_command(Sdsu_Cmd command);

	//+ Send a DSP command to a particular board id
	// > board_id - SDSU board identifier
	// > command - actual command to run
	virtual int sdsu_command(int board_id, Sdsu_Cmd command);

	//+ Send a DSP command to a particular board id, command has one data argument
	// > board_id - SDSU board identifier
	// > command - actual command to run
	// > data - argument
	virtual int sdsu_command(int board_id, Sdsu_Cmd command,int data);

	//+ Send a DSP command to a particular board id, command has two data arguments
	// > board_id - SDSU board identifier
	// > command - actual command to run
	// > data1 - argument 1
	// > data2 - argument 2
	virtual int sdsu_command(int board_id, Sdsu_Cmd command, int data1, int data2);

	//+ Send a DSP memory command to a particular board id
	// > board_id - SDSU board identifier
	// > command - actual command to run
	// > mem_space - memory bank to address
	// > address - actual address in memory bank
	virtual int sdsu_command(int board_id, Sdsu_Cmd command, int data, Sdsu_Memory_Space mem_space, int address);

	//+ Send a DSP voltage command to a particular board id
	// > board_id - SDSU board identifier
	// > command - actual command to run
	// > voltage_cmd - voltage command args
	virtual int sdsu_command(int board_id, Sdsu_Cmd command, Sdsu_Voltage_Cmd voltage_cmd);

	//+ Send a DSP MUX command to a particular board id
	// > board_id - SDSU board identifier
	// > command - actual command to run
	// > mux_cmd - MUX command args
	virtual int sdsu_command(int board_id, Sdsu_Cmd command, Sdsu_Mux_Cmd mux_cmd);

	//+ Ready the reply memory
	// > size - size to clear
	virtual void ready_reply_mem(int size);

	//+ returns a string that describes the dsp request
	virtual char* request_type_str();

	//+ Interpret, format and send command to interface
	// > arg1 - first argument in variable list
	virtual void send_command(int arg1, ...);

	//+ reset interface card
	virtual int send_interface_reset();

	//+ reset controller
	virtual int send_controller_reset();

	//+ Sets the timeout multiplier - multiplied by 1 sec to set full timeout
	// > timeout_multiplier - variable multiplier
	virtual void send_timeout(int timeout_multiplier);

	//+ Sends the PCI ASTRO driver setup
	virtual void send_pci_setup();

	//+ Send a command to the PCI DSP via the vector register
	// > command - actual command to run
	// > expected_reply - reply expected from this command
	// > data1 - argument 1
	// > ... - more data arguments
	virtual int send_pci_hcvr(Sdsu_Cmd command, int expected_reply, int data1, ...);

	//+ Send a direct command to the PCI DSP via the vector register.
	// > command - actual command to run
	// > arg - single argument to pass
	virtual void send_pci_cmd(int cmd, int arg);


	//+ Get a Source/Destination/Nargs style reply from the SDSU interface
	// > board_id - board from which reply should come
	// > expected_reply - the reply expected
	// > timeout - wait for timeout msecs
	// > added_reply - sometimes an extra reply is expected
	virtual int get_sdn_reply(int board_id, int expected_reply, int timeout, int added_reply=0);
	virtual int evaluate_sdn_reply(int actual_reply, int expected_reply, int added_reply=0);


	//+ Check the reply returned from the interface board against the specified expected reply value.
	// > reply - received reply
	// > expected_reply - expeted reply from previous command
	virtual int check_expected_reply(int reply, int expected_reply);

	//+ Check to see if a reply has been received
	// > board_id - board from which reply should come
	// > expected_reply - the reply expected
	virtual int sdsu_poll(int board_id, int expected_reply);

	//+ Read download files into configuration buffers
	// > board - SDSU board identifier
	// > filename - file to get DSP code from
	virtual int read_sdsu_download(int board, std::string& filename);

	//+ Download program into controller
	// > board - SDSU board identifier
	// > dsp_prog - program to download
	virtual int sdsu_download(int board, std::string& filename);

	//+ Moves a line of DSP code from file to buffer
	// > fp - reads a DSP program line from this file
	// > buf - and puts it into this buffer
	virtual void sdsu_read_line(FILE *fp, char *buf);

	//+ Determine download file type
	// > fp - file that DSP code lives in
	virtual int sdsu_get_download_type(FILE *fp);

	//+ Process a DSP file into an internal memory representation
	// > download_fp - file pointer
	// > mem_space - type of SDSU memory to download to
	// > addr - address to download program into
	// > sdsu_prog - memory version of program to load in
	virtual void sdsu_process_data(FILE *download_fp, Sdsu_Memory_Space mem_space, int addr, DSP_prog* dsp_prog);

	//+ report an SDSU error by compiling an error string
	// > local_errno - local SDSU erro code
	// > msg - error message
	virtual char* report_errno(int local_errno, char* msg);

	//+ run the long hardware test
	// > value - value to write in test
	// > board_id - board identifier
	// > command - command to use for test
	virtual int execute_long_test(int value, int id, Sdsu_Cmd command);

	//+ Do a hardware test
	// > short_test - set if only short version wanted
	// > disp_msg - set if required to display progress messages
	virtual void do_hardware_test(int short_test, int disp_msg, int ntests=0);

	//+ Resets timing board - must be overridde, ie specific to SDSU gen
	virtual void do_reset();

	//+ Sets controllers test pattern mode
	virtual void do_test_pattern();

	//+ Turns on controller power
	virtual void do_power_on();

	//+ Downloads timing board code
	virtual void do_timing_board();

	//+ Downloads utility board code
	virtual void do_utility_board();

	//+ Downloads PCI board code
	virtual void do_pci_board();

	//+ Set hardware byte swapping from PCI board if available
	virtual int set_pci_hardware_byte_swapping();

	//+ Sets gain
	virtual void do_gain();

	//+ Sets time constant
	virtual void do_tc();

	//+ Download DSP code to boards as required
	virtual void do_DSP_code_setup();

	//+ set board ids for various control functions
	virtual void do_set_boards();

	//+ Runs through the entire SDSU setup sequence
	virtual void do_setup();

	//+ configure ROI memory addresses
	virtual int get_regions_memory_address();

	//+ Read back current voltage settings
	virtual void read_current_voltage_settings();

	//+ translate current command into string name
	// > cmd - current command
	virtual char* current_command_str(int cmd);


	//+ Set up the readout of a detector, if successful return true.
	// > ns - number of controllers to sync.
	// > readout_rec - readout request structure.
	// > trgn - readout regions object.
	// ! isize - total bytes in readout
	//   isize may be modified by controller specific req.
	// > exptime - last exposure time.
	virtual void setup_readout(int ns, Detector_Readout* readout_rec, Detector_Regions* trgn,
			IR_Exposure_Info& ir_einfo, unsigned long& isize, double exptime);

	//+  Create an image readout buffer and returns pointer to buffer
	// > size - size of buffer to create
	virtual char* create_image_buffer(unsigned long size, int align = SDSU_ALIGN);

	//+ Starts the image transfer thread for an IR camera - not used on Astromed
	// > expose - exposure request.
	// > ir_tt - pointer to thread monitoring IR exposure timing
	virtual void start_ir_image_transfer_thread(IR_Expose& expose, Thread *ir_tt);

	//+ Starts image transfer thread
	virtual void start_image_transfer_thread();

	//+ Start reading out the ROI
	// > rr - number of the ROI
	virtual void start_row_region(int rr);

	//+ Wait for the readout to reach the end of the current segment
	// > rows_processed - number of rows expected so far
	// > row - row number of region
	// > rowbytes - number of bytes per row
	// ! rt - current count of total rows transferred in readout
	virtual void wait_segment_readout(int rows_processed, int row, int rowbytes, int& rtfr);

	//+ query the controller for the number of readouts done
	virtual long get_readouts_done();

	//+ query the controller for the current read running
	virtual long get_read_running();

	//+ Stop a readout
	virtual void do_stop_readout();

	//+ finish up the redout properly
	virtual void finish_readout();

	//+ Flush any leftover data from the controller
	virtual void flush_readout();

	// GROUP: Protected member variables
	int util_id;                          //+ ID of card that handles utility cmds
	int shutter_control_board;            //+ ID of board that does shutter control and timing
	int sdsu_errno;                       //+ Last DSP error
	int sdsu_bad_header_count;            //+ Count of bad DSP headers
	int send_rdc;                         //+ Set to true if RDC needed
	unsigned long cmd_buf[SDSU_CMD_BUF_SIZE];//+ DSP command communication buffer
	unsigned long *reply_buf;             //+ DSP reply communication buffer (aligned)
	int last_reply_len;                   //+ number of words in last reply received
	int last_do_readout;                  //+ Flag to store last do_readout
	Detector_Readout readout_req;         //+ readout request
	int sdsu_request;                     //+ current sdsu request
	int default_request;                  //+ default sdsu request for the interface in use
	int sdsu_last_header;                 //+ Last DSP header received
	int sdsu_last_reply;                  //+ Last DSP reply received
	Sdsu_Config_Data sdsu_config_data;    //+ Preserved config settings
	Sdsu_Dsp sdsu_dsp;                    //+ Preserved dsp settings
	Sdsu_Special_Dsp sdsu_special_dsp;    //+ Preserved special dsp command
	char current_command_buf[4];          //+ String representation of current command
	bool awaiting_sdsu_reply;             //+ Flag set when reading a reply from controller
	Sdsu_Init sdsu_init;                  //+ Init request
	bool timing_loaded;                   //+ Set when timing code downloaded to controller
	unsigned long nrows;                  //+ Number of rows for the current configuration
	unsigned long ncols;                  //+ Number of cols for the current configuration
	unsigned long buffer_size;            //+ User buffer size, could be smaller than DMA transfer, so used as ring
	unsigned long image_size;             //+ Size of image to be readout (or multiple images for IR)
	unsigned long period;                 //+ Time from start of first NDR to start of 2nd NDR (msec)
	unsigned long read_interval;          //+ Interval between image sets (msec)
	unsigned long read_time;              //+ Expected frame read time (msec)
	unsigned long nresets;                //+ Number of resets to do
	unsigned long reset_delay;            //+ delay to wait after reset (msec)
	int ncmd_words;                       //+ Number of words in current SDSU command
	int default_timeout;                  //+ Default timeout multiplier
	bool byte_swapping;                   //+ Set if byte swapping done by hardware on PCI systems
	bool update_status_vars;              //+ Set usually to update header and reply status vars, cleared during time-consuming ops

private:
	arc::device::CArcDevice* pDevice;
	epicsMutex deviceLock;
	ModeIdx current_mode;
	//+ Initialise any work variables
	void init_vars();

	//+ Add SDSU status parameters to the parameter DB
	// > init - set if parameters are to be initialised
	void add_status_parameters(bool init);

};

/* local function declarations */
//+ Thread for image DMA
// > arg - pointer to the sdsu_controller object
extern "C" void* sdsu_image_transfer_thread(void *arg);

#ifdef vxWorks
//+ Handler for reply interrupts
// > arg - pointer to the sdsu_controller object
extern "C" void handle_sdsu_reply(int arg);
#endif

#endif //_SDSU_H

