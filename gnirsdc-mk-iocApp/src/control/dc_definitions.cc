/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_definitions.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GNIRS PROJECT
 * 
 * FILENAME nifs_definitions.cc
 * 
 * GENERAL DESCRIPTION 
 *
 * This file is used to proved unique definitions for some objects that
 * have been decleared in the whole GNIRS code tree.
 * Typically a place for defining static member objects needed in other
 * code files.
 *
 * $Log: dc_definitions.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.2  2004/08/20 02:30:34  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.1  2004/03/19 00:47:22  pjy
 * Added
 *
 *
 * 
 */
#include "ipc.h"

//+ Map of named shared mem areas - global to all instances
// TODO: I guess we're not doing this?
// SharedMemMapType Shared_Mem::shared_mem_map; 

