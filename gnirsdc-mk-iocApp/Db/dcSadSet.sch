[schematic2]
uniq 15
[tools]
[detail]
s -720 2144 500 0 Top level GNIRS Detector Controller
s -720 2000 500 0 Status/Alarm Database schematic.
n 1536 416 2016 768 100
This is the top level schematic for the
GNIRS Detector Controller Status/Alarm
Database.
.
It contains no database records, but
defines the following macro variables
and includes the "dcSad" schematic:
.
top    = Top level record name prefix.
.
sadtop = Status/Alarm Database prefix.
.
.
_
[cell use]
use dcSad 96 999 100 0 dcSad#14
xform 0 576 1280
p 192 896 100 0 1 set0:sadtop $(dcsadtop)
p 192 848 100 0 1 set1:pvdir ./DC/data
p 192 800 100 0 1 set2:sadscan 1 second
use bc200tr -1024 -104 -100 0 frame
xform 0 656 1200
p 1552 64 100 0 1 author:R. Cardenes
p 1776 48 100 0 -1 border:C
p 1552 176 40 0 -1 comment1:1997-02-04 CICS version. S.M.Beard
p 1552 156 40 0 -1 comment2:2001-02-13 Modified for GNIRS. Peter Young
p 1552 136 40 0 -1 comment3:2020-05-01 Modified for GNIRS. Ricardo Cardenes
p 1808 48 100 0 -1 date:2020-05-01
p 1792 176 100 0 -1 project:Gemini GNIRS DC
p 1792 112 100 0 -1 title:dcSadTop.sch
[comments]
