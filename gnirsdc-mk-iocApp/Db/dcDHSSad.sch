[schematic2]
uniq 108
[tools]
[detail]
w 944 43 100 0 n#101 esirs.doConnect.FLNK 896 96 928 96 928 32 1008 32 embbis.doConnectO.SLNK
w 928 75 100 0 n#100 esirs.doConnect.VAL 896 64 1008 64 embbis.doConnectO.INP
w 616 587 100 0 n#99 esirs.fitsAvailable.FLNK 576 640 608 640 608 576 672 576 embbis.fitsAvailO.SLNK
w 600 619 100 0 n#98 esirs.fitsAvailable.VAL 576 608 672 608 embbis.fitsAvailO.INP
w -728 -437 100 0 n#97 esirs.dhsAvailable.FLNK -752 -384 -736 -384 -736 -448 -672 -448 embbis.dhsAvailO.SLNK
w -736 -405 100 0 n#96 esirs.dhsAvailable.VAL -752 -416 -672 -416 embbis.dhsAvailO.INP
w -48 75 100 0 n#93 esirs.fitsConnected.FLNK -96 128 -64 128 -64 64 16 64 embbis.fitsConnO.SLNK
w -64 107 100 0 n#92 esirs.fitsConnected.VAL -96 96 16 96 embbis.fitsConnO.INP
w 600 -437 100 0 n#90 esirs.dhsConnected.FLNK 544 -384 592 -384 592 -448 656 -448 embbis.dhsConnO.SLNK
w 576 -405 100 0 n#89 esirs.dhsConnected.VAL 544 -416 656 -416 embbis.dhsConnO.INP
s 1040 -1040 500 512 GNIRS DC DHS Status/Alarm Database
[cell use]
use estringins 1152 999 100 0 dhsProblemMsg
xform 0 1280 1072
p 1206 1238 100 0 0 DESC:Message indicating nature of DHS problem
p 1216 960 100 0 1 PV:$(sadtop)
use estringins 1152 775 100 0 fitsProbMsg
xform 0 1280 848
p 1206 1014 100 0 0 DESC:Message indicating nature of DHS problem
p 1216 736 100 0 1 PV:$(sadtop)
use elongins 768 999 100 0 dhsProblem
xform 0 896 1072
p 557 1273 100 0 0 DESC:Flag indicating DHS problem
p 832 960 100 0 1 PV:$(sadtop)
use elongins 768 775 100 0 fitsProblem
xform 0 896 848
p 557 1049 100 0 0 DESC:Flag indicating DHS problem
p 832 736 100 0 1 PV:$(sadtop)
use embbis 1072 96 100 0 doConnectO
xform 0 1136 32
p 1072 -80 100 0 1 ONST:No
p 1072 -112 100 0 1 PV:$(sadtop)
p 1072 -80 100 0 0 TWST:
p 1072 -48 100 0 1 ZRST:Yes
use embbis 80 128 100 0 fitsConnO
xform 0 144 64
p 80 -48 100 0 1 ONST:No
p 80 -80 100 0 1 PV:$(sadtop)
p 80 -48 100 0 0 TWST:
p 80 -16 100 0 1 ZRST:Yes
use embbis 720 -384 100 0 dhsConnO
xform 0 784 -448
p 720 -560 100 0 1 ONST:No
p 720 -592 100 0 1 PV:$(sadtop)
p 720 -560 100 0 0 TWST:
p 720 -528 100 0 1 ZRST:Yes
use embbis 736 640 100 0 fitsAvailO
xform 0 800 576
p 736 464 100 0 1 ONST:No
p 736 432 100 0 1 PV:$(sadtop)
p 736 464 100 0 0 TWST:
p 736 496 100 0 1 ZRST:Yes
use embbis -608 -384 100 0 dhsAvailO
xform 0 -544 -448
p -608 -560 100 0 1 ONST:No
p -608 -592 100 0 1 PV:$(sadtop)
p -608 -560 100 0 0 TWST:
p -608 -528 100 0 1 ZRST:Yes
use esirs 480 -153 100 0 doConnect
xform 0 688 0
p 576 -176 100 0 1 DESC:Make a connection
p 416 -352 100 0 0 EGU:0/1
p 416 -416 100 0 0 FDSC:Make a connection?
p 576 -208 100 0 1 FTVL:LONG
p 576 -272 100 0 1 PV:$(sadtop)
p 576 -240 100 0 1 SNAM:
use esirs -1136 871 100 0 fitsPort
xform 0 -928 1024
p -1040 848 100 0 1 DESC:FITS server port number
p -1200 672 100 0 0 EGU:
p -1200 608 100 0 0 FDSC:FITS server port number
p -1040 816 100 0 1 FTVL:LONG
p -1040 752 100 0 1 PV:$(sadtop)
p -1040 784 100 0 1 SNAM:
p -1040 704 100 0 0 def(FLNK):
use esirs -512 871 100 0 fitsServer
xform 0 -304 1024
p -416 848 100 0 1 DESC:FITS Server name
p -576 672 100 0 0 EGU:
p -576 608 100 0 0 FDSC:FITS server name
p -416 816 100 0 1 FTVL:STRING
p -416 752 100 0 1 PV:$(sadtop)
p -416 784 100 0 1 SNAM:
p -416 704 100 0 0 def(FLNK):
use esirs -512 -121 100 0 fitsConnected
xform 0 -304 32
p -416 -144 100 0 1 DESC:FITS server connected
p -576 -320 100 0 0 EGU:0/1
p -576 -384 100 0 0 FDSC:Is FITS server connected?
p -416 -176 100 0 1 FTVL:LONG
p -416 -240 100 0 1 PV:$(sadtop)
p -416 -208 100 0 1 SNAM:
use esirs 160 391 100 0 fitsAvailable
xform 0 368 544
p 256 368 100 0 1 DESC:FITS server available
p 96 192 100 0 0 EGU:0/1
p 96 128 100 0 0 FDSC:Is FITS server available?
p 256 336 100 0 1 FTVL:LONG
p 256 272 100 0 1 PV:$(sadtop)
p 256 304 100 0 1 SNAM:
use esirs 128 -633 100 0 dhsConnected
xform 0 336 -480
p 224 -656 100 0 1 DESC:DHS server connected
p 64 -832 100 0 0 EGU:0/1
p 64 -896 100 0 0 FDSC:Is DHS server connected?
p 224 -688 100 0 1 FTVL:LONG
p 224 -752 100 0 1 PV:$(sadtop)
p 224 -720 100 0 1 SNAM:
use esirs -1136 391 100 0 svName
xform 0 -928 544
p -1040 368 100 0 1 DESC:DHS server name
p -1200 128 100 0 0 FDSC:DHS server name
p -1040 336 100 0 1 FTVL:STRING
p -1040 272 100 0 1 PV:$(sadtop)
p -1040 304 100 0 1 SNAM:
use esirs -1168 -121 100 0 svAddr
xform 0 -960 32
p -1072 -144 100 0 1 DESC:DHS server IP address
p -1232 -384 100 0 0 FDSC:DHS server IP address
p -1072 -176 100 0 1 FTVL:STRING
p -1072 -240 100 0 1 PV:$(sadtop)
p -1072 -208 100 0 1 SNAM:
use esirs -1168 -633 100 0 dhsAvailable
xform 0 -960 -480
p -1072 -656 100 0 1 DESC:DHS available
p -1232 -832 100 0 0 EGU:0/1
p -1232 -896 100 0 0 FDSC:Is DHS available?
p -1072 -688 100 0 1 FTVL:LONG
p -1072 -752 100 0 1 PV:$(sadtop)
p -1072 -720 100 0 1 SNAM:
use bc200tr -1504 -1208 -100 0 frame
xform 0 176 96
p 1072 -1040 100 0 1 author:P Young
p 1296 -1056 100 0 -1 border:C
p 1072 -1072 100 0 1 checked:
p 1328 -1056 100 0 -1 date:22 March 2001
p 1312 -928 100 0 -1 project:Gemini GNIRS DC
p -1504 -1160 100 0 0 revision:1.0
p 1312 -992 150 0 -1 title:dcDHSSad
[comments]
