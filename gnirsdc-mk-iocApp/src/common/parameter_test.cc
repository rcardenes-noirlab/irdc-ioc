/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * RSAA CICADA PROJECT
 * 
 * FILENAME parameter_test.cc
 * 
 * GENERAL DESCRIPTION
 * Tests the parameter class. 
 *
 */
#include <iostream>
#include <unistd.h>
#include <netdb.h>
#include <vector>
#include <string>
#include "parameter.h"
#include "testParameterDB.h"
#include "utility.h"
#include "exception.h"

using namespace std;

/*
 *+ 
 * FUNCTION NAME: parameter_test()
 * 
 * INVOCATION: parameter_test();
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Simple test of the dc parameter class
 * 
 * DESCRIPTION: Tests Parameter by:
 * 1. instantiating a parameter object
 * 2. Adds,sets,gets,lists or clears the object from
 *    user input
 * 3. Exits and deletes object
 * 
 * EXTERNAL VARIABLES: None
 * 
 * PRIOR REQUIREMENTS: Must have EPICS databases created and EPICS loaded
 * on VxWorks IOC
 * 
 * DEFICIENCIES: None
 *
 *- 
 */

#ifdef vxWorks
int parameterTest()
#else
int main()
#endif
{
  try {
    char cmd[255],dt[255],vt[255];
    ParameterData pData;
    unsigned int i,off;
    vector<string*> sValue;
    vector<string*> sValid;
    string *sVal=NULL;
    unsigned int scount=0, svcount=0;
    vector<long*> lValue;
    vector<long*> lValid;
    long *lVal=NULL;
    unsigned int lcount=0, lvcount=0;
    vector<double*> dValue;
    vector<double*> dValid;
    double *dVal=NULL;
    unsigned int dcount=0, dvcount=0;
    ParameterDB *pDB = new TestParameterDB();
    Parameter *dcp = new Parameter(pDB, "dcp", NULL);

    cout << "Parameter Test Routine" << endl;
    cout << "Enter operation (A=add,P=put,p=put one,G=get,C=clear,L=list,Q=quit): ";
    cin >> cmd;
    while (cmd[0] != 'Q') {
      try {
	switch (cmd[0]) {
	case 'A':
	  cout << "Parameter name:"; cin >> pData.name;
	  cout << "Data type (String,Long,Double):"; cin >> dt;
	  cout << "Number of elements:"; cin >> pData.n;	
	  cout << "Validity type (Range,Set,None):"; cin >> vt;
	  if (vt[0] == 'R') {
	    pData.vType=VALIDITY_RANGE;
	  } else if (vt[0] == 'S') {
	    pData.vType=VALIDITY_SET;
	  } else {
	    pData.vType=VALIDITY_NONE;
	  }
	  switch (dt[0]) {
	  case 'S': 	  
	    sValue.push_back(new string[pData.n]);
	    pData.dType = PARAM_STRING;
	    for (i=0; i<pData.n; i++) {
	      cout << "Default string " << i << ":"; 
	      cin >> sValue[scount][i];
	    }
	    if (sVal != NULL) delete [] sVal;	     
	    sVal = NULL;
	    if (pData.vType==VALIDITY_RANGE) {
	      sValid.push_back(new string[2]);
	      pData.nValid = 2;
	      cout << "Min string:"; cin >> sValid[svcount][0];
	      cout << "Max string:"; cin >> sValid[svcount][1];
	      sVal = sValid[svcount];
	      svcount++;
	    } else if (pData.vType==VALIDITY_SET) {
	      cout << "Number of strings in set:"; cin >> pData.nValid;
	      sValid.push_back(new string[pData.nValid]);
	      for (i=0; i<pData.nValid; i++) {
		cout << "valid string " << i << ":"; 
		cin >> sValid[svcount][i];
	      }
	      sVal = sValid[svcount];
	      svcount++;
	    }
	    dcp->add(pData.name.c_str(), (const char*)"", sValue[scount], true, pData.n, sValue[scount], pData.vType, 
		     pData.nValid, sVal, UNPUBLISHED);
	    sVal = NULL;
	    scount++;
	    break;
	  case 'L': 
	    lValue.push_back(new long[pData.n]); 
	    pData.dType = PARAM_LONG; 
	    for (i=0; i<pData.n; i++) {
	      cout << "Default long " << i << ":"; 
	      cin >> lValue[lcount][i];
	    }
	    if (lVal != NULL) delete [] lVal;	     
	    lVal = NULL;
	    if (pData.vType==VALIDITY_RANGE) {
	      lValid.push_back(new long[2]); 
	      pData.nValid = 2;
	      cout << "Min value:"; cin >> lValid[lvcount][0];
	      cout << "Max value:"; cin >> lValid[lvcount][1];
	      lVal = lValid[lvcount];
	      lvcount++;
	    } else if (pData.vType == VALIDITY_SET) {
	      cout << "Number of integers in set:"; cin >> pData.nValid;
	      lValid.push_back(new long[pData.nValid]);
	      for (i=0; i<pData.nValid; i++) {
		cout << "valid integer " << i << ":"; cin >> lValid[lvcount][i];
	      }
	      lVal = lValid[lvcount];
	      lvcount++;
	    }
	    dcp->add(pData.name.c_str(), (const char*)"", (void*)lValue[lcount], true, pData.n, lValue[lcount], pData.vType, 
		     pData.nValid, lVal, UNPUBLISHED);
	    lVal = NULL;
	    lcount++;
	    break;
	  default: 
	    dValue.push_back(new double[pData.n]);
	    pData.dType = PARAM_DOUBLE;
	    for (i=0; i<pData.n; i++) {
	      cout << "Default double " << i << ":"; cin >> dValue[dcount][i];
	    }
	    if (dVal != NULL) delete [] dVal;	     
	    dVal = NULL;
	    if (pData.vType==VALIDITY_RANGE) {
	      dValid.push_back(new double[2]);
	      pData.nValid = 2;
	      cout << "Min value:"; cin >> dValid[dvcount][0];
	      cout << "Max value:"; cin >> dValid[dvcount][1];
	      dVal = dValid[dvcount];
	      dvcount++;
	    } else if (pData.vType == VALIDITY_SET) {
	      cout << "Number of floats in set:"; cin >> pData.nValid;
	      dValid.push_back(new double[pData.nValid]);
	      for (i=0; i<pData.nValid; i++) {
		cout << "valid float " << i << ":"; cin >> dValid[dcount][i];
	      }
	      dVal = dValid[dvcount];
	      dvcount++;
	    }
	    dcp->add(pData.name.c_str(), (const char*)"", (void*)dValue[dcount], true, pData.n, dValue[dcount], pData.vType, 
		     pData.nValid, dVal, UNPUBLISHED);
	    dVal = NULL;
	    dcount++;
	    break;
	  }
	  break;
	case 'P':
	  cout << "Parameter name:"; cin >> pData.name;
	  cout << "Number of elements:"; cin >> pData.n;
	  cout << "Offset:"; cin >> off;	
	  switch (dcp->dType(pData.name.c_str())) {
	  case PARAM_STRING:
	    if (sVal != NULL) delete [] sVal;	     
	    sVal = new string[pData.n];
	    for (i=0; i<pData.n; i++) {
	      cout << "String value " << i << ":"; cin >> sVal[i];
	    }
	    dcp->put(pData.name.c_str(), sVal, off, pData.n);
	    delete [] sVal;
	    sVal = NULL;
	    break;
	  case PARAM_LONG:
	    if (lVal != NULL) delete [] lVal;	     
	    lVal = new long[pData.n]; 
	    for (i=0; i<pData.n; i++) {
	      cout << "Long value " << i << ":"; cin >> lVal[i];
	    }
	    dcp->put(pData.name.c_str(), lVal, off, pData.n);
	    delete [] lVal;
	    lVal = NULL;
	    break;
	  case PARAM_DOUBLE:
	    if (dVal != NULL) delete [] dVal;	     
	    dVal = new double[pData.n];
	    for (i=0; i<pData.n; i++) {
	      cout << "Double value" << i << ":"; cin >> dVal[i];
	    }
	    dcp->put(pData.name.c_str(), dVal, off, pData.n);
	    delete [] dVal;
	    dVal = NULL;
	    break;
	  default:
	    break;
	  }
	  break;
	case 'p':
	  cout << "Parameter name:"; cin >> pData.name;
	  pData.n = 1;
	  off = 0;	
	  switch (dcp->dType(pData.name.c_str())) {
	  case PARAM_STRING: {
	    string s;
	    cout << "String value :"; cin >> s;
	    dcp->put(pData.name.c_str(), s);
	    break;
	  }
	  case PARAM_LONG: {
	    long l; 
	    cout << "Long value :"; cin >> l;
	    dcp->put(pData.name.c_str(), l);
	    break;
	  }
	  case PARAM_DOUBLE: {
	    double d;
	    cout << "Double value :"; cin >> d;
	    dcp->put(pData.name.c_str(), d);
	    break;
	  }
	  default:
	    break;
	  }
	  break;
	case 'G':
	  cout << "Parameter name:"; cin >> pData.name;
	  cout << "Number of elements:"; cin >> pData.n;
	  cout << "Offset:"; cin >> off;	
	  switch (dcp->dType(pData.name.c_str())) {
	  case PARAM_STRING:
	    if (sVal != NULL) delete [] sVal;	     
	    sVal = new string[pData.n];
	    dcp->get(pData.name.c_str(), sVal, off, pData.n);
	    for (i=0; i<pData.n; i++) {
	      cout << "Value " << i << "= '" << sVal[i] << "'" << endl;
	    }
	    delete [] sVal;
	    sVal = NULL;
	    break;
	  case PARAM_LONG:
	    if (lVal != NULL) delete [] lVal;	     
	    lVal = new long[pData.n]; 
	    dcp->get(pData.name.c_str(), lVal, off, pData.n);
	    for (i=0; i<pData.n; i++) {
	      cout << "Value " << i << "= '" << lVal[i] << "'" << endl;
	    }
	    delete [] lVal;
	    lVal = NULL;
	    break;
	  case PARAM_DOUBLE:
	    if (dVal != NULL) delete [] dVal;	     
	    dVal = new double[pData.n];
	    dcp->get(pData.name.c_str(), dVal, off, pData.n);
	    for (i=0; i<pData.n; i++) {
	      cout << "Value " << i << "= '" << dVal[i] << "'" << endl;
	    }
	    delete [] dVal;
	    dVal = NULL;
	    break;
	  default:
	    break;
	  }
	  break;
	case 'C':
	  cout << "Parameter name:"; cin >> pData.name;
	  dcp->clear(pData.name.c_str());
	  break;
	case 'L':
	  dcp->list();
	  break;
	default:
	  break;
	}
      }
      catch (Error& de) {
	cout << de.record_error(__FILE__,__LINE__) << endl;
      }

      cout << "Enter operation (A=add,P=put,G=get,C=clear,L=list,Q=quit): ";
      cin >> cmd;
    }
      
    delete dcp;
    delete pDB;
    for (i=0; i<scount; i++) delete [] sValue[i];
    for (i=0; i<svcount; i++) delete [] sValid[i];
    sValue.clear();
    sValid.clear();    
    for (i=0; i<lcount; i++) delete [] lValue[i];
    for (i=0; i<lvcount; i++) delete [] lValid[i];
    lValue.clear();
    lValid.clear();
    for (i=0; i<dcount; i++) delete [] dValue[i];
    for (i=0; i<dvcount; i++) delete [] dValid[i];
    dValue.clear();
    dValid.clear(); 
    if (lVal != NULL) delete [] lVal;	     
    if (dVal != NULL) delete [] dVal;	        
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return 0;
}
