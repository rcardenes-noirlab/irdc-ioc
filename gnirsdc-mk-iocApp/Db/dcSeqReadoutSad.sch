[schematic2]
uniq 13
[tools]
[detail]
w 1168 563 100 0 n#1 embbis.readModeOut.FLNK 1152 560 1184 560 1184 448 1280 448 esirs.readMode.SLNK
w 1176 531 100 0 n#2 embbis.readModeOut.VAL 1152 528 1200 528 1200 608 1280 608 esirs.readMode.INP
w 1176 1027 100 0 n#3 embbis.timeModeOut.VAL 1152 1024 1200 1024 1200 1088 1280 1088 esirs.timeMode.INP
w 1168 1059 100 0 n#4 embbis.timeModeOut.FLNK 1152 1056 1184 1056 1184 928 1280 928 esirs.timeMode.SLNK
w 792 1107 100 0 n#5 esirs.timeModeSet.FLNK 768 1104 816 1104 816 1040 896 1040 embbis.timeModeOut.SLNK
w 832 1075 100 0 n#6 esirs.timeModeSet.VAL 768 1072 896 1072 embbis.timeModeOut.INP
w 792 611 100 0 n#7 esirs.readModeSet.FLNK 768 608 816 608 816 544 896 544 embbis.readModeOut.SLNK
w 832 579 100 0 n#8 esirs.readModeSet.VAL 768 576 896 576 embbis.readModeOut.INP
w 704 -501 100 0 n#9 esirs.quadrantsSet.VAL 640 -504 768 -504 embbis.embbis#10.INP
w 664 -469 100 0 n#10 esirs.quadrantsSet.FLNK 640 -472 688 -472 688 -536 768 -536 embbis.embbis#10.SLNK
w 1048 -549 100 0 n#11 embbis.embbis#10.VAL 1024 -552 1072 -552 1072 -472 1152 -472 esirs.quadrants.INP
w 1040 -517 100 0 n#12 embbis.embbis#10.FLNK 1024 -520 1056 -520 1056 -632 1152 -632 esirs.quadrants.SLNK
s 1008 -1056 500 0 GNIRS DC Sequence Status/Alarm Database
[cell use]
use bc200tr -1504 -1208 -100 0 frame
xform 0 176 96
p 1072 -1040 100 0 1 author:M Jarnyk
p 1296 -1056 100 0 -1 border:C
p 1072 -1072 100 0 1 checked:
p 1328 -1056 100 0 -1 date:15 Oct 2001
p 1312 -928 100 0 -1 project:Gemini GNIRS DC
p 1312 -992 150 0 -1 title:dcSeqReadoutSad
use esirs -1344 -153 100 0 timeStamp
xform 0 -1136 0
p -1248 -176 100 0 1 DESC:Time stamp
p -1408 -352 100 0 0 EGU:
p -1408 -416 100 0 0 FDSC:Time Stamp
p -1248 -208 100 0 1 FTVL:STRING
p -1248 -272 100 0 1 PV:$(sadtop)
p -1248 -240 100 0 1 SNAM:
use esirs 352 855 100 0 timeModeSet
xform 0 560 1008
p 448 832 100 0 1 DESC:Time Calculation Method
p 288 656 100 0 0 EGU:
p 288 592 100 0 0 FDSC:Time Mode
p 448 800 100 0 1 FTVL:LONG
p 448 736 100 0 1 PV:$(sadtop)
p 448 768 100 0 1 SNAM:
use esirs -1344 871 100 0 nperiods
xform 0 -1136 1024
p -1248 832 100 0 1 DESC:Number of periods
p -1248 768 100 0 1 EGU:number
p -1408 608 100 0 0 FDSC:Number of periods
p -1248 800 100 0 1 FTVL:LONG
p -1248 704 100 0 1 PV:$(sadtop)
p -1248 736 100 0 1 SNAM:
use esirs -352 -633 100 0 nfowler
xform 0 -144 -480
p -256 -672 100 0 1 DESC:Number of Fowler samples
p -256 -736 100 0 1 EGU:number
p -416 -896 100 0 0 FDSC:Number of Fowler samples
p -256 -704 100 0 1 FTVL:LONG
p -256 -800 100 0 1 PV:$(sadtop)
p -256 -768 100 0 1 SNAM:
use esirs -736 -153 100 0 exposed
xform 0 -528 0
p -640 -176 100 0 1 DESC:Actual total integration time
p -800 -352 100 0 0 EGU:
p -800 -416 100 0 0 FDSC:Actual total integration time
p -640 -208 100 0 1 FTVL:DOUBLE
p -640 -272 100 0 1 PV:$(sadtop)
p -640 -240 100 0 1 SNAM:
use esirs 1312 -281 100 0 nref
xform 0 1520 -128
p 1408 -304 100 0 1 DESC:Number of reference samples
p 1248 -480 100 0 0 EGU:
p 1248 -544 100 0 0 FDSC:Number of reference samples
p 1408 -336 100 0 1 FTVL:LONG
p 1408 -400 100 0 1 PV:$(sadtop)
p 1408 -368 100 0 1 SNAM:
use esirs -192 359 100 0 npixReads
xform 0 16 512
p -96 336 100 0 1 DESC:Number of filter samples
p -256 160 100 0 0 EGU:
p -256 96 100 0 0 FDSC:Number of samples to average per pixel
p -96 304 100 0 1 FTVL:LONG
p -96 240 100 0 1 PV:$(sadtop)
p -96 272 100 0 1 SNAM:
use esirs 352 -153 100 0 ncoadds
xform 0 560 0
p 448 -192 100 0 1 DESC:Number of coadds
p 448 -256 100 0 1 EGU:number
p 288 -416 100 0 0 FDSC:Number of coadds
p 448 -224 100 0 1 FTVL:LONG
p 448 -320 100 0 1 PV:$(sadtop)
p 448 -288 100 0 1 SNAM:
use esirs -1344 359 100 0 resetDelay
xform 0 -1136 512
p -1248 320 100 0 1 DESC:Reset interval
p -1248 256 100 0 1 EGU:seconds
p -1408 96 100 0 0 FDSC:Reset interval
p -1248 288 100 0 1 FTVL:DOUBLE
p -1072 288 100 0 1 PREC:0
p -1248 192 100 0 1 PV:$(sadtop)
p -1248 224 100 0 1 SNAM:
use esirs -192 871 100 0 expMode
xform 0 16 1024
p -96 848 100 0 1 DESC:Exposure Mode
p -256 672 100 0 0 EGU:
p -256 608 100 0 0 FDSC:Exposure Mode
p -96 816 100 0 1 FTVL:STRING
p -96 752 100 0 1 PV:$(sadtop)
p -96 784 100 0 1 SNAM:
use esirs -192 -153 100 0 readIntvl
xform 0 16 0
p -96 -192 100 0 1 DESC:Read interval
p -96 -256 100 0 1 EGU:microseconds
p -256 -416 100 0 0 FDSC:Read interval
p -96 -224 100 0 1 FTVL:DOUBLE
p 80 -224 100 0 1 PREC:0
p -96 -320 100 0 1 PV:$(sadtop)
p -96 -288 100 0 1 SNAM:
use esirs -736 359 100 0 nresets
xform 0 -528 512
p -640 320 100 0 1 DESC:Number of resets per exposure
p -640 256 100 0 1 EGU:number
p -800 96 100 0 0 FDSC:Number of resets per exposure
p -640 288 100 0 1 FTVL:LONG
p -640 192 100 0 1 PV:$(sadtop)
p -640 224 100 0 1 SNAM:
use esirs -960 -633 100 0 exposedRQ
xform 0 -752 -480
p -864 -656 100 0 1 DESC:Total exposure time
p -1024 -832 100 0 0 EGU:
p -1024 -896 100 0 0 FDSC:Exposure Mode
p -864 -688 100 0 1 FTVL:DOUBLE
p -864 -752 100 0 1 PV:$(sadtop)
p -864 -720 100 0 1 SNAM:
use esirs -736 871 100 0 period
xform 0 -528 1024
p -640 832 100 0 1 DESC:NDR Period
p -640 768 100 0 1 EGU:microseconds
p -800 608 100 0 0 FDSC:NDR Period
p -640 800 100 0 1 FTVL:DOUBLE
p -464 800 100 0 1 PREC:0
p -640 704 100 0 1 PV:$(sadtop)
p -640 736 100 0 1 SNAM:
use esirs 352 359 100 0 readModeSet
xform 0 560 512
p 448 336 100 0 1 DESC:Read method
p 288 160 100 0 0 EGU:
p 288 96 100 0 0 FDSC:Exposure Mode
p 448 304 100 0 1 FTVL:LONG
p 448 240 100 0 1 PV:$(sadtop)
p 448 272 100 0 1 SNAM:
use esirs 864 -145 100 0 readTime
xform 0 1072 8
p 960 -184 100 0 1 DESC:Read time
p 960 -248 100 0 1 EGU:seconds
p 800 -408 100 0 0 FDSC:Read time
p 960 -216 100 0 1 FTVL:DOUBLE
p 1136 -216 100 0 1 PREC:0
p 960 -312 100 0 1 PV:$(sadtop)
p 960 -280 100 0 1 SNAM:
use esirs 1280 839 100 0 timeMode
xform 0 1488 992
p 1376 816 100 0 1 DESC:Time Mode Calculation
p 1216 640 100 0 0 EGU:
p 1216 576 100 0 0 FDSC:Time Mode Calculation
p 1376 784 100 0 1 FTVL:STRING
p 1376 720 100 0 1 PV:$(sadtop)
p 1376 752 100 0 1 SNAM:timeModeCalc
use esirs 1280 359 100 0 readMode
xform 0 1488 512
p 1376 336 100 0 1 DESC:Read Mode String
p 1216 160 100 0 0 EGU:
p 1216 96 100 0 0 FDSC:Read Mode String
p 1376 304 100 0 1 FTVL:STRING
p 1376 240 100 0 1 PV:$(sadtop)
p 1376 272 100 0 1 SNAM:
use embbis 896 471 100 0 readModeOut
xform 0 1024 544
p 944 400 100 0 1 ONST:BRIGHT
p 944 304 100 0 1 PV:$(sadtop)
p 944 336 100 0 1 THST:VERY_FAINT
p 944 368 100 0 1 TWST:FAINT
p 944 432 100 0 1 ZRST:VERY_BRIGHT
use embbis 896 967 100 0 timeModeOut
xform 0 1024 1040
p 944 896 100 0 1 ONST:Period
p 944 832 100 0 1 PV:$(sadtop)
p 944 832 100 0 0 THST:
p 944 864 100 0 1 TWST:#Periods
p 944 928 100 0 1 ZRST:Exptime
use elongins -1344 -665 100 0 totalCount
xform 0 -1216 -592
p -1296 -704 100 0 1 PV:$(sadtop)
use elongins -1344 -489 100 0 count
xform 0 -1216 -416
p -1280 -512 100 0 1 PV:$(sadtop)
use esirs 224 -721 100 0 quadrantsSet
xform 0 432 -568
p 320 -744 100 0 1 DESC:Read method
p 160 -920 100 0 0 EGU:
p 160 -984 100 0 0 FDSC:Exposure Mode
p 320 -776 100 0 1 FTVL:LONG
p 320 -840 100 0 1 PV:$(sadtop)
p 320 -808 100 0 1 SNAM:
use embbis 768 -609 100 0 embbis#10
xform 0 896 -536
p 927 -719 100 0 1 ONST:1_QUADRANT
p 816 -776 100 0 1 PV:$(sadtop)
p 816 -744 100 0 0 THST:
p 816 -712 100 0 0 TWST:
p 931 -699 100 0 1 ZRST:FULL_FRAME
use esirs 1152 -721 100 0 quadrants
xform 0 1360 -568
p 1248 -744 100 0 1 DESC:Read Mode String
p 1088 -920 100 0 0 EGU:
p 1088 -984 100 0 0 FDSC:Read Mode String
p 1248 -776 100 0 1 FTVL:STRING
p 1248 -840 100 0 1 PV:$(sadtop)
p 1248 -808 100 0 1 SNAM:
[comments]
