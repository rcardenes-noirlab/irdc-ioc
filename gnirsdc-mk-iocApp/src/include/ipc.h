/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
* FILENAME
*   ipc.h
 *
 * PURPOSE
 *   IPC class data structures and interface
 *
 * HISTORY
 *   July 1994 PJY    Created for RSAA CICADA package
 *   June 2002 PJY    Ported to VxWorks/GNIRS from RSAA CICADA package
 */
#ifndef _IPC_H
#define _IPC_H

// Include files
#include <stdlib.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/mman.h>
#include <netdb.h>
#include <csignal>
#include <epicsThread.h>
#include <memory>
#include <map>
#include <string>
#include "utility.h"
#include "exception.h"
#include "epicsMutex.h"
#include "epicsEvent.h"
#include "epicsMessageQueue.h"
#include "ipc.h"

// defines
#if !defined(IPC_ERR) || ((IPC_ERR) != -1)
#undef IPC_ERR
#define IPC_ERR     (-1)
#endif

const int IPC_WAIT = 0;                     // Block until IPC
const int IPC_NOWAIT = 0004000;
const int IPC_NAMELEN = 14;
const int IPC_HOSTLEN = 256;
const int IPC_MAXMSG = 64;
const int IPC_MAXMSGSIZE = 16384;
const int IPC_SOCKET_BACKLOG = 16;
const int IPC_THOUSAND = 1000;
const int IPC_MILLION = 1000000;
const int IPC_THOUSAND_MILLION = 1000000000;

//    The old code had default priority = 190. VxWorks priorities go from 0 (maximum)
//    to 255 (minimum), so interpolating to the 99-0 of OSI EPICS, we get ~25, which
//    is close enough to epicsThreadPriorityCAServerLow

const unsigned int THREAD_DEFAULT_PRIO  = (epicsThreadPriorityCAServerLow + 5);
const unsigned int THREAD_DEFAULT_STACK = epicsThreadGetStackSize(epicsThreadStackMedium);
const epicsThreadId epicsInvalidThread = nullptr;


// typedefs
//+ Set of modes for attaching to an IPC facility
enum IPC_Mode {CONNECT,ESTABLISH};
const int IPC_MODE_LEN = 2;

typedef char IPChostnametype[IPC_HOSTLEN];

// class declarations
/*
CLASS
    Mailbox
    The message queue class implements an inter-process message handling facility.
DESCRIPTION
    The message queue class implements an inter-process message handling facility. Provides
methods for sending and receiving messages between processes. Each mailbox is implemented
using the epicsMessageQueue facility.
KEYWORDS
    IPC
*/
extern "C" {typedef void (*SigAction_Proc)(int, siginfo_t *, void *);}
class Mailbox {
 public:
  /* Factories for mailboxe
   */
  static std::shared_ptr<Mailbox> create(const std::string& name, int maxmsg=IPC_MAXMSG, int maxmsg_size=IPC_MAXMSGSIZE);
  static std::shared_ptr<Mailbox> get(const std::string& name);

  // GROUP:   Member functions
  //+ Post a message to a message queue
  // > msg - pointer to buffer containing message
  // > msgsz - length of message in bytes
  // > msgflg -  message flag, action modifier
  void send_message(const char* msg, int msgsz, int msgflg=IPC_NOWAIT);

  //+ Wait for a message on a message queue
  // < msg - pointer to buffer containing message
  // > msgsz - length of message in bytes
  // > msgflg -  message flag, action modifier
  int get_message(char* msg,  int msgsz,  int msgflg);

  //+ Sets signal handler to be called when message received by mbox
  // > sig - signal to be used for notification
  // > handler - signal handler function
  void set_notify(int sig, SigAction_Proc handler);

  //+ Clears notification set for mbox
  void clear_notify();

  //+ Return the number of messages on queue
  unsigned long messages_waiting();

 private:
  //+ Simple constructor for mailbox
  Mailbox();
  //+ Constructor for using a mailbox, if own then establish, otherwise connect
  // > mqn - message queue name
  // > maxmsg - maximum number of messages
  // > maxmsg_size - maximum size of a message
  Mailbox(const std::string mqn, int maxmsg, int maxmsg_size);
  // GROUP:   Member variables
  std::shared_ptr<epicsMessageQueue> emq;    //+ message queue instance
  int msgs_received;                         //+ count of messages received
  int msgs_sent;                             //+ count of messages sent
  int max_msg_size;                          //+ keep track of message size
  std::string mqname;                        //+ name of message queue

};

// Some typedefs
typedef std::map<int, Mailbox*, less<int> > Mailbox_Array;

/*
CLASS
    Process
    Unix process class provides methods for starting, stopping and checking on separate
    Unix processes.
DESCRIPTION
    Uses Unix systems calls fork and execve to start a new Unix process. Uses waitpid and
    signal for checking and controlling process activity.
KEYWORDS
    IPC
*/
class Process {
 public:
  //+ Simple constructor
  Process();

  //+ Normal constructor to use to start process
  // > pname - new process name
  // > arglist - list of arguments to pass to process
  // > do_fork - flag if set will call fork() else prompt for PID of process called name
   Process(const char* pname, const char* arglist[], bool do_fork);

  //+ Normal constructor to use to start process after doing a setuid/setgid
  // > pname - new process name
  // > arglist - list of arguments to pass to process
  // > do_fork - flag if set will call fork() else prompt for PID of process called name
  // > uid - the uid to run as
  // > gid - the gid to run as
   Process(const char* pname, const char* arglist[], bool do_fork, uid_t uid, gid_t gid);

  // Destructor - kills process
  virtual ~Process();

  // GROUP:   Member variables
  pid_t pid;        //+ Unix process id
  int kill_it;      //+ Flag set if destructor should kill the process
  bool auto_fork;   //+ Flag set if fork system call should be called, otherwise prompts for manually started PID
  char *name;       //+ Name of Unix executable
  int exit_status;  //+ Unix process status upon termination
  uid_t uid;        //+ uid to run as
  gid_t gid;        //+ gid to run as

  // GROUP:   Member functions
  //+ Start the process using fork() if do_fork flag set
  // > pname - new process name
  // > arglist - list of arguments to pass to process
  // > do_fork - flag if set will call fork() else prompt for PID of process called name
  void start(const char* pname, const char* arglist[], bool do_fork);

  //+ find process and return its pid, returns 0 if not found
  // ! stopped - set on return if process was stopped
  pid_t find(int& stopped);

  //+ wait for process to change state
  void wait();

  //+ check if process has changed state without blocking
  int check_wait();

  //+ wait for process to change state within timeout, returns 0 if timeout
  // > timeout_msec - timeout period in msec
  int timed_wait(int timeout_msec);

  //+ send a signal to a process
  // > sig - Unix signal number
  void signal(int sig);

};

// Some typedefs
typedef std::map<int, Process*, less<int> > Process_Array;
typedef std::map<int, Process_Array, less<int> > Process_2D_Array;

/*
CLASS
    Semaphore
    Unix semaphore class implements an inter-process semaphore facility.
DESCRIPTION
    Unix semaphore class implements an inter-process semaphore facility.
Provides methods for using semaphores between processes. Each semaphore is
implemented as a named POSIX semaphore - see man sem_open.
KEYWORDS
    IPC
*/
class Semaphore {
public:
  /* Factories for semaphores
   * must_wait -> if false, the first call to wait will succeed automatically
   */
  static std::shared_ptr<Semaphore> create(const std::string& name, bool must_wait=true);
  static std::shared_ptr<Semaphore> get(const std::string& name);

  //+ Simple constructor
  Semaphore();

  //+ Normal constructor to use if attaching to a semaphore
  // > name - name to use
  // > initially_taken - initial state of the semaphore (free=false, taken=true)
  Semaphore(const std::string& name, bool initially_taken=false);


  // GROUP:   Member functions
  //+ Wait for semaphore to become positive
  void wait();

  //+ Check to see if a semaphore is positive within a timout period - return result
  // > timeout_msec - timeout period in msec
  bool try_wait(int timeout_msec = 0);

  //+ Take - synonym for wait, implemented inline
  void take() { wait(); };

  //+ Wait for semaphore to become positive within a timeout period - return result
  // > timeout_msec - timeout period in msec
  int timed_wait(int timeout_msec);

  void signal();

  //+ Give - synonym for signal, implemented inline
  void give() { signal(); };

  /*
  //+ get current semaphore value
  int get();

  //+ Destroy a semaphore
  // > sn - POSIX name of semaphore to destroy
  void destroy(const char* sn);
  */

  bool waiting;                         //+ set when wait called

  const std::string& name() const;
private:
  // GROUP:   Member variables
  //   Mutex can only be locked/unlocked by the same
  //   thread, so we'll need event to allow for a true
  //   semaphore.
  epicsEvent _event;
  epicsMutex _mutex;
  bool _signaled;
  const std::string& _name;
};


/*
CLASS
    Thread
    Replicates the epicsThread functionality adding a couple of extras (like ability
    to join).
*/

#define make_fn_epicsEvent_guard(ptr) event_guard<epicsEvent>(ptr, __func__)

template<typename T>
class event_guard {
public:
	event_guard(T* event, std::string name) : _name(name), _event(event) {}
	~event_guard() {
		_event->signal();
	}
private:
	std::string _name;
	T* _event;
};

extern "C" {typedef void *(*Thread_Proc)(void*);}

//+ structure used to pass to thread function - contains pointer to thread
class Thread;
struct Thread_Arg {
  Thread* thread;                       //+ pointer to thread
  Thread_Proc start_routine;		//+ the thread start function
  void* arg;                            //+ arbitrary extra argument
};


class Thread {
private:
  // GROUP:   Private member variables
  epicsMutex mu;
  epicsEvent cv;                        //+ condition variable (B semaphore) for thread
  epicsEvent finished;                  //+ Used to signal thread finalization
  bool locked;                          //+ set if mutex locked
  bool locking;                         //+ set when attempting to lock
  bool waiting;                         //+ set when wait called
public:

  // GROUP:   Public member variables
  epicsThreadId tid;
  bool predicate;                       //+ predicate for condition variable
  Loggable_Exception* except;		//+ Used if the thread throws
  string name;                          //+ Name of thread
  int priority;
  int stackSize;

  //+ Simple constructor
  Thread();

  //+ Normal constructor to use to start thread
  // > tname - new thread name
  // > start_routine - name of function to run in thread
  // > arg - pointer to data structure to provide as argument to new thread
  //
  Thread(const char* tname,
	 unsigned int priority = THREAD_DEFAULT_PRIO,
	 int stackSize = THREAD_DEFAULT_STACK,
	 bool dolock = false);

  //+ Destructor - kills thread
  virtual ~Thread();

  //+ Starts the thread
  void start();

  //+ Thread body
  virtual void run() = 0;

  //+ Wait for condition variable to be signalled
  void wait();

  //+ Wait for condition variable to be signalled within timeout, returns 0 if timeout
  // > timeout_msec - timeout period in msec
  int timed_wait(int timeout_msec);

  //+ signal condition variable
  void signal();

  //+ lock threads associated mutex
  void lock();

  //+ unlock threads associated mutex
  void unlock();

  //+Getter for locked
  bool get_locked() {return locked;};

  //+ Getter for locking
  bool get_locking() {return locking;};

  //+ Getter for waiting
  bool get_waiting() {return waiting;};

  //+ Wait until the thread "run" finishes
  void join();

  bool done();

  void run_and_wait();
};

int threadTest();

/*
CLASS
    Timer
DESCRIPTION
    This class is used to provide the same functionality as the system alarm()
    call, but without the use of signals
KEYWORDS
    Timer, alarm
*/
class Timer {
  protected:
    // GROUP:   Protected member variables
    epicsThread* thr;

  public:
    // GROUP:   Public member variables

    //+ true if the alarm has timed out, false if not
    bool timeout;

    //+ true if the timer is still active, false if not
    bool active;

    //+ the current alarm time
    int atime;

    // GROUP:   Public member functions
    //+ Constructor
    Timer();

    //+ Destructor
    virtual ~Timer();

    //+ Set the alarm and start it running
    // > sec - time in seconds
    void alarm(int sec);

    //+ Reset the alarm
    void reset() { alarm(0); }

    //+ The method that is called when the alarm times out
    virtual void do_timeout() { };
};

/*
CLASS
    Svc_Timer
DESCRIPTION
    Derived from the Timer class.  Causes the current process to exit
    when the alarm times out.
KEYWORDS
    Svc_Timer, Timer, alarm
*/
class Svc_Timer : public Timer {
  public:
    // GROUP:   Public member functions

    //+ The method that is called when the alarm times out
    void do_timeout();
};

/*
CLASS
    Test_Timer
DESCRIPTION
    Derived from the Timer class, and used simply for testing
KEYWORDS
    Test_Timer, Timer, alarm
*/
class Test_Timer : public Timer {
  public:
    // GROUP:   Public member functions

    //+ The method that is called when the alarm times out
    void do_timeout();
};

int timerTest();


/* Global function declarations */

//+ Function for generating an ipc name of form %s%d%s%d and up to 14 chars
// ! mname - name generated in supplied buffer
// > name - first base name
// > name2 - 2nd base name
// > id - primary identifier
// > pid - part identifier
void ipc_name(char* mname, const char* name, const char* name2, int id, int pid);

#endif //_IPC_H
