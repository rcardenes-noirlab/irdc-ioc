/*
 * Copyright (c) 2000-2003
 * $Id: fits_client.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 *
 * GEMINI GNIRS PROJECT
 * 
 * FILENAME fits_client.cc
 * 
 * GENERAL DESCRIPTION
 *
 * An example fits client program - simply sends a FITS image to fits server
 *
 * To run on
 * Solaris:      $ ./fits_client server_name server_port fits_name [rows cols [sndbuf]]
 * Where
 * server_name = name of fits server host
 * server_port = port number of fits server
 * fits_name = pathname of fits file to transfer
 * rows = number of rows in each single transfer (optional, default=256)
 * cols = number of cols in each single transfer (optional, default=1024)
 * sndbuf = TCP send buffer size (optional, default=system defined)
 *
 * $Log: fits_client.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Gnirs; first gemini-modified release
 *
 * Revision 1.5  2005/08/18 03:57:53  pjy
 * Added routine to run test in spawned task
 *
 * Revision 1.4  2005/07/18 00:42:37  pjy
 * Removed shared_image_buffer
 * Added frames_mem as param
 *
 * Revision 1.3  2004/12/22 06:13:47  pjy
 * DC data src files ready for first Gemini release
 *
 * Revision 1.2  2004/08/20 02:31:48  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.1  2004/03/19 01:51:46  pjy
 * Added
 *
 * Revision 1.1  2003/03/25 07:14:05  pjy
 * Initial checkin of Fits server and test client
 *
 *
 */
/*include files*/
#ifdef vxWorks
#include <vxWorks.h>
#include <errno.h>
#include <sockLib.h>
#include <inetLib.h>
#include <hostLib.h>
#include <ioLib.h>
#include <sysLib.h>
#include <timers.h>
#include <time.h>
#else
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#endif
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "ipc.h"
#include "fits.h"
#include "configuration.h"
#include "common.h"
#include "camera.h"
#include "ir_camera.h"
#include "platform_specific.h"
#include "config_table.h"
#include "utility.h"
#include "dc.h"

using namespace std;

/* global variables */
Detector_Image *shared_image_buffer = NULL;
Config *shared_config = NULL;

// Local function definitions
static void get_pixels(Fits* fits, int j, char* buf, int xo, int yo, int height, int width);

#ifdef vxWorks
// fitsClient "mayhem",10000,"cnf/gnirs_mdata16.fits","GNIRS"
static int fitsClient_task(char* server_name, int server_port, char* fits_name, char* inst, 
			   int rows, int cols, int sndbuf);
int fitsClient(char* server_name, int server_port, char* fits_name, char* inst)
{
  int tid;
  int rows=256;
  int cols=1024;
  int sndbuf=-1;

  // Start this in a spawned task with an appropriately sized stack
  if ((tid = taskSpawn("fitsClient", 100, VX_FP_TASK, 0x400000, (FUNCPTR) fitsClient_task, 
		      (int)server_name, server_port, (int)fits_name, (int)inst, 
		       rows, cols, sndbuf, 0,0,0)) == ERROR) {
    printErr("taskSpawn of fits_client_task failed, errno=%d!\n", errno,0,0,0,0,0);
  }

  return 0;
}
static int fitsClient_task(char* server_name, int server_port, char* fits_name, char* inst, 
			   int rows, int cols, int sndbuf)
{
#else
int main(int argc, char *argv[])
{
  int server_port;
  char server_name[IPC_HOSTLEN];
  int rows = 256;
  int cols = 1024; // default rows and columns for 512k transfer with bitpix = 16
  int sndbuf = -1;
  Fitsname fits_name;
  Nametype inst;
#endif
  TCPSocket_Client* fits_client = NULL;
  Filenametype fname;
  IR_Camera *ir_camera = NULL;
  Camera_Desc camera;
  Fits *fits = NULL;
  Camera_Table ctable;
  Region_Data regions;
  Detector_Image_Info image_info;
  Detector_Image_Header header;
  Detector_Image_Main_Header main_header;  
  Detector_Image_Sub_Header *sub_header = &header.Detector_Image_Header_u.sub_header;  
  Detector_Image_Close_Header *close_header = &header.Detector_Image_Header_u.close_header; 
  char *frames_mem = NULL;                   
  Platform_Specific specific;   
  Frame_Data *this_frame;
  Sub_Frame_Data* this_subframe;
  int ack,nr,j,xo,yo,n, f,sf,image_seq;
  struct timespec tps,tpf;           
  double ts,tf;  
  int status;
  char* buf = NULL;

#ifndef vxWorks
  if (argc < 5) {
    printf("Usage: fits_client host port fits_name instrument [rows cols [sndbuf]}\n");
    return -1;
  }
  strncpy(server_name, argv[1], IPC_HOSTLEN);
  server_port = atoi(argv[2]);
  strncpy(fits_name, argv[3], FITSNAMELEN);
  strncpy(inst, argv[4], NAMELEN);
  if (argc > 5)
    rows = atoi(argv[5]);
  if (argc > 6)
    cols = atoi(argv[6]);
  if (argc > 7)      
    sndbuf = atoi(argv[7]);
#endif
  
  try {
    message_write(DEBUG_FULL, "Fits_Client: starting, server=%s,port=%d, FITS file=%s",server_name, server_port,fits_name);  
    
    cout << "Reading camera configuration table " << CAMERA_TABLE << endl;
    // read in the camera configuration table
    strncpy(fname, CAMERA_TABLE, FILENAMELEN-1);
    ctable.read(fname);
    cout << "Done" << endl;

    camera = ctable[inst];

     // Allocate shared memory areas
    shared_config = new Config;
    
    shared_image_buffer = (Detector_Image*) malloc(sizeof(Detector_Image) + IMAGE_BUF_SIZE);  

    cout << "Instantiating an IR_Camera object" << endl;
    ir_camera = new IR_Camera(camera,0,0, &specific);
    ir_camera->image = shared_image_buffer;
    cout << "Done" << endl;

    // Create a client socket to connect to server
     cout << "Instantiating FITS client" << endl;
    fits_client = new TCPSocket_Client(server_name, server_port, sndbuf); 
    cout << "Done" << endl;
    
    message_write(DEBUG_FULL, "Connected OK to %s", server_name);
    
   // Open and read Fits file
    cout << "Opening input FITS file " << fits_name << endl;
    fits = new Fits(fits_name, O_RDONLY, TRUE);
    cout << "Done" << endl;


    // Describe a single full detector region
    regions.camera_coords = true;
    regions.nregions = 1;
    regions.xo[0] = regions.yo[0] = 0;
    regions.width[0] = camera.cols;
    regions.height[0] = camera.rows;
    regions.rcf = regions.ccf = 1;
    regions.overscan_cols = regions.overscan_rows = 0;
    regions.chip_mask = 1;

    cols = (cols>camera.cols)? camera.cols:cols;
    rows = (rows>camera.rows)? camera.rows:rows;
 
    image_info.unsigned_data= false;
    image_info.bitpix = fits->xt[0].bitpix;
    image_info.bscale = 1.0;
    image_info.bzero = 0.0;
  
    // Call the camera methods that can setup the data structures describing the image
    ir_camera->setup_regions(regions, image_info.bitpix);
    setup_image_header(0, 0, 0, false, &camera, main_header, ir_camera->rgn, regions, 
		       (char*)&ir_camera->image->frames_mem, image_info, false, false);

    image_seq = 0;
    main_header.image_seq = image_seq++;

    // Get timestamp 
    status = clock_gettime(CLOCK_REALTIME,&tps);
    
    header.type = IMAGE_MAIN_HEADER;
    header.Detector_Image_Header_u.main_header = main_header;
    fits_client->send_message((char*)&header, sizeof(header), 0, SECOND);
    message_write(DEBUG_FULL, "Sent main_header, image name=%s, size=%d", main_header.client_image_name, sizeof(header));
    
    // read acknowledgment 
    ack = 0;
    nr = fits_client->get_message((void*)&ack, sizeof(ack), 0, SECOND);
    if (ack != sizeof(header))
      message_write(DEBUG_FULL, "Ack=%d when should have been %d for main_header", ack, sizeof(header));
    else
      message_write(DEBUG_FULL, "Got main-header ack ok");
    
    // Send the frames data - after copying data to an aligned buffer
    frames_mem = (char*) memalign(ALIGN_SIZE, FRAMES_MEM_SIZE);
    memcpy(frames_mem, main_header.frames.frames_val, FRAMES_MEM_SIZE);

    fits_client->send_message(frames_mem, FRAMES_MEM_SIZE, 0, SECOND);
    cout << "Fits_client: Sent " << nr << " frames data bytes" << endl;

    free(frames_mem);

    // read acknowledgment 
    ack = 0;
    nr = fits_client->get_message((void*)&ack, sizeof(ack), 0, SECOND);
    if (ack != FRAMES_MEM_SIZE)
      message_write(DEBUG_FULL, "Ack=%d when should have been %d for frames_mem", ack, FRAMES_MEM_SIZE);
    else
      message_write(DEBUG_FULL, "Got frames_mem ack ok");

    // Write pixels - first allocate a send buffer.
    // Pixels are transferred rows*cols at a time starting at 0,0
    buf = new char[rows*cols*BYTE_PIX(fits->xt[0].bitpix)];
    
    // Set up sub_header fields
    sub_header->id = sub_header->cam_id = sub_header->cid = 0;
    
    // Loop over each frame transferring a buf sized chunk at a time
    j = (fits->extend)? 1:0;
    for (f=0; f<(int)main_header.frames.frames_len; f++) {
      this_frame = &main_header.frames.frames_val[f];

      for (sf=0; sf<(int)this_frame->subframes.subframes_len; sf++) {
	this_subframe = &this_frame->subframes.subframes_val[sf];
	sub_header->frame = f; 
	sub_header->subframe = sf;
	sub_header->subframe_idx = this_subframe->subframe_idx;
	sub_header->xdir = this_subframe->xdir;
	sub_header->ydir = this_subframe->ydir;
	sub_header->orient = this_subframe->orient;
	yo = 0;
	
	// Loop over row chunks
	while (yo < (int)this_subframe->height) {
	  
	  sub_header->y = yo + this_subframe->frame_yo;
	  sub_header->height = (yo+rows>(int)this_subframe->height)? 
	    this_subframe->height-yo:rows;
	  xo = 0;
	  
	  // Loop over column chunks
	  while (xo < (int)this_subframe->width) {
	    
	    sub_header->x = xo + this_subframe->frame_xo;
	    sub_header->width = (xo+cols>(int)this_subframe->width)? 
	      this_subframe->width-xo:cols;
	  
	    get_pixels(fits,j,buf,xo,yo,sub_header->height,sub_header->width);
	  
	    // Send the sub header
	    sub_header->image_seq = image_seq++;
	    header.type = IMAGE_SUB_HEADER;
	    fits_client->send_message((char*)&header, sizeof(header), 0, SECOND);
	    message_write(DEBUG_FULL, "Sent sub-header, xo=%d,yo=%d,width=%d,height=%d", xo, yo, sub_header->width, 
			  sub_header->height);
	    
	    // read acknowledgment 
	    ack = 0;
	    nr = fits_client->get_message((void*)&ack, sizeof(ack), 0, SECOND);
	    if (ack != sizeof(header))
	      message_write(DEBUG_FULL, "Ack=%d when should have been %d for sub_header", ack, sizeof(header));
	    else
	      message_write(DEBUG_FULL, "Got sub-header ack ok");
	    
	    // Send the pixels
	    n = sub_header->height * sub_header->width * BYTE_PIX(this_frame->image_info.bitpix);
	    fits_client->send_message(buf, n, 0, SECOND);
	    message_write(DEBUG_FULL, "Sent %d pixel bytes", n); 
	    
	    // read acknowledgment 
	    ack = 0;
	    nr = fits_client->get_message((void*)&ack, sizeof(ack), 0, SECOND);
	    if (ack != n)
	      message_write(DEBUG_FULL, "Ack=%d when should have been %d for pixels", ack, sizeof(header));
	    else
	      message_write(DEBUG_FULL, "Got pixel chunk ack ok");
	    
	    // Move to next set of columns
	    xo += cols;
	  }
	  
	  // Move to next set of rows
	  yo += rows;
	}
	j++;
      }
    }
    
    // Send the close header
    close_header->id = close_header->cam_id = close_header->cid = 0;
    close_header->image_seq = image_seq;
    header.type = IMAGE_CLOSE_HEADER;
    fits_client->send_message((char*)&header, sizeof(header), 0, SECOND);
    message_write(DEBUG_FULL, "Sent close-header...");
    
    // read acknowledgment 
    ack = 0;
    nr = fits_client->get_message((void*)&ack, sizeof(ack), 0, SECOND);    
    message_write(DEBUG_FULL, "Got close-header ack");
    
    /* Now report time taken for full operation */
    status = clock_gettime(CLOCK_REALTIME,&tpf);
    tf = (double)tpf.tv_sec + tpf.tv_nsec/1000000000.0;
    ts = (double)tps.tv_sec + tps.tv_nsec/1000000000.0;
    printf("Time for transfer= %e seconds, tpf=%ld,tps=%ld\n", tf-ts, tpf.tv_sec, tps.tv_sec);
  }
  catch (Error & e) {
    e.print_error(__FILE__, __LINE__);
  }
  
  // Free work space and netsocket client
  if (buf != NULL)
    delete [] buf;
  if (fits_client != NULL)
    delete fits_client;
  if (fits != NULL)
    delete fits;
  if (ir_camera != NULL)
    delete ir_camera;
  if (shared_config != NULL)
    delete shared_config;
  if (shared_image_buffer!=NULL)
    delete shared_image_buffer;
  return 0;
}
/*
 *+ 
 * FUNCTION NAME: get_pixels
 * 
 * INVOCATION: get_pixels(Fits* fits, int j, char* buf, int xo, int yo, int height, int width)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > fits - fits file to use for building main header
 * > j - fits extension
 * < buf - transfer buffer - filled with fits pixels
 * > xo - offset into frame
 * > yo - offset into frame
 * > height - height of sub frame to transfer
 * > width - width of sub frame to transfer
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Fills transfer buffer with pixels from Fits file
 * 
 * DESCRIPTION: 
 * Simply copies from fits file the rectangular area of pixels - no bounds
 * checking performed - assumed done externally.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
static void get_pixels(Fits* fits, int j, char* buf, int xo, int yo, int height, int width)
{
  short* spix = (short*) fits->xt[j].data;
  short* sbuf = (short*) buf;
  int* ipix = (int*) fits->xt[j].data;
  int* ibuf = (int*) buf;
  float* fpix = (float*) fits->xt[j].data;
  float* fbuf = (float*) buf;
  double* dpix = (double*) fits->xt[j].data;
  double* dbuf = (double*) buf;
  int k,c,r;

  // Some range checks
  if ((fits->xt[j].nx < width) || (fits->xt[j].ny < height)) 
    throw Error("Fits file size does not match readout config?",E_ERROR,EINVAL,__FILE__, __LINE__);
  
  k = 0;	
  switch (fits->xt[j].bitpix) {
  case SHORT_BITPIX:
    for (r=yo; r<yo+height; r++)
      for (c=xo; c<xo+width; c++)
	sbuf[k++] = spix[r*fits->xt[j].nx+c];
    break;
  case INT_BITPIX:
    for (r=yo; r<yo+height; r++)
      for (c=xo; c<xo+width; c++)
	ibuf[k++] = ipix[r*fits->xt[j].nx+c];
    break;
  case FLOAT_BITPIX:
    for (r=yo; r<yo+height; r++)
      for (c=xo; c<xo+width; c++)
	fbuf[k++] = fpix[r*fits->xt[j].nx+c];
    break;
  case DOUBLE_BITPIX:
    for (r=yo; r<yo+height; r++)
      for (c=xo; c<xo+width; c++)
	dbuf[k++] = dpix[r*fits->xt[j].nx+c];
    break;
  default:
    break;
  }
}
#ifdef vxWorks
// Routine to use to run test in spawned task
void spawn_fits_client(char* server_name, int server_port, char* fits_name, char* inst, 
		       int rows, int cols, int sndbuf)
{
  int dhs_tid;

  if ((dhs_tid = taskSpawn("fits_client_test", 100, VX_FP_TASK, 0x400000, (FUNCPTR) fitsClient, 
			   (int)server_name,server_port,(int)fits_name,(int)inst, rows, cols, sndbuf, 
			   0,0,0)) == ERROR) {
    message_write(ALWAYS_LOG,"spawn_fits_client: taskSpawn of fits_client failed!");
  } else
    message_write(ALWAYS_LOG,"spawn_fits_client: taskSpawn of fits_client succeeded!");
}
#endif

