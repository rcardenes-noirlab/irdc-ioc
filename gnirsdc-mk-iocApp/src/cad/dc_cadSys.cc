/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_cadSys.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI NIFS PROJECT
 *
 * FILENAME dc_cadSys.c
 *
 * GENERAL DESCRIPTION
 *
 * Handle CADS from NIFS DC system commands.  These are INIT, and .  Their
 * equivalent CAR records will be set to busy and then back to idle.
 *
 *
 * $Log: dc_cadSys.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.14  2005/09/27 19:55:26  pjy
 * Added cannot observe message to CAD mess field
 *
 * Revision 1.13  2005/09/24 00:43:47  pjy
 * Added support for extra debugging states
 *
 * Revision 1.12  2005/09/15 04:48:30  pjy
 * Removed check for lockTmp - handled by SNL
 *
 * Revision 1.11  2005/09/06 22:32:35  pjy
 * Added lockTmp check before accepting CAD
 *
 * Revision 1.10  2005/08/18 03:29:54  pjy
 * Added cadObserve
 *
 * Revision 1.9  2005/07/30 01:44:29  pjy
 * Fixed message len
 *
 * Revision 1.8  2005/07/30 01:14:27  pjy
 * Fixed typo
 *
 * Revision 1.7  2005/07/29 23:46:37  pjy
 * Use set_and_verify to check to see if observc is busy
 *
 * Revision 1.6  2005/07/12 06:47:29  pjy
 * removed init cad snam routine
 *
 * Revision 1.5  2005/04/27 03:42:33  pjy
 * Added simulate routines
 *
 * Revision 1.4  2004/08/20 02:27:04  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.3  2004/04/27 07:25:58  pjy
 * T2.2
 *
 * Revision 1.1  2004/03/23 01:59:54  pjy
 * Gem 8.6 port
 *
 * Revision 1.1  2003/03/11 01:29:34  pjy
 * Import modules
 *
 *
 */
/* VxWorks include files */
#include <cstdio>
#include <cstring>
#include <string>

#ifdef __cplusplus
extern "C" {
#endif
#include <cadRecord.h>
#include <cad.h>
#include <dbAccess.h>
#include <dbCommon.h>
#include <registryFunction.h>
#include <epicsExport.h>
#ifdef __cplusplus
}
#endif

#include "logging.h"
#include "dc.h"
#include "parameter.h"

// The general status DB
extern Parameter *dc_par;                      //+ Internal DC records
extern Parameter *dc_status;                   //+ Internal DC status parameters

#ifdef __cplusplus
extern "C" long CADsimulateInit(struct cadRecord *pCad);
extern "C" long CADsimulate(struct cadRecord *pCad);
extern "C" long dcCADobserve(struct cadRecord *pCad);
extern "C" long dcCADdebug( struct cadRecord *pcad );
#endif

static logger::Level simulate(logger::Level::None);   /* Required init simulate level */

/*+
 *   Function name:
 *   CADsimulateInit
 *
 *   Purpose:
 *   Initialisation function for "simulate" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the simulate CAD record is initialised.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *   If a valid simulate level is given the internal simulate level is altered.
 *
 *   simulate is the command for setting the desired simulation level.
 *   The command has no arguments
 *
 *   Invocation:
 *   struct cadRecord *pcad;
 *   status = CADsimulateInit( pcad );
 *
 *   Parameters in:
 *      None
 *
 *   Parameters out:
 *      None
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Peter Young RSAA
 *
 *   History:
 *   13-April-05: Original version.                        (pjy)
 *-
 */

long CADsimulateInit(struct cadRecord *pCad)
{
  long status;         /* return status */

  /* Initialise CAD status */

  status = CAD_ACCEPT;


  return status;
}

/* ===================================================================== */


/*+
 *   Function name:
 *   CADsimulate
 *
 *   Purpose:
 *   User defined function for "simulate" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the simulate CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *   If a valid simulate level is given the internal simulate level is altered.
 *
 *   simulate is the command for setting the desired simulation level.
 *
 *   Invocation:
 *   struct cadRecord *pcad;
 *   status = CADsimulate( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *      > pcad->a     *string     CAD input argument A
 *                                = Requested simulate level
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *      < pcad->vala  *string     CAD output value A
 *                                = Set simulate level
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Peter Young RSAA
 *
 *   History:
 *   13 April 2005: Original version.                        (pjy)
 *-
 */
long CADsimulate(struct cadRecord *pCad)
{
  long status;         /* return status */
  string msg,observeC;

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pCad->dir) {
  case menuDirectiveMARK:
    /* CAD MARK directive detected. Nothing needs to be done.
     */

    logger::message(logger::Level::Min, "simulate - MARK directive.");
    break;

  case menuDirectivePRESET:
    /* CAD PRESET directive detected. Check the simulate level given is valid.
     */

    dc_par->get(OBSERVEC, observeC);
    if (observeC == "BUSY") {
	status = CAD_REJECT;
	msg = "Cannot process CAD - OBSERVE running!";
	dc_status->put(CAD_START_MSG, msg);
	strncpy(pCad->mess, msg.c_str(), MAX_STRING_SIZE);
	status = CAD_REJECT;
    } else {
      logger::message(logger::Level::Min,  "simulate - PRESET directive.");
      logger::message(logger::Level::Full, "Requested simulate level = %s", pCad->a);

      if (strcmp(pCad->a, "NONE") == 0 ) {
	simulate = logger::Level::None;
	strcpy((char*)pCad->vala, (char*)pCad->a);
      } else if (strcmp( pCad->a, "FAST") == 0 ) {
	simulate = logger::Level::Min;
	strcpy((char*)pCad->vala, (char*)pCad->a);
      } else if (strcmp( pCad->a, "FULL") == 0 ) {
	simulate = logger::Level::Full;
	strcpy((char*)pCad->vala, (char*)pCad->a);
      } else {
	strncpy(pCad->mess, "simulate - Invalid simulate level",
		MAX_STRING_SIZE);
	status = CAD_REJECT;
      }
    }

    break;

  case menuDirectiveCLEAR:
    /* CAD CLEAR directive detected. */

    logger::message(logger::Level::Min, "simulate - CLEAR directive.");
    break;

  case menuDirectiveSTART:
    /* CAD START directive detected. Set the required simulate level.
     */

    logger::message(logger::Level::Min, "simulate - START directive. New level = %s", logger::to_string(simulate) );
    break;

  case menuDirectiveSTOP:
    /* CAD STOP directive detected. The normal way to simulate an observation is
     * through the ABORT sequence command, so reject this directive.
     */
    logger::message(logger::Level::None, "simulate - STOP directive. simulate cannot be stopped.");

    strncpy( pCad->mess, "simulate - Cannot be stopped", MAX_STRING_SIZE );
    status = CAD_REJECT;
    break;

  default:
    /* Unrecognised CAD directive detected. This is regarded as an error. */

    strncpy( pCad->mess, "simulate - Unrecognised CAD directive",
	     MAX_STRING_SIZE );
    status = CAD_REJECT;
    break;

  }
  return status;
}


/*+
 *   Function name:
 *   dcCADobserve
 *
 *   Purpose:
 *   User defined function for "observe" CAD record
 *
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *
 *   observe is the command for starting an observation.
 *   The command has one argument - the data label.
 *
 *   Invocation:
 *   struct cadRecord *pcad;
 *   status = CADobserve( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *      > pcad->a     *string     CAD input argument A
 *                                = data label
 *      > pcad->b     string      Instrument health. The command will be
 *                                rejected if this flag is not set to GOOD
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *      < pcad->vala  *string     CAD output value A
 *                                = data label
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   11-Nov-1996: Original version.                              (smb)
 *   21-Mar-1997: Data label and "applyC" arguments added.       (smb)
 *   16-Apr-1997: Check on instrument being busy added.    (smb)
 *   5-Aug-2005: Changed use of busy with check for health (pjy)
 *-
 */

long dcCADobserve( struct cadRecord *pcad )
{
  long status;         /* return status */
  string msg;

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

      logger::message(logger::Level::Min, "observe - MARK directive.");
      break;

    case menuDirectivePRESET:

/* CAD PRESET directive detected. The data label is assumed valid.
 * IS THERE A WAY OF CHECKING THE DATA LABEL?
 */

      logger::message(logger::Level::Min, "observe - PRESET directive.");

      printf("observe(dc_cadSys) - Data label = %s\n", pcad->a);
      logger::message(logger::Level::Full, "Data label = %s", pcad->a);

/*
 * Reject the command if the second argument indicates the instrument health is not GOOD
 */

      logger::message(logger::Level::Min, "Health = %s", pcad->b);
      if (strcmp("GOOD", pcad->b)) {
	status = CAD_REJECT;
	if (dc_status != NULL) {
	  msg = "Cannot do OBSERVE, health not GOOD!";
	  dc_status->put(CAD_START_MSG, msg);
	}
      }
/*
 * Only copy the data label to the output if the command has not been
 * rejected
 */

      if ( status != CAD_REJECT ) strcpy( (char*)pcad->vala, pcad->a );
      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

      logger::message(logger::Level::Min, "observe - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Do nothing. The directive is being
 * monitored by the CICS DC sequence code, which will start the appropriate
 * action.
 */

      logger::message(logger::Level::Min, "observe - START directive.");
      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. The normal way to stop an observation is
 * through the STOP sequence command, so reject this directive.
 */
      logger::message(logger::Level::None, "observe - STOP directive. Use STOP command instead.");

      strncpy( pcad->mess, "observe - Use STOP command instead.",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "observe - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}
/*+
 *   Function name:
 *   CADdebug
 *
 *   Purpose:
 *   User defined function for "debug" CAD record
 *
 *   Purpose:
 *   This routine is called whenever the debug CAD record is processed.
 *   It checks that the command is acceptable and returns a status and a
 *   message, which are written to the VAL and MESS fields of the CAD record.
 *   If a valid debug level is given the internal debug level is altered.
 *
 *   debug is the command for setting the desired debugging level.
 *
 *   Invocation:
 *   struct cadRecord *pcad;
 *   status = CADdebug( pcad );
 *
 *   Parameters in:
 *      > pcad->dir   *string     CAD directive
 *      > pcad->a     *string     CAD input argument A
 *                                = Requested debug level
 *
 *   Parameters out:
 *      < pcad->mess  *string     failure message written to CAD MESS field
 *      < pcad->vala  *string     CAD output value A
 *                                = Set debug level
 *
 *   Return value:
 *      < status      long        Status value written to CAD VAL field
 *
 *   Globals:
 *      External functions:
 *      None
 *
 *      External variables:
 *      None
 *
 *   Requirements:
 *   It is assumed the CAD record has already been initialized and the
 *   directive and any arguments have already been assembled into the pcad
 *   structure
 *
 *   Author:
 *   Steven Beard  (smb@roe.ac.uk)
 *
 *   History:
 *   17-Mar-1997: Original version.                        (smb)
 *          2005: Modified to include some new NIFS levels (pjy)
 *-
 */
static void upcase(char *s)
{
  char *c;

  for (c = s; *c != '\0'; c++) {
	*c = (char)toupper(*c);
  }
}

long dcCADdebug( struct cadRecord *pcad )
{
  long status;                  /* return status */
  static logger::Level debug;   /* Required debug level */

/* The magic number 40 is that used in cadRecord.h.It is a #define
   owing to the fact that ANSI C rejects using a const variable for
   an array length. */
#define CAD_INPUT_LENGTH  MAX_STRING_SIZE

  /* Buffer to which we copy the input before converting it to
     upper case */
  char buf[CAD_INPUT_LENGTH];

/* Initialise CAD status */

  status = CAD_ACCEPT;

/* Switch according to the CAD directive in DIR field */

  switch (pcad->dir)
  {
    case menuDirectiveMARK:

/* CAD MARK directive detected. Nothing needs to be done.
 */

      logger::message(logger::Level::Min, "debug - MARK directive.");
      break;

    case menuDirectivePRESET:

	  /* Copy the input to a buf and then convert it to upper case
         for comparison. */
	  strncpy(buf,pcad->a, CAD_INPUT_LENGTH-1);
	  upcase(buf);

/* CAD PRESET directive detected. Check the debug level given is valid.
 */

      logger::message(logger::Level::Min, "debug - PRESET directive.");
      logger::message(logger::Level::Full, "Requested debug level = %s", pcad->a);

      /* Get the current debugLevel  - PJY */
      debug = logger::getLevel();

      if ( strcmp( buf, "NOLOG" ) == 0 ) {
        debug = logger::Level::NoLog;
        strcpy( (char*)pcad->vala, pcad->a );
      } else if ( strcmp( buf, "NONE" ) == 0 ) {
	debug = logger::Level::None;
	strcpy( (char*)pcad->vala, pcad->a );
      } else if ( strcmp( buf, "MIN" ) == 0 ) {
	debug = logger::Level::Min;
	strcpy( (char*)pcad->vala, pcad->a );
      } else if ( strcmp( buf, "SDSU" ) == 0 ) {
	debug = logger::Level::Min;
	strcpy( (char*)pcad->vala, pcad->a );
      } else if ( strcmp( buf, "STATE" ) == 0 ) {
	debug = logger::Level::Min;
	strcpy( (char*)pcad->vala, pcad->a );
      } else if ( strcmp( buf, "SPECIAL" ) == 0 ) {
	debug = logger::Level::Min;
	strcpy( (char*)pcad->vala, pcad->a );
      } else if ( strcmp( buf, "CALCS" ) == 0 ) {
	debug = logger::Level::Min;
	strcpy( (char*)pcad->vala, pcad->a );
      } else if ( strcmp( buf, "FULL" ) == 0 ) {
	debug = logger::Level::Full;
	strcpy( (char*)pcad->vala, pcad->a );
      } else {
	strncpy(pcad->mess, "debug - Invalid debug level ", MAX_STRING_SIZE);
	status = CAD_REJECT;
      }

      break;

    case menuDirectiveCLEAR:

/* CAD CLEAR directive detected. */

      logger::message(logger::Level::Min, "debug - CLEAR directive.");
      break;

    case menuDirectiveSTART:

/* CAD START directive detected. Set the required debug level.
 *
 */

      logger::message(logger::Level::Min, "debug - START directive. New level = %s", logger::to_string(debug) );

      logger::setLevel( debug );

      logger::message(logger::Level::Min, "debug - New level = %s", logger::to_string(logger::getLevel()) );

      break;

    case menuDirectiveSTOP:

/* CAD STOP directive detected. The normal way to debug an observation is
 * through the ABORT sequence command, so reject this directive.
 */
      logger::message(logger::Level::None, "debug - STOP directive. debug cannot be stopped.");

      strncpy( pcad->mess, "debug - Cannot be stopped", MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

    default:

/* Unrecognised CAD directive detected. This is regarded as an error. */

      strncpy( pcad->mess, "debug - Unrecognised CAD directive",
        MAX_STRING_SIZE );
      status = CAD_REJECT;
      break;

  }
  return status;
}

epicsRegisterFunction(dcCADdebug);
