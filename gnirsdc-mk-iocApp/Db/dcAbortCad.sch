[schematic2]
uniq 6
[tools]
[detail]
w 3536 3115 -100 0 CLID inhier.CLID.P 3456 3168 3520 3168 3520 3104 3600 3104 ecad2.abort.ICID
w 4006 3147 100 0 n#1 junction 3952 3136 4096 3136 4096 2688 4176 2688 cadPlus.abortP.STAT
w 3956 3211 100 0 n#1 ecad2.abort.VAL 3920 3136 3952 3136 3952 3296 4048 3296 outhier.VAL.p
w 3968 2635 100 0 n#2 ecad2.abort.SPLK 3920 2624 4064 2624 4064 2720 4176 2720 cadPlus.abortP.SPIN
w 4080 2763 100 0 n#3 ecad2.abort.STLK 3920 2656 4032 2656 4032 2752 4176 2752 cadPlus.abortP.STIN
w 3988 3163 100 0 n#4 ecad2.abort.MESS 3920 3104 3984 3104 3984 3232 4048 3232 outhier.MESS.p
w 3572 3195 100 0 n#5 inhier.DIR.P 3504 3264 3568 3264 3568 3136 3600 3136 ecad2.abort.DIR
[cell use]
use inhier 3488 3223 100 0 DIR
xform 0 3504 3264
use inhier 3440 3127 100 0 CLID
xform 0 3456 3168
use cadPlus 4176 2800 150 0 abortP
xform 0 4256 2704
p 4196 2572 100 0 1 set0:mech
p 4176 2592 100 0 1 set1:cad abortP
use ecad2 3688 3184 150 0 abort
xform 0 3760 2880
p 3476 2620 100 0 0 MFLG:THREE STATES
p 3716 2820 65 0 1 SNAM:CADabort
use outhier 4016 3191 100 0 MESS
xform 0 4032 3232
use outhier 4016 3255 100 0 VAL
xform 0 4032 3296
use eborderC 2496 1599 100 0 eborderC#0
xform 0 4176 2904
p 5072 1752 100 768 -1 author:Peter Young
p 5072 1864 40 0 -1 comment1:1997-10-01 GMOS version. Ken Ramey
p 5072 1844 40 0 -1 comment2:2000-12-12 Modified for GNIRS. Peter Young
p 5072 1720 100 768 -1 date:2000-12-12
p 5296 1800 200 768 -1 file:dcAbortCad.sch
p 5568 1752 100 0 -1 page:1
p 5680 1752 100 0 -1 pages:1
p 5344 1752 100 0 -1 revision:0
p 5296 1864 150 768 -1 system:Gemini GNIRS DC
[comments]
