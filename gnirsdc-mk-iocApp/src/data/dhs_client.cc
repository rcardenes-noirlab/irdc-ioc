/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 * $Id
 * GNIRS PROJECT
 * 
 * FILENAME 
 *   dhs_client.cc
 * 
 * GENERAL DESCRIPTION
 *   Implementation of dhs_client class.
 *
 * ORIGINAL AUTHOR: 
 *   Peter Young
 *
 * HISTORY
 *
 *  April 2003 - PJY Created.
 *
 *INDENT-ON
 *
 * $Log: dhs_client.cc,v $
 * Revision 1.6  2006/08/17 20:05:04  gemvx
 * - INHERIT keyword now set to F
 * - updated ipc (Thread) src to eliminate a deadlock we were seeing.
 * - updated grating constant to properly handle offsetting.
 * - moved many keywords from extended header to primary
 * - grating luts
 *
 * Revision 1.5  2006/07/12 19:56:05  gemvx
 * - fixed issue with the IS debug mode not being properly setup after boot
 * - updated hallstep record src (before GEM8.7 iocCore would fail to execute)
 *  * disabled some initialization of a not yet loaded database
 *  * disabled updating the db link twice.
 * - updated flexure src so that only have one variable which controls whether flexure should be applied or not.
 * - changed default gimbal backlash to 200 (from 1000)
 * - reshufled header keywords between extended and primary headers
 * - added a script with ability to easily change which dhs is to be used
 *
 * Revision 1.4  2006/04/12 20:31:03  gemvx
 * - added Makefile.Dir and a few cvsignore files in WFS
 * - minor type fix in dhs_client.cc
 *
 * Revision 1.3  2006/03/29 07:03:50  gemvx
 * - changed how dhs/nifs interact
 *
 * Revision 1.2  2006/03/28 19:37:45  gemvx
 * - moved nifsStart to scripts dir
 *
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Gnirs; first gemini-modified release
 *
 * Revision 1.11  2005/09/23 01:48:39  pjy
 * Wrapped dhsConnect and dhsExit inside threads so that they can be deleted if
 * not complete within a timeout period. Prevents class from blocking
 * indefinitely.
 *
 * Revision 1.10  2005/09/09 20:50:15  pjy
 * Added more checks on DHS label - ensure no spaces
 *
 * Revision 1.9  2005/08/18 03:56:29  pjy
 * Added retrieve dataset method (not fully tested)
 *
 * Revision 1.8  2005/08/02 04:52:25  pjy
 * Checks for bad dhs label - requests generated one instead.
 * Init var
 *
 * Revision 1.7  2005/07/28 09:46:46  pjy
 * Removed 0.0 from label
 *
 * Revision 1.6  2005/07/24 12:02:04  pjy
 * Added support for connect cb
 * Removed disconnect and eventloopend before call to dhsExit (it does these calls anyway...)
 *
 * Revision 1.5  2005/07/18 00:42:10  pjy
 * Removed shared_image_buffer
 * Added frames_mem as param
 *
 * Revision 1.4  2005/07/14 06:55:07  pjy
 * Put back gradula dhs exit code.
 *
 * Revision 1.3  2004/12/22 06:13:44  pjy
 * DC data src files ready for first Gemini release
 *
 * Revision 1.2  2004/08/20 02:31:48  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.1  2004/03/19 01:51:43  pjy
 * Added
 *
 *
 -------------------------------------------------------------------------*/
#ifdef vxWorks
#include <vxWorks.h>
#include <stdioLib.h>
#include <sysLib.h>
#include <timers.h>
#endif

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/

#include <time.h>
#include "cicsLib.h"
#include "dhs.h"
#include "configuration.h"
#include "camera.h"
#include "dhs_client.h"
#include "dc.h"

/*---------------------------------------------------------------------------
 External functions
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 External variables
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/
const int DHS_TASK_PRIORITY = 100;               //+ VxWorks priority to run dhs command task
const int DHS_TASK_STACK = 0x200000;             //+ Size of dhs task stack

/*--------------------------------------------------------------------------
 Static variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Global variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Class definitions
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Non-member functions
 -------------------------------------------------------------------------*/
void* dhs_command(void *arg);
static DHS_DATA_TYPE bitpix_to_datatype(int bitpix, bool unsigned_data = false);

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: DHS_Client
 * 
 * INVOCATION: DHS_Client(const char* cname, const char* dsname, const char* ip)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > cname - name of DHS connection - this must be an authorised name from this client host
 * > dsname - name of DHS data server (this is not the hostname running the DHS!)
 * > ip - server ip number
 * > ecb - an error callback function
 * > ccb - an connection callback function
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Constructor - initialises a connection to the DHS
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
DHS_Client::DHS_Client(const char* cname, const char* dsname, const char* ip, Camera_Desc *cam, DHS_CB_FN_PTR ecb,
                        DHS_CB_FN_PTR ccb)
{

  this->dhs_connection=DHS_CONNECT_NULL;
  
  strncpy(connection_name, cname, IPC_HOSTLEN);
  strncpy(dhs_data_server_name, dsname, IPC_HOSTLEN);
  strncpy(server_ip, ip, IPC_HOSTLEN);

  try {
    message_write(DEBUG_FULL, "In DHS_Client constructor...");  

    dhs_result = DHS_S_SUCCESS;
    event_loop = false;
    data_set_ok = false;
    camera = cam;

    if (ecb != NULL) 
      error_cb = ecb;
    else
      error_cb = (DHS_CB_FN_PTR) &DHS_Client::error_callback;
          
    if ( ccb != NULL) 
      ccbf = (DHS_CB_FN_PTR) ccb;         
    else          
      ccbf = NULL;      

    // Now initialise the DHS library
    message_write(DEBUG_FULL, "DHS_Client: calling dhsInit...");  		
    dhsInit(connection_name, 16, &dhs_result);
    check_dhs("dhsInit", __FILE__, __LINE__);
  
    // Set up callback functions for errors and puts
    message_write(DEBUG_FULL, "DHS_Client: calling dhsCallbackSet...");  
    dhsCallbackSet(DHS_CBT_ERROR, error_cb, &dhs_result);
    check_dhs("dhsCallbackSet:ErrorCallback", __FILE__, __LINE__);
    
    // Start the eventloop
    message_write(DEBUG_FULL, "DHS_Client: calling dhsEventLoop..."); 
    dhsEventLoop(DHS_ELT_THREADED,  (DHS_THREAD *) NULL, &dhs_result);
    check_dhs("dhsEventLoop", __FILE__, __LINE__);
    event_loop = true;

	connecting = false;
    disconnecting = false;
                
   }
  catch (Error& dce) {
    message_write(DEBUG_FULL, dce.record_error(__FILE__,__LINE__)); 
    //dhsExit(&dhs_result); // May need to conditionally call this depending on whether hung in dhsConnect
    dce.rethrow();
  }
  
  message_write(DEBUG_FULL, "DHS_Client: constructor done...dhs_result=%d", dhs_result);  

}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: check_dhs
 * 
 * INVOCATION: check_dhs(const char* label, const char* fname, int lineno)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > label - text to prefix message with
 * > fname - name of file calling this method
 * > lineno - linue number of call  
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Checks DHS status value and throws exception if error
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::check_dhs(const char* label, const char* fname, int lineno)
{
  ostrstream ostr;
  if (dhs_result != DHS_S_SUCCESS) {
    ostr << "'" << label << "' error, DHS status=" << dhs_result << ends;
    throw Error(ostr, E_ERROR, dhs_result, fname, lineno);
  }
}
void DHS_Client::check_dhs(ostrstream& label, const char* fname, int lineno)
{
  char msg[MSGLEN];
  strncpy(msg, label.str(), MSGLEN);
  label.rdbuf()->freeze(0);
  check_dhs(msg, fname, lineno);
}



/*+--------------------------------------------------------------------------
 * FUNCTION NAME: dhs_connect
 * 
 * INVOCATION: dhs_connect()
 * 
 * PARAMETERS: 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Connects to the dhs server
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::dhs_connect()
{

  Thread* dhs_cmd = NULL;       // Thread used to run OBSERVE command
  const int DHS_CONNECT_TIMEOUT = HALF_SECOND*36; // Allow 18s to do a DHS connection. 
  int timeout;
  
  try {
  
	if (dhsIsConnected(dhs_connection, &dhs_result) != DHS_TRUE) {
	 message_write(DEBUG_FULL, "DHS_Client: trying to connect but already connected...");  
    }  
  
	// Might need to check if we're already connected here...  
    connecting = true;

    // Start a new thread so that this thread can listen for new commands.
    message_write(DEBUG_MIN, "Starting Thread for the dhsConnect command!");            
#ifdef _POSIX_PTHREAD_SEMANTICS
    dhs_cmd = new Thread("dhs_cmd", dhs_command, (void*)this, true);
#elif vxWorks
    dhs_cmd = new Thread("dhs_cmd", dhs_command, (void*)this, true, DHS_TASK_PRIORITY, 
                         DHS_TASK_STACK);
#endif
    
    // Now wait just a little while for the connection to complete
    timeout = 0;
    while ((timeout < DHS_CONNECT_TIMEOUT) && connecting) {
      rest(HALF_SECOND);
      timeout += HALF_SECOND;
    }
    
    if (connecting) {
      message_write(ALWAYS_LOG, "Failed to connect to the DHS in %d s?", DHS_CONNECT_TIMEOUT/1000);
#ifdef vxWorks
      taskDelete(dhs_cmd->tid);
#endif
      delete dhs_cmd;
      throw Error("DHS not connected!", E_ERROR, dhs_result, __FILE__, __LINE__);
     }
    delete dhs_cmd;

    // Make sure this connection is valid
    if (dhsIsConnected(dhs_connection, &dhs_result) != DHS_TRUE) {
      check_dhs("dhsIsConnected", __FILE__, __LINE__);
      throw Error("DHS not connected!", E_ERROR, dhs_result, __FILE__, __LINE__);
    }

    if (ccbf != NULL) {
      // Set up callback functions for the connection
      message_write(DEBUG_FULL, "DHS_Client: calling dhsCallbackSet for connection status...");  
      dhsCallbackSet(DHS_CBT_CONNECT, ccbf, &dhs_result);
      check_dhs("dhsCallbackSet:ConnectionCallback", __FILE__, __LINE__);
    }
  
   }
  catch (Error& dce) {
    message_write(DEBUG_FULL, dce.record_error(__FILE__,__LINE__)); 
    
	//for now lets not call this...
	//dhsExit(&dhs_result); // May need to conditionally call this depending on whether hung in dhsConnect
    
	dce.rethrow();
  }
  
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: dhs_disconnect
 * 
 * INVOCATION: dhs_connect()
 * 
 * PARAMETERS: 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Connects to the dhs server
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::dhs_disconnect()
{
  Thread* dhs_cmd = NULL;       
  const int DHS_DISCONNECT_TIMEOUT = HALF_SECOND*10;
  int timeout;

  message_write(DEBUG_FULL, "dhs_disconnect()");  
  if (data_set_ok) {
    dhs_result = DHS_S_SUCCESS;
    dhsBdDsFree(data_set, &dhs_result);
    if (dhs_result != DHS_S_SUCCESS) {
      message_write(ALWAYS_LOG,"dhs_disconnect: DHS error during dhsBdDsFree = %d", dhs_result);
    }    
  }
  
  dhs_result = DHS_S_SUCCESS;

  disconnecting = true;  
  connecting = false;

  // Start a new thread so that this thread can listen for new commands.
  message_write(DEBUG_MIN, "Starting Thread for the dhsExit command!");         
#ifdef _POSIX_PTHREAD_SEMANTICS
  dhs_cmd = new Thread("dhs_cmd", dhs_command, (void*)this, true);
#elif vxWorks
  dhs_cmd = new Thread("dhs_cmd", dhs_command, (void*)this, true, DHS_TASK_PRIORITY, 
                       DHS_TASK_STACK);
#endif

  // Now wait just a little while for the disconnect to complete
  timeout = 0;
  while ((timeout < DHS_DISCONNECT_TIMEOUT) && disconnecting) {
    rest(HALF_SECOND);
    timeout += HALF_SECOND;
  }

  if (disconnecting) {
    message_write(ALWAYS_LOG, "Failed to cleanly disconnect from DHS?");
#ifdef vxWorks
    taskDelete(dhs_cmd->tid);
#endif
  }
    
  delete dhs_cmd;
  dhs_cmd = NULL;
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME:  error_callback
 * 
 * INVOCATION: error_callback(DHS_CONNECT     connect,
 *                            DHS_STATUS      errorNum,
 *                            DHS_ERR_LEVEL   errorLev,
 *                            char *          msg,     
 *                            DHS_TAG         tag,     
 *                            void *          userData)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Callback for DHS to use on error
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::error_callback(DHS_CONNECT     connect,
                                DHS_STATUS      errorNum,
                                DHS_ERR_LEVEL   errorLev,
                                char *          msg,     
                                DHS_TAG         tag,     
                                void *          userData)
{
  switch (errorLev) {
  case DHS_EL_SEVERE:
    message_write (ALWAYS_LOG, "DHS_Client: DHS SEVERE Error: connection=%d errNum=%d \"%s\"",
                   (int) connect, (int) errorNum, msg);
    break;
  case DHS_EL_ERROR:
    message_write (ALWAYS_LOG, "DHS_Client: DHS Error: connection=%d errNum=%d \"%s\"",
                   (int) connect, (int) errorNum, msg);
    break;
  case DHS_EL_WARNING:
    message_write (ALWAYS_LOG, "DHS_Client: DHS Warning: connection=%d errNum=%d \"%s\"",
                   (int) connect, (int) errorNum, msg);
    break;
  case DHS_EL_INFO:
    message_write (DEBUG_FULL, "DHS_Client: DHS Info: connection=%d errNum=%d \"%s\"",
                   (int) connect, (int) errorNum, msg);
    break;
  default:
    break;
  }
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: setup_hdr_dataset
 * 
 * INVOCATION: setup_hdr_dataset(const char* instrument_name, DHS_BD_LIFETIME ltime, const char* contrib, 
 *                              const char* dlabel, int nqls, char** qls, Detector_Image_Main_Header& mh,
 *                              char* shared_frames_mem)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > instrument_name - name of instrument in use
 * > ltime - time to keep the dataset
 * > contrib - name of data contributor
 * > dlabel - data label provided (could be empty - in which case we fetch one from DHS)
 * > nqls - number of quick look displays
 * > qls - name of each qls
 * > mh - header data to use for setting up DHS attributes
 * > shared_frames_mem - frames memory location
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Sets up a new DHS dataset
 * 
 * DESCRIPTION: 
 * 
 * Creates a new DHS data set for the named instrument.
 * This follows the Gemini prescribed procedure for creating a dataset:
 * . get a new data set label for the connection
 * . set the data set lifetime
 * . set the contributor attribute
 * . set the quick look display attributes
 * . ask the DHS to create the dataset
 * . set the instrument name attribute
 * . set up a group of standard headers for the PHU
 * . set up frame informantion and standard headers for the IHUs
 *
 * pgroszko, july 10th, 2006: This method has been updated, and the saving of header keywords has been reshufled. 
 * This mosly invloved saving many to a PHU (and not the extended header as before). This was done on a request from the
 * astronomer, and although fine when only have one extension, might need to be refined if more extensions were ever 
 * introduced.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::setup_hdr_dataset(const char* inst, DHS_BD_LIFETIME ltime, const char* contrib, const char* dlabel, 
                                   int nqls, char** qls, Detector_Image_Main_Header& mh, char* shared_frames_mem)
{
  int i,j,c,chipi,ampi,xo,yo,xdir,ydir,f,sf,dhs_f;// indices/counters/temp vars
  Nametype name;                                  // used to build attribute values
  void *junk;                                     // Pointer to dummy storage area for IHU
  long origin[2] = {1,1};                         // Origin needed for each IHU frame                             
  int n_chips, n_amps;                            // counters
  unsigned long dims[2];                          // Array to contain data dimensions
  unsigned long axis_size[2];                     // Array to contain full frame size
  Frame_Data *this_frame;                         // Ptr to frame currently being processed
  Sub_Frame_Data *this_subframe;                  // Ptr to subframe currently being processed
  Mosaic_Mode mm = MOSAIC_NONE;                   // Mosaicing mode for output

  // The DC code delivers the data frame in the first frame - switch it to
  // last so that QLT can display (it can only display last frame)
  // NB - Remove this code when the DHS QL Tool is fixed!
  bool swap_first_last;                           

  try {
    message_write(DEBUG_FULL, "DHS_Client: In setup_dataset..."); 

    dhs_result = DHS_S_SUCCESS;
    data_set_ok = false;

    // Make sure this connection is valid
    if (dhsIsConnected(dhs_connection, &dhs_result) != DHS_TRUE) {
      check_dhs("dhsIsConnected", __FILE__, __LINE__);
      throw Error("DHS not connected!", E_ERROR, dhs_result, __FILE__, __LINE__);
    }

    // Copy the main data structures
    strncpy(instrument_name, inst, NAMELEN-1);

    // Copy the main header structure to local memory. The frames data structures also
    // need copying from the shared detector image memory
    copy_frame_references(mh, main_header, shared_frames_mem, frames_mem, true);
    
    // setup all the required control information for this data set
    setup_control_information(ltime, contrib, dlabel, nqls, qls);

    // Create a new dataset
    data_set = dhsBdDsNew(&dhs_result);
    check_dhs("dhsBdDsNew", __FILE__, __LINE__);
    data_set_ok = true;
    
    message_write(DEBUG_FULL, "DHS_Client: Created a new dataset...%d", data_set);  

    // Fill in the standard data_set attributes
    add_attribute(data_set, "instrument", DHS_DT_STRING, 0, NULL, instrument_name);

    // Full detector size
    sprintf(name,"[1:%ld,1:%ld]", main_header.camera_width, main_header.camera_height);
    add_attribute(data_set, "DETSIZE", DHS_DT_STRING, 0, NULL, name);

    // Count the number of chips and amps
    if (camera != NULL) {
      mm = camera->mosaic_mode;
      n_chips = n_amps = 0;
      for (i=0; i<camera->n_controllers; i++) {
        n_chips += camera->controller[i].n_chips;
        for (j=0; j<camera->controller[i].n_chips; j++) 
          n_amps += camera->controller[i].chip[j].n_amps;
      }
      
      add_attribute(data_set, "DETECTOR", DHS_DT_STRING, 0, NULL, camera->name); 
      add_attribute(data_set, "NCCDS", DHS_DT_INT32, 0, NULL, n_chips);
      add_attribute(data_set, "NAMPS", DHS_DT_INT32, 0, NULL, n_amps);
    }
    
    // Create DHS frames for each of the image frames - these contain no data
    // but do contain hdrs for the IHUs
    nframes = main_header.frames.frames_len;
    if (nframes > 0) {
      add_attribute(data_set, "NEXTEND", DHS_DT_INT32, 0, NULL, main_header.frames.frames_len);
    
      swap_first_last = (nframes > 1);

      for (f=0; f<(int)nframes; f++) {
        // Following call copied out of the GMOS DC code from Richard Wolfe/Steven Beard
        // I don't really understand this, other than I realise that these header
        // frames cannot be created without allocating some space.
        this_frame = &main_header.frames.frames_val[f];

        // The following expression swaps first and last frames if required
        // Because the science data will alwasy be in first frame but the QLT can
        // only display the last frame.
        dhs_f = (swap_first_last)? ((f==0)? nframes-1: ((f==nframes-1)? 0: f)): f;

        dims[0] = 1;
        dims[1] = this_frame->height;
        snprintf(name, NAMELEN, "%s_Frame%d", instrument_name, dhs_f+1);
        frame_data[dhs_f] = dhsBdFrameNew(data_set, name, dhs_f, bitpix_to_datatype(this_frame->image_info.bitpix),
                                     2, dims, (const void **) &junk, &dhs_result);

        // Now add `the basic IHU information 
        add_attribute(frame_data[dhs_f], "INHERIT", DHS_DT_BOOLEAN, 0, NULL, FALSE);
        switch (this_frame->image_info.data_type) {
        case RAW_DATA:
          add_attribute(frame_data[dhs_f], "dataType", DHS_DT_STRING, 0, NULL, "Intensity");
          break;
        case REF_DATA:
          add_attribute(frame_data[dhs_f], "dataType", DHS_DT_STRING, 0, NULL, "Reference");
          break;
        case QUALITY_DATA:
          add_attribute(frame_data[dhs_f], "dataType", DHS_DT_STRING, 0, NULL, "Quality");
          break;
        case VARIANCE_DATA:
          add_attribute(frame_data[dhs_f], "dataType", DHS_DT_STRING, 0, NULL, "Variance");
          break;
        default:
          ;
        }         
        
        dims[0] = 2;
        axis_size[0] = this_frame->width;
        axis_size[1] = this_frame->height;
        add_attribute(frame_data[dhs_f], "axisSize", DHS_DT_UINT32, 1, dims, axis_size);
        add_attribute(frame_data[dhs_f], "origin", DHS_DT_INT32, 1, dims, origin);      

        for (sf=0; sf<(int)this_frame->subframes.subframes_len; sf++) {
                   this_subframe = &this_frame->subframes.subframes_val[sf];

                   // Get the controller,chip and amp indices 
                   c = this_subframe->controller_idx;
                   chipi = this_subframe->chip_idx;
                   ampi = this_subframe->amp_idx;         

                   // Standard IHU FITS headers - first some names
                   if (camera != NULL ) {
                 if ((mm==MOSAIC_CHIP) || (sf==0)) {
                   add_attribute(data_set, get_attr_name("CCDNAME",sf,mm), DHS_DT_STRING, 0, NULL, 
                                 camera->controller[c].chip[chipi].name);

                   // The logical unbinned size of the CCD in section notation.  Normally
                   // this would be the physical size of the CCD unless drift scanning
                   // is done.  This is the full size even when subraster readouts are
                   // done.
                   sprintf(name,"[1:%d,1:%d]",camera->controller[c].chip[chipi].cols,
                           camera->controller[c].chip[chipi].rows);   
                   add_attribute(data_set, get_attr_name("CCDSIZE",sf,mm), DHS_DT_STRING, 0, NULL, name);

                   // CCD on-chip summing given as two or four integer numbers.  These define
                   // the summing of CCD pixels in the amplifier readout order.  The first
                   // two numbers give the number of pixels summed in the serial and parallel
                   // directions respectively.  
                   add_attribute(data_set, get_attr_name("CCDSUM",sf,mm), DHS_DT_STRING, 0, NULL, "1 1");      

                   // CCDSEC is the unbinned section of the logical CCD pixel raster covered by the
                   // amplifier readout in section notation.  The section must map directly
                   // to the specified data section through the binning and CCD to
                   // image coordiante transformation.  The image data section (DATASEC)
                   // is specified with the starting pixel less than the ending pixel.
                   // Thus the order of this section may be flipped depending on the
                   // coordinate transformation (which depends on how the CCD coordinate
                   // system is defined).
                   sprintf(name, "[%ld:%ld,%ld:%ld]", this_subframe->xo+1,
                           this_subframe->xo + this_subframe->width_unbinned,
                           this_subframe->yo + 1,
                           this_subframe->yo + this_subframe->height_unbinned);   
                   add_attribute(data_set, get_attr_name("CCDSEC",sf,mm), DHS_DT_STRING, 0, NULL, name);
                 }


                 // AMP based keywords - always write these, because this is the finest level of detail
                 add_attribute(data_set, get_attr_name("AMPNAME",sf,mm), DHS_DT_STRING, 0, NULL, 
                                   camera->controller[c].chip[chipi].amp[ampi].name);  

                 // Mapping of the CCD section to amplifier coordinates.
                 xdir = camera->controller[c].chip[chipi].amp[ampi].xdir;
                 ydir = camera->controller[c].chip[chipi].amp[ampi].ydir;
                 xo = camera->controller[c].chip[chipi].amp[ampi].xo + xdir*this_subframe->xo;
                 yo = camera->controller[c].chip[chipi].amp[ampi].yo + ydir*this_subframe->yo;
                 sprintf(name, "[%d:%ld,%d:%ld]", xo+1, xo+((this_subframe->width-1)*xdir)+1, yo+1, 
                         yo+((this_subframe->height-1)*ydir)+1);   
                 add_attribute(data_set, get_attr_name("AMPSEC",sf,mm), DHS_DT_STRING, 0, NULL, name);   
                 
				 if (sf == 0 && f == 0) {
                   // Mapping of the CCD section to detector coordinates. 
                   xo = this_frame->camera_xo;
                   yo = this_frame->camera_yo;
                   sprintf(name,"[%d:%ld,%d:%ld]", xo+1, xo+this_frame->width_unbinned, yo+1, yo+this_frame->height_unbinned);
                   add_attribute(data_set, get_attr_name("DETSEC",sf,mm), DHS_DT_STRING, 0, NULL, name);				 
				 
				 }								 
                 if (sf == 0) {
                   //Mapping of the CCD section to image coordinates (binned values, though only 1,1 supported so far)
                   sprintf(name, "[%d:%ld,%d:%ld]", 1, this_frame->data_width_unbinned, 1, this_frame->data_height_unbinned);
                   add_attribute(frame_data[dhs_f], get_attr_name("DATASEC",sf,mm), DHS_DT_STRING, 0, NULL, name);
                 }
          }
        }
      }
    }
  }
  catch (Error& dce) {
    DHS_STATUS dhs_err;
    DHS_ERR_LEVEL dhs_errLev;
    message_write (ALWAYS_LOG, "DHS_Client: setup_dataset failure: %s", dce.record_error(__FILE__, __LINE__));
    message_write (ALWAYS_LOG, "DHS_Client: DHS msg = '%s'", dhsMessage(&dhs_err, &dhs_errLev, &dhs_result));

    if (data_set_ok) {
      dhsBdDsFree(data_set, &dhs_result);    
      data_set_ok = false;
    }
    dce.rethrow();
  }
}


/*+--------------------------------------------------------------------------
 * FUNCTION NAME: setup_hdr_dataset
 * 
 * INVOCATION: setup_hdr_dataset(const char* instrument_name, DHS_BD_LIFETIME ltime, const char* contrib, 
 *                              const char* dlabel, int nqls, char** qls, Detector_Image_Main_Header& mh,
 *                              char* shared_frames_mem)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > instrument_name - name of instrument in use
 * > ltime - time to keep the dataset
 * > contrib - name of data contributor
 * > dlabel - data label provided (could be empty - in which case we fetch one from DHS)
 * > nqls - number of quick look displays
 * > qls - name of each qls
 * > mh - header data to use for setting up DHS attributes
 * > shared_frames_mem - frames memory location
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Sets up a new DHS dataset
 * 
 * DESCRIPTION: 
 * 
 * Creates a new DHS data set for the named instrument.
 * This follows the Gemini prescribed procedure for creating a dataset:
 * . get a new data set label for the connection
 * . set the data set lifetime
 * . set the contributor attribute
 * . set the quick look display attributes
 * . ask the DHS to create the dataset
 * . set the instrument name attribute
 * . set up a group of standard headers for the PHU
 * . set up frame informantion and standard headers for the IHUs
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::setup_hdr_datasetOLD(const char* inst, DHS_BD_LIFETIME ltime, const char* contrib, const char* dlabel, 
                                   int nqls, char** qls, Detector_Image_Main_Header& mh, char* shared_frames_mem)
{
  int i,j,c,chipi,ampi,xo,yo,xdir,ydir,f,sf,dhs_f;// indices/counters/temp vars
  Nametype name;                                  // used to build attribute values
  void *junk;                                     // Pointer to dummy storage area for IHU
  long origin[2] = {1,1};                         // Origin needed for each IHU frame                             
  int n_chips, n_amps;                            // counters
  unsigned long dims[2];                          // Array to contain data dimensions
  unsigned long axis_size[2];                     // Array to contain full frame size
  Frame_Data *this_frame;                         // Ptr to frame currently being processed
  Sub_Frame_Data *this_subframe;                  // Ptr to subframe currently being processed
  Mosaic_Mode mm = MOSAIC_NONE;                   // Mosaicing mode for output

  // The DC code delivers the data frame in the first frame - switch it to
  // last so that QLT can display (it can only display last frame)
  // NB - Remove this code when the DHS QL Tool is fixed!
  bool swap_first_last;                           

  try {
    message_write(DEBUG_FULL, "DHS_Client: In setup_dataset..."); 

    dhs_result = DHS_S_SUCCESS;
    data_set_ok = false;

    // Make sure this connection is valid
    if (dhsIsConnected(dhs_connection, &dhs_result) != DHS_TRUE) {
      check_dhs("dhsIsConnected", __FILE__, __LINE__);
      throw Error("DHS not connected!", E_ERROR, dhs_result, __FILE__, __LINE__);
    }

    // Copy the main data structures
    strncpy(instrument_name, inst, NAMELEN-1);

    // Copy the main header structure to local memory. The frames data structures also
    // need copying from the shared detector image memory
    copy_frame_references(mh, main_header, shared_frames_mem, frames_mem, true);
    
    // setup all the required control information for this data set
    setup_control_information(ltime, contrib, dlabel, nqls, qls);

    // Create a new dataset
    data_set = dhsBdDsNew(&dhs_result);
    check_dhs("dhsBdDsNew", __FILE__, __LINE__);
    data_set_ok = true;
    
    message_write(DEBUG_FULL, "DHS_Client: Created a new dataset...%d", data_set);  

    // Fill in the standard data_set attributes
    add_attribute(data_set, "instrument", DHS_DT_STRING, 0, NULL, instrument_name);

    // Full detector size
    sprintf(name,"[1:%ld,1:%ld]", main_header.camera_width, main_header.camera_height);
    add_attribute(data_set, "DETSIZE", DHS_DT_STRING, 0, NULL, name);

    // Count the number of chips and amps
    if (camera != NULL) {
      mm = camera->mosaic_mode;
      n_chips = n_amps = 0;
      for (i=0; i<camera->n_controllers; i++) {
        n_chips += camera->controller[i].n_chips;
        for (j=0; j<camera->controller[i].n_chips; j++) 
          n_amps += camera->controller[i].chip[j].n_amps;
      }
      
      add_attribute(data_set, "DETECTOR", DHS_DT_STRING, 0, NULL, camera->name); 
      add_attribute(data_set, "NCCDS", DHS_DT_INT32, 0, NULL, n_chips);
      add_attribute(data_set, "NAMPS", DHS_DT_INT32, 0, NULL, n_amps);
    }
    
    // Create DHS frames for each of the image frames - these contain no data
    // but do contain hdrs for the IHUs
    nframes = main_header.frames.frames_len;
    if (nframes > 0) {
      add_attribute(data_set, "NEXTEND", DHS_DT_INT32, 0, NULL, main_header.frames.frames_len);
    
      swap_first_last = (nframes > 1);

      for (f=0; f<(int)nframes; f++) {
        // Following call copied out of the GMOS DC code from Richard Wolfe/Steven Beard
        // I don't really understand this, other than I realise that these header
        // frames cannot be created without allocating some space.
        this_frame = &main_header.frames.frames_val[f];

        // The following expression swaps first and last frames if required
        // Because the science data will alwasy be in first frame but the QLT can
        // only display the last frame.
        dhs_f = (swap_first_last)? ((f==0)? nframes-1: ((f==nframes-1)? 0: f)): f;

        dims[0] = 1;
        dims[1] = this_frame->height;
        snprintf(name, NAMELEN, "%s_Frame%d", instrument_name, dhs_f+1);
        frame_data[dhs_f] = dhsBdFrameNew(data_set, name, dhs_f, bitpix_to_datatype(this_frame->image_info.bitpix),
                                     2, dims, (const void **) &junk, &dhs_result);

        // Now add `the basic IHU information 
        add_attribute(frame_data[dhs_f], "INHERIT", DHS_DT_BOOLEAN, 0, NULL, FALSE);
        switch (this_frame->image_info.data_type) {
        case RAW_DATA:
          add_attribute(frame_data[dhs_f], "dataType", DHS_DT_STRING, 0, NULL, "Intensity");
          break;
        case REF_DATA:
          add_attribute(frame_data[dhs_f], "dataType", DHS_DT_STRING, 0, NULL, "Reference");
          break;
        case QUALITY_DATA:
          add_attribute(frame_data[dhs_f], "dataType", DHS_DT_STRING, 0, NULL, "Quality");
          break;
        case VARIANCE_DATA:
          add_attribute(frame_data[dhs_f], "dataType", DHS_DT_STRING, 0, NULL, "Variance");
          break;
        default:
          ;
        }         
        
        dims[0] = 2;
        axis_size[0] = this_frame->width;
        axis_size[1] = this_frame->height;
        add_attribute(frame_data[dhs_f], "axisSize", DHS_DT_UINT32, 1, dims, axis_size);
        add_attribute(frame_data[dhs_f], "origin", DHS_DT_INT32, 1, dims, origin);      

        for (sf=0; sf<(int)this_frame->subframes.subframes_len; sf++) {
                   this_subframe = &this_frame->subframes.subframes_val[sf];

                   // Get the controller,chip and amp indices 
                   c = this_subframe->controller_idx;
                   chipi = this_subframe->chip_idx;
                   ampi = this_subframe->amp_idx;         

                   // Standard IHU FITS headers - first some names
                   if (camera != NULL ) {
                 if ((mm==MOSAIC_CHIP) || (sf==0)) {
                   add_attribute(frame_data[dhs_f], get_attr_name("CCDNAME",sf,mm), DHS_DT_STRING, 0, NULL, 
                                 camera->controller[c].chip[chipi].name);

                   // The logical unbinned size of the CCD in section notation.  Normally
                   // this would be the physical size of the CCD unless drift scanning
                   // is done.  This is the full size even when subraster readouts are
                   // done.
                   sprintf(name,"[1:%d,1:%d]",camera->controller[c].chip[chipi].cols,
                           camera->controller[c].chip[chipi].rows);   
                   add_attribute(frame_data[dhs_f], get_attr_name("CCDSIZE",sf,mm), DHS_DT_STRING, 0, NULL, name);

                   // CCD on-chip summing given as two or four integer numbers.  These define
                   // the summing of CCD pixels in the amplifier readout order.  The first
                   // two numbers give the number of pixels summed in the serial and parallel
                   // directions respectively.  
                   add_attribute(frame_data[dhs_f], get_attr_name("CCDSUM",sf,mm), DHS_DT_STRING, 0, NULL, "1 1");      

                   // CCDSEC is the unbinned section of the logical CCD pixel raster covered by the
                   // amplifier readout in section notation.  The section must map directly
                   // to the specified data section through the binning and CCD to
                   // image coordiante transformation.  The image data section (DATASEC)
                   // is specified with the starting pixel less than the ending pixel.
                   // Thus the order of this section may be flipped depending on the
                   // coordinate transformation (which depends on how the CCD coordinate
                   // system is defined).
                   sprintf(name, "[%ld:%ld,%ld:%ld]", this_subframe->xo+1,
                           this_subframe->xo + this_subframe->width_unbinned,
                           this_subframe->yo + 1,
                           this_subframe->yo + this_subframe->height_unbinned);   
                   add_attribute(frame_data[dhs_f], get_attr_name("CCDSEC",sf,mm), DHS_DT_STRING, 0, NULL, name);
                 }


                 // AMP based keywords - always write these, because this is the finest level of detail
                 add_attribute(frame_data[dhs_f], get_attr_name("AMPNAME",sf,mm), DHS_DT_STRING, 0, NULL, 
                                   camera->controller[c].chip[chipi].amp[ampi].name);  

                 // Mapping of the CCD section to amplifier coordinates.
                 xdir = camera->controller[c].chip[chipi].amp[ampi].xdir;
                 ydir = camera->controller[c].chip[chipi].amp[ampi].ydir;
                 xo = camera->controller[c].chip[chipi].amp[ampi].xo + xdir*this_subframe->xo;
                 yo = camera->controller[c].chip[chipi].amp[ampi].yo + ydir*this_subframe->yo;
                 sprintf(name, "[%d:%ld,%d:%ld]", xo+1, xo+((this_subframe->width-1)*xdir)+1, yo+1, 
                         yo+((this_subframe->height-1)*ydir)+1);   
                 add_attribute(frame_data[dhs_f], get_attr_name("AMPSEC",sf,mm), DHS_DT_STRING, 0, NULL, name);   

                 // Frame based keywords - one per FITS extension
                 if (sf == 0) {
                   // Mapping of the CCD section to detector coordinates. 
                   xo = this_frame->camera_xo;
                   yo = this_frame->camera_yo;
                   sprintf(name,"[%d:%ld,%d:%ld]", xo+1, xo+this_frame->width_unbinned, yo+1, yo+this_frame->height_unbinned);
                   add_attribute(frame_data[dhs_f], get_attr_name("DETSEC",sf,mm), DHS_DT_STRING, 0, NULL, name);

                   //Mapping of the CCD section to image coordinates (binned values, though only 1,1 supported so far)
                   sprintf(name, "[%d:%ld,%d:%ld]", 1, this_frame->data_width_unbinned, 1, this_frame->data_height_unbinned);
                   add_attribute(frame_data[dhs_f], get_attr_name("DATASEC",sf,mm), DHS_DT_STRING, 0, NULL, name);
                 }
          }
        }
      }
    }
  }
  catch (Error& dce) {
    DHS_STATUS dhs_err;
    DHS_ERR_LEVEL dhs_errLev;
    message_write (ALWAYS_LOG, "DHS_Client: setup_dataset failure: %s", dce.record_error(__FILE__, __LINE__));
    message_write (ALWAYS_LOG, "DHS_Client: DHS msg = '%s'", dhsMessage(&dhs_err, &dhs_errLev, &dhs_result));

    if (data_set_ok) {
      dhsBdDsFree(data_set, &dhs_result);    
      data_set_ok = false;
    }
    dce.rethrow();
  }
}





/*+--------------------------------------------------------------------------
 * FUNCTION NAME: setup_control_information
 * 
 * INVOCATION: setup_control_information(DHS_BD_LIFETIME ltime, const char* contrib, const char* dlabel, int nqls, 
 *                                       char** qls)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > ltime - time to keep the dataset
 * > contrib - name of data contributor
 * > dlabel - data label provided (could be empty - in which case we fetch one from DHS)
 * > nqls - number of quick look displays
 * > qls - name of each qls
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Sets up control information for a DHS dataset
 * 
 * DESCRIPTION: 
 * 
 * Sets up control information for a new DHS data set for the named instrument.
 * This follows the Gemini prescribed procedure for setting up control
 * information:

 * . get a new data set label for the connection
 * . set the data set lifetime
 * . set the contributor attribute
 * . set the quick look display attributes
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::setup_control_information(DHS_BD_LIFETIME ltime, const char* contrib, const char* dlabel, 
                                           int nqls, char** qls)
{  
  ostrstream ostr;                      // string buffer
  string label_str;                     // temporary label variable
  char* dhslabel;                       // dataset label
  int i;                                // counter
  bool user_dsname = false;             // flag indicating whether dataset name provided

  message_write(DEBUG_FULL, "DHS_Client: In setup_control_information..."); 
  
  dhs_result = DHS_S_SUCCESS;
  
  // Make sure this connection is valid
  if (dhsIsConnected(dhs_connection, &dhs_result) != DHS_TRUE) {
    check_dhs("dhsIsConnected", __FILE__, __LINE__);
    throw Error("DHS not connected!", E_ERROR, dhs_result, __FILE__, __LINE__);
  }

  // Make sure no spaces in label
  label_str = dlabel;
  strip_str(label_str, STRIP_BOTH); // strip leading & trailing space
  
  if (label_str.length() == 0) {
    // Get a unique name for this dataset from the DHS
    dhslabel = dhsBdName(dhs_connection, &dhs_result);
    check_dhs("dhsBdName", __FILE__, __LINE__);
    
    // message_write(DEBUG_FULL, "DHS_Client: Got dataSet label from DHS: %s", dhslabel);  
        //pgroszko
    message_write(DEBUG_MIN, "DHS_Client:setup_control_information: Got dataSet label from DHS: %s", dhslabel);
        
        
    // Fill in the other components of the dataset name
    strncpy(data_label, dhslabel, DHS_DATA_LABEL_LEN-1);
  } else {
        // message_write(DEBUG_FULL, "DHS_Client: Using provided dataSet label: %s", label_str.c_str()); 
        //pgroszko
    message_write(DEBUG_MIN, "DHS_Client:setup_control_information: Using provided dataSet label: %s", label_str.c_str());  
    strncpy(data_label, label_str.c_str(), DHS_DATA_LABEL_LEN-1);
    user_dsname = true;
  }
  
  // Set up the control information for the dataset. Catch any errors here and
  // ask for a generated name if user provided one fails
  try {
    lifetime = ltime;
    
        // dhsBdCtl(dhs_connection, DHS_BD_CTL_LIFETIME, data_label, lifetime, &dhs_result);
    // check_dhs("dhsBdCtl:LIFETIME", __FILE__, __LINE__);              

        //pgroszko... hack.. 
        if (!user_dsname)  {
                dhs_result=DHS_S_SUCCESS;
                message_write(DEBUG_MIN, "DHS_Client:setup_control_information: setting lifetime=%d via dhsBdCtl", lifetime);           
                dhsBdCtl(dhs_connection, DHS_BD_CTL_LIFETIME, data_label, lifetime, &dhs_result);
        check_dhs("dhsBdCtl:LIFETIME", __FILE__, __LINE__);     
        }
  } 
  catch (Error& dce) {
    if (user_dsname) {
         //pgroszko
      message_write(ALWAYS_LOG, "DHS_Client:setup_control_information: Failed to set lifetime for requested label - generating one");    
     // message_write(ALWAYS_LOG, "DHS_Client:setup_control_information: Failed to set lifetime for requested label - generating one"); 
      dhs_result = DHS_S_SUCCESS;
      dhslabel = dhsBdName(dhs_connection, &dhs_result);
      check_dhs("dhsBdName", __FILE__, __LINE__);
      //message_write(DEBUG_FULL, "DHS_Client: Got dataSet label from DHS: %s", dhslabel);  
          //pgroszko
                message_write(DEBUG_MIN, "DHS_Client:setup_control_information Got dataSet label from DHS: %s", dhslabel);        
      strncpy(data_label, dhslabel, DHS_DATA_LABEL_LEN-1);
      dhsBdCtl(dhs_connection, DHS_BD_CTL_LIFETIME, data_label, lifetime, &dhs_result);
      check_dhs("dhsBdCtl:LIFETIME", __FILE__, __LINE__);
    } else
      dce.rethrow();
  }
  
  //message_write(DEBUG_FULL, "DHS_Client: Set lifetime to %d", lifetime); 
  //pgroszko
  message_write(DEBUG_MIN, "DHS_Client: Set lifetime to %d", lifetime); 
  
        //pgroszko... hack.. should have already been done by the seqexec.
        if (!user_dsname)  {  
                // data contributors - we only allow one
                dhs_contrib[0] = (char*) contrib;
                dhsBdCtl(dhs_connection, DHS_BD_CTL_CONTRIB, data_label, 1, dhs_contrib, &dhs_result);
                check_dhs("dhsBdCtl:CONTRIB", __FILE__, __LINE__);
        }
  
  // Quick look streams - allow more than one
  if (nqls > DHS_CLIENT_MAX_QLS) {
    ostr << "Bad nqls value (" << nqls << ") must be <" << DHS_CLIENT_MAX_QLS << ends;
    throw Error(ostr, E_ERROR, -1, __FILE__, __LINE__);
  }
  
  for (i=0; i<nqls; i++) {
    dhs_qlStreams[i] = qls[i];
    message_write(DEBUG_FULL, "DHS_Client: QlStream %d = %s", i, dhs_qlStreams[i]);  
  }
  
  if (nqls > 0) {
    dhsBdCtl(dhs_connection, DHS_BD_CTL_QLSTREAM, data_label, nqls, dhs_qlStreams, &dhs_result);
    check_dhs("dhsBdCtl:QLSTREAM", __FILE__, __LINE__);
  }
  
  message_write(DEBUG_FULL, "DHS_Client: Done setting up control information...");  
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: DHS_Client::get_attr_name
 * 
 * INVOCATION: name = get_attr_name(char* base_attr_name, int idx, Mosaic_Mode mm)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > base_attr_name - firat part of attribute name
 * > idx - name index to append to base part
 * > mm - mosaic mode used for this output frame
 *
 * FUNCTION VALUE: modified attr_name
 * 
 * PURPOSE: Appends idx to base_attr_name 
 * 
 * DESCRIPTION: 
 * When mosaicing frames of data it is likely there will be multiple attributes
 * for the one frame with same name - eg AMPSIZE for multi-amp readouts. This
 * functions simple modifies the attribute name with the index of the attribute
 * in question (eg amp number) to prevent duplicates and retain information
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
char* DHS_Client::get_attr_name(const char* base_attr_name, int idx, Mosaic_Mode mm)
{
  if ((mm != MOSAIC_NONE) && (idx>0))
    snprintf(attr_name, NAMELEN, "%s%d", base_attr_name, idx+1);
  else
    strncpy(attr_name, base_attr_name, NAMELEN);
  return attr_name;             
}
 
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: DHS_Client::send_dataset
 * 
 * INVOCATION: DHS_Client::send_dataset(DHS_BOOLEAN last)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > last - set to DHS_TRUE when this is the last dataset for this set
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Sends that data_set to the DHS
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::send_dataset(DHS_BOOLEAN last)
{
  DHS_TAG put_tag;                   // tag returned by the put function
  DHS_CMD_STATUS send_status;        // Final status of the put
  char *msg;                         // failure message from server

  try {
    message_write(DEBUG_FULL, "DHS_Client: In send, last=%d", last);

    if (data_set_ok) {

      dhs_result = DHS_S_SUCCESS;
      
      // Make sure this connection is valid
      if (dhsIsConnected(dhs_connection, &dhs_result) != DHS_TRUE) {
        check_dhs("dhsIsConnected", __FILE__, __LINE__);
        throw Error("DHS not connected!", E_ERROR, dhs_result, __FILE__, __LINE__);
      }
      
      // Send the data to the DHS server. If this is the last data for the dataset, the last
      // parameter is set to TRUE.
      if (lifetime == DHS_BD_LT_TRANSIENT)
        put_tag = dhsBdPut(dhs_connection, data_label, DHS_BD_PT_DS_QL, last, data_set, NULL, &dhs_result);
      else
        put_tag = dhsBdPut(dhs_connection, data_label, DHS_BD_PT_DS, last, data_set, NULL, &dhs_result);
      check_dhs("dhsBdPut", __FILE__, __LINE__);
      
      message_write(DEBUG_FULL, "DHS_Client: Sent data to the DHS server for label %s", data_label); 
      
      // Wait for the data to be received
      dhsWait(1, &put_tag, &dhs_result);    
      check_dhs("dhsWait", __FILE__, __LINE__);
      
      send_status = dhsStatus(put_tag, &msg, &dhs_result);    
      check_dhs("dhsStatus",  __FILE__, __LINE__);
      
      if (send_status != DHS_CS_DONE) {
        message_write(DEBUG_FULL, "DHS_Client: DHS put failed: connection=%d label=%s",
                      (int) dhs_connection, data_label);
        if (msg != NULL)
          message_write(DEBUG_NONE, "DHS_Client: DHS put reason: %s", msg);
      } else
        message_write(DEBUG_FULL,"DHS_Client: Data sent OK");        
      
      // Free tag and data
      dhsTagFree(put_tag, &dhs_result);    
      check_dhs("dhsTagFree", __FILE__, __LINE__);
      
      // Free the data set
      dhsBdDsFree(data_set, &dhs_result);    
      data_set_ok = false;
      check_dhs("dhsBdDsFree", __FILE__, __LINE__);   

    } else

        message_write(DEBUG_FULL,"DHS_Client: No data Set to send?");        
      
  }
  catch (Error& dce) {
    dhsTagFree(put_tag, &dhs_result); 
    dce.rethrow();
  }
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: setup_data_chunk
 * 
 * INVOCATION: setup_data_chunk(Detector_Image_Sub_Header* sub_header)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Sets up chunk of data to transfer to the DHS
 * 
 * DESCRIPTION: 
 *
 * Interprets the image sub header structure and sets up a DHS dataset for
 * transfer. Each sub header may require more than one frame of data
 * transferred, this is determined by the frame_mask setting. For each frame
 * required, the dhsBdFrameNew is called so that the data buffers are setup.
 * Data should be placed in these buffers by the calling routine.
 *
 * NB. It is important to do the fllowing so that the DHS can accept frame chunks:
 * . Each dataset requires the "instrument" attribute
 * . The axisZize attribute should match the full frame (not chunk) size
 * . the origin attribute indicates where in the full frame this chunk will be written
 * . the bdFrame size is set in the bdFrameNew call and is the actual size of the chunk
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::setup_data_chunk(Detector_Image_Sub_Header* sub_header)
{
  ostrstream ostr;
  int f,sf,dhs_f;                    // Frame indices and counter
  unsigned long dims[2];             // Array to contain data dimensions
  long origin[2];                    // Array to contain origin position
  Nametype name;                     // used to build attribute values
  unsigned long chunk_axis_size[2];  // Array to contain sub frame size
  unsigned long axis_size[2];        // Array to contain full frame size
  Frame_Data *this_frame;            // Ptr to frame currently being processed
  Sub_Frame_Data *this_subframe;     // Ptr to subframe currently being processed

  // The DC code delivers the data frame in the first frame - switch it to
  // last so that QLT can display (it can only display last frame)
  // NB - Remove this code when the DHS QL Tool is fixed!
  bool swap_first_last;                           

  try {
    
    dhs_result = DHS_S_SUCCESS;

    // Make sure this connection is valid
    if (dhsIsConnected(dhs_connection, &dhs_result) != DHS_TRUE) {
      check_dhs("dhsIsConnected", __FILE__, __LINE__);
      throw Error("DHS not connected!", E_ERROR, dhs_result, __FILE__, __LINE__);
    }
      
    // Create a new chunk dataset
    data_set = dhsBdDsNew(&dhs_result);
    check_dhs("dhsBdDsNew", __FILE__, __LINE__);
    message_write(DEBUG_FULL, "Created a new dataset for chunk, width=%d, height=%d ...",
                  sub_header->width, sub_header->height);
    data_set_ok = true;

    // Always add the instrument keyword to each dataset
    add_attribute(data_set, "instrument", DHS_DT_STRING, 0, NULL, instrument_name);

    // Create frames for each of the detector quadrants
    f = sub_header->frame;
    sf = sub_header->subframe;
    this_frame = &main_header.frames.frames_val[f];
    this_subframe = &this_frame->subframes.subframes_val[sf];
    swap_first_last = (nframes > 1);
 
    // The following expression swaps first and last frames if required
    // Because the science data will alwasy be in first frame but the QLT can
    // only display the last frame.
    dhs_f = (swap_first_last)? ((f==0)? nframes-1: ((f==nframes-1)? 0: f)): f;
        
    // Create the intensity frame - always
    chunk_axis_size[0] = sub_header->width;
    chunk_axis_size[1] = sub_header->height;
    sprintf(name,"%s_Frame%d", instrument_name, dhs_f+1);
    frame_data[f] = dhsBdFrameNew(data_set, name, dhs_f, bitpix_to_datatype(this_frame->image_info.bitpix), 
                                 2, chunk_axis_size, (const void **) &pixel_data[f], &dhs_result);
    check_dhs("dhsBdFrameNew:pixels", __FILE__, __LINE__);
      
    // Fill in the pixel_data attributes
    dims[0] = 2;
    axis_size[0] = this_frame->width;
    axis_size[1] = this_frame->height;
    add_attribute(frame_data[dhs_f], "axisSize", DHS_DT_UINT32, 1, dims, axis_size);
        
    // origin is expressed as a pixel number starting from 1
    origin[0] = sub_header->x + 1;
    origin[1] = sub_header->y + 1;
    add_attribute(frame_data[dhs_f], "origin", DHS_DT_INT32, 1, dims, origin);
        
    message_write(DEBUG_FULL, "Chunk f=%d,sf=%d,xo=%ld,yo=%ld,w=%ld,h=%ld,x=%ld,y=%ld,w=%ld,h=%ld,xd=%d,yd=%d,o=%ld,%ld", 
                  f, sf, this_subframe->frame_xo, this_subframe->frame_yo, this_subframe->width, this_subframe->height, 
                  sub_header->x, sub_header->y, sub_header->width, sub_header->height, this_subframe->xdir, this_subframe->ydir,
                  origin[0], origin[1]);  
  }
  catch (Error& dce) {
    message_write (DEBUG_FULL, "Dhs_Client::setup_data_chunk failure: %s", dce.record_error(__FILE__, __LINE__));
    if (data_set_ok) {
      dhsBdDsFree(data_set, &dhs_result);    
      data_set_ok = false;
    }
    dce.rethrow();
  }
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: setup_end_dataset
 * 
 * INVOCATION: seup_end_dataset()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Sets up a final dataset for closing of a DHS transfer
 * 
 * DESCRIPTION: 
 * 
 * Creates a new DHS data set for the named instrument, so that final headers
 * can be written
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::setup_end_dataset()
{
  ostrstream ostr;                                // string buffer

  try {
    message_write(DEBUG_FULL, "DHS_Client: In setup_end_dataset..."); 

    dhs_result = DHS_S_SUCCESS;

    // Make sure this connection is valid
    if (dhsIsConnected(dhs_connection, &dhs_result) != DHS_TRUE) {
      check_dhs("dhsIsConnected", __FILE__, __LINE__);
      throw Error("DHS not connected!", E_ERROR, dhs_result, __FILE__, __LINE__);
    }

    // Create a new dataset
    data_set = dhsBdDsNew(&dhs_result);
    check_dhs("dhsBdDsNew", __FILE__, __LINE__);
    data_set_ok = true;
    
    message_write(DEBUG_FULL, "DHS_Client: Created a new end dataset...%d", data_set);  

    // Fill in the standard data_set attributes
    add_attribute(data_set, "instrument", DHS_DT_STRING, 0, NULL, instrument_name);

    // Add other final attributes here
  }
  catch (Error& dce) {
    message_write (DEBUG_FULL, "DHS_Client: setup_end_dataset failure: %s", dce.record_error(__FILE__, __LINE__));
    if (data_set_ok) {
      dhsBdDsFree(data_set, &dhs_result);    
      data_set_ok = false;
    }
    dce.rethrow();
  }
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: setup_simple_dataset
 * 
 * INVOCATION: setup_simple_dataset(const char* instrument_name, DHS_BD_LIFETIME ltime, const char* contrib, 
 *                                  const char* dlabel, int nqls, char** qls)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > instrument_name - name of instrument in use
 * > ltime - time to keep the dataset
 * > contrib - name of data contributor
 * > dlabel - data label provided (could be empty - in which case we fetch one from DHS)
 * > nqls - number of quick look displays
 * > qls - name of each qls
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Sets up a simple 1 frame dataset for DHS transfer
 * 
 * DESCRIPTION: 
 * 
 * Creates a new DHS data set for the named instrument.
 * This consists of a single frame of data that is sent in one chunk.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::setup_simple_dataset(const char* inst, DHS_BD_LIFETIME ltime, const char* contrib, const char* dlabel, 
                                      int nqls, char** qls, unsigned long width, unsigned long height, int bitpix)
{
  Nametype name;                        // used to build attribute values
  unsigned long axis_size[2];           // Array to contain full frame size
  unsigned long dims[2];                // Array to contain data dimensions
  long origin[2];                    // Array to contain origin position

  try {
    message_write(DEBUG_FULL, "DHS_Client: In setup_simple_dataset..."); 

    dhs_result = DHS_S_SUCCESS;

    // Make sure this connection is valid
    if (dhsIsConnected(dhs_connection, &dhs_result) != DHS_TRUE) {
      check_dhs("dhsIsConnected", __FILE__, __LINE__);
      throw Error("DHS not connected!", E_ERROR, dhs_result, __FILE__, __LINE__);
    }

    // Copy the main data structures
    strncpy(instrument_name, inst, NAMELEN-1);

    // setup all the required control information for this data set
    setup_control_information(ltime, contrib, dlabel, nqls, qls);   

    // Create a new dataset
    data_set = dhsBdDsNew(&dhs_result);
    check_dhs("dhsBdDsNew", __FILE__, __LINE__);
    data_set_ok = true;
    
    message_write(DEBUG_FULL, "DHS_Client: Created a new simple dataset...%d", data_set);  

    // Fill in the standard data_set attributes
    add_attribute(data_set, "instrument", DHS_DT_STRING, 0, NULL, instrument_name);

    // Add other final attributes here

    // Full frame size
    sprintf(name,"[1:%ld,1:%ld]", width, height);
    
        add_attribute(data_set, "DETSIZE", DHS_DT_STRING, 0, NULL, name);

    // Create the pixel_data frame - always
    axis_size[0] = width;
    axis_size[1] = height;
    sprintf(name,"Frame0");
    frame_data[0] = dhsBdFrameNew(data_set, name, 0, bitpix_to_datatype(bitpix), 
                                 2, axis_size, (const void **) &pixel_data[0], &dhs_result);
    check_dhs("dhsBdFrameNew:pixel_data", __FILE__, __LINE__);
      
    // Fill in the pixel_data attributes
    dims[0] = 2;
    axis_size[0] = width;
    axis_size[1] = height;
    add_attribute(frame_data[0], "axisSize", DHS_DT_UINT32, 1, dims, axis_size);
        
    // origin is expressed as a pixel number starting from 1
    origin[0] = 1;
    origin[1] = 1;
    add_attribute(frame_data[0], "origin", DHS_DT_INT32, 1, dims, origin);
    
    //Mapping of the CCD section to image coordinates (binned values, though only 1,1 supported so far)
//      sprintf(name, "[%d:%ld,%d:%ld]", 1, width, 1, height);
//      add_attribute(frame_data[0], "DATASEC", DHS_DT_STRING, 0, NULL, name);


  }
  catch (Error& dce) {
    message_write (DEBUG_FULL, "DHS_Client: setup_simple_dataset failure: %s", dce.record_error(__FILE__, __LINE__));
    if (data_set_ok) {
      dhsBdDsFree(data_set, &dhs_result);    
      data_set_ok = false;
    }
    dce.rethrow();
  }
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: DHS_Client::retrieve_dataset
 * 
 * INVOCATION: DHS_Client::retrieve_dataset(char* ds_name, DHS_CB_FN_PTR gcb)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > ds_name -  name of the dataset to retrieve
 * > gcb - function pointer to be called by the get callback - takes 2 pars (void*, unsigned long)
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Retrieves a named data_set from the DHS
 * 
 * DESCRIPTION: 
 *
 * Following recipe given in the Gemini ICD 3 - Bulk Data Transfer, this method
 * sends a dhsBdGet call to the DHS to fetch the named dataset.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::retrieve_dataset(char* ds_name, DHS_CB_FN_PTR gcb)
{
  DHS_TAG get_tag;                   // tag returned by the put function
  DHS_CMD_STATUS retrieve_status;    // Final status of the put
  char *msg;                         // failure message from server
  DHS_CB_FN_PTR get_data;
  DHS_CB_FN_PTR get_cb;

  try {
    message_write(DEBUG_FULL, "DHS_Client: In retrieve, dataset=%s", ds_name);

    dhs_result = DHS_S_SUCCESS;
    
    // Make sure this connection is valid
    if (dhsIsConnected(dhs_connection, &dhs_result) != DHS_TRUE) {
      check_dhs("dhsIsConnected", __FILE__, __LINE__);
      throw Error("DHS not connected!", E_ERROR, dhs_result, __FILE__, __LINE__);
    }
    
    if (gcb != NULL) 
      get_data = gcb;
    else
      get_data = (DHS_CB_FN_PTR) &DHS_Client::get_buffer;

    get_cb = (DHS_CB_FN_PTR) &DHS_Client::get_callback;

    // Set up callback functions for errors and puts
    message_write(DEBUG_FULL, "DHS_Client: calling dhsCallbackSet for get...");  
    dhsCallbackSet(DHS_CBT_GET, get_cb, &dhs_result);
    check_dhs("dhsCallbackSet:ErrorCallback", __FILE__, __LINE__);

    // Retrieve the data from the DHS server. 
    message_write(DEBUG_FULL, "DHS_Client: Retrieving data from the DHS server for label %s", ds_name); 
    
    get_tag = dhsBdGet(dhs_connection, ds_name, DHS_BD_GT_RAW, get_data, &dhs_result);
    check_dhs("dhsBdGet", __FILE__, __LINE__);
    message_write(DEBUG_FULL, "DHS_Client: getTag = 0x%x", get_tag); 
    
    sleep(10);

    // Wait for the data to be received
    dhsWait(1, &get_tag, &dhs_result);    
    check_dhs("dhsWait", __FILE__, __LINE__);
    
    retrieve_status = dhsStatus(get_tag, &msg, &dhs_result);    
    check_dhs("dhsStatus",  __FILE__, __LINE__);
    
    if (retrieve_status != DHS_CS_DONE) {
      message_write(DEBUG_FULL, "DHS_Client: DHS gett failed: connection=%d label=%s",
                    (int) dhs_connection, data_label);
      if (msg != NULL)
        message_write(DEBUG_NONE, "DHS_Client: DHS get reason: %s", msg);
    } else
      message_write(DEBUG_FULL,"DHS_Client: Data retrieved OK");        
    
    // Free tag and data
    dhsTagFree(get_tag, &dhs_result);    
    check_dhs("dhsTagFree", __FILE__, __LINE__);
    
  }
  catch (Error& dce) {
    message_write(DEBUG_FULL,"DHS_Client: Problem during DHS get: '%s'", dce.record_error(__FILE__,__LINE__));        
    dhsTagFree(get_tag, &dhs_result); 
    dce.rethrow();
  }
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME:  get_callback
 * 
 * INVOCATION: get_callback(DHS_CONNECT     connect,
 *                          DHS_TAG tag,
 *                          char *label,
 *                          DHS_BD_GET_TYPE type,
 *                          DHS_CMD_STATUS cmdStatus,
 *                          char *cmdStatusStr,
 *                          DHS_AV_LIST avList,
 *                          void* data,
 *                          unsigned long length,
 *                          void * userData)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Callback for DHS to use on get
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void DHS_Client::get_callback(DHS_CONNECT connect, DHS_TAG tag, char *label, DHS_BD_GET_TYPE type, DHS_CMD_STATUS cmdStatus,
                              char *cmdStatusStr, DHS_AV_LIST avList, void* data, unsigned long length, void * userData)
{ 
  DHS_STATUS status = DHS_S_SUCCESS;
  
  if (cmdStatus == DHS_CS_DONE) {
    // Call user supplied routine to handle available data
    ((void(*)(void*,unsigned long))userData)(data, length);
  } else if (cmdStatus == DHS_CS_ERROR) {
    message_write(ALWAYS_LOG, "DHS_Client: Failed to get data for label '%s'", label);
    if (cmdStatusStr != NULL) 
      message_write (ALWAYS_LOG, "DHS_Client: '%s'", cmdStatusStr);
  }
  
  dhsTagFree(tag, &status);
  if (status != DHS_S_SUCCESS) {
    message_write (ALWAYS_LOG, "DHS_Client: Error during dhsTagFree, status=%d", status);
  }
}
void DHS_Client::get_buffer(void *data, unsigned long length)
{
    message_write(ALWAYS_LOG, "DHS_Client: Default userData routine called for DHS Get, buflen=%d", length); 
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: ~DHS_Client
 * 
 * INVOCATION: delete dhs_client
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Destructor - closes DHS connection if one
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
DHS_Client::~DHS_Client()
{     
 	//dhsExit should disconnect all the clients, but at this point the process of calling dhsExit/dhsInit might be a culprit of some
	//problems. Try manually disconnecting before we call dhsExit()

	//Should keep internal state of whether we are (or at least should be) connected or not. This way we could avoid always asking dhs for this info.
    if (dhsIsConnected(dhs_connection, &dhs_result) == DHS_TRUE) {
		this->dhs_disconnect(); 
	}
	//If we've called dhs_disconnect() before this freeing the dataset should not be required.
  	if (data_set_ok) {
    	dhs_result = DHS_S_SUCCESS;
    	dhsBdDsFree(data_set, &dhs_result);
    	if (dhs_result != DHS_S_SUCCESS) {
      	message_write(ALWAYS_LOG,"dhs_disconnect: DHS error during dhsBdDsFree = %d", dhs_result);
    	}    
  	}	
		
	//For now assume dhsExit will return ok; in the long run might need a seperate thread calling.
	message_write(DEBUG_MIN, "DHS_Client: calling dhsExit..., connect state=%d", this->connection_state);  
	dhsExit(&this->dhs_result);
	message_write(DEBUG_MIN, "DHS_Client: dhsExit completed, result=%d", this->dhs_result);  
}


/*+--------------------------------------------------------------------------
 * FUNCTION NAME: dhs_command
 * 
 * INVOCATION: dhs_command(void *arg)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > arg - dhs command argument - expect this to be pointer to DHS_Client object
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Handles dhsConnect and  dhsDisconnect in thread
 * 
 * DESCRIPTION: Handles dhsDisconnect and dhsConnect in thread so that
 * a timeout can be implemented and prevent the application hanging
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void* dhs_command(void *arg)
{
	Thread_Arg* ta = (Thread_Arg*) arg;             // Argument passed to routines started from Thread class
	DHS_Client* dhs_client = (DHS_Client *) ta->arg;    // Pluck out client object from thread arg

	if (dhs_client == NULL)
		return;

  	try {
    if (dhs_client->disconnecting) {
	
  		message_write(DEBUG_MIN, "DHS_Client: calling dhsDisconnect..., connect state=%d", dhs_client->connection_state);  
  		dhsDisconnect(dhs_client->dhs_connection, &dhs_client->dhs_result);   
		dhs_client->check_dhs("dhsConnect", __FILE__, __LINE__);        // Do we need this check?
		
		dhs_client->dhs_connection=DHS_CONNECT_NULL;
		message_write(DEBUG_MIN, "DHS_Client: dhsDisconnect completed, result=%d", dhs_client->dhs_result);                           
 		dhs_client->check_dhs("dhsDisconnect", __FILE__, __LINE__);       
  
		dhs_client->disconnecting = false;
	
    } else if (dhs_client->connecting) {
	
		message_write(DEBUG_MIN, "DHS_Client: calling dhsConnect with IP=%s, data server name=%s", 
                    			dhs_client->server_ip, dhs_client->dhs_data_server_name);  
		dhs_client->dhs_connection = dhsConnect(dhs_client->server_ip, dhs_client->dhs_data_server_name, NULL, 
                                              	&dhs_client->dhs_result);
		dhs_client->check_dhs("dhsConnect", __FILE__, __LINE__);
		message_write(DEBUG_MIN, "DHS_Client: connection made = %d", dhs_client->dhs_connection);
		
		dhs_client->connecting = false;
    }
  }
  catch (Error &dce) {
    message_write(ALWAYS_LOG, "DHS fault - '%s'", dce.record_error(__FILE__,__LINE__));
  }
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: bitpix_to_datatype
 * 
 * INVOCATION: bitpix_to_datatype(int bitpix)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > bitpix - input FITS bitpix value to convert
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Converts a FITS bitpix value to a DHS data type
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
static DHS_DATA_TYPE bitpix_to_datatype(int bitpix, bool unsigned_data)
{
  switch (bitpix) {
  case BYTE_BITPIX:
    return DHS_DT_CHAR;
  case SHORT_BITPIX:
    if (unsigned_data)
      return DHS_DT_UINT16;
    else
      return DHS_DT_INT16;
  case INT_BITPIX:
    if (unsigned_data)
      return DHS_DT_UINT32;
    else
      return DHS_DT_INT32;
  case FLOAT_BITPIX:
    return DHS_DT_FLOAT;
  case DOUBLE_BITPIX:
    return DHS_DT_DOUBLE;
  default:
    return DHS_DT_STRING;
  }
}
    
