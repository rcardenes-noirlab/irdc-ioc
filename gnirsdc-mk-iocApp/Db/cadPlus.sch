[schematic2]
uniq 44
[tools]
[detail]
w 72 -701 100 0 STAT inhier.STAT.P -96 -704 288 -704 288 -608 448 -608 ecalcs.sp.INPB
w 284 -277 100 0 STAT junction 288 -608 288 64 432 64 ecalcs.st.INPB
w 888 -765 100 0 SPID junction 768 -768 1056 -768 outhier.SPID.p
w 552 -509 100 0 SPID ecalcs.sp.VAL 736 -768 768 -768 768 -512 384 -512 384 -576 448 -576 ecalcs.sp.INPA
w 888 -93 100 0 STID junction 768 -96 1056 -96 outhier.STID.p
w 552 171 100 0 STID ecalcs.st.VAL 720 -96 768 -96 768 160 384 160 384 96 432 96 ecalcs.st.INPA
w 188 -757 100 0 SPIN inhier.SPIN.P -96 -544 192 -544 192 -960 448 -960 ecalcs.sp.SLNK
w 144 -285 100 0 STIN inhier.STIN.P -96 -288 432 -288 ecalcs.st.SLNK
s 608 -1472 500 0 cadPlus.sch
s -64 -272 100 0 Start input link
s -64 -528 100 0 Stop input link
s -64 -688 100 0 CAD status
s 1008 -80 100 512 Start counter
s 1008 -752 100 512 Stop counter
n -672 -224 -320 128 100
This represents the contents
of a hierarchical symbol
"cadPlus", to be used to
count the number of START
and STOP activations of each
CAD record. 
_
[cell use]
use bc200tr -1248 -1656 -100 0 frame
xform 0 432 -352
p 1328 -1488 100 0 1 author:S.M.Beard
p 1552 -1488 100 0 -1 border:C
p 1312 -1520 100 0 1 checked:B.Goodrich
p 1584 -1488 100 0 -1 date:4 Feb 97
p 1552 -1376 100 0 -1 project:Core Instrument Control System
p 1552 -1440 100 0 -1 title:CAD start and stop counter
use outhier 1024 -809 100 0 SPID
xform 0 1040 -768
use outhier 1024 -137 100 0 STID
xform 0 1040 -96
use inhier -112 -745 100 0 STAT
xform 0 -96 -704
use inhier -112 -585 100 0 SPIN
xform 0 -96 -544
use inhier -112 -329 100 0 STIN
xform 0 -96 -288
use ecalcs 448 -1049 100 0 sp
xform 0 592 -784
p 512 -1072 100 0 1 CALC:B=0?A+1:A
p 160 -898 100 0 0 EGU:count
p 512 -1104 100 0 1 PV:$(top)$(cad):
use ecalcs 432 -377 100 0 st
xform 0 576 -112
p 496 -400 100 0 1 CALC:B=0?A+1:A
p 144 -226 100 0 0 EGU:count
p 496 -432 100 0 1 PV:$(top)$(cad):
[comments]
