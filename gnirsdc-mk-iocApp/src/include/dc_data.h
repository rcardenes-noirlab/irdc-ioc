#ifndef DC_DATA_H
#define DC_DATA_H

#include <string>
#include "controller.h"

namespace DC_Data {
	static const std::string DATA_OBSERVE_SEM = "DataObserve";

	std::string get_full_uuid_path_with_extension(std::string extension="");

	std::string get_data_path();
	std::string get_uuid();
	size_t dump_to(std::string, Controller *);

	std::string get_current_uuid();
}

#endif // DC_DATA_H
