/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 * $Id
 * GNIRS PROJECT
 * 
 * FILENAME 
 *   testdhs.cc
 * 
 * GENERAL DESCRIPTION
 *   Program to provide a simple test of the DHS system with timings.
 *   Sends data of same size of GNIRS array to DHS, ie 4 1kx1k quadrants of
 *   intensity, variance and quality pixels. Optionally sends data in 
 *   a set of small slices
 *
 * ORIGINAL AUTHOR: 
 *   Peter Young
 *
 * HISTORY

 *INDENT-ON
 *
 * $Log: testdhs_chunk.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Gnirs; first gemini-modified release
 *
 * Revision 1.2  2004/12/22 06:13:53  pjy
 * DC data src files ready for first Gemini release
 *
 * Revision 1.1  2004/03/19 01:51:54  pjy
 * Added
 *
 *
 -------------------------------------------------------------------------*/
#ifdef vxWorks
#include <vxWorks.h>
#endif

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/

#ifdef vxWorks
#include <stdioLib.h>
#include <sysLib.h>
#include <timers.h>
#else
#include <stdio.h>
#define printErr printf
#endif
#include <time.h>
#include "dhs.h"

/*---------------------------------------------------------------------------
 External functions
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 External variables
 --------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Static variables
 -------------------------------------------------------------------------*/

static DHS_CONNECT dhsConnection;  // DHS connection pointer

/*--------------------------------------------------------------------------
 Global variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Class definitions
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Non-member functions
 -------------------------------------------------------------------------*/

static void checkDhs(const char* label, int dhsResult, const char* fname, int lineno);
static void dhsErrorCallback(DHS_CONNECT,DHS_STATUS,DHS_ERR_LEVEL,char *,DHS_TAG,void *);
static void sendDhs(char* label, DHS_BD_DATASET dataSet, DHS_BOOLEAN last);

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: testDhsInit()
 * 
 * INVOCATION: testDhsInit
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Initialises a connection to the DHS
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void testDhsInit(char *hostip, char *server)
{
  DHS_STATUS dhsResult;              // DHS library status value
  DHS_THREAD dhsThreadId;            // DHS thread ID. 
  bool eventLoop = false;             // set TRUE if eventLoop is started
//    const char hostip[] = "150.203.89.131";
    
  try {
    printErr("In dhsInitTest...\n");  

    dhsResult = DHS_S_SUCCESS;

    // Now initialise the DHS library with space for 1 connection
    printErr("calling dhsInit...\n");  
    dhsInit("gnirsData", 10, &dhsResult);
    checkDhs("dhsInit", dhsResult, __FILE__, __LINE__);
    
    // Set up callback functions for errors and puts
    printErr("calling dhsCallbackSet...\n");  
    dhsCallbackSet(DHS_CBT_ERROR, (DHS_CB_FN_PTR) &dhsErrorCallback, &dhsResult);
    checkDhs("dhsCallbackSet:ErrorCallback", dhsResult, __FILE__, __LINE__);
    
    // Start the eventloop
    printErr("calling dhsEventLoop...\n");  
    dhsEventLoop(DHS_ELT_THREADED, &dhsThreadId, &dhsResult);
    checkDhs("dhsEventLoop", dhsResult, __FILE__, __LINE__);
    eventLoop = true;
    
    // Make a connection to the DHS data server    
    printErr("calling dhsConnect with IP=%s, name=%s\n",hostip, server);  
    dhsConnection = dhsConnect(hostip, server, NULL, &dhsResult);
    checkDhs("dhsConnect", dhsResult, __FILE__, __LINE__);
    printErr("connection made = %d\n",dhsConnection);
  }
  catch (int& dce) {
    printErr("dhsInit caught error at line %d...\n",dce);  
    if (eventLoop) dhsEventLoopEnd(&dhsResult);
    dhsExit(&dhsResult);
  }
  
  printErr("dhsInitTest done...\n");  

}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: void quitDhs()
 * 
 * INVOCATION: quitDhs
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Closes connection to the DHS
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void quitDhs()
{
  DHS_STATUS dhsResult = DHS_S_SUCCESS;
  dhsExit(&dhsResult);
  checkDhs("dhsExit",dhsResult,__FILE__, __LINE__);
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: transferDhs()
 * 
 * INVOCATION: transferDhs
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Transfers a data set to the DHS
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void transferDhs(unsigned long rows, unsigned long cols, int frames, int slices)
{
  DHS_STATUS dhsResult;              // DHS library status value
  char *contrib[1];                  // List of contributors to the data
  char* dataLabel;                   // Unique name returned by DHS
  char *qlStreams[2];                // List of streams to send data to
  char label[50];                    // Dataset name
  DHS_BD_DATASET hdr_dataSet;        // Id of the dataset for headers
  DHS_BD_DATASET dataSet;             // Id of the dataset
  DHS_BD_FRAME intensity[4];         // intensity frames for each quadrant
  unsigned long axisSize[2];         // Size of a frame
  unsigned long dims[2];             // Array to contain data dimensions
  float *intensityData[4];
//    float *varianceData[4];
  float *junk;
//    unsigned char *qualityData[4];
  unsigned long detx[4]={1025,1,1,1025};
  unsigned long dety[4]={1,1,1025,1025};  
  long origin[2];                    // Origin of frame

//    DHS_BD_FRAME variance[4];          // variance frames for each quadrant
//    DHS_BD_FRAME quality[4];           // quality frames for each quadrant

  unsigned int i,j,k,srows,scols;
  float v;
  int r,s,c;
  struct timespec tps,tpf;           // Timestamps
  double ts,tf;                      // times in seconds
  int status,q;
//    bool do_quality = false;
//    bool do_variance = false;
  bool do_ql = false;
  char name[32];

  try {
    printErr("In transferDhs...\n"); 

    if (frames>4) {
      printErr("Cannot handle %d frames - max is 4\n", frames); 
      return;
    }
    
    // Get timestamp
    status = clock_gettime(CLOCK_REALTIME,&tps);

    dhsResult = DHS_S_SUCCESS;
    
    // Get a unique name for this dataset from the DHS
    dataLabel = dhsBdName(dhsConnection, &dhsResult);
    checkDhs("dhsBdName", dhsResult, __FILE__, __LINE__);
    
    printErr("got dataSet label: %s\n", dataLabel);  

    // Set up the control information for the dataset
    dhsBdCtl(dhsConnection, DHS_BD_CTL_LIFETIME, dataLabel, DHS_BD_LT_PERMANENT, &dhsResult);
    checkDhs("dhsbdCtl:DHS_BD_CTL_LIFETIME", dhsResult, __FILE__, __LINE__);

    contrib[0] = (char*)"GNIRS";
    dhsBdCtl(dhsConnection, DHS_BD_CTL_CONTRIB, dataLabel, 1, contrib, &dhsResult);
    checkDhs("dhsbdCtl:DHS_BD_CTL_CONTRIB", dhsResult, __FILE__, __LINE__);

    if (do_ql) {
      qlStreams[0] = (char*)"gnirsScience";
      dhsBdCtl(dhsConnection, DHS_BD_CTL_QLSTREAM, dataLabel, 1, qlStreams, &dhsResult);
      checkDhs("dhsbdCtl:DHS_BD_CTL_QLSTREAM", dhsResult, __FILE__, __LINE__);
    }
   
    printErr("Control information setup ok...\n");  

    // Create a new dataset for transferring FITS header information
    hdr_dataSet = dhsBdDsNew(&dhsResult);
    checkDhs("dhsBdDsNew", dhsResult, __FILE__, __LINE__);
    
    printErr("Created a new dataset...\n");  

    // Fill in the dataset attributes for main header
    // Always add instrument keyword to each dataset
    dhsBdAttribAdd(hdr_dataSet, "instrument", DHS_DT_STRING, 0, NULL, "GNIRS", &dhsResult);
    checkDhs("dhsBdAttribAdd:instrument", dhsResult, __FILE__, __LINE__);

    sprintf(name,"[%d:%ld,%d:%ld]",1,cols*2,1,rows*2);
    dhsBdAttribAdd(hdr_dataSet, "DETSIZE", DHS_DT_STRING, 0, NULL, name, &dhsResult);
    checkDhs("dhsBdAttribAdd:DETSIZE", dhsResult, __FILE__, __LINE__);

    dhsBdAttribAdd(hdr_dataSet, "NCCDS", DHS_DT_INT32, 0, NULL, 1, &status);
    checkDhs("dhsBdAttribAdd:NCCDS", dhsResult, __FILE__, __LINE__);

    dhsBdAttribAdd(hdr_dataSet, "NEXTEND", DHS_DT_INT32, 0, NULL, frames, &status);
    checkDhs("dhsBdAttribAdd:NCCDS", dhsResult, __FILE__, __LINE__);

    dhsBdAttribAdd(hdr_dataSet, "NAMPS", DHS_DT_INT32, 0, NULL, frames, &status);
    checkDhs("dhsBdAttribAdd:NAMPS", dhsResult, __FILE__, __LINE__);

    // Create DHS frames for each of the image frames - these contain no data
    // but do contain hdrs for the IHUs
    axisSize[0] = cols;
    axisSize[1] = rows;
    sprintf(label, "%s.0.0", dataLabel);
    for (q=0; q<frames; q++) {
      // Following call copied out of the GMOS DC code from Richard Wolfe
      // I don't really understand this, other than I realise that these header
      // frames cannot created without allocating some space.
      dims[0] = 1;
      dims[1] = rows;
      sprintf(name,"GNIRSFrame%d",q+1);
      intensity[q] = dhsBdFrameNew(hdr_dataSet, name, q, DHS_DT_FLOAT,
				   2, dims, (const void **) &junk, &dhsResult);

      // Now add the basic IHU information 
      dhsBdAttribAdd(intensity[q], "INHERIT", DHS_DT_BOOLEAN, 0, NULL, 0, &dhsResult);
      checkDhs("dhsBdAttribAdd:INHERIT", dhsResult, __FILE__, __LINE__);
    
      dhsBdAttribAdd(intensity[q], "dataType", DHS_DT_STRING, 0, NULL, "Intensity", &dhsResult);
      checkDhs("dhsBdAttribAdd:dataType", dhsResult, __FILE__, __LINE__);

      dims[0] = 2;
      dhsBdAttribAdd(intensity[q], "axisSize", DHS_DT_UINT32, 1, dims, axisSize, &dhsResult);
      checkDhs("dhsBdAttribAdd:axisSize", dhsResult, __FILE__, __LINE__);

      origin[0] = 1;
      origin[1] = 1;
      dhsBdAttribAdd(intensity[q], "origin", DHS_DT_INT32, 1, dims, origin, &dhsResult);
      checkDhs("dhsBdAttribAdd:origin", dhsResult, __FILE__, __LINE__);

      dhsBdAttribAdd(intensity[q], "CCDNAME", DHS_DT_STRING, 0, NULL, "GNIRS", &dhsResult);
      checkDhs("dhsBdAttribAdd:CCDNAME", dhsResult, __FILE__, __LINE__);

      sprintf(name,"GNIRS%d",q+1);
      dhsBdAttribAdd(intensity[q], "AMPNAME", DHS_DT_STRING, 0, NULL, name, &dhsResult);
      checkDhs("dhsBdAttribAdd:AMPNAME", dhsResult, __FILE__, __LINE__);

      sprintf(name,"[%d:%ld,%d:%ld]",1,cols*2,1,rows*2);
      dhsBdAttribAdd(intensity[q], "CCDSIZE", DHS_DT_STRING, 0, NULL, name, &dhsResult);
      checkDhs("dhsBdAttribAdd:CCDSIZE", dhsResult, __FILE__, __LINE__);

      sprintf(name,"%d %d",1,1);
      dhsBdAttribAdd(intensity[q], "CCDSUM", DHS_DT_STRING, 0, NULL, name, &dhsResult);
      checkDhs("dhsBdAttribAdd:CCDSUM", dhsResult, __FILE__, __LINE__);

      sprintf(name,"[%ld:%ld,%ld:%ld]",detx[q],detx[q]+cols-1,dety[q],dety[q]+rows-1);
      dhsBdAttribAdd(intensity[q], "CCDSEC", DHS_DT_STRING, 0, NULL, name, &dhsResult);
      checkDhs("dhsBdAttribAdd:CCDSEC", dhsResult, __FILE__, __LINE__);

      dhsBdAttribAdd(intensity[q], "DETSEC", DHS_DT_STRING, 0, NULL, name, &dhsResult);
      checkDhs("dhsBdAttribAdd:DETSEC", dhsResult, __FILE__, __LINE__);

      dhsBdAttribAdd(intensity[q], "DATASEC", DHS_DT_STRING, 0, NULL, name, &dhsResult);
      checkDhs("dhsBdAttribAdd:DATASEC", dhsResult, __FILE__, __LINE__);
    }

    // Now send the header data and wait for the DHS to complete. Indicate that
    // this is not the last data that will be sent
    sendDhs(label, hdr_dataSet, DHS_FALSE);

    // Free the header dataset
    dhsBdDsFree(hdr_dataSet, &dhsResult);    
  
    // Send the data in slices
    srows = rows/slices;
    scols = cols/slices;
    r = c = 0;
    for (s=0; s<slices; s++) {

      // Create a new slice dataset - this dataset will encompass all frames
      dataSet = dhsBdDsNew(&dhsResult);
      checkDhs("dhsBdDsNew", dhsResult, __FILE__, __LINE__);
      printErr("Created a new dataset for slice %d\n", s);  

      // Always add instrument keyword to each dataset
      dhsBdAttribAdd(dataSet, "instrument", DHS_DT_STRING, 0, NULL, "GNIRS", &dhsResult);
      checkDhs("dhsBdAttribAdd:instrument", dhsResult, __FILE__, __LINE__);
      
      // Add a frame for each quadrant of the detector
      dims[0] = 2;      
      for (q=0; q<frames; q++) {
	
	// row or column based slices
	if (q%2 == 0) {
	  // rows - make sure we don't overrun
	  if ((r+srows) > rows)
	    srows = rows - r;
	  origin[0] = 1;
	  origin[1] = r+1;
	  axisSize[0] = cols;
	  axisSize[1] = srows;
	} else {
	  // cols - make sure we don't overrun
	  if ((c+scols) > cols)
	    scols = cols - c;
	  origin[0] = c+1;
	  origin[1] = 1;
	  axisSize[0] = scols;
	  axisSize[1] = rows;
	}

	// Create the intensity frame
	sprintf(name,"GNIRSFrame%d",q+1);
	intensity[q] = dhsBdFrameNew(dataSet, name, q, DHS_DT_FLOAT, 2, axisSize,
				     (const void **) &intensityData[q], &dhsResult);
	checkDhs("dhsBdFrameNew:intensity", dhsResult, __FILE__, __LINE__);
	
	printErr("Created a new frame for quadrant %d, origin=%d,%d\n", q+1, origin[0], origin[1]);  

	// Fill in the frame attributes
	
	// NB - All frames must have axissize set to dimensions of full frame
	axisSize[0] = cols;
	axisSize[1] = rows;
	dhsBdAttribAdd(intensity[q], "axisSize", DHS_DT_UINT32, 1, dims, axisSize, &dhsResult);
	checkDhs("dhsBdAttribAdd:axisSize", dhsResult, __FILE__, __LINE__);
	
	dhsBdAttribAdd(intensity[q], "origin", DHS_DT_INT32, 1, dims, origin, &dhsResult);
	checkDhs("dhsBdAttribAdd:origin", dhsResult, __FILE__, __LINE__);
	
	// Put the intensity data into the data array
	if (q%2 == 0) {
	  for (i=0; i<srows; i++) {
	    for (j=0; j<cols; j++) {
	      k = (i*cols)+j;
	      v = (r+i)*cols+j;
	      intensityData[q][k] = v*(q+1);
	    }
	  }
	} else {
	  for (i=0; i<rows; i++) {
	    for (j=0; j<scols; j++) {
	      k = (i*scols)+j;
	      v = (c+j)*rows+i;
	      intensityData[q][k] = v*(q+1);
	    }
	  }
	}
	
      }

      r+=srows;
      c+=scols;

      printErr("Created intensity frame %d\n", q);  
	
      printErr("DataSet size=%ld\n",dhsBdDsSize(dataSet,&dhsResult));
	
      sendDhs(label, dataSet, DHS_FALSE);
	
      dhsBdDsFree(dataSet, &dhsResult);    
      checkDhs("dhsBdDsFree", dhsResult, __FILE__, __LINE__);
    }

    
    // Create a last dataset for transferring final FITS header information
    hdr_dataSet = dhsBdDsNew(&dhsResult);
    checkDhs("dhsBdDsNew", dhsResult, __FILE__, __LINE__);
    
    printErr("Created a new dataset for final header info...\n");  

    // Always add instrument keyword to each dataset
    dhsBdAttribAdd(hdr_dataSet, "instrument", DHS_DT_STRING, 0, NULL, "GNIRS", &dhsResult);
    checkDhs("dhsBdAttribAdd:instrument", dhsResult, __FILE__, __LINE__);

    sendDhs(label, hdr_dataSet, DHS_TRUE);

    // Now report time taken for full operation
    status = clock_gettime(CLOCK_REALTIME,&tpf);
    tf = (double)tpf.tv_sec + tpf.tv_nsec/1000000000.0;
    ts = (double)tps.tv_sec + tps.tv_nsec/1000000000.0;
    printErr("Time for DHS transfer= %e seconds, tpf=%d,tps=%d\n", tf-ts, tpf.tv_sec, tps.tv_sec);
  }
  catch (int& dce) {
    printErr ("TransferDhs failure at line %d\n", dce);
  }
  printErr("Done transfer to DHS\n");        
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: checkDhs
 * 
 * INVOCATION: checkDhs(char* label, int dhsResult, char* fname, int lineno)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Checks DHS status value and reports if error
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
static void sendDhs(char* label, DHS_BD_DATASET dataSet, DHS_BOOLEAN last)
{
  DHS_TAG putTag;                    // tag returned by the put function
  DHS_CMD_STATUS sendStatus;         // Final status of the put
  DHS_STATUS dhsResult;              // DHS library status value
  char *msg;                         // failure message from server

  try {
    // Send the data to the DHS server. If this is the last data for the dataset, the last
    // parameter is set to TRUE.
    dhsResult = DHS_S_SUCCESS;
    putTag = dhsBdPut(dhsConnection, label, DHS_BD_PT_DS, last, dataSet, NULL, &dhsResult);
    checkDhs("dhsBdPut", dhsResult, __FILE__, __LINE__);
    
    printErr("Sent data to the DHS server for label %s\n", label);  
    
    // Wait for the data to be received
    dhsWait(1, &putTag, &dhsResult);    
    checkDhs("dhsWait", dhsResult, __FILE__, __LINE__);
    
    sendStatus = dhsStatus(putTag, &msg, &dhsResult);    
    checkDhs("dhsStatus", dhsResult, __FILE__, __LINE__);
    
    if (sendStatus != DHS_CS_DONE) {
      printErr ("DHS put failed: connection=%d label=%s\n",
		(int) dhsConnection, label);
      if (msg != NULL)
	printErr ("DHS put reason: %s\n", msg);
    } else
      printErr("Data sent OK\n");        
    
    // Free tag and data
    dhsTagFree(putTag, &dhsResult);    
    checkDhs("dhsTagFree", dhsResult, __FILE__, __LINE__);
  }
  catch (int& dce) {
    printErr ("TransferDhs failure at line %d\n", dce);
    dhsTagFree(putTag, &dhsResult); 
    throw dce;
  }
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: checkDhs
 * 
 * INVOCATION: checkDhs(char* label, int dhsResult, char* fname, int lineno)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Checks DHS status value and reports if error
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
static void checkDhs(const char* label, int dhsResult, const char* fname, int lineno)
{
  if (dhsResult != DHS_S_SUCCESS) {
    printErr("Error in DHS call %s, status=%d\n", label, dhsResult);
    throw lineno;
  }
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: dhsErrorCallback
 * 
 * INVOCATION: dhsErrorCallback(DHS_CONNECT     connect,
			  DHS_STATUS      errorNum,
			  DHS_ERR_LEVEL   errorLev,
			  char *          msg,     
			  DHS_TAG         tag,     
			  void *          userData)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Callback for DHS to use on error
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
static void dhsErrorCallback(DHS_CONNECT     connect,
			     DHS_STATUS      errorNum,
			     DHS_ERR_LEVEL   errorLev,
			     char *          msg,     
			     DHS_TAG         tag,     
			     void *          userData)
{
   printErr ("DHS error callback: connection=%d errNum=%d level=%d \"%s\"\n",
             (int) connect, (int) errorNum, (int) errorLev, msg);
}
// Get a dataset name
void dhsName()
{
  DHS_STATUS dhsResult;              // DHS library status value
  char* dataLabel;                   // Unique name returned by DHS

  try {
    printErr("In dhsName...\n"); 
    
    // Get a unique name for this dataset from the DHS
    dataLabel = dhsBdName(dhsConnection, &dhsResult);
    checkDhs("dhsBdName", dhsResult, __FILE__, __LINE__);
    
    printErr("datalabel = %s", dataLabel);
  }
  catch (int& dce) {
    printErr("dhsName caught error at line %d...\n",dce);  
  }
}
#ifndef vxWorks
int main() 
{
  testDhsInit((char*)"150.203.89.131", (char*)"dataServerNS");
  transferDhs(1024,1024,4,8);
  return 0;
   
}
#endif
