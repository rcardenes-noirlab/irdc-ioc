[schematic2]
uniq 121
[tools]
[detail]
w 290 -309 100 0 n#115 esirs.saveQual.FLNK 256 -256 288 -256 288 -320 352 -320 embbis.saveQualO.SLNK
w 274 -277 100 0 n#114 esirs.saveQual.VAL 256 -288 352 -288 embbis.saveQualO.INP
w 288 1115 100 0 n#98 esirs.cosmRej.FLNK 256 1168 288 1168 288 1104 336 1104 embbis.cosmRejO.SLNK
w 272 1147 100 0 n#97 esirs.cosmRej.VAL 256 1136 336 1136 embbis.cosmRejO.INP
w 1400 -421 100 0 n#94 esirs.save.FLNK 1408 -432 1440 -432 1440 -496 1472 -496 embbis.saveO.SLNK
w 1416 -453 100 0 n#93 esirs.save.VAL 1408 -464 1472 -464 embbis.saveO.INP
w 1400 75 100 0 n#92 esirs.saveVar.FLNK 1408 64 1440 64 1440 0 1472 0 embbis.saveVarOut.SLNK
w 1416 43 100 0 n#91 esirs.saveVar.VAL 1408 32 1472 32 embbis.saveVarOut.INP
w 1400 587 100 0 n#90 esirs.saveCoadds.FLNK 1408 576 1440 576 1440 512 1472 512 embbis.saveCoadO.SLNK
w 1416 555 100 0 n#89 esirs.saveCoadds.VAL 1408 544 1472 544 embbis.saveCoadO.INP
w 1400 1099 100 0 n#85 esirs.saveNdrs.FLNK 1408 1088 1440 1088 1440 1024 1472 1024 embbis.saveNdrsO.SLNK
w 1416 1067 100 0 n#84 esirs.saveNdrs.VAL 1408 1056 1472 1056 embbis.saveNdrsO.INP
s 1040 -1040 500 512 GNIRS DC DHS Status/Alarm Database
[cell use]
use esirs -144 455 100 0 cosmMin
xform 0 64 608
p -48 432 100 0 1 DESC:Cosmic ray minimum for rejection
p -208 256 100 0 0 EGU:0/1
p -208 192 100 0 0 FDSC:Cosmic Ray minimum for rejection
p -48 400 100 0 1 FTVL:DOUBLE
p -48 336 100 0 1 PV:$(sadtop)
p -48 368 100 0 1 SNAM:
use esirs -1312 935 100 0 cosmRate
xform 0 -1104 1088
p -1216 912 100 0 1 DESC:Cosmic Ray Rate - Simulation
p -1376 736 100 0 0 EGU:0/1
p -1376 672 100 0 0 FDSC:Cosmic ray Rate - simulation
p -1216 880 100 0 1 FTVL:DOUBLE
p -1216 816 100 0 1 PV:$(sadtop)
p -1216 848 100 0 1 SNAM:
use esirs -720 935 100 0 cosmThrsh
xform 0 -512 1088
p -624 912 100 0 1 DESC:Cosmic ray threshold for rejection
p -784 736 100 0 0 EGU:0/1
p -784 672 100 0 0 FDSC:Cosmic Ray threshold for rejection
p -624 880 100 0 1 FTVL:DOUBLE
p -624 816 100 0 1 PV:$(sadtop)
p -624 848 100 0 1 SNAM:
use esirs -160 919 100 0 cosmRej
xform 0 48 1072
p -64 896 100 0 1 DESC:Reject cosmic rays?
p -224 720 100 0 0 EGU:0/1
p -224 656 100 0 0 FDSC:Reject cosmic rays?
p -64 864 100 0 1 FTVL:LONG
p -64 800 100 0 1 PV:$(sadtop)
p -64 832 100 0 1 SNAM:
use esirs 992 327 100 0 saveCoadds
xform 0 1200 480
p 1088 304 100 0 1 DESC:Save each coadd?
p 928 128 100 0 0 EGU:
p 928 64 100 0 0 FDSC:Save each coadd?
p 1088 272 100 0 1 FTVL:LONG
p 1088 208 100 0 1 PV:$(sadtop)
p 1088 240 100 0 1 SNAM:
use esirs 992 -185 100 0 saveVar
xform 0 1200 -32
p 1088 -208 100 0 1 DESC:Requested exposures per data set
p 1088 -272 100 0 1 EGU:number
p 928 -448 100 0 0 FDSC:Requested exposures per data set
p 1088 -240 100 0 1 FTVL:LONG
p 1088 -336 100 0 1 PV:$(sadtop)
p 1088 -304 100 0 1 SNAM:
use esirs -160 -505 100 0 saveQual
xform 0 48 -352
p -64 -528 100 0 1 DESC:Actual exposures per data set
p -64 -592 100 0 1 EGU:number
p -224 -768 100 0 0 FDSC:Actual exposures per data set
p -64 -560 100 0 1 FTVL:LONG
p -64 -656 100 0 1 PV:$(sadtop)
p -64 -624 100 0 1 SNAM:
use esirs -1296 455 100 0 dataDest
xform 0 -1088 608
p -1200 432 100 0 1 DESC:Data destination (DHS|FITS|NONE)
p -1360 256 100 0 0 EGU:
p -1360 192 100 0 0 FDSC:Data destination (DHS|FITS|NONE)
p -1200 400 100 0 1 FTVL:STRING
p -1200 336 100 0 1 PV:$(sadtop)
p -1200 368 100 0 1 SNAM:
use esirs 992 -681 100 0 save
xform 0 1200 -528
p 1088 -704 100 0 1 DESC:Flag set if saving permanent copy
p 928 -880 100 0 0 EGU:
p 928 -944 100 0 0 FDSC:Flag set if saving permanent copy
p 1088 -736 100 0 1 FTVL:LONG
p 1088 -800 100 0 1 PV:$(sadtop)
p 1088 -768 100 0 1 SNAM:
use esirs 992 839 100 0 saveNdrs
xform 0 1200 992
p 1088 816 100 0 1 DESC:Save each read?
p 928 640 100 0 0 EGU:
p 928 576 100 0 0 FDSC:Save each read?
p 1088 784 100 0 1 FTVL:LONG
p 1088 720 100 0 1 PV:$(sadtop)
p 1088 752 100 0 1 SNAM:
use esirs -704 455 100 0 rawQLS
xform 0 -496 608
p -608 432 100 0 1 DESC:Spectral data QLS
p -768 256 100 0 0 EGU:
p -768 192 100 0 0 FDSC:Spectral data DHS quick look stream
p -608 400 100 0 1 FTVL:STRING
p -608 336 100 0 1 PV:$(sadtop)
p -608 368 100 0 1 SNAM:
use embbis 352 -393 100 0 saveQualO
xform 0 480 -320
p 416 -464 100 0 1 ONST:No
p 416 -496 100 0 1 PV:$(sadtop)
p 416 -432 100 0 1 ZRST:Yes
use embbis 336 1031 100 0 cosmRejO
xform 0 464 1104
p 400 960 100 0 1 ONST:No
p 400 928 100 0 1 PV:$(sadtop)
p 400 992 100 0 1 ZRST:Yes
use embbis 1472 -569 100 0 saveO
xform 0 1600 -496
p 1536 -640 100 0 1 ONST:No
p 1536 -672 100 0 1 PV:$(sadtop)
p 1536 -608 100 0 1 ZRST:Yes
use embbis 1472 -73 100 0 saveVarOut
xform 0 1600 0
p 1536 -144 100 0 1 ONST:No
p 1536 -176 100 0 1 PV:$(sadtop)
p 1536 -112 100 0 1 ZRST:Yes
use embbis 1472 439 100 0 saveCoadO
xform 0 1600 512
p 1536 368 100 0 1 ONST:No
p 1536 336 100 0 1 PV:$(sadtop)
p 1536 400 100 0 1 ZRST:Yes
use embbis 1472 951 100 0 saveNdrsO
xform 0 1600 1024
p 1536 880 100 0 1 ONST:No
p 1536 848 100 0 1 PV:$(sadtop)
p 1536 912 100 0 1 ZRST:Yes
use bc200tr -1504 -1208 -100 0 frame
xform 0 176 96
p 1072 -1040 100 0 1 author:M Jarnyk
p 1296 -1056 100 0 -1 border:C
p 1072 -1072 100 0 1 checked:
p 1328 -1056 100 0 -1 date:15 Oct 2001
p 1312 -928 100 0 -1 project:Gemini GNIRS DC
p -1504 -1160 100 0 0 revision:1.0
p 1312 -992 150 0 -1 title:dcObsDHSSad
[comments]
