/*
 * Copyright (c) 1994 - 2002 by MSSSO Computing Section 
 *
 * CICADA PROJECT
 *
 * FILENAME sdsu_setup.h
 * 
 * PURPOSE
 * Defines a class for handling sdsu setup files
 *
 */
#ifndef SDSU_SETUP_H
#define SDSU_SETUP_H

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#include "configuration.h"
#include "preferences.h"

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/
const int DEFAULT_DO_SIMULATION = FALSE;
const int DEFAULT_DO_COMMS_TEST = TRUE;
const int DEFAULT_DO_LONG_HARDWARE_TEST	= FALSE;
const int DEFAULT_DO_SHORT_HARDWARE_TEST = FALSE;
const int DEFAULT_DO_SDSU_RESET = FALSE;
const int DEFAULT_DO_POWER_ON = FALSE;
const int DEFAULT_DO_TIMING_BOARD = FALSE;
const int DEFAULT_HAS_UTILITY_BOARD = TRUE;
const int DEFAULT_DO_UTILITY_BOARD = FALSE;
const int DEFAULT_DO_INTERFACE_BOARD = FALSE;
const int DEFAULT_DO_TARGET_CHIP_TEMP = FALSE;
const int DEFAULT_DO_HEATER_ADU	= FALSE;
const int DEFAULT_DO_GAIN = FALSE;
const int DEFAULT_DO_TC = FALSE;
const int DEFAULT_DO_AB	= FALSE;
const int DEFAULT_DO_IDLE = FALSE;
const int DEFAULT_DO_TIMING_DOWNLOAD = TRUE;
const int DEFAULT_DO_UTILITY_DOWNLOAD = TRUE;
const int DEFAULT_DO_INTERFACE_DOWNLOAD = TRUE;
const int DEFAULT_HAS_COADDER = FALSE;
const int DEFAULT_DO_DOWNLOAD_COADDER = FALSE;
const int DEFAULT_DO_INIT_COADDER = FALSE;
const int DEFAULT_DO_SETUP_COADDER = FALSE;
const int DEFAULT_AB_SET = TRUE;
const int DEFAULT_CHIP_TEMP_SET	= TRUE;
const int DEFAULT_IDLE_SET = TRUE;
const int DEFAULT_ENABLE_AUTO_RDC = FALSE;
const int DEFAULT_GAIN_SETTING = 0;
const int DEFAULT_TC_SETTING = 1; // Fast = 1usec
const int DEFAULT_AB_SETTING = 0;
const int DEFAULT_TEST_PATTERN_BY_COADDER = FALSE;
const int DEFAULT_TIMING_LDA = 0;
const int DEFAULT_PON_TIMEOUT = 10;
const int DEFAULT_CLR_TIMEOUT = 100;
const int DEFAULT_IDLE_TIMEOUT = 8;
const int DEFAULT_UTILITY_LDA = 0;
const int DEFAULT_TIMING_SETTING = 10000;
const int DEFAULT_UTILITY_SETTING = 10000;
const int DEFAULT_INTERFACE_SETTING = 10000;
const int DEFAULT_COADDER_SETTING = 10000;
const int DEFAULT_HEATER_ADU = 0;
#define	DEFAULT_TIMING_FILENAME ""
#define	DEFAULT_UTILITY_FILENAME ""
#define	DEFAULT_INTERFACE_FILENAME ""
#define	DEFAULT_VOLTAGES_FILENAME ""
const int DEFAULT_TARGET_CHIP_TEMP = 0;
const int DEFAULT_HARDWARE_TEST_SETTING = 0;
const int DEFAULT_DO_VOLTAGES = FALSE;
const int DEFAULT_VOLTS_DESC = 0;

const int SDSU_MAX_TIMING_FILES = 3;

/*--------------------------------------------------------------------------
 Class declarations 
 -------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
CLASS
    Sdsu_Setup

DESCRIPTION  
    The Sdsu_Setup class is a preferencec class used for holding all sdsu
    controller preferences.
 
KEYWORDS
    Sdsu_Setup, Preferences.

-------------------------------------------------------------------------*/
class Sdsu_Setup:public Preferences {
public:
  Sdsu_Config_Data config;
  
  int gain_list[SDSU_MAX_TIMING_FILES];
  int do_gain_list[SDSU_MAX_TIMING_FILES];
  int tc_list[SDSU_MAX_TIMING_FILES];
  int do_tc_list[SDSU_MAX_TIMING_FILES];
  int ab_list[SDSU_MAX_TIMING_FILES];
  int do_ab_list[SDSU_MAX_TIMING_FILES];

  int timing_filename_is_special;
  Filenametype last_special_timg_file;
  Chip_Readmode_Type last_readout_mode;
  int last_gain;

  //+ Basic constructor - used for setting up preferences.
  Sdsu_Setup();

  //+ Destructor
  ~Sdsu_Setup();

  // GROUP: public member functions

  //+ add a setup item, itemp is pointer to desc
  // > data - pointer to data to insert into table
  // > pos - position in table for insertion
  void put(void* data, int pos=999);

  //+ Get an item from the table and make current
  // > item - description of item to get
  void get(Table_Item *item);       
};

#endif // SDSU_SETUP_H
