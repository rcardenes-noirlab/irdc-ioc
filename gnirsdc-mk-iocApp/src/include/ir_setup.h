/*
 * Copyright (c) 1994 - 2002 by MSSSO Computing Section 
 *
 * CICADA PROJECT
 *
 * FILENAME ir_setup.h
 * 
 * PURPOSE
 * Defines a class for handling ir setup files
 *
 */
#ifndef IR_SETUP_H
#define IR_SETUP_H

/*---------------------------------------------------------------------------
 Include files 
 --------------------------------------------------------------------------*/
#include "preferences.h"
#include "configuration.h"

/*---------------------------------------------------------------------------
 Constants 
 --------------------------------------------------------------------------*/
// TODO: Figure out this one...
#define IR_SETUP "cnf/ir_setup"

//+ Database parameter names
const char IR_REQ_TYPE[] = "req_type"; 
const char IR_ACTIVITY[] = "activity";
const char DO_IR_READOUT[] = "doReadout";
const char DO_RESET[] = "doReset";
// const char SET_INTERVAL[] = "setIntvl";
const char IR_READ_MODE[] = "readModeSet";
const char IR_QUAD_MODE[] = "quadrantsSet";
// const char TIME_MODE[] = "timeModeSet";
const char NFOWLER[] = "nfowler";
//const char READ_TIME[] = "readTime";
//const char READ_INTVAL[] = "readIntvl";
//const char PERIOD[] = "period";
//const char NPERIODS[] = "nperiods";
const char IR_EXPOSEDRQ[] = "exposedRQ";
const char IR_EXPOSED[] = "exposed";
const char NCOADDS[] = "ncoadds";
const char NRESETS[] = "nresets";
//const char RESET_DELAY[] = "resetDelay";
const char SAVE_NDR_DIR[] = "saveNdrDir";
const char DO_SUBTRACT[] = "doSubFile";
const char DO_SAVE[] = "save";
const char DO_SAVE_VAR[] = "saveVar";
const char DO_SAVE_QUAL[] = "saveQual";
const char DO_SAVE_NDR[] = "saveNdrs";
const char DO_DISPLAY_NDR[] = "displayNdrs";
const char DO_SAVE_COADD[] = "saveCoadds";
const char DO_DISPLAY_COADD[] = "displayCoadds";
const char DO_COSM_REJ[] = "cosmRej";
const char DO_CMP_IM[] = "doCmpIm";
const char WAVE_MIN[] = "waveMin";
const char WAVE_MAX[] = "waveMax";
const char COSM_THRESH[] = "cosmThrsh";
const char COSM_MIN[] = "cosmMin";
const char SUB_FNAME[] = "subFile";
const char PREP[] = "prep";
const char ACQ[] = "acq";
const char RDOUT[] = "rdout";
const char IR_PROGRESS[] = "irProgress";
const char IR_TARGET[] = "irTarget";
const char NCOADDS_DONE[] = "ncoaddsDone";
const char NREADS[] = "nreads";
const char NREADS_DONE[] = "nreadsDone";
const char NFS_DONE[] = "nfsDone";
const char TIME_LEFT[] = "timeLeft";
//const char PER_TIME_LEFT[] = "perTimeLeft";
const char EXPOSURE_ELAPSED[] = "expElapsed";
const char IR_MODE[] = "irMode";
const char VIEW_ENABLED[] = "viewEnabled";

//+ Defaults for Expose info structure
const int DEFAULT_DO_IR_READOUT = 1;
const int DEFAULT_DO_RESET = 1;
const int DEFAULT_COSMIC_REJ = 0;
const int DEFAULT_SAVE_EACH_NDR = 0;
const int DEFAULT_DISPLAY_EACH_NDR = 0;
const int DEFAULT_SAVE_EACH_COADD = 0;
const int DEFAULT_DISPLAY_EACH_COADD = 0;
//const int DEFAULT_SET_INTERVAL = 0;
const int DEFAULT_VIEW_ENABLED = 0;
const IR_Readmode_Type DEFAULT_READ_MODE = IR_BRIGHT_READOUT;
const IR_Quadrants_Type DEFAULT_QUAD_MODE = IR_FULL_FRAME;
// const IR_Timing_Mode DEFAULT_TIME_MODE = IR_CALC_EXPTIME;
const long DEFAULT_NFOWLER = 1;
// const double DEFAULT_READ_TIME = 5.28;
// const double DEFAULT_READ_INTVAL = 4.72;
// const double DEFAULT_PERIOD = 10.0;
// const long DEFAULT_NPERIODS = 1;
const long DEFAULT_NRESETS = 1;
const double DEFAULT_COSM_THRESH = 0;
const double DEFAULT_COSM_MIN = 0;
const long DEFAULT_NCOADDS = 1;
const long MAX_NREF = 1024;
const long MAX_NFOWLER = 128;
// const double MIN_READ_TIME = 0.001; // 1ms - something small above zero
// const double MAX_READ_TIME = 1200.0; // something reasonable - say 20 mins
// const double MAX_INTERVAL = 43200.0;// 12 hours = 60*60*12s
// const double MAX_PERIOD = 43200.0; // 12 hours = 60*60*12s
const double MAX_EXPOSED = 43200.0; // 12 hours = 60*60*12s
// const double MAX_RESET_DELAY = 3600.0; // 1 hours = 60*60*1s
// const long MAX_NPERIODS = 4320; // 12 hours of 0s readout interval at 10s duty cycle
const long MAX_NCOADDS = 4320; // 12 hours of DCS readouts at 10s duty cycle

//+ Defaults for expose data structure
const int DEFAULT_DO_SUBTRACT = FALSE;
const int DEFAULT_DO_SAVE_READ = FALSE;
const int DEFAULT_DO_SAVE_VAR = FALSE;
const int DEFAULT_DO_SAVE_QUAL = FALSE;
const int DEFAULT_DO_CMP_IM = FALSE;
const double DEFAULT_WAVE_MIN = 0.8;
const double DEFAULT_WAVE_MAX = 2.6;
#define DEFAULT_SUB_FNAME "ir_sub.fits"

/*--------------------------------------------------------------------------
 Class declarations 
 -------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
CLASS
    IR_Setup

DESCRIPTION  
    The IR_Setup class is a preferencec class used for holding all ir
    controller preferences.
 
KEYWORDS
    IR_Setup, Preferences.

-------------------------------------------------------------------------*/
class IR_Setup:public Preferences {
private:
  
  // GROUP: private member functions

  //+ add a setup item for a particular observing mode
  // > ir_mode - IR observing mode
  // > item - table item to update
  void put_ir_params(IR_Mode ir_mode, Table_Item *item);

  //+ Gets a setup structure from internal table item for a particular observing mode
  // > ir_mode - IR observing mode
  // > item - table item to update
  void get_ir_params(IR_Mode ir_mode, Table_Item *item);

public:
  IR_Exposure_Info view;                //+ parameters for view mode 
  IR_Exposure_Info obs;                 //+ parameters for observe mode 
  IR_Data_Info d_view;                  //+ parameters for view mode 
  IR_Data_Info d_obs;                   //+ parameters for observe mode 
  
  //+ Basic constructor - used for setting up IR parameters.
  IR_Setup();

  //+ Destructor
  ~IR_Setup();

  // GROUP: public member functions

  //+ add a setup item, data is pointer to desc
  // > data - pointer to data to insert into table
  // > pos - position in table for insertion
  void put(void* data, int pos=999);


  //+ Get an item from the table and make current
  // > item - description of item to get
  void get(Table_Item *item);       
};

#endif // IR_SETUP_H
