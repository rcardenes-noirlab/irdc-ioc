/*
 * Copyright (c) 1994-2005 by MSSSO Computing Section 
 * CICADA PROJECT
 * 
 * FILENAME ir_raw_fits.cc
 * 
 * GENERAL DESCRIPTION
 *
 * This program converts IR raw data files into a set of individual FITS files for
 * each raw image frame. It uses the main_header data structures written at the start of
 * each raw file to decode the following raw data.
 *
 * The program is designed to read from standard input the name of the raw data file.
 *
 */
#include <errno.h>
#include "configuration.h"
#include <libgen.h>
#include <iostream>
#include <unistd.h>
#include "fits.h"
#include "utility.h"
#include "exception.h"

/* Main program 
   Arguments: all optional
   image name
*/
int main(int argc, char *argv[])
{
  try {
    char msgbuf[MSGLEN];
    Filenametype raw_fname, fits_fname;
    long nfowler;
    Detector_Image_Info image_info;
    Frame_Data *frames;
    Sub_Frame_Data *this_subframe;        // Output subframe currently being looked at
    Fits new_fits;
    int raw_fd, stat, fs, j, n, i, ext, k, nframes, nsubframes, f, sf;
    long np;
    double bs[FITS_MAX_EXTEND+1];
    double bz[FITS_MAX_EXTEND+1];        // bscale and bzero for each extension
    int bp[FITS_MAX_EXTEND+1];           // bitpix for each extension
    Ext_Type et[FITS_MAX_EXTEND+1];         // extension type
    int naxes[MAX_IMAGE_FRAMES*2+2]={0}; // X and Y axis dimensions
    char *data,*alldata;
    char *ibp,*obp;
    short  *isp,*osp;
    int *iip,*oip;
    float *ifp,*ofp;
    double *idp,*odp;

    if (argc>1)
      strlcpy(raw_fname, argv[1], FILENAMELEN);
    else
      gets(raw_fname);

    try {
      
      // Open raw file
      if ((raw_fd = open(raw_fname,O_RDONLY)) == IPC_ERR) {
	snprintf(msgbuf, MSGLEN,"Failed to open %s",raw_fname);
	throw Error(msgbuf,E_ERROR,errno,__FILE__,__LINE__);
      }

      // Read in header info
      // First the number of fowler samples in this file
      if ((stat = read(raw_fd,(char*) &nfowler , sizeof(long))) == -1) {
	snprintf(msgbuf, MSGLEN,"Failed to read nfowler parameter from raw data file %s", raw_fname);
	throw Error(msgbuf, E_ERROR, errno, __FILE__,__LINE__);
      }
      // Now the image info structure
      if ((stat = read(raw_fd,(char*) &image_info, sizeof(Detector_Image_Info))) == -1) {
	snprintf(msgbuf, MSGLEN,"Failed to read image info header from raw data file %s", raw_fname);
	throw Error(msgbuf, E_ERROR, errno, __FILE__,__LINE__);
      }
      // Now the frame info
      if ((stat = read(raw_fd,(char*) &nframes, sizeof(int))) == -1) {
	snprintf(msgbuf, MSGLEN,"Failed to read nframes parameter from raw data file %s", raw_fname);
	throw Error(msgbuf, E_ERROR, errno, __FILE__,__LINE__);
      }

      frames = new Frame_Data[nframes];
      for (f=0; f<nframes; f++) {
	if ((stat = read(raw_fd,(char*) &frames[f], sizeof(Frame_Data))) == -1) {
	  snprintf(msgbuf, MSGLEN,"Failed to read nframes parameter from raw data file %s", raw_fname);
	  throw Error(msgbuf, E_ERROR, errno, __FILE__,__LINE__);
	}
	frames[f].subframes.subframes_val = new Sub_Frame_Data[frames[f].subframes.subframes_len];
	for (sf=0; sf<frames[f].subframes.subframes_len; sf++) {
	  if ((stat = read(raw_fd,(char*) &frames[f].subframes.subframes_val[sf], 
			   sizeof(Sub_Frame_Data))) == -1) {
	    snprintf(msgbuf, MSGLEN, "Failed to read subframes header from raw data file %s", raw_fname);
	    throw Error(msgbuf, E_ERROR, errno, __FILE__,__LINE__);
	  }
	}
      }

      // Work out how many amplifiers have been packed into the output frames
      // These are our real raw output frames
      nsubframes = 0;
      for (i=0; i<nframes; i++) {
	for (j=0; j<frames[i].subframes.subframes_len; j++) {
	  nsubframes++;
	}
      }

      // For each fowler sample create a new Fits file
      for (fs=0; fs<nfowler; fs++) {
	// Create the FITS file
	snprintf(fits_fname, MSGLEN,"%s_%d.fits",basename(raw_fname),fs);

	// First remove file if existing
	unlink(fits_fname);
	
	j = (nsubframes>1)? 1:0;
	n = (nsubframes>1)? 2:0;
	ext = (nsubframes>1)? nsubframes:0;
	
	for (i=0; i<nframes; i++) {
	  for (sf=0; sf<frames[i].subframes.subframes_len; sf++) {
	    this_subframe = &frames[i].subframes.subframes_val[sf];
	    bs[j] = image_info.bscale;
	    bz[j] = image_info.bzero;
	    bp[j] = image_info.bitpix;
	    et[j] = EXT_IMAGE;
	    naxes[n++] = this_subframe->width;
	    naxes[n++] =  this_subframe->height;
	    j++;
	  }
	}

	if (nsubframes>1) {
	  bs[0] = image_info.bscale;
	  bz[0] = image_info.bzero;
	  bp[0] = image_info.bitpix;
	}

	new_fits.create(fits_fname, et, bp, naxes, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH, 
			36, ext, 36, TRUE);
	new_fits.write_std_keywords(et,bs,bz);

	//Now read in data for this frame and write it to the Fits file
	// all frames are same size - use last set subframe as sample
	np = this_subframe->width*this_subframe->height;

	// Read all data into internal buffer
	alldata = new char[abs(image_info.bitpix)/8*np*nsubframes];
	if ((stat = read(raw_fd, alldata, abs(image_info.bitpix)/8*np*nsubframes)) == -1) {
	  snprintf(msgbuf, MSGLEN,"Failed to read IR raw data from %s", raw_fname);
	  throw Error(msgbuf, E_ERROR, errno, __FILE__,__LINE__);
	}
	
	// Now for each frame unravel the interleaved data
	if (nsubframes > 1) {
	  j = (nsubframes>1)? 1:0;
	  data = new char[abs(image_info.bitpix)/8*np];
	  for (i=0; i<nsubframes; i++) {
	    n = 0;
	    switch (image_info.bitpix) {
	    case 8:
	      ibp = alldata+i;
	      obp = data;
	      for (k=0; k<np; k++) {
		obp[k] = ibp[n];
		n+=nsubframes;
	      }
	      break;
	    case 16:
	      isp = (short*) alldata;
	      isp += i;
	      osp = (short*) data;
	      for (k=0; k<np; k++) {
		osp[k] = isp[n];
		n+=nsubframes;
	      }
	      break;
	    case 32:
	      iip = (int*) alldata;
	      iip += i;
	      oip = (int*) data;
	      for (k=0; k<np; k++) {
		oip[k] = iip[n];
		n+=nsubframes;
	      }
	      break;
	    case -32:
	      ifp = (float*) alldata;
	      ifp += i;
	      ofp = (float*) data;
	      for (k=0; k<np; k++) {
		ofp[k] = ifp[n];
		n+=nsubframes;
	      }
	      break;
	    case -64:
	      idp = (double*) alldata;
	      idp += i;
	      odp = (double*) data;
	      for (k=0; k<np; k++) {
		odp[k] = idp[n];
		n+=nsubframes;
	      }
	      break;
	    }
	    
	    // Now write out using Fits class to handle bitpix,bscale and bzero
	    new_fits.write_data(data, bp[j], j);
	    
	    // Free data buffer
	    delete [] data;
	    
	    j++;
	  }
	} else {
	  new_fits.write_data(alldata, bp[j], j);
	}
	
	delete [] alldata;
	
	// Now close this fits file
	new_fits.close();
      }
      delete [] frames;
    }
    catch (Error& e) {
      e.print_error(__FILE__, __LINE__);
    }
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
}


 
  

