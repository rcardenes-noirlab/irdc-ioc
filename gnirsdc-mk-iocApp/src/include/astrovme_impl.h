/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: astrovme_impl.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI GNIRS PROJECT
 * 
 * FILENAME astrovme_impl.h
 * 
 * GENERAL DESCRIPTION 
 *
 * This is a VxWorks version of the device driver to communicate with
 * the SDSU VME interface board. 
 *
 * This file contains private implementation details for VxWorks device driver for
 * the SDSU VME Interface Card. 
 *
 * Author: Peter Young (RSAA) - For the Gemini Telescope Project (GSAOI) - December 2003
 *
 * $Log: astrovme_impl.h,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.2  2005/02/25 04:45:55  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.1  2004/12/22 22:43:17  pjy
 * GNIRS include files at first Gemini release
 *
 * Revision 1.2  2004/08/20 02:18:24  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.1  2004/04/25 02:03:29  pjy
 * Pre T2.2 version
 *


 *
 */
#ifndef __ASTROVME_IMPL_H__
#define __ASTROVME_IMPL_H__

#include <sys/types.h>
#include <semLib.h>
#include <msgQLib.h>

/* 
 * Setup and timeout constants 
 */
static const int SDSU_VME_BOARD_REPLY_TIMEOUT = 3;  /* How long (seconds) to wait for reply */
static const int SDSU_VME_BOARD_RESET_TIME = 1000;  /* How long (milliseconds) to wait after reset*/
static const int SDSU_VME_BOARD_WAIT_TIME = 200;    /* How long (milliseconds) to wait after SRA command  */
static const int SDSU_VME_BOARD_SETTLE_TIME = 0;    /* How long (milliseconds) to wait after command to settle*/
static const int SDSU_INTERRUPT_LEVEL = 6;          /* VME bus interrupt level (set by card jumper)*/
static const int SDSU_VME_STATUS_WORD = 0x200000;   /* SDSU VME DSP X mem Status word address */
static const int SDSU_STATUS_READOUT = 0x1;         /* SDSU VME DSP Status - reading out bit */
static const int SDSU_STATUS_SRA_OK = 0x4;          /* SDSU VME DSP Status - SRA OK bit */
static const int SDSU_VME_OPTIONS_WORD = 0x200001;  /* SDSU VME DSP X mem Options word address */
static const int SDSU_OPTIONS_FB_INTR = 0x1;        /* SDSU VME DSP Options- Interrupt at begin and end of frame bit */
static const int SDSU_OPTIONS_2S_COMP = 0x2;        /* SDSU VME DSP Options- convert data to 16bit 2s complement bit */
static const int SDSU_OPTIONS_RDC_INTR = 0x4;       /* SDSU VME DSP Options- RDC Interrupt bit */
static const int SDSU_TEST_PATTERN_CONST_BIT = 0x4; /* SDSU test pattern constant or incrementing */ 
static const int SDSU_TEST_PATTERN_INC_PAR = 0xC;   /* Y memory location for test pattern inc value */
static const int SDSU_TEST_PATTERN_SEED_PAR = 0xD;  /* Y memory location for test pattern seed value */
static const int SDSU_REPLY_INT_NUM = 240;          /* reply interrupt vector*/
static const int SDSU_RDC_INT_NUM = 241;            /* rdc counter interrupt vector*/
static const int SDSU_RDC_INT_NUM2 = 242;           /* rdc image complete interrupt vector*/
static const int REPLY_MASK = 0x00ffffff;           /* only look at low 24 bits of reply words */
static const int SDSU_ALIGN = 0x400;                /* align buffers to 1024 byte boundary*/
static const int SDSU_REPLY_MAX_MSGS = 10;          /* Maximum number of reply messages in message queue */
static const int SDSU_BUFFER_ADDRESS = 0x400040;    /* Currently this Y: memory location holds the high 16 bits of 
						       image address buffer. Remove when FBA command implemented */

/* 
 * Make following definition #define so that it can be used in array
 * dimensioning 
 */
#define SDSU_REPLY_BUF_LEN 32                       /* length of reply ring buffer */

/* 
 * Some Commands interpreted by the driver 
 */
static const int SDSU_VME_ID = 0x1;          /* VME board ID */
static const int SDSU_TIM_ID = 0x2;          /* Timing board ID */
static const int SDSU_UTIL_ID = 0x2;         /* Utility board ID */
static const int SDSU_MEM_P = 0x0;           /* P memoray space */
static const int SDSU_MEM_X = 0x1;           /* X memoray space */
static const int SDSU_MEM_Y = 0x2;           /* Y memoray space */
#define N_BOARDS  4                          /* interface,timing,utility,coadder*/
#define N_MEMS 3                             /* P,X,Y */
#define MAX_MEM 0x3fff                       /* 16k words */


#define SDSU_ABR 0x414252        /* ABR - Abort */
#define SDSU_CAT 0x434154	 /* CAT - Test Coadder data link */ 
#define SDSU_CDS 0x434453	 /* CDS - Correlated Double Sampling */ 
#define SDSU_CLR 0x434C52        /* CLR - Clear detector */ 
#define SDSU_CON 0x434F4E	 /* CON - Power On (same as PON) */ 
#define SDSU_CRT 0x435254        /* CRT - Calibrate frame read time */ 
#define SDSU_DCA 0x444341        /* DCA - Load coadder from Timing SRAM */
#define SDSU_DFS 0x444653        /* DFS - Set digital filter samples */
#define SDSU_DON 0x444F4E        /* DON - Done */
#define SDSU_ERR 0x455252        /* ERR - system error */
#define SDSU_FBA 0x464241        /* FBA - Set frame buffer address*/
#define SDSU_ICA 0x494341        /* ICA - Initialise coadder */
#define SDSU_IDL 0x49444C        /* IDL - Start system idle mode */
#define SDSU_LCA 0x4C4341        /* LCA - Load coadder from Timing ROM */
#define SDSU_PCA 0x504341        /* PCA - Set pixel count address*/
#define SDSU_POF 0x504F46	 /* POF - Power Off	*/ 
#define SDSU_PON 0x504F4E	 /* PON - Power On	*/ 
#define SDSU_PRM 0x50524D        /* PRM - Set readout parameters */
#define SDSU_RCC 0x524343        /* RCC - read controller config */
#define SDSU_RDC 0x524443        /* RDC - run readout command */
#define SDSU_RET 0x524554	 /* RET - Get Exposure Time	*/ 
#define SDSU_RDM 0x52444D        /* RDM - read DSP memory location */
#define SDSU_ROP 0x524F50        /* ROP - Read options word */
#define SDSU_RRS 0x525253        /* RRS - Remote reset to timing board */
#define SDSU_SBN 0x53424E        /* SBN - set bias number */
#define SDSU_SBV 0x534256        /* SBV - set bias supply voltages */
#define SDSU_SCA 0x534341        /* SCA - setup coadder */
#define SDSU_SET 0x534554	 /* SET - Set Exposure Time	*/ 
#define SDSU_SEX 0x534558        /* SEX - Start exposure */
#define SDSU_SFS 0x534653	 /* SFS - Send Fowler Samples	*/ 
#define SDSU_SRA 0x535241        /* SRA - Set reply buffer address*/
#define SDSU_SRD 0x535244        /* SRD - Set reset delay */
#define SDSU_SRF 0x535246        /* SRF - Set number of reference samples */
#define SDSU_SMX 0x534D58        /* SMX - Set MUX */
#define SDSU_SNR 0x534E52        /* SNR - Set reset count */
#define SDSU_SRM 0x53524D        /* SRM - Select reset mode */
#define SDSU_STP 0x535450        /* STP - Stop idle mode */
#define SDSU_STX 0x535458        /* STX - Stop exposure */
#define SDSU_SUR 0x535552        /* SUR - Set Up The Ramp Mode */ 
#define SDSU_SYR 0x535952        /* SYR - system reset */
#define SDSU_TDL 0x54444C        /* TDL - run readout command */
#define SDSU_WOP 0x574F50        /* WOP - write options word */
#define SDSU_WRM 0x57524D        /* WRM - write DSP memory location */

/*******************************************************************************
	astrovme_state - Driver state structure.  All state variables related
			 to the state of the driver are kept in here.
*******************************************************************************/
typedef struct {
  DEV_HDR header;                       /* VxWorks driver table structure */
  
  uint32_t* command_register;           /* VME jumpered address of board converted to local address */
  uint32_t* reset_register;             /* converted local address, 4 bytes on from cmd address*/
  uint32_t* vme_reply_addr;             /* reply buffer mapped to VME address space */
  uint32_t* vme_image_addr;             /* image buffer mapped to VME address space */
  uint32_t* vme_pixel_counter;          /* pixel counter mapped to VME address space */

  /* Reply buffer - ring buffer space allocated on alignment boundary */
  uint32_t* reply_buf;                  /* ring buffer */
  int reply_idx;                        /* ring buffer address pointer */

  /* Image address */
  uint16_t *image_addr;                 /* pixel address */

  /* Pixel counter - aligned address*/
  uint32_t *pixel_counter;              /* pixel counter */
 
  /* operation flags */
  int open;		                /* Flag set when driver opened              */
  int busy;		                /* Set when transfers in progress           */
  int intr_busy;		        /* Set when interrupt service in progress   */
  int ioctl_busy;                       /* set when an ioctl operation in progress */
  int read_busy;                        /* set when a read is running */
  int rdc_sent;                         /* set when an RDC command has been trapped */
  int stop;                             /* TRUE if STOP ioctl received */
  int abort;                            /* TRUE if ABORT ioctl received */
  int simulate;                         /* TRUE if in simulation mode */
  int sim_dsp_mem[N_BOARDS][N_MEMS][MAX_MEM];/* simulate DSP memory */
  int timeout_mult;                     /* standard timeout multiplier */

  /* Readout setup information */
  uint32_t nreads;                      /* Number of reads to do in multiple IR read*/
  uint32_t nsamples;                    /* Number of samples to do in multiple IR read*/
  uint32_t namps;                       /* Number of readout amps */
  uint32_t readouts_done;               /* readouts done so far in multiple IR read */
  uint32_t read_running;                /* number of the current read running */
  uint32_t read_wait;                   /* length of time to wait for first read msecs*/
  uint32_t read_interval;               /* length of time between succesive IR readouts msecs*/
  uint32_t reset_delay;                 /* length of time to delay before expecting first data after a reset*/
  uint32_t buffer_size;                 /* size of cyclic output buffer in bytes - at least 2ximage_size*/
  uint32_t image_size;                  /* size of one image frame */
  double exptime;                       /* exposure time set with SET command  - seconds*/
  uint32_t read_time;                   /* Time expected to read one frame - msec */
  uint64_t readout_time;                /* Time required for a readout usec */
 
  /* VxWorks device information */
  int instance;                         /* vme board instance number */
  int devAdd;                           /* set when iosDevAdd successful */
  
  /* interrupt control info*/
  int isr_tid;                          /* interrupt handling task */
  int reply_intr_count;                 /* count of number of reply interrupts received */
  int rdc_intr_count;                   /* count of number of rdc interrupts received */
  int sdsu_int_enable;                  /* interrupts enabled flag */
 
  /* Thread locking and synchronization */
  MSG_Q_ID reply_mq;                    /* SDSU reply message queue*/
  SEM_ID semAccess;                     /* semaphore guarding SDSU access */
  SEM_ID semIntr;
  
} Astrovme_State;

/* Following needed for intdisconnect code (google search for intdisc.c)
   #include <drv/intrCtl/i82378zb.h> */
/* interrupt handler desciption */
typedef struct intHandlerDesc {
  VOIDFUNCPTR vec;                     /* interrupt vector */
  int arg;                             /* interrupt handler argument */
  struct intHandlerDesc *next;         /* next interrupt handler & argument */
} INT_HANDLER_DESC;

/* Interrupt service routine type decl */
/*  typedef void (*Astrovme_ISR)(int, int); */

#endif

