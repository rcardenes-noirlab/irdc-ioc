/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  fits_split.cc
 * 
 * GENERAL DESCRIPTION
 *  Program to split a MEF FITS file into multiple single FITS files
 *   
 * ORIGINAL AUTHOR: 
 *   Peter Young - RSAA 1993
 *
 * HISTORY
 *   Sept 1993 PJY    Created for RSAA CICADA package
 *   Nov 2002 PJY     Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */
#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "fits.h"
#include "exception.h"

#ifdef vxWorks
int fitsSplit()
#else
int main()
#endif
{
  try {
    Fits old_fits;
    Fits new_fits;
    Fitsname old_name;
    Fitsname new_name;
    Fitsname fname;
    int naxes[2],i;

    // Test algorithm
    // 1. Read an existing FITS file
    // 2. Split into multiple single extension files
    // 3. Copy header data
    try {
      cout << "Enter name of MEF FITS file: ";
      cin >> old_name;
      old_fits.read(old_name);
      cout << " - " << old_name << " read ok - " << old_fits.nextend << " extensions found." << endl;
      if (old_fits.extend) {
	cout << "Enter name of new FITS file: ";
	cin >> new_name;
	
	for (i=0; i<old_fits.nextend; i++)  { 
	  naxes[0] = old_fits.xt[i+1].nx;
	  naxes[1] = old_fits.xt[i+1].ny;

	  snprintf(fname, FITSNAMELEN,"%s_%d",new_name,i+1);
	  new_fits.create(fname, &old_fits.xt[i+1].type, &old_fits.xt[i+1].bitpix, naxes, S_IRUSR|S_IWUSR, 
			  old_fits.num_keywords(0) + old_fits.num_keywords(i+1,TRUE));
	  new_fits.write_std_keywords(&old_fits.xt[i+1].type, &old_fits.xt[i+1].bscale,
				      &old_fits.xt[i+1].bzero, &old_fits.xt[i+1].pcount, &old_fits.xt[i+1].gcount);
	  new_fits.copy_headers(old_fits,0,0,TRUE); // Copy main header keywords
	  new_fits.copy_headers(old_fits,0,i+1,TRUE); // Copy extension header keywords
	  new_fits.write_data(old_fits.xt[i+1].data, old_fits.xt[i+1].bitpix);
	  new_fits.close();
       }
      }
    }
    catch (Error& e) {
      e.print_error(__FILE__,__LINE__);
    }
      
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return 0;
}
