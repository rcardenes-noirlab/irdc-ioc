/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  fits.cc
 * 
 * GENERAL DESCRIPTION
 *  Implementation of simple FITS reading and writing routines
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 1993
 *
 * HISTORY
 *   Sept 1993 PJY    Created for RSAA CICADA package
 *   June 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */

/* Include files */
#include <math.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>
#include <string>
#include <errno.h>
#include "fits.h"

using namespace std;

/* defines */
#define MIN(m1,m2) ((m1<m2)? (m1):(m2))
#define MAX(m1,m2) ((m1>m2)? (m1):(m2))

/* typedefs */

/* class declarations */


/* global variables */

/* local function declarations */
//+ constructs a FITS header record.
static void hdr_line(char *p,char **ap,const char *line);

/* class function definitions */
/*
 *+ 
 * FUNCTION NAME: Fits::Fits()
 * 
 * INVOCATION: fits = new Fits()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Constructor
 * 
 * DESCRIPTION: Calls the initialise method to initialise member variables
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Fits::Fits()
{
  xt = NULL;
  btable = NULL;
  initialise(0);
}

/*
 *+ 
 * FUNCTION NAME: Fits::Fits
 * 
 * INVOCATION: fits = new Fits(char* filename, mode_t mode, int readin)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > filename - Fits file to open
 * > mode - open mode - standard unix open() mode options
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Initialises and opens existing file for read or update
 * 
 * DESCRIPTION: Initialises member variables; calls open method and the 
 * sets open flags
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Fits::Fits(char* filename, mode_t mode, int readin)
{
  xt = NULL;
  btable = NULL;
  initialise(0);
  if (readin)
    read(filename);
  else 
    open(filename);
  if (mode == O_RDWR) 
    file_open_update = TRUE;    
}

/*
 *+ 
 * FUNCTION NAME: Fits::Fits
 * 
 * INVOCATION: fits = new Fits(char* filename, int* outbitpix)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > filename - Fits file to open
 * > outbitpix - array of output bitpix required - file is opened with data converted to these types
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Initialises, opens and reads existing file 
 * 
 * DESCRIPTION: Initialises member variables; calls read method 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *-
 */
Fits::Fits(char* filename, int* outbitpix)
{
  xt = NULL;
  btable = NULL;
  initialise(0);
  read(filename,outbitpix);
}

/*
 *+ 
 * FUNCTION NAME: Fits::Fits
 * 
 * INVOCATION: fits = new Fits(Fits input_image, Fitsname new_name, mode_t mode, bool open_file)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > input_image - reference to an existing Fits file. 
 * > new_name    - disk filename of the Fits file to be created
 * > mode - access mode, default=O_RDONLY, others defined in sys/fcntl.h.
 * > open_file - if TRUE, the Fits copy is left open.
 *               if FALSE, the copy is neither opened or read.
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Copy Constructor
 * 
 * DESCRIPTION: Returns a new copy of a given Fits file.
 *  This method reads the input file, copies to a new file and then
 *  opens the new file for write if required.
 *
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Fits::Fits(Fits& input_image, Fitsname new_name, mode_t mode, bool open_file)
{
  const int BUF_LEN = 32767;             // size of file buffer
  FILE* in_fd;                           // input file descriptor
  FILE* out_fd;                          // input file descriptor
  char buffer[BUF_LEN];                  // file buffer
  ostringstream ostr;                        // Message log  
  int n,n2;

  // Copy input file to new output file byte by byte.
  if ((in_fd = fopen(input_image.fitsname, "r")) == NULL) {
    ostr << "Failed to open the FITS file '" << input_image.fitsname << "' for copy?" << ends;
    throw Error(ostr, E_ERROR, errno, __FILE__, __LINE__);
  }
  
  // Now open new empty output file for write
  if ((out_fd = fopen(new_name, "wb")) == NULL) {
    ostr << "Failed to open new FITS file '" << new_name << "'?" << ends;
    throw Error(ostr, E_ERROR, errno, __FILE__, __LINE__);
  }

  // Loop over input file until EOF
  while (!feof(in_fd)) {
    if ((n = fread((void*)&buffer, 1, BUF_LEN, in_fd)) != 0) {
      if ((n2 = fwrite((void*)&buffer, 1, n, out_fd)) != n) {
	ostr << "Failed to write to new FITS file '" << new_name << "' (" << n2 << " of " << n << ")?" << ends;
	throw Error(ostr, E_ERROR, errno, __FILE__, __LINE__);
      }
    }
  }
      
  // Now close the files as copy has finished
  if (fclose(in_fd) != 0) {
    ostr << "Failed to close FITS file '" << input_image.fitsname << "'?" << ends;
    throw Error(ostr, E_ERROR, errno, __FILE__, __LINE__);
  }

  if (fclose(out_fd) != 0) {
    ostr << "Failed to close new FITS file '" << new_name << "'?" << ends;
    throw Error(ostr, E_ERROR, errno, __FILE__, __LINE__);
  }

  // Initialise new Fits file parameters and leave open for write if required.
  xt = NULL;
  btable = NULL;
  initialise(0);
  if (open_file) {
    read(new_name); // open for read-only by default
    if (mode == O_WRONLY )
      file_open_write = TRUE;
    else if (mode == O_RDWR) 
      file_open_update = TRUE;    
  } // end if mode
}

/*
 *+ 
 * FUNCTION NAME: Fits::initialise()
 * 
 * INVOCATION: fits->initialise(int ne)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > ne - number of extensions to initialise
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Initialise FITS object with default values
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::initialise(int ne)
{
  //  cout << "In Initialize" << endl;
  file_open_read = FALSE;
  file_open_write = FALSE;
  file_open_update = FALSE;
  fix_simple_keyword = FALSE;
  headers_read = FALSE;
  open_data = FALSE;
  phu = 0;
  nextend = ne;
  naxis = 2;
  extend = FALSE;
  total_hdr_size = 0;
  total_data_size = 0;
  mosaic_able = FALSE;
  mosaic_data = FALSE;
  mosaic_datap = NULL;
#ifdef SOLARIS
  data_vm_mapped = FALSE;
  use_vm = TRUE;
#elif vxWorks
  data_vm_mapped = FALSE;
  use_vm = FALSE;
#endif

  // Clear memory for extensions if any
  //    cout << "deleting memory for extensions" << endl;
  if (xt) {
    delete [] xt;
    xt = NULL;
  }
  if (btable) {
    delete [] btable;
    btable = NULL;
  }
}
/*
 *+ 
 * FUNCTION NAME: Fits::alloc_xtension()
 * 
 * INVOCATION: fits->alloc_xtension(int ne)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > ne - number of extensions to allocate
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Allocate data for FITS extensions and initialises values
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *-
 */
void Fits::alloc_xtension(int ne)
{
  xt = new Extension[ne+1];
  btable = new Bintable[ne+1];
}

/*
 * FUNCTION NAME: Fits::realloc_xtension()
 * 
 * INVOCATION: fits->realloc_xtension(int old_ne, int new_ne)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > old_ne - number of extensions previously allocated
 * > new_ne - new number of extensions to allocate
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Allocate data for FITS extensions and initialises values
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *-
 */
void Fits::realloc_xtension(int old_ne, int new_ne)
{
  int i,n;

  Extension* old_xt = xt;
  Bintable* old_btable = btable;

  // Make sure the PHU is taken into account
  old_ne++;
  new_ne++;

  n = (old_ne < new_ne ? old_ne : new_ne);

  xt = new Extension[new_ne];
  btable = new Bintable[new_ne];

  for (i=0; i<n; i++) {
    xt[i] = old_xt[i];
    btable[i] = old_btable[i];
  }
  if (old_xt) {
    delete [] old_xt;
  }
  if (old_btable) {
    delete [] old_btable;
  }
}

/*
 *+ 
 * FUNCTION NAME: Fits::open
 * 
 * INVOCATION: fits->open(char* filename)
 * 
 * PARAMETERS: 
 * > filename - Name of FITS file to open
 * 
 * FUNCTION VALUE: void
 * 
 * PURPOSE: Open an existing FITS file and reads header keywords
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::open(char* filename)
{
  const int MAX_FITS_KEYS = FITS_KEYS_PER_BLOCK*10; // Sensible number of keywords should be less than following?
  Fitsline hdr,tformkw,ttypekw;
  int n,i,j,simple,kc,ke,pc,gc,bp,nx,ny;
  double bs,bz;
  int x1,x2,y1,y2,minx=MAXI,maxx=MINI,miny=MAXI,maxy=MINI,xbin,ybin;
  bool done;
  bool detsec_found;
  bool flag;

  // Sanity checks
  if (file_open_read) {
    snprintf(errmsg, MSGLEN, "Fits file already open: %s",fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  if (file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file already open for write: %s",fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  // OK - lets try to open
  strlcpy(fitsname,filename,FITSNAMELEN);

  if ((fd = ::open(fitsname,O_RDONLY,0)) == IPC_ERR) {
    snprintf(errmsg, MSGLEN, "Unable to open Fits file %s, status=%d",fitsname, errno);
    throw Error(errmsg, E_ERROR, errno, __FILE__, __LINE__);    
  }

  // Read standard headers using sift routine - only handle simple 2 axes
  // Position into file first
  if ((n=lseek(fd, 0, SEEK_SET)) == IPC_ERR) {
    snprintf(errmsg, MSGLEN,"Error positioning FITS file %s to primary header unit, status=%d", fitsname,errno);
    throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
  }
  
  simple = FALSE;
  kc = ke = nx = ny = pc = 0;
  gc = 1;
  bs = 1.0;
  bz = 0.0;
  
  
  do {
    if (((n=::read(fd,hdr,FITSKEYWORDLEN)) == IPC_ERR) || (n!=FITSKEYWORDLEN)) {
      snprintf(errmsg, MSGLEN,"Error reading Fits hdr keywords, file %s, status=%d, nbytes=%d", fitsname,errno,n);
      throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
    }
    // Null terminate the hdr string just read
    hdr[FITSKEYWORDLEN] = 0;
    kc++;
    if (kc>MAX_FITS_KEYS) {
      snprintf(errmsg, MSGLEN,"No END keyword found in file %s for PHU - looks suspect???", fitsname);
      throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
    }
    
    if (keysift(hdr,"SIMPLE")!=NULL) {
      simple = (!strcmp(key,"T")); 
    } else if (keysift(hdr,"BITPIX")!=NULL) {
      bp = atoi(key);
    } else if (keysift(hdr,"NAXIS")!=NULL) {
      naxis = atoi(key);
    } else if (keysift(hdr,"NAXIS1")!=NULL) {
      nx = atoi(key);
    } else if (keysift(hdr,"NAXIS2")!=NULL) {
      ny = atoi(key);
    } else if (keysift(hdr,"PCOUNT")!=NULL) {
      pc = atoi(key);
    } else if (keysift(hdr,"GCOUNT")!=NULL) {
      gc = atoi(key);
    } else if (keysift(hdr,"EXTEND")!=NULL) {
      extend=(!strcmp(key,"T"));
    } else if (keysift(hdr,"NEXTEND")!=NULL) {
      nextend=atoi(key);
    } else if (keysift(hdr,"BZERO")!=NULL) {
      bz = atof(key);
    } else if (keysift(hdr,"BSCALE")!=NULL) {
      bs = atof(key);
    } else ke++;

  } while (!check_end(hdr));
    

  // Some validity checks
  if ((nextend < 0) || (nextend > FITS_MAX_EXTEND)) {
    snprintf(errmsg, MSGLEN, "Illegal NEXTEND value %d ???", nextend);
    throw Error(errmsg, E_ERROR, EINVAL, __FILE__, __LINE__);
  }

  if ((nx < 0) || (ny < 0)) {
    snprintf(errmsg, MSGLEN, "Illegal axis size values %d,%d ???", nx, ny);
    throw Error(errmsg, E_ERROR, EINVAL, __FILE__, __LINE__);
  }

  if ((bp != BYTE_BITPIX) && (bp != SHORT_BITPIX) && (bp != INT_BITPIX) && (bp != FLOAT_BITPIX) && 
      (bp != DOUBLE_BITPIX)) {
    snprintf(errmsg, MSGLEN, "Illegal BITPIX value %d ???", bp);
    throw Error(errmsg, E_ERROR, EINVAL, __FILE__, __LINE__);
  }

  // Now allocate space for number of extensions required plus phu
  alloc_xtension(nextend);

  // Set values of phu variables
  xt[0].keysexist = kc;
  xt[0].keysextra = ke;
  xt[0].bitpix = bp;
  xt[0].nx = nx;
  xt[0].ny = ny;
  xt[0].pcount = pc;
  xt[0].gcount = gc;
  xt[0].bzero = bz;
  xt[0].bscale = bs;
  // assume data attached to non-MEF files are IMAGE
  xt[0].type = EXT_IMAGE;

  if (!simple) {
    snprintf(errmsg, MSGLEN,"SIMPLE=T keyword not found in file %s ???", fitsname);
    throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
  }

  // Initialise size parameters
  phu =  (int) ceil((double)xt[0].keysexist*FITSKEYWORDLEN/FITSBLOCK);
  xt[0].data_size = BYTE_PIX(xt[0].bitpix)*xt[0].nx*xt[0].ny;
  xt[0].hdr_size = phu*FITSBLOCK;
      
  // Make sure multiple of FITSBLOCK size
  if (xt[0].data_size%FITSBLOCK) 
    xt[0].data_size = (xt[0].data_size/FITSBLOCK+1)*FITSBLOCK;

  // Adjust detx if not MEF
  if (!extend) {
    xt[0].detx = xt[0].nx;
    xt[0].dety = xt[0].ny;
  }   

  total_data_size = xt[0].data_size;
  total_hdr_size = xt[0].hdr_size;

  // Now read any extension headers
  if (extend) {
    mosaic_able = TRUE;
    done = false;
    i = 1; // Look for first extension
    do {
      // Reallocate memory to allow for the new extension
      realloc_xtension(i-1, i);
      xt[i].keysexist = 0;
      xt[i].bscale = xt[0].bscale;
      xt[i].bzero = xt[0].bzero;
      x1 = x2 = y1 = y2 = 0;
      xbin = ybin = 1;
      detsec_found = false;

      // Position into file first
      if ((n=lseek(fd, total_hdr_size + total_data_size, SEEK_SET)) == IPC_ERR) {
	snprintf(errmsg, MSGLEN,"Error positioning FITS file %s to image header unit %d, status=%d", fitsname,i,errno);
	throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
      }
      do {
	if (((n=::read(fd,hdr,FITSKEYWORDLEN)) == IPC_ERR) || (n!=FITSKEYWORDLEN)) {
	  if (n!=FITSKEYWORDLEN) {
	    done = true;
	    i--;
	    break;
	  } else {
	    snprintf(errmsg, MSGLEN,"Error reading Fits hdr keywords, extension %d, file %s, status=%d, nbytes=%d",
		    i,fitsname,errno,n);
	    throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	  }
	}
	// Null terminate the hdr string just read
	hdr[FITSKEYWORDLEN] = 0;

	xt[i].keysexist++;
	flag = false;
	if (xt[i].keysexist>MAX_FITS_KEYS) {
	  snprintf(errmsg, MSGLEN,"No END keyword found in file %s for IHU %d after reading %d keyword - looks suspect???",
		  fitsname, i,xt[i+1].keysexist);
	  throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	}
	if (keysift(hdr,"NAXIS1")!=NULL) {
	  xt[i].nx=atoi(key);
	} else if (keysift(hdr,"NAXIS2")!=NULL) {
	  xt[i].ny=atoi(key);
	} else if (keysift(hdr,"XTENSION")!=NULL) {
	  // cope with the various IMAGE kw format the various 
	  // Cicada versions write, ie, ignore trailing spaces
	  if (!strncmp(key,"IMAGE",5)) {
	    xt[i].type=EXT_IMAGE;
	  } else if (!strcmp(key,"BINTABLE")) {    
	    xt[i].type=EXT_BINTABLE;
	  } else {
	    snprintf(errmsg, MSGLEN,"Error: unknown FITS extension type: %s ", key);
	    throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	  }
	} else if (keysift(hdr,"BITPIX")!=NULL) {
	  xt[i].bitpix=atoi(key);
	} else if (keysift(hdr,"NAXIS")!=NULL) {
	  continue;
	} else if (keysift(hdr,"BSCALE")!=NULL) {
	  xt[i].bscale=atof(key);
	} else if (keysift(hdr,"BZERO")!=NULL) {
	  xt[i].bzero=atof(key);
	} else if (keysift(hdr,"DETSEC")!=NULL) {
	  detsec_found = true;
	  sscanf(key,"[%d:%d,%d:%d]",&x1,&x2,&y1,&y2);
	} else if (keysift(hdr,"CCDSUM")!=NULL) {
	  sscanf(key,"%d %d",&xbin,&ybin);
	} else if (keysift(hdr,"PCOUNT")!=NULL) {
	  continue;
	} else if (keysift(hdr,"GCOUNT")!=NULL) {
	  continue;
	} else if (keysift(hdr,"INHERIT")!=NULL) {
	  continue;
	} else if (keysift(hdr,"EXTNAME")!=NULL) {
	  continue;
	} else if (keysift(hdr,"EXTVER")!=NULL) {
	  continue;
	} else if (keysift(hdr,"IMAGEID")!=NULL) {
	  continue;
	} else if (keysift(hdr,"TFIELDS")!=NULL) {
	  btable[i].tfields = atoi(key);
	  continue;
	}

	// this is ugly, having to loop over all possible indices to
	// search for the TTYPE/TFORM keywords, but the header might
	// have these before the TFIELDS kw, so we dont know how many
	// of them there are yet.  We could probably clean this up by
	// doing this as part of the TFIELDS processing, but it makes
	// the code more complicated and I dont have time right now.
	for (j=1; j<=FITS_MAX_TFIELDS; j++) {
	  snprintf(ttypekw, FITSKEYWORDLEN,"TTYPE%d",j);
	  if (keysift(hdr,ttypekw)!=NULL) {
	    btable[i].ttype[j].assign(key);
	    flag = true;
	    break;
	  }
	  snprintf(tformkw, FITSKEYWORDLEN,"TFORM%d",j);
	  if (keysift(hdr,tformkw)!=NULL) {
	    btable[i].tform[j].assign(key);	      
	    flag = true;
	    break;
	  }
	}
	if (flag) {
	  continue;
	} else {
	  xt[i].keysextra++;
	}
      } while (!check_end(hdr));
      

      // Some validity checks
      if ((xt[i].nx < 0) || (xt[i].ny < 0)) {
	snprintf(errmsg, MSGLEN, "Illegal axis size values %d,%d for extension %d ???", xt[i].nx, xt[i].ny, i);
	throw Error(errmsg, E_ERROR, EINVAL, __FILE__, __LINE__);
      }

      if ((xt[i].bitpix != BYTE_BITPIX) && (xt[i].bitpix != SHORT_BITPIX) && (xt[i].bitpix != INT_BITPIX) && 
	  (xt[i].bitpix != FLOAT_BITPIX) && (xt[i].bitpix != DOUBLE_BITPIX)) {
	snprintf(errmsg, MSGLEN, "Illegal BITPIX value %d for extension %d ???", xt[i].bitpix, i);
	throw Error(errmsg, E_ERROR, EINVAL, __FILE__, __LINE__);
      }

      if (!done) {
	// Initialise size parameters
	xt[i].ihu =  (int) ceil((double)xt[i].keysexist*FITSKEYWORDLEN/FITSBLOCK);
	xt[i].data_size = BYTE_PIX(xt[i].bitpix)*xt[i].nx*xt[i].ny;
	xt[i].hdr_size = xt[i].ihu*FITSBLOCK;
	
	// Make sure multiple of FITSBLOCK size
	if (xt[i].data_size%FITSBLOCK) 
	  xt[i].data_size = (xt[i].data_size/FITSBLOCK+1)*FITSBLOCK;
	total_data_size += xt[i].data_size;
	total_hdr_size += xt[i].hdr_size;
	
	// Adjust image size by binning
	if (xbin && ybin) {
	  x1 = (int)ceil((double)x1/xbin);
	  x2 = (int)ceil((double)x2/xbin);
	  y1 = (int)ceil((double)y1/ybin);
	  y2 = (int)ceil((double)y2/ybin);
	}
	xt[i].detx = x1;
	xt[i].dety = y1;
	mosaic_able = (mosaic_able && detsec_found && (xt[i].type == EXT_IMAGE));
	minx = MIN(x1,minx);
	maxx = MAX(x2,maxx);
	miny = MIN(y1,miny);
	maxy = MAX(y2,maxy);
      }
      
      // Continue looking for extensions if 
      // 1. nextend not specified and we are not at end of file, or
      // 2. nextend specified and we haven't yet got there
      if ((!nextend && (!done)) || (i<nextend)) 
	i++;
      else 
	done = true;
    } while (!done);
    
    // set number of extensions to number found
    nextend = i;
  }
  
  // Work out size of image to mosaic
  if (extend && mosaic_able) {
    xt[0].detx = maxx-minx+1;
    xt[0].dety = maxy-miny+1;    
    // Remap each extensions detx and dety into coords based on mosaic origin
    for (i=1; i<=nextend; i++) {
      xt[i].detx -= minx;
      xt[i].dety -= miny;
    }
  }

  // Check to see if we can position to last byte - file might be truncated.
  if ((n=lseek(fd, total_hdr_size + total_data_size - 1, SEEK_SET)) == IPC_ERR) {
    snprintf(errmsg, MSGLEN,"Error positioning FITS file %s to end of data %ld - truncated file, status=%d", fitsname,
	    (long) total_hdr_size + total_data_size - 1, errno);
    throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
  }
  // Read last byte
  if (((n=::read(fd,hdr,1)) == IPC_ERR) || (n!=1)) {
    snprintf(errmsg, MSGLEN,"Error reading last byte of file %s - file invalid, status=%d", fitsname,errno);
    throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
  }

  // Set file_open 
  file_open_read = TRUE;

  if (use_vm) {
    // Close the file
    if (::close(fd)== IPC_ERR) {
      snprintf(errmsg, MSGLEN,"Error closing Fits file %s", fitsname);
      throw Error(errmsg, E_WARNING, errno, __FILE__, __LINE__);
    }
  }
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: Fits::check_end
 * 
 * INVOCATION: fits->check_end(string hdr)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > str - Fits header line
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Check for END keyword
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: Expects the keyword "END" to be in a fixed position in the
 *   keyword.
 *
 *-------------------------------------------------------------------------*/
int Fits::check_end(string hdr)
{
  if (hdr.substr(0,8)=="END     ") return TRUE;
  else return FALSE;
}  
/*
 *+ 
 * FUNCTION NAME: Fits::read
 * 
 * INVOCATION: fits->read(char *filename, int* outbitpix, int do_scaling, int lock_it, int od, int md, unsigned long ext_mask)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > filename - Name of FITS file to read
 * > outbitpix - array of output bitpix required - file is opened with data converted to these types
 * > do_scaling - use bzero/bscale on pixels when reading
 * > lock_it - if mmap available lock pages
 * > od - Do we want to read data extensions or just headers?
 * > md - Do we want to mosaic multi-extension files?
 * > ext_mask - bitmask indicating which extensions to read in an MEF file
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Read a fits file
 * 
 * DESCRIPTION: Reads an existing fits file and sets pointers to both
 * header sections and pixel data for each required FITS
 * extension. For mmap enabled systems, optionally uses the virtual
 * memory mmap call to map directly to FITS extensions that don't
 * require scaling/translating or byte swapping.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *-
 */
void Fits::read(char *filename, int* outbitpix, int do_scaling, int lock_it, int od, int md, unsigned long ext_mask)
{
  int npix,i,mode;
  int scaled=FALSE;
  char* bdata;
  short* sdata;
  int* idata;
  float* fdata;
  double * ddata;
  char* file_data;
  int wid, hei, owid;
  bool free_outbitpix = false;
  bool same_bitpix;
  int n;
  Byte_Order_Type local_byte_order=byte_order();  
  long adjusted_offset,adjustment;
  long pagesize = 1;

#ifndef vxWorks
  pagesize = sysconf(_SC_PAGESIZE);

  if (!pagesize) {
    throw Error("Fits class call to sysconf, returned zero pagesize!", E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
#endif

  // First some sanity checks
  if (file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file already open for write: %s", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  // If already read - just return
  if (headers_read) 
    return; 

  // If not opened yet then open here
  if (!file_open_read) 
    open(filename);

  // If data in FITS file scaled or translated then cannot use VM mapping
  // because calling routine assumes data is not scaled/translated
  scaled = FALSE;
  if (do_scaling) {
    for (i=0; i<=nextend; i++) {
      if (!i || ((1<<(i-1)) & ext_mask))
	scaled = (xt[i].bscale != 1.0) || (xt[i].bzero != 0.0);
      if (scaled) break;
    }
  }

  // allow for the output type to be different to the input fits type e.g. if we
  // have a scaled short FITS file the output range may be outside the limits
  // allowed by a short type, and a user may specify real, say, as the output
  // type.  If NULL then the input types are used
  if (outbitpix == NULL) {
    free_outbitpix = true;
    outbitpix = new int[nextend+1];
    for (i=0; i<=nextend; i++)
      outbitpix[i] = xt[i].bitpix;
  }

  // Check to see if in and out bitpix are the same
  same_bitpix = true;
  for (i=0; i<=nextend; i++) {
    if (outbitpix[i] != xt[i].bitpix) {
      same_bitpix = false;
      break;
    }
  }

  // Do we want to mosaic multi-extension files?
  mosaic_data = md && extend && mosaic_able;
  if (mosaic_data) 
    ext_mask=FITS_MAX_EXTEND_MASK;

  // can only use VM mapping if data is not scaled and in same byte order as this machine - and is not mosaiced
  if (use_vm) {
    data_vm_mapped = (!scaled && (local_byte_order==WORD_BIG_ENDIAN) && same_bitpix && !mosaic_data);
  }

  // Do we want to map to data or just headers?
  open_data = od;

  // Set the file mode
  mode = (file_open_update)? S_IRWXU:0;

  // Now set up mappings to header and data parts
  total_data_size = total_hdr_size = 0;
  for (i=0; i<=nextend; i++) {
    if (use_vm) {
      adjusted_offset = (total_hdr_size + total_data_size)/pagesize * pagesize;
      adjustment = (total_hdr_size + total_data_size) - adjusted_offset;
#ifndef vxWorks
      xt[i].vmheader = new Virtual_Mem(filename, false, xt[i].hdr_size + adjustment, adjusted_offset,mode);
      xt[i].header = xt[i].header_start = (char *) (xt[i].vmheader->buf + adjustment);
#endif
      total_hdr_size += xt[i].hdr_size;
      adjusted_offset = (total_hdr_size + total_data_size)/pagesize * pagesize;
      adjustment = (total_hdr_size + total_data_size) - adjusted_offset;
    } else {
      xt[i].header_start = new char[xt[i].hdr_size];
      xt[i].header = xt[i].header_start;
      memset((void*)xt[i].header,0,xt[i].hdr_size);
      
      // Seek and copy data to header 
      if ((n=lseek(fd, total_hdr_size + total_data_size, SEEK_SET)) == IPC_ERR) {
	snprintf(errmsg, MSGLEN,"Error positioning FITS file %s to image header unit %d!", fitsname, i);
	throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
      }
      if ((n=::read(fd, xt[i].header, xt[i].hdr_size)) == IPC_ERR) {
	snprintf(errmsg, MSGLEN,"Error reading header %d: %s!", i, fitsname);
	throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
      }
      total_hdr_size += xt[i].hdr_size;
    }

    // Now read the data segment for this extension if required
    if (open_data) {
      if ((!i || ((1<<(i-1)) & ext_mask)) && (xt[i].data_size>0)) {
	if (use_vm && data_vm_mapped) {
#ifndef vxWorks
	  xt[i].vmdata = new Virtual_Mem(filename, false, xt[i].data_size + adjustment, adjusted_offset, 0, lock_it);
	  xt[i].data = (char *)(xt[i].vmdata->buf + adjustment);	
#endif 
	} else { 
	  if (use_vm) {
	    // temporarily map to data part for data copy
#ifndef vxWorks
	    xt[i].vmdata = new Virtual_Mem(filename, false, xt[i].data_size + adjustment, adjusted_offset, mode, lock_it);
	    file_data = (char *)(xt[i].vmdata->buf + adjustment);
#endif 
	  } else {
	    file_data = new char[xt[i].data_size];

	    // Seek to data position and read data
	    if ((n=lseek(fd, total_hdr_size + total_data_size, SEEK_SET)) == IPC_ERR) {
	      snprintf(errmsg, MSGLEN,"Error positioning FITS file %s to image data extension %d!", fitsname, i);
	      throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	    }
	    if ((n=::read(fd, file_data, xt[i].data_size)) == IPC_ERR) {
	      snprintf(errmsg, MSGLEN,"Error reading data extension %d: %s!", i, fitsname);
	      throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	    }  
	  }

	  wid = xt[i].nx;
	  hei = xt[i].ny;

	  // now create new buffer of correct size
	  if (mosaic_data) {
	    if (i==1) {
	      // We only want one big buffer for a mosaiced file - allocate once
	      npix = xt[0].detx * xt[0].dety;
	      owid = xt[0].detx;
	      if (!npix) {
		throw Error("No DETSIZE keyword in header - cannot mosaic data!", E_ERROR, IPC_ERR, __FILE__, __LINE__);    
	      } 
	      
	      // Because we have extensions make the PHU bitpix = first image extension bitpix
	      outbitpix[0] = outbitpix[1];
	      
	      // Now allocate an homogenous image buffer for full mosaiced image
	      mosaic_datap = new char[BYTE_PIX(outbitpix[i])*npix];
	      xt[0].data = mosaic_datap;
	    } else if (outbitpix[i] != outbitpix[1]) {
	      // We have a homogenous buffer! Force the output data type to be consistent
	      outbitpix[i] = outbitpix[1]; 
	    }

	    // Calculate starting location in mosaic
	    xt[i].data = mosaic_datap + BYTE_PIX(outbitpix[i])*(xt[i].detx + owid*xt[i].dety);
	  } else {
	    npix = wid*hei;
	    owid = wid;
	    xt[i].data = new char[BYTE_PIX(outbitpix[i])*npix];
	  }

	  // Set base address of data for each data type
	  bdata = xt[i].data;
	  sdata = (short *) xt[i].data;
	  idata = (int *) xt[i].data;
	  fdata = (float *) xt[i].data;
	  ddata = (double *) xt[i].data;
	  
	  // Now scale and copy file data to output data array
	  switch (outbitpix[i]) {
	  case BYTE_BITPIX: scale_in(bdata, file_data, owid, wid, hei, xt[i].bitpix, xt[i].bscale, xt[i].bzero);
	    break;
	  case SHORT_BITPIX: scale_in(sdata, file_data, owid, wid, hei, xt[i].bitpix, xt[i].bscale, xt[i].bzero);
	    break;
	  case INT_BITPIX: scale_in(idata, file_data, owid, wid, hei, xt[i].bitpix, xt[i].bscale, xt[i].bzero);
	    break;
	  case FLOAT_BITPIX: scale_in(fdata, file_data, owid, wid, hei, xt[i].bitpix, xt[i].bscale, xt[i].bzero);
	    break;
	  case DOUBLE_BITPIX: scale_in(ddata, file_data, owid, wid, hei, xt[i].bitpix, xt[i].bscale, xt[i].bzero);
	    break;
	  default:;
	  }
	  
	  if (use_vm) {
	    // Remove temporary mapping
#ifndef vxWorks
	    delete xt[i].vmdata;
#endif
	  } else {
	    delete [] file_data; 
	  }
	} // else block after condition (use_vm && data_vm_mapped)
      }
    }

    // data_size calculated from open
    total_data_size += xt[i].data_size; 
  }

  headers_read = TRUE;
  if (free_outbitpix)
    delete [] outbitpix;
}
/*
 *+ 
 * FUNCTION NAME: Fits::close
 * 
 * INVOCATION: close(int abort)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > abort - true if aborting an image
 *
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Closes a fits image and checks structure
 * 
 * DESCRIPTION: Writes the END keword for each FITS extension and
 *  checks that FITS structure is legal. Resets all internal membar
 *  vars.
 *  Pads the header block with spaces after all the keywords
 *   (see the FITS standard at
 *   http://archive.stsci.edu/fits/users_guide/node42.html.)
 *  When writing Fits files during data acquisition, padding and the "END"
 *  keyword:
 *  The Fits files have a pre-determined number of keywords per block that may
 *  or may not be satisfied when the file is finally closed.
 *  If, for some reason, less than the expected number of header keywords
 *  are written, (perhaps the telescope failed to deliver some),
 *  then it is important to have the END keyword at the end of the second
 *  block, otherwise the Fits file would be invalid. It is quite ok to pad with
 *  blanks between the last valid keyword and the END keyword.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *-
 */
void Fits::close(int abort)
{
  int i,hu,j,diff,ow;
  char *pos;
  int n;
   
  if (!file_open_read && !file_open_write && !file_open_update) {
    return;
  }

  // Set these to false in case an exception is thrown
  file_open_read = FALSE;
  ow = file_open_write;
  file_open_write = FALSE;
  file_open_update = FALSE;

  // Close file after checking headers
  for (i=0; i<=nextend; i++) {
    if (xt[i].headers_written) {

      // Check that FITS file was correctly contructed
      hu = (i) ? xt[i].ihu:phu;
      
      // Too few keywords? That is - we have not written any keywords in the
      // last block of the header unit. Write some blanks to ensure that headers
      // are written into the start of the last block.
      diff = (hu-1)*FITS_KEYS_PER_BLOCK - xt[i].headers_written + 1;
      for (j=0; j<diff; j++) {
	hdr_line(xt[i].header, &xt[i].header, " "); 
	xt[i].headers_written++;
      } 
      
      // Write the end keyword - may already have been written at the end of the
      // header unit as part of std_keywords - if so remove it, by overwriting
      // with blanks.  Note that the only place that the END keyword could have
      // been written would have been at the end of the header unit.
      if (xt[i].end_written) {
	diff = (hu*FITS_KEYS_PER_BLOCK - xt[i].headers_written)*FITSKEYWORDLEN;
	pos = xt[i].header + diff;
	hdr_line(pos, &pos, "   "); 
	xt[i].headers_written--;
      }

      // Now write END as last keyword - in the last block.
      hdr_line(xt[i].header, &xt[i].header, "END"); 
      xt[i].headers_written++;

      // Now pad out the rest of the header unit with blanks
      diff = hu*FITS_KEYS_PER_BLOCK - xt[i].headers_written;
      for (j=0; j<diff; j++) {
	hdr_line(xt[i].header, &xt[i].header, " "); 
	xt[i].headers_written++;
      } 

      // Too many keywords? Perhaps more keywords were written than allowed for
      // by the preset size of the file - bomb out if so.
      if (!abort && (xt[i].headers_written > hu*FITS_KEYS_PER_BLOCK)) {
	snprintf(errmsg, MSGLEN,"Poorly constructed FITS file %s! %d keywords, when max %d expected!", 
		fitsname, xt[i].headers_written, hu*FITS_KEYS_PER_BLOCK);
	throw Error(errmsg, E_WARNING, IPC_ERR, __FILE__, __LINE__);
      }   

    }
  }

  // if writing a new file rewrite the simple keyword to validate the file. This
  // would have been omitted when writing the standard keywords to prevent any
  // premature opening and interpretation of a partially built file.
  if (fix_simple_keyword && ow && !abort) {
    file_open_write = TRUE;
    xt[0].header = xt[0].header_start;
    write_bkeyword("SIMPLE", TRUE, "FITS format");
    file_open_write = FALSE;
  }

  if (!use_vm) {
    // Because header data has been written to memory - dump into file here for
    // vxworks because mmap not supported.
    if (ow) {
      total_data_size = total_hdr_size = 0;
      for (i=0; i<=nextend; i++) {
	// Now write the header structure
	if ((n=lseek(fd, total_hdr_size + total_data_size, SEEK_SET)) == -1) {
	  snprintf(errmsg, MSGLEN,"Error positioning FITS file %s to image header!", fitsname);
	  throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	}
	if ((n=write(fd, xt[i].header_start, xt[i].hdr_size)) == -1) {
	  snprintf(errmsg, MSGLEN,"Error writing FITS file %s, extension %d!",fitsname, i);
	  throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	}
	total_data_size += xt[i].data_size;
	total_hdr_size += xt[i].hdr_size;
      }
    }
    
    // Now close the file
    if (::close(fd)== IPC_ERR) {
      snprintf(errmsg, MSGLEN,"Error closing Fits file %s", fitsname);
      throw Error(errmsg, E_WARNING, errno, __FILE__, __LINE__);
    }
  }
    
  // Now remove any allocated data structures if data wasn't mapped
  for (i=0; i<=nextend; i++) {

    // Remove the header mapping
    if (use_vm) {
#ifndef vxWorks
      if (xt[i].vmheader!=NULL) 
	delete xt[i].vmheader; 
#endif
    } else 
      delete [] xt[i].header_start;

    if ((i==0) && extend) {
      if (mosaic_data) 
	delete [] mosaic_datap; 
    }
    if ((!open_data) || (!headers_read) || (mosaic_data)) 
      continue;

    // remove the data mapping for the individual extension
    if (use_vm && data_vm_mapped) {
#ifndef vxWorks
      if (xt[i].vmdata!=NULL) 
	delete xt[i].vmdata; 
#endif
    } else {
      if (xt[i].data!=NULL) 
        delete [] xt[i].data; 
    }
  }

  // Reset relevant internal variables
  initialise(nextend);
}
  
/*
 *+ 
 * FUNCTION NAME: void Fits::create(char *filename)
 * 
 * INVOCATION: fits->create(filename)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > filename - name of new fits file
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Create a new empty fits file
 * 
 * DESCRIPTION: Opens and sizes a new FITS file - if facility is
 * available mmaps to header segments and optionally mmaps to data
 * segments.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::create(char *filename, Ext_Type *et, int* bp, int *ne, int mode, int phk, int ext, int ihk, int od)
{
  int i;
  long adjusted_offset,adjustment;
  long pagesize = 1;
#ifndef vxWorks
  pagesize = sysconf(_SC_PAGESIZE);
#endif

  // Some sanity checks - Make sure not already opened
  if (file_open_read) {
    snprintf(errmsg, MSGLEN, "Fits file already open for read: %s", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
  if (file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file already open for write: %s", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  // Calculate sizes of file parts
  strlcpy(fitsname,filename,FITSNAMELEN);
  phu = (int) ceil((double)phk*FITSKEYWORDLEN/FITSBLOCK);
  if (phu==0) 
    phu = 1;
  nextend = ext; 
  extend = (nextend>0);

  // Allocate space for extension data
  alloc_xtension(nextend);

  // We only support 2 dimensional images
  naxis = 2; 
  // total_data_size = 0; // Surely this should have been set before this routine ?
  // total_hdr_size = 0;
  for (i=0; i<=nextend; i++) {
    xt[i].nx = ne[i*2];
    xt[i].ny = ne[i*2+1];
    xt[i].bitpix = bp[i];
    xt[i].type = et[i];
    xt[i].data_size = BYTE_PIX(bp[i])*ne[i*2]*ne[i*2+1];
    xt[i].ihu = (int) ceil((double)ihk*FITSKEYWORDLEN/FITSBLOCK);
    xt[i].hdr_size = (i)? xt[i].ihu*FITSBLOCK:phu*FITSBLOCK;

    // Make sure multiple of FITSBLOCK size
    if (xt[i].data_size%FITSBLOCK) 
      xt[i].data_size = (xt[i].data_size/FITSBLOCK+1)*FITSBLOCK;
    total_data_size += xt[i].data_size;
    total_hdr_size += xt[i].hdr_size;
  }

  if (use_vm) {
#ifndef vxWorks
    // Memory map a new empty file, a multiple of FITSBLOCK size
    Virtual_Mem* vmfits = new Virtual_Mem(fitsname, true, total_hdr_size + total_data_size, 0, mode);
    
    // Now remove the mapping because file is created.
    delete vmfits;
    data_vm_mapped = FALSE;
#endif
  } else {
    // Create and Size a new file
    if ((fd = ::open(fitsname, O_WRONLY | O_CREAT | O_TRUNC, mode)) == IPC_ERR) {
      snprintf(errmsg, MSGLEN,"Unable to create Fits file: %s",fitsname);
      throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
    }
    
#ifdef vxWorks
    //Loop to write NULL bytes to file (ftruncate would normally do this, but it
    //doesn't work under vxWorks
    const int BUF_LEN = 32767;
    char buffer[BUF_LEN] = {0};
    memset((void*)&buffer, 0, BUF_LEN);
    int nw;
    int n = 0;
    while (n < (total_hdr_size + total_data_size)) {
      nw = MIN(BUF_LEN, total_hdr_size + total_data_size - n);
      if (write(fd, buffer, nw) == IPC_ERR) {
	snprintf(errmsg, MSGLEN,"Failed to truncate file %s to length %d", fitsname, total_hdr_size + total_data_size);
	throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
      }
      n += nw;
    }
#else 
    if (chmod(fitsname, mode) == IPC_ERR) {
      snprintf(errmsg, MSGLEN,"Failed to chmod %s",fitsname);
      throw Error (errmsg,E_ERROR,errno,__FILE__,__LINE__);
    }
    if (ftruncate(fd, total_hdr_size + total_data_size) == IPC_ERR) {
      snprintf(errmsg, MSGLEN,"Failed to truncate file %s to length %d", fitsname, total_hdr_size + total_data_size);
      throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
    }
#endif
  }

  // Now set up mappings to header and data parts
  open_data = od;
  total_data_size = total_hdr_size = 0;
  for (i=0; i<=nextend; i++) {
    if (use_vm) {
      // adjust offset to multiple of system pagesize
      adjusted_offset = (total_hdr_size + total_data_size)/pagesize * pagesize;
      adjustment = (total_hdr_size + total_data_size) - adjusted_offset;
#ifndef vxWorks
      xt[i].vmheader = new Virtual_Mem(fitsname, false, xt[i].hdr_size + adjustment, adjusted_offset,mode);
      xt[i].header = xt[i].header_start = (char *) (xt[i].vmheader->buf + adjustment);
#endif
      total_hdr_size += xt[i].hdr_size;
      adjusted_offset = (total_hdr_size + total_data_size)/pagesize * pagesize;
      adjustment = (total_hdr_size + total_data_size) - adjusted_offset;

      // Optionally map to data segments
      if (open_data) {
#ifndef vxWorks
	xt[i].vmdata = new Virtual_Mem(fitsname, false, xt[i].data_size + adjustment, adjusted_offset, mode);
	xt[i].data = (char *)(xt[i].vmdata->buf + adjustment);
	data_vm_mapped = TRUE;
#endif
      }
    } else { 
      xt[i].header_start = new char[xt[i].hdr_size];
      xt[i].header = xt[i].header_start;
      memset((void*)xt[i].header,0,xt[i].hdr_size);
      total_hdr_size += xt[i].hdr_size;
    }

    total_data_size += xt[i].data_size;
  }

  // Set file_open 
  file_open_write = file_open_update = TRUE;
  headers_read = TRUE;
}

/*
 *+ 
 * FUNCTION NAME: Fits::write_std_keywords
 * 
 * INVOCATION: fits->write_std_keywords(Ext_Type *et, double *bs, double *bz, int* pc, int* gc, int decimals)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > et - type of FITS extensions
 * > bs - bscale for each extension
 * > bz - bzero for each extension
 * > pc - pcount for each extension
 * > gc - gcount for each extension
 * > decimals - number of decimal place precision for floats
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Write the standard Fits header keywords to a fits file
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::write_std_keywords(Ext_Type *et, double *bs, double *bz, int* pc, int* gc, int decimals)
{
  int i,ext_num,kwnum;
  Fitsline line;
  char *pos;
  int diff;
  string etstr;  // fits table extension type as a string
  Fitsline kw, kwcomment; // temp strings for keywords and kw comments
  

  // Usual sanity checks
  if (!file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
    
  // Loop over each extension to set member variables
  for (i=0; i<=nextend; i++) {
    // PHU cant be an extension   
    if (i!=0) { 
      xt[i].type=et[i]; 
    }
    xt[i].bscale = bs[i];
    xt[i].bzero = bz[i];
    xt[i].pcount = (pc==NULL)? 0:pc[i];
    xt[i].gcount = (gc==NULL)? 1:gc[i];
  }
  
  // Reset header pointer to start and header counter to zero
  xt[0].header = xt[0].header_start;
  xt[0].headers_written = 0;

  // Write the mandatory keywords.

  // For SIMPLE - just place a COMMENT- to be overwritten when file is closed. 
  // This is to prevent any early interpretation of a partially built file.
  fix_simple_keyword = TRUE;
  write_skeyword("COMMENT", "FILE NOT READY", "FITS file being written");
  // For extension images make the PHU bitpix same as first extension
  xt[0].bitpix = (extend)? xt[1].bitpix:xt[0].bitpix;
  if (xt[0].bitpix == 0) {
    snprintf(errmsg, MSGLEN, "Fits file %s, must have BITPIX != 0!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
  write_ikeyword("BITPIX", xt[0].bitpix, "Number of bits per pixel",0);
  if (!extend) { // non-MEF file
    write_ikeyword("NAXIS", 2, "Number of image axes",0);
    write_ikeyword("NAXIS1", xt[0].nx, "Number of pixels",0);
    write_ikeyword("NAXIS2", xt[0].ny, "Number of pixels",0);
    write_bkeyword("EXTEND", FALSE, "FITS extensions",0);
    if (xt[0].bitpix>0) {
      write_dkeyword("BSCALE", xt[0].bscale, decimals, "Pixel scale factor",0);
      write_dkeyword("BZERO", xt[0].bzero, decimals, "Pixel scale offset",0);
    }
    write_ikeyword("PCOUNT", xt[0].pcount, "Number of pixels following data",0);
    write_ikeyword("GCOUNT", xt[0].gcount, "Number of groups",0);
  } else { // phu of MEF file
    write_ikeyword("NAXIS", 0, "Number of image axes",0);    
    write_bkeyword("EXTEND", TRUE, "FITS extensions",0);
    write_ikeyword("NEXTEND", nextend, "Number of image extensions",0);
    if (xt[0].bitpix>0) {
      write_dkeyword("BSCALE", xt[0].bscale, decimals, "Pixel scale factor",0);
      write_dkeyword("BZERO", xt[0].bzero, decimals, "Pixel scale offset",0);
    }
  }

  // Write END to end of PHU - so that FITS file is legal
  diff = (phu*FITS_KEYS_PER_BLOCK - (xt[0].headers_written + 1))*FITSKEYWORDLEN;
  pos = xt[0].header + diff;
  hdr_line(pos, &pos, "END"); 
  xt[0].headers_written++;
  xt[0].end_written = TRUE;

  // Now write any image extension keywords
  for (ext_num=1; ext_num<=nextend; ext_num++) {
    // Reset header pointer
    xt[ext_num].header = xt[ext_num].header_start;
    xt[ext_num].headers_written = 0;

    switch(xt[ext_num].type) {
    case EXT_IMAGE:
      etstr="IMAGE   ";
      break;
    case EXT_BINTABLE:
      etstr="BINTABLE";
      break;
    default:
      snprintf(errmsg, MSGLEN, "Error: Unknown FITS extension type: %d for extension number %d", xt[ext_num].type,ext_num);
      throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
    }
      
    // Write the mandatory keywords
    write_skeyword("XTENSION", etstr.c_str(), "FITS extension type", ext_num);
    write_ikeyword("BITPIX", xt[ext_num].bitpix, "Number of bits per pixel", ext_num);

    // sanity check: FITS stds requires BITPIX=8 for a BINTABLE
    if ((xt[ext_num].type==EXT_BINTABLE) && (xt[ext_num].bitpix != 8)) {
      snprintf(errmsg, MSGLEN, "Error: Illegal BITPIX=%d for BINTABLE extension %d.\nBINTABLE extensions require BITPIX=8", 
	      xt[ext_num].bitpix,ext_num);
      throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);
    }
    write_ikeyword("NAXIS", 2, "Number of image axes", ext_num);
    write_ikeyword("NAXIS1", xt[ext_num].nx, "Number of pixels", ext_num);
    write_ikeyword("NAXIS2", xt[ext_num].ny, "Number of pixels", ext_num);
    if (xt[ext_num].bitpix>0 && xt[ext_num].type == EXT_IMAGE) {
      write_dkeyword("BSCALE", xt[ext_num].bscale, decimals, "Pixel scale factor",ext_num);
      write_dkeyword("BZERO", xt[ext_num].bzero, decimals, "Pixel scale offset",ext_num);
    }
    write_ikeyword("PCOUNT", xt[ext_num].pcount, "Number of pixels following data",ext_num);
    write_ikeyword("GCOUNT", xt[ext_num].gcount, "Number of groups",ext_num);
    if (xt[ext_num].type==EXT_IMAGE) {    
      write_bkeyword("INHERIT", TRUE, "Inherit global keywords", ext_num);
    }else if (xt[ext_num].type==EXT_BINTABLE) {
      write_ikeyword("TFIELDS", btable[ext_num].tfields,"number of fields in each row",ext_num);
      for (kwnum=1; (kwnum<=btable[ext_num].tfields) && (kwnum<=FITS_MAX_TFIELDS); kwnum++) {
	snprintf(kw, FITSKEYWORDLEN,"TTYPE%d",kwnum);	
	snprintf(kwcomment, FITSKEYWORDLEN,"label for field %d",kwnum);
	write_skeyword(kw, btable[ext_num].ttype[kwnum].c_str(), kwcomment, ext_num);
	snprintf(kw, FITSKEYWORDLEN,"TFORM%d",kwnum);
	snprintf(kwcomment, FITSKEYWORDLEN,"data format for field %d",kwnum);
	write_skeyword(kw, btable[ext_num].tform[kwnum].c_str(), kwcomment, ext_num);
      }
    }
    snprintf(line, FITSKEYWORDLEN,"im%d",ext_num);
    write_skeyword("EXTNAME", line, "Extension name", ext_num);
    write_ikeyword("EXTVER", ext_num, "Extension version", ext_num);
    write_ikeyword("IMAGEID", ext_num, "Image identification", ext_num);

    // Write END to end of IHU - so that FITS file is legal
    diff = (xt[ext_num].ihu*FITS_KEYS_PER_BLOCK - (xt[ext_num].headers_written + 1))*FITSKEYWORDLEN;
    pos = xt[ext_num].header + diff;
    hdr_line(pos, &pos, "END"); 
    xt[ext_num].headers_written++;
    xt[ext_num].end_written = TRUE;
  }
  //  cout << "Standard headers written" <<endl;
}

/*
 *+ 
 * FUNCTION NAME: Fits::write_keyword_line
 * 
 * INVOCATION: fits->write_keyword_line(const char *tline, int ext_num)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > tline - header line to write
 * > ext_num - extension number
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Write an arbitrary preformated keyword
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::write_keyword_line(const char *tline, int ext_num)
{
  Fitsline line;
  int hu = (ext_num>0)? xt[ext_num].ihu:phu;

  strlcpy(line,tline,FITSKEYWORDLEN);

  // Usual sanity checks
  if (!file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
    
  hdr_line(xt[ext_num].header,&xt[ext_num].header,line); 
  xt[ext_num].headers_written++;
  xt[ext_num].keysexist++;

  // Have we written too many for size reserved?
  if (xt[ext_num].headers_written == hu*FITS_KEYS_PER_BLOCK) {
    snprintf(errmsg, MSGLEN,"Poorly constructed FITS file %s! %d keywords, when max %d expected!", 
	    fitsname, xt[ext_num].headers_written+1, hu*FITS_KEYS_PER_BLOCK);
    throw Error(errmsg, E_WARNING, IPC_ERR, __FILE__, __LINE__);
  }   
}	
/*
 *+ 
 * FUNCTION NAME: Fits::write_ikeyword
 * 
 * INVOCATION: fits->write_ikeyword(const char *keyword, int keyval, const char* comment, int ext_num)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > keyword - FITS keyword to include
 * > keyval - value
 * > comment - keyword comment
 * > ext_num - MEF extension number
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Write an integer keyword
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::write_ikeyword(const char *keyword, int keyval, const char* comment, int ext_num)
{
  Fitsline line;
  int hu = (ext_num>0)? xt[ext_num].ihu:phu;

  // Usual sanity checks
  if (!file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
      
  snprintf(line, FITSKEYWORDLEN, "%-8s=%21d / %s",keyword,keyval,comment);
  hdr_line(xt[ext_num].header,&xt[ext_num].header,line); 
  xt[ext_num].headers_written++;
  xt[ext_num].keysexist++;

  // Have we written too many for size reserved?
  if (xt[ext_num].headers_written == hu*FITS_KEYS_PER_BLOCK) {
    snprintf(errmsg, MSGLEN,"Poorly constructed FITS file %s! %d keywords, when max %d expected!", 
	    fitsname, xt[ext_num].headers_written+1, hu*FITS_KEYS_PER_BLOCK);
    throw Error(errmsg, E_WARNING, IPC_ERR, __FILE__, __LINE__);
  }   
}	
/*
 *+ 
 * FUNCTION NAME: Fits::update_ikeyword
 * 
 * INVOCATION: fits->update_ikeyword(const char *keyword, int keyval, const char* comment, int ext_num, int wr)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > keyword - FITS keyword to include
 * > keyval - value
 * > comment - keyword comment
 * > ext_num - MEF extension number
 * > wr - set if ok to write new keyword
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Update an integer keyword
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::update_ikeyword(const char *keyword, int keyval, const char* comment, int ext_num, int wr)
{
  Fitsline line;
  char *hdr;

  // Usual sanity checks
  if (!file_open_update) {
    snprintf(errmsg, MSGLEN, "Fits file %s, (extension %d) not open for write!", fitsname,ext_num);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
      
  // Find existing keyword and rewrite
  if ((hdr = find_keyword(keyword, ext_num)) != NULL) {
    snprintf(line, FITSKEYWORDLEN, "%-8s=%21d / %s",keyword,keyval,comment);
    hdr_line(hdr,&hdr,line); 
  } else if (wr) {
    // Try to overwrite space
    if ((hdr = find_keyword("", ext_num)) != NULL) {
      snprintf(line, FITSKEYWORDLEN, "%-8s=%21d / %s",keyword,keyval,comment);
      hdr_line(hdr,&hdr,line); 
    } else { // write new keyword at end
      write_ikeyword(keyword, keyval, comment, ext_num);
    } 
  } else {
    snprintf(errmsg, MSGLEN, "Keyword %s, not found in %s, extension %d!", keyword, fitsname,ext_num);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
}

/*
 *+ 
 * FUNCTION NAME: Fits::write_bkeyword
 * 
 * INVOCATION: fits->write_bkeyword(const char *keyword, int keyval, const char* comment, int ext_num)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > keyword - FITS keyword to include
 * > keyval - value
 * > comment - keyword comment
 * > ext_num - MEF extension number
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Write an boolean keyword
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::write_bkeyword(const char *keyword, int keyval, const char* comment, int ext_num)
{
  Fitsline line;
  int hu = (ext_num>0)? xt[ext_num].ihu:phu;

  // Usual sanity checks
  if (!file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
      
  if (keyval)
    snprintf(line, FITSKEYWORDLEN, "%-8s=%21s / %s",keyword,"T",comment);
  else 
    snprintf(line, FITSKEYWORDLEN, "%-8s=%21s / %s",keyword,"F",comment);
  hdr_line(xt[ext_num].header,&xt[ext_num].header,line); 
  xt[ext_num].headers_written++;
  xt[ext_num].keysexist++;

  // Have we written too many for size reserved?
  if (xt[ext_num].headers_written == hu*FITS_KEYS_PER_BLOCK) {
    snprintf(errmsg, MSGLEN,"Poorly constructed FITS file %s! %d keywords, when max %d expected!", 
	    fitsname, xt[ext_num].headers_written+1, hu*FITS_KEYS_PER_BLOCK);
    throw Error(errmsg, E_WARNING, IPC_ERR, __FILE__, __LINE__);
  }   
}	
/*
 *+ 
 * FUNCTION NAME: Fits::update_bkeyword
 * 
 * INVOCATION: fits->update_bkeyword(const char *keyword, int keyval, const char* comment, int ext_num, int wr)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > keyword - FITS keyword to include
 * > keyval - value
 * > comment - keyword comment
 * > ext_num - MEF extension number
 * > wr - set if ok to write new keyword
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Update a boolean keyword
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::update_bkeyword(const char *keyword, int keyval, const char* comment, int ext_num, int wr)
{
  Fitsline line;
  char *hdr;

  // Usual sanity checks
  if (!file_open_update) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
      
  // Find existing keyword and rewrite
  if (keyval)
    snprintf(line, FITSKEYWORDLEN, "%-8s=%21s / %s",keyword,"T",comment);
  else 
    snprintf(line, FITSKEYWORDLEN, "%-8s=%21s / %s",keyword,"F",comment);
  if ((hdr = find_keyword(keyword, ext_num)) != NULL) {
    hdr_line(hdr,&hdr,line); 
  } else if (wr) {
    // Try to overwrite space
    if ((hdr = find_keyword("", ext_num)) != NULL) {
      hdr_line(hdr,&hdr,line); 
    } else { // write new keyword at end
      write_bkeyword(keyword, keyval, comment, ext_num);
    } 
  } else {
    snprintf(errmsg, MSGLEN, "Keyword %s, not found in %s!", keyword, fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
}

/*
 *+ 
 * FUNCTION NAME: Fits::write_dkeyword
 * 
 * INVOCATION: fits->write_dkeyword(const char *keyword, double keyval, int decimals, const char* comment, int ext_num)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > keyword - FITS keyword to include
 * > keyval - value
 * > decimals - number of decimals places of precision
 * > comment - keyword comment
 * > ext_num - MEF extension number
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Write a floating point keyword
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::write_dkeyword(const char *keyword, double keyval, int decimals, const char* comment, int ext_num)
{
  Fitsline line;
  int hu = (ext_num>0)? xt[ext_num].ihu:phu;

  // Usual sanity checks
  if (!file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
      
  // Available comment space is FITSCOMMENTLEN characters
  snprintf(line, FITSKEYWORDLEN, "%-8s=%21.*E / %s",keyword,decimals,keyval,comment);
  hdr_line(xt[ext_num].header,&xt[ext_num].header,line); 
  xt[ext_num].headers_written++;
  xt[ext_num].keysexist++;

  // Have we written too many for size reserved?
  if (xt[ext_num].headers_written == hu*FITS_KEYS_PER_BLOCK) {
    snprintf(errmsg, MSGLEN,"Poorly constructed FITS file %s! %d keywords, when max %d expected!", 
	    fitsname, xt[ext_num].headers_written+1, hu*FITS_KEYS_PER_BLOCK);
    throw Error(errmsg, E_WARNING, IPC_ERR, __FILE__, __LINE__);
  }   
}	
/*
 *+ 
 * FUNCTION NAME: Fits::update_dkeyword
 * 
 * INVOCATION: fits->update_dkeyword(const char *keyword, double keyval, int decimals, const char* comment, int ext_num, int wr)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > keyword - FITS keyword to include
 * > keyval - value
 * > decimals - number of decimals places of precision
 * > comment - keyword comment
 * > ext_num - MEF extension number
 * > wr - set if ok to write new keyword
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Update a floating point keyword
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::update_dkeyword(const char *keyword, double keyval, int decimals, const char* comment, int ext_num, int wr)
{
  Fitsline line;
  char *hdr;

  // Usual sanity checks
  if (!file_open_update) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
      
  // Find existing keyword and rewrite
  if ((hdr = find_keyword(keyword, ext_num)) != NULL) {
    snprintf(line, FITSKEYWORDLEN, "%-8s=%21.*E / %s",keyword,decimals,keyval,comment);
    hdr_line(hdr,&hdr,line); 
  } else if (wr) {
    // Try to overwrite space
    if ((hdr = find_keyword("", ext_num)) != NULL) {
      snprintf(line, FITSKEYWORDLEN, "%-8s=%21.*E / %s",keyword,decimals,keyval,comment);
      hdr_line(hdr,&hdr,line); 
    } else { // write new keyword at end
      write_dkeyword(keyword, keyval, decimals, comment, ext_num);
    } 
  } else {
    snprintf(errmsg, MSGLEN, "Keyword %s, not found in %s!", keyword, fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
}	

/*
 *+ 
 * FUNCTION NAME: Fits::write_skeyword
 * 
 * INVOCATION: fits->write_skeyword(const char *keyword, const char* keyval, const char* comment, int ext_num)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > keyword - FITS keyword to include
 * > keyval - value
 * > comment - keyword comment
 * > ext_num - MEF extension number
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Write a string keyword
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::write_skeyword(const char *keyword, const char* keyval, const char* comment, int ext_num)
{
  Fitsline line;
  int hu = (ext_num>0)? xt[ext_num].ihu:phu;
  int len;

  // Usual sanity checks
  if (!file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
      
  len = (strlen(keyval)>21)?1:21-strlen(keyval)-1;
  snprintf(line, FITSKEYWORDLEN, "%-8s= '%s'%*s %s",keyword,keyval,len,"/",comment);
  hdr_line(xt[ext_num].header,&xt[ext_num].header,line); 
  xt[ext_num].headers_written++;
  xt[ext_num].keysexist++;

  // Have we written too many for size reserved?
  if (xt[ext_num].headers_written == hu*FITS_KEYS_PER_BLOCK) {
    snprintf(errmsg, MSGLEN,"Poorly constructed FITS file %s! %d keywords, when max %d expected!", 
	    fitsname, xt[ext_num].headers_written+1, hu*FITS_KEYS_PER_BLOCK);
    throw Error(errmsg, E_WARNING, IPC_ERR, __FILE__, __LINE__);
  }   
}
/*
 *+ 
 * FUNCTION NAME: Fits::update_skeyword
 * 
 * INVOCATION: fits->update_skeyword(const char *keyword, const char* keyval, const char* comment, int ext_num, int wr)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > keyword - FITS keyword to include
 * > keyval - value
 * > comment - keyword comment
 * > ext_num - MEF extension number
 * > wr - set if ok to write new keyword
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Update a floating point keyword
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::update_skeyword(const char *keyword, const char* keyval, const char* comment, int ext_num, int wr)
{
  Fitsline line;
  char *hdr;
  int len;

  // Usual sanity checks
  if (!file_open_update) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
      
  // Find existing keyword and rewrite
  len = (strlen(keyval)>21)?1:21-strlen(keyval)-1;
  snprintf(line, FITSKEYWORDLEN, "%-8s= '%s'%*s %s",keyword,keyval,len,"/",comment);
  if ((hdr = find_keyword(keyword, ext_num)) != NULL) {
    hdr_line(hdr,&hdr,line); 
  } else if (wr) {
    // Try to overwrite space
    if ((hdr = find_keyword("", ext_num)) != NULL) {
      hdr_line(hdr,&hdr,line); 
    } else { // write new keyword at end
      write_skeyword(keyword, keyval, comment, ext_num);
    } 
  } else {
    snprintf(errmsg, MSGLEN, "Keyword %s, not found in %s!", keyword, fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
}

/*
 *+ 
 * FUNCTION NAME: Fits::read_keyword
 * 
 * INVOCATION: fits->read_keyword(const char *keyword, int ext_num)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > keyword - FITS keyword to include
 * > ext_num - MEF extension number
 * 
 * FUNCTION VALUE: char*
 * 
 * PURPOSE: Readback a keyword
 * 
 * DESCRIPTION: This routine mallocs space for keyword value.
 * This memory needs to be freed by the calling program when finished.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: memory allocated - requires caller to free
 *
 *- 
 */
char* Fits::read_keyword(const char *keyword, int ext_num)
{
  int nhdrs,i;
  char *hdr,*val;
  string tval;
  Fitsline fhdr;

  // Usual sanity checks
  if (!file_open_read && !file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for read!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
  
  // setup pointers to headers
  if (!headers_read && !file_open_write) 
    read(fitsname,&xt[ext_num].bitpix,FALSE,FALSE,FALSE);

  nhdrs = (ext_num==0)? phu*FITSBLOCK/FITSKEYWORDLEN:
    xt[ext_num].ihu*FITSBLOCK/FITSKEYWORDLEN;
  
  // Loop through header block until keyword found
  hdr = xt[ext_num].header_start;
  for (i=0; i<nhdrs; i++) {
    strlcpy(fhdr,hdr,FITSKEYWORDLEN);

    if ((keysift(fhdr,keyword))!=NULL) {
      tval.assign(key);
      val = (char*) malloc(tval.length()+1);
      strcpy(val,tval.c_str());
      return val;
    }
    
    // Increment header pointer
    hdr += FITSKEYWORDLEN;
  }

  // Return NULL string. NB make sure this points to static data
  return NULL;
}
/*
 *+ 
 * FUNCTION NAME: Fits::find_keyword
 * 
 * INVOCATION: find_keyword(const char *keyword, int ext_num)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > keyword - FITS keyword to include
 * > ext_num - MEF extension number
 * 
 * FUNCTION VALUE: char*
 * 
 * PURPOSE: This routine finds a keyword and returns its address
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
char* Fits::find_keyword(const char *keyword, int ext_num)
{
  int nhdrs,i;
  char *hdr;
  string tval;
  Fitsline fhdr;

  // Usual sanity checks
  if (!file_open_read && !file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for read!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }
  
  // setup pointers to headers
  if (!headers_read && !file_open_write) 
    read(fitsname,&xt[ext_num].bitpix,FALSE,FALSE,FALSE);

  nhdrs = (ext_num==0)? phu*FITSBLOCK/FITSKEYWORDLEN:
    xt[ext_num].ihu*FITSBLOCK/FITSKEYWORDLEN;
  
  // Loop through header block until keyword found
  hdr = xt[ext_num].header_start;
  for (i=0; i<nhdrs; i++) {
    strlcpy(fhdr,hdr,FITSKEYWORDLEN);
   
    if (strlen(keyword) == 0) {
      // Looking for blank
      tval = fhdr;
      strip_str(tval);
      if (tval.length()==0)
	return hdr;
    } else if (keysift(fhdr,keyword)!=NULL) {
      return hdr;
    }
    // Increment header pointer
    hdr += FITSKEYWORDLEN;
  }
  // Return NULL string - not found
  return NULL;
}

/*
 *+ 
 * FUNCTION NAME: Fits::write_data
 * 
 * INVOCATION: fits->write_data(char *src_data, int in_bitpix, int ext_num, int xo, int yo, int wid, int hei)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > src_data = source fits data buffer
 * > in_bitpix = bitpix of input data
 * > ext_num = output extension
 * > xo = starting x coord to write
 * > yo = starting y coord to write
 * > wid = row width
 * > hei = column height
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Write fits rectangular array of fits data to file, xo,yo,wid and hei
 * are in pixel coords.
 * 
 * DESCRIPTION: Data can be written to file either by direct memory mapping if
 * available or by using file seeks and writes. If scaling and translation are
 * required then this is performed first.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- */
void Fits::write_data(char *src_data, int in_bitpix, int ext_num, int xo, int yo, int wid, int hei)
{
  bool scaled;
  bool whole;
  int wpos, n, i, npix, offset, r;
  char *file_data, *src_p;
  Byte_Order_Type local_byte_order=byte_order();  

  // Usual sanity checks
  if (!file_open_write || !open_data) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  // Check if wid and hei have been set, if not set them to extension size
  if (wid<0) 
    wid = xt[ext_num].nx;
  if (hei<0)
    hei = xt[ext_num].ny;

  npix = wid*hei;

  if (use_vm) {
    // Ensure we are writing to a valid address
    if (xt[ext_num].data == NULL) {
      snprintf(errmsg, MSGLEN, "Image section %d not ready, file %s!",ext_num, fitsname);
      throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
    }
  } else {
    // Work out where to write data in file
    wpos = xt[ext_num].hdr_size;
    for (i=0; i<ext_num; i++) {
      wpos += xt[ext_num].hdr_size;
      if ((i+1) < ext_num)
	wpos += xt[ext_num].data_size;
    }
  }

  // Is the data to be scaled 
  scaled = ((xt[ext_num].bscale != 1.0) || (xt[ext_num].bzero != 0.0));

  // Are we writing the whole image extension?
  whole = ((xo == 0) && (yo == 0) && (wid == xt[ext_num].nx) && (hei == xt[ext_num].ny));

  if (!scaled && (in_bitpix == xt[ext_num].bitpix) && (local_byte_order==WORD_BIG_ENDIAN)) {
    if (whole) {
      // Use efficient buld data operations to write whole image
      if (use_vm) {
	memcpy(xt[ext_num].data, src_data, BYTE_PIX(xt[ext_num].bitpix)*npix);
      } else {
	if ((n = lseek(fd, wpos, SEEK_SET)) == -1) {
	  snprintf(errmsg, MSGLEN,"Error positioning FITS file %s to data position %d!", fitsname, wpos);
	  throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	}
	if ((n = write(fd, src_data, BYTE_PIX(xt[ext_num].bitpix)*npix)) == -1) {
	  snprintf(errmsg, MSGLEN,"Error writing FITS file %s, extension %d!",fitsname, i);
	  throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	}
      }
    } else {
      // Copy the data without directly row by row. This allows for columns of data in a file
      // to be written independently.
      // NB: input data could be of any data type

      n = BYTE_PIX(xt[ext_num].bitpix)*wid;
      if (!use_vm)
	// Allocate some temporary storage space for a row of output data
	file_data = new char[n];  
      
      src_p = src_data;

      // Now write data a row at a time because we could only be writing a column of data
      // This is flexible but may be inefficient
      for (r=yo; r<yo+hei; r++) {
	
	// Set the memory mapping pointer to right position in file for this row
	offset = BYTE_PIX(xt[ext_num].bitpix)*(xt[ext_num].nx * r + xo);
	if (use_vm)
	  file_data = xt[ext_num].data + offset;	
	
	switch (in_bitpix) {
	case BYTE_BITPIX:
	  copy_out(src_p,file_data,wid,xt[ext_num].bitpix);
	  break;
	case SHORT_BITPIX:
	  copy_out((short*)src_p,file_data,wid,xt[ext_num].bitpix);
	  break;
	case INT_BITPIX:
	  copy_out((int*)src_p,file_data,wid,xt[ext_num].bitpix);
	  break;
	case FLOAT_BITPIX:
	  copy_out((float*)src_p,file_data,wid,xt[ext_num].bitpix);
	  break;
	case DOUBLE_BITPIX:
	  copy_out((double*)src_p,file_data,wid,xt[ext_num].bitpix);
	  break;
	default:
	  break;
	}
	src_p += n;
	
	// For non vm mapped files - use seek to get to right file position
	if (!use_vm) {
	  if ((n = lseek(fd, wpos + offset, SEEK_SET)) == -1) {
	    snprintf(errmsg, MSGLEN,"Error positioning FITS file %s to data position %d!", fitsname, wpos);
	    throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	  }
	  if ((n = write(fd, file_data, n)) == -1) {
	    snprintf(errmsg, MSGLEN,"Error writing FITS file %s, extension %d!",fitsname, i);
	    throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	  }
	}
      }
      
      if (!use_vm)
	delete [] file_data;
    }
  } else {
    // Need to scale and translate data before writing
    // NB: input data could be of any data type

    n = BYTE_PIX(xt[ext_num].bitpix)*wid;
    if (!use_vm)
      // Allocate some temporary storage space for a row of output data
      file_data = new char[n];

    src_p = src_data;

    // Now write data a row at a time because we could only be writing a column of data
    // This is flexible but may be inefficient
    for (r=yo; r<yo+hei; r++) {

      // Set the memory mapping pointer to right position in file for this row
      offset = BYTE_PIX(xt[ext_num].bitpix)*(xt[ext_num].nx * r + xo);
      if (use_vm)
	file_data = xt[ext_num].data + offset;	
    
      switch (in_bitpix) {
      case BYTE_BITPIX:
	scale_out(src_p,file_data,wid,xt[ext_num].bitpix,xt[ext_num].bscale,xt[ext_num].bzero);
	break;
      case SHORT_BITPIX:
	scale_out((short*)src_p,file_data,wid,xt[ext_num].bitpix,xt[ext_num].bscale,xt[ext_num].bzero);
	break;
      case INT_BITPIX:
	scale_out((int*)src_p,file_data,wid,xt[ext_num].bitpix,xt[ext_num].bscale,xt[ext_num].bzero);
	break;
      case FLOAT_BITPIX:
	scale_out((float*)src_p,file_data,wid,xt[ext_num].bitpix,xt[ext_num].bscale,xt[ext_num].bzero);
	break;
      case DOUBLE_BITPIX:
	scale_out((double*)src_p,file_data,wid,xt[ext_num].bitpix,xt[ext_num].bscale,xt[ext_num].bzero);
	break;
      default:
	break;
      }
      src_p += n;

      // For non vm mapped files - use seek to get to right file position
      if (!use_vm) {
	if ((n = lseek(fd, wpos + offset, SEEK_SET)) == -1) {
	  snprintf(errmsg, MSGLEN,"Error positioning FITS file %s to data position %d!", fitsname, wpos);
	  throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	}
	if ((n = write(fd, file_data, n)) == -1) {
	  snprintf(errmsg, MSGLEN,"Error writing FITS file %s, extension %d!",fitsname, i);
	  throw Error(errmsg,E_ERROR,errno,__FILE__,__LINE__);
	}
      }
    }

    if (!use_vm)
      delete [] file_data;
  }
}
/*
 *+ 
 * FUNCTION NAME: Fits::num_keywords
 * 
 * INVOCATION: fits->num_keywords(int ext_num, int extra_only)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > ext_num - MEF extension number
 * > extra_only - set if required to count only non-standard keywords
 * 
 * FUNCTION VALUE: int - number of keywords in extension header
 * 
 * PURPOSE: Get number of Fits header keywords
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
int Fits::num_keywords(int ext_num, int extra_only)
{
  // Usual sanity checks
  if (!file_open_read && !file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  // Ensure image extension exists
  if (ext_num > nextend) {
    snprintf(errmsg, MSGLEN, "Image section %d illegal, file %s!",ext_num, fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  if (extra_only)
    return xt[ext_num].keysextra;
  else
    return xt[ext_num].keysexist;
}

/*
 *+ 
 * FUNCTION NAME: Fits::copy_headers
 * 
 * INVOCATION: fits->copy_headers(Fits& old_fits, int ext_num, int old_ext_num, int extra_only)
 * 
 * PARAMETERS: 
 * > old_fits - Name of old FITS file to copy from
 * > old_ext_num - old fits file extension
 * > ext_num - new fits file extension
 * > extra_only - only copy non-standard headers
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Copy headers from old FITS extension to new FITS file
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
void Fits::copy_headers(Fits& old_fits, int ext_num, int old_ext_num, int extra_only)
{
  int i,j;
  char *hdr;
  Fitsline tformkw,ttypekw;
  int hu = (ext_num>0)? xt[ext_num].ihu:phu;
  bool flag;

  // Usual sanity checks
  if (!file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open for write!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  for (i=0; i<old_fits.xt[old_ext_num].keysexist; i++) {
    hdr = old_fits.get_keyword(i+1,old_ext_num);
    if (extra_only) {
      flag = false;
      // ignore keyword if a standard
      if (keysift(hdr,"SIMPLE") != NULL) continue;
      else if (keysift(hdr,"NAXIS1") != NULL) continue;
      else if (keysift(hdr,"NAXIS2") != NULL)  continue;
      else if (keysift(hdr,"XTENSION") != NULL) continue;
      else if (keysift(hdr,"BITPIX") != NULL) continue;
      else if (keysift(hdr,"NAXIS") != NULL) continue;
      else if (keysift(hdr,"BSCALE") != NULL) continue;
      else if (keysift(hdr,"BZERO") != NULL) continue;
      else if (keysift(hdr,"PCOUNT") != NULL) continue;
      else if (keysift(hdr,"GCOUNT") != NULL) continue;
      else if (keysift(hdr,"INHERIT") != NULL) continue;
      else if (keysift(hdr,"EXTNAME") != NULL) continue;
      else if (keysift(hdr,"EXTVER") != NULL) continue;
      else if (keysift(hdr,"IMAGEID") != NULL) continue;
      else if (keysift(hdr,"NEXTEND") != NULL) continue;
      else if (keysift(hdr,"EXTEND") != NULL) continue;
      else if (keysift(hdr,"TFIELDS") != NULL) continue;

      // This is ugly, having to loop over all possible indices to
      // search for the TTYPE/TFORM keywords, but the header might
      // have these before the TFIELDS kw, so we dont know how many of
      // them there are yet.  We could probably clean this up by doing
      // this as part of the TFIELDS processing, but it makes the code
      // more complicated and I dont have time right now.
      for (j=1;j<=FITS_MAX_TFIELDS; j++) {
	snprintf(ttypekw, FITSKEYWORDLEN,"TTYPE%d",j);
	snprintf(tformkw, FITSKEYWORDLEN,"TFORM%d",j);
	if (keysift(hdr,ttypekw) != NULL) {
	  flag = true;
	  break;
	} else if (keysift(hdr,tformkw) != NULL) {
	  flag = true;
	  break;
	}       
      }
      if (flag) {
	continue;
      }
    }

    if (check_end(hdr))
      continue; // do not copy the END keyword.

    hdr_line(xt[ext_num].header,&xt[ext_num].header,hdr); 
    xt[ext_num].headers_written++;
    xt[ext_num].keysexist++;

    // Have we written too many for size reserved?
    if (xt[ext_num].headers_written >= hu*FITS_KEYS_PER_BLOCK) {
      snprintf(errmsg, MSGLEN,"Poorly constructed FITS file %s! %d keywords, when max %d expected!", 
	      fitsname, xt[ext_num].headers_written+1, hu*FITS_KEYS_PER_BLOCK);
      throw Error(errmsg, E_WARNING, IPC_ERR, __FILE__, __LINE__);
    }   
  }
}

/*
 *+ 
 * FUNCTION NAME:  Fits::get_keyword
 * 
 * INVOCATION: fits->get_keyword(int key_no, int ext_num)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: char* - header line
 * 
 * PURPOSE: Get a keyword - keys are numbered 1-keysexist
 * 
 * DESCRIPTION: Uses internal character buffer to store value of key
 * header sought.  returns a pointer to this buffer
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
char* Fits::get_keyword(int key_no, int ext_num)
{
  // Usual sanity checks
  if (!file_open_read && !file_open_write) {
    snprintf(errmsg, MSGLEN, "Fits file %s, not open!", fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  // Ensure image extension in range
  if (ext_num > nextend) {
    snprintf(errmsg, MSGLEN, "Image section %d illegal, file %s!",ext_num, fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  if ((key_no <= 0) || (key_no > xt[ext_num].keysexist)) {
    snprintf(errmsg, MSGLEN, "No such key %d in extension %d, file %s!",key_no, ext_num, fitsname);
    throw Error(errmsg, E_ERROR, IPC_ERR, __FILE__, __LINE__);    
  }

  // setup pointers to headers
  if (!headers_read) 
    read(fitsname,&xt[ext_num].bitpix,FALSE,FALSE,FALSE);

  strlcpy(card, (char*)(xt[ext_num].header_start + (key_no-1)*FITSKEYWORDLEN), FITSKEYWORDLEN);
  return card;
}

/*
 *+ 
 * FUNCTION NAME: Fits::~Fits
 * 
 * INVOCATION: delete fits
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Destructor - make sure file closed
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
Fits::~Fits()
{
  if (file_open_read || file_open_write || file_open_update) {
    close();    
  }
  if (xt)
    delete [] xt;
  if (btable)
    delete [] btable;
}

/* local function definitions */

/*
 *+ 
 * FUNCTION NAME: hdr_line
 * 
 * INVOCATION: hdr_line(char *p, char **ap, const char *line)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * ! p - pointer into buffer
 * ! ap - address of p
 * > line - header descriptor
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: constructs a FITS header record
 * 
 * DESCRIPTION: Local function hdr_line - writes a FITS line to memory
 * address.  Copies line to p and pads to 80 chars with spaces
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *-
 */
static void hdr_line(char *p, char **ap, const char *line)
{
  int i, len;

  len = ((int)strlen(line) <= FITSKEYWORDLEN) ? strlen(line) : FITSKEYWORDLEN;
  memcpy(p, line, len);
  p = p + len;
  for (i=len; i<FITSKEYWORDLEN; i++) {
    *p++ = ' ';
  }
  if (ap) *ap = p;
}
/*
 *+ 
 * FUNCTION NAME: keysift
 * 
 * INVOCATION: keysift(const char *line, const char *par)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: string - parameter value
 * 
 * PURPOSE: searchs for parameter in string
 * 
 * DESCRIPTION: 
 *  keysifts line for parameter setting of type PAR=val, returns val
 *  must apply strtok to temporary string, since strtok does not
 *  preserve the original string.
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *  - fails with kws with single quotes in the comment part
 *
 *- 
 */
char* Fits::keysift(const char *line, const char *par)
{
// return NULL if missing, "" if kw is there but has no value.
  char *s = NULL;
  Fitsline tmp;
  char *firstq,*lastq;
  char* tokPtr(0);
  int kwlen=0;

  strlcpy(tmp,line,FITSKEYWORDLEN);
  if ((s = strtok_r(tmp,"= ",&tokPtr)) == NULL) {
    key[0] = '\0';
    return NULL;
  } else 

  if (strcmp(s,par) == 0) {
    strlcpy(tmp,line,FITSKEYWORDLEN);
    // only check kwds with "="
    if ((s = strchr((char *)tmp,'=')) != NULL) { 
      firstq=strchr(s,'\''); // pos of first quote in kw
      lastq=strrchr(s,'\''); // pos of  last quote in kw
      kwlen = lastq-firstq-1; 
      
      // return value from legit string kwds.
      if ((firstq != NULL) && (lastq != NULL) && (kwlen>0) ) {
	s=firstq+1;
	s[kwlen]=0;
	strlcpy(key,s,FITSKEYWORDLEN);
      } else if(kwlen==0) { // return zero-length string kwds
	key [0] = '\0';
      } else {  // no quotes, return whatever is after the "="
	strlcpy(key, strtok_r(s,"= ",&tokPtr),FITSKEYWORDLEN);
      }
      return key;
    } else { // no = in the KW
      key [0] = '\0';
      return NULL;
    }
  } else { // didnt match the KW we were looking for
    key [0] = '\0';
    return NULL;
  }
}

Bintable::Bintable()
{
  int j;

  tfields=-1;
  for (j=0;j<FITS_MAX_TFIELDS;j++) {
    ttype[j]="";
    tform[j]="";
  }
}

Extension::Extension()
{
  // poisoned value. If this actually gets into a FITS file, attempts to open it will throw
  type = EXT_NOT_INITIALISED;
  nx = ny = 0;
  bitpix = 16;
  pcount = 0;
  gcount = 1;
  bzero = 0.0;
  bscale = 1.0;
  ihu = 0;
  data = NULL; 
  header = NULL;
  header_start = NULL; 
  data_size = 0;
  hdr_size = 0;
  headers_written = 0;
  end_written = FALSE;
  keysexist = 0;    
  keysextra = 0;
  detx = 0;
  dety = 0;
#ifndef vxWorks
  vmdata = NULL;
  vmheader = NULL;
#endif
}
