/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_cadSDSU.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI NIFS PROJECT
 * 
 * FILENAME cad_sdsu.c
 * 
 * GENERAL DESCRIPTION
 *
 * Handle CADS from commands that are ignored by the NIFS DC.  These are DATUM,
 * GUIDE, ENDGUIDE, VERIFY, ENDVERIFY and ENDOBSERVE.  Their equivalent CAR
 * records will be set to busy and then back to idle.
 *
 * $Log: dc_cadSDSU.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.12  2005/07/29 23:46:37  pjy
 * Use set_and_verify to check to see if observc is busy
 *
 * Revision 1.11  2005/07/28 09:38:28  pjy
 * Added snam for sdsutest
 *
 * Revision 1.10  2005/07/14 06:46:46  pjy
 * change cad testpattern to testpat
 *
 * Revision 1.9  2005/07/12 06:46:59  pjy
 * Added test pattern snam routine
 *
 * Revision 1.8  2005/02/25 04:48:38  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.7  2004/12/23 22:40:39  pjy
 * Fixed comments
 *
 * Revision 1.6  2004/12/22 06:09:54  pjy
 * DC cad src files ready for first Gemini release
 *
 * Revision 1.5  2004/08/20 02:27:04  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.4  2004/04/27 07:25:56  pjy
 * T2.2
 *
 * Revision 1.1  2004/03/23 01:59:51  pjy
 * Gem 8.6 port
 *
 * Revision 1.2  2003/09/19 06:24:22  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.1  2003/03/11 01:29:30  pjy
 * Import modules
 *
 * 
 */
/* VxWorks include files */
#include <cstdio>
#include <cstring>

#ifdef __cplusplus
extern "C" {
#endif
#include "genSubRecord.h"
#include "cadRecord.h"
#include "sirRecord.h"
#include "cad.h"
#include "dbAccess.h"
#include "dbCommon.h"
#ifdef __cplusplus
}
#endif
#include "sdsu_controller.h"
#include "logging.h"

/* Function declarations */
#ifdef __cplusplus
extern "C" long dcCADDSPCommand(struct cadRecord *pCad); 
extern "C" long dcCADinitSDSU(struct cadRecord *pCad); 
extern "C" long dcCADtestPat(struct cadRecord *pCad);
extern "C" long dcCADsetupSDSU(struct cadRecord *pCad); 
extern "C" long dcCADshutdownSDSU(struct cadRecord *pCad); 
extern "C" long dcCADtestSDSU(struct cadRecord *pCad);
extern "C" long dcCADdebugSDSU(struct cadRecord *pCad);
extern "C" void set_and_verify_output_cad(struct cadRecord *pCad);
extern "C" long dcDSPCmdAscii(struct genSubRecord *pGenSub);
extern "C" long dcDSPAsciiCmd(struct sirRecord *pSir);
extern "C" long dcDSPHeaderHex(struct sirRecord *pSir);
extern "C" long dcDSPReplyHex(struct sirRecord *pSir);
#endif


/*
 *+
 * FUNCTION NAME:
 * dcCADDSPCommand
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADDSPCommand( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for SDSU DSP CAD record
 *
 * DESCRIPTION:
 * This routine is called whenever an SDSU DSP CAD record is processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADDSPCommand(struct cadRecord *pCad) 
{
  long status;                     /* return status */
  
  status = CAD_ACCEPT;
  
  logger::message(logger::Level::Full,"dcCADDSPCommand: %d directive.", pCad->dir);
  
  /* Create name of record to process car from name of this record*/
  
  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:                    
    break;

  case menuDirectivePRESET:		
    /* Reject the command if the arguments don't validate */
    try {
      // Verify input parameters and set outputs
      set_and_verify_output_cad(pCad);

      logger::message(logger::Level::Full,"dcCADDSPCommand: Cad verified ok!");
    } 
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;

  case menuDirectiveSTART:		

    /* Handle start functionality for SDSU Clk record */
    
    break;
    
  case menuDirectiveSTOP:			/* Really can't	*/
    strncpy(pCad->mess, "dcCADDSPCommand: Cannot stop!", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
    
  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADDSPCommand: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  } 
  return status;
}


/*
 *+
 * FUNCTION NAME:
 * dcCADinitSDSU
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADinitSDSU( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for SDSU Clk CAD record
 *
 * DESCRIPTION:
 * This routine is called whenever an SDSU Clk CAD record is processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADinitSDSU(struct cadRecord *pCad) 
{
  long status;                     /* return status */
  
  status = CAD_ACCEPT;
  
  logger::message(logger::Level::Full,"dcCADinitSDSU: %d directive.", pCad->dir);
  
  /* Create name of record to process car from name of this record*/
  
  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:                    
    break;

  case menuDirectivePRESET:		
    /* Reject the command if the arguments don't validate */
    try {
      // Verify input parameters and set outputs
      set_and_verify_output_cad(pCad);
      
      logger::message(logger::Level::Full,"dcCADInitSDSU: Cad verified ok!");
    } 
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;

  case menuDirectiveSTART:		

    break;
    
  case menuDirectiveSTOP:			/* Really can't	*/
    strncpy(pCad->mess, "dcCADinitSDSU: Cannot stop!", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
    
  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADinitSDSU: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  } 
  return status;
}


/*
 *+
 * FUNCTION NAME:
 * dcCADsetupSDSU
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADsetupSDSU( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for SDSU Clk CAD record
 *
 * DESCRIPTION:
 * This routine is called whenever an SDSU setup CAD record is processed.
 * This could be the resetSDSU, shutdownSDSU, testSDSU CADs - none of
 * which take any input parameters.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADsetupSDSU(struct cadRecord *pCad) 
{
  long status;                     /* return status */
  
  status = CAD_ACCEPT;
  
  logger::message(logger::Level::Full,"dcCADsetupSDSU: %d directive.", pCad->dir);
  
  /* Create name of record to process car from name of this record*/
  
  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:                    
    break;

  case menuDirectivePRESET:		
    /* Reject the command if the arguments don't validate */
    try {
      // Verify input parameters and set outputs
      set_and_verify_output_cad(pCad);
      
      logger::message(logger::Level::Full,"dcCADSetupSDSU: Cad verified ok!");
    } 
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;
    
  case menuDirectiveSTART:		

    /* Handle start functionality for SDSU Clk record */
    
    break;
    
  case menuDirectiveSTOP:			/* Really can't	*/
    strncpy(pCad->mess, "dcCADsetupSDSU: Cannot stop!", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
    
  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADsetupSDSU: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  } 
  return status;
}

/*
 *+
 * FUNCTION NAME:
 * dcCADdebugSDSU
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADdebugSDSU( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for SDSU DSP CAD record
 *
 * DESCRIPTION:
 * This routine is called whenever an SDSU DSP CAD record is processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADdebugSDSU(struct cadRecord *pCad) 
{
  long status;                     /* return status */
  
  status = CAD_ACCEPT;
  
  logger::message(logger::Level::Full,"dcCADdebugSDSU: %d directive.", pCad->dir);
  
  /* Create name of record to process car from name of this record*/
  
  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:                    
    break;

  case menuDirectivePRESET:		
    /* Reject the command if the arguments don't validate */
    try {
      // Verify input parameters and set outputs
      set_and_verify_output_cad(pCad);

      logger::message(logger::Level::Full,"dcCADdebugSDSU: Cad verified ok!");
    } 
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;

  case menuDirectiveSTART:		

    /* Handle start functionality for SDSU Clk record */
    
    break;
    
  case menuDirectiveSTOP:			/* Really can't	*/
    strncpy(pCad->mess, "dcCADdebugSDSU: Cannot stop!", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
    
  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADdebugSDSU: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  } 
  return status;
}
/*
 *+
 * FUNCTION NAME:
 * dcCADtestSDSU
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADtestSDSU( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for SDSU DSP CAD record
 *
 * DESCRIPTION:
 * This routine is called whenever an SDSU DSP CAD record is processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADtestSDSU(struct cadRecord *pCad) 
{
  long status;                     /* return status */
  
  status = CAD_ACCEPT;
  
  logger::message(logger::Level::Full,"dcCADtestSDSU: %d directive.", pCad->dir);
  
  /* Create name of record to process car from name of this record*/
  
  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:                    
    break;

  case menuDirectivePRESET:		
    /* Reject the command if the arguments don't validate */
    try {
      // Verify input parameters and set outputs
      set_and_verify_output_cad(pCad);

      logger::message(logger::Level::Full,"dcCADtestSDSU: Cad verified ok!");
    } 
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;

  case menuDirectiveSTART:		

    /* Handle start functionality for SDSU Clk record */
    
    break;
    
  case menuDirectiveSTOP:			/* Really can't	*/
    strncpy(pCad->mess, "dcCADtestSDSU: Cannot stop!", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
    
  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADtestSDSU: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  } 
  return status;
}

/*
 *+
 * FUNCTION NAME:
 * dcCADtestPat
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADtestPat( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for SDSU Clk CAD record
 *
 * DESCRIPTION:
 * This routine is called whenever an SDSU Clk CAD record is processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADtestPat(struct cadRecord *pCad) 
{
  long status;                     /* return status */
  
  status = CAD_ACCEPT;
  
  logger::message(logger::Level::Full,"dcCADtestPat: %d directive.", pCad->dir);
  
  /* Create name of record to process car from name of this record*/
  
  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:                    
    break;

  case menuDirectivePRESET:		
    /* Reject the command if the arguments don't validate */
    try {
      // Verify input parameters and set outputs
      set_and_verify_output_cad(pCad);
      
      logger::message(logger::Level::Full,"dcCADtestPat: Cad verified ok!");
    } 
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;

  case menuDirectiveSTART:		

    break;
    
  case menuDirectiveSTOP:			/* Really can't	*/
    strncpy(pCad->mess, "dcCADtestPat: Cannot stop!", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
    
  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADtestPat: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  } 
  return status;
}

/*
 *+
 * FUNCTION NAME:
 * dcDSPCmdAscii
 *
 * INVOCATION:
 * struct genSubRecord *pGenSub;
 * long status;
 *
 * status = dcDSPCmdAscii( *pGenSub )
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pGenSub   (struct genSubRecord *)   pointer to genSub data structure
 *
 * FUNCTION VALUE:
 * long  Status value, 0 for success
 *
 * PURPOSE:
 * User defined function for converting an inut DSP command to its ASCII representation.
 *
 * DESCRIPTION:
 * This routine is called whenever the appropriate genSub records are processed.
 * It takes a 3 character SDSU DSP command (eg TDL) and converts it to a long
 * word with ASCII values for the 3 chars in the 3 least significant bytes (eg
 * TDL = 0x0054444C).
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * The genSub record needing this routine must have its SNAM property set to
 * dcDSPCmdAscii. 
 *
 * DEFICIENCIES:
 * None known
 *
 * HISTORY (optional):
 *
 *- 
 */


long dcDSPCmdAscii(struct genSubRecord *pGenSub)
{
  long icmd;
  long status = OK;
  char cmd[MAX_STRING_SIZE];

  try {

  if (pGenSub->a == NULL) {
      throw Error("NULL pointer found on gensub input A", E_ERROR, -1, __FILE__, __LINE__);
  }
    
    // Make sure output values have been setup for Val,Err amd Msg
    if (pGenSub->vala == NULL) 
      throw Error("NULL pointer found on gensub output A", E_ERROR, -1, __FILE__, __LINE__);
    
    // Make sure data types have been specified correctly
    if (pGenSub->ftva != DBF_LONG)
      throw Error("Wrong data type specified on gensub output A", E_ERROR, -1, __FILE__, __LINE__);
    

    // Everything OK - process input cmd
    strncpy(cmd, (char*) pGenSub->a, MAX_STRING_SIZE-1);
    icmd =  (cmd[0]<<16 | cmd[1]<<8 | cmd[2]);
    
    // Now put the converted ASCII code as long word
    *(long*)pGenSub->vala = icmd;

    logger::message(logger::Level::Full,"SDSU DSP Command %s = 0x%x", cmd, *(long*)pGenSub->vala);
  }
  catch (Error& err) {
    status = err.errnum;
    logger::message(logger::Level::NoLog, "dcDSPCmdAscii: Error converting DSP cmd using gensub '%s'", pGenSub->name);
    logger::message(logger::Level::NoLog, "dcDSPCmdAscii: %s", err.record_error(__FILE__, __LINE__));
  }  
  
  return status;
}

/*
 *+
 * FUNCTION NAME:
 * dcDSPAsciiCmd
 *
 * INVOCATION:
 * struct sirRecord *pSir;
 * long status;
 *
 * status = dcDSPAsciiCmd(*pSir)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pSir   (struct sirRecord *)   pointer to SIR data structure
 *
 * FUNCTION VALUE:
 * long  Status value, 0 for success
 *
 * PURPOSE:
 * User defined function for converting an inut DSP command to its String representation.
 *
 * DESCRIPTION:

 * This routine is called whenever the appropriate SIR records are processed.
 * It takes an integer (ascii) command (eg TDL = 0x0054444C) and converts it to
 * a 3 character SDSU DSP command (eg TDL).
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * The Sir record needing this routine must have its SNAM property set to
 * dcDSPAsciiCmd. 
 *
 * DEFICIENCIES:
 * None known
 *
 * HISTORY (optional):
 *
 *- 
 */


long dcDSPAsciiCmd(struct sirRecord *pSir)
{
  long icmd;
  long status = OK;
  char cmd[MAX_STRING_SIZE];

  try {

    if (pSir->val == NULL)
      throw Error("NULL pointer found on SIR ", E_ERROR, -1, __FILE__, __LINE__);
    
    // Make sure data types have been specified correctly
    if (pSir->ftvl != DBF_STRING)
      throw Error("Wrong data type specified on SIR VAL", E_ERROR, -1, __FILE__, __LINE__);
    
    // Everything OK - process input 
    icmd = atol((char*)pSir->val);
    cmd[0] = (icmd >> 16) & 0xff;
    cmd[1] = (icmd >> 8) & 0xff;
    cmd[2] = icmd & 0xff;
    cmd[3] = 0;
    strncpy((char*) pSir->val, cmd, MAX_STRING_SIZE-1);
    
    logger::message(logger::Level::Full,"SDSU DSP Command %s = 0x%x", cmd, icmd);
  }
  catch (Error& err) {
    status = err.errnum;
    logger::message(logger::Level::NoLog, "dcDSPAsciiCmd: Error converting DSP cmd using SIR '%s'", pSir->name);
    logger::message(logger::Level::NoLog, "dcDSPAsciiCmd: %s", err.record_error(__FILE__, __LINE__));
  }  
  
  return status;
}

/*
 *+
 * FUNCTION NAME:
 * dcDSPHeaderHex
 *
 * INVOCATION:
 * struct sirRecord *pSir;
 * long status;
 *
 * status = dcDSPHeaderHex(*pSir)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pSir   (struct sirRecord *)   pointer to SIR data structure
 *
 * FUNCTION VALUE:
 * long  Status value, 0 for success
 *
 * PURPOSE: 
 * User defined function for converting a DSP header word to its HEX
 * representation.
 *
 * DESCRIPTION:
 *
 * This routine is called whenever the appropriate SIR records are processed.
 * It takes an integer SDN header word and converts it to a Hex string.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * The Sir record needing this routine must have its SNAM property set to
 * dcDSPHeaderHex. 
 *
 * DEFICIENCIES:
 * None known
 *
 * HISTORY (optional):
 *
 *- 
 */
long dcDSPHeaderHex(struct sirRecord *pSir)
{
  long iword;
  long status = OK;
  char hex[MAX_STRING_SIZE];

  try {

    if (pSir->val == NULL)
      throw Error("NULL pointer found on SIR", E_ERROR, -1, __FILE__, __LINE__);
    
    // Make sure data types have been specified correctly
    if (pSir->ftvl != DBF_STRING)
      throw Error("Wrong data type specified on SIR VAL", E_ERROR, -1, __FILE__, __LINE__);
    
    // Everything OK - process input 
    iword = atol((char*)pSir->val);
    snprintf(hex, MAX_STRING_SIZE-1, "0x%lx", iword);
    strcpy((char*) pSir->val, hex);
  }
  catch (Error& err) {
    status = err.errnum;
    logger::message(logger::Level::NoLog, "dcDSPHeaderHex: Error converting DSP word using sir '%s'", pSir->name);
    logger::message(logger::Level::NoLog, "dcDSPHeaderHex: %s", err.record_error(__FILE__, __LINE__));
  }  
  
  return status;
}
/*
 *+
 * FUNCTION NAME:
 * dcDSPReplyhex
 *
 * INVOCATION:
 * struct sirRecord *pSir;
 * long status;
 *
 * status = dcDSPReplyhex(*pSir)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pSir   (struct sirRecord *)   pointer to SIR data structure
 *
 * FUNCTION VALUE:
 * long  Status value, 0 for success
 *
 * PURPOSE: 
 * User defined function for converting a DSP reply word to its HEX
 * representation.
 *
 * DESCRIPTION:
 *
 * This routine is called whenever the appropriate SIR records are processed.
 * It takes an integer SDN reply word and converts it to a Hex string.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * The Sir record needing this routine must have its SNAM property set to
 * dcDSPReplyhex. 
 *
 * DEFICIENCIES:
 * None known
 *
 * HISTORY (optional):
 *
 *- 
 */
long dcDSPReplyHex(struct sirRecord *pSir)
{
  long iword;
  long status = OK;
  char cmd[MAX_STRING_SIZE];
  char hex[MAX_STRING_SIZE];

  try {

    if (pSir->val == NULL)
      throw Error("NULL pointer found on SIR", E_ERROR, -1, __FILE__, __LINE__);
    
    // Make sure data types have been specified correctly
    if (pSir->ftvl != DBF_STRING)
      throw Error("Wrong data type specified on SIR VAL", E_ERROR, -1, __FILE__, __LINE__);
    
    // Everything OK - process input 
    iword = atol((char*)pSir->val);
    if (iword == SDSU_ACTUAL_VALUE) {
      strcpy(hex,"Actual Value");
    } else {
      cmd[0] = (iword >> 16) & 0xff;
      cmd[1] = (iword >> 8) & 0xff;
      cmd[2] = iword & 0xff;
      cmd[3] = 0;
      snprintf(hex, MAX_STRING_SIZE-1, "0x%lx (%s)", iword, cmd);
    }
    strcpy((char*) pSir->val, hex);
  }
  catch (Error& err) {
    status = err.errnum;
    logger::message(logger::Level::NoLog, "dcDSPReplyHex: Error converting DSP word using sir '%s'", pSir->name);
    logger::message(logger::Level::NoLog, "dcDSPReplyHex: %s", err.record_error(__FILE__, __LINE__));
  }  
  
  return status;
}

