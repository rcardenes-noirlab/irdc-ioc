/*
 * Copyright (c) 1994-1998 by MSSSO Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME sdsu2_controller.cc
 *
 * GENERAL DESCRIPTION
 * This module implements the SDSU generation 2 hardware class.
 * It is derived from the SDSU Generic class.
 */

/*---------------------------------------------------------------------------
 Include files
 --------------------------------------------------------------------------*/
#include "sdsu2_controller.h"
#include "detector_regions.h"

/* defines */

/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/* class function definitions */
/*============================================================================
 *  Sdsu2_Controller
 *===========================================================================*/
/*
 *+
 * FUNCTION NAME: Sdsu2_Controller constructors
 *
 * INVOCATION:
 *  sdsu_controller = new Sdsu2_Controller(cam,camid,c); or
 *  sdsu_controller = new Sdsu2_Controller(cam,camid,c,param_host,st)
 *  sdsu_controller = new Sdsu2_Controller(st,im,is,ss,ps,msg,cf,rep,deb);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > cam - Camera_desc - a description of the camera
 *  > camid - camera id
 *  > c - camera controller number
 *  > param_host - name of host holding parameter database
 *  > st - pointer to instrument status structure shared mem
 *  > im - pointer to image shared memory
 *  > is - pointer to image semaphore
 *  > ss - pointer to status semaphore
 *  > ps - pointer to process state semaphore
 *  > msg - pointer to message shared memory
 *  > cf - pointer to camera configuration shared memory
 *  > rep - pointer to debugging report
 *  > deb - debug level mask
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Simple constructor used for handling configuration without action
 *  Normal constructor used on active camera on instrument computer
 *
 * DESCRIPTION:
 * The Sdsu2_Controller class implements a Cicada camera interface to the Sdsu II
 * controller. THis inherits the basic functions of the Sdsu I controller and
 * adds some extensions.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu2_Controller::Sdsu2_Controller(Camera_Desc& cam, int camid, int c, Platform_Specific* ps)
  :Sdsu_Controller(cam,camid,c,ps)
{
}
/*
 *+
 * FUNCTION NAME: Sdsu_Controller::Sdsu_Controller()
 *
 * INVOCATION: controller = new Sdsu_Controller(Camera_Desc cam, int camid, int c, const char* param_host,
 *                          Instrument_Status *st, paramMapType *pm)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cam - description of camera that is parent of this controller. Also holds
 *         controller description
 * > camid - identification umber of parent camera
 * > c - identification number of this controller within the camera.
 * > param_host - name of host where parameter database resides.
 * > st - pointer to instrument status data structure for all instrument status
 * > pm - pointer to parameter map type
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 * Constructor used for initialising configuration variables and setting up the
 * parameter database
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generation II SDSU controller
 * object.
 *
 * This version of the controller is used just for holding a controllers
 * configuration information and also its status parameter database. It is not
 * used to interface with real hardware. It is, therefore, only useful in
 * modules that need to know about a configuration and be able to read/write
 * status parameters but do not actually interface with hardware.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu2_Controller::Sdsu2_Controller(Camera_Desc& cam, int camid, int c, const char* param_host, Instrument_Status *st,
				   Platform_Specific* ps)
  :Sdsu_Controller(cam, camid, c, param_host, st, ps)
{
  controller_statusp->put(CONTROLLER_TYPE, (int) SDSU2);
}
/*
 *+
 * FUNCTION NAME: Sdsu_Controller::Sdsu_Controller()
 *
 * INVOCATION: controller = new Sdsu_Controller(Instrument_Status *st, Parameter* ip, Parameter* cp,
                                                Parameter* op, int deb)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > st - pointer to instrument status data structure for all instrument status
 * > ip - pointer to the parameter database for the instrument
 * > cp - pointer to the parameter database for the camera
 * > op - pointer to the operation parameter database
 * > deb - debug flag
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Constructor used for interfacing to real controller hardware
 *
 * DESCRIPTION:
 * This constructor is used to create an instance of a generation II SDSU controller
 * object.
 *
 * This version of the controller is used for interfacing with real hardware. It
 * is, therefore, required for components that are attached to the hardware. Its
 * derived classes know how to communicate with specific controllers, while this
 * parent class can be used in a generic way to deal with all controller types.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu2_Controller::Sdsu2_Controller(Instrument_Status *st, Parameter* ip, Parameter* cp, Parameter* op,
				 int deb, Platform_Specific* ps)
  :Sdsu_Controller(st,ip,cp,op,deb,ps)
{
  controller_statusp->put(CONTROLLER_TYPE, (int) SDSU2);

  // Now open the interface to the controller
  open_and_check_interface();
}

/*
 *+
 * FUNCTION NAME: do_gain
 *
 * INVOCATION:
 *  do_gain();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Sets controller gain according to configured setting
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::do_gain()
{
  int ret_value;
  message_write("Setting gain...");
  switch (sdsu_config.config_data.gain_setting) {
  case GN1:
  case GN2:
  case GN4:
  case GN9:
    ret_value = sdsu_command(TIM_ID, SDSU_SGN, sdsu_config.config_data.gain_setting,
			    sdsu_config.config_data.tc_setting);
    if(ret_value != SDSU_OK)
      message_write("Gain can not be set to %d nor speed to %d!", sdsu_config.config_data.gain_setting,
		    sdsu_config.config_data.tc_setting);
    else
      message_write("Gain set to %d, speed set to %d!", sdsu_config.config_data.gain_setting,
		    sdsu_config.config_data.tc_setting);
    break;
  default:
    message_write("Illegal gain setting for this controller - not set to %d",
		  sdsu_config.config_data.gain_setting);
  }
}

/*
 *+
 * FUNCTION NAME: do_tc
 *
 * INVOCATION:
 *  do_tc();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Sets controllers time constant algorithm according to configured setting
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::do_tc()
{
  int ret_value;
  message_write("Setting time constant...");
  switch (sdsu_config.config_data.tc_setting) {
  case 0:
  case 1:
    ret_value = sdsu_command(TIM_ID, SDSU_SGN, sdsu_config.config_data.gain_setting,
			    sdsu_config.config_data.tc_setting);
    if(ret_value != SDSU_OK)
      message_write("Gain can not be set to %d  nor time constant to %d!", sdsu_config.config_data.gain_setting,
		    sdsu_config.config_data.tc_setting);
    else
      message_write("Gain set to %d, time constant set to %d!", sdsu_config.config_data.gain_setting,
		    sdsu_config.config_data.tc_setting);
    break;
  default:
    message_write("Illegal time constant setting for this controller - not set to %d",
		  sdsu_config.config_data.tc_setting);
  }
}
/*
 *+
 * FUNCTION NAME: do_test_pattern
 *
 * INVOCATION:
 *  do_test_pattern();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Sets controllers test pattern mode
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::do_test_pattern()
{
  int stored_value = 0;
  int ret_value = SDSU_OK;

  // Read the current SDSU_Y_OPTIONS_WORD_PAR
  stored_value = sdsu_command(TIM_ID, SDSU_RDM, 0, SDSU_MEM_Y, SDSU_Y_OPTIONS_WORD_PAR);

  if (verify_spec.do_test_pattern) {
    message_write("Enabling test pattern...");

    // Set enable bit
    stored_value |= SDSU_TEST_PATTERN_BIT;

    // Set/clear constant type bit
    if (verify_spec.constant_val) {
      stored_value |= SDSU_TEST_PATTERN_CONST_BIT;
    } else if (stored_value & SDSU_TEST_PATTERN_CONST_BIT) {
      stored_value = stored_value ^ SDSU_TEST_PATTERN_CONST_BIT;
    }

    // Set/clear test pattern source bit
    if (sdsu_config.config_data.has_coadder_board && sdsu_config.config_data.test_pattern_by_coadder) {
      stored_value |= SDSU_TEST_PATTERN_SOURCE_BIT;
    } else if (stored_value & SDSU_TEST_PATTERN_SOURCE_BIT) {
      stored_value = stored_value ^ SDSU_TEST_PATTERN_SOURCE_BIT;
    }

    // Now write the new options word
    ret_value = sdsu_command(TIM_ID, SDSU_WRM, stored_value, SDSU_MEM_Y, SDSU_Y_OPTIONS_WORD_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to enable test pattern options bits, err=%d", ret_value);

    // Now set the test pattern constant, seed, inc and overscan values
    ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.seed, SDSU_MEM_Y, SDSU_TEST_PATTERN_SEED_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to set test pattern seed, err=%d", ret_value);

    ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.increment, SDSU_MEM_Y, SDSU_TEST_PATTERN_INC_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to set test pattern increment, err=%d", ret_value);

    ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.amp_increment, SDSU_MEM_Y, SDSU_TEST_PATTERN_AMP_INC_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to set test pattern amp increment, err=%d", ret_value);

    ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.readout_increment, SDSU_MEM_Y, SDSU_TEST_PATTERN_READ_INC_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to set test pattern readout increment, err=%d", ret_value);

    ret_value = sdsu_command(TIM_ID, SDSU_WRM, verify_spec.overscan, SDSU_MEM_Y, SDSU_TEST_PATTERN_OS_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to set test pattern overscan, err=%d", ret_value);
  } else if (stored_value & SDSU_TEST_PATTERN_BIT) {
    message_write("Disabling test pattern...");
    ret_value = sdsu_command(TIM_ID, SDSU_WRM, stored_value ^ SDSU_TEST_PATTERN_BIT, SDSU_MEM_Y, SDSU_Y_OPTIONS_WORD_PAR);
    if (ret_value != SDSU_OK)
      message_write("Failed to disable test pattern bit, err=%d", ret_value);
  }
}

/*
 *+
 * FUNCTION NAME: do_ab
 *
 * INVOCATION:
 *  do_ab();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Sets controllers anti-blooming mode
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::do_ab()
{
  int stored_value = 0;
  int ret_value = SDSU_OK;

  // Read the current SDSU_Y_OPTIONS_WORD_PAR
  stored_value = sdsu_command(TIM_ID, SDSU_RDM, 0, SDSU_MEM_Y, SDSU_Y_OPTIONS_WORD_PAR);

  if (sdsu_config.config_data.do_ab) {
    message_write("Setting anti-blooming...");
    ret_value = sdsu_command(TIM_ID, SDSU_WRM, stored_value | SDSU_AB_BIT,
			    SDSU_MEM_Y, SDSU_Y_OPTIONS_WORD_PAR);
  } else if (stored_value & SDSU_AB_BIT) {
    ret_value = sdsu_command(TIM_ID, SDSU_WRM, stored_value ^ SDSU_AB_BIT,
			    SDSU_MEM_Y, SDSU_Y_OPTIONS_WORD_PAR);
  }
  if (ret_value != SDSU_OK) {
    message_write("Failed to set anti-blooming bit, err=%d", ret_value);
  }

  // Now set the anti-blooming setting
  ret_value = sdsu_command(TIM_ID, SDSU_WRM, sdsu_config.config_data.ab_setting,
			  SDSU_MEM_Y, SDSU_AB_SETTING_PAR);
  if (ret_value != SDSU_OK)
    message_write("Failed to set anti-blooming frequency, err=%d", ret_value);
}

/*
 *+
 * FUNCTION NAME: get_regions_memory_address()
 *
 * INVOCATION:
 *  addr = get_regions_memory_address()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *  Regions start address
 *
 * PURPOSE:
 *  returns addres of DSP regions setup memory
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
int Sdsu2_Controller::get_regions_memory_address()
{
  return SDSU2_RGNS_START_PAR;
}
/*
 *+
 * FUNCTION NAME: Sdsu_Controller::setup_readout
 *
 * INVOCATION: setup_readout(int ns, Detector_Readout& readout_rec, Detector_Regions* trgn,
 *                                int& total_bytes, double exptime)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ns - number of controllers to sync
 * ! readout_rec - readout request structure
 * > trgn - readout regions object
 * ! total_bytes - total bytes in readout - may be modified by controller specific req
 * > exptime - last exposure time
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: setup the readout
 *
 * DESCRIPTION: Load the DSP program with info about this readout
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::setup_readout(int ns, Detector_Readout* readout_rec,Detector_Regions* trgn, unsigned long& total_bytes,
				     double exptime)
{

  // Call the sdsu generic readout setup method
  Sdsu_Controller::setup_readout(ns, readout_rec, trgn, total_bytes, exptime);

  if (camera.type == CCD_CAMERA) {
    // Now un/set anti-blooming mode
    do_ab();
  }

  // Now un/set the test pattern for the readout
  do_test_pattern();
}
/*
 *+
 * FUNCTION NAME: do_reset
 *
 * INVOCATION:
 *  do_reset();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Resets timing board
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *  Configuration shared memory
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::do_reset()
{
  int board_header, nargs, ios;

  // Board header is src|dest|nargs is 3 low order bytes (src is host computer = 0)
  // we need 3 arguments for this command, ie header, cmd, data
  nargs = 3;
  board_header = (TIM_ID << 8) | 3;

  message_write("Resetting Timing Board...");

  // First reset the interface board
  sdsu_command(SDSU_INTERFACE_RESET);

  // Increase timeout for reset to be that of a clear
  send_timeout(n_sync*sdsu_config.config_data.clr_timeout);
  switch (controller.bus) {
#ifdef vxWorks
  case BUS_VME:
    send_command(board_header, (int)SDSU_RRS, SDSU_END);
    break;
#endif
  case BUS_PCI:
    if (send_pci_hcvr(SDSU_PCI_RESET_CONTROLLER, SDSU_SYR, SDSU_END) != SDSU_OK) {
      message_write("Failed to read PCI reply header = (0x%lx)!", reply_buf[0]);
    }
    break;
  default:
    break;
  }

  // test get_reply() for errors and report
  try {
    if ((ios = get_sdn_reply(TIM_ID, SDSU_SYR, DEFAULT_TIMEOUT)) == SDSU_ERROR)
      message_write("Timing reset can not be completed, err=",ios);
    else
      message_write("Reset complete.");
  }
  catch (Error& e) {
    message_write(e.record_error(__FILE__,__LINE__));
    message_write("Timing reset can not be completed, err=",ios);
    // Restore timeout
    send_timeout(n_sync*default_timeout);
    e.rethrow();
  }

  // Restore timeout
  send_timeout(n_sync*default_timeout);
}

/*
 *+
 * FUNCTION NAME: Sdsu2_Controller::~Sdsu2_Controller
 *
 * INVOCATION: delete sdsu2
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Destructor
 *
 * DESCRIPTION: Clear dsp hash tables
 *
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
Sdsu2_Controller::~Sdsu2_Controller()
{
}
/*
 *+
 * FUNCTION NAME: test_command
 *
 * INVOCATION:
 *  test_command(name,board_id, command);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > name - string to be used to idenitfy command name in report
 *  > board_id - identifies board that command is sent to
 *  > command - the SDSU command to be tested
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Tests an SDSU command
 *
 * DESCRIPTION:
 *  An SDSU command is sent is sent to the board identified by board_id. If
 *  the board fails to acknowledge the command appropriately, then the
 *  method throws an exception.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *  The appropriate DSP code must have been downloaded to the controller
 *  for the commands to work.
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::test_command(const char *name, int board_id, Sdsu_Cmd command)
{
  int ret_value;
  char msg_buf[MSGLEN];

  message_write("%s test",name);
  ret_value = sdsu_command(board_id, command);
  if (ret_value != SDSU_OK) {
	snprintf(msg_buf, MSGLEN,"Failed %s test. Error = %d",name, ret_value);
	throw Error(msg_buf, E_ERROR, 0, __FILE__,__LINE__);
  }
  message_write("Passed %s test",name);
}
/*
 *+
 * FUNCTION NAME: test_command
 *
 * INVOCATION:
 *  test_command(name,board_id, command);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > name - string to be used to idenitfy command name in report
 *  > board_id - identifies board that command is sent to
 *  > command - the SDSU command to be tested
 *  > val - parameter that the SDSU command takes
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Tests an SDSU command
 *
 * DESCRIPTION:
 *  An SDSU command is sent is sent to the board identified by board_id. If
 *  the board fails to acknowledge the command appropriately, then the
 *  method throws an exception.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *  The appropriate DSP code must have been downloaded to the controller
 *  for the commands to work.
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::test_command(const char *name, int board_id, Sdsu_Cmd command, int val)
{
  int ret_value;
  char msg_buf[MSGLEN];

  message_write("%s test",name);
  ret_value = sdsu_command(board_id, command, val);
  if (ret_value != SDSU_OK) {
	snprintf(msg_buf, MSGLEN,"Failed %s test. Error = %d",name, ret_value);
	throw Error(msg_buf, E_ERROR, 0, __FILE__,__LINE__);
  }
  message_write("Passed %s test",name);
}
/*
 *+
 * FUNCTION NAME: test_command
 *
 * INVOCATION:
 *  test_command(name,board_id, command);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  > name - string to be used to idenitfy command name in report
 *  > board_id - identifies board that command is sent to
 *  > command - the SDSU command to be tested
 *  > val1 - first parameter that SDSU command takes
 *  > val2 - second parameter that SDSU command takes
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Tests an SDSU command
 *
 * DESCRIPTION:
 *  An SDSU command is sent is sent to the board identified by board_id. If
 *  the board fails to acknowledge the command appropriately, then the
 *  method throws an exception.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *  The appropriate DSP code must have been downloaded to the controller
 *  for the commands to work.
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::test_command(const char *name, int board_id, Sdsu_Cmd command, int val1, int val2)
{
  int ret_value;
  char msg_buf[MSGLEN];

  message_write("%s test",name);
  ret_value = sdsu_command(board_id, command, val1, val2);
  if (ret_value != SDSU_OK) {
	snprintf(msg_buf, MSGLEN,"Failed %s test. Error = %d",name, ret_value);
	throw Error(msg_buf, E_ERROR, 0, __FILE__,__LINE__);
  }
  message_write("Passed %s test",name);
}
/*
 *+
 * FUNCTION NAME: test_boot_commands
 *
 * INVOCATION:
 *  test_boot_commands();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  To test the DSP commands that are supported by the controller
 *  after it has loaded its boot code.
 *
 * DESCRIPTION:
 *  Tests the basic DSP commands that are supported by the code loaded
 *  from boot ROM.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::test_boot_commands()
{

  message_write("**** Running boot command tests ****" );

  test_command("POF", util_id, SDSU_POF);

  // This call does a PON, but changes the timeout. Let the do_power_on
  // routine handle this timeout.
  message_write("PON test");
  try {
	do_power_on();
  }
  catch (Error& ce) {
	message_write("Failed power on test");
    ce.rethrow();
  }
  message_write("Passed PON test" );
  message_write("**** Finished boot command tests - all tests passed successfully ****");
}
/*
 *+
 * FUNCTION NAME: Sdsu_Controller::test_timing_board_commands
 *
 * INVOCATION: test_timing_board_commands
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE: Exercise the timing board interface
 *
 * DESCRIPTION: Sends DSP commands to the timing board
 *              and throws if a command fails
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::test_timing_board_commands()
{
  const int DFS_NPIX_READS=4;
  const int DFS_DO_AVG=1;
  const int SRF_NREFN=8;
  const int SFS_NFOWLER=10;
  const int SUR_NPERIODS=1;
  const int SRM_RESET_MODE=1;
  const int SRD_RESET_DELAY=5;
  const int SNR_RESET_COUNT=2;
  //  const int CDS_DO_START_READ=1;
  const int SET_READ_INTERVAL=12;

  message_write("**** Running timing board tests ****" );

  test_command("IDL", TIM_ID, SDSU_IDL);
  test_command("STP", TIM_ID, SDSU_STP);
  test_command("CLR", TIM_ID, SDSU_CLR);
  test_command("DFS", TIM_ID, SDSU_DFS, DFS_NPIX_READS, DFS_DO_AVG);
  test_command("SRF", TIM_ID, SDSU_SRF, SRF_NREFN);
  test_command("SFS", TIM_ID, SDSU_SFS, SFS_NFOWLER);
  test_command("SUR", TIM_ID, SDSU_SUR, SUR_NPERIODS);
  test_command("SRM", TIM_ID, SDSU_SRM, SRM_RESET_MODE);
  test_command("SRD", TIM_ID, SDSU_SRD, SRD_RESET_DELAY);
  test_command("SNR", TIM_ID, SDSU_SNR, SNR_RESET_COUNT);
  test_command("SET", TIM_ID, SDSU_SET, SET_READ_INTERVAL);

  message_write("**** Finished timing board tests - all tests passed successfully ****" );
}
/*
 *+
 * FUNCTION NAME: Sdsu_Controller::test_coadder_commands
 *
 * INVOCATION: test_coadder_commands
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Exercise the coadder board interface
 *
 * DESCRIPTION:
 *  Sends DSP commands to the coadder board and throws if a command fails.
 *  Only runs if a coadder board is present.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::test_coadder_commands()
{
  int ret_value;
  char msg_buf[MSGLEN];
  int i;


  if (!sdsu_config.config_data.has_coadder_board) {
	message_write("No coadder board present so no coadder tests will be performed" );
	return;
  }

  message_write("**** Running coadder board tests ****" );


  test_command("LCA", TIM_ID, SDSU_LCA);
  test_command("DCA", TIM_ID, SDSU_DCA);

  message_write("CAT test" );
  for (i=1; i<=10; i++) {
	ret_value = sdsu_command(TIM_ID, SDSU_CAT, i);
	if ((ret_value != i) && (ret_value != SDSU_ABORTED)) {
	  snprintf(msg_buf, MSGLEN,"Failed Coadder CAT Test. Wrote %08x, read %08x",i, ret_value);
	  throw Error(msg_buf, E_ERROR, 0, __FILE__,__LINE__);
	}
  }

  message_write("Passed CAT test" );

  test_command("ICA", TIM_ID, SDSU_ICA);
  test_command("SCA", TIM_ID, SDSU_SCA);

  message_write("**** Finished coadder board tests - all tests passed successfully ****");
}

/*
 *+
 * FUNCTION NAME: init_test
 *
 * INVOCATION:
 *  init_test();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Download DSP code to the controller.
 *
 * DESCRIPTION:
 *  Opens the interface and downloads DSP code, previously loaded into memory,
 *  into the controller.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *  The DSP code must have previously been loaded into memory.
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::init_test()
{
  // Download DSP code
  do_DSP_code_setup();

}

/*
 *+
 * FUNCTION NAME: run_dsp_tests
 *
 * INVOCATION:
 *  run_dsp_tests();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *  Test the cicada to controller interface
 *
 * DESCRIPTION:
 *  Sends a series of DSP commands to the controller and test that the
 *  commands have been acknowledged.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::run_dsp_tests()
{
  test_boot_commands();
  test_timing_board_commands();
  test_coadder_commands();
}
/*
 *+
 * FUNCTION NAME: do_voltage_tests
 *
 * INVOCATION:
 *  do_voltage_tests();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 *
 * PURPOSE:
 *
 * DESCRIPTION:
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void Sdsu2_Controller::do_voltage_tests()
{
  message_write("do_voltage_tests: I don't know what to do yet");
}
