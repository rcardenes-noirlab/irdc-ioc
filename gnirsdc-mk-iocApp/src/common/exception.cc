/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  exception.cc
 * 
 * GENERAL DESCRIPTION
 *  Library of C++ exception routines
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 1993
 *
 * HISTORY
 *
 */

/* Include files */
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <errno.h>
#include "exception.h"
#include "utility.h"

using namespace std;

/* defines */

/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/*
 * FUNCTION NAME: strerrtype
 * 
 * INVOCATION: str = strerrtype(Error_Type e)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > e - error type
 * 
 * FUNCTION VALUE: The error type as a string
 * 
 * PURPOSE: Retrieve a string representation of an error type
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
const string strerrtype(Error_Type e)
{
  string ret;
  switch (e) {
    case E_FATAL:
      ret = "Fatal error";
      break;
    case E_ERROR:
      ret = "Error";
      break;
    case E_WARNING:
      ret = "Warning";
      break;
    case E_INFO:
      ret = "Info";
      break;
    default:
      ret = "";
      break;
  }
  return ret;
}

/*
 * FUNCTION NAME: log_exception
 * 
 * INVOCATION: log_exception(std::exception& e)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > e - exception to log
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Log an exception
 * 
 * DESCRIPTION: Simply uses cerr for the moment
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: Should use a more sophisticated logging approach
 *
 *-------------------------------------------------------------------------*/
void log_exception(std::exception& e, const char* f, const int l)
{
  cerr << f << ": line " << l << ".  Caught exception: " << e.what() << endl;
}

/* class function definitions */

/*============================================================================
 *  Error Class
 *===========================================================================*/
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: Error::Error
 * 
 * INVOCATION: throw Error(const char* m)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > m - error message
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Constructor - set error variables
 * 
 * DESCRIPTION: 
 * This is the generic Error with errnum == -1, and type == E_ERROR.
 * Used for passing data associated with an exception to handler.
 * Stores arguments in member variables for use in handler.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
Error::Error(const char* m) :
  Loggable_Exception(m),
  msg(m),
  errnum(-1),
  type(E_ERROR)
{
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: Error::Error
 * 
 * INVOCATION: throw Error(Error& e,const char* f,int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > e - error to log
 * > f - filename where exception thrown
 * > l - lineno of filename
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Constructor - set error variables
 * 
 * DESCRIPTION: 
 * Used for passing data associated with an exception to handler.
 * Stores arguments in member variables for use in handler.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
Error::Error(Error& e,const char* f,int l) : 
  Loggable_Exception(e,f,l),
  msg(e.msg),
  errnum(e.errnum),
  type(e.type)
{
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: Error::Error
 * 
 * INVOCATION: throw Error(std::exception& e,Error_Type t,const char* f,int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > e - error to log
 * > t - error type
 * > f - filename where exception thrown
 * > l - lineno of filename
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Constructor - set error variables
 * 
 * DESCRIPTION: 
 * Used for passing data associated with an exception to handler.
 * Stores arguments in member variables for use in handler.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
Error::Error(std::exception& e,Error_Type t,const char* f,int l) : 
  Loggable_Exception(e,f,l),
  msg(e.what()),
  errnum(-1),
  type(t)
{
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: Error::Error
 * 
 * INVOCATION: throw Error(const char* m,Error_Type e,int eno,const char* f,int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > m - error message
 * > e - error type
 * > eno - error code (usually Unix errno)
 * > f - filename where exception thrown
 * > l - lineno of filename
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Constructor - set error variables
 * 
 * DESCRIPTION: 
 * Used for passing data associated with an exception to handler.
 * Stores arguments in member variables for use in handler.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
Error::Error(const char* m,Error_Type e,int eno,const char* f,int l) : 
  Loggable_Exception(strerrtype(e)+": "+m+" ("+to_string(eno)+": "+strerror(eno)+")", f, l),
  msg(m),
  errnum(eno),
  type(e)
{
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: Error::Error
 * 
 * INVOCATION: throw Error(const char* m,Error_Type e,int eno,const char* f,int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > m - error message
 * > e - error type
 * > f - filename where exception thrown
 * > l - lineno of filename
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Constructor - set error variables
 * 
 * DESCRIPTION: 
 * Used for passing data associated with an exception to handler.
 * Stores arguments in member variables for use in handler.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
Error::Error(const char* m,Error_Type e,const char* f,int l) :
  Loggable_Exception(strerrtype(e)+": "+m, f, l),
  msg(m),
  errnum(-1),
  type(e)
{
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: Error::Error
 * 
 * INVOCATION: throw Error(ostringstream& o,Error_Type e,int eno,const char* f,int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > o - error stream
 * > e - error type
 * > eno - error code (usually Unix errno)
 * > f - filename where exception thrown
 * > l - lineno of filename
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Constructor - set error variables
 * 
 * DESCRIPTION: 
 * Used for passing data associated with an exception to handler.
 * Stores arguments in member variables for use in handler.
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
Error::Error(ostringstream& o,Error_Type e,int eno,const char* f,int l) : 
  Loggable_Exception(strerrtype(e)+": "+o.str()+" ("+to_string(eno)+": "+strerror(eno)+")", f, l), 
  msg(o.str()),
  errnum(eno),
  type(e)
{  
  // Unfreeze the stream so that it can be automatically freed
  // o.rdbuf()->freeze(0);
}

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: Error::record_error
 * 
 * INVOCATION: record_error(const char* f, int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > f - filename where exception thrown
 * > l - lineno of filename
 * 
 * FUNCTION VALUE: formatted char*
 * 
 * PURPOSE: Method for recording error in message buffer
 * 
 * DESCRIPTION: 
 * Formats an error message in iostream buffer and returns address of char buffer
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
char* Error::record_error(const char* f, int l)
{
  ostringstream ostr;
  string buf;
  ostr << f << " line " << l << " caught: " << what() << ends;
  buf = ostr.str();
  // Unfreeze the stream so that it can be automatically freed
  // ostr.rdbuf()->freeze(0);
  return (char*) buf.c_str();
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: Error::print_error
 * 
 * INVOCATION: print_error(const char* f, int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > f - filename where exception thrown
 * > l - lineno of filename
 * 
 * FUNCTION VALUE: none
 * 
 * PURPOSE: Method for printing an error message to stdout
 * 
 * DESCRIPTION: 
 * Formats an error message on cout iostream
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void Error::print_error(const char* f, int l)
{  
  cout << record_error(f, l) << endl;
}
/*============================================================================
 *  End of Error Class
 *===========================================================================*/

//////////////////////////////////////////////////////////
//
// IPC_Error class methods 
//
//////////////////////////////////////////////////////////
/*
 *+ 
 * FUNCTION NAME: IPC_Error::IPC_Error
 * 
 * INVOCATION: throw IPC_Error(const char* m,Error_Type e,int eno,const char* f,int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > m - message string
 * > e - error type
 * > eno - error code
 * > f - source filename
 * > l - source line no
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Simple IPC_Error constructor
 * 
 * DESCRIPTION: Does nothing - defers to base class Error
 * 
 * EXTERNAL VARIABLES: 
 * 
 * PRIOR REQUIREMENTS: 
 * 
 * DEFICIENCIES: 
 *
 *- 
 */
IPC_Error::IPC_Error(const char* m, Error_Type e, int eno, const char* f, int l)
  :Error(m,e,eno,f,l) 
{
}
IPC_Error::~IPC_Error()  throw() {}

/*+
 * FUNCTION NAME: Loggable_Exception
 * 
 * INVOCATION: Loggable_Exception(string m)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *    > m - error message to log
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Constructor
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 */
Loggable_Exception::Loggable_Exception(string m) :
  what_str(m)
{
}

/*+
 * FUNCTION NAME: Loggable_Exception
 * 
 * INVOCATION: Loggable_Exception(const char *m, const char* f, int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *    > m - error message to relay
 *    > f - file from which error was relayed
 *    > l - line number from which error was relayed
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 */
Loggable_Exception::Loggable_Exception(string m, const char* f, int l) :
  what_str(m + ", " + f + " line " + to_string(l))
{  
}

/*+
 * FUNCTION NAME: Loggable_Exception
 * 
 * INVOCATION: Loggable_Exception(std::exception& e, const char* f, int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *    > e - error to relay
 *    > f - file from which error was relayed
 *    > l - line from which error was relayed
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 */
Loggable_Exception::Loggable_Exception(std::exception& e, const char* f, int l) :
  what_str(string(e.what()) + ", " + f + " line " + to_string(l))
{
}

/*+
 * FUNCTION NAME: Loggable_Exception
 * 
 * INVOCATION: Loggable_Exception(ostringstream& o, const char* f, int l)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 *    > o - error message to relay
 *    > f - file from which error was relayed
 *    > l - line from which error was relayed
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 */
Loggable_Exception::Loggable_Exception(ostringstream& o, const char* f, int l) :
  what_str(string(o.str()) + ", " + f + " line " + to_string(l))
{
  // Unfreeze the stream so that it can be automatically freed
  // o.rdbuf()->freeze(0);
}

/*+
 * FUNCTION NAME: what
 * 
 * INVOCATION: what()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: *char - the error message
 * 
 * PURPOSE: 
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 */
const char * Loggable_Exception::what() const throw()
{
  return what_str.c_str();
}

/*+
 * FUNCTION NAME: Loggable_Exception::rethrow()
 * 
 * INVOCATION: rethrow()
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: rethrow if can't handle here
 * 
 * DESCRIPTION: 
 * Simply rethrows the exception
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void Loggable_Exception::rethrow()
{  
  throw;
}
Timeout_Error::~Timeout_Error()  throw() {}
Null_Pointer_Error::~Null_Pointer_Error()  throw() {}
Mutex_Error::~Mutex_Error()  throw() {}
