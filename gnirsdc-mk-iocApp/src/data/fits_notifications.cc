/*
 * Copyright (c) 2000-2003 RSAA
 * $Id: fits_notifications.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 *
 * GEMINI GNIRS PROJECT
 * 
 * FILENAME fits_notifications.cc
 * 
 * GENERAL DESCRIPTION
 *
 * This program waits for image notification messages to be sent to a notification mailbox.
 * When messages arrive it prints out a description of the image that is available.
 *
 * To run on
 * Solaris:      $ ./fits_notification
 * 
 * $Log
 *
 *
 */
/*include files*/
#ifdef vxWorks
#error This code only runs under Solaris
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <netinet/in.h>
#include "ipc.h"
#include "fits.h"
#include "image.h"
#include "configuration.h"
#include "utility.h"

using namespace std;

/* global variables */
extern int errno;

/* function definitions */
static void notify_new_image(Mailbox* image_mox, Image_Descriptor& image);

/* Main program - handles input from serial or socket interface */
int main(int argc, char *argv[])
{
  // Parameters

  // data structures 
  Image_Descriptor image;
  Mailbox* image_mbox = NULL;                     // Mailbox for latest image bulletins
  char qname[IPCNAMELEN];

  // Trap any exceptions
  try {   
    cout << "Fits_Notification: Starting." << endl;
    
    cout << "Fits_Notification: Connecting to image message queue ... " << endl;
    ipc_name(qname, "Image", ".", geteuid(), 0);
    image_mbox = new Mailbox(qname, CONNECT, RWALL);    
    
    // Start accepting connections - does not exit
    while (true) {
      image_mbox->get_message((char *)&image, sizeof(Image_Descriptor), IPC_WAIT);
      cout << "Fits_Notification: message received ... " << endl;
      cout << "Image name = " << image.image_name << endl;
    }
  }
  catch (Error & e) {
    e.print_error(__FILE__, __LINE__);
  } 
}
      
