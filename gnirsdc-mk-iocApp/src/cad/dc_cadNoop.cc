/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_cadNoop.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI NIFS PROJECT
 * 
 * FILENAME cad_noop.c
 * 
 * GENERAL DESCRIPTION
 *
 * Handle CADS from commands that are ignored by the NIFS DC.  These are DATUM,
 * GUIDE, ENDGUIDE, VERIFY, ENDVERIFY and ENDOBSERVE.  Their equivalent CAR
 * records will be set to busy and then back to idle.
 *
 * Acknowledgment
 * Uses code developed by J. Tvedt of NOAO for GMOS.
 *
 * $Log: dc_cadNoop.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.8  2005/09/26 04:54:14  pjy
 * Added call to set_verify... to handle OBSERVE Cad being busy.
 *
 * Revision 1.7  2005/06/07 06:49:04  pjy
 * Remove hard coded nifs string
 *
 * Revision 1.6  2004/08/20 02:27:04  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.5  2004/04/27 07:25:55  pjy
 * T2.2
 *
 * Revision 1.1  2004/03/23 01:59:49  pjy
 * Gem 8.6 port
 *
 * Revision 1.3  2003/09/19 06:24:21  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.2  2003/03/25 06:26:20  pjy
 * Consistent naming using _
 *
 * Revision 1.1  2003/03/11 01:29:28  pjy
 * Import modules
 *
 * 
 */
/* VxWorks include files */
#include <cstdio>
#include <cstring>
#ifdef __cplusplus
extern "C" {
#endif
#include "cadRecord.h"
#include "cad.h"
#include "dbAccess.h"
#include "dbCommon.h"
#include <registryFunction.h>
#include <epicsExport.h>
#ifdef __cplusplus
}
#endif
#include "logging.h"
#include "dc.h"
#include "dc_epics_db.h"

#ifdef __cplusplus
extern "C" long dcCADnoOp(struct cadRecord *pCad);
extern "C" void set_and_verify_output_cad(struct cadRecord *pCad);
#endif

/* Function declarations */
static long dcProcessRec(const char *prefix, const char *record);

/*
 *+
 * FUNCTION NAME:
 * dcCADnoOp
 *
 * INVOCATION:
 * struct cadRecord *pCad;
 * status = dcCADnoOp( pCad );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * ! pCad   (struct cadRecord *)   pointer to CAD data structure
 *
 * FUNCTION VALUE:
 * long  Status value written to CAD VAL field
 *
 * PURPOSE:
 * User defined function for noop CAD records
 *
 * DESCRIPTION:
 * This routine is called whenever a noop CAD record is processed.
 * On a START directive, this function forces processing of the
 * related car record. 
 *
 * The function always accepts the command unless there is an error
 * on the START.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * It is assumed the CAD record has already been initialized.
 *
 * DEFICIENCIES:
 * None
 *
 *-
 */

long dcCADnoOp(struct cadRecord *pCad) 
{
  long status;                     /* return status */
  char rec[80];
  char name[80];
  
  status = CAD_ACCEPT;
  
  logger::message(logger::Level::Min, "dcCADnoOp: %d directive.", pCad->dir);
  
  /* Create name of record to process car from name of this record*/
  strcpy(rec,pCad->name);
  strtok(rec,":");
  strtok(NULL,":");
  strcpy(name,strtok(NULL,":"));
  strcat(name,"Toggle");
  // strcat(name,0);
  
  switch (pCad->dir){
  case menuDirectiveMARK:                /* No action required for these*/
  case menuDirectiveCLEAR:                    
    break;

  case menuDirectivePRESET:		
    /* Reject the command if the arguments don't validate */
    try {
      // Verify input parameters and set outputs
      // Though no inputs expected for these CADs, the call will check to see if OBSERVE is 
      // running and REJECT this CAD if so
      set_and_verify_output_cad(pCad);
      logger::message(logger::Level::Full,"dcCADNoop: Noop Cad verified ok!");
    } 
    catch (Error& err) {
      status = CAD_REJECT;
      err.print_error(__FILE__, __LINE__);
      strncpy(pCad->mess, err.msg.c_str(), MAX_STRING_SIZE-1);
    }
    break;

  case menuDirectiveSTART:		
    /* process record, interface record sets car to busy and then idle after a few seconds*/
    if (dcProcessRec(DCTOP, name) != OK) {
      strncpy(pCad->mess, "dcCADnoOp: Error toggling noopC", MAX_STRING_SIZE - 1);
      status = CAD_REJECT;
    }
    
    break;
    
  case menuDirectiveSTOP:			/* Really can't	*/
    strncpy(pCad->mess, "dcCADnoOp: Cannot stop!", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
    
  default:				/* Unknown directive*/
    strncpy(pCad->mess, "dcCADnoOp: Unrecognized directive", MAX_STRING_SIZE-1);
    status = CAD_REJECT;
    break;
  } 
  return status;
}


/*
 *+
 * FUNCTION NAME:
 * dcProcessRec
 *
 * INVOCATION:
 * char *prefix;
 * char *record;
 * long status;
 *
 * status = dcProcessRec(prefix, record);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > prefix   (char *)   pointer to string containing db prefix
 * > reocrd   (char *)   pointer to string containing db reocrd name
 *
 * FUNCTION VALUE:
 * long  Status value, 0 indicates success
 *
 * PURPOSE:
 * Process the specified EPICS record
 *
 * DESCRIPTION:
 * This routine constructs the complete record name from the input
 * parameters and calls the appropriate db routines to cause that
 * record to be processed.
 *
 * EXTERNAL VARIABLES:
 * None
 *
 * PRIOR REQUIREMENTS:
 * The specified reocrd must exist and able to be processed.
 *
 * DEFICIENCIES:
 * None known
 *
 * HISTORY (optional):
 * 23-Apr-1997  Original version.		   J.E. Tvedt
 *
 *-
 */
static long dcProcessRec(const char *prefix, const char *record)
{
  char fieldName[MAX_STRING_SIZE];
  struct dbAddr addr;
  long status;
  ostringstream ostr;
  
  // Initialize the return value 
  status = OK;
  
  // Form the fieldName of the record 
  snprintf(fieldName,MAX_STRING_SIZE-1,"%s%s.VAL",prefix,record);
  
  try {
    // Get the address of the data structure  and handle any errors 
    if ((status = dbNameToAddr (fieldName,&addr)) != OK) {
      ostr << "Failed in dbNameToAddr for " << fieldName << ends;
      throw Error(ostr.str().c_str(), E_ERROR, status, __FILE__, __LINE__);
    }
    
    // If successful, process the record. 
    if ((status = dbProcess(addr.precord)) != OK) {
      ostr << "Failed in dbProcess for " << fieldName << ends;
      throw Error(ostr.str().c_str(), E_ERROR, status, __FILE__, __LINE__);
    }
  } 
  catch (Error& err) {
    status = err.errnum;
    logger::message(logger::Level::Min, "dcProcessRec: %s", err.record_error(__FILE__, __LINE__));    
  }
  
  return status;
}

epicsRegisterFunction(dcCADnoOp);
