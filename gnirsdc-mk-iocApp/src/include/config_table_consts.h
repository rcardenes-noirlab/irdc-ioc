/*
 * Copyright (c) 2003 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME config_table_consts.h
 * 
 * GENERAL DESCRIPTION
 *   Contains the constants used by config_table.h
 *   Separated from that file so that teldisk can obtain access to
 *   configuration constants without needing to link against libcicada
 * 
 */

#ifndef _CONFIG_TABLE_CONSTS_H
#define _CONFIG_TABLE_CONSTS_H

// Constants 

//+ Name of table files
// Use directory named cnf in vxWorks top level DC directory
// because use of config clashes with other use.
const char INSTRUMENT_TABLE[] =  "cnf/instrument_table";
const char CAMERA_TABLE[] =  "cnf/camera_table";
const char CONTROLLER_TABLE[] =  "cnf/controller_table";
const char CHIP_TABLE[] =  "cnf/chip_table";


//+ Misc string lengths

//+ typedefs 

//+ maximum numbers of any configuration item
const int CONFIG_MAX_INSTRUMENTS = 40;   // Maximum number of instruments.
const int CONFIG_MAX_CAMERAS = 40;       // Maximum number of CAMERAS we own
const int CONFIG_MAX_CONTROLLERS = 40;   // Maximum number of controllers.
const int CONFIG_MAX_CHIPS = 128;         // Maximum number of CHIPS we own

#endif //_CONFIG_TABLE_CONSTS_H 
