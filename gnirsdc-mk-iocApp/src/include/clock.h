#ifndef __CLOCK_H__
#define __CLOCK_H__

#include <chrono>
#include <map>
#include <string>
#include <functional>
#include <epicsTime.h>

using std::chrono::system_clock;
using std::chrono::steady_clock;
using sys_time_point = std::chrono::time_point<system_clock>;
using sty_time_point = std::chrono::time_point<steady_clock>;


class Clock {
public:
	Clock();

	void reset();

	void set_timing_prefix(std::string new_t_prefix);
	void set_timing_index(unsigned new_t_index);
	unsigned timing_index() const;

	sty_time_point set_period_start() { return sty_period_start = steady_clock::now(); }
	sty_time_point set_period_end() { return sty_period_end = steady_clock::now(); }

	void set_period_start(const sty_time_point &&t);
	void set_period_end(const sty_time_point &&t);

	sty_time_point get_period_start() const;
	sty_time_point get_period_end() const;

	epicsTime to_epicsTime(const sty_time_point& t) const;
	epicsTime to_epicsTime(const sty_time_point&& t) const;

	void add_measurement(sty_time_point measurement, bool increment_index=true);

	void visit_measurements(std::function<void(std::string, double)> fn) const;

	std::string time_point_to_string(const sty_time_point &t) const;
	std::string ref_time_point_to_string() const {
		return time_point_to_string(sty_clock_ref);
	}

private:
	sys_time_point sys_clock_ref;
	sty_time_point sty_clock_ref;

	sty_time_point sty_period_start;
	sty_time_point sty_period_end;

	std::map<std::string, sty_time_point> measurements;

	std::string t_prefix;
	unsigned t_index;
};

#endif // __CLOCK_H__
