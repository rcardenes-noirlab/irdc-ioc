[schematic2]
uniq 10
[tools]
[detail]
w 1800 2171 100 0 n#1 hwin.hwin#97.in 1696 2160 1952 2160 ecad4.observe.INPB
w 1848 2267 100 0 n#2 estringouts.observationID.OUT 1696 2384 1792 2384 1792 2256 1952 2256 ecad4.observe.A
w 2344 2235 100 0 n#3 ecad4.observe.OUTA 2272 2224 2464 2224 hwout.hwout#86.outp
w 1824 2507 -100 0 CLID inhier.CLID.P 1808 2496 1888 2496 1888 2416 1952 2416 ecad4.observe.ICID
w 2332 2539 100 0 n#4 ecad4.observe.VAL 2272 2448 2336 2448 2336 2640 2436 2640 outhier.VAL.p
w 2366 2459 100 0 n#4 junction 2336 2448 2432 2448 2432 1872 2480 1872 cadPlus.observeP.STAT
w 2312 1819 100 0 n#5 ecad4.observe.SPLK 2272 1808 2400 1808 2400 1904 2480 1904 cadPlus.observeP.SPIN
w 2400 1947 100 0 n#6 ecad4.observe.STLK 2272 1840 2368 1840 2368 1936 2480 1936 cadPlus.observeP.STIN
w 2364 2491 100 0 n#7 ecad4.observe.MESS 2272 2416 2368 2416 2368 2576 2436 2576 outhier.MESS.p
w 1916 2507 100 0 n#8 inhier.DIR.P 1888 2576 1920 2576 1920 2448 1952 2448 ecad4.observe.DIR
w 1266 1442 -100 0 n#9 estringouts.fakeLockTemp.FLNK 1256 1432 1328 1432 1328 1568 1456 1568 estringouts.lockTmp.DOL
[cell use]
use hwin 1040 1527 100 0 hwin#103
xform 0 1136 1568
p 1043 1560 100 0 -1 val(in):$(engtop)lockTmp
use hwin 1504 2119 100 0 hwin#97
xform 0 1600 2160
p 1507 2152 100 0 -1 val(in):$(sadtop)health.VAL PP MS
use estringouts 1456 1463 100 0 lockTmp
xform 0 1584 1536
p 1456 1424 100 0 1 DESC:Temperature Lock reflected from CC DB
p 1392 1342 100 0 0 OMSL:closed_loop
p 1392 1374 100 0 0 PINI:YES
p 1392 1470 100 0 0 SCAN:10 second
p 1392 1502 100 0 0 VAL:CHANGING
p 1424 1568 75 1280 -1 pproc(DOL):PP
use estringouts 1440 2327 100 0 observationID
xform 0 1568 2400
use hwout 2464 2183 100 0 hwout#86
xform 0 2560 2224
p 2560 2215 100 0 -1 val(outp):$(sadtop)dataLabel.VAL PP MS
use inhier 1792 2455 100 0 CLID
xform 0 1808 2496
use inhier 1872 2535 100 0 DIR
xform 0 1888 2576
use cadPlus 2480 1984 150 0 observeP
xform 0 2560 1888
p 2480 1776 100 0 1 set1:cad observeP
use outhier 2404 2599 100 0 VAL
xform 0 2420 2640
use outhier 2404 2535 100 0 MESS
xform 0 2420 2576
use ecad4 2028 2492 150 0 observe
xform 0 2112 2128
p 1888 1552 100 768 0 FTVA:STRING
p 1888 1520 100 3072 0 FTVB:STRING
p 3476 2620 100 0 0 MFLG:THREE STATES
p 2032 1744 100 0 1 SNAM:dcCADobserve
p 2272 2224 75 768 -1 pproc(OUTA):PP
use eborderC 608 799 100 0 eborderC#0
xform 0 2288 2104
p 3184 952 100 768 -1 author:Peter Young
p 3184 1088 40 0 -1 comment1:1997-10-01 GMOS version. Ken Ramey
p 3184 1068 40 0 -1 comment2:1999-01-28 Added calc record. Janet Tvedt
p 3184 1048 40 0 -1 comment3:2000-12-12 Modified for GNIRS. Peter Young
p 3184 920 100 768 -1 date:2000-12-12
p 3408 1000 200 768 -1 file:dcObserveCad.sch
p 3680 952 100 0 -1 page:1
p 3792 952 100 0 -1 pages:1
p 3456 952 100 0 -1 revision:0
p 3408 1064 150 768 -1 system:Gemini GNIRS DC
use estringouts 1011 1340 100 0 fakeLockTemp
xform 0 1128 1416
p 1009 1308 100 0 1 def(DOL):NOT_CHANGING
[comments]
