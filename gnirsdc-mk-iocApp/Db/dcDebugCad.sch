[schematic2]
uniq 23
[tools]
[detail]
w 4520 2811 100 0 n#22 ecad4.debug.OUTA 4448 2800 4640 2800 hwout.hwout#21.outp
w 4476 3101 100 0 n#3 ecad4.debug.VAL 4448 3024 4480 3024 4480 3188 4576 3188 outhier.VAL.p
w 4526 3035 100 0 n#3 cadPlus.debugP.STAT 4704 2352 4608 2352 4608 3024 4480 3024 junction
w 4552 2395 100 0 n#20 ecad4.debug.SPLK 4448 2384 4704 2384 cadPlus.debugP.SPIN
w 4552 2427 100 0 n#19 ecad4.debug.STLK 4448 2416 4704 2416 cadPlus.debugP.STIN
w 3976 3083 -100 0 CLID inhier.CLID.P 3936 3072 4064 3072 4064 2992 4128 2992 ecad4.debug.ICID
w 3568 2851 100 0 n#14 embbis.debugMenu.FLNK 3536 2848 3648 2848 estringouts.debugMenuStr.SLNK
w 3584 2883 100 0 n#10 embbis.debugMenu.VAL 3536 2816 3568 2816 3568 2880 3648 2880 estringouts.debugMenuStr.DOL
w 3998 2835 100 0 n#8 estringouts.debugMenuStr.OUT 3904 2832 4128 2832 ecad4.debug.A
w 4508 3053 100 0 n#4 ecad4.debug.MESS 4448 2992 4512 2992 4512 3124 4576 3124 outhier.MESS.p
w 4092 3085 100 0 n#2 inhier.DIR.P 4000 3156 4096 3156 4096 3024 4128 3024 ecad4.debug.DIR
s 2480 1984 200 0 The CAD user subroutine sets a global variable in the CICS logging utilities to the desired debug level. 
s 2480 1920 200 0 The local subroutine CADdebug will set the debug level in the gnirs_dc_pars structure.
[cell use]
use hwout 4640 2759 100 0 hwout#21
xform 0 4736 2800
p 4736 2791 100 0 -1 val(outp):$(sadtop)debugMode.VAL PP MS
use cadPlus 4736 2480 100 0 debugP
xform 0 4784 2368
p 4704 2256 100 0 1 set1:cad debugP
use inhier 3920 3031 100 0 CLID
xform 0 3936 3072
use inhier 3984 3115 100 0 DIR
xform 0 4000 3156
use embbis 3356 2892 100 0 debugMenu
xform 0 3408 2832
p 3312 2592 100 0 0 FFST:
p 3312 2592 100 0 1 FRST:SDSU
p 3312 2592 100 0 0 FTST:
p 3312 2624 100 0 1 FVST:STATE
p 3312 2720 100 0 1 ONST:NONE
p 3312 2528 100 0 1 SVST:SPECIAL
p 3312 2560 100 0 1 SXST:CALCS
p 3312 2656 100 0 1 THST:FULL
p 3312 2688 100 0 1 TWST:MIN
p 3312 2752 100 0 1 ZRST:NOLOG
use estringouts 3740 2916 100 0 debugMenuStr
xform 0 3776 2848
p 3712 2768 100 0 1 OMSL:closed_loop
use outhier 4544 3083 100 0 MESS
xform 0 4560 3124
use outhier 4544 3147 100 0 VAL
xform 0 4560 3188
use ecad4 4220 3080 100 0 debug
xform 0 4288 2704
p 4224 2832 100 0 1 FTVA:STRING
p 4224 2800 100 0 0 FTVB:STRING
p 4208 2288 100 0 1 INAM:CADdebugInit
p 3476 2620 100 0 0 MFLG:THREE STATES
p 4208 2320 100 0 1 SNAM:dcCADdebug
p 4448 2426 75 0 -1 pproc(STLK):PP
use eborderC 2368 1403 100 0 eborderC#0
xform 0 4048 2708
p 4944 1556 100 768 -1 author:Peter Young
p 4944 1680 40 0 -1 comment1:1997-10-01 Original version Ken Ramey
p 4944 1660 40 0 -1 comment2:1999-01-25 Modified  Janet Tvedt
p 4944 1640 40 0 -1 comment3:2001-02-05 Modified for GNIRS Peter Young
p 4944 1524 100 768 -1 date:2001-02-05
p 5168 1604 200 768 -1 file:dcDebugCad.sch
p 5440 1556 100 0 -1 page:1
p 5552 1556 100 0 -1 pages:1
p 5216 1556 100 0 -1 revision:0
p 5168 1668 150 768 -1 system:Gemini GNIRS DC
[comments]
