[schematic2]
uniq 29
[tools]
[detail]
w 1634 1067 100 0 n#1 ecad20.dataView.OUTI 1584 1056 1744 1056 hwout.hwout#489.outp
w 1170 795 100 0 n#2 estringouts.view_cosmMin.OUT 1152 784 1248 784 1248 1088 1264 1088 ecad20.dataView.I
w 1634 1387 100 0 n#3 ecad20.dataView.OUTD 1584 1376 1744 1376 hwout.hwout#485.outp
w 802 1355 100 0 n#4 ebos.view_doCmpIm.OUT 528 1152 576 1152 576 1344 1088 1344 1088 1408 1264 1408 ecad20.dataView.D
w 586 1675 100 0 n#5 embbis.view_dataDest.FLNK 528 1728 576 1728 576 1664 656 1664 estringouts.view_dataDestStr.SLNK
w 562 1707 100 0 n#6 embbis.view_dataDest.VAL 528 1696 656 1696 estringouts.view_dataDestStr.DOL
w 1018 1659 100 0 n#7 estringouts.view_dataDestStr.OUT 912 1648 1184 1648 1184 1600 1264 1600 ecad20.dataView.A
w 874 1083 100 0 n#8 ebos.view_cosmRej.OUT 528 912 624 912 624 1072 1184 1072 1184 1216 1264 1216 ecad20.dataView.G
w 1162 955 100 0 n#9 estringouts.view_cosmThrsh.OUT 1152 944 1232 944 1232 1152 1264 1152 ecad20.dataView.H
w 986 1259 100 0 n#10 estringouts.view_waveMin.OUT 912 1248 1120 1248 1120 1344 1264 1344 ecad20.dataView.E
w 1186 1291 100 0 n#11 estringouts.view_waveMax.OUT 1152 1152 1168 1152 1168 1280 1264 1280 ecad20.dataView.F
w 1178 1483 100 0 n#12 estringouts.view_subFile.OUT 1152 1472 1264 1472 ecad20.dataView.C
w 1634 1131 100 0 n#13 ecad20.dataView.OUTH 1584 1120 1744 1120 hwout.hwout#455.outp
w 1634 1195 100 0 n#14 ecad20.dataView.OUTG 1584 1184 1744 1184 hwout.hwout#453.outp
w 90 523 100 0 n#15 elongins.dataViewID.VAL 32 512 208 512 ecars.dataViewC.ICID
w 1810 171 100 0 n#16 junction 1680 1792 1728 1792 1728 160 1952 160 cadPlus.dataViewP.STAT
w 522 1995 100 0 n#16 ecad20.dataView.VAL 1584 1792 1680 1792 1680 1984 -576 1984 -576 1232 -432 1232 eapply.dataViewApply.INPA
w 1658 139 100 0 n#17 ecad20.dataView.SPLK 1584 128 1792 128 1792 192 1952 192 cadPlus.dataViewP.SPIN
w 1794 235 100 0 n#18 ecad20.dataView.STLK 1584 160 1696 160 1696 224 1952 224 cadPlus.dataViewP.STIN
w 538 1899 100 0 n#19 eapply.dataViewApply.OUTA -48 1232 144 1232 144 1888 992 1888 992 1792 1264 1792 ecad20.dataView.DIR
w 1634 1259 100 0 n#20 ecad20.dataView.OUTF 1584 1248 1744 1248 hwout.hwout#338.outp
w 1634 1323 100 0 n#21 ecad20.dataView.OUTE 1584 1312 1744 1312 hwout.hwout#412.outp
w 1634 1451 100 0 n#22 ecad20.dataView.OUTC 1584 1440 1744 1440 hwout.hwout#406.outp
w 1634 1515 100 0 n#23 ecad20.dataView.OUTB 1584 1504 1744 1504 hwout.hwout#405.outp
w 1634 1579 100 0 n#24 ecad20.dataView.OUTA 1584 1568 1744 1568 hwout.hwout#400.outp
w 826 1579 100 0 n#25 ecad20.dataView.B 1264 1536 1152 1536 1152 1568 560 1568 560 1424 528 1424 ebos.view_doSubFile.OUT
w 522 2027 100 0 n#26 ecad20.dataView.MESS 1584 1760 1712 1760 1712 2016 -608 2016 -608 1200 -432 1200 eapply.dataViewApply.INMA
w -494 1307 -100 0 CLID inhier.CLID.P -528 1376 -496 1376 -496 1296 -432 1296 eapply.dataViewApply.CLID
w 538 1867 100 0 n#27 eapply.dataViewApply.OCLA -48 1200 192 1200 192 1856 944 1856 944 1760 1264 1760 ecad20.dataView.ICID
w 68 1371 100 0 MESS eapply.dataViewApply.MESS -48 1296 64 1296 64 1456 112 1456 outhier.MESS.p
w -12 1427 100 0 VAL eapply.dataViewApply.VAL -48 1328 -16 1328 -16 1536 112 1536 outhier.VAL.p
w -460 1395 100 0 n#28 inhier.DIR.P -496 1472 -464 1472 -464 1328 -432 1328 eapply.dataViewApply.DIR
s -688 -48 200 0 Up to 20 observation parameters can be set here
s -688 32 200 0 View mode data parameters Apply/CAD record
[cell use]
use hwout 1744 1015 100 0 hwout#489
xform 0 1840 1056
p 1888 1056 100 0 -1 val(outp):$(sadtop)d_view_cosmMin PP NMS
use hwout 1744 1207 100 0 hwout#338
xform 0 1840 1248
p 1888 1248 100 0 -1 val(outp):$(sadtop)d_view_waveMax PP NMS
use hwout 1744 1527 100 0 hwout#400
xform 0 1840 1568
p 1888 1568 100 0 -1 val(outp):$(sadtop)d_view_dataDest PP NMS
use hwout 1744 1463 100 0 hwout#405
xform 0 1840 1504
p 1888 1504 100 0 -1 val(outp):$(sadtop)d_view_doSubFile PP NMS
use hwout 1744 1399 100 0 hwout#406
xform 0 1840 1440
p 1888 1440 100 0 -1 val(outp):$(sadtop)d_view_subFile PP NMS
use hwout 1744 1271 100 0 hwout#412
xform 0 1840 1312
p 1888 1312 100 0 -1 val(outp):$(sadtop)d_view_waveMin PP NMS
use hwout 1744 1143 100 0 hwout#453
xform 0 1840 1184
p 1888 1184 100 0 -1 val(outp):$(sadtop)d_view_cosmRej PP NMS
use hwout 1744 1079 100 0 hwout#455
xform 0 1840 1120
p 1888 1120 100 0 -1 val(outp):$(sadtop)d_view_cosmThrsh PP NMS
use hwout 1744 1335 100 0 hwout#485
xform 0 1840 1376
p 1888 1376 100 0 -1 val(outp):$(sadtop)d_view_doCmpIm PP NMS
use estringouts 896 727 100 0 view_cosmMin
xform 0 1024 800
use estringouts 896 1415 100 0 view_subFile
xform 0 1024 1488
use estringouts 656 1191 100 0 view_waveMin
xform 0 784 1264
use estringouts 896 1095 100 0 view_waveMax
xform 0 1024 1168
use estringouts 896 887 100 0 view_cosmThrsh
xform 0 1024 960
use estringouts 656 1591 100 0 view_dataDestStr
xform 0 784 1664
p 592 1470 100 0 0 OMSL:closed_loop
use ebos 336 1536 100 0 view_doSubFile
xform 0 400 1456
p 352 1328 100 0 1 ONAM:No
p 352 1360 100 0 1 ZNAM:Yes
use ebos 336 1024 100 0 view_cosmRej
xform 0 400 944
p 352 816 100 0 1 ONAM:No
p 352 848 100 0 1 ZNAM:Yes
use ebos 336 1264 100 0 view_doCmpIm
xform 0 400 1184
p 352 1056 100 0 1 ONAM:No
p 352 1088 100 0 1 ZNAM:Yes
use embbis 352 1776 100 0 view_dataDest
xform 0 400 1712
p 336 1632 100 0 1 ONST:FITS
p 448 1632 100 0 1 TWST:NONE
p 224 1632 100 0 1 ZRST:DHS
use elongins -160 608 100 0 dataViewID
xform 0 -96 528
use cadPlus 1952 272 100 0 dataViewP
xform 0 2032 176
p 1952 64 100 3072 1 set1:cad dataViewP
use ecars 288 592 150 0 dataViewC
xform 0 368 432
p 176 240 100 0 1 def(FLNK):$(top)combCars13.PROC
use inhier -512 1431 100 0 DIR
xform 0 -496 1472
use inhier -544 1335 100 0 CLID
xform 0 -528 1376
use ecad20 1344 1840 150 0 dataView
xform 0 1424 960
p 1360 1728 100 0 0 CTYP:9
p 1360 1536 100 0 1 FTVA:STRING
p 1360 1504 100 0 1 FTVB:LONG
p 1360 1472 100 0 1 FTVC:STRING
p 1360 1440 100 0 1 FTVD:LONG
p 1360 1408 100 0 1 FTVE:DOUBLE
p 1360 1376 100 0 1 FTVF:DOUBLE
p 1360 1344 100 0 1 FTVG:LONG
p 1360 1312 100 0 1 FTVH:DOUBLE
p 1360 1280 100 0 1 FTVI:DOUBLE
p 1360 1248 100 0 0 FTVJ:LONG
p 1360 1216 100 0 0 FTVK:STRING
p 1360 1184 100 0 0 FTVL:DOUBLE
p 1360 1152 100 0 0 FTVM:DOUBLE
p 1392 848 100 0 0 INAM:dcCADdataInit
p 1360 60 100 0 1 SNAM:dcCADdata
use eapply -368 1408 150 0 dataViewApply
xform 0 -240 1056
use outhier 80 1495 100 0 VAL
xform 0 96 1536
use outhier 80 1415 100 0 MESS
xform 0 96 1456
use eborderC -848 -313 100 0 eborderC#87
xform 0 832 992
p 1728 -160 100 768 -1 author:Peter Young
p 1728 -20 40 0 -1 comment1:2001-02-02 Modified for GNIRS. Peter Young
p 1728 -192 100 768 -1 date:2000-11-02
p 1952 -112 200 768 -1 file:dcDataView.sch
p 2224 -160 100 0 -1 page:1
p 2336 -160 100 0 -1 pages:1
p 2000 -160 100 0 -1 revision:0
p 1952 -48 150 768 -1 system:Gemini GNIRS DC
[comments]
