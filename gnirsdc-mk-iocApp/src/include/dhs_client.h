/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2004 by RSAA Computing Section 
 * $Id: dhs_client.h,v 1.4 2006/07/12 19:56:05 gemvx Exp $
 * GEMINI PROJECT
 * 
 * FILENAME 
 *   dhs_client.h
 * 
 * PURPOSE 
 *
 * Class that interacts with Gemini's DHS. An object of this class is
 * setup as a client of the DHS. This may be deone from either a Solaris or
 * vxWorks platform.
 *
 * HISTORY
 *   April 2003 PJY Created for RSAA's Gemini instruments GNIRS & GSAOI
 */
#ifndef _DHS_CLIENT_H
#define _DHS_CLIENT_H

// Include files 
#include "dhs.h"
#include "configuration.h"
#include "camera.h"
#include "ipc.h"
#include "utility.h"

// Constants
const int DHS_CLIENT_MAX_CONTRIB = 1;
const int DHS_CLIENT_MAX_QLS = 4;
const int DHS_MAX_FRAMES = 4;

// defines 

// Class definitions
/*
CLASS
    DHS
    DHS Client class. 
DESCRIPTION
    Handles connections and communications with Gemini DHS servers. This class provides a simple
    interface that encapsulates DHS access.
KEYWORDS
    DHS
*/

class DHS_Client {
private:
  // GROUP:   Private member variables
  DHS_THREAD dhs_thread_id;                       //+ DHS thread ID. 
  bool event_loop;                                //+ set DHS_TRUE if eventLoop is started
  DHS_CB_FN_PTR	error_cb;                         //+ the Error callback function to use
  
  DHS_CB_FN_PTR ccbf;							  //+ the dhs callback function to use
  
  DHS_CB_FN_PTR	connect_cb;                       //+ the Connection callback function to use
  DHS_BD_DATASET data_set;                        //+ Id of the dataset
  bool data_set_ok;                               //+ set true when the data set is setup ok
  DHS_BD_LIFETIME lifetime;                       //+ Time to keep the data
  char *dhs_contrib[DHS_CLIENT_MAX_CONTRIB];      //+ List of contributors to the data
  char *dhs_qlStreams[DHS_CLIENT_MAX_QLS];        //+ List of streams to send data to
  DHS_BD_FRAME frame_data[DHS_MAX_FRAMES];        //+ data buffers for each output frame required
  Nametype instrument_name;                       //+ All connections are associated with an instrument
  Detector_Image_Main_Header main_header;         //+ Header describing the image to transfer
  char frames_mem[FRAMES_MEM_SIZE];               //+ Memory holding data structures for readout frames
  int nframes;                                    //+ Number of frames in the data_set
  Nametype attr_name;                             //+ Name of latest attribut being considered in dataset  

  // GROUP:   Private member functions

  //+ Sets up control information for a DHS dataset
  // > ltime - time to keep the dataset
  // > contrib - name of data contributor
  // > dlabel - data label provided (could be empty - in which case we fetch one from DHS)
  // > nqls - number of quick look displays
  // > qls - name of each qls
  void setup_control_information(DHS_BD_LIFETIME ltime, const char* contrib, const char* dlabel, 
				 int nqls, char** qls);

  //+ Callback for DHS to use on error
  static void error_callback(DHS_CONNECT, DHS_STATUS, DHS_ERR_LEVEL, char *, DHS_TAG, void *);

  //+ Callback for DHS to use on get calls
  static void get_callback(DHS_CONNECT, DHS_TAG, char *, DHS_BD_GET_TYPE, DHS_CMD_STATUS,
			   char *, DHS_AV_LIST, void *, unsigned long, void *);
  static void DHS_Client::get_buffer(void *data, unsigned long length);

  //+ Appends idx to base_attr_name 
  // > base_attr_name - first part of attribute name
  // > idx - name index to append to base part
  // > mm - mosaic mode used for this frame
  char* get_attr_name(const char* base_attr_name, int idx, Mosaic_Mode mm);

 protected:
  // GROUP:   Protected member variables
    
public:
  
  // GROUP:   Public member variables
  DHS_CONNECT dhs_connection;                     //+ DHS connection pointer
  DHS_STATUS dhs_result;                          //+ DHS library status value
  char connection_name[IPC_HOSTLEN];              //+ Name of connection to server - must be in DHS authorised list
  char dhs_data_server_name[IPC_HOSTLEN];         //+ Name of DHS data server - not hostname
  char data_label[DHS_DATA_LABEL_LEN];            //+ Unique name returned by DHS for dataset
  char server_ip[IPC_HOSTLEN];                    //+ IP number (or hostname) of DHS server host end of connection
  void *pixel_data[DHS_MAX_FRAMES];               //+ Array of data values for each frame
  Camera_Desc * camera;                           //+ Description of the camera being used to take DHS images

  int msgs_received;                              //+ counter of received messages
  int msgs_sent;                                  //+ counter of sent messages
  DHS_CON_STATE connection_state;                 //+ State of the connection - set by callback function
  bool connecting;                                //+ Set true when trying to connect
  bool disconnecting;                             //+ Set true when trying to disconnect from DHS  		  

  //+ Simple constructor
  DHS_Client() { };
  
  //+ Normal constructor to use to connect to the DHS server
  // > cname - name of DHS connection
  // > dsname - name of DHS data server
  // > ip - server ip number
  DHS_Client(const char* cname, const char* dsname, const char* ip, Camera_Desc *cam = NULL, DHS_CB_FN_PTR ecb = NULL,
	     DHS_CB_FN_PTR ccb = NULL);

  //+ Destructor - kills connection to DHS server
  virtual ~DHS_Client();   

  // GROUP:   Public member functions

  //+ Checks DHS status value and reports if error
  // > label - text to prefix message with
  // > fname - name of file calling this method
  // > lineno - linue number of call  
  void check_dhs(const char* label, const char* fname, int lineno);
  void check_dhs(ostringstream& label, const char* fname, int lineno);
  
  //+ Dhs connect/disconnect
  void dhs_connect();
  void dhs_disconnect();
  
  //+ Sets up a new DHS dataset
  // > inst - name of instrument in use
  // > ltime - time to keep the dataset
  // > contrib - name of data contributor
  // > nqls - number of quick look displays
  // > qls - name of each qls
  // > main_header - description of image to be sent to DHS
  // > shared_frames_mem - frames memory location
  void setup_hdr_dataset(const char* inst, DHS_BD_LIFETIME ltime, const char* contrib, const char* dlabel, 
			 int nqls, char** qls, Detector_Image_Main_Header& mh, char* shared_frames_mem);

  void setup_hdr_datasetOLD(const char* inst, DHS_BD_LIFETIME ltime, const char* contrib, const char* dlabel, 
			 int nqls, char** qls, Detector_Image_Main_Header& mh, char* shared_frames_mem);			 
  
  //+ Sends that data_set to the DHS
  //+ > last - set to DHS_TRUE when this is the last dataset for this exposure
  void send_dataset(DHS_BOOLEAN last);

  //+ Sets up the data frames for this DHS dataset 
  // > sub_header - description of image chunk to be sent to DHS
  void setup_data_chunk(Detector_Image_Sub_Header *sub_header);

  //+ Sets up a final dataset for closing of a DHS transfer
  void setup_end_dataset();

  //+ Sets up a simple 1 frame dataset for DHS transfer
  // > ltime - time to keep the dataset
  // > inst - name of instrument in use
  // > contrib - name of data contributor
  // > nqls - number of quick look displays
  // > qls - name of each qls
  // > width - width of dataset frame
  // > height - height of dataset frame
  // > bitpix - bitpix of data to go into frame
  void setup_simple_dataset(const char* inst, DHS_BD_LIFETIME ltime, const char* contrib, const char* dlabel, 
			    int nqls, char** qls, unsigned long width, unsigned long height, int bitpix);

  // Retrieves a named data_set from the DHS
  // > ds_name -  name of the dataset to retrieve
  // > gcb - function pointer to be called by the get callback - takes 2 pars (void*, unsigned long)
  void retrieve_dataset(char* ds_name, DHS_CB_FN_PTR gcb = NULL);

  //+ Adds an attribute to the dataset
  // > avl - name of AV list to be extended
  // > name - name of attribute
  // > dt - DHS data type of attribute
  // > ndims - number of attribute dimensions
  // > dims - array of dimension sizes
  // > value - value of attribute
  template <typename T> void add_attribute(DHS_AV_LIST avl, const char* name, DHS_DATA_TYPE dt, int ndims, unsigned long *dims, 
					   T value);
  template <typename T> void add_attribute(DHS_AV_LIST avl, const char* name, DHS_DATA_TYPE dt, int ndims, unsigned long *dims, 
					   T* value);

};

/*+--------------------------------------------------------------------------
 * FUNCTION NAME: add_attribute
 * 
 * INVOCATION: add_attribute(DHS_AV_LIST avl, const char* name, DHS_DATA_TYPE dt, int ndims, unsigned long *dims, void* value)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > avl - name of AV list to be extended
 * > name - name of attribute
 * > dt - DHS data type of attribute
 * > ndims - number of attribute dimensions
 * > dims - array of dimension sizes
 * > value - value of attribute
 * 
 * FUNCTION VALUE: None
 * 
 * PURPOSE: Adds an attribute to the dataset
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
template <typename T> void DHS_Client::add_attribute(DHS_AV_LIST avl, const char* name, DHS_DATA_TYPE dt, int ndims, 
						     unsigned long *dims, T value)
{
  ostringstream ostr;
  if (data_set != 0) {
    dhsBdAttribAdd(avl, name, dt, ndims, dims, value, &dhs_result);
    ostr << "dhsBdAttribAdd:" << name << ends;
    check_dhs(ostr, __FILE__, __LINE__);
  }
}
template <typename T> void DHS_Client::add_attribute(DHS_AV_LIST avl, const char* name, DHS_DATA_TYPE dt, int ndims, 
						     unsigned long *dims, T* value)
{
  ostringstream ostr;
  if (data_set != 0) {
    dhsBdAttribAdd(avl, name, dt, ndims, dims, value, &dhs_result);
    ostr << "dhsBdAttribAdd:" << name << ends;
    check_dhs(ostr, __FILE__, __LINE__);
  }
}
#endif // DHS_CLIENT_H
