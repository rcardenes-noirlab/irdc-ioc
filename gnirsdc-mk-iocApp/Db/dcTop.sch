[schematic2]
uniq 1
[tools]
[detail]
[cell use]
use bb200tr -496 -144 100 0 bb200tr#3
xform 0 784 656
use dcSet 440 344 100 0 dcSet#2
xform 0 776 680
p 442 319 100 0 1 set0:realtop $(top)
p 442 300 100 0 1 set1:name GNIRS
p 443 278 100 0 1 set2:top $(top)
p 442 261 100 0 1 set3:sadtop $(top)sad:
p 444 242 100 0 1 set4:dctop $(top)dc:
p 443 220 100 0 1 set5:dcsadtop $(top)sad:dc:
p 444 201 100 0 1 set6:agtop xx:
p 444 182 100 0 1 set7:ccsadtop $(top)sad:cc:
p 442 163 100 0 1 set8:wfssadtop xxxx:
[comments]
