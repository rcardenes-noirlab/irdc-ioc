/*
 * Copyright (c) 2000-2002 by RSAA
 *
 * CICADA PROJECT
 *
 * FILENAME testParameterDB.h
 * 
 * PURPOSE
 * Definitions for access to EPICS database from simple class interface
 * This is derived from the base class ParameterDB
 *
 * REQUIREMENTS
 * Requires use of STL class "string"
 *
 */

#ifndef TEST_PARAMETER_DB_H
#define TEST_PARAMETER_DB_H

#include <string>
#include <rpc/rpc.h>
#include "parameterDB.h"

using namespace std;

// put definitions here 

/*
CLASS
    TestParameterDB
    Test parameters - these are set by Test processes
DESCRIPTION  
    Test parameters - these are set by Test processes

KEYWORDS
    ParameterDB
*/
class TestParameterDB: public ParameterDB {
 public:

  //+ Constructor
  TestParameterDB();

  //+ Destructor
  ~TestParameterDB();

  // GROUP:   Member functions
  //+ set parameter address from name
  // > dbName - name of database variable
  // > param - common name of parameter
  // > prefix - database prefix
  // > suffix - database suffix
  void* setAddr(const char* dbName, const char* param, const char* prefix, const char* suffix);

  //+ ParameterDB put methods - overloaded to support simple data types
  // > dbName - name of parameter db that has called this put
  // > param - name of parameter
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local addr - used for memcpy
  // > value - value of parameter to set
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to put - default 1
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const string* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const char* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr,
	   const unsigned char* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const short* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const unsigned short* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const int* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const unsigned int* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const long* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const unsigned long* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const float* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const double* value, unsigned int offset, unsigned int n);
  void put(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   const bool* value, unsigned int offset, unsigned int n);
  
  // ParameterDB get methods, overloaded to support multiple data types
  // > paramDBName - name of parameter db that has called this put
  // > dbName - name of parameter in backend database
  // > param - common name of parameter - may be same as dbName
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local addr - used for memcpy
  // > value - value of parameter to set
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to put - default 1
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   string* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   char* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   unsigned char* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   short* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   unsigned short* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   int* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   unsigned int* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   long* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   unsigned long* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   float* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   double* value, unsigned int offset, unsigned int n);
  void get(const char* paramDBName, const char* dbName, const char* param, const void* addr, const void* localAddr, 
	   bool* value, unsigned int offset, unsigned int n);
  
  //+ Free address in use
  // > addr - pointer to DB addr structure to free
  void freeAddr(const void* addr);

  //+ publish the named parameter
  // These methods are used to support parameter monitoring
  // > addr - pointer to DB addr structure 
  void publish(const void* addr);

  //+ Request to attach the local and database addresses
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local address
  void attach(const void* addr, const void* localAddr);

  //+ add a callback for when a monitored parameter changes
  // > addr - pointer to DB addr structure 
  // > callback - pointer ro callback function
  int add_callback(const void* addr, Parameter_Callback* callback);

  //+ remove a callback for a monitored parameter
  // > cb_id - callback identifier
  void remove_callback(int cb_id);
 
  //+ wait for a parameter to change
  // > addr - pointer to DB addr structure 
  void wait(const void* addr);

 private:
  //+ These methods do the real work of putting and getting
  // > paramDBName - name of parameter db that has called this get
  // > param - name of parameter
  // > addr - pointer to DB addr structure 
  // > localAddr - pointer to local addr - used for memcpy
  // < value - value of parameter to get
  // > offset - offset added to addr to get desired starting address - default 0
  // > n - number of elements to put - default 1
  template <class T> void put_test_param(const char* paramDBName, const char* param, const void *addr,  const void* localAddr, 
					 T* value, unsigned int offset, unsigned int n);
  template <class T> void get_test_param(const char* paramDBName, const char* param, const void *addr,  const void* localAddr, 
					 T* value, unsigned int offset, unsigned int n);
};

#endif /* TEST_PARAMETER_DB_H */
