/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  ipc_test_main.cc
 * 
 * GENERAL DESCRIPTION
 *  Main program to test IPC C++ classes.
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 1993
 *
 * HISTORY
 *   Sept 1993 PJY    Created for RSAA CICADA package
 *   Sept 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */

/* Include files */

#ifdef vxWorks
#include <vxWorks.h>
#endif
#include <time.h>
#include "ipc.h"
#include "test_ipc.h"
#include "exception.h"

/* defines */


/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/* class function definitions */

/* Main program */
#ifdef vxWorks
int ipcSlave()
#else
int main()
#endif
{
  try {
    Test_Smem* sm;
    int ready;
    struct timespec ts;

    try {
      cout << "Slave: starting..." << endl;

      // Connect to mailbox for passing messages
      Mailbox mbox1("mbox1",CONNECT,S_IRUSR|S_IWUSR);
      Mailbox mbox2("mbox2",CONNECT,S_IRUSR|S_IWUSR);

      //connect shared mem
      Shared_Mem smem("smem1",CONNECT,sizeof(Test_Smem),S_IRUSR|S_IWUSR);
      sm = (Test_Smem *) smem.address;


      //Establish a semaphore for controlling shared memory
      Semaphore sem1("sem1",CONNECT,RWALL);
      Semaphore sem2("sem2",ESTABLISH,RWALL);
      
      cout << "Slave: connected to mailboxes and semaphores, waiting for sem1..." << endl;

      /* Do some communicating */
      
      sem1.wait(); // Wait for semaphore to reach 1

      cout << "Slave: a=" << sm->a << " b=" << sm->b << endl;
      sm->a = 0;
      sm->b = 0.0;
      sem2.set(); // Set semaphore to 1 indicating finished

      cout << "Slave: sem2 set" << endl;
      cout << "Slave: sleeping 2s then setting sem2 again" << endl;
      ts.tv_sec = 2;
      ts.tv_nsec = 0;
      nanosleep(&ts,NULL);
      sem2.set();
      
      cout << "Slave: waiting on mailbox message ..." << endl;

      mbox1.get_message((char *) &ready, sizeof(ready), 0);

      cout << "Slave: got mailbox message, ready = " << ready << endl;
      ready=TRUE;
      
      mbox2.send_message((char *) &ready, sizeof(ready));


      sem2.wait(); // Wait for semaphore to reach 1
      sem1.set(); // Set semaphore to 0 indicating finished

      // Now wait until sem1 is cleared by master
      cout << "Slave: waiting for sem1 to clear" << endl;
      ts.tv_sec = 0;
      ts.tv_nsec = 10000000;
      while (sem1.get()) 
	nanosleep(&ts,NULL);
      cout << "Slave: waiting for sem2 to clear" << endl;
      while (sem2.get()) 
	nanosleep(&ts,NULL);

      cout << "Slave: sleeping 2s" << endl;
      ts.tv_sec = 2;
      ts.tv_nsec = 0;
      nanosleep(&ts,NULL);

      cout << "Slave: exiting" << endl;
    }
    catch (IPC_Error& e) {
      cout << "Slave Error: " << endl;
      e.print_error(__FILE__, __LINE__);
    }
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return(0);
}
