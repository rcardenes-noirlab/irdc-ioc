/* Copyright (c) 1994 by MSSSO Computing Section 
 *
 * Test IPC data structures 
 */

#ifndef _TESTIPC_H
#define _TESTIPC_H

/* Include files */

/* defines */

/* typedefs */
struct Test_Smem {
 int a;
 double b;
};

/* class declarations */

/* Global function declarations */

#ifdef  __cplusplus
extern "C" int ipcSlave();
#endif

#endif  /* _TESTIPC_H */
