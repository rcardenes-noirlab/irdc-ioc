/*
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME detector_regions_test.cc
 * 
 * GENERAL DESCRIPTION
 * Test routine for Cicada_Regions class.
 *
 */

#include <iostream>
#include "detector_regions.h"
#include "utility.h"
#include "exception.h"
#include "config_table.h"

void show_structs(Detector_Regions* rgn);

#ifdef vxWorks
int detectorRegionsTest()
#endif
{
  try {
    Camera_Desc camera;
    Controller_Desc controller;
    int i,j,k,f;
    unsigned long long m;
    string name;
    unsigned short n;
    unsigned long xo[4];
    unsigned long yo[4];
    unsigned long width[4];
    unsigned long height[4];
    unsigned long osrows,oscols;
    unsigned short nf;
    unsigned short rcf=1,ccf=1;
    Detector_Regions *rgn;
    Controller_Table ctable;
    Filenametype fname;
    Mosaic_Mode mm;
    bool append_os;
    string choice;
    Regions_Frame_Map::iterator rgn_iter;

    // read in the controller configuration table
    try {
#ifdef vxWorks
      strlcpy(fname,CONTROLLER_TABLE,FILENAMELEN);
#endif
      ctable.read(fname);
    }  
    catch(Error &e ) {
      e.print_error(__FILE__,__LINE__);
    }

    // Now select the controller configuration to use
    cout << "#   Controller Name" << endl;
    for (i=0; i<ctable.n_items; i++) {
      cout << ctable[i].name << "\tnum_chips= " <<ctable[i].n_chips;
      cout << " size=" << ctable[i].chip[0].cols << "x" << ctable[i].chip[0].rows;
      cout << " n_amps=" << ctable[i].chip[0].n_amps;
      
      for (j=0; j<ctable[i].chip[0].n_amps; j++) {
	cout << " A" << j << "=" << ctable[i].chip[0].amp[j].width << "x" << ctable[i].chip[0].amp[j].height <<
	  "+" << ctable[i].chip[0].amp[j].xo << "+" << ctable[i].chip[0].amp[j].yo;
      }
      cout << endl;
    }

#ifdef vxWorks
    name = "GNIRS";
#endif

    controller = ctable[name.c_str()];

    cout << "\nUsing " << name << ". Size= " << controller.cols << "x" << controller.rows << endl;
    
    camera.controller[0] = controller;
    camera.cont_xo[0] = 0;
    camera.cont_yo[0] = 0;

    try {

      // constructor with region info
#ifdef vxWorks
      n = 1;
      xo[0] = yo[0] = oscols = osrows = 0;
      width[0] = height[0] = 1024;
      mm = MOSAIC_NONE;
#endif

      cout << "\n\nTesting complex constructor:" << endl;

      rgn = new Detector_Regions (camera, 0, true, mm, append_os, n, xo, width, yo, height, rcf, ccf, osrows, oscols);
      cout << "\n\nTesting print_amp_specs():" << endl;
      rgn->print_amp_specs();
      cout << "\n\nTesting show_structs():" << endl;
      show_structs(rgn);
      cout << "\nTesting frame dimensions:" << endl;

      nf = rgn->frames.size();
      rgn_iter = rgn->frames.begin();
      while (rgn_iter != rgn->frames.end()) {
	f = (*rgn_iter).first;

	cout << "for frame " << f << " nsframes is " << rgn->frames[f].subframes.size() << endl;
	for (i=0; i<(int)rgn->frames[f].subframes.size(); i++) {
	  cout << " subframe " << i << ": xo,yo,width,height= " <<
	    rgn->frames[f].subframes[i].frame_xo << ", " <<
	    rgn->frames[f].subframes[i].frame_yo << "  " <<
	    rgn->frames[f].subframes[i].width << ", " << 
	    rgn->frames[f].subframes[i].height << endl;
	}
	rgn_iter++;
      }
      
      cout << "\n Testing get_frame_mask(amp,par,ser)..." << endl;
      for (i=0; i<rgn->namps; i++) {  // amps
	for (j=0; j<rgn->npar_regions; j++) {  // pars
	  for (k=0; k<rgn->par_regions[j].nser_regions; k++) {  // sers
	    m = rgn->get_frame_mask(100+i,j,k); // add 100 for debugging
	    if (m!=0)
	      cout << "Frame mask for " << i << "," << j << "," << k << " is " << m << endl;	  
	  }
	}
      }

      delete rgn;
    }
    catch(Error &e ){
      e.print_error(__FILE__,__LINE__);
    }
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return 0;
}
/*+--------------------------------------------------------------------------
 * FUNCTION NAME: show_structs
 * 
 * INVOCATION: show_structs(Detector_Regions* rgn)
 * 
 * PARAMETERS: (">" input, "!" modified, "<" output) 
 * > rgn - region readout description
 * 
 * FUNCTION VALUE: 
 * 
 * PURPOSE: Routine to print out region structures
 * 
 * DESCRIPTION: 
 * 
 * EXTERNAL VARIABLES: None.
 * 
 * PRIOR REQUIREMENTS: None.
 * 
 * DEFICIENCIES: 
 *
 *-------------------------------------------------------------------------*/
void show_structs(Detector_Regions* rgn)
{
  int i=0,j=0;

  for (i=0; i<rgn->npar_regions; i++) {
    cout << "For par region " << i << ":" << endl;
    cout << "yo= " << rgn->par_regions[i].yo <<endl;
    cout << "skip= " << rgn->par_regions[i].skip << endl;
    cout << "height= " << rgn->par_regions[i].height << endl;
    cout << "width= " << rgn->par_regions[i].width << endl;
    cout << "remain= " << rgn->par_regions[i].remain << endl;
    cout << "amp_mask= " << rgn->par_regions[i].amp_mask << endl;
    cout << "region_mask= " << rgn->par_regions[i].region_mask << endl;
    cout << "Nserial for this par= " << rgn->par_regions[i].nser_regions << endl;
    for (j=0;j<rgn->par_regions[i].nser_regions; j++) {
      cout << "\t" << j << " " << "ser start= " << rgn->par_regions[i].ser_regions[j].xo << endl;
      cout << "\t" << j << " " << "ser skip = " << rgn->par_regions[i].ser_regions[j].skip << endl;
      cout << "\t" << j << " " << "ser width= " << rgn->par_regions[i].ser_regions[j].width << endl;
    }
    cout <<endl;
  }
  cout << "totalpix= " << rgn->total_readout_pixels() << endl;
  cout << "totalbinnedpix= " << rgn->total_binned_readout_pixels() << endl;
  cout << "number of subframes= " << rgn->nsframes << endl;
}
