[schematic2]
uniq 517
[tools]
[detail]
w 1826 1115 100 0 n#516 ebos.sdsuInitBo.OUT 1776 1104 1936 1104 hwout.hwout#515.outp
w 282 1835 100 0 n#513 dcTestPattern.dcTestPattern#509.MESS 896 1312 1120 1312 1120 1824 -496 1824 -496 1072 -192 1072 eapply.controlSDSUApply.INMB
w 282 1803 100 0 n#512 dcTestPattern.dcTestPattern#509.VAL 896 1344 1088 1344 1088 1792 -464 1792 -464 1104 -192 1104 eapply.controlSDSUApply.INPB
w 314 1083 100 0 n#511 eapply.controlSDSUApply.OCLB 192 1072 496 1072 496 1312 672 1312 dcTestPattern.dcTestPattern#509.CLID
w 298 1115 100 0 n#510 eapply.controlSDSUApply.OUTB 192 1104 464 1104 464 1344 672 1344 dcTestPattern.dcTestPattern#509.DIR
w 1762 587 100 0 n#497 elongins.DSPDirectCmdID.VAL 1712 576 1872 576 ecars.DSPDirectCmdC.ICID
w 1802 1579 100 0 n#494 elongins.setupSDSUID.VAL 1776 1568 1888 1568 ecars.setupSDSUC.ICID
w 322 235 100 0 n#492 dcSDSUUtils.dcSDSUUtils#486.MESS 896 1040 1120 1040 1120 224 -416 224 -416 1008 -192 1008 eapply.controlSDSUApply.INMC
w 330 203 100 0 n#491 dcSDSUUtils.dcSDSUUtils#486.VAL 896 1072 1168 1072 1168 192 -448 192 -448 1040 -192 1040 eapply.controlSDSUApply.INPC
w 354 1019 100 0 n#490 eapply.controlSDSUApply.OCLC 192 1008 576 1008 576 1040 672 1040 dcSDSUUtils.dcSDSUUtils#486.CLID
w 322 1051 100 0 n#489 eapply.controlSDSUApply.OUTC 192 1040 512 1040 512 1072 672 1072 dcSDSUUtils.dcSDSUUtils#486.DIR
w 314 955 100 0 n#484 dcSDSUDSPBoard.dcSDSUDSPBoard#488.CLID 672 752 496 752 496 944 192 944 eapply.controlSDSUApply.OCLD
w 338 987 100 0 n#483 eapply.controlSDSUApply.OUTD 192 976 544 976 544 784 672 784 dcSDSUDSPBoard.dcSDSUDSPBoard#488.DIR
w 314 267 100 0 n#481 dcSDSUDSPBoard.dcSDSUDSPBoard#488.VAL 896 784 1072 784 1072 256 -384 256 -384 976 -192 976 eapply.controlSDSUApply.INPD
w 314 299 100 0 n#482 dcSDSUDSPBoard.dcSDSUDSPBoard#488.MESS 896 752 1040 752 1040 288 -352 288 -352 944 -192 944 eapply.controlSDSUApply.INMD
w 274 1771 100 0 n#472 dcSDSUSetup.dcSDSUSetup#478.MESS 896 1600 1040 1600 1040 1760 -432 1760 -432 1136 -192 1136 eapply.controlSDSUApply.INMA
w 274 1739 100 0 n#471 dcSDSUSetup.dcSDSUSetup#478.VAL 896 1632 1008 1632 1008 1728 -400 1728 -400 1168 -192 1168 eapply.controlSDSUApply.INPA
w 514 1611 100 0 n#466 dcSDSUSetup.dcSDSUSetup#478.CLID 672 1600 416 1600 416 1136 192 1136 eapply.controlSDSUApply.OCLA
w 498 1643 100 0 n#465 eapply.controlSDSUApply.OUTA 192 1168 384 1168 384 1632 672 1632 dcSDSUSetup.dcSDSUSetup#478.DIR
w -254 1243 -100 0 CLID inhier.CLID.P -304 1312 -256 1312 -256 1232 -192 1232 eapply.controlSDSUApply.CLID
w -270 1419 100 0 n#347 inhier.DIR.P -256 1408 -224 1408 -224 1264 -192 1264 eapply.controlSDSUApply.DIR
w 194 1243 100 0 n#117 eapply.controlSDSUApply.MESS 192 1232 256 1232 256 1392 320 1392 outhier.MESS.p
w 242 1483 100 0 n#115 eapply.controlSDSUApply.VAL 192 1264 224 1264 224 1472 320 1472 outhier.VAL.p
s 1664 752 100 0 SDSU DSP commands use the SDSU DSP CAR
s 1600 1712 100 0 SDSU setup and SDSU utils use the same setupSDSU CAR
s -576 -176 200 0 if the inisSDSU CAD is marked. SNL code monitors the initSDSUP record.
s -576 16 200 0 SDSU controller setup parameters Apply/CAD records. Up to 20 controller parameters
s -576 -48 200 0 can be set here as well as handling power on/off and setting video mode.
s -576 -112 200 0 These are transferred to the gnirs_dc_par class object. Hardware init is done
[cell use]
use hwout 1936 1063 100 0 hwout#515
xform 0 2032 1104
p 2032 1095 100 0 -1 val(outp):$(sadtop)initSDSUDone.VAL PP NMS
use ebos 1584 1216 100 0 sdsuInitBo
xform 0 1648 1136
p 1552 992 100 0 1 ONAM:Set Init Not Done
p 1552 1024 100 0 1 ZNAM:Set Init Done
use dcTestPattern 672 1095 100 0 dcTestPattern#509
xform 0 784 1248
use elongins 1520 1511 100 0 setupSDSUID
xform 0 1648 1584
use elongins 1472 512 100 0 DSPDirectCmdID
xform 0 1584 592
use ecars 1952 1648 100 0 setupSDSUC
xform 0 2048 1488
p 1920 1312 100 0 1 def(FLNK):$(top)combCars11.PROC
use ecars 1936 656 100 0 DSPDirectCmdC
xform 0 2032 496
p 1920 304 100 0 1 def(FLNK):$(top)combCars22.PROC
use dcSDSUDSPBoard 672 535 100 0 dcSDSUDSPBoard#488
xform 0 784 688
use dcSDSUUtils 672 823 100 0 dcSDSUUtils#486
xform 0 784 976
use dcSDSUSetup 672 1383 100 0 dcSDSUSetup#478
xform 0 784 1536
use inhier -272 1367 100 0 DIR
xform 0 -256 1408
use inhier -320 1271 100 0 CLID
xform 0 -304 1312
use eapply -128 1344 150 0 controlSDSUApply
xform 0 0 992
use outhier 288 1351 100 0 MESS
xform 0 304 1392
use outhier 288 1431 100 0 VAL
xform 0 304 1472
use eborderC -784 -313 100 0 eborderC#87
xform 0 896 992
p 1792 -160 100 768 -1 author:Peter Young
p 1792 -20 40 0 -1 comment1:1999-01-11 GMOS version. Janet Tvedt
p 1792 -36 40 0 -1 comment2:2000-11-02 Modified for GNIRS. Peter Young
p 1792 -192 100 768 -1 date:2000-11-02
p 2016 -112 200 768 -1 file:dcSDSUControl
p 2288 -160 100 0 -1 page:1
p 2400 -160 100 0 -1 pages:1
p 2064 -160 100 0 -1 revision:0
p 2016 -48 150 768 -1 system:Gemini GNIRS DC
[comments]
