/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2004 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *   exception.h
 * 
 * PURPOSE
 * Exception data structures and interface  
 *
 * HISTORY
 *
 */
#ifndef _EXCEPTION_H
#define _EXCEPTION_H

// Include files 
#ifdef vxWorks
#include <vxWorks.h>
#endif
#include <sys/stat.h>
#include <string>
#include <stdexcept>
#include "utility.h"

using namespace std;

//+ defines and constants

// typedefs 
//+ Set of Error types
enum Error_Type {
  E_OK,
  E_INFO,
  E_WARNING,
  E_ERROR,
  E_FATAL
};

//+ A function to return a string describing the given error type
// > e - the error type to describe
const string strerrtype(Error_Type e);

//+ A function to log an exception
void log_exception(std::exception& e, const char* f, const int l);


/*
CLASS
    Loggable_Exception
DESCRIPTION  
    An exception that can be used for loggable error conditions.
KEYWORDS
    Exception
*/
class Loggable_Exception: public std::exception {

  public:
    //+ Constructor
    // > m - message to log
    Loggable_Exception(string m);

    //+ Constructor
    // > e - exception to log
    // > f - file where the error was caught
    // > l - line number where the error was caught
    Loggable_Exception(std::exception& e, const char* f, int l);

    //+ Constructor
    // > m - message to log
    // > f - file where the error was caught
    // > l - line number where the error was caught
    Loggable_Exception(string m, const char* f, int l);

    //+ Constructor
    // > o - message to log
    // > f - file where the error was caught
    // > l - line number where the error was caught
    Loggable_Exception(ostringstream& o, const char* f, int l);

    //+ Destructor
    virtual ~Loggable_Exception() throw() { };
  
    //+ Return a description of the error
    virtual const char* what() const throw();

    //+ rethrow if can't handle here
    virtual void rethrow(); 

  private:
    //+ The string that what() returns
    const string what_str;

};

/*
CLASS
    Error
    Used for exception handling.  
DESCRIPTION  
    Used for exception handling. Allows exceptions to be thrown with a message
    string, exception type, exception code and source file and line number. 
KEYWORDS
    Exception
*/
class Error: public Loggable_Exception {
 public:
  //+ Constructor
  // > m - message string
  Error(const char* m);

  //+ Constructor
  // > e  - exception to log
  // > f  - source filename
  // > l  - source line no
  Error(Error& e,const char* f,int l);

  //+ Constructor
  // > e  - exception to log
  // > t - error type
  // > f  - source filename
  // > l  - source line no
  Error(std::exception& e,Error_Type t,const char* f,int l);

  //+ Constructor
  // > m  - message string
  // > e  - error type
  // > eno  - error code
  // > f  - source filename
  // > l  - source line no
  Error(const char* m,Error_Type e,int eno,const char* f,int l);

  //+ Constructor
  // > m  - message string
  // > e  - error type
  // > f  - source filename
  // > l  - source line no
  Error(const char* m,Error_Type e,const char* f,int l);

  //+ Constructor
  // > o  - message stream
  // > e  - error type
  // > eno  - error code
  // > f  - source filename
  // > l  - source line no
  Error(ostringstream& o,Error_Type e,int eno,const char* f,int l);

  //+ Destructor
  virtual ~Error() throw() {};

  // GROUP:   Member functions
  //+ Method for recording error in message buffer
  // > f  - source filename
  // > l  - source line no
  virtual char* record_error(const char* f, int l);

  //+ Method for printing an error message to stdout
  // > f  - source filename
  // > l  - source line no
  virtual void print_error(const char* f, int l);

 //+ The message
  const string msg;

  //+ The error number
  const int errnum;

  //+ The error type
  const Error_Type type;
};

/*
CLASS
    IPC_Error
    Used for IPC class exception handling.  
DESCRIPTION  
    Used for IPC class exception handling. Allows exceptions to be thrown with a message
    string, exception type, exception code and source file and line number.  
KEYWORDS
    IPC Exception
*/
class IPC_Error: public Error {
 public:
  //+ Constructor
  // > m  - message string
  // > e  - error type
  // > eno  - error code
  // > f  - source filename
  // > l  - source line no
  IPC_Error(const char* m, Error_Type e, int eno, const char* f, int l);

  //+ Destructor
  virtual ~IPC_Error() throw();

};

/*
CLASS
    Timeout_Error
DESCRIPTION  
        Indicates that a timeout occured
KEYWORDS
    Exception
*/
class Timeout_Error: public Loggable_Exception {

 public:
  //+ Constructors
  // > m - message to log
  Timeout_Error(const char *m) : Loggable_Exception(m) {};
  // > o - message ostringstream to log
  Timeout_Error(ostringstream& o, const char* f, int l) : Loggable_Exception(o,f,l) {};

  //+ Destructor
  virtual ~Timeout_Error() throw();

};

/*
CLASS
    Null_Pointer_Error
DESCRIPTION  
    An exception that can be used when a null pointer was encountered but
    not expected.
KEYWORDS
    Exception
*/
class Null_Pointer_Error: public Loggable_Exception {
  public:
  //+ Constructor
  // > f  - source filename
  // > l  - source line no
  Null_Pointer_Error(const char* f, const int l) : Loggable_Exception("Null pointer",f,l) {};
  
  //+ Destructor
  virtual ~Null_Pointer_Error() throw();

};

/*
CLASS
    Mutex_Error
DESCRIPTION  
    An exception that can be used when a mutex related error occurs.
KEYWORDS
    Exception
*/
class Mutex_Error: public Loggable_Exception {
  public:
    //+ Constructor
    // > f  - source filename
    // > l  - source line no
    Mutex_Error(const char* f, const int l) : Loggable_Exception("Mutex error",f,l) {};

    //+ Destructor
    virtual ~Mutex_Error() throw();
};
#endif //_EXCEPTION_H
