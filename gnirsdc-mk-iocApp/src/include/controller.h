/*
 * Copyright (c) 1994-2002 by RSAA Computing Section
 *
 * CICADA PROJECT
 *
 * FILENAME controller.h
 *
 * PURPOSE
 * Defines generic operations for a detector controller This class is used to
 * interface to a generic astronomy detector controller.  Classes derived off
 * this are used to interface directly to specific types of detector controller
 * and therefore contain all specific knowledge of that particular controller.
 * This class will be used by a Camera object to interface generically to a
 * Controller to control detector setup and operation. It has members for
 * buffering detector data after readout before that data is transferred out of
 * the system.
 *
 */
#ifndef _CONTROLLER_H
#define _CONTROLLER_H

// Include files
#include "detector_regions.h"
#include "parameter.h"
#include "platform_specific.h"
#include "ipc.h"
#include "logging.h"
#include "clock.h"

/*---------------------------------------------------------------------------
 Constants
 --------------------------------------------------------------------------*/

// Defaults for Camera_State structure

// Defaults for verifying test patterns
const int DEFAULT_DO_TEST_PATTERN = FALSE;
const int DEFAULT_TEST_PATTERN_CONSTANT	= FALSE;
const int DEFAULT_TEST_PATTERN_SEED = 1000;
const int DEFAULT_TEST_PATTERN_INCREMENT = 1;
const int DEFAULT_TEST_PATTERN_OVERSCAN = 10;

// Default time (msec) to wait for an image segment to be readout
const int IMAGE_WAIT = 100;
// TODO: We're setting 60 seconds, but this is actually variable
const int IMAGE_WAIT_TIMEOUT = 60000;

// Time conversion constants
const double BILLION = 1.0E9;  // For converting nanosecs to decimal seconds
const double THOUSAND = 1.0E3; // For converting msecs to decimal seconds
const double DMILLION = 1.0E6; // For converting usecs to decimal seconds
const long MILLION = 1000000;  // For converting usecs to seconds with truncation

// Database name constants
static const char CONTROLLER_TYPE[] = "controller_type";
static const char CONTROLLER_TYPE_DB[] = "";

static const char EXPOSURE[] = "exposure";
static const char TEMPERATURE[] = "temperature";
static const char ADU[] = "adu";

/*--------------------------------------------------------------------------
 Typedefs
 -------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------
 Global variables
 -------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
 Class declarations
 -------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
CLASS
    Controller

DESCRIPTION
    Base Controller class definition - Actual controller definitions
	are derived from this.

KEYWORDS
    Controller, Hardware
----------------------------------------------------------------------------*/
class Controller {

public:

  static const std::string PARAMETER_NAME;

  //+ Basic constructor - used for setting up configuration.
  // > cam   - Camera_Desc - description of the camera
  // > camid - camera id
  // > c     - controller id
  Controller(Camera_Desc cam, int camid, int c, Platform_Specific* ps);

  //+ Midlevel constructor - used for accessing parameter information.
  // > cam   - Camera_Desc - description of the camera
  // > camid - camera id
  // > c 	  - controller id
  // > param_host - host on which the parameter database resides
  // > st	- status memory for this instrument
  // > pm	- parameter map
  Controller(Camera_Desc cam, int camid, int c, const char* param_host, Instrument_Status *st, Platform_Specific* ps);

  //+ Main constructor
  // > st  - status memory for this instrument
  // > ip  - instrument status parameter object
  // > cp  -
  // > op  -
  // > deb - debug level
  Controller(Instrument_Status *st, Parameter* ip, Parameter* cp, Parameter* op, int deb, Platform_Specific* ps);

   //+ Controller class Destructor
  virtual ~Controller();

  // GROUP: Controller class Public Member Functions.

  // These Controller methods are available with both constructors.

  //+ Return hardware configuration
  // > read_configuration -
  virtual Controller_Config* getconfig(int read_configuration) = 0;

  // GROUP: These Controller methods are available only with main constructor.


  //+ handler for the RESET signal - called by camera
  virtual void handle_reset();

  //+ clear the reset signal
  virtual void clear_reset();


  //+ Creates an image readout buffer and returns pointer to buffer.
  // > size - size in bytes of the buffer.
  virtual char* create_image_buffer(unsigned long size, int align = 0);

  // GROUP: These Controller class methods must be defined by sub-class
  // - make virtual pure here.

  //+ Do hardware initialisation - must be derived.
  // > init_rec - initialisation record.
  virtual void init(Init init_rec) = 0;

  //+ Do a controller reset - must be derived
  virtual void controller_reset() = 0;

  //+ Do a controller test - must be derived
  virtual void controller_test(int ntests=10) = 0;

  //+ Do a controller shutdown - must be derived
  virtual void controller_shutdown() = 0;

  //+ Resets detector hardware (hardware flush).
  // > reset - reset parameters.
  // > exp - reset in an exposure sequence.
  virtual void reset_detector(Detector_Reset reset, bool exp = false) = 0;

  //+ Prepare for an Exposure
  // > ns - number of controllers to sync
  virtual void prepare_exposure(int ns = 1) = 0;

  // GROUP: Start an image transfer in a thread.

  //+ Starts the image transfer thread for an IR camera - calls next function.
  // > expose - exposure request.
  // > ir_tt - pointer to thread monitoring IR exposure timing
  virtual void start_ir_image_transfer_thread(IR_Expose& expose, Thread *ir_tt) = 0;

  //+ Starts the image transfer thread for all cameras.
  virtual void start_image_transfer_thread() = 0;

  //+ Takes actions necessary before reading a row region.
  // > rr - row region id (row number of region).
  virtual void start_row_region(int rr) = 0;

  //+ Wait on thread to complete a segment readout.
  // > rows_processed - number of rows expected so far.
  // > row - row number of region.
  // > rowbytes - number of bytes per row.
  // ! rt - current count of total rows transferred in readout.
  virtual void wait_segment_readout(int rows_processed, int row, int rowbytes, int& rtfr) = 0;

  //+ Returns the number of readouts completed.
  virtual long get_readouts_done() = 0;

  //+ Returns the number of current readout.
  virtual long get_read_running() = 0;

  //+ Returns the full readout status
  virtual void get_readout_status(int rowbytes) = 0;

  //+ Stops the readouts.
  virtual void do_stop_readout() = 0;

  //+ Cleans up after a readout either finishes or is aborted.
  virtual void finish_readout() = 0;


  //+ Sets the exposure time
  // > msec - exposure time in msec to set
  virtual void set_exposure_time(int msec) {};

  //+ Gets the exposure time counter value - msec
  virtual int get_exposure_time() {return 0;};

  //+ Sends the "start exposure" command
  virtual epicsTime start_exposure() = 0;

  virtual epicsTime get_start_exposure() const { return exposure_clock.to_epicsTime(exposure_clock.get_period_start()); }
  virtual epicsTime get_end_exposure() const { return exposure_clock.to_epicsTime(exposure_clock.get_period_end()); }

  virtual double exposure_lapsed() = 0;

  //+ Perform necessary steps cleaning up after an IR exposure.
  virtual void cleanup_exposure();

  // GROUP: basic Controller setup readout method - enhanced by sub-class.

  //+ Set up the readout of a detector, if successful return true.
  // > ns - number of controllers to sync.
  // > readout_rec - readout request structure.
  // > trgn - readout regions object.
  // ! isize - total bytes in readout
  //   isize may be modified by controller specific req.
  // > exptime - last exposure time.
  virtual void setup_readout(int ns, Detector_Readout* readout_rec, Detector_Regions* trgn,
		             IR_Exposure_Info &ir_einfo, unsigned long& isize, double exptime);

  //+ Prepare the verification buffer for readout verification.
  // > rn - readout number.
  virtual void setup_verify_data(int rn);

  //+ initialise readout parameters.
  // > s - sample number.
  // > r - read number.
  virtual void init_readout(int s, int r);

  //+ finalise readout parameters.
  virtual void finalise_readout();

  //+ Write a message to the message buffer and report file
  // > format - message format
  // > ... - parameter list
  template<typename ... Args>
  void message_write(const string& format, Args const & ... args) {
	  logger::message(logger::Level::Min, format, args...);
  }

  //+ Sets reporting object and debug level
  // > deb - debugging flag
  virtual void change_debug(int deb);

  //+ Sets simulation mode - defaults to do nothing
  // > sim - simulation level
  virtual void change_simulation_mode(int sim);

  //+ Checks controllers info
  virtual void get_controller_info() {};

  virtual size_t dump_to(FILE *dest) = 0;

  // GROUP: Controller class Public member Variables.
  Camera_Desc camera;                   //+ Camera description
  Instrument_Status *status;            //+ Status information
  Instrument_Ctl_Status *ctl_status;    //+ Instrument process status info
  Controller_Desc controller;           //+ Controller description
  Filenametype controller_dir;          //+ Directory with controller config
  Camera_Status *camera_status;         //+ Full camera status
  Controller_Status *controller_status; //+ specific controller status
  int debug;                            //+ debug level
  int interface_fd;                     //+ Interface device file descriptor after open
  bool use_interface_fd;                //+ Use interface device file descriptor to map image buffer
  ParameterDB *controller_pDB;          //+ Cicada parameter DB object
  Parameter *controller_statusp;        //+ Cicada parameter status database
  Parameter* inst_statusp;              //+ Cicada inst control parameter database
  Parameter* camera_statusp;            //+ Cicada camera control parameter database
  Parameter* op_statusp;                //+ Cicada camera operations parameter database
  epicsTimeStamp hr_exptime;            //+ Total exposure time (nanoseconds).
  int n_sync;                           //+ Number of controllers to synchronise
  double last_exptime;                  //+ Most recently completed exposure time
  bool stop_readout;                    //+ Set if a readout in progress is to be stopped

  Thread *image_transfer_thr;           //+ thread used for image transfer from controller hardware
  Thread *ir_timer_thr;                 //+ thread used for IR timing - started by IR Camera object
  int rows_transfered;                  //+ Number of rows transfered in readout
  bool doing_image_transfer;            //+ Flag indicating image tranfer started
  bool finished_image_transfer;         //+ Flag indicating image tranfer finished
  bool exposure_done;                   //+ Flag to indicate that exposure processing has completed
  int nbuffers;                         //+ Number of readout buffers setup
  int transfer_buffer;                  //+ Number of readout buffer currently being filled (cumulative)
  int buffer_number;                    //+ Number of readout buffer currently being filled (cycle)
  char image_readout_thread_msg[MSGLEN];//+ Thread message buffer
  bool image_readout_thread_abort;      //+ Set if image readout thread fails
  int image_readout_errno;              //+ Image readout status
  unsigned long image_buffer_size;      //+ Size of buffer
  unsigned long actual_image_size;      //+ total bytes in requested readout
  Detector_Image_Info image_info;       //+ details of image information
  char *image_buffer_ptr;               //+ Pointer to image buffer
  char *verify_buf;                     //+ pointer to buffer to use when verifying data
  Verify_Spec verify_spec;              //+ Test data verification settings
  int controller_abort;                 //+ Abort flag

  //+ For IR controllers need to know following
  IR_Exposure_Info ir_einfo;            //+ Exposure info used for IR cameras
  long nreads;                          //+ counter used for IR camera multiple read requirements
  long nsamples;                        //+ counter used for IR camera multiple sample requirements
  long readout_no;                      //+ readout counter - read that should be going
  long readouts_done;                   //+ readout done counter
  long read_running;                    //+ actual readout currently running

protected:

  // GROUP: Controller class Protected member Functions.


  // GROUP: Controller class Protected Variables.
  Config_Data config;                   //+ Current configuration data copy
  int cid;                              //+ Controller ID (could be multi-controller)
  int initialised;                      //+ Set when initialised
  Platform_Specific* specific;             //+ platform specific data
  char *image_buffer;                   //+ temporary buffer space
  Config *current_config;               //+ Current configuration data - shared memory
  unsigned long last_size;              //+ Preserved size of image buffer (pixels)
  unsigned long last_verify_size;       //+ Preserved size of verification image buffer (pixels)
  Detector_Regions* rgn;                //+ Region spec from readout request
  Region_Data regions;                  //+ region info for this readout
  int unsigned_data;                    //+ True if raw data is unsigned
  int test_mode;                        //+ True if only testing camera without data proc
  Error_Type msg_severity;              //+ Set when indicating last message severity
  Clock exposure_clock;                 //+ Stores timing measurements taken during exposure.

  ParamMapType *controller_param_map;

  //+ Initialise some member variables for constructors.
  virtual void init_vars();

  //+ Add status parameters to the status database - called from
  //+ main constructor
  // > param_host - hostname where parameters reside
  // > init - flag set if required to initialise added parameters
  virtual void add_status_parameters(const char* param_host, bool init);

private:
};

/*
 *+
 * FUNCTION NAME:  template<class T> void generate_pixels
 *
 * INVOCATION: generate_pixels(T *dp, unsigned short& val, int& n, unsigned short inc, unsigned short ainc, unsigned short rinc,
 *                             int namps, int sw, int swb, int rcf)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * < dp - pointer to output data
 * ! val - pixel value - updated
 * ! n - pixel counter into dp
 * > inc - pixel increment
 * > ainc - amplifier increment
 * > rinc - readout increment
 * > namps - number of amps
 * > sw - width of serial region
 * > swb - binned width of serial region
 * > rcf - row compression factor
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Generates data for readout verification
 *
 * DESCRIPTION: Prepare the verification buffer using specified formula
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES: None
 *
 *-
 */
template<class T> void generate_pixels(T *dp, T& val, T maxv, int& n, unsigned short inc, unsigned short ainc,
                                      unsigned short rinc, int namps, int sw, int swb, int rcf)
{
  int c,nc,i;

  c = 0;
  // Loop through the serial row pixel by pixel
  for (nc=0; nc<swb; nc++) {

    // Copy a pixel by emulating the multiplex delivery of data from each amp
    for (i=0; i<namps; i++) {
      dp[n++] = ((val+i*ainc + rinc)>maxv)? (val+i*ainc + rinc)-maxv-1:val + i*ainc + rinc;
    }// End of amp pixel copy loop

    // Increment to next pixel value - wrapping if reached limit of data
    val = ((val+inc)>maxv)? (val+inc)-maxv-1:val+inc;

    c+=rcf;
  } // End of serial column loop
}

#endif //_CONTROLLER_H
