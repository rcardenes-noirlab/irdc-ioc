/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2002 by RSAA Computing Section 
 *
 * CICADA PROJECT
 * 
 * FILENAME 
 *  stats_test.cc
 * 
 * GENERAL DESCRIPTION
 *  Main program to test stats function
 *   
 * ORIGINAL AUTHOR: 
 *  Peter Young - RSAA 2000
 *
 * HISTORY
 *   June 2000 PJY    Created for RSAA CICADA package
 *   Sept 2002 PJY    Ported to VxWorks/NIFS from RSAA CICADA package
 *
 * HISTORY
 *
 */

/* Include files */

#include <stdio.h>
#include "image.h"
#include "fits.h"
#include "stats.h"
#include "exception.h"

/* defines */


/* typedefs */

/* class declarations */

/* global variables */

/* local function declarations */

/* local function definitions */

/* class function definitions */

/* Main program */
#ifdef vxWorks
int statsTest()
#else
int main()
#endif
{
  try {
    char buf[MSGLEN];
    Fits im;
    Fitsname fname;
    int n;
    int ss,sstep,ssize;
    double std_dev[100],median[100],mean[100],min[100];
    double max[100],mode[100],zmin[100],zmax[100],zslope[100];
    int i,loop,j;
    float con;
    float * sample;

    try {

      cout << "Enter name of fits file:";
      cin >> fname;
      im.read(fname);
      cout << "Enter contrast:";
      cin >> con;
      cout << "Enter loop count:";
      cin >> loop;
      if (loop>100) loop = 100;
      cout << "Enter first image sample #:";
      cin >> ssize;
      cout << "Enter image sample step:";
      cin >> sstep;

      sample = new float[ssize+loop*sstep];
      
      for (i=0; i<=im.extend; i++) {
	if (im.extend && i==0) continue;
	n = im.xt[i].nx*im.xt[i].ny;
	cout << "   n      SD   med     mod   mean      min      max       zmin      zmax  zslope" << endl;
	ss = ssize;
	for (j=0; j<loop; j++) {
	  switch (im.xt[i].bitpix) {
	  case BYTE_BITPIX: 
	    calc_stats(im.xt[i].data,n,im.xt[i].bitpix,std_dev[j],median[j],mean[j],
			min[j],max[j],mode[j],zmin[j],zmax[j],zslope[j],sample,ss,con);	
	    break;
	  case SHORT_BITPIX:
	    if (im.xt[i].bzero == 0x8000) 
	      calc_stats((unsigned short *)im.xt[i].data,n,im.xt[i].bitpix,std_dev[j],median[j],mean[j],
			  min[j],max[j],mode[j],zmin[j],zmax[j],zslope[j],sample,ss,con);	
	    else 
	      calc_stats((short *)im.xt[i].data,n,im.xt[i].bitpix,std_dev[j],median[j],mean[j],
			  min[j],max[j],mode[j],zmin[j],zmax[j],zslope[j],sample,ss,con);	
	    break;
	  case INT_BITPIX:
	    if (im.xt[i].bzero == 0x80000000)
	      calc_stats((unsigned int *)im.xt[i].data,n,im.xt[i].bitpix,std_dev[j],median[j],mean[j],
			  min[j],max[j],mode[j],zmin[j],zmax[j],zslope[j],sample,ss,con);	
	    else 
	      calc_stats((int *)im.xt[i].data,n,im.xt[i].bitpix,std_dev[j],median[j],mean[j],
			  min[j],max[j],mode[j],zmin[j],zmax[j],zslope[j],sample,ss,con);	
	    break;
	  case FLOAT_BITPIX:
	    calc_stats((float *)im.xt[i].data,n,im.xt[i].bitpix,std_dev[j],median[j],mean[j],
			min[j],max[j],mode[j],zmin[j],zmax[j],zslope[j],sample,ss,con);	
	    break;
	  case DOUBLE_BITPIX:
	    calc_stats((double *)im.xt[i].data,n,im.xt[i].bitpix,std_dev[j],median[j],mean[j],
			min[j],max[j],mode[j],zmin[j],zmax[j],zslope[j],sample,ss,con);	
	    break;
	  }
	  printf("%6d %.2f %.2f %.2f %.2f %9.2f %9.2f %9.2f %9.2f %.5f\n",ss,std_dev[j],median[j],
		 mode[j],mean[j],min[j],max[j],zmin[j],zmax[j],zslope[j]);
	  ss += sstep;
	}
	
      }
    }
    catch (Error& e){
      strlcpy(buf,e.record_error(__FILE__,__LINE__), MSGLEN);
      printf("Error=%s\n",buf);
    }
  }
  catch (std::exception& e) {
    log_exception(e,__FILE__,__LINE__);
    return 1;
  }
  return(0);
}

