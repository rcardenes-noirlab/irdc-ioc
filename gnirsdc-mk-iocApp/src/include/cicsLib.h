/*
*   FILENAME
*   --------
*   cicslib.h
*
*   PURPOSE
*   -------
*   This file defines the functions contained in the cicslib library.
*   It should be included by any routine wishing to use the library
*
*   REQUIREMENTS
*   ------------
*   If the definitions of the functions in cicslib are changed, or if
*   more functions are added, this file must be kept up to date
*
*   LIMITATIONS
*   -----------
*   If this file is included in an EPICS state notation program, the state
*   notation compiler will issue a warning message claiming that the
*   functions are being used without being declared. This is a "feature"
*   of the compiler and can be ignored
*
*   AUTHOR
*   ------
*   Steven Beard  (smb@roe.ac.uk)
*
*   HISTORY
*   -------
*   04-Dec-1996: Original version.                           (smb)
*   16-Dec-1996: cicsLogFloat added.                         (smb)
*   10-Jan-1997: cicsLogDouble added.                        (smb)
*   14-Jan-1997: cicsSetDebug added.                         (smb)
*   16-Jan-1997: Header modified and Functions renamed to
*                conform to new SPS.                         (smb)
*   09-Jun-1997: More debug level constants added.           (smb)
*   17-Jun-1997: cicsInitLogging added.                      (smb)
*   26-Jun-1997: DB and CA functions added.                  (smb)
*/
/* *INDENT-OFF* */
/*
 * $Log: cicsLib.h,v $
 * Revision 1.1  2005/12/21 23:01:58  gemvx
 * - LIB updates needed to properly build the code
 *
 * Revision 1.3  2004/08/20 02:43:33  pjy
 * Added cicsGetDebug routine
 *
 * Revision 1.2  2003/08/06 05:07:52  pjy
 * C++ declarations
 * Include posix threads semantics if MACRO defined
 *
 * Revision 1.1  2003/03/11 03:52:12  pjy
 * Import
 *
 * Revision 1.1.1.1  2000/09/21 01:33:37  jarnyk
 * Niri sources
 *
 * Revision 1.4  1999/11/13 06:35:07  yamada
 * Release 1.00 alpha 06.
 *
 * Revision 1.3  1999/11/09 11:40:41  yamada
 * Made it possible to add a lot more parameters to a cad record.
 *
 * Revision 1.2  1999/10/14 04:02:14  yamada
 * Better code sharing and stricter input checking for CAD subroutines.
 *
 * Revision 1.1  1997/07/26 01:30:29  yamada
 * Initial release
 *
 */
/* *INDENT-ON* */

#ifndef CICSLIB
#define CICSLIB

/* Declare the constants used by functions in the cicslib library. */

/*
 * These constants define the numerical codes used to store the current
 * debugging level. The levels are:
 *
 * NOLOG - only error messages;
 * NONE  - only error messages and log messages;
 * MIN   - minimal debugging (log messages plus a few others);
 * FULL  - full debugging (messages giving a full running comentary);
 */

#define CICS_DB_NOLOG 0
#define CICS_DB_NONE  1
#define CICS_DB_MIN   2
#define CICS_DB_FULL  3

/*
 * The above constants may also be used to define the level of each
 * message logged to the system. The following constants are used as
 * synonyms.
 */

#define CICS_DB_ERROR 0
#define CICS_DB_LOG   1


/* Declare the functions in the cicslib library. */

#ifdef __cplusplus
extern "C" {
#endif
long cicsCaGet( char *, char *, unsigned short, void * );
long cicsCaPut( char *, char *, unsigned short, void * );
long cicsDbGet( char *, char *, unsigned short, void * );
long cicsDbPut( char *, char *, unsigned short, void * );
void cicsSetDebug( const long );
long cicsGetDebug( void );
#ifdef __cplusplus
}
#endif

#endif
