/*
 * Copyright (c) 2000-2002 by RSAA
 * $Id: dc_control.cc,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI PROJECT
 *
 * FILENAME dc_control.cc
 *
 * GENERAL DESCRIPTION
 *
 * This file implements the DC control task routines. These, along with the
 * control SNL tasks, form the hub of the DC software design. They are
 * called when the IOC boots from the SNL tasks which should always be
 * running. They initialise the lower level software environment and then the
 * SNL codes sets itself up to wait on commands delivered through the gemini
 * EPICS layer. Its initial state is WAITING, ie waiting to accept initial
 * commands from the GEMINI EPICS environment. The POSIX message queue facility
 * is used and a signal handler is setup with the mq_notify call for notifying
 * the task when a message is delivered by the DC CAD routines.
 *
 * $Log: dc_control.cc,v $
 * Revision 1.1.1.1  2005/12/21 11:25:50  gemvx
 * - Nifs; first gemini-modified release
 *
 * Revision 1.31  2005/09/23 01:37:50  pjy
 * Increased timeouts
 * Fixed reverse polarity of EPICS bools
 *
 * Revision 1.30  2005/09/11 22:53:00  pjy
 * Strings for enums in messages
 *
 * Revision 1.29  2005/09/11 03:02:15  pjy
 * Removed initialisation of SNL controlled health
 *
 * Revision 1.28  2005/09/10 20:02:27  pjy
 * Moved handling_dhs_connection to dc_data during startup
 *
 * Revision 1.27  2005/09/07 02:04:17  pjy
 * Fixed readmode and timemode inputs
 *
 * Revision 1.26  2005/09/06 22:40:55  pjy
 * Added statusMsgs.
 * Changed readMode and timeMode
 *
 * Revision 1.25  2005/08/18 03:35:19  pjy
 * Better health handling
 * Only send OBSERVE to slave (who then passes to data)
 * Fix for handling ir_setup missing
 *
 * Revision 1.24  2005/08/02 04:50:10  pjy
 * DHS connection fixes.
 * Changes to test pattern params
 *
 * Revision 1.23  2005/07/30 01:13:09  pjy
 * Fixed typo
 *
 * Revision 1.22  2005/07/29 23:51:15  pjy
 * Added dc_controller_info cadstartmsg observec
 *
 * Revision 1.21  2005/07/28 09:44:40  pjy
 * Added sdsu_ntests
 * Removed duplicate setting of detid
 *
 * Revision 1.20  2005/07/26 08:18:30  pjy
 * Added nframes
 *
 * Revision 1.19  2005/07/24 12:00:07  pjy
 * Added reconnecting flag
 * Added reconnecting timeout
 *
 * Revision 1.18  2005/07/21 11:52:14  pjy
 * Handle waveMin and waveMax calcs from CC
 * Added CAR BUSY/IDLE for setDHSInfo
 *
 * Revision 1.17  2005/07/19 06:35:51  pjy
 * Added bunit
 * Added detector info SIR updates
 *
 * Revision 1.16  2005/07/14 06:50:42  pjy
 * Added "saving" param
 *
 * Revision 1.15  2005/07/12 07:01:53  pjy
 * Removed CC db refs
 * Added sim_mode_str,verify,gratingname and cancel_sdsu
 * Fixed boolean pars
 * Removed nref and npixReads
 * More validation
 *
 * Revision 1.14  2005/06/07 07:43:39  pjy
 * include dc_epics_db
 *
 * Revision 1.13  2005/06/07 06:58:54  pjy
 * Remove hard coded nifs db names
 *
 * Revision 1.12  2005/05/18 01:55:10  pjy
 * Added cosmMin theshold parameter
 *
 * Revision 1.11  2005/04/27 03:46:01  pjy
 * Changed sdsu_health to camera_health
 *
 * Revision 1.10  2005/02/25 04:51:50  pjy
 * DC working with PCI board on SVYFD
 *
 * Revision 1.9  2004/12/23 22:45:33  pjy
 * Restored some debug messages to DEBUG_FULL
 *
 * Revision 1.8  2004/12/22 06:12:26  pjy
 * DC control src files ready for first Gemini release
 *
 * Revision 1.7  2004/08/22 23:20:10  pjy
 * Added slave_state parameter
 *
 * Revision 1.6  2004/08/20 02:30:33  pjy
 * Gem 8.6 port
 * Testing as at August 2004
 *
 * Revision 1.5  2004/04/25 02:24:54  pjy
 * *** empty log message ***
 *
 * Revision 1.4  2003/09/19 06:24:26  pjy
 * Checkpoint commit where DC code is working for simulation OBSERVE command, CAD verification near complete, SAD updating near complete and VIEW mode cycles ok.
 *
 * Revision 1.3  2003/03/25 07:12:21  pjy
 * Consistent naming with _
 *
 * Revision 1.2  2003/03/01 06:19:54  pjy
 * Progress up to RSAA fires - begginning of sdsu VME tests.
 *
 * Revision 1.1  2001/10/19 00:28:38  pjy
 * Added for GNIRS DC
 *
 *
 */
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include "logging.h"
#include <iocsh.h> 
#include <epicsThread.h>
#include <epicsExport.h>

#include "dc.h"
#include "dc_epics_db.h"
#include "parameter.h"
#include "epicsParameterDB.h"
#include "utility.h"
#include "ipc.h"
#include "common.h"
#include "configuration.h"
#include "config_table.h"
#include "ir_setup.h"
#include "ir_camera.h"
#include "dummy_controller.h"

//+ Constants
#define EPICS_BOOL(x) ((x)? EPICS_TRUE:EPICS_FALSE)

//+ EPICS parameters
#include "dc_epics_param.h"

static const int SLAVE_TASK_PRIORITY = epicsThreadPriorityScanLow - 1;       //+ VxWorks priority to run slave task
static const int DATA_TASK_PRIORITY = epicsThreadPriorityCAServerLow + 1;    //+ VxWorks priority to run data task
static const int INIT_TASK_PRIORITY = epicsThreadPriorityScanLow - 1;        //+ VxWorks priority to run init task - simply performs initialisation
static const int SLAVE_WAIT = 60;                 //+ Time to wait for slave to start (s)
static const int DATA_WAIT = 30;                  //+ Time to wait for data to start (s)
static const int DATA_CONNECT_WAIT = 20;          //+ Time to wait for data to connect to the data handling system
static const int DC_MESSAGE_BUF_SIZE = 4096;      //+ Size of message ring buffer
static const char CAD_PREFIX[] = DCTOP;           //+ EPICS DC CAD database prefix
static const char CC_PREFIX[] = CCSADTOP;         //+ EPICS CC SAD database prefix

//+ Globals variables
//+ Parameter databases
extern ParameterDB *dc_status_db;                 //+ EPICS interface to the DC SAD
extern Parameter *dc_status;                      //+ Internal DC status parameters
extern ParameterDB *dc_par_db;                    //+ EPICS interface to the CAD pars
extern Parameter *dc_par;                         //+ Internal control parameters

//+ Data buffers
extern Detector_Image *shared_image_buffer;       //+ image transfer buffer for camera parts
extern float* dc_variance[4];                     //+ variance data array
extern unsigned char* dc_quality[4];              //+ quality data array
extern float* dc_ref_data[4];                     //+ reference pixel array

//+ Control semaphores
static shared_ptr<Semaphore> sem_slave_ready;     //+ Semphore indicating slave is ready
static shared_ptr<Semaphore> sem_data_ready;      //+ Semphore indicating data ready for transfer


//+ State
extern DC_State dc_state;                         //+ DC global state
extern DC_State dc_slave_state;                   //+ state of slave task
extern DC_State dc_data_state;                    //+ current Data task run state
extern Debug_Level debug_mode;                    //+ DC debugging mode
extern Nametype debug_mode_str;                   //+ debug mode as seen in EPICS DB
extern Nametype sim_mode_str;                     //+ simulation mode as seen in EPICS DB
extern Nametype state_str;                        //+ state as seen in EPICS DB
extern bool aborting;                             //+ flag set if OBSERVE is aborted
extern bool sdsu_canceling;                       //+ flag set if SDSU ops canceled
extern bool stopping;                             //+ flag set if OBSERVE is stopped
extern bool saving;                               //+ flag set when saving OBSERVE data
extern bool connection;                           //+ flag set when connecting/disconnecting to data handler
extern bool sdsu_init_done;                       //+ flag set if sdsu init complete
extern bool sdsu_cmd_running;                     //+ flag set if sdsu command active
extern bool camera_setup_changed;                 //+ Flag set when any readout setting change

//+ Configuration, status, control and reporting structures
extern Nametype configName;                       //+ Name of camera configuration
extern Instrument_Status *inst_status;            //+ Full instrument status - local copy
extern Config *shared_config;                     //+ Full camera configuration - read from disk
// extern Message *dc_message;                       //+ Message handling object
// extern Report *dc_report;                         //+ Reporting object
extern Verify_Spec verify_spec;                   //+ Readout verification/test pattern spec
extern bool do_verify;                            //+ Do readout verification

//+ Data destination
extern DC_Dest data_destination;                  //+ Destination of data - either dhs, fits or none

//+ Data handler connection
extern bool doConnect;                            //+ Connect/disconnect from data handler

//+ FITS server
extern bool fits_available;                       //+ flag set if FITS server is available
extern bool fits_connected;                       //+ flag set when fits server connected
extern long fits_server_port;                     //+ FITS server socket port number
extern Hostnametype fits_server_name;             //+ FITS server host name

//+ System health records
extern Nametype dc_health;                        //+ DC health state
extern EPICS_String dc_health_msg;                //+ DC health state message
extern Nametype data_health;                      //+ Data task health state
extern EPICS_String data_health_msg;              //+ Data task health state message
extern Nametype slave_health;                     //+ Slave task health state
extern EPICS_String slave_health_msg;             //+ Slave task health state message
extern Nametype camera_health;                    //+ Camera health state
extern EPICS_String camera_health_msg;            //+ Camera health state message
extern bool camera_op;                            //+ Camera operation flag
extern EPICS_String camera_op_msg;                //+ Camera operation message

//+ Detector info SIRS
extern Nametype detector_type;                    //+ Detector type
extern Nametype detector_id;                      //+ detector id
extern Nametype timing_dsp;                       //+ SDSU timing DSP name
extern Nametype interface_dsp;                    //+ SDSU interface DSP name

//+ SDSU DSP command SIRS
extern long sdsu_board;                           //+ SDSU direct command board id
extern long sdsu_cmd;                             //+ SDSU direct command
extern long sdsu_mem_space;                       //+ SDSU direct command memory space
extern long sdsu_mem_addr;                        //+ SDSU direct command memory address
extern long sdsu_arg1;                            //+ SDSU direct command argument 1
extern long sdsu_arg2;                            //+ SDSU direct command argument 2
extern int sdsu_debug;                            //+ SDSU debug command argument
extern int sdsu_ntests;                           //+ SDSU test command argument
extern bool sdsu_comms;                           //+ SDSU comms flag

//+ Camera object
extern IR_Camera *ir_camera;                      //+ The infra red camera object
static IR_Camera* ir_soft_camera = nullptr;          //+ Pointer to 'soft' instance of camera object for accessing status

//+ Parameters required for image spectral compression and stacking
extern double gratingMin;                         //+ Grating min wavelength (microns)
extern double gratingMax;                         //+ Grating max wavelength (microns)
extern Nametype gratingName;                      //+ Name of current grating

//+ Mailboxes and state
static DC_State dc_control_state;                 //+ Current run state
static shared_ptr<Mailbox> slave_mbox;            //+ Slave task message queue
static shared_ptr<Mailbox> data_mbox;             //+ data task message queue

static shared_ptr<Semaphore> image_ready_sem;     //+ semaphore indicating image trasfer buffer ready
static shared_ptr<Semaphore> image_empty_sem;     //+ semaphore indicating image trasfer buffer empty
static shared_ptr<Semaphore> data_xfer_sem;       //+ semaphore contolling access to data xfer pipeline

static epicsThreadId dc_slave_tid = 0;            //+ task ID of slave task
static epicsThreadId dc_data_tid  = 0;            //+ task ID of data task

static string cad_input_str;                      //+ Used as a common memory location for parameters written to CAD inputs
static string observec;                           //+ Location for observeC value
static string status_str;                         //+ Location for status string

//+ The input setup object
extern IR_Setup* ir_setup;                        //+ Used for retrieving and storing setup information from disk

//+ Global functions
#ifdef __cplusplus
extern "C" void dc_control_startup(epicsEvent *);
extern "C" void dc_control_shutdown();
extern "C" void dc_control_command(DC_Control_Msg msg);
extern "C" void dc_startup();
extern "C" void dc_init();
extern "C" void dc_start_observe();
extern "C" void dc_stop();
extern "C" void dc_abort();
extern "C" void dc_debug();
extern "C" void dc_reboot();
extern "C" void dc_controller_init();
extern "C" void dc_manage_data_server_connection();
extern "C" void dc_controller_cmd();
extern "C" void dc_controller_info();
extern "C" void dc_controller_shutdown();
extern "C" void dc_controller_test();
extern "C" void dc_controller_reset();
extern "C" void dc_controller_debug();
extern "C" void dc_controller_simulation();
extern "C" void dc_controller_cancel();
extern "C" void load_camera_setup(int load);
#endif

//+ Local functions
static void read_configuration();
static void add_parameters();
static void add_ir_params(IR_Mode ir_mode);
static void load_ir_params(IR_Mode ir_mode);
static void load_config_params();
static void dc_command(DC_Cmd cmd, bool send_slave, bool send_data);


//////////////////////////////////////////////////////////////////////////////////////////////////////
//    H E L P E R    C L A S S E S
//////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////
//    G L O B A L    F U N C T I O N S
//////////////////////////////////////////////////////////////////////////////////////////////////////

static bool is_task_running(epicsThreadId tid)
{
	return !epicsThreadIsSuspended(tid);
}

/*
 *+
 * FUNCTION NAME: void dc_control_startup(epicsEvent *)
 *
 * INVOCATION: dc_control_startup()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > event - an event that signals completion of the thread
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: initialises the DC DC Control data structures
 *
 * DESCRIPTION: Sets up following
 * . The DC status and parameter objects
 * . The Slave message queue
 * . creates the data arrays
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_control_startup(epicsEvent *event)
{
  char pname[IPC_NAMELEN];                // Used for naming shared data structures
  auto termination_event = make_fn_epicsEvent_guard(event); // Ensures that we'll signal completion

  logger::setLevel(logger::Level::Full);

  try {
    logger::message(logger::Level::Full, "In dc_control_startup");

    // Instantiate Par and Status objects for this run
    if (dc_par_db != nullptr) {
      throw Error("Setup must only be called once!", E_ERROR, -1, __FILE__, __LINE__);
    }

    // First the Parameter DB
    dc_par_db = new EpicsParameterDB();
//    ipc_name(pname, "Ctlr", "Par", 0, 0);
    strcpy(pname, "CtrlPar");
    dc_par = new Parameter(dc_par_db, pname, nullptr, CAD_PREFIX);

    // Now the DC status DB
//    ipc_name(pname, "Ctlr", "DStat", 0, 0);
    strcpy(pname, "CtrlDStat");
    dc_status_db = new EpicsParameterDB();
    dc_status = new Parameter(dc_status_db, pname, nullptr, IR_PREFIX);  

    logger::message(logger::Level::Full, "Databases created");

    // Create the transfer image buffer - do this by allocating enough space for
    // the headers and image array contiguously
    shared_image_buffer = (Detector_Image*) malloc(sizeof(Detector_Image) + IMAGE_BUF_SIZE);
    if (!shared_image_buffer) {
      throw Error("Unable to allocate space for shared image!", E_ERROR, errno, __FILE__, __LINE__);
    }

    logger::message(logger::Level::Full, "Shared image created, address=0x%p, size=%ld",
		    shared_image_buffer,
		    sizeof(Detector_Image) + IMAGE_BUF_SIZE);

    // Create the configuration buffer
    shared_config = new Config;

    // Create the instrument status structure
    inst_status = new Instrument_Status;
   
    inst_status->id = 0;
    inst_status->pid = 0;
   

    // Create slave message queue
    slave_mbox = Mailbox::create(DC::SLAVE_MBOX);
    data_mbox = Mailbox::create(DC::DATA_MBOX);
    {
	    char test[1500];
	    try {
	    	data_mbox->get_message(test, 1500, 1);
	    }
	    catch (IPC_Error e) {
		    //logger::message(logger::Level::Min, "Error testing Slave Mailbox: %s", e.what());
	    }
	    try {
	    	slave_mbox->get_message(test, 1500, 1);
	    }
	    catch (IPC_Error e) {
		    //logger::message(logger::Level::Min, "Error testing Data Mailbox: %s", e.what());
	    }
    }
    logger::message(logger::Level::Full, "Mailboxes created");

    // Now create image buffer semaphores
    image_ready_sem = Semaphore::create(Camera::IMAGE_READY_SEM);
    // Don't wait for the first semaphore == image is empty
    image_empty_sem = Semaphore::create(Camera::IMAGE_EMPTY_SEM, false);
    data_xfer_sem = Semaphore::create(Camera::DATA_XFER_SEM);

    // Add some parameters - these are parameter DB variables that access the
    // EPICS CAD/CAR and SAD databases
    add_parameters();  

    logger::message(logger::Level::Full, "Parameters added");

    dc_control_state = DC_State::DC_CONTROL_WAITING;
    dc_state = DC_State::DC_STOPPED;

    // Set the current debug_mode based on what is in database
    dc_set_debug_level();

    logger::message(logger::Level::Full, "Control and status parameter object instantiated");
    logger::message(logger::Level::Full, "Data structures created");
    logger::message(logger::Level::Full, "Message queues setup");

    // Create a binary semaphore to handshake with slave
    // TODO: Maybe we should raise something if this fails? We're not checking at all...
    sem_slave_ready = Semaphore::create(DC::SLAVE_READY_SEM);

    // Create a binary semaphore to handshake with data
    sem_data_ready = Semaphore::create(DC::DATA_READY_SEM);

    logger::message(logger::Level::Full, "Semaphores created");

    dc_set_health(DC_HEALTH, E_OK, DC_HEALTH_MSG, dc_health_msg, " ");

  }
  catch (Error& dce) {
    logger::message(logger::Level::NoLog,dce.record_error(__FILE__,__LINE__));
    dc_set_health(DC_HEALTH, dce.type, DC_HEALTH_MSG, dc_health_msg, (char*)dce.msg.c_str());
  }
 
  logger::message(logger::Level::Min, "dc_control_startup done");
}
/*
 *+
 * FUNCTION NAME: void dc_control_command(DC_Control_Msg msg)
 *
 * INVOCATION: dc_control_command(msg)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > msg - this is the command message from the SNL code
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Used to start/restart DC slave and data tasks
 *
 * DESCRIPTION: The dc_ss SNL task has received an INIT command
 * from theGemini environment. This routine is then called to start or
 * restart the DC slave and data tasks. An INIT command is then passed
 * to the slave message queue. This is alos used to initiate a REBOOT.
 *
 * EXTERNAL VARIABLES: Requires access to slave_mbox, dc_par and dc_status
 *
 * PRIOR REQUIREMENTS: dc_control_command must be called before this is called
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_control_command(DC_Control_Msg msg)
{
  char msgbuf[MSGLEN];              // Used for building status messages

  try {
    logger::message(logger::Level::Full, "In dc_control_command");
    if (dc_control_state == DC_State::DC_CONTROL_WAITING)
      logger::message(logger::Level::Full, "DC_State::DC_CONTROL_WAITING");
    else
      logger::message(logger::Level::Full, "DC_State::DC_CONTROL_READY");
   
    if (dc_par_db == nullptr) {
      throw Error("Setup must be called first!", E_ERROR, -1, __FILE__, __LINE__);
    }

    switch (msg.cmd) {

    case DC_Cmd::INIT:
      logger::message(logger::Level::Full,  "dc_control_command: INIT");

      // Set state to INITIALIZING
      dc_state = DC_State::DC_INITIALIZING;

      // Check if already running slave and data, if so start them
      if (dc_control_state == DC_State::DC_CONTROL_WAITING) {
	// Get name of configuration to use
	dc_status->get(CONFIGNAME, configName, 0, NAMELEN);

	logger::message(logger::Level::Full, "About to read configuration for %s", configName);

	// Read configuration information from disk
	read_configuration();

	// Spawn the slave task
	logger::message(logger::Level::Full, "spawning slave task");
	if ((dc_slave_tid = epicsThreadCreate("dc_slave", SLAVE_TASK_PRIORITY, epicsThreadGetStackSize(epicsThreadStackMedium),
					(EPICSTHREADFUNC) dc_slave, nullptr)) == 0)
	{
	  throw Error("taskSpawn of dc_slave failed!", E_ERROR, errno, __FILE__, __LINE__);
	}

	logger::message(logger::Level::Full, "spawned slave task");

	// wait for slave to start
	if (!sem_slave_ready->timed_wait(SLAVE_WAIT * 1000)) {
	  logger::message(logger::Level::NoLog, "dc_slave not ready within %ds - cleaning up...", SLAVE_WAIT);
	  // TODO: Delete task dc_slave...
	  /*
	  if (taskDelete(dc_slave_tid) == ERROR) {
	    logger::message(logger::Level::NoLog, "TaskDelete of dc_slave failed!");
	  }
	  */
	  throw Error("Because dc_slave had problems starting - bailing out!", E_ERROR, errno, __FILE__, __LINE__);
	}

	logger::message(logger::Level::Full, "Got slave ready semaphore");

	// Spawn the data task
	logger::message(logger::Level::Full, "spawning data task");
	if ((dc_data_tid = epicsThreadCreate("dc_data", DATA_TASK_PRIORITY, epicsThreadGetStackSize(epicsThreadStackBig),
					(EPICSTHREADFUNC) dc_data, nullptr)) == 0)
	{
	  throw Error("taskSpawn of dc_data failed!", E_ERROR, errno, __FILE__, __LINE__);
	}

	logger::message(logger::Level::Full, "spawned data task");

	// wait for data to start
	if (!sem_data_ready->timed_wait(DATA_WAIT * 1000)) {
	  logger::message(logger::Level::NoLog, "dc_data not ready within %ds - cleaning up...", DATA_WAIT);
	  /*
	   * TODO: How to destroy a thread with OSI???
	  if (taskDelete(dc_data_tid) == ERROR) {
	    logger::message(logger::Level::NoLog, "TaskDelete of dc_data failed!");
	  }
	  */
	  throw Error("Because dc_data had problems starting - bailing out!", E_ERROR, errno, __FILE__, __LINE__);
	}
      } else {
      	logger::message(logger::Level::NoLog, "slave & data already running, sending new INIT!");
      }

      // Send INIT command to slave
      dc_control_state = DC_State::DC_CONTROL_READY;

      dc_command(DC_Cmd::INIT, true, true);

      // For INIT we must wait on the slave and data tasks to complete
      // TODO: We want to reaplace ALL instances of sprintf. Find out a way
      if (!sem_slave_ready->timed_wait(SLAVE_WAIT * 1000)) {
	sprintf(msgbuf, "dc_slave failed to complete INIT within %ds ...", SLAVE_WAIT);
	throw Error(msgbuf, E_ERROR, ETIME, __FILE__, __LINE__);
      }
      if (!sem_data_ready->timed_wait(DATA_WAIT * 1000)) {
	sprintf(msgbuf, "dc_data failed to complete INIT within %ds ...", DATA_WAIT);
	throw Error(msgbuf, E_ERROR, ETIME, __FILE__, __LINE__);
      }

      // Load current settings for next setup. This saves the engineer from
      // resetting all input parameters after start up
      load_camera_setup(TRUE);
      logger::message(logger::Level::Full, "Restored saved setup!");

      // Set state to RUNNING
      dc_state = DC_State::DC_RUNNING;

      logger::message(logger::Level::Full, "INIT completed ok");
      break;

    case DC_Cmd::OBSERVE:
      // Only send the OBSERVE command to the slave task - it will pass on to
      // data if ok
      if (dc_control_state != DC_State::DC_CONTROL_READY) {
	logger::message(logger::Level::NoLog, "DC Control not READY - send INIT first!");
      } else {
	dc_command(msg.cmd, true, false);
      }
      break;


    case DC_Cmd::REBOOT:
      logger::message(logger::Level::Full, "REBOOT");

      // Set state to stopped and send message to tasks to stop
      if (dc_control_state != DC_State::DC_CONTROL_WAITING) {
	// switch back to waiting state
	dc_control_state = DC_State::DC_CONTROL_WAITING;
	dc_state = DC_State::DC_STOPPED;
	dc_command(DC_Cmd::REBOOT, true, true);
      }
      break;

    case DC_Cmd::DEBUG:
      logger::message(logger::Level::Full, "DEBUG");
      dc_set_debug_level();
      dc_command(msg.cmd, true, false);
      break;     

    case DC_Cmd::DO_CONNECTION:
      // TODO: This controls the connection. DHS is not an option any longer
      if (dc_control_state != DC_State::DC_CONTROL_READY) {
	logger::message(logger::Level::NoLog, "DC Control not READY - send INIT first!");
      } else {
	dc_status->get(DOCONNECT, doConnect);

	// Send connect to Data task only
	dc_command(msg.cmd, false, true);

	// Wait for the data task to be ready
	if (!sem_data_ready->timed_wait(DATA_CONNECT_WAIT * 1000)) {
		/* TODO: Figure out...
	  if (handling_dhs_connection) {
	    dc_status->put(DHS_PROBLEM, true);
	    strncpy(msgbuf,"Timeout handling DHS connection?",MAX_STRING_SIZE);
	    dc_status->put(DHS_PROBLEM_MSG,msgbuf,0,strlen(msgbuf));
	  } else
	  */ if (fits_available) {
	    dc_status->put(FITS_PROBLEM, true);
	    strncpy(msgbuf,"Timeout handling FITS server connection?",MAX_STRING_SIZE);
	    dc_status->put(FITS_PROBLEM_MSG,msgbuf,0,strlen(msgbuf));
	  }
	}

	dc_status->put(CONNECTION, false);

	/*
	if (dhs_available)
	  errlogPrintf( "DHS connection handled doConnect=%d, connected=%d!", // TODO: DEBUG_FULL
			doConnect, dhs_connected);
			*/
	if (fits_available)
	  logger::message(logger::Level::Full, "FITS server connection handled doConnect=%d, connected=%d!",
			doConnect, fits_connected);

	// Set state to RUNNING
	dc_state = DC_State::DC_RUNNING;
      }
      break;

    case DC_Cmd::CONTROLLER_INIT:
    case DC_Cmd::CONTROLLER_SHUTDOWN:
    case DC_Cmd::CONTROLLER_TEST:
    case DC_Cmd::CONTROLLER_RESET:
    case DC_Cmd::CONTROLLER_SIMULATE:
    case DC_Cmd::CONTROLLER_INFO:
    case DC_Cmd::CONTROLLER_CMD:
      if (dc_control_state != DC_State::DC_CONTROL_READY) {
	logger::message(logger::Level::NoLog, "DC Control not READY - send INIT first!");
      } else {
	dc_command(msg.cmd, true, false);
      }
      break;

    default:
      if (dc_control_state != DC_State::DC_CONTROL_READY) {
	logger::message(logger::Level::NoLog, "DC Control not READY - send INIT first!");
      } else {
	dc_command(msg.cmd, true, true);
      }
      break;
    }

  }
  catch (Error& dce) {
    logger::message(logger::Level::NoLog, dce.record_error(__FILE__,__LINE__));
    dc_set_health(DC_HEALTH, dce.type, DC_HEALTH_MSG, dc_health_msg, (char*)dce.msg.c_str());
  }
}

/*
 *+
 * FUNCTION NAME: void dc_control_shutdown(void)
 *
 * INVOCATION: dc_control_shutdown()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: initialises the DC Control data structures
 *
 * DESCRIPTION: Sets up following
 * . The DC status and parameter objects
 * . The Slave message queue
 * . creates the data arrays
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

void dc_control_shutdown()
{
  const int TASK_TIMEOUT = 10000;    // Time to wait for slave and data to finish (msec)
  DC_Control_Msg msg;                // message to send to slave and data
  int timer;                         // timeout counter

  try {
    logger::message(logger::Level::Full, "In dc_control_shutdown");
   
   // Send slave a REBOOT to shut it down
    if (dc_slave_tid != 0) {
      msg.cmd = DC_Cmd::REBOOT;
      dc_control_command(msg);
      logger::message(logger::Level::Full, "REBOOT sent to slave & data tasks!");
    }

    // Wait until Slave finishes before exiting
    timer = 0;
    if (dc_slave_tid != 0) {
      while (is_task_running(dc_slave_tid) && (timer <= TASK_TIMEOUT)) {
	epicsThreadSleep(HALF_SECOND / 1000.0);
	timer += HALF_SECOND;
      }
      if (timer > TASK_TIMEOUT) {
	logger::message(logger::Level::Min, "dc_control_shutdown: Slave task failed to complete in timeout period!"); 
	/* TODO: Figure out if we can kill the thread in an easy way
	if (taskDeleteForce(dc_slave_tid) == ERROR) {
	  logger::message(logger::Level::NoLog, "TaskDelete of dc_slave failed!");
	}
	*/
      }
      dc_slave_tid = 0;
    }
    logger::message(logger::Level::Full, "Verified slave task exited!");
   

    // Wait until Data finishes before exiting
    timer = 0;
    if (dc_data_tid != 0) {
      while (is_task_running(dc_data_tid) && (timer <= TASK_TIMEOUT)) {
	epicsThreadSleep(HALF_SECOND / 1000.0);
	timer += HALF_SECOND;
      }
      if (timer > TASK_TIMEOUT) {
	logger::message(logger::Level::Min, "dc_control_shutdown: Data task failed to complete in timeout period!"); 
	/*
	 * TODO: Figure out how to delete the task
	if (taskDeleteForce(dc_data_tid) == ERROR) {
	  logger::message(logger::Level::NoLog, "TaskDelete of dc_data failed!");
	}
	*/
      }
      dc_data_tid = 0;
    }
    logger::message(logger::Level::Full, "Verified data task exited!");

    // Now remove the C++ objects
    /*
     * TODO: Delete semaphores and mailboxes
    if (image_ready_sem != nullptr) {
      delete image_ready_sem;
      image_ready_sem = nullptr;
    }

    if (image_empty_sem != nullptr) {
      delete image_empty_sem;
      image_empty_sem = nullptr;
    }

    if (data_xfer_sem != nullptr) {
      delete data_xfer_sem;
      data_xfer_sem = nullptr;
    }
    logger::message(logger::Level::Full, "Semaphores deleted.");

    // Close and remove message queues
    if (slave_mbox != nullptr) {
      delete slave_mbox;
      slave_mbox = nullptr;
    }

    if (data_mbox != nullptr) {
      delete data_mbox;
      data_mbox = nullptr;
    }
    logger::message(logger::Level::Full, "Mailboxes deleted.");
    */


    // Delete the soft camera instance
    if (ir_soft_camera != nullptr) {
      delete ir_soft_camera;
      ir_soft_camera = nullptr;
      logger::message(logger::Level::Full, "Soft camera instance deleted.");
    }

    // Delete the Config and status buffers
    if (inst_status != nullptr) {
      delete inst_status;
      inst_status = nullptr;
    }

    if (shared_config != nullptr) {
      delete shared_config;
      shared_config = nullptr;
    }
    logger::message(logger::Level::Full, "Config and status buffers deleted.");

    // Delete data arrays
    if (shared_image_buffer != nullptr) {
      free((void*)shared_image_buffer);
      shared_image_buffer = nullptr;
    }
    logger::message(logger::Level::Full, "Shared image buffer deleted.");

    // Delete Par and Status objects
    if (dc_par != nullptr) {
      delete dc_par;
      dc_par = nullptr;
    }
    if (dc_par_db != nullptr) {
      delete dc_par_db;
      dc_par_db = nullptr;
    }
    if (dc_status != nullptr) {
      delete dc_status;
      dc_status = nullptr;
    }
    if (dc_status_db != nullptr) {
      delete dc_status_db;
      dc_status_db = nullptr;
    }
    logger::message(logger::Level::Full,"Parameter objects deleted.");

    sem_slave_ready.reset();
    sem_data_ready.reset();

   // Set back to WAITING state
    dc_control_state = DC_State::DC_CONTROL_WAITING;

  }
  catch (Error& dce) {
    logger::message(logger::Level::NoLog, dce.record_error(__FILE__,__LINE__));
  }
 
  logger::message(logger::Level::Min, "dc_control_shutdown done");
}
/*
 *+
 * FUNCTION NAME: dc_startup()
 *
 * INVOCATION: dc_startup()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Spawns a task to run the dc_control_startup command
 *
 * DESCRIPTION: Needs to be done in a spawned task because of the stack size required.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *

 *-
 */
void dc_startup()
{
  epicsThreadId dc_startup_id;
  epicsEvent done;

  if ((dc_startup_id = epicsThreadCreate("dc_startup", INIT_TASK_PRIORITY, epicsThreadGetStackSize(epicsThreadStackMedium),
					(EPICSTHREADFUNC) dc_control_startup, &done)) == 0)
  {
    logger::message(logger::Level::NoLog,"dc_startup: taskSpawn of dc_control_startup failed!");
  } else {
    logger::message(logger::Level::Full, "dc_startup: taskSpawn of dc_control_startup succeeded!");
  }

  // Wait until Init finishes before exiting
  // TODO: Maaaaaybe we should put a timer on this?
  logger::message(logger::Level::Full, "dc_startup: waiting for completion");
  done.wait();

  logger::message(logger::Level::Min, "dc_startup task completed ok");
}

/*
 *+
 * FUNCTION NAME: dc_init_thread(epicsEvent *event)
 *
 * INVOCATION: dc_init_thread()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > event - an event to signal completion of the thread
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Runs dc_control INIT command.
 *
 * DESCRIPTION: Needs to be done separately from dc_init so that we
 *              can pass a non-pointer as dc_control_command's argument.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */

void dc_init_thread(epicsEvent *event) {
  DC_Control_Msg msg;
  auto termination_event = make_fn_epicsEvent_guard(event);

  msg.cmd = DC_Cmd::INIT;
  logger::message(logger::Level::Full, "dc_init_thread: Running dc_control_command(INIT)");
  dc_control_command(msg);
}

/*
 *+
 * FUNCTION NAME: dc_init()
 *
 * INVOCATION: dc_init()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Spawns a task to run the dc_control INIT command
 *
 * DESCRIPTION: Needs to be done in a spawned task because of the stack size required.
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_init()
{
  epicsThreadId dc_init_id;
  epicsEvent done;
  const char task_name[] = "dc_init";

  if ((dc_init_id = epicsThreadCreate(task_name, INIT_TASK_PRIORITY, epicsThreadGetStackSize(epicsThreadStackMedium),
					(EPICSTHREADFUNC) dc_init_thread, &done)) == 0)
  {
    logger::message(logger::Level::NoLog,"dc_init: taskSpawn of dc_init_thread() failed!");
  } else
    logger::message(logger::Level::Full,"dc_init: taskSpawn of dc_init_thread() succeeded!");

  // Wait until Init finishes before exiting
  done.wait();

  if (dc_state == DC_State::DC_RUNNING) {
    logger::message(logger::Level::Full,"dc_init task completed ok.");
    dc_status->put(STATE, "RUNNING", 0, NAMELEN);
  } else {

    // Else the slave and/or the data tasks have failed to start properly
    // indicate why here
    logger::message(logger::Level::Full,"dc_init task failed to complete!");

    // Set the system state flag into init_check
    dc_status->put(STATE, "INIT_CHECK", 0, NAMELEN);
  }
}
/*
 *+
 * FUNCTION NAME: dc_start_observe()
 *
 * INVOCATION: dc_start_observe()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends the OBSERVE message to the control routine
 *
 * DESCRIPTION: Simply sets up a OBSERVE command and calls dc_control_command
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_start_observe()
{
  DC_Control_Msg msg;
 
  // use control routine to send OBSERVE command
  msg.cmd = DC_Cmd::OBSERVE;
  dc_control_command(msg);
 }

/*
 *+
 * FUNCTION NAME: dc_stop()
 *
 * INVOCATION: dc_stop()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends the STOP message to the control routine
 *
 * DESCRIPTION: Simply sets up a STOp command and calls dc_control_command
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_stop()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::STOP;
  dc_control_command(msg);
}

/*
 *+
 * FUNCTION NAME: dc_manage_data_server_connection
 *
 * INVOCATION: dc_manage_data_server_connection()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends the DO_CONNECTION message to the control routine
 *
 * DESCRIPTION: Simply sets up a DO_CONNECTION command and calls dc_control_command
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_manage_data_server_connection()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::DO_CONNECTION;
  dc_control_command(msg);
}

/*
 *+
 * FUNCTION NAME: dc_abort()
 *
 * INVOCATION: dc_abort()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends the ABORT message to the control routine
 *
 * DESCRIPTION: Simply sets up a Abort command and calls dc_control_command
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_abort()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::ABORT;
  dc_control_command(msg);
}
/*
 *+
 * FUNCTION NAME: dc_debug()
 *
 * INVOCATION: dc_debug()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends the DEBUG message to the control routine
 *
 * DESCRIPTION: Simply sets up a Debug command and calls dc_control_command
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_debug()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::DEBUG;
  dc_control_command(msg);
}
/*
 *+
 * FUNCTION NAME: dc_reboot()
 *
 * INVOCATION: dc_reboot()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends the REBOOT message to the control routine
 *
 * DESCRIPTION: Simply sets up a Reboot command and calls dc_control_command
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_reboot()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::REBOOT;
  dc_control_command(msg);
}
/*
 *+
 * FUNCTION NAME: dc_controller_init()
 *
 * INVOCATION: dc_controller_init()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends the CONTROLLER_INIT message to the control routine
 *
 * DESCRIPTION: Simply sets up an CONTROLLER_INIT command and calls dc_control_command
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_controller_init()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::CONTROLLER_INIT;
  dc_control_command(msg);
}

/*
 *+
 * FUNCTION NAME: dc_controller_cmd()
 *
 * INVOCATION: dc_controller_cmd()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends a DSP command to the SDSU controller
 *
 * DESCRIPTION: Grabs the current SDSU command detail from the SAD database
 * and sends to the SDSU controller code for executiuon
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_controller_cmd()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::CONTROLLER_CMD;
  logger::message(logger::Level::NoLog,"Got controller cmd!");
  dc_control_command(msg);
}
/*
 *+
 * FUNCTION NAME: dc_controller_info()
 *
 * INVOCATION: dc_controller_info()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends a DSP command to the SDSU controller
 *
 * DESCRIPTION: Grabs the current SDSU command detail from the SAD database
 * and sends to the SDSU controller code for executiuon
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_controller_info()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::CONTROLLER_INFO;
  logger::message(logger::Level::Full, "Got controller info!");
  dc_control_command(msg);
}
/*
 *+
 * FUNCTION NAME: dc_controller_shutdown()
 *
 * INVOCATION: dc_controller_shutdown()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends a DSP command to the SDSU controller
 *
 * DESCRIPTION: Grabs the current SDSU command detail from the SAD database
 * and sends to the SDSU controller code for executiuon
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_controller_shutdown()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::CONTROLLER_SHUTDOWN;
  dc_control_command(msg);
}
/*
 *+
 * FUNCTION NAME: dc_controller_reset()
 *
 * INVOCATION: dc_controller_reset()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends a DSP command to the SDSU controller
 *
 * DESCRIPTION: Grabs the current SDSU command detail from the SAD database
 * and sends to the SDSU controller code for executiuon
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_controller_reset()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::CONTROLLER_RESET;
  dc_control_command(msg);
}
/*
 *+
 * FUNCTION NAME: dc_controller_debug()
 *
 * INVOCATION: dc_controller_debug()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends a DSP command to the SDSU controller
 *
 * DESCRIPTION: Grabs the current SDSU command detail from the SAD database
 * and sends to the SDSU controller code for executiuon
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_controller_debug()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::CONTROLLER_DEBUG;
  dc_control_command(msg);
}

/*
 *+
 * FUNCTION NAME: dc_controller_simulation()
 *
 * INVOCATION: dc_controller_simulation()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends the CONTROLLER_SIMULATION message to the control routine
 *
 * DESCRIPTION: Simply sets up a Simulate command and calls dc_control_command
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_controller_simulation()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::CONTROLLER_SIMULATE;
  dc_control_command(msg);
}

/*
 *+
 * FUNCTION NAME: dc_controller_cancel()
 *
 * INVOCATION: dc_controller_cancel()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends a DSP command to the SDSU controller
 *
 * DESCRIPTION: Grabs the current SDSU command detail from the SAD database
 * and sends to the SDSU controller code for executiuon
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_controller_cancel()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::CONTROLLER_CANCEL;
  dc_control_command(msg);
}
/*
 *+
 * FUNCTION NAME: dc_controller_test()
 *
 * INVOCATION: dc_controller_test()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: None
 *
 * PURPOSE: Sends a DSP command to the SDSU controller
 *
 * DESCRIPTION: Grabs the current SDSU command detail from the SAD database
 * and sends to the SDSU controller code for executiuon
 *
 * EXTERNAL VARIABLES: None
 *
 * PRIOR REQUIREMENTS: None
 *
 * DEFICIENCIES:
 *
 *-
 */
void dc_controller_test()
{
  DC_Control_Msg msg;
  msg.cmd = DC_Cmd::CONTROLLER_TEST;
  dc_control_command(msg);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
//    L O C A L    F U N C T I O N S
//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *+
 * FUNCTION NAME: void read_configuration()
 *
 * INVOCATION: read_configuration()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Add all EPICS parameters used by the DC task
 *
 * DESCRIPTION:
 *
 * The DC tasks control, slave, data and readout access EPICS parameters for
 * read and write the the DC paramter database scheme. THis routine will add
 * all parameters needed by these tasks. Note that some parameters are accessed
 * by the DC camera object - these are added separately as needed by the
 * camera and controller class code.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *- */
static void read_configuration()
{
  Camera_Table cameraTable;           //+ Configuration  table for the DC camera
  Hostnametype host;                  //+ Name of this host
  Platform_Specific specific;         //+ platform specific data - for us nothing
  Camera_Desc *this_camera;
  Controller_Desc *this_controller;
 
  logger::message(logger::Level::Full, "reading camera configuration, %s\n", CAMERA_TABLE);

  // read in the camera configuration table
  cameraTable.read(CAMERA_TABLE);
 
  logger::message(logger::Level::Full,"Camera table read ok.");
 
  // Now set up configuration data structure
  shared_config->config_data.type = CAMERA;

  shared_config->config_data.Config_Data_u.camera.desc = cameraTable[configName];
  this_camera = &shared_config->config_data.Config_Data_u.camera.desc;
  this_controller = &this_camera->controller[0];

  logger::message(logger::Level::Min,"Camera %s found in table ok, controller 0 type=%d.", configName,
		this_controller->type);

  shared_config->config_data.Config_Data_u.camera.cam_id = 0;
  shared_config->config_data.Config_Data_u.camera.cid = 0;
  shared_config->config_data.Config_Data_u.camera.camera.type = IR_CAMERA;
  shared_config->config_data.Config_Data_u.camera.camera.Camera_Op_Config_u.ir.filler = 0;

  logger::message(logger::Level::Full,"Creating controller object with simple constructor");
 
  if (gethostname(host, HOSTLEN) == -1) {
    throw Error("Failed to get hostname of this IOC!", E_ERROR, errno, __FILE__, __LINE__);
  }

  // Delete the current soft camera instance if it exists
  if (ir_soft_camera != nullptr) {
    printf("There was a soft camera. Deleting!\n");
    delete ir_soft_camera;
    ir_soft_camera = nullptr;
  }

  logger::message(logger::Level::Full, "Creating soft IR_Camera instance, host=%s", host);
  ir_soft_camera = new IR_Camera(shared_config->config_data.Config_Data_u.camera.desc, 0, 0, host, inst_status,
				 &specific);

  logger::message(logger::Level::Full, "Soft camera object created, getting config");

  // Now call getconfig method
  shared_config->config_data.Config_Data_u.camera.controller = *ir_soft_camera->controller->getconfig(TRUE);

  logger::message(logger::Level::Full, "Controller configuration for %s read ok.\n", this_camera->controller[0].name);

}

/*
 *+
 * FUNCTION NAME: void load_camera_setup()
 *
 * INVOCATION: load_camera_setup()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Load CAD parameters with last saved settings
 *
 * DESCRIPTION:
 *
 * Each CAD parameter gets loaded with settings last saved. This is to
 * simplify engineering operations. It will save reloading all parameters
 * manually after each restart. The OCS should override these setting during
 * normal operation.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
void load_camera_setup(int load)
{
  try {
    logger::message(logger::Level::Min, "Reading setup file %s", IR_SETUP);
    if (ir_setup != nullptr) {
      delete ir_setup;
    }
    ir_setup = new IR_Setup();

    // Catch any errors when trying to read the default setup file
    // If this fails then we revert to system defaults
    try {
      ir_setup->read(IR_SETUP);
    }
    catch (Error& dce) {
      logger::message(logger::Level::NoLog, dce.record_error(__FILE__,__LINE__));
      dc_set_health(DC_HEALTH, E_WARNING, DC_HEALTH_MSG, dc_health_msg, (char*)dce.msg.c_str());
      // Lets try writing a new default file
      delete ir_setup;
      ir_setup = new IR_Setup();
      ir_setup->write(IR_SETUP);     
    }
    camera_setup_changed = false;

    if (load) {
      logger::message(logger::Level::Min, "Loading parameters");
    
      // Now that we have our parameters, update the CAD inputs
      load_ir_params(IR_VIEW_MODE);
      load_ir_params(IR_OBSERVE_MODE);

      load_config_params();
    }
  }
  catch (Error& dce) {
    logger::message(logger::Level::NoLog, dce.record_error(__FILE__,__LINE__));
    dc_set_health(DC_HEALTH, dce.type, DC_HEALTH_MSG, dc_health_msg, (char*)dce.msg.c_str());
  }
}

/*
 *+
 * FUNCTION NAME: void load_ir_params()
 *
 * INVOCATION: load_ir_params(IR_Mode ir_mode)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ir_mode - set to either VIEW or OBSERVE mode
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Load IR CAD parameters with current IR_Setup values
 *
 * DESCRIPTION:
 *
 * Puts an IR_Setup parameter into its EPICS CAD input
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void load_ir_params(IR_Mode ir_mode)
{
  string mode;
  IR_Exposure_Info *ir_einfo;           //+ Pointer to current ir exposure info structure
  IR_Data_Info *ir_dinfo;               //+ Pointer to current ir data info structure 
 
  if (dc_par == nullptr)
    return;
 
  if (ir_setup != nullptr) {

    // Note that boolean type values have to be negated for EPICS booleans

    if (ir_mode == IR_VIEW_MODE) {
      mode = "view_";
      ir_einfo = &ir_setup->view;
      ir_dinfo = &ir_setup->d_view;

      // Following parameters are only for VIEW mode
      dc_par->put(mode+DO_SUBTRACT, to_string(EPICS_BOOL(ir_dinfo->doSubFile)));
      dc_par->put(mode+SUB_FNAME, to_string(ir_dinfo->subFile));
      dc_par->put(mode+DO_CMP_IM, to_string(EPICS_BOOL(ir_dinfo->doCmpIm)));
      dc_par->put(mode+WAVE_MIN, to_string(ir_dinfo->waveMin));
      dc_par->put(mode+WAVE_MAX, to_string(ir_dinfo->waveMax));
    } else {
      mode = "obs_";
      ir_einfo = &ir_setup->obs;
      ir_dinfo = &ir_setup->d_obs;

      // Following parameters are only for OBSERVE mode
      dc_par->put(mode+DO_SAVE_VAR, to_string(EPICS_BOOL(ir_dinfo->saveVar)));
      dc_par->put(mode+DO_SAVE_QUAL, to_string(EPICS_BOOL(ir_dinfo->saveQual)));
      dc_par->put(mode+DO_SAVE_NDR, to_string(EPICS_BOOL(ir_dinfo->saveNdrs)));
      dc_par->put(mode+DO_SAVE_COADD, to_string(EPICS_BOOL(ir_dinfo->saveCoadds)));
    }

    dc_par->put(mode+IR_READ_MODE_IN, to_string((int)ir_einfo->readModeSet));
    dc_par->put(mode+IR_QUADRANTS_IN, to_string((int)ir_einfo->quadrantsSet));
/*
    dc_par->put(mode+NRESETS, to_string(ir_einfo->nresets));
    dc_par->put(mode+RESET_DELAY, to_string(ir_einfo->resetDelay));
    dc_par->put(mode+NFOWLER, to_string(ir_einfo->nfowler));
    dc_par->put(mode+PERIOD, to_string(ir_einfo->period));
    dc_par->put(mode+NPERIODS, to_string(ir_einfo->nperiods));
    dc_par->put(mode+NCOADDS, to_string(ir_einfo->ncoadds));
    dc_par->put(mode+READ_TIME, to_string(ir_einfo->readTime));
    dc_par->put(mode+READ_INTVAL, to_string(ir_einfo->readIntvl));
    dc_par->put(mode+TIME_MODE_IN, to_string((int)ir_einfo->timeModeSet));
*/
    dc_par->put(mode+EXPOSED, to_string(ir_einfo->exposed));

    dc_par->put(mode+DO_COSM_REJ, to_string(EPICS_BOOL(ir_dinfo->cosmRej)));
    dc_par->put(mode+COSM_THRESH, to_string(ir_dinfo->cosmThrsh));
    dc_par->put(mode+COSM_MIN, to_string(ir_dinfo->cosmMin));

  } else {
    throw Error("Make sure IR_SETUP object created!", E_ERROR, EINVAL, __FILE__, __LINE__);
  }

}

/*
 *+
 * FUNCTION NAME: void load_config_params()
 *
 * INVOCATION: load_config_params()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Load Config CAD parameters with current configuration values
 *
 * DESCRIPTION:
 *
 * Puts a configuration parameter into its EPICS CAD input
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void load_config_params()
{
  if (dc_par == nullptr)
    return;
 
}
 
/*
 *+
 * FUNCTION NAME: void add_parameters()
 *
 * INVOCATION: add_parameters()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Add all EPICS parameters used by the DC task
 *
 * DESCRIPTION:
 *
 * The DC tasks control, slave, data and readout access EPICS parameters for
 * read and write the the DC paramter database scheme. THis routine will add
 * all parameters needed by these tasks. Note that some parameters are accessed
 * by the DC camera object - these are added separately as needed by the
 * camera and controller class code.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void add_parameters()
{
  long lValid[6];
  static string cad_err_mess;          //+ Used as a common memory location for messages written to CAD MESS field
  static string obs_dd;                //+ Local heap based memory for the data_destination strings. NB do not make these
  static string view_dd;               //+ global because their constructors won't necessarily be called!
  string sValid[3];
  bool bValid[2];
  int i;
  Nametype status_par, status_par_db;  //+ Status message parameter names

  logger::message(logger::Level::Full, "Adding parameters to internal parameter DB");

  // Pointer to instrument control status part of status structure
  Instrument_Ctl_Status *ctl_status = &inst_status->inst_ctl_status;

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Add some status parameters that the dc_control task needs access to
  // Do this by creating an IR camera object that does not connect with hardware
  // but only sets up appropriate software hooks.
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // Add configuration name
  dc_status->add(CONFIGNAME, CONFIG_NAME_DB, (void*)configName, false,
		 NAMELEN, configName, VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

  // Add non camera specific parameters
  dc_status->add(DEBUG_MODE, DEBUG_MODE_DB, (void*)debug_mode_str, false, NAMELEN, debug_mode_str,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

  dc_status->add(SIM_MODE, SIM_MODE_DB, (void*)sim_mode_str, false, NAMELEN, sim_mode_str,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

  dc_status->add(STATE, STATE_DB, (void*)state_str, false, NAMELEN, state_str,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

  dc_status->add(SLAVE_STATE, SLAVE_STATE_DB, (void*)&dc_slave_state, false, 1, (int*)&dc_slave_state,
		VALIDITY_NONE, 0, (int*)nullptr, UNPUBLISHED);

  dc_status->add(DATA_STATE, DATA_STATE_DB, (void*)&dc_data_state, false, 1, (int*)&dc_data_state,
		VALIDITY_NONE, 0, (int*)nullptr, UNPUBLISHED);

  dc_status->add(ABORTING, ABORTING_FLAG_DB, (void*)aborting, false, 1, &aborting,
		VALIDITY_NONE, 0, (bool*)nullptr, UNPUBLISHED);

  dc_status->add(SAVING, SAVING_FLAG_DB, (void*)saving, false, 1, &saving,
		VALIDITY_NONE, 0, (bool*)nullptr, UNPUBLISHED);

  dc_status->add(SDSU_CANCELING, SDSU_CANCELING_FLAG_DB, (void*)sdsu_canceling, false, 1, &sdsu_canceling,
		VALIDITY_NONE, 0, (bool*)nullptr, UNPUBLISHED);

  dc_status->add(STOPPING, STOPPING_FLAG_DB, (void*)stopping, false, 1, &stopping,
		VALIDITY_NONE, 0, (bool*)nullptr, UNPUBLISHED);

  dc_status->add(CAD_START_MSG, CAD_START_MSG_DB, (void*)&cad_input_str, false, 1,  &cad_input_str,
		VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);

  dc_status->add(CONNECTION, CONNECTION_DB, (void*)connection, false, 1, &connection,
		VALIDITY_NONE, 0, (bool*)nullptr, UNPUBLISHED);

  dc_status->add(SDSU_INIT_DONE, SDSU_INIT_DONE_FLAG_DB, (void*)sdsu_init_done, false, 1, &sdsu_init_done,
		VALIDITY_NONE, 0, (bool*)nullptr, UNPUBLISHED);

  dc_status->add(SDSU_CMD_RUNNING, SDSU_CMD_RUNNING_DB, (void*)sdsu_cmd_running, false, 1, &sdsu_cmd_running,
		VALIDITY_NONE, 0, (bool*)nullptr, UNPUBLISHED);

  // DHS parameters
  sValid[0] = "DHS";
  sValid[1] = "FITS";
  sValid[2] = "NONE";
  dc_status->add(OBS_DATADEST, OBS_DATA_DEST_DB, (void*)&obs_dd, false, 1, &obs_dd,
		VALIDITY_SET, 3, sValid, UNPUBLISHED);

  dc_status->add(VIEW_DATADEST, VIEW_DATA_DEST_DB, (void*)&view_dd, false, 1, &view_dd,
		VALIDITY_SET, 3, sValid, UNPUBLISHED);

  bValid[0] = false;
  bValid[1] = true;

  /* TODO: This is linked to DHS, but see if we actually want them or not
  dc_status->add(BUNIT, BUNIT_DB, (void*)bunit, false, NAMELEN, bunit,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);
  dc_status->add(NFRAMES, NFRAMES_DB, (void*)nframes, false, 1, &nframes,
		VALIDITY_NONE, 0, (long*)nullptr, UNPUBLISHED);
  */

  // Health records
  dc_status->add(DC_HEALTH, DC_HEALTH_DB, (void*)dc_health, false, NAMELEN, dc_health,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);
  dc_status->add(DC_HEALTH_MSG, DC_HEALTH_MSG_DB, (void*)dc_health_msg, false, MAX_STRING_SIZE, dc_health_msg,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);


  strcpy(data_health, "GOOD");
  dc_status->add(DATA_HEALTH, DATA_HEALTH_DB, (void*)data_health, true, NAMELEN, data_health,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);
  strcpy(data_health_msg, " ");
  dc_status->add(DATA_HEALTH_MSG, DATA_HEALTH_MSG_DB, (void*)data_health_msg, true, MAX_STRING_SIZE, data_health_msg,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

  strcpy(slave_health, "GOOD");
  dc_status->add(SLAVE_HEALTH, SLAVE_HEALTH_DB, (void*)slave_health, true, NAMELEN, slave_health,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);
  strcpy(slave_health_msg, " ");
  dc_status->add(SLAVE_HEALTH_MSG, SLAVE_HEALTH_MSG_DB, (void*)slave_health_msg, true, MAX_STRING_SIZE, slave_health_msg,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

  strcpy(camera_health, "GOOD");
  dc_status->add(CAMERA_HEALTH, CAMERA_HEALTH_DB, (void*)camera_health, true, NAMELEN, camera_health,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);
  strcpy(camera_health_msg, " ");
  dc_status->add(CAMERA_HEALTH_MSG, CAMERA_HEALTH_MSG_DB, (void*)camera_health_msg, true, MAX_STRING_SIZE, camera_health_msg,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

  dc_status->add(CAMERA_OP, CAMERA_OP_DB, (void*)&camera_op, false, 1, &camera_op,
		 VALIDITY_SET, 2, bValid, UNPUBLISHED);
  strcpy(camera_op_msg, " ");
  dc_status->add(CAMERA_OP_MSG, CAMERA_OP_MSG_DB, (void*)camera_op_msg, true, MAX_STRING_SIZE, camera_op_msg,
		 VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

  // Status strings - written to when using ALWAYS_LOG
  for (i=0; i<N_STATUS_STRS; i++) {
    snprintf(status_par, NAMELEN, "%s%d", STATUS_STR, i+1);
    snprintf(status_par_db, NAMELEN, "%s%d", STATUS_STR_DB, i+1);
    dc_status->add(status_par, status_par_db, (void*)&status_str, false, 1, &status_str,
		   VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  }	
		
  // Internal progress vars
  lValid[0] = 0;
  lValid[1] = MAXI;
  dc_status->add(PROGRESS, nullptr, (void*)&ctl_status->progress, false, 1, &ctl_status->progress,
		VALIDITY_RANGE, 2, lValid, PUBLISHED);

  dc_status->add(TARGET, nullptr, (void*)&ctl_status->target, false, 1, &ctl_status->target,
		VALIDITY_RANGE, 2, lValid, PUBLISHED);

  dc_status->add(ITERATION, nullptr, (void*)&ctl_status->iteration, false, 1, &ctl_status->iteration,
		VALIDITY_RANGE, 2, lValid, PUBLISHED);

  // Controller vars
  strcpy(detector_type, " ");
  dc_status->add(DETTYPE, DETTYPE_DB, (void*)detector_type, true, MAX_STRING_SIZE, detector_type,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);
  strcpy(detector_id, " ");
  dc_status->add(DETID, DETID_DB, (void*)detector_id, true, MAX_STRING_SIZE, detector_id,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);
  strcpy(timing_dsp, " ");
  dc_status->add(TIMDSP, TIMDSP_DB, (void*)timing_dsp, true, MAX_STRING_SIZE, timing_dsp,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);
  strcpy(interface_dsp, " ");
  dc_status->add(INTERFACEDSP, INTERFACEDSP_DB, (void*)interface_dsp, true, MAX_STRING_SIZE, interface_dsp,
		VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

  // SDSU DSP command parameters
  // Board id - NB GNIRS does not have a utility card
  lValid[0] = 0;
  lValid[1] = INTERFACE_ID;
  lValid[2] = TIM_ID;
  dc_status->add(SDSU_BOARD, SDSU_BOARD_DB, (void*)&sdsu_board, false, 1, &sdsu_board,
		VALIDITY_SET, 3, lValid, UNPUBLISHED);

  dc_status->add(SDSU_CMD, SDSU_CMD_DB, (void*)&sdsu_cmd, false, 1, &sdsu_cmd,
		VALIDITY_NONE, 0, (long*)nullptr, UNPUBLISHED);
 
  lValid[0] = (long)SDSU_MEM_P;
  lValid[1] = (long)SDSU_MEM_X;
  lValid[2] = (long)SDSU_MEM_Y;
  dc_status->add(SDSU_MEM_SPACE, SDSU_MEM_SPACE_DB, (void*)&sdsu_mem_space, false, 1, &sdsu_mem_space,
		VALIDITY_SET, 3, lValid, UNPUBLISHED);

  lValid[0] = 0;
  lValid[1] = SDSU_DOWNLOAD_ADDR_MAX-1;
  dc_status->add(SDSU_MEM_ADDR, SDSU_MEM_ADDR_DB, (void*)&sdsu_mem_addr, false, 1, &sdsu_mem_addr,
		VALIDITY_RANGE, 0, lValid, UNPUBLISHED);

  dc_status->add(SDSU_ARG1, SDSU_ARG1_DB, (void*)&sdsu_arg1, false, 1, &sdsu_arg1,
		VALIDITY_NONE, 0, (long*)nullptr, UNPUBLISHED);
  dc_status->add(SDSU_ARG2, SDSU_ARG2_DB, (void*)&sdsu_arg2, false, 1, &sdsu_arg2,
		VALIDITY_NONE, 0, (long*)nullptr, UNPUBLISHED);
  dc_status->add(SDSU_DEBUG, SDSU_DEBUG_DB, (void*)&sdsu_debug, false, 1, &sdsu_debug,
		VALIDITY_NONE, 0, (int*)nullptr, UNPUBLISHED);
  dc_status->add(SDSU_NTESTS, SDSU_NTESTS_DB, (void*)&sdsu_ntests, false, 1, &sdsu_ntests,
		VALIDITY_NONE, 0, (int*)nullptr, UNPUBLISHED);
  dc_status->add(SDSU_COMMS, SDSU_COMMS_DB, (void*)&sdsu_comms, false, 1, &sdsu_comms,
		 VALIDITY_SET, 2, bValid, UNPUBLISHED);

  // IR CAD input parameters
  add_ir_params(IR_VIEW_MODE);
  add_ir_params(IR_OBSERVE_MODE);

  // Add CC parameters we need access to for compresed im calcs
  dc_status->add(GRATING_MIN, GRATING_MIN_DB, (void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_status->add(GRATING_MAX, GRATING_MAX_DB, (void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_status->add(GRATING_NAME, GRATING_NAME_DB, (void*)gratingName, false, NAMELEN, gratingName,
  		 VALIDITY_NONE, 0, (char*)nullptr, UNPUBLISHED);

  // Observe CAR
  dc_par->add(OBSERVEC, OBSERVEC_DB, (void*)&observec, false, 1, &observec,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);

  // Add controller error messages
  dc_par->add(CONTROLLER_INIT_MESS, CONTROLLER_INIT_MESS_DB, (void*)&cad_err_mess, false, 1, &cad_err_mess,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(CONTROLLER_TEST_MESS, CONTROLLER_TEST_MESS_DB, (void*)&cad_err_mess, false, 1, &cad_err_mess,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(CONTROLLER_RESET_MESS, CONTROLLER_RESET_MESS_DB, (void*)&cad_err_mess, false, 1, &cad_err_mess,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(CONTROLLER_SHUTDOWN_MESS, CONTROLLER_SHUTDOWN_MESS_DB, (void*)&cad_err_mess, false, 1, &cad_err_mess,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(CONTROLLER_DEBUG_MESS, CONTROLLER_DEBUG_MESS_DB, (void*)&cad_err_mess, false, 1, &cad_err_mess,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(CONTROLLER_CANCEL_MESS, CONTROLLER_CANCEL_MESS_DB, (void*)&cad_err_mess, false, 1, &cad_err_mess,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(CONTROLLER_CMD_MESS, CONTROLLER_CMD_MESS_DB, (void*)&cad_err_mess, false, 1, &cad_err_mess,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);

  // Add controller test pattern parameters - to input parameter database and also to SAD
  dc_par->add(TEST_PATTERN_SEED, TEST_PATTERN_SEED_DB, (void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(TEST_PATTERN_INC, TEST_PATTERN_INC_DB, (void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(TEST_PATTERN_AMP_INC, TEST_PATTERN_AMP_INC_DB, (void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(TEST_PATTERN_READ_INC, TEST_PATTERN_READ_INC_DB, (void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(TEST_PATTERN_OS, TEST_PATTERN_OS_DB, (void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);

  dc_status->add(TEST_PATTERN_SEED, TEST_PATTERN_SEED_DB, (void*)&verify_spec.seed, false, 1, &verify_spec.seed,
	      VALIDITY_NONE, 0, (unsigned short*)nullptr, UNPUBLISHED);
  dc_status->add(TEST_PATTERN_INC, TEST_PATTERN_INC_DB, (void*)&verify_spec.increment, false, 1, &verify_spec.increment,
	      VALIDITY_NONE, 0, (unsigned short*)nullptr, UNPUBLISHED);
  dc_status->add(TEST_PATTERN_AMP_INC, TEST_PATTERN_AMP_INC_DB, (void*)&verify_spec.amp_increment, false, 1,
		 &verify_spec.amp_increment, VALIDITY_NONE, 0, (unsigned short*)nullptr, UNPUBLISHED);
  dc_status->add(TEST_PATTERN_READ_INC, TEST_PATTERN_READ_INC_DB, (void*)&verify_spec.readout_increment, false, 1,
		 &verify_spec.readout_increment, VALIDITY_NONE, 0, (unsigned short*)nullptr, UNPUBLISHED);
  dc_status->add(TEST_PATTERN_OS, TEST_PATTERN_OS_DB, (void*)&verify_spec.overscan, false, 1, &verify_spec.overscan,
	      VALIDITY_NONE, 0, (unsigned short*)nullptr, UNPUBLISHED);
  dc_status->add(TEST_PATTERN_VERIFY, TEST_PATTERN_VERIFY_DB, (void*)&do_verify, false, 1, (bool*)&do_verify,
	      VALIDITY_NONE, 0, (bool*)nullptr, UNPUBLISHED);

}
/*
 *+
 * FUNCTION NAME: void add_ir_params()
 *
 * INVOCATION: add_ir_params(IR_Mode ir_mode)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > ir_mode - set to either VIEW or OBSERVE mode
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Adds IR CAD parameters to the dc_par database
 *
 * DESCRIPTION:
 *
 * Adds IR_Setup parameters to database. Handles VIEW and OBSERVE
 * modes separately
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */
static void add_ir_params(IR_Mode ir_mode)
{
  string mode;
 
  if (ir_mode == IR_VIEW_MODE) {
    mode = "view_";

    // Following parameters are only for VIEW mode
    dc_par->add(mode+DO_SUBTRACT, mode+DO_SUBTRACT, (void*)&cad_input_str, false, 1, &cad_input_str,
		VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
    dc_par->add(mode+SUB_FNAME, mode+SUB_FNAME, (void*)&cad_input_str, false, 1, &cad_input_str,
		VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
    dc_par->add(mode+DO_CMP_IM, mode+DO_CMP_IM, (void*)&cad_input_str, false, 1, &cad_input_str,
		VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
    dc_par->add(mode+WAVE_MIN, mode+WAVE_MIN, (void*)&cad_input_str, false, 1, &cad_input_str,
		VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
    dc_par->add(mode+WAVE_MAX, mode+WAVE_MAX, (void*)&cad_input_str, false, 1, &cad_input_str,
		VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  } else {
    mode = "obs_";
   
    // Following parameters are only for OBSERVE mode
    dc_par->add(mode+DO_SAVE_VAR, mode+DO_SAVE_VAR,(void*)&cad_input_str, false, 1, &cad_input_str,
		VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
    dc_par->add(mode+DO_SAVE_QUAL, mode+DO_SAVE_QUAL,(void*)&cad_input_str, false, 1, &cad_input_str,
		VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
    dc_par->add(mode+DO_SAVE_NDR, mode+DO_SAVE_NDR,(void*)&cad_input_str, false, 1, &cad_input_str,
		VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
    dc_par->add(mode+DO_SAVE_COADD, mode+DO_SAVE_COADD,(void*)&cad_input_str, false, 1, &cad_input_str,
		VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  }
 
  // seq CAD inputs
  dc_par->add(mode+IR_READ_MODE_IN, mode+IR_READ_MODE_IN,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(mode+IR_QUADRANTS_IN, mode+IR_QUADRANTS_IN,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
/*
  dc_par->add(mode+NRESETS, mode+NRESETS,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(mode+RESET_DELAY, mode+RESET_DELAY,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(mode+NFOWLER, mode+NFOWLER,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(mode+PERIOD, mode+PERIOD,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(mode+NPERIODS, mode+NPERIODS,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(mode+NCOADDS, mode+NCOADDS,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
*/
  dc_par->add(mode+EXPOSED, mode+EXPOSED,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
/*
  dc_par->add(mode+READ_TIME, mode+READ_TIME,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(mode+READ_INTVAL, mode+READ_INTVAL,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(mode+TIME_MODE_IN, mode+TIME_MODE_IN,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
*/

  // data CAD inputs
  dc_par->add(mode+DO_COSM_REJ, mode+DO_COSM_REJ,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(mode+COSM_THRESH, mode+COSM_THRESH,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
  dc_par->add(mode+COSM_MIN, mode+COSM_MIN,(void*)&cad_input_str, false, 1, &cad_input_str,
	      VALIDITY_NONE, 0, (string*)nullptr, UNPUBLISHED);
}
/*
 *+
 * FUNCTION NAME: void dc_command(DC_Cmd::void)
 *
 * INVOCATION: dc_command(DC_Cmd cmd, bool send_slave, bool send_data)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * > cmd - Command to send to sub tasks
 * > send_slave - flag set if to send to slave task
 * > send_data - flag set if to send to data task
 *
 * FUNCTION VALUE: none
 *
 * PURPOSE: Passes command to
 *
 * DESCRIPTION: Sets up following
 * . The DC DC status and parameter objects
 * . The Slave message queue
 * . creates the data arrays
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 *
 * DEFICIENCIES:
 *
 *-
 */

static void dc_command(DC_Cmd cmd, bool send_slave, bool send_data)
{
  DC_Slave_Msg smsg;                // Command to forward to slave
  DC_Data_Msg dmsg;                 // Command to forward to data

  if (send_data) {
    dmsg.cmd = cmd;
    logger::message(logger::Level::Full, "Sending %s message to data", to_string(dmsg.cmd).c_str());
    data_mbox->send_message((char*)&dmsg, sizeof(DC_Data_Msg));
    logger::message(logger::Level::Full, "Sent %s message to data, msgs waiting=%ld", to_string(dmsg.cmd).c_str(), data_mbox->messages_waiting());
  }

  if (send_slave) {
    smsg.cmd = cmd;
    logger::message(logger::Level::Full, "Sending %s message to slave", to_string(smsg.cmd).c_str());
    slave_mbox->send_message((char*)&smsg, sizeof(DC_Slave_Msg));
    logger::message(logger::Level::Full, "Sent %s message to slave, msgs waiting=%ld", to_string(smsg.cmd).c_str(), slave_mbox->messages_waiting());
  }
 
}

//-----------
// Registrars

static const iocshArg dcControlLoadCameraSetupArg0 = {"load",     iocshArgInt };

static const iocshArg *dcControlLoadCameraSetupArgs[] = { &dcControlLoadCameraSetupArg0 };

static const iocshFuncDef dcControlLoadCameraSetupFuncDef =
         {"dcControlLoadCameraSetup", 1, dcControlLoadCameraSetupArgs};

static void dcControlLoadCameraSetupCallFunc(const iocshArgBuf *args)
{
	load_camera_setup(args[0].ival);
}

static void dcControlRegistrar2(void) {
	iocshRegister(&dcControlLoadCameraSetupFuncDef, dcControlLoadCameraSetupCallFunc);
}


epicsExportRegistrar(dcControlRegistrar2);
