[schematic2]
uniq 90
[tools]
[detail]
w 1160 -69 100 0 n#87 esirs.doCmpIm.VAL 1120 -80 1248 -80 embbis.doCmpImO.INP
w 1184 -101 100 0 n#86 esirs.doCmpIm.FLNK 1120 -48 1168 -48 1168 -112 1248 -112 embbis.doCmpImO.SLNK
w 1128 587 100 0 n#83 esirs.cosmRej.FLNK 1120 576 1184 576 1184 512 1248 512 embbis.cosmRejO.SLNK
w 1160 555 100 0 n#82 esirs.cosmRej.VAL 1120 544 1248 544 embbis.cosmRejO.INP
w 1184 1035 100 0 n#77 esirs.doSubFile.FLNK 1120 1088 1168 1088 1168 1024 1248 1024 embbis.doSubFilO.SLNK
w 1160 1067 100 0 n#76 esirs.doSubFile.VAL 1120 1056 1248 1056 embbis.doSubFilO.INP
s 1040 -1040 500 512 GNIRS DC DHS Status/Alarm Database
[cell use]
use esirs -1168 391 100 0 rawQLS
xform 0 -960 544
p -1072 368 100 0 1 DESC:Spectral data QLS
p -1232 192 100 0 0 EGU:
p -1232 128 100 0 0 FDSC:Spectral data DHS quick look stream
p -1072 336 100 0 1 FTVL:STRING
p -1072 272 100 0 1 PV:$(sadtop)
p -1072 304 100 0 1 SNAM:
use esirs -1152 839 100 0 cmpQLS
xform 0 -944 992
p -1056 816 100 0 1 DESC:Compressed image QLS
p -1216 640 100 0 0 EGU:
p -1216 576 100 0 0 FDSC:Compressed image DHS quick look stream
p -1056 784 100 0 1 FTVL:STRING
p -1056 720 100 0 1 PV:$(sadtop)
p -1056 752 100 0 1 SNAM:
use esirs -512 839 100 0 cosmThrsh
xform 0 -304 992
p -416 816 100 0 1 DESC:Cosmic ray threshold for rejection
p -576 640 100 0 0 EGU:0/1
p -576 576 100 0 0 FDSC:Cosmic Ray threshold for rejection
p -416 784 100 0 1 FTVL:DOUBLE
p -416 720 100 0 1 PV:$(sadtop)
p -416 752 100 0 1 SNAM:
use esirs 704 327 100 0 cosmRej
xform 0 912 480
p 800 304 100 0 1 DESC:Reject cosmic rays?
p 640 128 100 0 0 EGU:0/1
p 640 64 100 0 0 FDSC:Reject cosmic rays?
p 800 272 100 0 1 FTVL:LONG
p 800 208 100 0 1 PV:$(sadtop)
p 800 240 100 0 1 SNAM:
use esirs 128 327 100 0 waveMax
xform 0 336 480
p 224 304 100 0 1 DESC:Wavelength maximum
p 64 128 100 0 0 EGU:microns
p 64 64 100 0 0 FDSC:Wavelength maximum
p 224 272 100 0 1 FTVL:DOUBLE
p 224 208 100 0 1 PV:$(sadtop)
p 224 240 100 0 1 SNAM:
use esirs -1136 -297 100 0 dataDest
xform 0 -928 -144
p -1040 -320 100 0 1 DESC:Data destination (DHS|FITS|NONE)
p -1200 -496 100 0 0 EGU:
p -1200 -560 100 0 0 FDSC:Data destination (DHS|FITS|NONE)
p -1040 -352 100 0 1 FTVL:STRING
p -1040 -416 100 0 1 PV:$(sadtop)
p -1040 -384 100 0 1 SNAM:
use esirs -512 327 100 0 waveMin
xform 0 -304 480
p -416 304 100 0 1 DESC:Wavelength minimum
p -416 240 100 0 1 EGU:microns
p -576 64 100 0 0 FDSC:Wavelength minimum
p -416 272 100 0 1 FTVL:DOUBLE
p -416 176 100 0 1 PV:$(sadtop)
p -416 208 100 0 1 SNAM:
use esirs 128 -281 100 0 subFile
xform 0 336 -128
p 224 -304 100 0 1 DESC:Name of sub file
p 64 -480 100 0 0 EGU:time
p 64 -544 100 0 0 FDSC:Name of file to subtract from raw data
p 224 -336 100 0 1 FTVL:STRING
p 224 -400 100 0 1 PV:$(sadtop)
p 320 -416 100 0 0 SCAN:Passive
p 224 -368 100 0 1 SNAM:
use esirs 704 839 100 0 doSubFile
xform 0 912 992
p 800 816 100 0 1 DESC:Is image subtracted ?
p 640 640 100 0 0 EGU:
p 640 576 100 0 0 FDSC:Is image subtracted from raw data?
p 800 784 100 0 1 FTVL:LONG
p 800 720 100 0 1 PV:$(sadtop)
p 800 752 100 0 1 SNAM:
use esirs 704 -297 100 0 doCmpIm
xform 0 912 -144
p 800 -320 100 0 1 DESC$:Compress the image along spectral acces and stack slices
p 640 -496 100 0 0 EGU:0/1
p 640 -560 100 0 0 FDSC$:Compress the image along the spectral axis and stack slices
p 800 -352 100 0 1 FTVL:LONG
p 800 -416 100 0 1 PV:$(sadtop)
p 800 -384 100 0 1 SNAM:
use esirs -496 -297 100 0 cosmMin
xform 0 -288 -144
p -400 -320 100 0 1 DESC:Cosmic ray minimum for rejection
p -560 -496 100 0 0 EGU:0/1
p -560 -560 100 0 0 FDSC:Cosmic Ray minimum for rejection
p -400 -352 100 0 1 FTVL:DOUBLE
p -400 -416 100 0 1 PV:$(sadtop)
p -400 -384 100 0 1 SNAM:
use embbis 1248 439 100 0 cosmRejO
xform 0 1376 512
p 1312 336 100 0 1 ONST:No
p 1312 400 100 0 1 PV:$(sadtop)
p 1312 368 100 0 1 ZRST:Yes
use embbis 1248 951 100 0 doSubFilO
xform 0 1376 1024
p 1312 848 100 0 1 ONST:No
p 1312 912 100 0 1 PV:$(sadtop)
p 1312 880 100 0 1 ZRST:Yes
use embbis 1248 -185 100 0 doCmpImO
xform 0 1376 -112
p 1312 -288 100 0 1 ONST:No
p 1312 -224 100 0 1 PV:$(sadtop)
p 1312 -256 100 0 1 ZRST:Yes
use bc200tr -1504 -1208 -100 0 frame
xform 0 176 96
p 1072 -1040 100 0 1 author:M Jarnyk
p 1296 -1056 100 0 -1 border:C
p 1072 -1072 100 0 1 checked:
p 1328 -1056 100 0 -1 date:15 Oct 2001
p 1312 -928 100 0 -1 project:Gemini GNIRS DC
p -1504 -1160 100 0 0 revision:1.0
p 1312 -992 150 0 -1 title:dcViewDHSSad
[comments]
