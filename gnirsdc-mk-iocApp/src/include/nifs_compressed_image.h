/*-------------------------------------------------------------------------
 * Copyright (c) 1994-2004 by RSAA Computing Section 
 * $Id: gnirs_compressed_image.h,v 1.1.1.1 2005/12/21 11:25:50 gemvx Exp $
 * GEMINI PROJECT
 * 
 * FILENAME 
 *   gnirs_compressed_image.h
 * 
 * PURPOSE 
 *
 * Class that builds a compressed image from a GNIRS raw data frame progressively
 * As data are clocked from the GNIRS detector they are passed to this routine
 * so that a compressed image of the object being observed can be constructed
 *
 * HISTORY
 *   November 2004 PJY Created for RSAA's Gemini instrument GNIRS
 */
#ifndef _GNIRS_COMPRESSED_IMAGE_H
#define _GNIRS_COMPRESSED_IMAGE_H

// Include files 


// Constants
const int GNIRS_SLICES = 28;             //+ Number of GNIRS image slices (fixed by optics)
const int GNIRS_SLICE_HEIGHT = 71;       //+ spatial height of a slice
const int GNIRS_SPECTRAL_WIDTH = 2048;   //+ Number of pixels in spectral direction
const int GNIRS_HEIGHT = 2048;           //+ Number of pixels in spacial direction
const int GNIRS_FIRST_ROW = 55;          //+ Row on detector where first slice starts
const int GNIRS_DUMMY_SLICES = 28;       //+ Number of slices in the dummy GNIRS image

// Class definitions
/*
CLASS
    GNIRS_Compressed_Image
    GNIRS Compressed Image class. 
DESCRIPTION
   Class that builds a compressed image from a GNIRS raw data frame progressively
  As data are clocked from the GNIRS detector they are passed to this routine
  so that a compressed image of the object being observed can be constructed.
   
KEYWORDS
    GNIRS
*/

class GNIRS_Compressed_Image {
private:
  // GROUP:   Private member variables
  double grating_min;                   //+ Beginning of grating wavelength range
  double grating_max;                   //+ End of grating wavelength range
  double wave_min;                      //+ Beginning of wavelength range
  double wave_max;                      //+ End of wavelength range
  int spectral_width;                   //+ spectral width
  int first_row;                        //+ Row on detector where first slice starts

  int xmin;                             //+ min GNIRS column to sum
  int xmax;                             //+ max GNIRS column to sum
  int raw_npix;                         //+ number of pixels in raw compressed image (nslices*slice_height)
  float* raw_image;                     //+ pointer to raw image buffer
  
  // GROUP:   Private member functions
  
protected:
  // GROUP:   Protected member variables
    
public:
  
  // GROUP:   Public member variables
  int nslices;                          //+ Number of GNIRS image slices (fixed by optics)
  int slice_height;                     //+ spatial height of a slice
  int nx;                               //+ number pix on xaxis
  int ny;                               //+ number pix on yaxis
  int npix;                             //+ number of pixels in compressed image (nslices*slice_height)
  float* image;                         //+ pointer to image buffer

  //+ Simple constructor
  GNIRS_Compressed_Image() { };

  //+ Normal constructor to use to create compressed image
  GNIRS_Compressed_Image(double _grating_min, double _grating_max, double _wave_min, double _wave_max, int _nslices = GNIRS_SLICES, 
			int _slice_height = GNIRS_SLICE_HEIGHT, int _spectral_width = GNIRS_SPECTRAL_WIDTH, 
			int _first_row = GNIRS_FIRST_ROW);
  
  //+ Destructor - kills connection to DHS server
  virtual ~GNIRS_Compressed_Image();   

  // GROUP:   Public member functions

  //+ Add a chunk of pixels to the compressed image
  // > fp - ptr to floating point pixels
  // > x - x offset of chunk in detector coordinates
  // > y - y offset of chunk in detector coordinates
  // > width - width of chunk
  // > height - height of chunk
  void add(float* fp, int x, int y, int width, int height);

  //+ Expand an image in x and y directions
  // > xf - x axis expansion
  // > yf - y axis expansion
  void expand(int xf, int yf);
};

#endif
