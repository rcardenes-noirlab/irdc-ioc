[schematic2]
uniq 7
[tools]
[detail]
w 1746 235 100 0 n#1 hwout.hwout#497.outp 1792 224 1760 224 embbos.cfgLock.OUT
w 1026 667 100 0 n#2 cadPlus.initSDSUP.STAT 1120 656 992 656 992 1504 junction
w 1010 1723 100 0 n#2 ecad8.initSDSU.VAL 960 1504 992 1504 992 1712 1088 1712 outhier.VAL.p
w 978 651 100 0 n#3 ecad8.initSDSU.STLK 960 640 1056 640 1056 720 1120 720 cadPlus.initSDSUP.STIN
w 578 1483 -100 0 CLID inhier.CLID.P 528 1552 576 1552 576 1472 640 1472 ecad8.initSDSU.ICID
w 994 619 100 0 n#4 ecad8.initSDSU.SPLK 960 608 1088 608 1088 688 1120 688 cadPlus.initSDSUP.SPIN
w 562 1659 100 0 n#5 inhier.DIR.P 576 1648 608 1648 608 1504 640 1504 ecad8.initSDSU.DIR
w 962 1483 100 0 n#6 ecad8.initSDSU.MESS 960 1472 1024 1472 1024 1632 1088 1632 outhier.MESS.p
s 448 16 200 0 The initialisation operation is done
s -576 16 200 0 SDSU controller initialisation CAD record.
s -576 -64 200 0 when the attached CADplus is triggered. SNL code monitors the CADPlus records.
s 1792 128 100 0 This record is set by the SNL
s 1792 96 100 0 code and sets the Cfg interlock
s 1792 64 100 0 in the CC database.
[cell use]
use hwout 1792 183 100 0 hwout#497
xform 0 1888 224
p 1888 215 100 0 -1 val(outp):$(engtop)lockCfgWr PP NMS
use ecad8 704 1552 100 0 initSDSU
xform 0 800 1056
p 736 1440 100 0 0 CTYP:0
p 736 1312 100 0 0 FTVA:LONG
p 736 1264 100 0 0 FTVB:LONG
p 736 1200 100 0 0 FTVC:LONG
p 736 1136 100 0 0 FTVD:LONG
p 736 1072 100 0 0 FTVE:LONG
p 736 1008 100 0 0 FTVF:LONG
p 736 944 100 0 0 FTVG:LONG
p 704 544 100 0 1 SNAM:dcCADinitSDSU
use embbos 1568 304 100 0 cfgLock
xform 0 1632 224
p 1568 80 100 0 1 ONST:LOCKED
p 1568 112 100 0 1 ZRST:UNLOCKED
use inhier 512 1511 100 0 CLID
xform 0 528 1552
use inhier 560 1607 100 0 DIR
xform 0 576 1648
use cadPlus 1120 768 150 0 initSDSUP
xform 0 1200 672
p 1120 560 100 0 1 set1:cad initSDSUP
use outhier 1056 1671 100 0 VAL
xform 0 1072 1712
use outhier 1056 1591 100 0 MESS
xform 0 1072 1632
use eborderC -784 -313 100 0 eborderC#87
xform 0 896 992
p 1792 -160 100 768 -1 author:Peter Young
p 1792 -20 40 0 -1 comment1:1999-01-11 GMOS version. Janet Tvedt
p 1792 -36 40 0 -1 comment2:2000-11-02 Modified for GNIRS. Peter Young
p 1792 -64 50 0 -1 comment3:2001-10-16 Modified M Jarnyk
p 1792 -192 100 768 -1 date:2000-11-02
p 2016 -112 200 768 -1 file:dcSDSUSetup
p 2288 -160 100 0 -1 page:1
p 2400 -160 100 0 -1 pages:1
p 2064 -160 100 0 -1 revision:0
p 2016 -48 150 768 -1 system:Gemini GNIRS DC
[comments]
